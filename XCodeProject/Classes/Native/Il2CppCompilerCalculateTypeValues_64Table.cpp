﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// BigIntegerLibrary.Base10BigInteger/DigitContainer
struct DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707;
// BigIntegerLibrary.BigInteger/DigitContainer
struct DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A;
// BigIntegerLibrary.BigInteger[]
struct BigIntegerU5BU5D_tA2925CFCD3B84C567E0A1EB4CC442B09C5C595EA;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466;
// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4;
// System.Action`1<ZXing.Result>
struct Action_1_t259956C287BF93B084E3B8AB9B153FE709531D37;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Byte[][]
struct ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t4BA061A629B701D5E5FAF7870E20AF0C68331CD5;
// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object>
struct IDictionary_2_tEB80604A95ECCEC9791230106E95F71A86DEE6AF;
// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object>
struct IDictionary_2_t12F35070D54BC5A73DCB2AD86A2DD9499E7B8FFB;
// System.Collections.Generic.IList`1<ZXing.QrCode.Internal.AlignmentPattern>
struct IList_1_t8C9BE039DDD994DED2BE3C4D6AB67225FCA0861D;
// System.Collections.Generic.IList`1<ZXing.Reader>
struct IList_1_t8D10A9E713DD04CC3F912E693243C5C87E35299A;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs>
struct List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs>
struct List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483;
// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs>
struct List_1_tE2C2476479F1A844897C46751D09491B2534EC87;
// System.Collections.Generic.List`1<ZXing.QrCode.Internal.FinderPattern>
struct List_1_t7493FF292FD1F3FD85ED1BEF8DD9B2F66FA54ABB;
// System.Collections.Generic.List`1<ZXing.ResultPoint[]>
struct List_1_t1A3FD96799990BD7BAB0D698FA36A285BEAA0B3E;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer>
struct Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D;
// System.Func`3<System.Int32,System.Int32,System.Boolean>[]
struct Func_3U5BU5D_tC558F376F328B1873892E6FF2BC3091C9C568553;
// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource>
struct Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE;
// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource>
struct Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Int32[][]
struct Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43;
// System.Int64[][]
struct Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.SByte[]
struct SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889;
// System.Single[][]
struct SingleU5BU5DU5BU5D_tBF3DDFFDDB461488F731F6F7ABE150AFCB620135;
// System.String
struct String_t;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Experimental.ISubsystemDescriptor
struct ISubsystemDescriptor_tDF5EB3ED639A15690D2CB9993789BB21F24D3934;
// UnityEngine.XR.ARKit.ARWorldMapSessionExtensions/OnAsyncConversionCompleteDelegate
struct OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B;
// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem
struct XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86;
// ZXing.Binarizer
struct Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309;
// ZXing.Common.BitMatrix
struct BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2;
// ZXing.Common.DecodingOptions
struct DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7;
// ZXing.Common.ReedSolomon.ReedSolomonDecoder
struct ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427;
// ZXing.LuminanceSource
struct LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1;
// ZXing.PDF417.Internal.BarcodeMetadata
struct BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A;
// ZXing.PDF417.Internal.BoundingBox
struct BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56;
// ZXing.PDF417.Internal.Codeword[]
struct CodewordU5BU5D_t747A0CB22206259FE26AC760D800A703A05EF7F4;
// ZXing.PDF417.Internal.DetectionResultColumn[]
struct DetectionResultColumnU5BU5D_t1ED8F30DD30BA49F7EDB7B22BA57D78FE3705A95;
// ZXing.PDF417.Internal.EC.ErrorCorrection
struct ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7;
// ZXing.PDF417.Internal.EC.ModulusGF
struct ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2;
// ZXing.PDF417.Internal.EC.ModulusPoly
struct ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E;
// ZXing.QrCode.Internal.ByteMatrix
struct ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8;
// ZXing.QrCode.Internal.Decoder
struct Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D;
// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D;
// ZXing.QrCode.Internal.ErrorCorrectionLevel[]
struct ErrorCorrectionLevelU5BU5D_tFBFA3B097D501D2AF5B14C2DBBBDF2B3C6E900CC;
// ZXing.QrCode.Internal.FinderPattern
struct FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE;
// ZXing.QrCode.Internal.FormatInformation
struct FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26;
// ZXing.QrCode.Internal.Mode
struct Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD;
// ZXing.QrCode.Internal.Version
struct Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1;
// ZXing.QrCode.Internal.Version/ECB[]
struct ECBU5BU5D_t980EAD239246CC90E06A178A3C2ED7671E9E6B0E;
// ZXing.QrCode.Internal.Version/ECBlocks[]
struct ECBlocksU5BU5D_tA72795133BAA9387DE9185AEEB240E175CCCE440;
// ZXing.QrCode.Internal.Version[]
struct VersionU5BU5D_t9A0DBA9F613F4D181A35C7CEF93691870C8CE02D;
// ZXing.Reader
struct Reader_t95B4B0FA61FAE2F9BFE55946AE05EC84095FCF22;
// ZXing.ResultPoint
struct ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166;
// ZXing.ResultPointCallback
struct ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3;
// ZXing.ResultPoint[]
struct ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T03C365C1B1F3344459E0514A186E8BC92EDBCDAD_H
#define U3CMODULEU3E_T03C365C1B1F3344459E0514A186E8BC92EDBCDAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t03C365C1B1F3344459E0514A186E8BC92EDBCDAD 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T03C365C1B1F3344459E0514A186E8BC92EDBCDAD_H
#ifndef U3CMODULEU3E_T1CF7C307ED677A54A6DD437A7514266A3972F693_H
#define U3CMODULEU3E_T1CF7C307ED677A54A6DD437A7514266A3972F693_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t1CF7C307ED677A54A6DD437A7514266A3972F693 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T1CF7C307ED677A54A6DD437A7514266A3972F693_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef DIGITCONTAINER_TDDB682D7845C79E7F18C0CAC83399527EBD0F707_H
#define DIGITCONTAINER_TDDB682D7845C79E7F18C0CAC83399527EBD0F707_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.Base10BigInteger/DigitContainer
struct  DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707  : public RuntimeObject
{
public:
	// System.Int64[][] BigIntegerLibrary.Base10BigInteger/DigitContainer::digits
	Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7* ___digits_0;

public:
	inline static int32_t get_offset_of_digits_0() { return static_cast<int32_t>(offsetof(DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707, ___digits_0)); }
	inline Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7* get_digits_0() const { return ___digits_0; }
	inline Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7** get_address_of_digits_0() { return &___digits_0; }
	inline void set_digits_0(Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7* value)
	{
		___digits_0 = value;
		Il2CppCodeGenWriteBarrier((&___digits_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITCONTAINER_TDDB682D7845C79E7F18C0CAC83399527EBD0F707_H
#ifndef DIGITCONTAINER_T297A710854E4D48D2B51D89683C6F4C9E708DC8A_H
#define DIGITCONTAINER_T297A710854E4D48D2B51D89683C6F4C9E708DC8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.BigInteger/DigitContainer
struct  DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A  : public RuntimeObject
{
public:
	// System.Int64[][] BigIntegerLibrary.BigInteger/DigitContainer::digits
	Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7* ___digits_0;

public:
	inline static int32_t get_offset_of_digits_0() { return static_cast<int32_t>(offsetof(DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A, ___digits_0)); }
	inline Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7* get_digits_0() const { return ___digits_0; }
	inline Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7** get_address_of_digits_0() { return &___digits_0; }
	inline void set_digits_0(Int64U5BU5DU5BU5D_t48EED7F296B890719EEEF89825CB8A3134AF5FC7* value)
	{
		___digits_0 = value;
		Il2CppCodeGenWriteBarrier((&___digits_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DIGITCONTAINER_T297A710854E4D48D2B51D89683C6F4C9E708DC8A_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#define SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem
struct  Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181  : public RuntimeObject
{
public:
	// UnityEngine.Experimental.ISubsystemDescriptor UnityEngine.Experimental.Subsystem::m_subsystemDescriptor
	RuntimeObject* ___m_subsystemDescriptor_0;

public:
	inline static int32_t get_offset_of_m_subsystemDescriptor_0() { return static_cast<int32_t>(offsetof(Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181, ___m_subsystemDescriptor_0)); }
	inline RuntimeObject* get_m_subsystemDescriptor_0() const { return ___m_subsystemDescriptor_0; }
	inline RuntimeObject** get_address_of_m_subsystemDescriptor_0() { return &___m_subsystemDescriptor_0; }
	inline void set_m_subsystemDescriptor_0(RuntimeObject* value)
	{
		___m_subsystemDescriptor_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_subsystemDescriptor_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_T9D40133A9FEB984E2C3695B077DA4DDFC91CA181_H
#ifndef ARWORLDMAPREQUESTSTATUSEXTENSIONS_T8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F_H
#define ARWORLDMAPREQUESTSTATUSEXTENSIONS_T8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequestStatusExtensions
struct  ARWorldMapRequestStatusExtensions_t8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUESTSTATUSEXTENSIONS_T8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F_H
#ifndef ARWORLDMAPSESSIONEXTENSIONS_TBBC8030654182B90E45AEBB62C1B2A34DBC0C145_H
#define ARWORLDMAPSESSIONEXTENSIONS_TBBC8030654182B90E45AEBB62C1B2A34DBC0C145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapSessionExtensions
struct  ARWorldMapSessionExtensions_tBBC8030654182B90E45AEBB62C1B2A34DBC0C145  : public RuntimeObject
{
public:

public:
};

struct ARWorldMapSessionExtensions_tBBC8030654182B90E45AEBB62C1B2A34DBC0C145_StaticFields
{
public:
	// UnityEngine.XR.ARKit.ARWorldMapSessionExtensions/OnAsyncConversionCompleteDelegate UnityEngine.XR.ARKit.ARWorldMapSessionExtensions::s_OnAsyncWorldMapCompleted
	OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B * ___s_OnAsyncWorldMapCompleted_0;

public:
	inline static int32_t get_offset_of_s_OnAsyncWorldMapCompleted_0() { return static_cast<int32_t>(offsetof(ARWorldMapSessionExtensions_tBBC8030654182B90E45AEBB62C1B2A34DBC0C145_StaticFields, ___s_OnAsyncWorldMapCompleted_0)); }
	inline OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B * get_s_OnAsyncWorldMapCompleted_0() const { return ___s_OnAsyncWorldMapCompleted_0; }
	inline OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B ** get_address_of_s_OnAsyncWorldMapCompleted_0() { return &___s_OnAsyncWorldMapCompleted_0; }
	inline void set_s_OnAsyncWorldMapCompleted_0(OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B * value)
	{
		___s_OnAsyncWorldMapCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_OnAsyncWorldMapCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPSESSIONEXTENSIONS_TBBC8030654182B90E45AEBB62C1B2A34DBC0C145_H
#ifndef U3CU3EC_TF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_H
#define U3CU3EC_TF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeReader/<>c
struct  U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_StaticFields
{
public:
	// ZXing.BarcodeReader/<>c ZXing.BarcodeReader/<>c::<>9
	U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_H
#ifndef BARCODEREADERGENERIC_T2282D9A3ED758D48E77D5D04C27A71753A574D75_H
#define BARCODEREADERGENERIC_T2282D9A3ED758D48E77D5D04C27A71753A574D75_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeReaderGeneric
struct  BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75  : public RuntimeObject
{
public:
	// ZXing.Reader ZXing.BarcodeReaderGeneric::reader
	RuntimeObject* ___reader_2;
	// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric::createRGBLuminanceSource
	Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 * ___createRGBLuminanceSource_3;
	// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric::createBinarizer
	Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D * ___createBinarizer_4;
	// System.Boolean ZXing.BarcodeReaderGeneric::usePreviousState
	bool ___usePreviousState_5;
	// ZXing.Common.DecodingOptions ZXing.BarcodeReaderGeneric::options
	DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7 * ___options_6;
	// System.Action`1<ZXing.Result> ZXing.BarcodeReaderGeneric::ResultFound
	Action_1_t259956C287BF93B084E3B8AB9B153FE709531D37 * ___ResultFound_7;
	// System.Boolean ZXing.BarcodeReaderGeneric::<AutoRotate>k__BackingField
	bool ___U3CAutoRotateU3Ek__BackingField_8;
	// System.Boolean ZXing.BarcodeReaderGeneric::<TryInverted>k__BackingField
	bool ___U3CTryInvertedU3Ek__BackingField_9;

public:
	inline static int32_t get_offset_of_reader_2() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___reader_2)); }
	inline RuntimeObject* get_reader_2() const { return ___reader_2; }
	inline RuntimeObject** get_address_of_reader_2() { return &___reader_2; }
	inline void set_reader_2(RuntimeObject* value)
	{
		___reader_2 = value;
		Il2CppCodeGenWriteBarrier((&___reader_2), value);
	}

	inline static int32_t get_offset_of_createRGBLuminanceSource_3() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___createRGBLuminanceSource_3)); }
	inline Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 * get_createRGBLuminanceSource_3() const { return ___createRGBLuminanceSource_3; }
	inline Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 ** get_address_of_createRGBLuminanceSource_3() { return &___createRGBLuminanceSource_3; }
	inline void set_createRGBLuminanceSource_3(Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 * value)
	{
		___createRGBLuminanceSource_3 = value;
		Il2CppCodeGenWriteBarrier((&___createRGBLuminanceSource_3), value);
	}

	inline static int32_t get_offset_of_createBinarizer_4() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___createBinarizer_4)); }
	inline Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D * get_createBinarizer_4() const { return ___createBinarizer_4; }
	inline Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D ** get_address_of_createBinarizer_4() { return &___createBinarizer_4; }
	inline void set_createBinarizer_4(Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D * value)
	{
		___createBinarizer_4 = value;
		Il2CppCodeGenWriteBarrier((&___createBinarizer_4), value);
	}

	inline static int32_t get_offset_of_usePreviousState_5() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___usePreviousState_5)); }
	inline bool get_usePreviousState_5() const { return ___usePreviousState_5; }
	inline bool* get_address_of_usePreviousState_5() { return &___usePreviousState_5; }
	inline void set_usePreviousState_5(bool value)
	{
		___usePreviousState_5 = value;
	}

	inline static int32_t get_offset_of_options_6() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___options_6)); }
	inline DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7 * get_options_6() const { return ___options_6; }
	inline DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7 ** get_address_of_options_6() { return &___options_6; }
	inline void set_options_6(DecodingOptions_t47C061C31B88DE9FB76187F5289E251C3B2E35B7 * value)
	{
		___options_6 = value;
		Il2CppCodeGenWriteBarrier((&___options_6), value);
	}

	inline static int32_t get_offset_of_ResultFound_7() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___ResultFound_7)); }
	inline Action_1_t259956C287BF93B084E3B8AB9B153FE709531D37 * get_ResultFound_7() const { return ___ResultFound_7; }
	inline Action_1_t259956C287BF93B084E3B8AB9B153FE709531D37 ** get_address_of_ResultFound_7() { return &___ResultFound_7; }
	inline void set_ResultFound_7(Action_1_t259956C287BF93B084E3B8AB9B153FE709531D37 * value)
	{
		___ResultFound_7 = value;
		Il2CppCodeGenWriteBarrier((&___ResultFound_7), value);
	}

	inline static int32_t get_offset_of_U3CAutoRotateU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___U3CAutoRotateU3Ek__BackingField_8)); }
	inline bool get_U3CAutoRotateU3Ek__BackingField_8() const { return ___U3CAutoRotateU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CAutoRotateU3Ek__BackingField_8() { return &___U3CAutoRotateU3Ek__BackingField_8; }
	inline void set_U3CAutoRotateU3Ek__BackingField_8(bool value)
	{
		___U3CAutoRotateU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CTryInvertedU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75, ___U3CTryInvertedU3Ek__BackingField_9)); }
	inline bool get_U3CTryInvertedU3Ek__BackingField_9() const { return ___U3CTryInvertedU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CTryInvertedU3Ek__BackingField_9() { return &___U3CTryInvertedU3Ek__BackingField_9; }
	inline void set_U3CTryInvertedU3Ek__BackingField_9(bool value)
	{
		___U3CTryInvertedU3Ek__BackingField_9 = value;
	}
};

struct BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75_StaticFields
{
public:
	// System.Func`2<ZXing.LuminanceSource,ZXing.Binarizer> ZXing.BarcodeReaderGeneric::defaultCreateBinarizer
	Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D * ___defaultCreateBinarizer_0;
	// System.Func`5<System.Byte[],System.Int32,System.Int32,ZXing.RGBLuminanceSource/BitmapFormat,ZXing.LuminanceSource> ZXing.BarcodeReaderGeneric::defaultCreateRGBLuminanceSource
	Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 * ___defaultCreateRGBLuminanceSource_1;

public:
	inline static int32_t get_offset_of_defaultCreateBinarizer_0() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75_StaticFields, ___defaultCreateBinarizer_0)); }
	inline Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D * get_defaultCreateBinarizer_0() const { return ___defaultCreateBinarizer_0; }
	inline Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D ** get_address_of_defaultCreateBinarizer_0() { return &___defaultCreateBinarizer_0; }
	inline void set_defaultCreateBinarizer_0(Func_2_tE776185CE298FA6C8F318215141B74E34A068A5D * value)
	{
		___defaultCreateBinarizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCreateBinarizer_0), value);
	}

	inline static int32_t get_offset_of_defaultCreateRGBLuminanceSource_1() { return static_cast<int32_t>(offsetof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75_StaticFields, ___defaultCreateRGBLuminanceSource_1)); }
	inline Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 * get_defaultCreateRGBLuminanceSource_1() const { return ___defaultCreateRGBLuminanceSource_1; }
	inline Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 ** get_address_of_defaultCreateRGBLuminanceSource_1() { return &___defaultCreateRGBLuminanceSource_1; }
	inline void set_defaultCreateRGBLuminanceSource_1(Func_5_tC04EE920F614E30A7A38CA60F77DA27EECCE9C35 * value)
	{
		___defaultCreateRGBLuminanceSource_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCreateRGBLuminanceSource_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARCODEREADERGENERIC_T2282D9A3ED758D48E77D5D04C27A71753A574D75_H
#ifndef U3CU3EC_T20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_H
#define U3CU3EC_T20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeReaderGeneric/<>c
struct  U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_StaticFields
{
public:
	// ZXing.BarcodeReaderGeneric/<>c ZXing.BarcodeReaderGeneric/<>c::<>9
	U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_H
#ifndef BINARIZER_T5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309_H
#define BINARIZER_T5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Binarizer
struct  Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309  : public RuntimeObject
{
public:
	// ZXing.LuminanceSource ZXing.Binarizer::source
	LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * ___source_0;

public:
	inline static int32_t get_offset_of_source_0() { return static_cast<int32_t>(offsetof(Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309, ___source_0)); }
	inline LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * get_source_0() const { return ___source_0; }
	inline LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 ** get_address_of_source_0() { return &___source_0; }
	inline void set_source_0(LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * value)
	{
		___source_0 = value;
		Il2CppCodeGenWriteBarrier((&___source_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARIZER_T5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309_H
#ifndef BINARYBITMAP_TCF98378D7F884CDFBC4985A0ACCFFBE31FC51461_H
#define BINARYBITMAP_TCF98378D7F884CDFBC4985A0ACCFFBE31FC51461_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BinaryBitmap
struct  BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461  : public RuntimeObject
{
public:
	// ZXing.Binarizer ZXing.BinaryBitmap::binarizer
	Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309 * ___binarizer_0;
	// ZXing.Common.BitMatrix ZXing.BinaryBitmap::matrix
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___matrix_1;

public:
	inline static int32_t get_offset_of_binarizer_0() { return static_cast<int32_t>(offsetof(BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461, ___binarizer_0)); }
	inline Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309 * get_binarizer_0() const { return ___binarizer_0; }
	inline Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309 ** get_address_of_binarizer_0() { return &___binarizer_0; }
	inline void set_binarizer_0(Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309 * value)
	{
		___binarizer_0 = value;
		Il2CppCodeGenWriteBarrier((&___binarizer_0), value);
	}

	inline static int32_t get_offset_of_matrix_1() { return static_cast<int32_t>(offsetof(BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461, ___matrix_1)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_matrix_1() const { return ___matrix_1; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_matrix_1() { return &___matrix_1; }
	inline void set_matrix_1(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___matrix_1 = value;
		Il2CppCodeGenWriteBarrier((&___matrix_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYBITMAP_TCF98378D7F884CDFBC4985A0ACCFFBE31FC51461_H
#ifndef LUMINANCESOURCE_T0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1_H
#define LUMINANCESOURCE_T0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.LuminanceSource
struct  LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1  : public RuntimeObject
{
public:
	// System.Int32 ZXing.LuminanceSource::width
	int32_t ___width_0;
	// System.Int32 ZXing.LuminanceSource::height
	int32_t ___height_1;

public:
	inline static int32_t get_offset_of_width_0() { return static_cast<int32_t>(offsetof(LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1, ___width_0)); }
	inline int32_t get_width_0() const { return ___width_0; }
	inline int32_t* get_address_of_width_0() { return &___width_0; }
	inline void set_width_0(int32_t value)
	{
		___width_0 = value;
	}

	inline static int32_t get_offset_of_height_1() { return static_cast<int32_t>(offsetof(LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1, ___height_1)); }
	inline int32_t get_height_1() const { return ___height_1; }
	inline int32_t* get_address_of_height_1() { return &___height_1; }
	inline void set_height_1(int32_t value)
	{
		___height_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LUMINANCESOURCE_T0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1_H
#ifndef MULTIFORMATREADER_TDC3266BB3E38302717ABCB100E1C320E5C15FB34_H
#define MULTIFORMATREADER_TDC3266BB3E38302717ABCB100E1C320E5C15FB34_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.MultiFormatReader
struct  MultiFormatReader_tDC3266BB3E38302717ABCB100E1C320E5C15FB34  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<ZXing.DecodeHintType,System.Object> ZXing.MultiFormatReader::hints
	RuntimeObject* ___hints_0;
	// System.Collections.Generic.IList`1<ZXing.Reader> ZXing.MultiFormatReader::readers
	RuntimeObject* ___readers_1;

public:
	inline static int32_t get_offset_of_hints_0() { return static_cast<int32_t>(offsetof(MultiFormatReader_tDC3266BB3E38302717ABCB100E1C320E5C15FB34, ___hints_0)); }
	inline RuntimeObject* get_hints_0() const { return ___hints_0; }
	inline RuntimeObject** get_address_of_hints_0() { return &___hints_0; }
	inline void set_hints_0(RuntimeObject* value)
	{
		___hints_0 = value;
		Il2CppCodeGenWriteBarrier((&___hints_0), value);
	}

	inline static int32_t get_offset_of_readers_1() { return static_cast<int32_t>(offsetof(MultiFormatReader_tDC3266BB3E38302717ABCB100E1C320E5C15FB34, ___readers_1)); }
	inline RuntimeObject* get_readers_1() const { return ___readers_1; }
	inline RuntimeObject** get_address_of_readers_1() { return &___readers_1; }
	inline void set_readers_1(RuntimeObject* value)
	{
		___readers_1 = value;
		Il2CppCodeGenWriteBarrier((&___readers_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MULTIFORMATREADER_TDC3266BB3E38302717ABCB100E1C320E5C15FB34_H
#ifndef ONEDREADER_TBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_H
#define ONEDREADER_TBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.OneDReader
struct  OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC  : public RuntimeObject
{
public:

public:
};

struct OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields
{
public:
	// System.Int32 ZXing.OneD.OneDReader::INTEGER_MATH_SHIFT
	int32_t ___INTEGER_MATH_SHIFT_0;
	// System.Int32 ZXing.OneD.OneDReader::PATTERN_MATCH_RESULT_SCALE_FACTOR
	int32_t ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1;

public:
	inline static int32_t get_offset_of_INTEGER_MATH_SHIFT_0() { return static_cast<int32_t>(offsetof(OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields, ___INTEGER_MATH_SHIFT_0)); }
	inline int32_t get_INTEGER_MATH_SHIFT_0() const { return ___INTEGER_MATH_SHIFT_0; }
	inline int32_t* get_address_of_INTEGER_MATH_SHIFT_0() { return &___INTEGER_MATH_SHIFT_0; }
	inline void set_INTEGER_MATH_SHIFT_0(int32_t value)
	{
		___INTEGER_MATH_SHIFT_0 = value;
	}

	inline static int32_t get_offset_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() { return static_cast<int32_t>(offsetof(OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_StaticFields, ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1)); }
	inline int32_t get_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() const { return ___PATTERN_MATCH_RESULT_SCALE_FACTOR_1; }
	inline int32_t* get_address_of_PATTERN_MATCH_RESULT_SCALE_FACTOR_1() { return &___PATTERN_MATCH_RESULT_SCALE_FACTOR_1; }
	inline void set_PATTERN_MATCH_RESULT_SCALE_FACTOR_1(int32_t value)
	{
		___PATTERN_MATCH_RESULT_SCALE_FACTOR_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONEDREADER_TBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC_H
#ifndef BARCODEMETADATA_TF3EEE8FE516C577460E35B108452BF404A24728A_H
#define BARCODEMETADATA_TF3EEE8FE516C577460E35B108452BF404A24728A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BarcodeMetadata
struct  BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A  : public RuntimeObject
{
public:
	// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::<ColumnCount>k__BackingField
	int32_t ___U3CColumnCountU3Ek__BackingField_0;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::<ErrorCorrectionLevel>k__BackingField
	int32_t ___U3CErrorCorrectionLevelU3Ek__BackingField_1;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::<RowCountUpper>k__BackingField
	int32_t ___U3CRowCountUpperU3Ek__BackingField_2;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::<RowCountLower>k__BackingField
	int32_t ___U3CRowCountLowerU3Ek__BackingField_3;
	// System.Int32 ZXing.PDF417.Internal.BarcodeMetadata::<RowCount>k__BackingField
	int32_t ___U3CRowCountU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CColumnCountU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A, ___U3CColumnCountU3Ek__BackingField_0)); }
	inline int32_t get_U3CColumnCountU3Ek__BackingField_0() const { return ___U3CColumnCountU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CColumnCountU3Ek__BackingField_0() { return &___U3CColumnCountU3Ek__BackingField_0; }
	inline void set_U3CColumnCountU3Ek__BackingField_0(int32_t value)
	{
		___U3CColumnCountU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorCorrectionLevelU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A, ___U3CErrorCorrectionLevelU3Ek__BackingField_1)); }
	inline int32_t get_U3CErrorCorrectionLevelU3Ek__BackingField_1() const { return ___U3CErrorCorrectionLevelU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CErrorCorrectionLevelU3Ek__BackingField_1() { return &___U3CErrorCorrectionLevelU3Ek__BackingField_1; }
	inline void set_U3CErrorCorrectionLevelU3Ek__BackingField_1(int32_t value)
	{
		___U3CErrorCorrectionLevelU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CRowCountUpperU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A, ___U3CRowCountUpperU3Ek__BackingField_2)); }
	inline int32_t get_U3CRowCountUpperU3Ek__BackingField_2() const { return ___U3CRowCountUpperU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CRowCountUpperU3Ek__BackingField_2() { return &___U3CRowCountUpperU3Ek__BackingField_2; }
	inline void set_U3CRowCountUpperU3Ek__BackingField_2(int32_t value)
	{
		___U3CRowCountUpperU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CRowCountLowerU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A, ___U3CRowCountLowerU3Ek__BackingField_3)); }
	inline int32_t get_U3CRowCountLowerU3Ek__BackingField_3() const { return ___U3CRowCountLowerU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CRowCountLowerU3Ek__BackingField_3() { return &___U3CRowCountLowerU3Ek__BackingField_3; }
	inline void set_U3CRowCountLowerU3Ek__BackingField_3(int32_t value)
	{
		___U3CRowCountLowerU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CRowCountU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A, ___U3CRowCountU3Ek__BackingField_4)); }
	inline int32_t get_U3CRowCountU3Ek__BackingField_4() const { return ___U3CRowCountU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CRowCountU3Ek__BackingField_4() { return &___U3CRowCountU3Ek__BackingField_4; }
	inline void set_U3CRowCountU3Ek__BackingField_4(int32_t value)
	{
		___U3CRowCountU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARCODEMETADATA_TF3EEE8FE516C577460E35B108452BF404A24728A_H
#ifndef BARCODEVALUE_T33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F_H
#define BARCODEVALUE_T33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BarcodeValue
struct  BarcodeValue_t33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F  : public RuntimeObject
{
public:
	// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32> ZXing.PDF417.Internal.BarcodeValue::values
	RuntimeObject* ___values_0;

public:
	inline static int32_t get_offset_of_values_0() { return static_cast<int32_t>(offsetof(BarcodeValue_t33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F, ___values_0)); }
	inline RuntimeObject* get_values_0() const { return ___values_0; }
	inline RuntimeObject** get_address_of_values_0() { return &___values_0; }
	inline void set_values_0(RuntimeObject* value)
	{
		___values_0 = value;
		Il2CppCodeGenWriteBarrier((&___values_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARCODEVALUE_T33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F_H
#ifndef BOUNDINGBOX_T08AA711EC82C223AA81B9069390385E08A355D56_H
#define BOUNDINGBOX_T08AA711EC82C223AA81B9069390385E08A355D56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.BoundingBox
struct  BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.PDF417.Internal.BoundingBox::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_0;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<TopLeft>k__BackingField
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * ___U3CTopLeftU3Ek__BackingField_1;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<TopRight>k__BackingField
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * ___U3CTopRightU3Ek__BackingField_2;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<BottomLeft>k__BackingField
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * ___U3CBottomLeftU3Ek__BackingField_3;
	// ZXing.ResultPoint ZXing.PDF417.Internal.BoundingBox::<BottomRight>k__BackingField
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * ___U3CBottomRightU3Ek__BackingField_4;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MinX>k__BackingField
	int32_t ___U3CMinXU3Ek__BackingField_5;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MaxX>k__BackingField
	int32_t ___U3CMaxXU3Ek__BackingField_6;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MinY>k__BackingField
	int32_t ___U3CMinYU3Ek__BackingField_7;
	// System.Int32 ZXing.PDF417.Internal.BoundingBox::<MaxY>k__BackingField
	int32_t ___U3CMaxYU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___image_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_U3CTopLeftU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CTopLeftU3Ek__BackingField_1)); }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * get_U3CTopLeftU3Ek__BackingField_1() const { return ___U3CTopLeftU3Ek__BackingField_1; }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 ** get_address_of_U3CTopLeftU3Ek__BackingField_1() { return &___U3CTopLeftU3Ek__BackingField_1; }
	inline void set_U3CTopLeftU3Ek__BackingField_1(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * value)
	{
		___U3CTopLeftU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTopLeftU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTopRightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CTopRightU3Ek__BackingField_2)); }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * get_U3CTopRightU3Ek__BackingField_2() const { return ___U3CTopRightU3Ek__BackingField_2; }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 ** get_address_of_U3CTopRightU3Ek__BackingField_2() { return &___U3CTopRightU3Ek__BackingField_2; }
	inline void set_U3CTopRightU3Ek__BackingField_2(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * value)
	{
		___U3CTopRightU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTopRightU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CBottomLeftU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CBottomLeftU3Ek__BackingField_3)); }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * get_U3CBottomLeftU3Ek__BackingField_3() const { return ___U3CBottomLeftU3Ek__BackingField_3; }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 ** get_address_of_U3CBottomLeftU3Ek__BackingField_3() { return &___U3CBottomLeftU3Ek__BackingField_3; }
	inline void set_U3CBottomLeftU3Ek__BackingField_3(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * value)
	{
		___U3CBottomLeftU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBottomLeftU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CBottomRightU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CBottomRightU3Ek__BackingField_4)); }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * get_U3CBottomRightU3Ek__BackingField_4() const { return ___U3CBottomRightU3Ek__BackingField_4; }
	inline ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 ** get_address_of_U3CBottomRightU3Ek__BackingField_4() { return &___U3CBottomRightU3Ek__BackingField_4; }
	inline void set_U3CBottomRightU3Ek__BackingField_4(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166 * value)
	{
		___U3CBottomRightU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBottomRightU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMinXU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CMinXU3Ek__BackingField_5)); }
	inline int32_t get_U3CMinXU3Ek__BackingField_5() const { return ___U3CMinXU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CMinXU3Ek__BackingField_5() { return &___U3CMinXU3Ek__BackingField_5; }
	inline void set_U3CMinXU3Ek__BackingField_5(int32_t value)
	{
		___U3CMinXU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMaxXU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CMaxXU3Ek__BackingField_6)); }
	inline int32_t get_U3CMaxXU3Ek__BackingField_6() const { return ___U3CMaxXU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CMaxXU3Ek__BackingField_6() { return &___U3CMaxXU3Ek__BackingField_6; }
	inline void set_U3CMaxXU3Ek__BackingField_6(int32_t value)
	{
		___U3CMaxXU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CMinYU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CMinYU3Ek__BackingField_7)); }
	inline int32_t get_U3CMinYU3Ek__BackingField_7() const { return ___U3CMinYU3Ek__BackingField_7; }
	inline int32_t* get_address_of_U3CMinYU3Ek__BackingField_7() { return &___U3CMinYU3Ek__BackingField_7; }
	inline void set_U3CMinYU3Ek__BackingField_7(int32_t value)
	{
		___U3CMinYU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CMaxYU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56, ___U3CMaxYU3Ek__BackingField_8)); }
	inline int32_t get_U3CMaxYU3Ek__BackingField_8() const { return ___U3CMaxYU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CMaxYU3Ek__BackingField_8() { return &___U3CMaxYU3Ek__BackingField_8; }
	inline void set_U3CMaxYU3Ek__BackingField_8(int32_t value)
	{
		___U3CMaxYU3Ek__BackingField_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOUNDINGBOX_T08AA711EC82C223AA81B9069390385E08A355D56_H
#ifndef CODEWORD_TD90F903011EE5D063DD3BA2A4D90E6449F688974_H
#define CODEWORD_TD90F903011EE5D063DD3BA2A4D90E6449F688974_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.Codeword
struct  Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974  : public RuntimeObject
{
public:
	// System.Int32 ZXing.PDF417.Internal.Codeword::<StartX>k__BackingField
	int32_t ___U3CStartXU3Ek__BackingField_1;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<EndX>k__BackingField
	int32_t ___U3CEndXU3Ek__BackingField_2;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<Bucket>k__BackingField
	int32_t ___U3CBucketU3Ek__BackingField_3;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<Value>k__BackingField
	int32_t ___U3CValueU3Ek__BackingField_4;
	// System.Int32 ZXing.PDF417.Internal.Codeword::<RowNumber>k__BackingField
	int32_t ___U3CRowNumberU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CStartXU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974, ___U3CStartXU3Ek__BackingField_1)); }
	inline int32_t get_U3CStartXU3Ek__BackingField_1() const { return ___U3CStartXU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CStartXU3Ek__BackingField_1() { return &___U3CStartXU3Ek__BackingField_1; }
	inline void set_U3CStartXU3Ek__BackingField_1(int32_t value)
	{
		___U3CStartXU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CEndXU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974, ___U3CEndXU3Ek__BackingField_2)); }
	inline int32_t get_U3CEndXU3Ek__BackingField_2() const { return ___U3CEndXU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CEndXU3Ek__BackingField_2() { return &___U3CEndXU3Ek__BackingField_2; }
	inline void set_U3CEndXU3Ek__BackingField_2(int32_t value)
	{
		___U3CEndXU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CBucketU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974, ___U3CBucketU3Ek__BackingField_3)); }
	inline int32_t get_U3CBucketU3Ek__BackingField_3() const { return ___U3CBucketU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CBucketU3Ek__BackingField_3() { return &___U3CBucketU3Ek__BackingField_3; }
	inline void set_U3CBucketU3Ek__BackingField_3(int32_t value)
	{
		___U3CBucketU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974, ___U3CValueU3Ek__BackingField_4)); }
	inline int32_t get_U3CValueU3Ek__BackingField_4() const { return ___U3CValueU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CValueU3Ek__BackingField_4() { return &___U3CValueU3Ek__BackingField_4; }
	inline void set_U3CValueU3Ek__BackingField_4(int32_t value)
	{
		___U3CValueU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRowNumberU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974, ___U3CRowNumberU3Ek__BackingField_5)); }
	inline int32_t get_U3CRowNumberU3Ek__BackingField_5() const { return ___U3CRowNumberU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CRowNumberU3Ek__BackingField_5() { return &___U3CRowNumberU3Ek__BackingField_5; }
	inline void set_U3CRowNumberU3Ek__BackingField_5(int32_t value)
	{
		___U3CRowNumberU3Ek__BackingField_5 = value;
	}
};

struct Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974_StaticFields
{
public:
	// System.Int32 ZXing.PDF417.Internal.Codeword::BARCODE_ROW_UNKNOWN
	int32_t ___BARCODE_ROW_UNKNOWN_0;

public:
	inline static int32_t get_offset_of_BARCODE_ROW_UNKNOWN_0() { return static_cast<int32_t>(offsetof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974_StaticFields, ___BARCODE_ROW_UNKNOWN_0)); }
	inline int32_t get_BARCODE_ROW_UNKNOWN_0() const { return ___BARCODE_ROW_UNKNOWN_0; }
	inline int32_t* get_address_of_BARCODE_ROW_UNKNOWN_0() { return &___BARCODE_ROW_UNKNOWN_0; }
	inline void set_BARCODE_ROW_UNKNOWN_0(int32_t value)
	{
		___BARCODE_ROW_UNKNOWN_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODEWORD_TD90F903011EE5D063DD3BA2A4D90E6449F688974_H
#ifndef DECODEDBITSTREAMPARSER_T0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_H
#define DECODEDBITSTREAMPARSER_T0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980  : public RuntimeObject
{
public:

public:
};

struct DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields
{
public:
	// System.Char[] ZXing.PDF417.Internal.DecodedBitStreamParser::PUNCT_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___PUNCT_CHARS_0;
	// System.Char[] ZXing.PDF417.Internal.DecodedBitStreamParser::MIXED_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___MIXED_CHARS_1;
	// BigIntegerLibrary.BigInteger[] ZXing.PDF417.Internal.DecodedBitStreamParser::EXP900
	BigIntegerU5BU5D_tA2925CFCD3B84C567E0A1EB4CC442B09C5C595EA* ___EXP900_2;

public:
	inline static int32_t get_offset_of_PUNCT_CHARS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields, ___PUNCT_CHARS_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_PUNCT_CHARS_0() const { return ___PUNCT_CHARS_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_PUNCT_CHARS_0() { return &___PUNCT_CHARS_0; }
	inline void set_PUNCT_CHARS_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___PUNCT_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier((&___PUNCT_CHARS_0), value);
	}

	inline static int32_t get_offset_of_MIXED_CHARS_1() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields, ___MIXED_CHARS_1)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_MIXED_CHARS_1() const { return ___MIXED_CHARS_1; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_MIXED_CHARS_1() { return &___MIXED_CHARS_1; }
	inline void set_MIXED_CHARS_1(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___MIXED_CHARS_1 = value;
		Il2CppCodeGenWriteBarrier((&___MIXED_CHARS_1), value);
	}

	inline static int32_t get_offset_of_EXP900_2() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields, ___EXP900_2)); }
	inline BigIntegerU5BU5D_tA2925CFCD3B84C567E0A1EB4CC442B09C5C595EA* get_EXP900_2() const { return ___EXP900_2; }
	inline BigIntegerU5BU5D_tA2925CFCD3B84C567E0A1EB4CC442B09C5C595EA** get_address_of_EXP900_2() { return &___EXP900_2; }
	inline void set_EXP900_2(BigIntegerU5BU5D_tA2925CFCD3B84C567E0A1EB4CC442B09C5C595EA* value)
	{
		___EXP900_2 = value;
		Il2CppCodeGenWriteBarrier((&___EXP900_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDBITSTREAMPARSER_T0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_H
#ifndef DETECTIONRESULT_T6582585457566AD9445F4C08102CB688440BB54E_H
#define DETECTIONRESULT_T6582585457566AD9445F4C08102CB688440BB54E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DetectionResult
struct  DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E  : public RuntimeObject
{
public:
	// ZXing.PDF417.Internal.BarcodeMetadata ZXing.PDF417.Internal.DetectionResult::<Metadata>k__BackingField
	BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A * ___U3CMetadataU3Ek__BackingField_0;
	// ZXing.PDF417.Internal.DetectionResultColumn[] ZXing.PDF417.Internal.DetectionResult::<DetectionResultColumns>k__BackingField
	DetectionResultColumnU5BU5D_t1ED8F30DD30BA49F7EDB7B22BA57D78FE3705A95* ___U3CDetectionResultColumnsU3Ek__BackingField_1;
	// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResult::<Box>k__BackingField
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 * ___U3CBoxU3Ek__BackingField_2;
	// System.Int32 ZXing.PDF417.Internal.DetectionResult::<ColumnCount>k__BackingField
	int32_t ___U3CColumnCountU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CMetadataU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E, ___U3CMetadataU3Ek__BackingField_0)); }
	inline BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A * get_U3CMetadataU3Ek__BackingField_0() const { return ___U3CMetadataU3Ek__BackingField_0; }
	inline BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A ** get_address_of_U3CMetadataU3Ek__BackingField_0() { return &___U3CMetadataU3Ek__BackingField_0; }
	inline void set_U3CMetadataU3Ek__BackingField_0(BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A * value)
	{
		___U3CMetadataU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMetadataU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CDetectionResultColumnsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E, ___U3CDetectionResultColumnsU3Ek__BackingField_1)); }
	inline DetectionResultColumnU5BU5D_t1ED8F30DD30BA49F7EDB7B22BA57D78FE3705A95* get_U3CDetectionResultColumnsU3Ek__BackingField_1() const { return ___U3CDetectionResultColumnsU3Ek__BackingField_1; }
	inline DetectionResultColumnU5BU5D_t1ED8F30DD30BA49F7EDB7B22BA57D78FE3705A95** get_address_of_U3CDetectionResultColumnsU3Ek__BackingField_1() { return &___U3CDetectionResultColumnsU3Ek__BackingField_1; }
	inline void set_U3CDetectionResultColumnsU3Ek__BackingField_1(DetectionResultColumnU5BU5D_t1ED8F30DD30BA49F7EDB7B22BA57D78FE3705A95* value)
	{
		___U3CDetectionResultColumnsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDetectionResultColumnsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CBoxU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E, ___U3CBoxU3Ek__BackingField_2)); }
	inline BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 * get_U3CBoxU3Ek__BackingField_2() const { return ___U3CBoxU3Ek__BackingField_2; }
	inline BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 ** get_address_of_U3CBoxU3Ek__BackingField_2() { return &___U3CBoxU3Ek__BackingField_2; }
	inline void set_U3CBoxU3Ek__BackingField_2(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 * value)
	{
		___U3CBoxU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoxU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CColumnCountU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E, ___U3CColumnCountU3Ek__BackingField_3)); }
	inline int32_t get_U3CColumnCountU3Ek__BackingField_3() const { return ___U3CColumnCountU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CColumnCountU3Ek__BackingField_3() { return &___U3CColumnCountU3Ek__BackingField_3; }
	inline void set_U3CColumnCountU3Ek__BackingField_3(int32_t value)
	{
		___U3CColumnCountU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTIONRESULT_T6582585457566AD9445F4C08102CB688440BB54E_H
#ifndef DETECTIONRESULTCOLUMN_TE9087A29135703042A8D509F68778284E6F46EF3_H
#define DETECTIONRESULTCOLUMN_TE9087A29135703042A8D509F68778284E6F46EF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DetectionResultColumn
struct  DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3  : public RuntimeObject
{
public:
	// ZXing.PDF417.Internal.BoundingBox ZXing.PDF417.Internal.DetectionResultColumn::<Box>k__BackingField
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 * ___U3CBoxU3Ek__BackingField_0;
	// ZXing.PDF417.Internal.Codeword[] ZXing.PDF417.Internal.DetectionResultColumn::<Codewords>k__BackingField
	CodewordU5BU5D_t747A0CB22206259FE26AC760D800A703A05EF7F4* ___U3CCodewordsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBoxU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3, ___U3CBoxU3Ek__BackingField_0)); }
	inline BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 * get_U3CBoxU3Ek__BackingField_0() const { return ___U3CBoxU3Ek__BackingField_0; }
	inline BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 ** get_address_of_U3CBoxU3Ek__BackingField_0() { return &___U3CBoxU3Ek__BackingField_0; }
	inline void set_U3CBoxU3Ek__BackingField_0(BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56 * value)
	{
		___U3CBoxU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBoxU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCodewordsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3, ___U3CCodewordsU3Ek__BackingField_1)); }
	inline CodewordU5BU5D_t747A0CB22206259FE26AC760D800A703A05EF7F4* get_U3CCodewordsU3Ek__BackingField_1() const { return ___U3CCodewordsU3Ek__BackingField_1; }
	inline CodewordU5BU5D_t747A0CB22206259FE26AC760D800A703A05EF7F4** get_address_of_U3CCodewordsU3Ek__BackingField_1() { return &___U3CCodewordsU3Ek__BackingField_1; }
	inline void set_U3CCodewordsU3Ek__BackingField_1(CodewordU5BU5D_t747A0CB22206259FE26AC760D800A703A05EF7F4* value)
	{
		___U3CCodewordsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCodewordsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTIONRESULTCOLUMN_TE9087A29135703042A8D509F68778284E6F46EF3_H
#ifndef DETECTOR_TC3C9DEF66D12EF6A5869D91A688D9997865D167E_H
#define DETECTOR_TC3C9DEF66D12EF6A5869D91A688D9997865D167E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.Detector
struct  Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E  : public RuntimeObject
{
public:

public:
};

struct Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields
{
public:
	// System.Int32[] ZXing.PDF417.Internal.Detector::INDEXES_START_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INDEXES_START_PATTERN_0;
	// System.Int32[] ZXing.PDF417.Internal.Detector::INDEXES_STOP_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___INDEXES_STOP_PATTERN_1;
	// System.Int32[] ZXing.PDF417.Internal.Detector::START_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___START_PATTERN_2;
	// System.Int32[] ZXing.PDF417.Internal.Detector::STOP_PATTERN
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___STOP_PATTERN_3;

public:
	inline static int32_t get_offset_of_INDEXES_START_PATTERN_0() { return static_cast<int32_t>(offsetof(Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields, ___INDEXES_START_PATTERN_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INDEXES_START_PATTERN_0() const { return ___INDEXES_START_PATTERN_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INDEXES_START_PATTERN_0() { return &___INDEXES_START_PATTERN_0; }
	inline void set_INDEXES_START_PATTERN_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INDEXES_START_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier((&___INDEXES_START_PATTERN_0), value);
	}

	inline static int32_t get_offset_of_INDEXES_STOP_PATTERN_1() { return static_cast<int32_t>(offsetof(Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields, ___INDEXES_STOP_PATTERN_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_INDEXES_STOP_PATTERN_1() const { return ___INDEXES_STOP_PATTERN_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_INDEXES_STOP_PATTERN_1() { return &___INDEXES_STOP_PATTERN_1; }
	inline void set_INDEXES_STOP_PATTERN_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___INDEXES_STOP_PATTERN_1 = value;
		Il2CppCodeGenWriteBarrier((&___INDEXES_STOP_PATTERN_1), value);
	}

	inline static int32_t get_offset_of_START_PATTERN_2() { return static_cast<int32_t>(offsetof(Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields, ___START_PATTERN_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_START_PATTERN_2() const { return ___START_PATTERN_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_START_PATTERN_2() { return &___START_PATTERN_2; }
	inline void set_START_PATTERN_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___START_PATTERN_2 = value;
		Il2CppCodeGenWriteBarrier((&___START_PATTERN_2), value);
	}

	inline static int32_t get_offset_of_STOP_PATTERN_3() { return static_cast<int32_t>(offsetof(Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields, ___STOP_PATTERN_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_STOP_PATTERN_3() const { return ___STOP_PATTERN_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_STOP_PATTERN_3() { return &___STOP_PATTERN_3; }
	inline void set_STOP_PATTERN_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___STOP_PATTERN_3 = value;
		Il2CppCodeGenWriteBarrier((&___STOP_PATTERN_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTOR_TC3C9DEF66D12EF6A5869D91A688D9997865D167E_H
#ifndef ERRORCORRECTION_T3D0F6DF238C51631A755DB5CCB1629E9613E28B7_H
#define ERRORCORRECTION_T3D0F6DF238C51631A755DB5CCB1629E9613E28B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.EC.ErrorCorrection
struct  ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7  : public RuntimeObject
{
public:
	// ZXing.PDF417.Internal.EC.ModulusGF ZXing.PDF417.Internal.EC.ErrorCorrection::field
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7, ___field_0)); }
	inline ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * get_field_0() const { return ___field_0; }
	inline ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCORRECTION_T3D0F6DF238C51631A755DB5CCB1629E9613E28B7_H
#ifndef MODULUSGF_T51A31A9598800B20BDBB473FB376AE23CAD8D2F2_H
#define MODULUSGF_T51A31A9598800B20BDBB473FB376AE23CAD8D2F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.EC.ModulusGF
struct  ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2  : public RuntimeObject
{
public:
	// System.Int32[] ZXing.PDF417.Internal.EC.ModulusGF::expTable
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___expTable_1;
	// System.Int32[] ZXing.PDF417.Internal.EC.ModulusGF::logTable
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___logTable_2;
	// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::<Zero>k__BackingField
	ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E * ___U3CZeroU3Ek__BackingField_3;
	// ZXing.PDF417.Internal.EC.ModulusPoly ZXing.PDF417.Internal.EC.ModulusGF::<One>k__BackingField
	ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E * ___U3COneU3Ek__BackingField_4;
	// System.Int32 ZXing.PDF417.Internal.EC.ModulusGF::modulus
	int32_t ___modulus_5;

public:
	inline static int32_t get_offset_of_expTable_1() { return static_cast<int32_t>(offsetof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2, ___expTable_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_expTable_1() const { return ___expTable_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_expTable_1() { return &___expTable_1; }
	inline void set_expTable_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___expTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___expTable_1), value);
	}

	inline static int32_t get_offset_of_logTable_2() { return static_cast<int32_t>(offsetof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2, ___logTable_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_logTable_2() const { return ___logTable_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_logTable_2() { return &___logTable_2; }
	inline void set_logTable_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___logTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___logTable_2), value);
	}

	inline static int32_t get_offset_of_U3CZeroU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2, ___U3CZeroU3Ek__BackingField_3)); }
	inline ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E * get_U3CZeroU3Ek__BackingField_3() const { return ___U3CZeroU3Ek__BackingField_3; }
	inline ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E ** get_address_of_U3CZeroU3Ek__BackingField_3() { return &___U3CZeroU3Ek__BackingField_3; }
	inline void set_U3CZeroU3Ek__BackingField_3(ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E * value)
	{
		___U3CZeroU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CZeroU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3COneU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2, ___U3COneU3Ek__BackingField_4)); }
	inline ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E * get_U3COneU3Ek__BackingField_4() const { return ___U3COneU3Ek__BackingField_4; }
	inline ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E ** get_address_of_U3COneU3Ek__BackingField_4() { return &___U3COneU3Ek__BackingField_4; }
	inline void set_U3COneU3Ek__BackingField_4(ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E * value)
	{
		___U3COneU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3COneU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_modulus_5() { return static_cast<int32_t>(offsetof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2, ___modulus_5)); }
	inline int32_t get_modulus_5() const { return ___modulus_5; }
	inline int32_t* get_address_of_modulus_5() { return &___modulus_5; }
	inline void set_modulus_5(int32_t value)
	{
		___modulus_5 = value;
	}
};

struct ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2_StaticFields
{
public:
	// ZXing.PDF417.Internal.EC.ModulusGF ZXing.PDF417.Internal.EC.ModulusGF::PDF417_GF
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * ___PDF417_GF_0;

public:
	inline static int32_t get_offset_of_PDF417_GF_0() { return static_cast<int32_t>(offsetof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2_StaticFields, ___PDF417_GF_0)); }
	inline ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * get_PDF417_GF_0() const { return ___PDF417_GF_0; }
	inline ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 ** get_address_of_PDF417_GF_0() { return &___PDF417_GF_0; }
	inline void set_PDF417_GF_0(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * value)
	{
		___PDF417_GF_0 = value;
		Il2CppCodeGenWriteBarrier((&___PDF417_GF_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULUSGF_T51A31A9598800B20BDBB473FB376AE23CAD8D2F2_H
#ifndef MODULUSPOLY_T440945E805EC44D0CEED65FABFFA405C8482B31E_H
#define MODULUSPOLY_T440945E805EC44D0CEED65FABFFA405C8482B31E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.EC.ModulusPoly
struct  ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E  : public RuntimeObject
{
public:
	// ZXing.PDF417.Internal.EC.ModulusGF ZXing.PDF417.Internal.EC.ModulusPoly::field
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * ___field_0;
	// System.Int32[] ZXing.PDF417.Internal.EC.ModulusPoly::coefficients
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___coefficients_1;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E, ___field_0)); }
	inline ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * get_field_0() const { return ___field_0; }
	inline ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2 * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}

	inline static int32_t get_offset_of_coefficients_1() { return static_cast<int32_t>(offsetof(ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E, ___coefficients_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_coefficients_1() const { return ___coefficients_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_coefficients_1() { return &___coefficients_1; }
	inline void set_coefficients_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___coefficients_1 = value;
		Il2CppCodeGenWriteBarrier((&___coefficients_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODULUSPOLY_T440945E805EC44D0CEED65FABFFA405C8482B31E_H
#ifndef PDF417CODEWORDDECODER_TD708BEB97EF160311AE2C6DA2140650FA5A9E665_H
#define PDF417CODEWORDDECODER_TD708BEB97EF160311AE2C6DA2140650FA5A9E665_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417CodewordDecoder
struct  PDF417CodewordDecoder_tD708BEB97EF160311AE2C6DA2140650FA5A9E665  : public RuntimeObject
{
public:

public:
};

struct PDF417CodewordDecoder_tD708BEB97EF160311AE2C6DA2140650FA5A9E665_StaticFields
{
public:
	// System.Single[][] ZXing.PDF417.Internal.PDF417CodewordDecoder::RATIOS_TABLE
	SingleU5BU5DU5BU5D_tBF3DDFFDDB461488F731F6F7ABE150AFCB620135* ___RATIOS_TABLE_0;

public:
	inline static int32_t get_offset_of_RATIOS_TABLE_0() { return static_cast<int32_t>(offsetof(PDF417CodewordDecoder_tD708BEB97EF160311AE2C6DA2140650FA5A9E665_StaticFields, ___RATIOS_TABLE_0)); }
	inline SingleU5BU5DU5BU5D_tBF3DDFFDDB461488F731F6F7ABE150AFCB620135* get_RATIOS_TABLE_0() const { return ___RATIOS_TABLE_0; }
	inline SingleU5BU5DU5BU5D_tBF3DDFFDDB461488F731F6F7ABE150AFCB620135** get_address_of_RATIOS_TABLE_0() { return &___RATIOS_TABLE_0; }
	inline void set_RATIOS_TABLE_0(SingleU5BU5DU5BU5D_tBF3DDFFDDB461488F731F6F7ABE150AFCB620135* value)
	{
		___RATIOS_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier((&___RATIOS_TABLE_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417CODEWORDDECODER_TD708BEB97EF160311AE2C6DA2140650FA5A9E665_H
#ifndef PDF417DETECTORRESULT_T1EA56AD672F48F88BF30365C6D1A053369E6B514_H
#define PDF417DETECTORRESULT_T1EA56AD672F48F88BF30365C6D1A053369E6B514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417DetectorResult
struct  PDF417DetectorResult_t1EA56AD672F48F88BF30365C6D1A053369E6B514  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.PDF417.Internal.PDF417DetectorResult::<Bits>k__BackingField
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___U3CBitsU3Ek__BackingField_0;
	// System.Collections.Generic.List`1<ZXing.ResultPoint[]> ZXing.PDF417.Internal.PDF417DetectorResult::<Points>k__BackingField
	List_1_t1A3FD96799990BD7BAB0D698FA36A285BEAA0B3E * ___U3CPointsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CBitsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PDF417DetectorResult_t1EA56AD672F48F88BF30365C6D1A053369E6B514, ___U3CBitsU3Ek__BackingField_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_U3CBitsU3Ek__BackingField_0() const { return ___U3CBitsU3Ek__BackingField_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_U3CBitsU3Ek__BackingField_0() { return &___U3CBitsU3Ek__BackingField_0; }
	inline void set_U3CBitsU3Ek__BackingField_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___U3CBitsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBitsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPointsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PDF417DetectorResult_t1EA56AD672F48F88BF30365C6D1A053369E6B514, ___U3CPointsU3Ek__BackingField_1)); }
	inline List_1_t1A3FD96799990BD7BAB0D698FA36A285BEAA0B3E * get_U3CPointsU3Ek__BackingField_1() const { return ___U3CPointsU3Ek__BackingField_1; }
	inline List_1_t1A3FD96799990BD7BAB0D698FA36A285BEAA0B3E ** get_address_of_U3CPointsU3Ek__BackingField_1() { return &___U3CPointsU3Ek__BackingField_1; }
	inline void set_U3CPointsU3Ek__BackingField_1(List_1_t1A3FD96799990BD7BAB0D698FA36A285BEAA0B3E * value)
	{
		___U3CPointsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPointsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417DETECTORRESULT_T1EA56AD672F48F88BF30365C6D1A053369E6B514_H
#ifndef PDF417HIGHLEVELENCODER_TB57B785770A4EA26D37AA70EFFB548704A7D5595_H
#define PDF417HIGHLEVELENCODER_TB57B785770A4EA26D37AA70EFFB548704A7D5595_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417HighLevelEncoder
struct  PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595  : public RuntimeObject
{
public:

public:
};

struct PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields
{
public:
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::TEXT_MIXED_RAW
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___TEXT_MIXED_RAW_0;
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::TEXT_PUNCTUATION_RAW
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___TEXT_PUNCTUATION_RAW_1;
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::MIXED
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___MIXED_2;
	// System.SByte[] ZXing.PDF417.Internal.PDF417HighLevelEncoder::PUNCTUATION
	SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* ___PUNCTUATION_3;
	// System.String ZXing.PDF417.Internal.PDF417HighLevelEncoder::DEFAULT_ENCODING_NAME
	String_t* ___DEFAULT_ENCODING_NAME_4;

public:
	inline static int32_t get_offset_of_TEXT_MIXED_RAW_0() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields, ___TEXT_MIXED_RAW_0)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_TEXT_MIXED_RAW_0() const { return ___TEXT_MIXED_RAW_0; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_TEXT_MIXED_RAW_0() { return &___TEXT_MIXED_RAW_0; }
	inline void set_TEXT_MIXED_RAW_0(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___TEXT_MIXED_RAW_0 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_MIXED_RAW_0), value);
	}

	inline static int32_t get_offset_of_TEXT_PUNCTUATION_RAW_1() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields, ___TEXT_PUNCTUATION_RAW_1)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_TEXT_PUNCTUATION_RAW_1() const { return ___TEXT_PUNCTUATION_RAW_1; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_TEXT_PUNCTUATION_RAW_1() { return &___TEXT_PUNCTUATION_RAW_1; }
	inline void set_TEXT_PUNCTUATION_RAW_1(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___TEXT_PUNCTUATION_RAW_1 = value;
		Il2CppCodeGenWriteBarrier((&___TEXT_PUNCTUATION_RAW_1), value);
	}

	inline static int32_t get_offset_of_MIXED_2() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields, ___MIXED_2)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_MIXED_2() const { return ___MIXED_2; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_MIXED_2() { return &___MIXED_2; }
	inline void set_MIXED_2(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___MIXED_2 = value;
		Il2CppCodeGenWriteBarrier((&___MIXED_2), value);
	}

	inline static int32_t get_offset_of_PUNCTUATION_3() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields, ___PUNCTUATION_3)); }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* get_PUNCTUATION_3() const { return ___PUNCTUATION_3; }
	inline SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889** get_address_of_PUNCTUATION_3() { return &___PUNCTUATION_3; }
	inline void set_PUNCTUATION_3(SByteU5BU5D_t623D1F33C61DEAC564E2B0560E00F1E1364F7889* value)
	{
		___PUNCTUATION_3 = value;
		Il2CppCodeGenWriteBarrier((&___PUNCTUATION_3), value);
	}

	inline static int32_t get_offset_of_DEFAULT_ENCODING_NAME_4() { return static_cast<int32_t>(offsetof(PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields, ___DEFAULT_ENCODING_NAME_4)); }
	inline String_t* get_DEFAULT_ENCODING_NAME_4() const { return ___DEFAULT_ENCODING_NAME_4; }
	inline String_t** get_address_of_DEFAULT_ENCODING_NAME_4() { return &___DEFAULT_ENCODING_NAME_4; }
	inline void set_DEFAULT_ENCODING_NAME_4(String_t* value)
	{
		___DEFAULT_ENCODING_NAME_4 = value;
		Il2CppCodeGenWriteBarrier((&___DEFAULT_ENCODING_NAME_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417HIGHLEVELENCODER_TB57B785770A4EA26D37AA70EFFB548704A7D5595_H
#ifndef PDF417SCANNINGDECODER_T62267070A08A1CD992B20CD982CF7F3E068C300C_H
#define PDF417SCANNINGDECODER_T62267070A08A1CD992B20CD982CF7F3E068C300C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.PDF417ScanningDecoder
struct  PDF417ScanningDecoder_t62267070A08A1CD992B20CD982CF7F3E068C300C  : public RuntimeObject
{
public:

public:
};

struct PDF417ScanningDecoder_t62267070A08A1CD992B20CD982CF7F3E068C300C_StaticFields
{
public:
	// ZXing.PDF417.Internal.EC.ErrorCorrection ZXing.PDF417.Internal.PDF417ScanningDecoder::errorCorrection
	ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7 * ___errorCorrection_0;

public:
	inline static int32_t get_offset_of_errorCorrection_0() { return static_cast<int32_t>(offsetof(PDF417ScanningDecoder_t62267070A08A1CD992B20CD982CF7F3E068C300C_StaticFields, ___errorCorrection_0)); }
	inline ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7 * get_errorCorrection_0() const { return ___errorCorrection_0; }
	inline ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7 ** get_address_of_errorCorrection_0() { return &___errorCorrection_0; }
	inline void set_errorCorrection_0(ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7 * value)
	{
		___errorCorrection_0 = value;
		Il2CppCodeGenWriteBarrier((&___errorCorrection_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417SCANNINGDECODER_T62267070A08A1CD992B20CD982CF7F3E068C300C_H
#ifndef PDF417COMMON_TE0FC7FFB8A17297646426570713B605036D70C32_H
#define PDF417COMMON_TE0FC7FFB8A17297646426570713B605036D70C32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.PDF417Common
struct  PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32  : public RuntimeObject
{
public:

public:
};

struct PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields
{
public:
	// System.Int32 ZXing.PDF417.PDF417Common::INVALID_CODEWORD
	int32_t ___INVALID_CODEWORD_0;
	// System.Int32 ZXing.PDF417.PDF417Common::NUMBER_OF_CODEWORDS
	int32_t ___NUMBER_OF_CODEWORDS_1;
	// System.Int32 ZXing.PDF417.PDF417Common::MAX_CODEWORDS_IN_BARCODE
	int32_t ___MAX_CODEWORDS_IN_BARCODE_2;
	// System.Int32 ZXing.PDF417.PDF417Common::MIN_ROWS_IN_BARCODE
	int32_t ___MIN_ROWS_IN_BARCODE_3;
	// System.Int32 ZXing.PDF417.PDF417Common::MAX_ROWS_IN_BARCODE
	int32_t ___MAX_ROWS_IN_BARCODE_4;
	// System.Int32 ZXing.PDF417.PDF417Common::MODULES_IN_CODEWORD
	int32_t ___MODULES_IN_CODEWORD_5;
	// System.Int32 ZXing.PDF417.PDF417Common::MODULES_IN_STOP_PATTERN
	int32_t ___MODULES_IN_STOP_PATTERN_6;
	// System.Int32 ZXing.PDF417.PDF417Common::BARS_IN_MODULE
	int32_t ___BARS_IN_MODULE_7;
	// System.Int32[] ZXing.PDF417.PDF417Common::EMPTY_INT_ARRAY
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___EMPTY_INT_ARRAY_8;
	// System.Int32[] ZXing.PDF417.PDF417Common::SYMBOL_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___SYMBOL_TABLE_9;
	// System.Int32[] ZXing.PDF417.PDF417Common::CODEWORD_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CODEWORD_TABLE_10;

public:
	inline static int32_t get_offset_of_INVALID_CODEWORD_0() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___INVALID_CODEWORD_0)); }
	inline int32_t get_INVALID_CODEWORD_0() const { return ___INVALID_CODEWORD_0; }
	inline int32_t* get_address_of_INVALID_CODEWORD_0() { return &___INVALID_CODEWORD_0; }
	inline void set_INVALID_CODEWORD_0(int32_t value)
	{
		___INVALID_CODEWORD_0 = value;
	}

	inline static int32_t get_offset_of_NUMBER_OF_CODEWORDS_1() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___NUMBER_OF_CODEWORDS_1)); }
	inline int32_t get_NUMBER_OF_CODEWORDS_1() const { return ___NUMBER_OF_CODEWORDS_1; }
	inline int32_t* get_address_of_NUMBER_OF_CODEWORDS_1() { return &___NUMBER_OF_CODEWORDS_1; }
	inline void set_NUMBER_OF_CODEWORDS_1(int32_t value)
	{
		___NUMBER_OF_CODEWORDS_1 = value;
	}

	inline static int32_t get_offset_of_MAX_CODEWORDS_IN_BARCODE_2() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___MAX_CODEWORDS_IN_BARCODE_2)); }
	inline int32_t get_MAX_CODEWORDS_IN_BARCODE_2() const { return ___MAX_CODEWORDS_IN_BARCODE_2; }
	inline int32_t* get_address_of_MAX_CODEWORDS_IN_BARCODE_2() { return &___MAX_CODEWORDS_IN_BARCODE_2; }
	inline void set_MAX_CODEWORDS_IN_BARCODE_2(int32_t value)
	{
		___MAX_CODEWORDS_IN_BARCODE_2 = value;
	}

	inline static int32_t get_offset_of_MIN_ROWS_IN_BARCODE_3() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___MIN_ROWS_IN_BARCODE_3)); }
	inline int32_t get_MIN_ROWS_IN_BARCODE_3() const { return ___MIN_ROWS_IN_BARCODE_3; }
	inline int32_t* get_address_of_MIN_ROWS_IN_BARCODE_3() { return &___MIN_ROWS_IN_BARCODE_3; }
	inline void set_MIN_ROWS_IN_BARCODE_3(int32_t value)
	{
		___MIN_ROWS_IN_BARCODE_3 = value;
	}

	inline static int32_t get_offset_of_MAX_ROWS_IN_BARCODE_4() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___MAX_ROWS_IN_BARCODE_4)); }
	inline int32_t get_MAX_ROWS_IN_BARCODE_4() const { return ___MAX_ROWS_IN_BARCODE_4; }
	inline int32_t* get_address_of_MAX_ROWS_IN_BARCODE_4() { return &___MAX_ROWS_IN_BARCODE_4; }
	inline void set_MAX_ROWS_IN_BARCODE_4(int32_t value)
	{
		___MAX_ROWS_IN_BARCODE_4 = value;
	}

	inline static int32_t get_offset_of_MODULES_IN_CODEWORD_5() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___MODULES_IN_CODEWORD_5)); }
	inline int32_t get_MODULES_IN_CODEWORD_5() const { return ___MODULES_IN_CODEWORD_5; }
	inline int32_t* get_address_of_MODULES_IN_CODEWORD_5() { return &___MODULES_IN_CODEWORD_5; }
	inline void set_MODULES_IN_CODEWORD_5(int32_t value)
	{
		___MODULES_IN_CODEWORD_5 = value;
	}

	inline static int32_t get_offset_of_MODULES_IN_STOP_PATTERN_6() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___MODULES_IN_STOP_PATTERN_6)); }
	inline int32_t get_MODULES_IN_STOP_PATTERN_6() const { return ___MODULES_IN_STOP_PATTERN_6; }
	inline int32_t* get_address_of_MODULES_IN_STOP_PATTERN_6() { return &___MODULES_IN_STOP_PATTERN_6; }
	inline void set_MODULES_IN_STOP_PATTERN_6(int32_t value)
	{
		___MODULES_IN_STOP_PATTERN_6 = value;
	}

	inline static int32_t get_offset_of_BARS_IN_MODULE_7() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___BARS_IN_MODULE_7)); }
	inline int32_t get_BARS_IN_MODULE_7() const { return ___BARS_IN_MODULE_7; }
	inline int32_t* get_address_of_BARS_IN_MODULE_7() { return &___BARS_IN_MODULE_7; }
	inline void set_BARS_IN_MODULE_7(int32_t value)
	{
		___BARS_IN_MODULE_7 = value;
	}

	inline static int32_t get_offset_of_EMPTY_INT_ARRAY_8() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___EMPTY_INT_ARRAY_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_EMPTY_INT_ARRAY_8() const { return ___EMPTY_INT_ARRAY_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_EMPTY_INT_ARRAY_8() { return &___EMPTY_INT_ARRAY_8; }
	inline void set_EMPTY_INT_ARRAY_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___EMPTY_INT_ARRAY_8 = value;
		Il2CppCodeGenWriteBarrier((&___EMPTY_INT_ARRAY_8), value);
	}

	inline static int32_t get_offset_of_SYMBOL_TABLE_9() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___SYMBOL_TABLE_9)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_SYMBOL_TABLE_9() const { return ___SYMBOL_TABLE_9; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_SYMBOL_TABLE_9() { return &___SYMBOL_TABLE_9; }
	inline void set_SYMBOL_TABLE_9(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___SYMBOL_TABLE_9 = value;
		Il2CppCodeGenWriteBarrier((&___SYMBOL_TABLE_9), value);
	}

	inline static int32_t get_offset_of_CODEWORD_TABLE_10() { return static_cast<int32_t>(offsetof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields, ___CODEWORD_TABLE_10)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CODEWORD_TABLE_10() const { return ___CODEWORD_TABLE_10; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CODEWORD_TABLE_10() { return &___CODEWORD_TABLE_10; }
	inline void set_CODEWORD_TABLE_10(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CODEWORD_TABLE_10 = value;
		Il2CppCodeGenWriteBarrier((&___CODEWORD_TABLE_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417COMMON_TE0FC7FFB8A17297646426570713B605036D70C32_H
#ifndef PDF417READER_T5652665E03131800768344065D8FA07DEB44F66B_H
#define PDF417READER_T5652665E03131800768344065D8FA07DEB44F66B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.PDF417Reader
struct  PDF417Reader_t5652665E03131800768344065D8FA07DEB44F66B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417READER_T5652665E03131800768344065D8FA07DEB44F66B_H
#ifndef PDF417RESULTMETADATA_T9608B7F614ECACA21BF737740E526A4E0FAD930F_H
#define PDF417RESULTMETADATA_T9608B7F614ECACA21BF737740E526A4E0FAD930F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.PDF417ResultMetadata
struct  PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F  : public RuntimeObject
{
public:
	// System.Int32 ZXing.PDF417.PDF417ResultMetadata::<SegmentIndex>k__BackingField
	int32_t ___U3CSegmentIndexU3Ek__BackingField_0;
	// System.String ZXing.PDF417.PDF417ResultMetadata::<FileId>k__BackingField
	String_t* ___U3CFileIdU3Ek__BackingField_1;
	// System.Int32[] ZXing.PDF417.PDF417ResultMetadata::<OptionalData>k__BackingField
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___U3COptionalDataU3Ek__BackingField_2;
	// System.Boolean ZXing.PDF417.PDF417ResultMetadata::<IsLastSegment>k__BackingField
	bool ___U3CIsLastSegmentU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSegmentIndexU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F, ___U3CSegmentIndexU3Ek__BackingField_0)); }
	inline int32_t get_U3CSegmentIndexU3Ek__BackingField_0() const { return ___U3CSegmentIndexU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CSegmentIndexU3Ek__BackingField_0() { return &___U3CSegmentIndexU3Ek__BackingField_0; }
	inline void set_U3CSegmentIndexU3Ek__BackingField_0(int32_t value)
	{
		___U3CSegmentIndexU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CFileIdU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F, ___U3CFileIdU3Ek__BackingField_1)); }
	inline String_t* get_U3CFileIdU3Ek__BackingField_1() const { return ___U3CFileIdU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CFileIdU3Ek__BackingField_1() { return &___U3CFileIdU3Ek__BackingField_1; }
	inline void set_U3CFileIdU3Ek__BackingField_1(String_t* value)
	{
		___U3CFileIdU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileIdU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COptionalDataU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F, ___U3COptionalDataU3Ek__BackingField_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_U3COptionalDataU3Ek__BackingField_2() const { return ___U3COptionalDataU3Ek__BackingField_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_U3COptionalDataU3Ek__BackingField_2() { return &___U3COptionalDataU3Ek__BackingField_2; }
	inline void set_U3COptionalDataU3Ek__BackingField_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___U3COptionalDataU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COptionalDataU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIsLastSegmentU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F, ___U3CIsLastSegmentU3Ek__BackingField_3)); }
	inline bool get_U3CIsLastSegmentU3Ek__BackingField_3() const { return ___U3CIsLastSegmentU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CIsLastSegmentU3Ek__BackingField_3() { return &___U3CIsLastSegmentU3Ek__BackingField_3; }
	inline void set_U3CIsLastSegmentU3Ek__BackingField_3(bool value)
	{
		___U3CIsLastSegmentU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PDF417RESULTMETADATA_T9608B7F614ECACA21BF737740E526A4E0FAD930F_H
#ifndef ALIGNMENTPATTERNFINDER_T0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B_H
#define ALIGNMENTPATTERNFINDER_T0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.AlignmentPatternFinder
struct  AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.AlignmentPatternFinder::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_0;
	// System.Collections.Generic.IList`1<ZXing.QrCode.Internal.AlignmentPattern> ZXing.QrCode.Internal.AlignmentPatternFinder::possibleCenters
	RuntimeObject* ___possibleCenters_1;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::startX
	int32_t ___startX_2;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::startY
	int32_t ___startY_3;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::width
	int32_t ___width_4;
	// System.Int32 ZXing.QrCode.Internal.AlignmentPatternFinder::height
	int32_t ___height_5;
	// System.Single ZXing.QrCode.Internal.AlignmentPatternFinder::moduleSize
	float ___moduleSize_6;
	// System.Int32[] ZXing.QrCode.Internal.AlignmentPatternFinder::crossCheckStateCount
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___crossCheckStateCount_7;
	// ZXing.ResultPointCallback ZXing.QrCode.Internal.AlignmentPatternFinder::resultPointCallback
	ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * ___resultPointCallback_8;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___image_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_possibleCenters_1() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___possibleCenters_1)); }
	inline RuntimeObject* get_possibleCenters_1() const { return ___possibleCenters_1; }
	inline RuntimeObject** get_address_of_possibleCenters_1() { return &___possibleCenters_1; }
	inline void set_possibleCenters_1(RuntimeObject* value)
	{
		___possibleCenters_1 = value;
		Il2CppCodeGenWriteBarrier((&___possibleCenters_1), value);
	}

	inline static int32_t get_offset_of_startX_2() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___startX_2)); }
	inline int32_t get_startX_2() const { return ___startX_2; }
	inline int32_t* get_address_of_startX_2() { return &___startX_2; }
	inline void set_startX_2(int32_t value)
	{
		___startX_2 = value;
	}

	inline static int32_t get_offset_of_startY_3() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___startY_3)); }
	inline int32_t get_startY_3() const { return ___startY_3; }
	inline int32_t* get_address_of_startY_3() { return &___startY_3; }
	inline void set_startY_3(int32_t value)
	{
		___startY_3 = value;
	}

	inline static int32_t get_offset_of_width_4() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___width_4)); }
	inline int32_t get_width_4() const { return ___width_4; }
	inline int32_t* get_address_of_width_4() { return &___width_4; }
	inline void set_width_4(int32_t value)
	{
		___width_4 = value;
	}

	inline static int32_t get_offset_of_height_5() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___height_5)); }
	inline int32_t get_height_5() const { return ___height_5; }
	inline int32_t* get_address_of_height_5() { return &___height_5; }
	inline void set_height_5(int32_t value)
	{
		___height_5 = value;
	}

	inline static int32_t get_offset_of_moduleSize_6() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___moduleSize_6)); }
	inline float get_moduleSize_6() const { return ___moduleSize_6; }
	inline float* get_address_of_moduleSize_6() { return &___moduleSize_6; }
	inline void set_moduleSize_6(float value)
	{
		___moduleSize_6 = value;
	}

	inline static int32_t get_offset_of_crossCheckStateCount_7() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___crossCheckStateCount_7)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_crossCheckStateCount_7() const { return ___crossCheckStateCount_7; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_crossCheckStateCount_7() { return &___crossCheckStateCount_7; }
	inline void set_crossCheckStateCount_7(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___crossCheckStateCount_7 = value;
		Il2CppCodeGenWriteBarrier((&___crossCheckStateCount_7), value);
	}

	inline static int32_t get_offset_of_resultPointCallback_8() { return static_cast<int32_t>(offsetof(AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B, ___resultPointCallback_8)); }
	inline ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * get_resultPointCallback_8() const { return ___resultPointCallback_8; }
	inline ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 ** get_address_of_resultPointCallback_8() { return &___resultPointCallback_8; }
	inline void set_resultPointCallback_8(ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * value)
	{
		___resultPointCallback_8 = value;
		Il2CppCodeGenWriteBarrier((&___resultPointCallback_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENTPATTERNFINDER_T0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B_H
#ifndef BITMATRIXPARSER_TC677F7EC7B28F2505C839D0C90549BAF02167636_H
#define BITMATRIXPARSER_TC677F7EC7B28F2505C839D0C90549BAF02167636_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.BitMatrixParser
struct  BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.BitMatrixParser::bitMatrix
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___bitMatrix_0;
	// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.BitMatrixParser::parsedVersion
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 * ___parsedVersion_1;
	// ZXing.QrCode.Internal.FormatInformation ZXing.QrCode.Internal.BitMatrixParser::parsedFormatInfo
	FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26 * ___parsedFormatInfo_2;
	// System.Boolean ZXing.QrCode.Internal.BitMatrixParser::mirrored
	bool ___mirrored_3;

public:
	inline static int32_t get_offset_of_bitMatrix_0() { return static_cast<int32_t>(offsetof(BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636, ___bitMatrix_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_bitMatrix_0() const { return ___bitMatrix_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_bitMatrix_0() { return &___bitMatrix_0; }
	inline void set_bitMatrix_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___bitMatrix_0 = value;
		Il2CppCodeGenWriteBarrier((&___bitMatrix_0), value);
	}

	inline static int32_t get_offset_of_parsedVersion_1() { return static_cast<int32_t>(offsetof(BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636, ___parsedVersion_1)); }
	inline Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 * get_parsedVersion_1() const { return ___parsedVersion_1; }
	inline Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 ** get_address_of_parsedVersion_1() { return &___parsedVersion_1; }
	inline void set_parsedVersion_1(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 * value)
	{
		___parsedVersion_1 = value;
		Il2CppCodeGenWriteBarrier((&___parsedVersion_1), value);
	}

	inline static int32_t get_offset_of_parsedFormatInfo_2() { return static_cast<int32_t>(offsetof(BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636, ___parsedFormatInfo_2)); }
	inline FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26 * get_parsedFormatInfo_2() const { return ___parsedFormatInfo_2; }
	inline FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26 ** get_address_of_parsedFormatInfo_2() { return &___parsedFormatInfo_2; }
	inline void set_parsedFormatInfo_2(FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26 * value)
	{
		___parsedFormatInfo_2 = value;
		Il2CppCodeGenWriteBarrier((&___parsedFormatInfo_2), value);
	}

	inline static int32_t get_offset_of_mirrored_3() { return static_cast<int32_t>(offsetof(BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636, ___mirrored_3)); }
	inline bool get_mirrored_3() const { return ___mirrored_3; }
	inline bool* get_address_of_mirrored_3() { return &___mirrored_3; }
	inline void set_mirrored_3(bool value)
	{
		___mirrored_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMATRIXPARSER_TC677F7EC7B28F2505C839D0C90549BAF02167636_H
#ifndef BLOCKPAIR_T883A80F1975479C09CCF8E5B17C460F85638B627_H
#define BLOCKPAIR_T883A80F1975479C09CCF8E5B17C460F85638B627_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.BlockPair
struct  BlockPair_t883A80F1975479C09CCF8E5B17C460F85638B627  : public RuntimeObject
{
public:
	// System.Byte[] ZXing.QrCode.Internal.BlockPair::dataBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___dataBytes_0;
	// System.Byte[] ZXing.QrCode.Internal.BlockPair::errorCorrectionBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___errorCorrectionBytes_1;

public:
	inline static int32_t get_offset_of_dataBytes_0() { return static_cast<int32_t>(offsetof(BlockPair_t883A80F1975479C09CCF8E5B17C460F85638B627, ___dataBytes_0)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_dataBytes_0() const { return ___dataBytes_0; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_dataBytes_0() { return &___dataBytes_0; }
	inline void set_dataBytes_0(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___dataBytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___dataBytes_0), value);
	}

	inline static int32_t get_offset_of_errorCorrectionBytes_1() { return static_cast<int32_t>(offsetof(BlockPair_t883A80F1975479C09CCF8E5B17C460F85638B627, ___errorCorrectionBytes_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_errorCorrectionBytes_1() const { return ___errorCorrectionBytes_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_errorCorrectionBytes_1() { return &___errorCorrectionBytes_1; }
	inline void set_errorCorrectionBytes_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___errorCorrectionBytes_1 = value;
		Il2CppCodeGenWriteBarrier((&___errorCorrectionBytes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKPAIR_T883A80F1975479C09CCF8E5B17C460F85638B627_H
#ifndef BYTEMATRIX_TF9DC0A40ACF26CA79F43048903B06E555B91B0F8_H
#define BYTEMATRIX_TF9DC0A40ACF26CA79F43048903B06E555B91B0F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.ByteMatrix
struct  ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8  : public RuntimeObject
{
public:
	// System.Byte[][] ZXing.QrCode.Internal.ByteMatrix::bytes
	ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* ___bytes_0;
	// System.Int32 ZXing.QrCode.Internal.ByteMatrix::width
	int32_t ___width_1;
	// System.Int32 ZXing.QrCode.Internal.ByteMatrix::height
	int32_t ___height_2;

public:
	inline static int32_t get_offset_of_bytes_0() { return static_cast<int32_t>(offsetof(ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8, ___bytes_0)); }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* get_bytes_0() const { return ___bytes_0; }
	inline ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7** get_address_of_bytes_0() { return &___bytes_0; }
	inline void set_bytes_0(ByteU5BU5DU5BU5D_tD1CB918775FFB351821F10AC338FECDDE22DEEC7* value)
	{
		___bytes_0 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_0), value);
	}

	inline static int32_t get_offset_of_width_1() { return static_cast<int32_t>(offsetof(ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8, ___width_1)); }
	inline int32_t get_width_1() const { return ___width_1; }
	inline int32_t* get_address_of_width_1() { return &___width_1; }
	inline void set_width_1(int32_t value)
	{
		___width_1 = value;
	}

	inline static int32_t get_offset_of_height_2() { return static_cast<int32_t>(offsetof(ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8, ___height_2)); }
	inline int32_t get_height_2() const { return ___height_2; }
	inline int32_t* get_address_of_height_2() { return &___height_2; }
	inline void set_height_2(int32_t value)
	{
		___height_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BYTEMATRIX_TF9DC0A40ACF26CA79F43048903B06E555B91B0F8_H
#ifndef DATABLOCK_TB039200C1595B1F47D5D85D439D6BB26C9528166_H
#define DATABLOCK_TB039200C1595B1F47D5D85D439D6BB26C9528166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DataBlock
struct  DataBlock_tB039200C1595B1F47D5D85D439D6BB26C9528166  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.DataBlock::numDataCodewords
	int32_t ___numDataCodewords_0;
	// System.Byte[] ZXing.QrCode.Internal.DataBlock::codewords
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___codewords_1;

public:
	inline static int32_t get_offset_of_numDataCodewords_0() { return static_cast<int32_t>(offsetof(DataBlock_tB039200C1595B1F47D5D85D439D6BB26C9528166, ___numDataCodewords_0)); }
	inline int32_t get_numDataCodewords_0() const { return ___numDataCodewords_0; }
	inline int32_t* get_address_of_numDataCodewords_0() { return &___numDataCodewords_0; }
	inline void set_numDataCodewords_0(int32_t value)
	{
		___numDataCodewords_0 = value;
	}

	inline static int32_t get_offset_of_codewords_1() { return static_cast<int32_t>(offsetof(DataBlock_tB039200C1595B1F47D5D85D439D6BB26C9528166, ___codewords_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_codewords_1() const { return ___codewords_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_codewords_1() { return &___codewords_1; }
	inline void set_codewords_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___codewords_1 = value;
		Il2CppCodeGenWriteBarrier((&___codewords_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATABLOCK_TB039200C1595B1F47D5D85D439D6BB26C9528166_H
#ifndef DATAMASK_TFDFB5566B35D29D6309AEE024C03BD19942D18EF_H
#define DATAMASK_TFDFB5566B35D29D6309AEE024C03BD19942D18EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DataMask
struct  DataMask_tFDFB5566B35D29D6309AEE024C03BD19942D18EF  : public RuntimeObject
{
public:

public:
};

struct DataMask_tFDFB5566B35D29D6309AEE024C03BD19942D18EF_StaticFields
{
public:
	// System.Func`3<System.Int32,System.Int32,System.Boolean>[] ZXing.QrCode.Internal.DataMask::DATA_MASKS
	Func_3U5BU5D_tC558F376F328B1873892E6FF2BC3091C9C568553* ___DATA_MASKS_0;

public:
	inline static int32_t get_offset_of_DATA_MASKS_0() { return static_cast<int32_t>(offsetof(DataMask_tFDFB5566B35D29D6309AEE024C03BD19942D18EF_StaticFields, ___DATA_MASKS_0)); }
	inline Func_3U5BU5D_tC558F376F328B1873892E6FF2BC3091C9C568553* get_DATA_MASKS_0() const { return ___DATA_MASKS_0; }
	inline Func_3U5BU5D_tC558F376F328B1873892E6FF2BC3091C9C568553** get_address_of_DATA_MASKS_0() { return &___DATA_MASKS_0; }
	inline void set_DATA_MASKS_0(Func_3U5BU5D_tC558F376F328B1873892E6FF2BC3091C9C568553* value)
	{
		___DATA_MASKS_0 = value;
		Il2CppCodeGenWriteBarrier((&___DATA_MASKS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAMASK_TFDFB5566B35D29D6309AEE024C03BD19942D18EF_H
#ifndef U3CU3EC_TE866C1CD845B436CA1AA94ED90C86D1BC441E783_H
#define U3CU3EC_TE866C1CD845B436CA1AA94ED90C86D1BC441E783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DataMask/<>c
struct  U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783_StaticFields
{
public:
	// ZXing.QrCode.Internal.DataMask/<>c ZXing.QrCode.Internal.DataMask/<>c::<>9
	U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783 * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE866C1CD845B436CA1AA94ED90C86D1BC441E783_H
#ifndef DECODEDBITSTREAMPARSER_T3CD6F4F82581779102F82AE6B53DB3C70103E1AA_H
#define DECODEDBITSTREAMPARSER_T3CD6F4F82581779102F82AE6B53DB3C70103E1AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.DecodedBitStreamParser
struct  DecodedBitStreamParser_t3CD6F4F82581779102F82AE6B53DB3C70103E1AA  : public RuntimeObject
{
public:

public:
};

struct DecodedBitStreamParser_t3CD6F4F82581779102F82AE6B53DB3C70103E1AA_StaticFields
{
public:
	// System.Char[] ZXing.QrCode.Internal.DecodedBitStreamParser::ALPHANUMERIC_CHARS
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ALPHANUMERIC_CHARS_0;

public:
	inline static int32_t get_offset_of_ALPHANUMERIC_CHARS_0() { return static_cast<int32_t>(offsetof(DecodedBitStreamParser_t3CD6F4F82581779102F82AE6B53DB3C70103E1AA_StaticFields, ___ALPHANUMERIC_CHARS_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ALPHANUMERIC_CHARS_0() const { return ___ALPHANUMERIC_CHARS_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ALPHANUMERIC_CHARS_0() { return &___ALPHANUMERIC_CHARS_0; }
	inline void set_ALPHANUMERIC_CHARS_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ALPHANUMERIC_CHARS_0 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHANUMERIC_CHARS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEDBITSTREAMPARSER_T3CD6F4F82581779102F82AE6B53DB3C70103E1AA_H
#ifndef DECODER_T726229B65302EC26E07C22FCFD9B35767A06408D_H
#define DECODER_T726229B65302EC26E07C22FCFD9B35767A06408D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Decoder
struct  Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D  : public RuntimeObject
{
public:
	// ZXing.Common.ReedSolomon.ReedSolomonDecoder ZXing.QrCode.Internal.Decoder::rsDecoder
	ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * ___rsDecoder_0;

public:
	inline static int32_t get_offset_of_rsDecoder_0() { return static_cast<int32_t>(offsetof(Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D, ___rsDecoder_0)); }
	inline ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * get_rsDecoder_0() const { return ___rsDecoder_0; }
	inline ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 ** get_address_of_rsDecoder_0() { return &___rsDecoder_0; }
	inline void set_rsDecoder_0(ReedSolomonDecoder_t254937BB15BEE22F18671B0FA89DDBE9060A4427 * value)
	{
		___rsDecoder_0 = value;
		Il2CppCodeGenWriteBarrier((&___rsDecoder_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODER_T726229B65302EC26E07C22FCFD9B35767A06408D_H
#ifndef DETECTOR_TBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E_H
#define DETECTOR_TBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Detector
struct  Detector_tBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.Detector::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_0;
	// ZXing.ResultPointCallback ZXing.QrCode.Internal.Detector::resultPointCallback
	ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * ___resultPointCallback_1;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(Detector_tBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E, ___image_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_resultPointCallback_1() { return static_cast<int32_t>(offsetof(Detector_tBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E, ___resultPointCallback_1)); }
	inline ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * get_resultPointCallback_1() const { return ___resultPointCallback_1; }
	inline ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 ** get_address_of_resultPointCallback_1() { return &___resultPointCallback_1; }
	inline void set_resultPointCallback_1(ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * value)
	{
		___resultPointCallback_1 = value;
		Il2CppCodeGenWriteBarrier((&___resultPointCallback_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTOR_TBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E_H
#ifndef ENCODER_TE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_H
#define ENCODER_TE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Encoder
struct  Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5  : public RuntimeObject
{
public:

public:
};

struct Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_StaticFields
{
public:
	// System.Int32[] ZXing.QrCode.Internal.Encoder::ALPHANUMERIC_TABLE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___ALPHANUMERIC_TABLE_0;
	// System.String ZXing.QrCode.Internal.Encoder::DEFAULT_BYTE_MODE_ENCODING
	String_t* ___DEFAULT_BYTE_MODE_ENCODING_1;

public:
	inline static int32_t get_offset_of_ALPHANUMERIC_TABLE_0() { return static_cast<int32_t>(offsetof(Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_StaticFields, ___ALPHANUMERIC_TABLE_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_ALPHANUMERIC_TABLE_0() const { return ___ALPHANUMERIC_TABLE_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_ALPHANUMERIC_TABLE_0() { return &___ALPHANUMERIC_TABLE_0; }
	inline void set_ALPHANUMERIC_TABLE_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___ALPHANUMERIC_TABLE_0 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHANUMERIC_TABLE_0), value);
	}

	inline static int32_t get_offset_of_DEFAULT_BYTE_MODE_ENCODING_1() { return static_cast<int32_t>(offsetof(Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_StaticFields, ___DEFAULT_BYTE_MODE_ENCODING_1)); }
	inline String_t* get_DEFAULT_BYTE_MODE_ENCODING_1() const { return ___DEFAULT_BYTE_MODE_ENCODING_1; }
	inline String_t** get_address_of_DEFAULT_BYTE_MODE_ENCODING_1() { return &___DEFAULT_BYTE_MODE_ENCODING_1; }
	inline void set_DEFAULT_BYTE_MODE_ENCODING_1(String_t* value)
	{
		___DEFAULT_BYTE_MODE_ENCODING_1 = value;
		Il2CppCodeGenWriteBarrier((&___DEFAULT_BYTE_MODE_ENCODING_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODER_TE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_H
#ifndef ERRORCORRECTIONLEVEL_TAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_H
#define ERRORCORRECTIONLEVEL_TAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.ErrorCorrectionLevel
struct  ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::bits
	int32_t ___bits_5;
	// System.Int32 ZXing.QrCode.Internal.ErrorCorrectionLevel::ordinal_Renamed_Field
	int32_t ___ordinal_Renamed_Field_6;
	// System.String ZXing.QrCode.Internal.ErrorCorrectionLevel::name
	String_t* ___name_7;

public:
	inline static int32_t get_offset_of_bits_5() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D, ___bits_5)); }
	inline int32_t get_bits_5() const { return ___bits_5; }
	inline int32_t* get_address_of_bits_5() { return &___bits_5; }
	inline void set_bits_5(int32_t value)
	{
		___bits_5 = value;
	}

	inline static int32_t get_offset_of_ordinal_Renamed_Field_6() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D, ___ordinal_Renamed_Field_6)); }
	inline int32_t get_ordinal_Renamed_Field_6() const { return ___ordinal_Renamed_Field_6; }
	inline int32_t* get_address_of_ordinal_Renamed_Field_6() { return &___ordinal_Renamed_Field_6; }
	inline void set_ordinal_Renamed_Field_6(int32_t value)
	{
		___ordinal_Renamed_Field_6 = value;
	}

	inline static int32_t get_offset_of_name_7() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D, ___name_7)); }
	inline String_t* get_name_7() const { return ___name_7; }
	inline String_t** get_address_of_name_7() { return &___name_7; }
	inline void set_name_7(String_t* value)
	{
		___name_7 = value;
		Il2CppCodeGenWriteBarrier((&___name_7), value);
	}
};

struct ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields
{
public:
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::L
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * ___L_0;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::M
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * ___M_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::Q
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * ___Q_2;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.ErrorCorrectionLevel::H
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * ___H_3;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel[] ZXing.QrCode.Internal.ErrorCorrectionLevel::FOR_BITS
	ErrorCorrectionLevelU5BU5D_tFBFA3B097D501D2AF5B14C2DBBBDF2B3C6E900CC* ___FOR_BITS_4;

public:
	inline static int32_t get_offset_of_L_0() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields, ___L_0)); }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * get_L_0() const { return ___L_0; }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D ** get_address_of_L_0() { return &___L_0; }
	inline void set_L_0(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * value)
	{
		___L_0 = value;
		Il2CppCodeGenWriteBarrier((&___L_0), value);
	}

	inline static int32_t get_offset_of_M_1() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields, ___M_1)); }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * get_M_1() const { return ___M_1; }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D ** get_address_of_M_1() { return &___M_1; }
	inline void set_M_1(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * value)
	{
		___M_1 = value;
		Il2CppCodeGenWriteBarrier((&___M_1), value);
	}

	inline static int32_t get_offset_of_Q_2() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields, ___Q_2)); }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * get_Q_2() const { return ___Q_2; }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D ** get_address_of_Q_2() { return &___Q_2; }
	inline void set_Q_2(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * value)
	{
		___Q_2 = value;
		Il2CppCodeGenWriteBarrier((&___Q_2), value);
	}

	inline static int32_t get_offset_of_H_3() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields, ___H_3)); }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * get_H_3() const { return ___H_3; }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D ** get_address_of_H_3() { return &___H_3; }
	inline void set_H_3(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * value)
	{
		___H_3 = value;
		Il2CppCodeGenWriteBarrier((&___H_3), value);
	}

	inline static int32_t get_offset_of_FOR_BITS_4() { return static_cast<int32_t>(offsetof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields, ___FOR_BITS_4)); }
	inline ErrorCorrectionLevelU5BU5D_tFBFA3B097D501D2AF5B14C2DBBBDF2B3C6E900CC* get_FOR_BITS_4() const { return ___FOR_BITS_4; }
	inline ErrorCorrectionLevelU5BU5D_tFBFA3B097D501D2AF5B14C2DBBBDF2B3C6E900CC** get_address_of_FOR_BITS_4() { return &___FOR_BITS_4; }
	inline void set_FOR_BITS_4(ErrorCorrectionLevelU5BU5D_tFBFA3B097D501D2AF5B14C2DBBBDF2B3C6E900CC* value)
	{
		___FOR_BITS_4 = value;
		Il2CppCodeGenWriteBarrier((&___FOR_BITS_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCORRECTIONLEVEL_TAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_H
#ifndef FINDERPATTERNFINDER_TA313F18B8DC0DF2C252836117C550E6132BB1FF2_H
#define FINDERPATTERNFINDER_TA313F18B8DC0DF2C252836117C550E6132BB1FF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternFinder
struct  FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2  : public RuntimeObject
{
public:
	// ZXing.Common.BitMatrix ZXing.QrCode.Internal.FinderPatternFinder::image
	BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * ___image_0;
	// System.Collections.Generic.List`1<ZXing.QrCode.Internal.FinderPattern> ZXing.QrCode.Internal.FinderPatternFinder::possibleCenters
	List_1_t7493FF292FD1F3FD85ED1BEF8DD9B2F66FA54ABB * ___possibleCenters_1;
	// System.Boolean ZXing.QrCode.Internal.FinderPatternFinder::hasSkipped
	bool ___hasSkipped_2;
	// System.Int32[] ZXing.QrCode.Internal.FinderPatternFinder::crossCheckStateCount
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___crossCheckStateCount_3;
	// ZXing.ResultPointCallback ZXing.QrCode.Internal.FinderPatternFinder::resultPointCallback
	ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * ___resultPointCallback_4;

public:
	inline static int32_t get_offset_of_image_0() { return static_cast<int32_t>(offsetof(FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2, ___image_0)); }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * get_image_0() const { return ___image_0; }
	inline BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 ** get_address_of_image_0() { return &___image_0; }
	inline void set_image_0(BitMatrix_t35850E7E834FAECFF48E762116DAFFC85822CDE2 * value)
	{
		___image_0 = value;
		Il2CppCodeGenWriteBarrier((&___image_0), value);
	}

	inline static int32_t get_offset_of_possibleCenters_1() { return static_cast<int32_t>(offsetof(FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2, ___possibleCenters_1)); }
	inline List_1_t7493FF292FD1F3FD85ED1BEF8DD9B2F66FA54ABB * get_possibleCenters_1() const { return ___possibleCenters_1; }
	inline List_1_t7493FF292FD1F3FD85ED1BEF8DD9B2F66FA54ABB ** get_address_of_possibleCenters_1() { return &___possibleCenters_1; }
	inline void set_possibleCenters_1(List_1_t7493FF292FD1F3FD85ED1BEF8DD9B2F66FA54ABB * value)
	{
		___possibleCenters_1 = value;
		Il2CppCodeGenWriteBarrier((&___possibleCenters_1), value);
	}

	inline static int32_t get_offset_of_hasSkipped_2() { return static_cast<int32_t>(offsetof(FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2, ___hasSkipped_2)); }
	inline bool get_hasSkipped_2() const { return ___hasSkipped_2; }
	inline bool* get_address_of_hasSkipped_2() { return &___hasSkipped_2; }
	inline void set_hasSkipped_2(bool value)
	{
		___hasSkipped_2 = value;
	}

	inline static int32_t get_offset_of_crossCheckStateCount_3() { return static_cast<int32_t>(offsetof(FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2, ___crossCheckStateCount_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_crossCheckStateCount_3() const { return ___crossCheckStateCount_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_crossCheckStateCount_3() { return &___crossCheckStateCount_3; }
	inline void set_crossCheckStateCount_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___crossCheckStateCount_3 = value;
		Il2CppCodeGenWriteBarrier((&___crossCheckStateCount_3), value);
	}

	inline static int32_t get_offset_of_resultPointCallback_4() { return static_cast<int32_t>(offsetof(FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2, ___resultPointCallback_4)); }
	inline ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * get_resultPointCallback_4() const { return ___resultPointCallback_4; }
	inline ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 ** get_address_of_resultPointCallback_4() { return &___resultPointCallback_4; }
	inline void set_resultPointCallback_4(ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3 * value)
	{
		___resultPointCallback_4 = value;
		Il2CppCodeGenWriteBarrier((&___resultPointCallback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDERPATTERNFINDER_TA313F18B8DC0DF2C252836117C550E6132BB1FF2_H
#ifndef CENTERCOMPARATOR_T15EA58584F0F461CD3CEAC3568F23992A1612256_H
#define CENTERCOMPARATOR_T15EA58584F0F461CD3CEAC3568F23992A1612256_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator
struct  CenterComparator_t15EA58584F0F461CD3CEAC3568F23992A1612256  : public RuntimeObject
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPatternFinder/CenterComparator::average
	float ___average_0;

public:
	inline static int32_t get_offset_of_average_0() { return static_cast<int32_t>(offsetof(CenterComparator_t15EA58584F0F461CD3CEAC3568F23992A1612256, ___average_0)); }
	inline float get_average_0() const { return ___average_0; }
	inline float* get_address_of_average_0() { return &___average_0; }
	inline void set_average_0(float value)
	{
		___average_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CENTERCOMPARATOR_T15EA58584F0F461CD3CEAC3568F23992A1612256_H
#ifndef FURTHESTFROMAVERAGECOMPARATOR_TED7292884E68C755936E18FCBD940CECDD3EBD8B_H
#define FURTHESTFROMAVERAGECOMPARATOR_TED7292884E68C755936E18FCBD940CECDD3EBD8B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator
struct  FurthestFromAverageComparator_tED7292884E68C755936E18FCBD940CECDD3EBD8B  : public RuntimeObject
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPatternFinder/FurthestFromAverageComparator::average
	float ___average_0;

public:
	inline static int32_t get_offset_of_average_0() { return static_cast<int32_t>(offsetof(FurthestFromAverageComparator_tED7292884E68C755936E18FCBD940CECDD3EBD8B, ___average_0)); }
	inline float get_average_0() const { return ___average_0; }
	inline float* get_address_of_average_0() { return &___average_0; }
	inline void set_average_0(float value)
	{
		___average_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FURTHESTFROMAVERAGECOMPARATOR_TED7292884E68C755936E18FCBD940CECDD3EBD8B_H
#ifndef FINDERPATTERNINFO_TF90C418EFA55463C411DC3D50EFE0FD64F4CD945_H
#define FINDERPATTERNINFO_TF90C418EFA55463C411DC3D50EFE0FD64F4CD945_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPatternInfo
struct  FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945  : public RuntimeObject
{
public:
	// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::bottomLeft
	FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * ___bottomLeft_0;
	// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::topLeft
	FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * ___topLeft_1;
	// ZXing.QrCode.Internal.FinderPattern ZXing.QrCode.Internal.FinderPatternInfo::topRight
	FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * ___topRight_2;

public:
	inline static int32_t get_offset_of_bottomLeft_0() { return static_cast<int32_t>(offsetof(FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945, ___bottomLeft_0)); }
	inline FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * get_bottomLeft_0() const { return ___bottomLeft_0; }
	inline FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE ** get_address_of_bottomLeft_0() { return &___bottomLeft_0; }
	inline void set_bottomLeft_0(FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * value)
	{
		___bottomLeft_0 = value;
		Il2CppCodeGenWriteBarrier((&___bottomLeft_0), value);
	}

	inline static int32_t get_offset_of_topLeft_1() { return static_cast<int32_t>(offsetof(FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945, ___topLeft_1)); }
	inline FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * get_topLeft_1() const { return ___topLeft_1; }
	inline FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE ** get_address_of_topLeft_1() { return &___topLeft_1; }
	inline void set_topLeft_1(FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * value)
	{
		___topLeft_1 = value;
		Il2CppCodeGenWriteBarrier((&___topLeft_1), value);
	}

	inline static int32_t get_offset_of_topRight_2() { return static_cast<int32_t>(offsetof(FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945, ___topRight_2)); }
	inline FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * get_topRight_2() const { return ___topRight_2; }
	inline FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE ** get_address_of_topRight_2() { return &___topRight_2; }
	inline void set_topRight_2(FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE * value)
	{
		___topRight_2 = value;
		Il2CppCodeGenWriteBarrier((&___topRight_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDERPATTERNINFO_TF90C418EFA55463C411DC3D50EFE0FD64F4CD945_H
#ifndef FORMATINFORMATION_TECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_H
#define FORMATINFORMATION_TECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FormatInformation
struct  FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26  : public RuntimeObject
{
public:
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.FormatInformation::errorCorrectionLevel
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * ___errorCorrectionLevel_2;
	// System.Byte ZXing.QrCode.Internal.FormatInformation::dataMask
	uint8_t ___dataMask_3;

public:
	inline static int32_t get_offset_of_errorCorrectionLevel_2() { return static_cast<int32_t>(offsetof(FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26, ___errorCorrectionLevel_2)); }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * get_errorCorrectionLevel_2() const { return ___errorCorrectionLevel_2; }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D ** get_address_of_errorCorrectionLevel_2() { return &___errorCorrectionLevel_2; }
	inline void set_errorCorrectionLevel_2(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * value)
	{
		___errorCorrectionLevel_2 = value;
		Il2CppCodeGenWriteBarrier((&___errorCorrectionLevel_2), value);
	}

	inline static int32_t get_offset_of_dataMask_3() { return static_cast<int32_t>(offsetof(FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26, ___dataMask_3)); }
	inline uint8_t get_dataMask_3() const { return ___dataMask_3; }
	inline uint8_t* get_address_of_dataMask_3() { return &___dataMask_3; }
	inline void set_dataMask_3(uint8_t value)
	{
		___dataMask_3 = value;
	}
};

struct FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_StaticFields
{
public:
	// System.Int32[][] ZXing.QrCode.Internal.FormatInformation::FORMAT_INFO_DECODE_LOOKUP
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___FORMAT_INFO_DECODE_LOOKUP_0;
	// System.Int32[] ZXing.QrCode.Internal.FormatInformation::BITS_SET_IN_HALF_BYTE
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___BITS_SET_IN_HALF_BYTE_1;

public:
	inline static int32_t get_offset_of_FORMAT_INFO_DECODE_LOOKUP_0() { return static_cast<int32_t>(offsetof(FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_StaticFields, ___FORMAT_INFO_DECODE_LOOKUP_0)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_FORMAT_INFO_DECODE_LOOKUP_0() const { return ___FORMAT_INFO_DECODE_LOOKUP_0; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_FORMAT_INFO_DECODE_LOOKUP_0() { return &___FORMAT_INFO_DECODE_LOOKUP_0; }
	inline void set_FORMAT_INFO_DECODE_LOOKUP_0(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___FORMAT_INFO_DECODE_LOOKUP_0 = value;
		Il2CppCodeGenWriteBarrier((&___FORMAT_INFO_DECODE_LOOKUP_0), value);
	}

	inline static int32_t get_offset_of_BITS_SET_IN_HALF_BYTE_1() { return static_cast<int32_t>(offsetof(FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_StaticFields, ___BITS_SET_IN_HALF_BYTE_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_BITS_SET_IN_HALF_BYTE_1() const { return ___BITS_SET_IN_HALF_BYTE_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_BITS_SET_IN_HALF_BYTE_1() { return &___BITS_SET_IN_HALF_BYTE_1; }
	inline void set_BITS_SET_IN_HALF_BYTE_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___BITS_SET_IN_HALF_BYTE_1 = value;
		Il2CppCodeGenWriteBarrier((&___BITS_SET_IN_HALF_BYTE_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATINFORMATION_TECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_H
#ifndef MASKUTIL_TCD316E65E1A74F24EBE3B0E1A9E165F6829D9409_H
#define MASKUTIL_TCD316E65E1A74F24EBE3B0E1A9E165F6829D9409_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.MaskUtil
struct  MaskUtil_tCD316E65E1A74F24EBE3B0E1A9E165F6829D9409  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MASKUTIL_TCD316E65E1A74F24EBE3B0E1A9E165F6829D9409_H
#ifndef MATRIXUTIL_TE13307CA93A76F507F8CBE25D71D0B0B959F316D_H
#define MATRIXUTIL_TE13307CA93A76F507F8CBE25D71D0B0B959F316D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.MatrixUtil
struct  MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D  : public RuntimeObject
{
public:

public:
};

struct MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields
{
public:
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::POSITION_DETECTION_PATTERN
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___POSITION_DETECTION_PATTERN_0;
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::POSITION_ADJUSTMENT_PATTERN
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___POSITION_ADJUSTMENT_PATTERN_1;
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2;
	// System.Int32[][] ZXing.QrCode.Internal.MatrixUtil::TYPE_INFO_COORDINATES
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___TYPE_INFO_COORDINATES_3;

public:
	inline static int32_t get_offset_of_POSITION_DETECTION_PATTERN_0() { return static_cast<int32_t>(offsetof(MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields, ___POSITION_DETECTION_PATTERN_0)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_POSITION_DETECTION_PATTERN_0() const { return ___POSITION_DETECTION_PATTERN_0; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_POSITION_DETECTION_PATTERN_0() { return &___POSITION_DETECTION_PATTERN_0; }
	inline void set_POSITION_DETECTION_PATTERN_0(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___POSITION_DETECTION_PATTERN_0 = value;
		Il2CppCodeGenWriteBarrier((&___POSITION_DETECTION_PATTERN_0), value);
	}

	inline static int32_t get_offset_of_POSITION_ADJUSTMENT_PATTERN_1() { return static_cast<int32_t>(offsetof(MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields, ___POSITION_ADJUSTMENT_PATTERN_1)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_POSITION_ADJUSTMENT_PATTERN_1() const { return ___POSITION_ADJUSTMENT_PATTERN_1; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_POSITION_ADJUSTMENT_PATTERN_1() { return &___POSITION_ADJUSTMENT_PATTERN_1; }
	inline void set_POSITION_ADJUSTMENT_PATTERN_1(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___POSITION_ADJUSTMENT_PATTERN_1 = value;
		Il2CppCodeGenWriteBarrier((&___POSITION_ADJUSTMENT_PATTERN_1), value);
	}

	inline static int32_t get_offset_of_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2() { return static_cast<int32_t>(offsetof(MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields, ___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2() const { return ___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2() { return &___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2; }
	inline void set_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2 = value;
		Il2CppCodeGenWriteBarrier((&___POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2), value);
	}

	inline static int32_t get_offset_of_TYPE_INFO_COORDINATES_3() { return static_cast<int32_t>(offsetof(MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields, ___TYPE_INFO_COORDINATES_3)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_TYPE_INFO_COORDINATES_3() const { return ___TYPE_INFO_COORDINATES_3; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_TYPE_INFO_COORDINATES_3() { return &___TYPE_INFO_COORDINATES_3; }
	inline void set_TYPE_INFO_COORDINATES_3(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___TYPE_INFO_COORDINATES_3 = value;
		Il2CppCodeGenWriteBarrier((&___TYPE_INFO_COORDINATES_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATRIXUTIL_TE13307CA93A76F507F8CBE25D71D0B0B959F316D_H
#ifndef QRCODE_TD603DF4ACFB98D2888F453012084047B0FBB7B0B_H
#define QRCODE_TD603DF4ACFB98D2888F453012084047B0FBB7B0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.QRCode
struct  QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B  : public RuntimeObject
{
public:
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.QRCode::<Mode>k__BackingField
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___U3CModeU3Ek__BackingField_1;
	// ZXing.QrCode.Internal.ErrorCorrectionLevel ZXing.QrCode.Internal.QRCode::<ECLevel>k__BackingField
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * ___U3CECLevelU3Ek__BackingField_2;
	// ZXing.QrCode.Internal.Version ZXing.QrCode.Internal.QRCode::<Version>k__BackingField
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 * ___U3CVersionU3Ek__BackingField_3;
	// System.Int32 ZXing.QrCode.Internal.QRCode::<MaskPattern>k__BackingField
	int32_t ___U3CMaskPatternU3Ek__BackingField_4;
	// ZXing.QrCode.Internal.ByteMatrix ZXing.QrCode.Internal.QRCode::<Matrix>k__BackingField
	ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8 * ___U3CMatrixU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CModeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B, ___U3CModeU3Ek__BackingField_1)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_U3CModeU3Ek__BackingField_1() const { return ___U3CModeU3Ek__BackingField_1; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_U3CModeU3Ek__BackingField_1() { return &___U3CModeU3Ek__BackingField_1; }
	inline void set_U3CModeU3Ek__BackingField_1(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___U3CModeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CModeU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CECLevelU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B, ___U3CECLevelU3Ek__BackingField_2)); }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * get_U3CECLevelU3Ek__BackingField_2() const { return ___U3CECLevelU3Ek__BackingField_2; }
	inline ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D ** get_address_of_U3CECLevelU3Ek__BackingField_2() { return &___U3CECLevelU3Ek__BackingField_2; }
	inline void set_U3CECLevelU3Ek__BackingField_2(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D * value)
	{
		___U3CECLevelU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CECLevelU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B, ___U3CVersionU3Ek__BackingField_3)); }
	inline Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 * get_U3CVersionU3Ek__BackingField_3() const { return ___U3CVersionU3Ek__BackingField_3; }
	inline Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 ** get_address_of_U3CVersionU3Ek__BackingField_3() { return &___U3CVersionU3Ek__BackingField_3; }
	inline void set_U3CVersionU3Ek__BackingField_3(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1 * value)
	{
		___U3CVersionU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVersionU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CMaskPatternU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B, ___U3CMaskPatternU3Ek__BackingField_4)); }
	inline int32_t get_U3CMaskPatternU3Ek__BackingField_4() const { return ___U3CMaskPatternU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CMaskPatternU3Ek__BackingField_4() { return &___U3CMaskPatternU3Ek__BackingField_4; }
	inline void set_U3CMaskPatternU3Ek__BackingField_4(int32_t value)
	{
		___U3CMaskPatternU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMatrixU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B, ___U3CMatrixU3Ek__BackingField_5)); }
	inline ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8 * get_U3CMatrixU3Ek__BackingField_5() const { return ___U3CMatrixU3Ek__BackingField_5; }
	inline ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8 ** get_address_of_U3CMatrixU3Ek__BackingField_5() { return &___U3CMatrixU3Ek__BackingField_5; }
	inline void set_U3CMatrixU3Ek__BackingField_5(ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8 * value)
	{
		___U3CMatrixU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMatrixU3Ek__BackingField_5), value);
	}
};

struct QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B_StaticFields
{
public:
	// System.Int32 ZXing.QrCode.Internal.QRCode::NUM_MASK_PATTERNS
	int32_t ___NUM_MASK_PATTERNS_0;

public:
	inline static int32_t get_offset_of_NUM_MASK_PATTERNS_0() { return static_cast<int32_t>(offsetof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B_StaticFields, ___NUM_MASK_PATTERNS_0)); }
	inline int32_t get_NUM_MASK_PATTERNS_0() const { return ___NUM_MASK_PATTERNS_0; }
	inline int32_t* get_address_of_NUM_MASK_PATTERNS_0() { return &___NUM_MASK_PATTERNS_0; }
	inline void set_NUM_MASK_PATTERNS_0(int32_t value)
	{
		___NUM_MASK_PATTERNS_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QRCODE_TD603DF4ACFB98D2888F453012084047B0FBB7B0B_H
#ifndef QRCODEDECODERMETADATA_TBB3F521268CFC674E2F944505529CED41B0B9FC0_H
#define QRCODEDECODERMETADATA_TBB3F521268CFC674E2F944505529CED41B0B9FC0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.QRCodeDecoderMetaData
struct  QRCodeDecoderMetaData_tBB3F521268CFC674E2F944505529CED41B0B9FC0  : public RuntimeObject
{
public:
	// System.Boolean ZXing.QrCode.Internal.QRCodeDecoderMetaData::mirrored
	bool ___mirrored_0;

public:
	inline static int32_t get_offset_of_mirrored_0() { return static_cast<int32_t>(offsetof(QRCodeDecoderMetaData_tBB3F521268CFC674E2F944505529CED41B0B9FC0, ___mirrored_0)); }
	inline bool get_mirrored_0() const { return ___mirrored_0; }
	inline bool* get_address_of_mirrored_0() { return &___mirrored_0; }
	inline void set_mirrored_0(bool value)
	{
		___mirrored_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QRCODEDECODERMETADATA_TBB3F521268CFC674E2F944505529CED41B0B9FC0_H
#ifndef VERSION_TD151A57660A82DB744C62D59CFCBA720AAE775D1_H
#define VERSION_TD151A57660A82DB744C62D59CFCBA720AAE775D1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Version
struct  Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version::versionNumber
	int32_t ___versionNumber_2;
	// System.Int32[] ZXing.QrCode.Internal.Version::alignmentPatternCenters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___alignmentPatternCenters_3;
	// ZXing.QrCode.Internal.Version/ECBlocks[] ZXing.QrCode.Internal.Version::ecBlocks
	ECBlocksU5BU5D_tA72795133BAA9387DE9185AEEB240E175CCCE440* ___ecBlocks_4;
	// System.Int32 ZXing.QrCode.Internal.Version::totalCodewords
	int32_t ___totalCodewords_5;

public:
	inline static int32_t get_offset_of_versionNumber_2() { return static_cast<int32_t>(offsetof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1, ___versionNumber_2)); }
	inline int32_t get_versionNumber_2() const { return ___versionNumber_2; }
	inline int32_t* get_address_of_versionNumber_2() { return &___versionNumber_2; }
	inline void set_versionNumber_2(int32_t value)
	{
		___versionNumber_2 = value;
	}

	inline static int32_t get_offset_of_alignmentPatternCenters_3() { return static_cast<int32_t>(offsetof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1, ___alignmentPatternCenters_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_alignmentPatternCenters_3() const { return ___alignmentPatternCenters_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_alignmentPatternCenters_3() { return &___alignmentPatternCenters_3; }
	inline void set_alignmentPatternCenters_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___alignmentPatternCenters_3 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentPatternCenters_3), value);
	}

	inline static int32_t get_offset_of_ecBlocks_4() { return static_cast<int32_t>(offsetof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1, ___ecBlocks_4)); }
	inline ECBlocksU5BU5D_tA72795133BAA9387DE9185AEEB240E175CCCE440* get_ecBlocks_4() const { return ___ecBlocks_4; }
	inline ECBlocksU5BU5D_tA72795133BAA9387DE9185AEEB240E175CCCE440** get_address_of_ecBlocks_4() { return &___ecBlocks_4; }
	inline void set_ecBlocks_4(ECBlocksU5BU5D_tA72795133BAA9387DE9185AEEB240E175CCCE440* value)
	{
		___ecBlocks_4 = value;
		Il2CppCodeGenWriteBarrier((&___ecBlocks_4), value);
	}

	inline static int32_t get_offset_of_totalCodewords_5() { return static_cast<int32_t>(offsetof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1, ___totalCodewords_5)); }
	inline int32_t get_totalCodewords_5() const { return ___totalCodewords_5; }
	inline int32_t* get_address_of_totalCodewords_5() { return &___totalCodewords_5; }
	inline void set_totalCodewords_5(int32_t value)
	{
		___totalCodewords_5 = value;
	}
};

struct Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1_StaticFields
{
public:
	// System.Int32[] ZXing.QrCode.Internal.Version::VERSION_DECODE_INFO
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___VERSION_DECODE_INFO_0;
	// ZXing.QrCode.Internal.Version[] ZXing.QrCode.Internal.Version::VERSIONS
	VersionU5BU5D_t9A0DBA9F613F4D181A35C7CEF93691870C8CE02D* ___VERSIONS_1;

public:
	inline static int32_t get_offset_of_VERSION_DECODE_INFO_0() { return static_cast<int32_t>(offsetof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1_StaticFields, ___VERSION_DECODE_INFO_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_VERSION_DECODE_INFO_0() const { return ___VERSION_DECODE_INFO_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_VERSION_DECODE_INFO_0() { return &___VERSION_DECODE_INFO_0; }
	inline void set_VERSION_DECODE_INFO_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___VERSION_DECODE_INFO_0 = value;
		Il2CppCodeGenWriteBarrier((&___VERSION_DECODE_INFO_0), value);
	}

	inline static int32_t get_offset_of_VERSIONS_1() { return static_cast<int32_t>(offsetof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1_StaticFields, ___VERSIONS_1)); }
	inline VersionU5BU5D_t9A0DBA9F613F4D181A35C7CEF93691870C8CE02D* get_VERSIONS_1() const { return ___VERSIONS_1; }
	inline VersionU5BU5D_t9A0DBA9F613F4D181A35C7CEF93691870C8CE02D** get_address_of_VERSIONS_1() { return &___VERSIONS_1; }
	inline void set_VERSIONS_1(VersionU5BU5D_t9A0DBA9F613F4D181A35C7CEF93691870C8CE02D* value)
	{
		___VERSIONS_1 = value;
		Il2CppCodeGenWriteBarrier((&___VERSIONS_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VERSION_TD151A57660A82DB744C62D59CFCBA720AAE775D1_H
#ifndef ECB_TA7C4D638E18E1F839A13E41A9F4AE866FF41A452_H
#define ECB_TA7C4D638E18E1F839A13E41A9F4AE866FF41A452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Version/ECB
struct  ECB_tA7C4D638E18E1F839A13E41A9F4AE866FF41A452  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version/ECB::count
	int32_t ___count_0;
	// System.Int32 ZXing.QrCode.Internal.Version/ECB::dataCodewords
	int32_t ___dataCodewords_1;

public:
	inline static int32_t get_offset_of_count_0() { return static_cast<int32_t>(offsetof(ECB_tA7C4D638E18E1F839A13E41A9F4AE866FF41A452, ___count_0)); }
	inline int32_t get_count_0() const { return ___count_0; }
	inline int32_t* get_address_of_count_0() { return &___count_0; }
	inline void set_count_0(int32_t value)
	{
		___count_0 = value;
	}

	inline static int32_t get_offset_of_dataCodewords_1() { return static_cast<int32_t>(offsetof(ECB_tA7C4D638E18E1F839A13E41A9F4AE866FF41A452, ___dataCodewords_1)); }
	inline int32_t get_dataCodewords_1() const { return ___dataCodewords_1; }
	inline int32_t* get_address_of_dataCodewords_1() { return &___dataCodewords_1; }
	inline void set_dataCodewords_1(int32_t value)
	{
		___dataCodewords_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECB_TA7C4D638E18E1F839A13E41A9F4AE866FF41A452_H
#ifndef ECBLOCKS_T974AF513E12A611B900BFA1CB3679332F0E70DF9_H
#define ECBLOCKS_T974AF513E12A611B900BFA1CB3679332F0E70DF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Version/ECBlocks
struct  ECBlocks_t974AF513E12A611B900BFA1CB3679332F0E70DF9  : public RuntimeObject
{
public:
	// System.Int32 ZXing.QrCode.Internal.Version/ECBlocks::ecCodewordsPerBlock
	int32_t ___ecCodewordsPerBlock_0;
	// ZXing.QrCode.Internal.Version/ECB[] ZXing.QrCode.Internal.Version/ECBlocks::ecBlocks
	ECBU5BU5D_t980EAD239246CC90E06A178A3C2ED7671E9E6B0E* ___ecBlocks_1;

public:
	inline static int32_t get_offset_of_ecCodewordsPerBlock_0() { return static_cast<int32_t>(offsetof(ECBlocks_t974AF513E12A611B900BFA1CB3679332F0E70DF9, ___ecCodewordsPerBlock_0)); }
	inline int32_t get_ecCodewordsPerBlock_0() const { return ___ecCodewordsPerBlock_0; }
	inline int32_t* get_address_of_ecCodewordsPerBlock_0() { return &___ecCodewordsPerBlock_0; }
	inline void set_ecCodewordsPerBlock_0(int32_t value)
	{
		___ecCodewordsPerBlock_0 = value;
	}

	inline static int32_t get_offset_of_ecBlocks_1() { return static_cast<int32_t>(offsetof(ECBlocks_t974AF513E12A611B900BFA1CB3679332F0E70DF9, ___ecBlocks_1)); }
	inline ECBU5BU5D_t980EAD239246CC90E06A178A3C2ED7671E9E6B0E* get_ecBlocks_1() const { return ___ecBlocks_1; }
	inline ECBU5BU5D_t980EAD239246CC90E06A178A3C2ED7671E9E6B0E** get_address_of_ecBlocks_1() { return &___ecBlocks_1; }
	inline void set_ecBlocks_1(ECBU5BU5D_t980EAD239246CC90E06A178A3C2ED7671E9E6B0E* value)
	{
		___ecBlocks_1 = value;
		Il2CppCodeGenWriteBarrier((&___ecBlocks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ECBLOCKS_T974AF513E12A611B900BFA1CB3679332F0E70DF9_H
#ifndef QRCODEREADER_T662C5686AF3F1EC13E0D5812A1268901A31F801C_H
#define QRCODEREADER_T662C5686AF3F1EC13E0D5812A1268901A31F801C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.QRCodeReader
struct  QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C  : public RuntimeObject
{
public:
	// ZXing.QrCode.Internal.Decoder ZXing.QrCode.QRCodeReader::decoder
	Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D * ___decoder_1;

public:
	inline static int32_t get_offset_of_decoder_1() { return static_cast<int32_t>(offsetof(QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C, ___decoder_1)); }
	inline Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D * get_decoder_1() const { return ___decoder_1; }
	inline Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D ** get_address_of_decoder_1() { return &___decoder_1; }
	inline void set_decoder_1(Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D * value)
	{
		___decoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_1), value);
	}
};

struct QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C_StaticFields
{
public:
	// ZXing.ResultPoint[] ZXing.QrCode.QRCodeReader::NO_POINTS
	ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* ___NO_POINTS_0;

public:
	inline static int32_t get_offset_of_NO_POINTS_0() { return static_cast<int32_t>(offsetof(QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C_StaticFields, ___NO_POINTS_0)); }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* get_NO_POINTS_0() const { return ___NO_POINTS_0; }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C** get_address_of_NO_POINTS_0() { return &___NO_POINTS_0; }
	inline void set_NO_POINTS_0(ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* value)
	{
		___NO_POINTS_0 = value;
		Il2CppCodeGenWriteBarrier((&___NO_POINTS_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QRCODEREADER_T662C5686AF3F1EC13E0D5812A1268901A31F801C_H
#ifndef QRCODEWRITER_T0E2F30CBC804571278B1EF03034085F53F9E09D7_H
#define QRCODEWRITER_T0E2F30CBC804571278B1EF03034085F53F9E09D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.QRCodeWriter
struct  QRCodeWriter_t0E2F30CBC804571278B1EF03034085F53F9E09D7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QRCODEWRITER_T0E2F30CBC804571278B1EF03034085F53F9E09D7_H
#ifndef RESULTPOINT_TB5E62D12630394F9102A06C86C7FBF293B269166_H
#define RESULTPOINT_TB5E62D12630394F9102A06C86C7FBF293B269166_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.ResultPoint
struct  ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166  : public RuntimeObject
{
public:
	// System.Single ZXing.ResultPoint::x
	float ___x_0;
	// System.Single ZXing.ResultPoint::y
	float ___y_1;
	// System.Byte[] ZXing.ResultPoint::bytesX
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytesX_2;
	// System.Byte[] ZXing.ResultPoint::bytesY
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytesY_3;
	// System.String ZXing.ResultPoint::toString
	String_t* ___toString_4;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_bytesX_2() { return static_cast<int32_t>(offsetof(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166, ___bytesX_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytesX_2() const { return ___bytesX_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytesX_2() { return &___bytesX_2; }
	inline void set_bytesX_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytesX_2 = value;
		Il2CppCodeGenWriteBarrier((&___bytesX_2), value);
	}

	inline static int32_t get_offset_of_bytesY_3() { return static_cast<int32_t>(offsetof(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166, ___bytesY_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytesY_3() const { return ___bytesY_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytesY_3() { return &___bytesY_3; }
	inline void set_bytesY_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytesY_3 = value;
		Il2CppCodeGenWriteBarrier((&___bytesY_3), value);
	}

	inline static int32_t get_offset_of_toString_4() { return static_cast<int32_t>(offsetof(ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166, ___toString_4)); }
	inline String_t* get_toString_4() const { return ___toString_4; }
	inline String_t** get_address_of_toString_4() { return &___toString_4; }
	inline void set_toString_4(String_t* value)
	{
		___toString_4 = value;
		Il2CppCodeGenWriteBarrier((&___toString_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTPOINT_TB5E62D12630394F9102A06C86C7FBF293B269166_H
#ifndef SUPPORTCLASS_T619C93DCCD87C08548C4DE2BA5498814BC8AF87A_H
#define SUPPORTCLASS_T619C93DCCD87C08548C4DE2BA5498814BC8AF87A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.SupportClass
struct  SupportClass_t619C93DCCD87C08548C4DE2BA5498814BC8AF87A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUPPORTCLASS_T619C93DCCD87C08548C4DE2BA5498814BC8AF87A_H
#ifndef BIGINTEGEREXCEPTION_T031C834FFDAD6CF18A708914DB5603E69C6004C1_H
#define BIGINTEGEREXCEPTION_T031C834FFDAD6CF18A708914DB5603E69C6004C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.BigIntegerException
struct  BigIntegerException_t031C834FFDAD6CF18A708914DB5603E69C6004C1  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGEREXCEPTION_T031C834FFDAD6CF18A708914DB5603E69C6004C1_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#define SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.Subsystem`1<UnityEngine.XR.FaceSubsystem.XRFaceSubsystemDescriptor>
struct  Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C  : public Subsystem_t9D40133A9FEB984E2C3695B077DA4DDFC91CA181
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBSYSTEM_1_T7A31187B2E3DFF03501265C781C9A8547A073F0C_H
#ifndef TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#define TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Experimental.XR.TrackableId
struct  TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B 
{
public:
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId1
	uint64_t ___m_SubId1_1;
	// System.UInt64 UnityEngine.Experimental.XR.TrackableId::m_SubId2
	uint64_t ___m_SubId2_2;

public:
	inline static int32_t get_offset_of_m_SubId1_1() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId1_1)); }
	inline uint64_t get_m_SubId1_1() const { return ___m_SubId1_1; }
	inline uint64_t* get_address_of_m_SubId1_1() { return &___m_SubId1_1; }
	inline void set_m_SubId1_1(uint64_t value)
	{
		___m_SubId1_1 = value;
	}

	inline static int32_t get_offset_of_m_SubId2_2() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B, ___m_SubId2_2)); }
	inline uint64_t get_m_SubId2_2() const { return ___m_SubId2_2; }
	inline uint64_t* get_address_of_m_SubId2_2() { return &___m_SubId2_2; }
	inline void set_m_SubId2_2(uint64_t value)
	{
		___m_SubId2_2 = value;
	}
};

struct TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.Experimental.XR.TrackableId::s_InvalidId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___s_InvalidId_0;

public:
	inline static int32_t get_offset_of_s_InvalidId_0() { return static_cast<int32_t>(offsetof(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B_StaticFields, ___s_InvalidId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_s_InvalidId_0() const { return ___s_InvalidId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_s_InvalidId_0() { return &___s_InvalidId_0; }
	inline void set_s_InvalidId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___s_InvalidId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACKABLEID_TA539F57E82A04D410FE11E10ACC830CF7CD71F7B_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#define ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequest
struct  ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequest::m_RequestId
	int32_t ___m_RequestId_0;

public:
	inline static int32_t get_offset_of_m_RequestId_0() { return static_cast<int32_t>(offsetof(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167, ___m_RequestId_0)); }
	inline int32_t get_m_RequestId_0() const { return ___m_RequestId_0; }
	inline int32_t* get_address_of_m_RequestId_0() { return &___m_RequestId_0; }
	inline void set_m_RequestId_0(int32_t value)
	{
		___m_RequestId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUEST_T4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167_H
#ifndef BARCODEREADER_TEC59FAC295BC5A75121E2CF723C04E0748921224_H
#define BARCODEREADER_TEC59FAC295BC5A75121E2CF723C04E0748921224_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeReader
struct  BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224  : public BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75
{
public:
	// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReader::createLuminanceSource
	Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE * ___createLuminanceSource_11;

public:
	inline static int32_t get_offset_of_createLuminanceSource_11() { return static_cast<int32_t>(offsetof(BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224, ___createLuminanceSource_11)); }
	inline Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE * get_createLuminanceSource_11() const { return ___createLuminanceSource_11; }
	inline Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE ** get_address_of_createLuminanceSource_11() { return &___createLuminanceSource_11; }
	inline void set_createLuminanceSource_11(Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE * value)
	{
		___createLuminanceSource_11 = value;
		Il2CppCodeGenWriteBarrier((&___createLuminanceSource_11), value);
	}
};

struct BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224_StaticFields
{
public:
	// System.Func`4<UnityEngine.Color32[],System.Int32,System.Int32,ZXing.LuminanceSource> ZXing.BarcodeReader::defaultCreateLuminanceSource
	Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE * ___defaultCreateLuminanceSource_10;

public:
	inline static int32_t get_offset_of_defaultCreateLuminanceSource_10() { return static_cast<int32_t>(offsetof(BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224_StaticFields, ___defaultCreateLuminanceSource_10)); }
	inline Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE * get_defaultCreateLuminanceSource_10() const { return ___defaultCreateLuminanceSource_10; }
	inline Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE ** get_address_of_defaultCreateLuminanceSource_10() { return &___defaultCreateLuminanceSource_10; }
	inline void set_defaultCreateLuminanceSource_10(Func_4_tDDD1BFDEF0F1C46318D4D1E8FEABFF6954DCC9FE * value)
	{
		___defaultCreateLuminanceSource_10 = value;
		Il2CppCodeGenWriteBarrier((&___defaultCreateLuminanceSource_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARCODEREADER_TEC59FAC295BC5A75121E2CF723C04E0748921224_H
#ifndef BASELUMINANCESOURCE_T8CA757494571D978084F5027CC520F1FDA9C3163_H
#define BASELUMINANCESOURCE_T8CA757494571D978084F5027CC520F1FDA9C3163_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BaseLuminanceSource
struct  BaseLuminanceSource_t8CA757494571D978084F5027CC520F1FDA9C3163  : public LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1
{
public:
	// System.Byte[] ZXing.BaseLuminanceSource::luminances
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___luminances_2;

public:
	inline static int32_t get_offset_of_luminances_2() { return static_cast<int32_t>(offsetof(BaseLuminanceSource_t8CA757494571D978084F5027CC520F1FDA9C3163, ___luminances_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_luminances_2() const { return ___luminances_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_luminances_2() { return &___luminances_2; }
	inline void set_luminances_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___luminances_2 = value;
		Il2CppCodeGenWriteBarrier((&___luminances_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASELUMINANCESOURCE_T8CA757494571D978084F5027CC520F1FDA9C3163_H
#ifndef INVERTEDLUMINANCESOURCE_TD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01_H
#define INVERTEDLUMINANCESOURCE_TD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.InvertedLuminanceSource
struct  InvertedLuminanceSource_tD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01  : public LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1
{
public:
	// ZXing.LuminanceSource ZXing.InvertedLuminanceSource::delegate
	LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * ___delegate_2;
	// System.Byte[] ZXing.InvertedLuminanceSource::invertedMatrix
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___invertedMatrix_3;

public:
	inline static int32_t get_offset_of_delegate_2() { return static_cast<int32_t>(offsetof(InvertedLuminanceSource_tD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01, ___delegate_2)); }
	inline LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * get_delegate_2() const { return ___delegate_2; }
	inline LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 ** get_address_of_delegate_2() { return &___delegate_2; }
	inline void set_delegate_2(LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1 * value)
	{
		___delegate_2 = value;
		Il2CppCodeGenWriteBarrier((&___delegate_2), value);
	}

	inline static int32_t get_offset_of_invertedMatrix_3() { return static_cast<int32_t>(offsetof(InvertedLuminanceSource_tD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01, ___invertedMatrix_3)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_invertedMatrix_3() const { return ___invertedMatrix_3; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_invertedMatrix_3() { return &___invertedMatrix_3; }
	inline void set_invertedMatrix_3(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___invertedMatrix_3 = value;
		Il2CppCodeGenWriteBarrier((&___invertedMatrix_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVERTEDLUMINANCESOURCE_TD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01_H
#ifndef CODABARREADER_TA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_H
#define CODABARREADER_TA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.CodaBarReader
struct  CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:
	// System.Text.StringBuilder ZXing.OneD.CodaBarReader::decodeRowResult
	StringBuilder_t * ___decodeRowResult_7;
	// System.Int32[] ZXing.OneD.CodaBarReader::counters
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___counters_8;
	// System.Int32 ZXing.OneD.CodaBarReader::counterLength
	int32_t ___counterLength_9;

public:
	inline static int32_t get_offset_of_decodeRowResult_7() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC, ___decodeRowResult_7)); }
	inline StringBuilder_t * get_decodeRowResult_7() const { return ___decodeRowResult_7; }
	inline StringBuilder_t ** get_address_of_decodeRowResult_7() { return &___decodeRowResult_7; }
	inline void set_decodeRowResult_7(StringBuilder_t * value)
	{
		___decodeRowResult_7 = value;
		Il2CppCodeGenWriteBarrier((&___decodeRowResult_7), value);
	}

	inline static int32_t get_offset_of_counters_8() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC, ___counters_8)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_counters_8() const { return ___counters_8; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_counters_8() { return &___counters_8; }
	inline void set_counters_8(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___counters_8 = value;
		Il2CppCodeGenWriteBarrier((&___counters_8), value);
	}

	inline static int32_t get_offset_of_counterLength_9() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC, ___counterLength_9)); }
	inline int32_t get_counterLength_9() const { return ___counterLength_9; }
	inline int32_t* get_address_of_counterLength_9() { return &___counterLength_9; }
	inline void set_counterLength_9(int32_t value)
	{
		___counterLength_9 = value;
	}
};

struct CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields
{
public:
	// System.Int32 ZXing.OneD.CodaBarReader::MAX_ACCEPTABLE
	int32_t ___MAX_ACCEPTABLE_2;
	// System.Int32 ZXing.OneD.CodaBarReader::PADDING
	int32_t ___PADDING_3;
	// System.Char[] ZXing.OneD.CodaBarReader::ALPHABET
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___ALPHABET_4;
	// System.Int32[] ZXing.OneD.CodaBarReader::CHARACTER_ENCODINGS
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___CHARACTER_ENCODINGS_5;
	// System.Char[] ZXing.OneD.CodaBarReader::STARTEND_ENCODING
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___STARTEND_ENCODING_6;

public:
	inline static int32_t get_offset_of_MAX_ACCEPTABLE_2() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields, ___MAX_ACCEPTABLE_2)); }
	inline int32_t get_MAX_ACCEPTABLE_2() const { return ___MAX_ACCEPTABLE_2; }
	inline int32_t* get_address_of_MAX_ACCEPTABLE_2() { return &___MAX_ACCEPTABLE_2; }
	inline void set_MAX_ACCEPTABLE_2(int32_t value)
	{
		___MAX_ACCEPTABLE_2 = value;
	}

	inline static int32_t get_offset_of_PADDING_3() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields, ___PADDING_3)); }
	inline int32_t get_PADDING_3() const { return ___PADDING_3; }
	inline int32_t* get_address_of_PADDING_3() { return &___PADDING_3; }
	inline void set_PADDING_3(int32_t value)
	{
		___PADDING_3 = value;
	}

	inline static int32_t get_offset_of_ALPHABET_4() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields, ___ALPHABET_4)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_ALPHABET_4() const { return ___ALPHABET_4; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_ALPHABET_4() { return &___ALPHABET_4; }
	inline void set_ALPHABET_4(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___ALPHABET_4 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHABET_4), value);
	}

	inline static int32_t get_offset_of_CHARACTER_ENCODINGS_5() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields, ___CHARACTER_ENCODINGS_5)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_CHARACTER_ENCODINGS_5() const { return ___CHARACTER_ENCODINGS_5; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_CHARACTER_ENCODINGS_5() { return &___CHARACTER_ENCODINGS_5; }
	inline void set_CHARACTER_ENCODINGS_5(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___CHARACTER_ENCODINGS_5 = value;
		Il2CppCodeGenWriteBarrier((&___CHARACTER_ENCODINGS_5), value);
	}

	inline static int32_t get_offset_of_STARTEND_ENCODING_6() { return static_cast<int32_t>(offsetof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields, ___STARTEND_ENCODING_6)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_STARTEND_ENCODING_6() const { return ___STARTEND_ENCODING_6; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_STARTEND_ENCODING_6() { return &___STARTEND_ENCODING_6; }
	inline void set_STARTEND_ENCODING_6(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___STARTEND_ENCODING_6 = value;
		Il2CppCodeGenWriteBarrier((&___STARTEND_ENCODING_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODABARREADER_TA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_H
#ifndef CODE128READER_T279C9EDF71B08071A4E0DA45EE1A888D014C409C_H
#define CODE128READER_T279C9EDF71B08071A4E0DA45EE1A888D014C409C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.OneD.Code128Reader
struct  Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C  : public OneDReader_tBADC3E0365598C30BA76B3A2FD9CFB68C2B821AC
{
public:

public:
};

struct Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields
{
public:
	// System.Int32[][] ZXing.OneD.Code128Reader::CODE_PATTERNS
	Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* ___CODE_PATTERNS_2;
	// System.Int32 ZXing.OneD.Code128Reader::MAX_AVG_VARIANCE
	int32_t ___MAX_AVG_VARIANCE_3;
	// System.Int32 ZXing.OneD.Code128Reader::MAX_INDIVIDUAL_VARIANCE
	int32_t ___MAX_INDIVIDUAL_VARIANCE_4;

public:
	inline static int32_t get_offset_of_CODE_PATTERNS_2() { return static_cast<int32_t>(offsetof(Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields, ___CODE_PATTERNS_2)); }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* get_CODE_PATTERNS_2() const { return ___CODE_PATTERNS_2; }
	inline Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43** get_address_of_CODE_PATTERNS_2() { return &___CODE_PATTERNS_2; }
	inline void set_CODE_PATTERNS_2(Int32U5BU5DU5BU5D_tCA34E042D233821D51B4DAFB480EE602F2DBEF43* value)
	{
		___CODE_PATTERNS_2 = value;
		Il2CppCodeGenWriteBarrier((&___CODE_PATTERNS_2), value);
	}

	inline static int32_t get_offset_of_MAX_AVG_VARIANCE_3() { return static_cast<int32_t>(offsetof(Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields, ___MAX_AVG_VARIANCE_3)); }
	inline int32_t get_MAX_AVG_VARIANCE_3() const { return ___MAX_AVG_VARIANCE_3; }
	inline int32_t* get_address_of_MAX_AVG_VARIANCE_3() { return &___MAX_AVG_VARIANCE_3; }
	inline void set_MAX_AVG_VARIANCE_3(int32_t value)
	{
		___MAX_AVG_VARIANCE_3 = value;
	}

	inline static int32_t get_offset_of_MAX_INDIVIDUAL_VARIANCE_4() { return static_cast<int32_t>(offsetof(Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields, ___MAX_INDIVIDUAL_VARIANCE_4)); }
	inline int32_t get_MAX_INDIVIDUAL_VARIANCE_4() const { return ___MAX_INDIVIDUAL_VARIANCE_4; }
	inline int32_t* get_address_of_MAX_INDIVIDUAL_VARIANCE_4() { return &___MAX_INDIVIDUAL_VARIANCE_4; }
	inline void set_MAX_INDIVIDUAL_VARIANCE_4(int32_t value)
	{
		___MAX_INDIVIDUAL_VARIANCE_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CODE128READER_T279C9EDF71B08071A4E0DA45EE1A888D014C409C_H
#ifndef DETECTIONRESULTROWINDICATORCOLUMN_TFE78E0F364DAAB13092052C991910C850F6B26BA_H
#define DETECTIONRESULTROWINDICATORCOLUMN_TFE78E0F364DAAB13092052C991910C850F6B26BA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn
struct  DetectionResultRowIndicatorColumn_tFE78E0F364DAAB13092052C991910C850F6B26BA  : public DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3
{
public:
	// System.Boolean ZXing.PDF417.Internal.DetectionResultRowIndicatorColumn::<IsLeft>k__BackingField
	bool ___U3CIsLeftU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsLeftU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DetectionResultRowIndicatorColumn_tFE78E0F364DAAB13092052C991910C850F6B26BA, ___U3CIsLeftU3Ek__BackingField_2)); }
	inline bool get_U3CIsLeftU3Ek__BackingField_2() const { return ___U3CIsLeftU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CIsLeftU3Ek__BackingField_2() { return &___U3CIsLeftU3Ek__BackingField_2; }
	inline void set_U3CIsLeftU3Ek__BackingField_2(bool value)
	{
		___U3CIsLeftU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DETECTIONRESULTROWINDICATORCOLUMN_TFE78E0F364DAAB13092052C991910C850F6B26BA_H
#ifndef ALIGNMENTPATTERN_T9BE63C52A771F31D2B57BA097960F8FDB86588C4_H
#define ALIGNMENTPATTERN_T9BE63C52A771F31D2B57BA097960F8FDB86588C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.AlignmentPattern
struct  AlignmentPattern_t9BE63C52A771F31D2B57BA097960F8FDB86588C4  : public ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166
{
public:
	// System.Single ZXing.QrCode.Internal.AlignmentPattern::estimatedModuleSize
	float ___estimatedModuleSize_5;

public:
	inline static int32_t get_offset_of_estimatedModuleSize_5() { return static_cast<int32_t>(offsetof(AlignmentPattern_t9BE63C52A771F31D2B57BA097960F8FDB86588C4, ___estimatedModuleSize_5)); }
	inline float get_estimatedModuleSize_5() const { return ___estimatedModuleSize_5; }
	inline float* get_address_of_estimatedModuleSize_5() { return &___estimatedModuleSize_5; }
	inline void set_estimatedModuleSize_5(float value)
	{
		___estimatedModuleSize_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALIGNMENTPATTERN_T9BE63C52A771F31D2B57BA097960F8FDB86588C4_H
#ifndef FINDERPATTERN_TE3AB02775E6BCC56D11BF83BED8316D490329DBE_H
#define FINDERPATTERN_TE3AB02775E6BCC56D11BF83BED8316D490329DBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.FinderPattern
struct  FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE  : public ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166
{
public:
	// System.Single ZXing.QrCode.Internal.FinderPattern::estimatedModuleSize
	float ___estimatedModuleSize_5;
	// System.Int32 ZXing.QrCode.Internal.FinderPattern::count
	int32_t ___count_6;

public:
	inline static int32_t get_offset_of_estimatedModuleSize_5() { return static_cast<int32_t>(offsetof(FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE, ___estimatedModuleSize_5)); }
	inline float get_estimatedModuleSize_5() const { return ___estimatedModuleSize_5; }
	inline float* get_address_of_estimatedModuleSize_5() { return &___estimatedModuleSize_5; }
	inline void set_estimatedModuleSize_5(float value)
	{
		___estimatedModuleSize_5 = value;
	}

	inline static int32_t get_offset_of_count_6() { return static_cast<int32_t>(offsetof(FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE, ___count_6)); }
	inline int32_t get_count_6() const { return ___count_6; }
	inline int32_t* get_address_of_count_6() { return &___count_6; }
	inline void set_count_6(int32_t value)
	{
		___count_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FINDERPATTERN_TE3AB02775E6BCC56D11BF83BED8316D490329DBE_H
#ifndef READEREXCEPTION_T0B65A1A4C77C6490166DF90D1206CD113D9F75E8_H
#define READEREXCEPTION_T0B65A1A4C77C6490166DF90D1206CD113D9F75E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.ReaderException
struct  ReaderException_t0B65A1A4C77C6490166DF90D1206CD113D9F75E8  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READEREXCEPTION_T0B65A1A4C77C6490166DF90D1206CD113D9F75E8_H
#ifndef WRITEREXCEPTION_T64A2D5441667D3429DEDF25ED6A3D371568904B2_H
#define WRITEREXCEPTION_T64A2D5441667D3429DEDF25ED6A3D371568904B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.WriterException
struct  WriterException_t64A2D5441667D3429DEDF25ED6A3D371568904B2  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITEREXCEPTION_T64A2D5441667D3429DEDF25ED6A3D371568904B2_H
#ifndef SIGN_T210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C_H
#define SIGN_T210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.Sign
struct  Sign_t210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C 
{
public:
	// System.Int32 BigIntegerLibrary.Sign::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Sign_t210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIGN_T210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#define POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Pose
struct  Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 
{
public:
	// UnityEngine.Vector3 UnityEngine.Pose::position
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___position_0;
	// UnityEngine.Quaternion UnityEngine.Pose::rotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___rotation_1;

public:
	inline static int32_t get_offset_of_position_0() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___position_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_position_0() const { return ___position_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_position_0() { return &___position_0; }
	inline void set_position_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___position_0 = value;
	}

	inline static int32_t get_offset_of_rotation_1() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29, ___rotation_1)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_rotation_1() const { return ___rotation_1; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_rotation_1() { return &___rotation_1; }
	inline void set_rotation_1(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___rotation_1 = value;
	}
};

struct Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields
{
public:
	// UnityEngine.Pose UnityEngine.Pose::k_Identity
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___k_Identity_2;

public:
	inline static int32_t get_offset_of_k_Identity_2() { return static_cast<int32_t>(offsetof(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29_StaticFields, ___k_Identity_2)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_k_Identity_2() const { return ___k_Identity_2; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_k_Identity_2() { return &___k_Identity_2; }
	inline void set_k_Identity_2(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___k_Identity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSE_T2997DE3CB3863E4D78FCF42B46FC481818823F29_H
#ifndef ARWORLDMAPREQUESTSTATUS_TF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC_H
#define ARWORLDMAPREQUESTSTATUS_TF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapRequestStatus
struct  ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.ARWorldMapRequestStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARWORLDMAPREQUESTSTATUS_TF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC_H
#ifndef XRARKITBLENDSHAPELOCATION_T2F4D95E87BAFF0BB690B931B8C91D77859D34C28_H
#define XRARKITBLENDSHAPELOCATION_T2F4D95E87BAFF0BB690B931B8C91D77859D34C28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.XRArkitBlendShapeLocation
struct  XRArkitBlendShapeLocation_t2F4D95E87BAFF0BB690B931B8C91D77859D34C28 
{
public:
	// System.Int32 UnityEngine.XR.ARKit.XRArkitBlendShapeLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XRArkitBlendShapeLocation_t2F4D95E87BAFF0BB690B931B8C91D77859D34C28, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRARKITBLENDSHAPELOCATION_T2F4D95E87BAFF0BB690B931B8C91D77859D34C28_H
#ifndef XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#define XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem
struct  XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86  : public Subsystem_1_t7A31187B2E3DFF03501265C781C9A8547A073F0C
{
public:
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceAdded
	Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * ___faceAdded_4;
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceUpdated
	Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * ___faceUpdated_5;
	// System.Action`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::faceRemoved
	Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * ___faceRemoved_6;

public:
	inline static int32_t get_offset_of_faceAdded_4() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceAdded_4)); }
	inline Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * get_faceAdded_4() const { return ___faceAdded_4; }
	inline Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B ** get_address_of_faceAdded_4() { return &___faceAdded_4; }
	inline void set_faceAdded_4(Action_1_tC60D9BF31F8190A675CBE7DE6C644BBC432EA87B * value)
	{
		___faceAdded_4 = value;
		Il2CppCodeGenWriteBarrier((&___faceAdded_4), value);
	}

	inline static int32_t get_offset_of_faceUpdated_5() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceUpdated_5)); }
	inline Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * get_faceUpdated_5() const { return ___faceUpdated_5; }
	inline Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 ** get_address_of_faceUpdated_5() { return &___faceUpdated_5; }
	inline void set_faceUpdated_5(Action_1_tFC89EA1D6FACA3D17CE797444240A0D4CA9127D4 * value)
	{
		___faceUpdated_5 = value;
		Il2CppCodeGenWriteBarrier((&___faceUpdated_5), value);
	}

	inline static int32_t get_offset_of_faceRemoved_6() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86, ___faceRemoved_6)); }
	inline Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * get_faceRemoved_6() const { return ___faceRemoved_6; }
	inline Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 ** get_address_of_faceRemoved_6() { return &___faceRemoved_6; }
	inline void set_faceRemoved_6(Action_1_t9B35D772EF08A6A56CC70BBA88E04E6BA5BE2466 * value)
	{
		___faceRemoved_6 = value;
		Il2CppCodeGenWriteBarrier((&___faceRemoved_6), value);
	}
};

struct XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields
{
public:
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceAddedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesAddedThisFrame
	List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * ___s_FacesAddedThisFrame_1;
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceUpdatedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesUpdatedThisFrame
	List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * ___s_FacesUpdatedThisFrame_2;
	// System.Collections.Generic.List`1<UnityEngine.XR.FaceSubsystem.FaceRemovedEventArgs> UnityEngine.XR.FaceSubsystem.XRFaceSubsystem::s_FacesRemovedThisFrame
	List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * ___s_FacesRemovedThisFrame_3;

public:
	inline static int32_t get_offset_of_s_FacesAddedThisFrame_1() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesAddedThisFrame_1)); }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * get_s_FacesAddedThisFrame_1() const { return ___s_FacesAddedThisFrame_1; }
	inline List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A ** get_address_of_s_FacesAddedThisFrame_1() { return &___s_FacesAddedThisFrame_1; }
	inline void set_s_FacesAddedThisFrame_1(List_1_tAF498548EF78A979CA19AF2EEE32849D1F3B737A * value)
	{
		___s_FacesAddedThisFrame_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesAddedThisFrame_1), value);
	}

	inline static int32_t get_offset_of_s_FacesUpdatedThisFrame_2() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesUpdatedThisFrame_2)); }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * get_s_FacesUpdatedThisFrame_2() const { return ___s_FacesUpdatedThisFrame_2; }
	inline List_1_tE2C2476479F1A844897C46751D09491B2534EC87 ** get_address_of_s_FacesUpdatedThisFrame_2() { return &___s_FacesUpdatedThisFrame_2; }
	inline void set_s_FacesUpdatedThisFrame_2(List_1_tE2C2476479F1A844897C46751D09491B2534EC87 * value)
	{
		___s_FacesUpdatedThisFrame_2 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesUpdatedThisFrame_2), value);
	}

	inline static int32_t get_offset_of_s_FacesRemovedThisFrame_3() { return static_cast<int32_t>(offsetof(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_StaticFields, ___s_FacesRemovedThisFrame_3)); }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * get_s_FacesRemovedThisFrame_3() const { return ___s_FacesRemovedThisFrame_3; }
	inline List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 ** get_address_of_s_FacesRemovedThisFrame_3() { return &___s_FacesRemovedThisFrame_3; }
	inline void set_s_FacesRemovedThisFrame_3(List_1_t00AB34A40BAE55CA5A4DEFE780E65EFC7B80A483 * value)
	{
		___s_FacesRemovedThisFrame_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_FacesRemovedThisFrame_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACESUBSYSTEM_TE0367EC4739D16FE0CA2820AAF6688D9ABA66B86_H
#ifndef BARCODEFORMAT_T5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A_H
#define BARCODEFORMAT_T5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.BarcodeFormat
struct  BarcodeFormat_t5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A 
{
public:
	// System.Int32 ZXing.BarcodeFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BarcodeFormat_t5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BARCODEFORMAT_T5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A_H
#ifndef COLOR32LUMINANCESOURCE_T656B988B56E74F7D098C0917EA3BE66A9163A3AA_H
#define COLOR32LUMINANCESOURCE_T656B988B56E74F7D098C0917EA3BE66A9163A3AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Color32LuminanceSource
struct  Color32LuminanceSource_t656B988B56E74F7D098C0917EA3BE66A9163A3AA  : public BaseLuminanceSource_t8CA757494571D978084F5027CC520F1FDA9C3163
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR32LUMINANCESOURCE_T656B988B56E74F7D098C0917EA3BE66A9163A3AA_H
#ifndef DECODEHINTTYPE_T6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF_H
#define DECODEHINTTYPE_T6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.DecodeHintType
struct  DecodeHintType_t6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF 
{
public:
	// System.Int32 ZXing.DecodeHintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DecodeHintType_t6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECODEHINTTYPE_T6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF_H
#ifndef ENCODEHINTTYPE_T8CFD0EAB2657FA9E27B05AAE65F320921789E8AA_H
#define ENCODEHINTTYPE_T8CFD0EAB2657FA9E27B05AAE65F320921789E8AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.EncodeHintType
struct  EncodeHintType_t8CFD0EAB2657FA9E27B05AAE65F320921789E8AA 
{
public:
	// System.Int32 ZXing.EncodeHintType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EncodeHintType_t8CFD0EAB2657FA9E27B05AAE65F320921789E8AA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENCODEHINTTYPE_T8CFD0EAB2657FA9E27B05AAE65F320921789E8AA_H
#ifndef FORMATEXCEPTION_T182FE700C0A582FDF3C3CCE9C3E14576DF372617_H
#define FORMATEXCEPTION_T182FE700C0A582FDF3C3CCE9C3E14576DF372617_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.FormatException
struct  FormatException_t182FE700C0A582FDF3C3CCE9C3E14576DF372617  : public ReaderException_t0B65A1A4C77C6490166DF90D1206CD113D9F75E8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATEXCEPTION_T182FE700C0A582FDF3C3CCE9C3E14576DF372617_H
#ifndef MODE_TDA24C1E76C9ADDB731E06F476107059880B93EA3_H
#define MODE_TDA24C1E76C9ADDB731E06F476107059880B93EA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.PDF417.Internal.DecodedBitStreamParser/Mode
struct  Mode_tDA24C1E76C9ADDB731E06F476107059880B93EA3 
{
public:
	// System.Int32 ZXing.PDF417.Internal.DecodedBitStreamParser/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_tDA24C1E76C9ADDB731E06F476107059880B93EA3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_TDA24C1E76C9ADDB731E06F476107059880B93EA3_H
#ifndef NAMES_T3971C3CE518B30C64CF847091748A25B28B0D9C9_H
#define NAMES_T3971C3CE518B30C64CF847091748A25B28B0D9C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Mode/Names
struct  Names_t3971C3CE518B30C64CF847091748A25B28B0D9C9 
{
public:
	// System.Int32 ZXing.QrCode.Internal.Mode/Names::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Names_t3971C3CE518B30C64CF847091748A25B28B0D9C9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMES_T3971C3CE518B30C64CF847091748A25B28B0D9C9_H
#ifndef RGBLUMINANCESOURCE_T2ABCDCB40ECC28EE75CAA94F0783FFEB47675D93_H
#define RGBLUMINANCESOURCE_T2ABCDCB40ECC28EE75CAA94F0783FFEB47675D93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.RGBLuminanceSource
struct  RGBLuminanceSource_t2ABCDCB40ECC28EE75CAA94F0783FFEB47675D93  : public BaseLuminanceSource_t8CA757494571D978084F5027CC520F1FDA9C3163
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RGBLUMINANCESOURCE_T2ABCDCB40ECC28EE75CAA94F0783FFEB47675D93_H
#ifndef BITMAPFORMAT_T7FC1576E756C5343AD09912793E9FA7F31C2B198_H
#define BITMAPFORMAT_T7FC1576E756C5343AD09912793E9FA7F31C2B198_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.RGBLuminanceSource/BitmapFormat
struct  BitmapFormat_t7FC1576E756C5343AD09912793E9FA7F31C2B198 
{
public:
	// System.Int32 ZXing.RGBLuminanceSource/BitmapFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BitmapFormat_t7FC1576E756C5343AD09912793E9FA7F31C2B198, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BITMAPFORMAT_T7FC1576E756C5343AD09912793E9FA7F31C2B198_H
#ifndef RESULTMETADATATYPE_T0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569_H
#define RESULTMETADATATYPE_T0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.ResultMetadataType
struct  ResultMetadataType_t0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569 
{
public:
	// System.Int32 ZXing.ResultMetadataType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ResultMetadataType_t0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTMETADATATYPE_T0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569_H
#ifndef BASE10BIGINTEGER_TE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_H
#define BASE10BIGINTEGER_TE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.Base10BigInteger
struct  Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539  : public RuntimeObject
{
public:
	// BigIntegerLibrary.Base10BigInteger/DigitContainer BigIntegerLibrary.Base10BigInteger::digits
	DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707 * ___digits_2;
	// System.Int32 BigIntegerLibrary.Base10BigInteger::size
	int32_t ___size_3;
	// BigIntegerLibrary.Sign BigIntegerLibrary.Base10BigInteger::sign
	int32_t ___sign_4;

public:
	inline static int32_t get_offset_of_digits_2() { return static_cast<int32_t>(offsetof(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539, ___digits_2)); }
	inline DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707 * get_digits_2() const { return ___digits_2; }
	inline DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707 ** get_address_of_digits_2() { return &___digits_2; }
	inline void set_digits_2(DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707 * value)
	{
		___digits_2 = value;
		Il2CppCodeGenWriteBarrier((&___digits_2), value);
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_sign_4() { return static_cast<int32_t>(offsetof(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539, ___sign_4)); }
	inline int32_t get_sign_4() const { return ___sign_4; }
	inline int32_t* get_address_of_sign_4() { return &___sign_4; }
	inline void set_sign_4(int32_t value)
	{
		___sign_4 = value;
	}
};

struct Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_StaticFields
{
public:
	// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::Zero
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 * ___Zero_0;
	// BigIntegerLibrary.Base10BigInteger BigIntegerLibrary.Base10BigInteger::One
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 * ___One_1;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_StaticFields, ___Zero_0)); }
	inline Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 * get_Zero_0() const { return ___Zero_0; }
	inline Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 ** get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 * value)
	{
		___Zero_0 = value;
		Il2CppCodeGenWriteBarrier((&___Zero_0), value);
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_StaticFields, ___One_1)); }
	inline Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 * get_One_1() const { return ___One_1; }
	inline Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 ** get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539 * value)
	{
		___One_1 = value;
		Il2CppCodeGenWriteBarrier((&___One_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASE10BIGINTEGER_TE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_H
#ifndef BIGINTEGER_TD18B67412A584801E54BE23A0A81A0C328384038_H
#define BIGINTEGER_TD18B67412A584801E54BE23A0A81A0C328384038_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BigIntegerLibrary.BigInteger
struct  BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038  : public RuntimeObject
{
public:
	// BigIntegerLibrary.BigInteger/DigitContainer BigIntegerLibrary.BigInteger::digits
	DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A * ___digits_4;
	// System.Int32 BigIntegerLibrary.BigInteger::size
	int32_t ___size_5;
	// BigIntegerLibrary.Sign BigIntegerLibrary.BigInteger::sign
	int32_t ___sign_6;

public:
	inline static int32_t get_offset_of_digits_4() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038, ___digits_4)); }
	inline DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A * get_digits_4() const { return ___digits_4; }
	inline DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A ** get_address_of_digits_4() { return &___digits_4; }
	inline void set_digits_4(DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A * value)
	{
		___digits_4 = value;
		Il2CppCodeGenWriteBarrier((&___digits_4), value);
	}

	inline static int32_t get_offset_of_size_5() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038, ___size_5)); }
	inline int32_t get_size_5() const { return ___size_5; }
	inline int32_t* get_address_of_size_5() { return &___size_5; }
	inline void set_size_5(int32_t value)
	{
		___size_5 = value;
	}

	inline static int32_t get_offset_of_sign_6() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038, ___sign_6)); }
	inline int32_t get_sign_6() const { return ___sign_6; }
	inline int32_t* get_address_of_sign_6() { return &___sign_6; }
	inline void set_sign_6(int32_t value)
	{
		___sign_6 = value;
	}
};

struct BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields
{
public:
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Zero
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * ___Zero_0;
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::One
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * ___One_1;
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Two
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * ___Two_2;
	// BigIntegerLibrary.BigInteger BigIntegerLibrary.BigInteger::Ten
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * ___Ten_3;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields, ___Zero_0)); }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * get_Zero_0() const { return ___Zero_0; }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 ** get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * value)
	{
		___Zero_0 = value;
		Il2CppCodeGenWriteBarrier((&___Zero_0), value);
	}

	inline static int32_t get_offset_of_One_1() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields, ___One_1)); }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * get_One_1() const { return ___One_1; }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 ** get_address_of_One_1() { return &___One_1; }
	inline void set_One_1(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * value)
	{
		___One_1 = value;
		Il2CppCodeGenWriteBarrier((&___One_1), value);
	}

	inline static int32_t get_offset_of_Two_2() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields, ___Two_2)); }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * get_Two_2() const { return ___Two_2; }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 ** get_address_of_Two_2() { return &___Two_2; }
	inline void set_Two_2(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * value)
	{
		___Two_2 = value;
		Il2CppCodeGenWriteBarrier((&___Two_2), value);
	}

	inline static int32_t get_offset_of_Ten_3() { return static_cast<int32_t>(offsetof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields, ___Ten_3)); }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * get_Ten_3() const { return ___Ten_3; }
	inline BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 ** get_address_of_Ten_3() { return &___Ten_3; }
	inline void set_Ten_3(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038 * value)
	{
		___Ten_3 = value;
		Il2CppCodeGenWriteBarrier((&___Ten_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BIGINTEGER_TD18B67412A584801E54BE23A0A81A0C328384038_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef ARKITFACESUBSYSTEM_T24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_H
#define ARKITFACESUBSYSTEM_T24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitFaceSubsystem
struct  ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9  : public XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86
{
public:

public:
};

struct ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_StaticFields
{
public:
	// UnityEngine.XR.FaceSubsystem.XRFaceSubsystem UnityEngine.XR.ARKit.ARKitFaceSubsystem::s_CurrentInstance
	XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * ___s_CurrentInstance_7;

public:
	inline static int32_t get_offset_of_s_CurrentInstance_7() { return static_cast<int32_t>(offsetof(ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_StaticFields, ___s_CurrentInstance_7)); }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * get_s_CurrentInstance_7() const { return ___s_CurrentInstance_7; }
	inline XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 ** get_address_of_s_CurrentInstance_7() { return &___s_CurrentInstance_7; }
	inline void set_s_CurrentInstance_7(XRFaceSubsystem_tE0367EC4739D16FE0CA2820AAF6688D9ABA66B86 * value)
	{
		___s_CurrentInstance_7 = value;
		Il2CppCodeGenWriteBarrier((&___s_CurrentInstance_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARKITFACESUBSYSTEM_T24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_H
#ifndef XRFACEARKITBLENDSHAPECOEFFICIENT_T829BF667B33D5FF78CD88C48B4DA4A69076AB42E_H
#define XRFACEARKITBLENDSHAPECOEFFICIENT_T829BF667B33D5FF78CD88C48B4DA4A69076AB42E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.XRFaceArkitBlendShapeCoefficient
struct  XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E 
{
public:
	// UnityEngine.XR.ARKit.XRArkitBlendShapeLocation UnityEngine.XR.ARKit.XRFaceArkitBlendShapeCoefficient::m_ArkitBlendShapeLocation
	int32_t ___m_ArkitBlendShapeLocation_0;
	// System.Single UnityEngine.XR.ARKit.XRFaceArkitBlendShapeCoefficient::m_Coefficient
	float ___m_Coefficient_1;

public:
	inline static int32_t get_offset_of_m_ArkitBlendShapeLocation_0() { return static_cast<int32_t>(offsetof(XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E, ___m_ArkitBlendShapeLocation_0)); }
	inline int32_t get_m_ArkitBlendShapeLocation_0() const { return ___m_ArkitBlendShapeLocation_0; }
	inline int32_t* get_address_of_m_ArkitBlendShapeLocation_0() { return &___m_ArkitBlendShapeLocation_0; }
	inline void set_m_ArkitBlendShapeLocation_0(int32_t value)
	{
		___m_ArkitBlendShapeLocation_0 = value;
	}

	inline static int32_t get_offset_of_m_Coefficient_1() { return static_cast<int32_t>(offsetof(XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E, ___m_Coefficient_1)); }
	inline float get_m_Coefficient_1() const { return ___m_Coefficient_1; }
	inline float* get_address_of_m_Coefficient_1() { return &___m_Coefficient_1; }
	inline void set_m_Coefficient_1(float value)
	{
		___m_Coefficient_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACEARKITBLENDSHAPECOEFFICIENT_T829BF667B33D5FF78CD88C48B4DA4A69076AB42E_H
#ifndef XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#define XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.FaceSubsystem.XRFace
struct  XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246 
{
public:
	// UnityEngine.Experimental.XR.TrackableId UnityEngine.XR.FaceSubsystem.XRFace::m_TrackableId
	TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  ___m_TrackableId_0;
	// UnityEngine.Pose UnityEngine.XR.FaceSubsystem.XRFace::m_Pose
	Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  ___m_Pose_1;
	// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::m_WasUpdated
	int32_t ___m_WasUpdated_2;
	// System.Int32 UnityEngine.XR.FaceSubsystem.XRFace::m_IsTracked
	int32_t ___m_IsTracked_3;

public:
	inline static int32_t get_offset_of_m_TrackableId_0() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_TrackableId_0)); }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  get_m_TrackableId_0() const { return ___m_TrackableId_0; }
	inline TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B * get_address_of_m_TrackableId_0() { return &___m_TrackableId_0; }
	inline void set_m_TrackableId_0(TrackableId_tA539F57E82A04D410FE11E10ACC830CF7CD71F7B  value)
	{
		___m_TrackableId_0 = value;
	}

	inline static int32_t get_offset_of_m_Pose_1() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_Pose_1)); }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  get_m_Pose_1() const { return ___m_Pose_1; }
	inline Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29 * get_address_of_m_Pose_1() { return &___m_Pose_1; }
	inline void set_m_Pose_1(Pose_t2997DE3CB3863E4D78FCF42B46FC481818823F29  value)
	{
		___m_Pose_1 = value;
	}

	inline static int32_t get_offset_of_m_WasUpdated_2() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_WasUpdated_2)); }
	inline int32_t get_m_WasUpdated_2() const { return ___m_WasUpdated_2; }
	inline int32_t* get_address_of_m_WasUpdated_2() { return &___m_WasUpdated_2; }
	inline void set_m_WasUpdated_2(int32_t value)
	{
		___m_WasUpdated_2 = value;
	}

	inline static int32_t get_offset_of_m_IsTracked_3() { return static_cast<int32_t>(offsetof(XRFace_tD2DF3125FE693EE9DABE72A78349CB7F07A51246, ___m_IsTracked_3)); }
	inline int32_t get_m_IsTracked_3() const { return ___m_IsTracked_3; }
	inline int32_t* get_address_of_m_IsTracked_3() { return &___m_IsTracked_3; }
	inline void set_m_IsTracked_3(int32_t value)
	{
		___m_IsTracked_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XRFACE_TD2DF3125FE693EE9DABE72A78349CB7F07A51246_H
#ifndef MODE_T801C8B469C50D4CDE456CD441D27FD6F8B4426CD_H
#define MODE_T801C8B469C50D4CDE456CD441D27FD6F8B4426CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.QrCode.Internal.Mode
struct  Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD  : public RuntimeObject
{
public:
	// ZXing.QrCode.Internal.Mode/Names ZXing.QrCode.Internal.Mode::<Name>k__BackingField
	int32_t ___U3CNameU3Ek__BackingField_0;
	// System.Int32[] ZXing.QrCode.Internal.Mode::characterCountBitsForVersions
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___characterCountBitsForVersions_11;
	// System.Int32 ZXing.QrCode.Internal.Mode::<Bits>k__BackingField
	int32_t ___U3CBitsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD, ___U3CNameU3Ek__BackingField_0)); }
	inline int32_t get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(int32_t value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_characterCountBitsForVersions_11() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD, ___characterCountBitsForVersions_11)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_characterCountBitsForVersions_11() const { return ___characterCountBitsForVersions_11; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_characterCountBitsForVersions_11() { return &___characterCountBitsForVersions_11; }
	inline void set_characterCountBitsForVersions_11(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___characterCountBitsForVersions_11 = value;
		Il2CppCodeGenWriteBarrier((&___characterCountBitsForVersions_11), value);
	}

	inline static int32_t get_offset_of_U3CBitsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD, ___U3CBitsU3Ek__BackingField_12)); }
	inline int32_t get_U3CBitsU3Ek__BackingField_12() const { return ___U3CBitsU3Ek__BackingField_12; }
	inline int32_t* get_address_of_U3CBitsU3Ek__BackingField_12() { return &___U3CBitsU3Ek__BackingField_12; }
	inline void set_U3CBitsU3Ek__BackingField_12(int32_t value)
	{
		___U3CBitsU3Ek__BackingField_12 = value;
	}
};

struct Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields
{
public:
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::TERMINATOR
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___TERMINATOR_1;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::NUMERIC
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___NUMERIC_2;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::ALPHANUMERIC
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___ALPHANUMERIC_3;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::STRUCTURED_APPEND
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___STRUCTURED_APPEND_4;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::BYTE
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___BYTE_5;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::ECI
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___ECI_6;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::KANJI
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___KANJI_7;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::FNC1_FIRST_POSITION
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___FNC1_FIRST_POSITION_8;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::FNC1_SECOND_POSITION
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___FNC1_SECOND_POSITION_9;
	// ZXing.QrCode.Internal.Mode ZXing.QrCode.Internal.Mode::HANZI
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * ___HANZI_10;

public:
	inline static int32_t get_offset_of_TERMINATOR_1() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___TERMINATOR_1)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_TERMINATOR_1() const { return ___TERMINATOR_1; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_TERMINATOR_1() { return &___TERMINATOR_1; }
	inline void set_TERMINATOR_1(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___TERMINATOR_1 = value;
		Il2CppCodeGenWriteBarrier((&___TERMINATOR_1), value);
	}

	inline static int32_t get_offset_of_NUMERIC_2() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___NUMERIC_2)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_NUMERIC_2() const { return ___NUMERIC_2; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_NUMERIC_2() { return &___NUMERIC_2; }
	inline void set_NUMERIC_2(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___NUMERIC_2 = value;
		Il2CppCodeGenWriteBarrier((&___NUMERIC_2), value);
	}

	inline static int32_t get_offset_of_ALPHANUMERIC_3() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___ALPHANUMERIC_3)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_ALPHANUMERIC_3() const { return ___ALPHANUMERIC_3; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_ALPHANUMERIC_3() { return &___ALPHANUMERIC_3; }
	inline void set_ALPHANUMERIC_3(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___ALPHANUMERIC_3 = value;
		Il2CppCodeGenWriteBarrier((&___ALPHANUMERIC_3), value);
	}

	inline static int32_t get_offset_of_STRUCTURED_APPEND_4() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___STRUCTURED_APPEND_4)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_STRUCTURED_APPEND_4() const { return ___STRUCTURED_APPEND_4; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_STRUCTURED_APPEND_4() { return &___STRUCTURED_APPEND_4; }
	inline void set_STRUCTURED_APPEND_4(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___STRUCTURED_APPEND_4 = value;
		Il2CppCodeGenWriteBarrier((&___STRUCTURED_APPEND_4), value);
	}

	inline static int32_t get_offset_of_BYTE_5() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___BYTE_5)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_BYTE_5() const { return ___BYTE_5; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_BYTE_5() { return &___BYTE_5; }
	inline void set_BYTE_5(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___BYTE_5 = value;
		Il2CppCodeGenWriteBarrier((&___BYTE_5), value);
	}

	inline static int32_t get_offset_of_ECI_6() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___ECI_6)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_ECI_6() const { return ___ECI_6; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_ECI_6() { return &___ECI_6; }
	inline void set_ECI_6(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___ECI_6 = value;
		Il2CppCodeGenWriteBarrier((&___ECI_6), value);
	}

	inline static int32_t get_offset_of_KANJI_7() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___KANJI_7)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_KANJI_7() const { return ___KANJI_7; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_KANJI_7() { return &___KANJI_7; }
	inline void set_KANJI_7(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___KANJI_7 = value;
		Il2CppCodeGenWriteBarrier((&___KANJI_7), value);
	}

	inline static int32_t get_offset_of_FNC1_FIRST_POSITION_8() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___FNC1_FIRST_POSITION_8)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_FNC1_FIRST_POSITION_8() const { return ___FNC1_FIRST_POSITION_8; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_FNC1_FIRST_POSITION_8() { return &___FNC1_FIRST_POSITION_8; }
	inline void set_FNC1_FIRST_POSITION_8(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___FNC1_FIRST_POSITION_8 = value;
		Il2CppCodeGenWriteBarrier((&___FNC1_FIRST_POSITION_8), value);
	}

	inline static int32_t get_offset_of_FNC1_SECOND_POSITION_9() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___FNC1_SECOND_POSITION_9)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_FNC1_SECOND_POSITION_9() const { return ___FNC1_SECOND_POSITION_9; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_FNC1_SECOND_POSITION_9() { return &___FNC1_SECOND_POSITION_9; }
	inline void set_FNC1_SECOND_POSITION_9(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___FNC1_SECOND_POSITION_9 = value;
		Il2CppCodeGenWriteBarrier((&___FNC1_SECOND_POSITION_9), value);
	}

	inline static int32_t get_offset_of_HANZI_10() { return static_cast<int32_t>(offsetof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields, ___HANZI_10)); }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * get_HANZI_10() const { return ___HANZI_10; }
	inline Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD ** get_address_of_HANZI_10() { return &___HANZI_10; }
	inline void set_HANZI_10(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD * value)
	{
		___HANZI_10 = value;
		Il2CppCodeGenWriteBarrier((&___HANZI_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T801C8B469C50D4CDE456CD441D27FD6F8B4426CD_H
#ifndef RESULT_T7343A2FB36C7950CDDB138EF0E885EE5CBF7102E_H
#define RESULT_T7343A2FB36C7950CDDB138EF0E885EE5CBF7102E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.Result
struct  Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E  : public RuntimeObject
{
public:
	// System.String ZXing.Result::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_0;
	// System.Byte[] ZXing.Result::<RawBytes>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CRawBytesU3Ek__BackingField_1;
	// ZXing.ResultPoint[] ZXing.Result::<ResultPoints>k__BackingField
	ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* ___U3CResultPointsU3Ek__BackingField_2;
	// ZXing.BarcodeFormat ZXing.Result::<BarcodeFormat>k__BackingField
	int32_t ___U3CBarcodeFormatU3Ek__BackingField_3;
	// System.Collections.Generic.IDictionary`2<ZXing.ResultMetadataType,System.Object> ZXing.Result::<ResultMetadata>k__BackingField
	RuntimeObject* ___U3CResultMetadataU3Ek__BackingField_4;
	// System.Int64 ZXing.Result::<Timestamp>k__BackingField
	int64_t ___U3CTimestampU3Ek__BackingField_5;
	// System.Int32 ZXing.Result::<NumBits>k__BackingField
	int32_t ___U3CNumBitsU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CTextU3Ek__BackingField_0)); }
	inline String_t* get_U3CTextU3Ek__BackingField_0() const { return ___U3CTextU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_0() { return &___U3CTextU3Ek__BackingField_0; }
	inline void set_U3CTextU3Ek__BackingField_0(String_t* value)
	{
		___U3CTextU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRawBytesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CRawBytesU3Ek__BackingField_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CRawBytesU3Ek__BackingField_1() const { return ___U3CRawBytesU3Ek__BackingField_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CRawBytesU3Ek__BackingField_1() { return &___U3CRawBytesU3Ek__BackingField_1; }
	inline void set_U3CRawBytesU3Ek__BackingField_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CRawBytesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRawBytesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResultPointsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CResultPointsU3Ek__BackingField_2)); }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* get_U3CResultPointsU3Ek__BackingField_2() const { return ___U3CResultPointsU3Ek__BackingField_2; }
	inline ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C** get_address_of_U3CResultPointsU3Ek__BackingField_2() { return &___U3CResultPointsU3Ek__BackingField_2; }
	inline void set_U3CResultPointsU3Ek__BackingField_2(ResultPointU5BU5D_t7AA26698F2F42BD89512FA4F3E8FE3145CEC630C* value)
	{
		___U3CResultPointsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultPointsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CBarcodeFormatU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CBarcodeFormatU3Ek__BackingField_3)); }
	inline int32_t get_U3CBarcodeFormatU3Ek__BackingField_3() const { return ___U3CBarcodeFormatU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CBarcodeFormatU3Ek__BackingField_3() { return &___U3CBarcodeFormatU3Ek__BackingField_3; }
	inline void set_U3CBarcodeFormatU3Ek__BackingField_3(int32_t value)
	{
		___U3CBarcodeFormatU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CResultMetadataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CResultMetadataU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CResultMetadataU3Ek__BackingField_4() const { return ___U3CResultMetadataU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CResultMetadataU3Ek__BackingField_4() { return &___U3CResultMetadataU3Ek__BackingField_4; }
	inline void set_U3CResultMetadataU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CResultMetadataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResultMetadataU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTimestampU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CTimestampU3Ek__BackingField_5)); }
	inline int64_t get_U3CTimestampU3Ek__BackingField_5() const { return ___U3CTimestampU3Ek__BackingField_5; }
	inline int64_t* get_address_of_U3CTimestampU3Ek__BackingField_5() { return &___U3CTimestampU3Ek__BackingField_5; }
	inline void set_U3CTimestampU3Ek__BackingField_5(int64_t value)
	{
		___U3CTimestampU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CNumBitsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E, ___U3CNumBitsU3Ek__BackingField_6)); }
	inline int32_t get_U3CNumBitsU3Ek__BackingField_6() const { return ___U3CNumBitsU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CNumBitsU3Ek__BackingField_6() { return &___U3CNumBitsU3Ek__BackingField_6; }
	inline void set_U3CNumBitsU3Ek__BackingField_6(int32_t value)
	{
		___U3CNumBitsU3Ek__BackingField_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULT_T7343A2FB36C7950CDDB138EF0E885EE5CBF7102E_H
#ifndef DELEGATEXRFACEANCHORADDED_T4C37182B2443CE6D65E2C406B6BC527FF22E7268_H
#define DELEGATEXRFACEANCHORADDED_T4C37182B2443CE6D65E2C406B6BC527FF22E7268_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitFaceSubsystem/DelegateXrFaceAnchorAdded
struct  DelegateXrFaceAnchorAdded_t4C37182B2443CE6D65E2C406B6BC527FF22E7268  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEXRFACEANCHORADDED_T4C37182B2443CE6D65E2C406B6BC527FF22E7268_H
#ifndef DELEGATEXRFACEANCHORREMOVED_T38731B8AEB3696EC1281AFE0D0E165C3C18C4BB4_H
#define DELEGATEXRFACEANCHORREMOVED_T38731B8AEB3696EC1281AFE0D0E165C3C18C4BB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitFaceSubsystem/DelegateXrFaceAnchorRemoved
struct  DelegateXrFaceAnchorRemoved_t38731B8AEB3696EC1281AFE0D0E165C3C18C4BB4  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEXRFACEANCHORREMOVED_T38731B8AEB3696EC1281AFE0D0E165C3C18C4BB4_H
#ifndef DELEGATEXRFACEANCHORUPDATED_TCBBB53FC1132F3A2AAC5E0C1E3E6CA34E65F8C7C_H
#define DELEGATEXRFACEANCHORUPDATED_TCBBB53FC1132F3A2AAC5E0C1E3E6CA34E65F8C7C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitFaceSubsystem/DelegateXrFaceAnchorUpdated
struct  DelegateXrFaceAnchorUpdated_tCBBB53FC1132F3A2AAC5E0C1E3E6CA34E65F8C7C  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEXRFACEANCHORUPDATED_TCBBB53FC1132F3A2AAC5E0C1E3E6CA34E65F8C7C_H
#ifndef DELEGATEXRFACESESSIONBEGINFRAME_T03BCAADC9955F8AF986BD37858554A2BB5503D62_H
#define DELEGATEXRFACESESSIONBEGINFRAME_T03BCAADC9955F8AF986BD37858554A2BB5503D62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARKitFaceSubsystem/DelegateXrFaceSessionBeginFrame
struct  DelegateXrFaceSessionBeginFrame_t03BCAADC9955F8AF986BD37858554A2BB5503D62  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DELEGATEXRFACESESSIONBEGINFRAME_T03BCAADC9955F8AF986BD37858554A2BB5503D62_H
#ifndef ONASYNCCONVERSIONCOMPLETEDELEGATE_T0713AF22D6B429530D6D08158EA74E6A447B2D5B_H
#define ONASYNCCONVERSIONCOMPLETEDELEGATE_T0713AF22D6B429530D6D08158EA74E6A447B2D5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARKit.ARWorldMapSessionExtensions/OnAsyncConversionCompleteDelegate
struct  OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONASYNCCONVERSIONCOMPLETEDELEGATE_T0713AF22D6B429530D6D08158EA74E6A447B2D5B_H
#ifndef RESULTPOINTCALLBACK_T7B6C4C8FC76F24D1640A246B9B2AF75477B989F3_H
#define RESULTPOINTCALLBACK_T7B6C4C8FC76F24D1640A246B9B2AF75477B989F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZXing.ResultPointCallback
struct  ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESULTPOINTCALLBACK_T7B6C4C8FC76F24D1640A246B9B2AF75477B989F3_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6400 = { sizeof (ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167)+ sizeof (RuntimeObject), sizeof(ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167 ), 0, 0 };
extern const int32_t g_FieldOffsetTable6400[1] = 
{
	ARWorldMapRequest_t4BCB51645BFFB333AC6DDEE3D8AE757BE3B8C167::get_offset_of_m_RequestId_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6401 = { sizeof (ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6401[8] = 
{
	ARWorldMapRequestStatus_tF71BE763C5F9644F3D7377ACE110F7FFBBE3D5DC::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6402 = { sizeof (ARWorldMapRequestStatusExtensions_t8FC86F2BC224C9CCC808FF3B4610B22BE5F8051F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6403 = { sizeof (ARWorldMapSessionExtensions_tBBC8030654182B90E45AEBB62C1B2A34DBC0C145), -1, sizeof(ARWorldMapSessionExtensions_tBBC8030654182B90E45AEBB62C1B2A34DBC0C145_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6403[1] = 
{
	ARWorldMapSessionExtensions_tBBC8030654182B90E45AEBB62C1B2A34DBC0C145_StaticFields::get_offset_of_s_OnAsyncWorldMapCompleted_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6404 = { sizeof (OnAsyncConversionCompleteDelegate_t0713AF22D6B429530D6D08158EA74E6A447B2D5B), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6405 = { sizeof (U3CModuleU3E_t1CF7C307ED677A54A6DD437A7514266A3972F693), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6406 = { sizeof (ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9), -1, sizeof(ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6406[1] = 
{
	ARKitFaceSubsystem_t24E01CF6C590EFAC9B51F7B1D341C646A69E9CD9_StaticFields::get_offset_of_s_CurrentInstance_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6407 = { sizeof (DelegateXrFaceAnchorAdded_t4C37182B2443CE6D65E2C406B6BC527FF22E7268), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6408 = { sizeof (DelegateXrFaceAnchorUpdated_tCBBB53FC1132F3A2AAC5E0C1E3E6CA34E65F8C7C), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6409 = { sizeof (DelegateXrFaceAnchorRemoved_t38731B8AEB3696EC1281AFE0D0E165C3C18C4BB4), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6410 = { sizeof (DelegateXrFaceSessionBeginFrame_t03BCAADC9955F8AF986BD37858554A2BB5503D62), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6411 = { sizeof (XRArkitBlendShapeLocation_t2F4D95E87BAFF0BB690B931B8C91D77859D34C28)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6411[53] = 
{
	XRArkitBlendShapeLocation_t2F4D95E87BAFF0BB690B931B8C91D77859D34C28::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6412 = { sizeof (XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E)+ sizeof (RuntimeObject), sizeof(XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E ), 0, 0 };
extern const int32_t g_FieldOffsetTable6412[2] = 
{
	XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E::get_offset_of_m_ArkitBlendShapeLocation_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	XRFaceArkitBlendShapeCoefficient_t829BF667B33D5FF78CD88C48B4DA4A69076AB42E::get_offset_of_m_Coefficient_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6413 = { sizeof (U3CModuleU3E_t03C365C1B1F3344459E0514A186E8BC92EDBCDAD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6414 = { sizeof (Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539), -1, sizeof(Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6414[5] = 
{
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_StaticFields::get_offset_of_Zero_0(),
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539_StaticFields::get_offset_of_One_1(),
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539::get_offset_of_digits_2(),
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539::get_offset_of_size_3(),
	Base10BigInteger_tE9C3EFEBCD5C4BA80BC347E0BAEC7BABCBAF5539::get_offset_of_sign_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6415 = { sizeof (DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6415[1] = 
{
	DigitContainer_tDDB682D7845C79E7F18C0CAC83399527EBD0F707::get_offset_of_digits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6416 = { sizeof (BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038), -1, sizeof(BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6416[7] = 
{
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields::get_offset_of_Zero_0(),
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields::get_offset_of_One_1(),
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields::get_offset_of_Two_2(),
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038_StaticFields::get_offset_of_Ten_3(),
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038::get_offset_of_digits_4(),
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038::get_offset_of_size_5(),
	BigInteger_tD18B67412A584801E54BE23A0A81A0C328384038::get_offset_of_sign_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6417 = { sizeof (DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6417[1] = 
{
	DigitContainer_t297A710854E4D48D2B51D89683C6F4C9E708DC8A::get_offset_of_digits_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6418 = { sizeof (BigIntegerException_t031C834FFDAD6CF18A708914DB5603E69C6004C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6419 = { sizeof (Sign_t210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6419[3] = 
{
	Sign_t210F2FABC6751BDC7B1258B3DFD1E5B92B479B1C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6420 = { sizeof (BarcodeFormat_t5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6420[22] = 
{
	BarcodeFormat_t5E09075B3F0913C2A4BDAD4259D3B044B2A08B5A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6421 = { sizeof (BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224), -1, sizeof(BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6421[2] = 
{
	BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224_StaticFields::get_offset_of_defaultCreateLuminanceSource_10(),
	BarcodeReader_tEC59FAC295BC5A75121E2CF723C04E0748921224::get_offset_of_createLuminanceSource_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6422 = { sizeof (U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D), -1, sizeof(U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6422[1] = 
{
	U3CU3Ec_tF6E2B20621E8C00D347F3DA0F2C9AD5065B7B45D_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6423 = { sizeof (BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75), -1, sizeof(BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6423[10] = 
{
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75_StaticFields::get_offset_of_defaultCreateBinarizer_0(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75_StaticFields::get_offset_of_defaultCreateRGBLuminanceSource_1(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_reader_2(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_createRGBLuminanceSource_3(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_createBinarizer_4(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_usePreviousState_5(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_options_6(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_ResultFound_7(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_U3CAutoRotateU3Ek__BackingField_8(),
	BarcodeReaderGeneric_t2282D9A3ED758D48E77D5D04C27A71753A574D75::get_offset_of_U3CTryInvertedU3Ek__BackingField_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6424 = { sizeof (U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647), -1, sizeof(U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6424[1] = 
{
	U3CU3Ec_t20F8339FCCCE1D1A9B29A47187C8DAD2C35E3647_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6425 = { sizeof (BaseLuminanceSource_t8CA757494571D978084F5027CC520F1FDA9C3163), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6425[1] = 
{
	BaseLuminanceSource_t8CA757494571D978084F5027CC520F1FDA9C3163::get_offset_of_luminances_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6426 = { sizeof (Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6426[1] = 
{
	Binarizer_t5D0E2F63D754FE8B0F6C1EC6B20AB8710A257309::get_offset_of_source_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6427 = { sizeof (BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6427[2] = 
{
	BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461::get_offset_of_binarizer_0(),
	BinaryBitmap_tCF98378D7F884CDFBC4985A0ACCFFBE31FC51461::get_offset_of_matrix_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6428 = { sizeof (DecodeHintType_t6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6428[16] = 
{
	DecodeHintType_t6E6E8BFCAD6A36A06B3F9F189A9E4FDFB47B5ECF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6429 = { sizeof (EncodeHintType_t8CFD0EAB2657FA9E27B05AAE65F320921789E8AA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6429[18] = 
{
	EncodeHintType_t8CFD0EAB2657FA9E27B05AAE65F320921789E8AA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6430 = { sizeof (FormatException_t182FE700C0A582FDF3C3CCE9C3E14576DF372617), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6431 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6432 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6433 = { sizeof (InvertedLuminanceSource_tD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6433[2] = 
{
	InvertedLuminanceSource_tD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01::get_offset_of_delegate_2(),
	InvertedLuminanceSource_tD7403987A2A7A5E07C5F770B89B5D5EDC8D32B01::get_offset_of_invertedMatrix_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6434 = { sizeof (LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6434[2] = 
{
	LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1::get_offset_of_width_0(),
	LuminanceSource_t0F427CE4A5D082F5EDE0C5C61B5629D5D966C2C1::get_offset_of_height_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6435 = { sizeof (MultiFormatReader_tDC3266BB3E38302717ABCB100E1C320E5C15FB34), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6435[2] = 
{
	MultiFormatReader_tDC3266BB3E38302717ABCB100E1C320E5C15FB34::get_offset_of_hints_0(),
	MultiFormatReader_tDC3266BB3E38302717ABCB100E1C320E5C15FB34::get_offset_of_readers_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6436 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6437 = { sizeof (ReaderException_t0B65A1A4C77C6490166DF90D1206CD113D9F75E8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6438 = { sizeof (Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6438[7] = 
{
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CTextU3Ek__BackingField_0(),
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CRawBytesU3Ek__BackingField_1(),
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CResultPointsU3Ek__BackingField_2(),
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CBarcodeFormatU3Ek__BackingField_3(),
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CResultMetadataU3Ek__BackingField_4(),
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CTimestampU3Ek__BackingField_5(),
	Result_t7343A2FB36C7950CDDB138EF0E885EE5CBF7102E::get_offset_of_U3CNumBitsU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6439 = { sizeof (ResultMetadataType_t0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6439[13] = 
{
	ResultMetadataType_t0AA9EC240B63DEF714ED0DE2128ED6A2DA14D569::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6440 = { sizeof (ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6440[5] = 
{
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166::get_offset_of_x_0(),
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166::get_offset_of_y_1(),
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166::get_offset_of_bytesX_2(),
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166::get_offset_of_bytesY_3(),
	ResultPoint_tB5E62D12630394F9102A06C86C7FBF293B269166::get_offset_of_toString_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6441 = { sizeof (ResultPointCallback_t7B6C4C8FC76F24D1640A246B9B2AF75477B989F3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6442 = { sizeof (RGBLuminanceSource_t2ABCDCB40ECC28EE75CAA94F0783FFEB47675D93), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6443 = { sizeof (BitmapFormat_t7FC1576E756C5343AD09912793E9FA7F31C2B198)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6443[14] = 
{
	BitmapFormat_t7FC1576E756C5343AD09912793E9FA7F31C2B198::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6444 = { sizeof (SupportClass_t619C93DCCD87C08548C4DE2BA5498814BC8AF87A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6445 = { sizeof (Color32LuminanceSource_t656B988B56E74F7D098C0917EA3BE66A9163A3AA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6446 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6447 = { sizeof (WriterException_t64A2D5441667D3429DEDF25ED6A3D371568904B2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6448 = { sizeof (QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C), -1, sizeof(QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6448[2] = 
{
	QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C_StaticFields::get_offset_of_NO_POINTS_0(),
	QRCodeReader_t662C5686AF3F1EC13E0D5812A1268901A31F801C::get_offset_of_decoder_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6449 = { sizeof (QRCodeWriter_t0E2F30CBC804571278B1EF03034085F53F9E09D7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6450 = { sizeof (BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6450[4] = 
{
	BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636::get_offset_of_bitMatrix_0(),
	BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636::get_offset_of_parsedVersion_1(),
	BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636::get_offset_of_parsedFormatInfo_2(),
	BitMatrixParser_tC677F7EC7B28F2505C839D0C90549BAF02167636::get_offset_of_mirrored_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6451 = { sizeof (DataBlock_tB039200C1595B1F47D5D85D439D6BB26C9528166), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6451[2] = 
{
	DataBlock_tB039200C1595B1F47D5D85D439D6BB26C9528166::get_offset_of_numDataCodewords_0(),
	DataBlock_tB039200C1595B1F47D5D85D439D6BB26C9528166::get_offset_of_codewords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6452 = { sizeof (DataMask_tFDFB5566B35D29D6309AEE024C03BD19942D18EF), -1, sizeof(DataMask_tFDFB5566B35D29D6309AEE024C03BD19942D18EF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6452[1] = 
{
	DataMask_tFDFB5566B35D29D6309AEE024C03BD19942D18EF_StaticFields::get_offset_of_DATA_MASKS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6453 = { sizeof (U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783), -1, sizeof(U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6453[1] = 
{
	U3CU3Ec_tE866C1CD845B436CA1AA94ED90C86D1BC441E783_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6454 = { sizeof (DecodedBitStreamParser_t3CD6F4F82581779102F82AE6B53DB3C70103E1AA), -1, sizeof(DecodedBitStreamParser_t3CD6F4F82581779102F82AE6B53DB3C70103E1AA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6454[1] = 
{
	DecodedBitStreamParser_t3CD6F4F82581779102F82AE6B53DB3C70103E1AA_StaticFields::get_offset_of_ALPHANUMERIC_CHARS_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6455 = { sizeof (Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6455[1] = 
{
	Decoder_t726229B65302EC26E07C22FCFD9B35767A06408D::get_offset_of_rsDecoder_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6456 = { sizeof (ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D), -1, sizeof(ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6456[8] = 
{
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields::get_offset_of_L_0(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields::get_offset_of_M_1(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields::get_offset_of_Q_2(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields::get_offset_of_H_3(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D_StaticFields::get_offset_of_FOR_BITS_4(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D::get_offset_of_bits_5(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D::get_offset_of_ordinal_Renamed_Field_6(),
	ErrorCorrectionLevel_tAB0FC09D0924CA5E82A980C3E283119C7D8AB71D::get_offset_of_name_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6457 = { sizeof (FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26), -1, sizeof(FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6457[4] = 
{
	FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_StaticFields::get_offset_of_FORMAT_INFO_DECODE_LOOKUP_0(),
	FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26_StaticFields::get_offset_of_BITS_SET_IN_HALF_BYTE_1(),
	FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26::get_offset_of_errorCorrectionLevel_2(),
	FormatInformation_tECCD723FCCF88FED4B5EF7C17AE1831447A5CD26::get_offset_of_dataMask_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6458 = { sizeof (Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD), -1, sizeof(Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6458[13] = 
{
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_TERMINATOR_1(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_NUMERIC_2(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_ALPHANUMERIC_3(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_STRUCTURED_APPEND_4(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_BYTE_5(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_ECI_6(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_KANJI_7(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_FNC1_FIRST_POSITION_8(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_FNC1_SECOND_POSITION_9(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD_StaticFields::get_offset_of_HANZI_10(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD::get_offset_of_characterCountBitsForVersions_11(),
	Mode_t801C8B469C50D4CDE456CD441D27FD6F8B4426CD::get_offset_of_U3CBitsU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6459 = { sizeof (Names_t3971C3CE518B30C64CF847091748A25B28B0D9C9)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6459[11] = 
{
	Names_t3971C3CE518B30C64CF847091748A25B28B0D9C9::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6460 = { sizeof (QRCodeDecoderMetaData_tBB3F521268CFC674E2F944505529CED41B0B9FC0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6460[1] = 
{
	QRCodeDecoderMetaData_tBB3F521268CFC674E2F944505529CED41B0B9FC0::get_offset_of_mirrored_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6461 = { sizeof (Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1), -1, sizeof(Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6461[6] = 
{
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1_StaticFields::get_offset_of_VERSION_DECODE_INFO_0(),
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1_StaticFields::get_offset_of_VERSIONS_1(),
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1::get_offset_of_versionNumber_2(),
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1::get_offset_of_alignmentPatternCenters_3(),
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1::get_offset_of_ecBlocks_4(),
	Version_tD151A57660A82DB744C62D59CFCBA720AAE775D1::get_offset_of_totalCodewords_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6462 = { sizeof (ECBlocks_t974AF513E12A611B900BFA1CB3679332F0E70DF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6462[2] = 
{
	ECBlocks_t974AF513E12A611B900BFA1CB3679332F0E70DF9::get_offset_of_ecCodewordsPerBlock_0(),
	ECBlocks_t974AF513E12A611B900BFA1CB3679332F0E70DF9::get_offset_of_ecBlocks_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6463 = { sizeof (ECB_tA7C4D638E18E1F839A13E41A9F4AE866FF41A452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6463[2] = 
{
	ECB_tA7C4D638E18E1F839A13E41A9F4AE866FF41A452::get_offset_of_count_0(),
	ECB_tA7C4D638E18E1F839A13E41A9F4AE866FF41A452::get_offset_of_dataCodewords_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6464 = { sizeof (AlignmentPattern_t9BE63C52A771F31D2B57BA097960F8FDB86588C4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6464[1] = 
{
	AlignmentPattern_t9BE63C52A771F31D2B57BA097960F8FDB86588C4::get_offset_of_estimatedModuleSize_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6465 = { sizeof (AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6465[9] = 
{
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_image_0(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_possibleCenters_1(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_startX_2(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_startY_3(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_width_4(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_height_5(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_moduleSize_6(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_crossCheckStateCount_7(),
	AlignmentPatternFinder_t0A38D743D75B2F9A1D6EC6E9C47A16AB667E186B::get_offset_of_resultPointCallback_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6466 = { sizeof (Detector_tBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6466[2] = 
{
	Detector_tBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E::get_offset_of_image_0(),
	Detector_tBD5F13C804B4BB7B9CF5B69AC3A6FAD6BA3B2F7E::get_offset_of_resultPointCallback_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6467 = { sizeof (FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6467[2] = 
{
	FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE::get_offset_of_estimatedModuleSize_5(),
	FinderPattern_tE3AB02775E6BCC56D11BF83BED8316D490329DBE::get_offset_of_count_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6468 = { sizeof (FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6468[5] = 
{
	FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2::get_offset_of_image_0(),
	FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2::get_offset_of_possibleCenters_1(),
	FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2::get_offset_of_hasSkipped_2(),
	FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2::get_offset_of_crossCheckStateCount_3(),
	FinderPatternFinder_tA313F18B8DC0DF2C252836117C550E6132BB1FF2::get_offset_of_resultPointCallback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6469 = { sizeof (FurthestFromAverageComparator_tED7292884E68C755936E18FCBD940CECDD3EBD8B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6469[1] = 
{
	FurthestFromAverageComparator_tED7292884E68C755936E18FCBD940CECDD3EBD8B::get_offset_of_average_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6470 = { sizeof (CenterComparator_t15EA58584F0F461CD3CEAC3568F23992A1612256), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6470[1] = 
{
	CenterComparator_t15EA58584F0F461CD3CEAC3568F23992A1612256::get_offset_of_average_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6471 = { sizeof (FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6471[3] = 
{
	FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945::get_offset_of_bottomLeft_0(),
	FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945::get_offset_of_topLeft_1(),
	FinderPatternInfo_tF90C418EFA55463C411DC3D50EFE0FD64F4CD945::get_offset_of_topRight_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6472 = { sizeof (BlockPair_t883A80F1975479C09CCF8E5B17C460F85638B627), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6472[2] = 
{
	BlockPair_t883A80F1975479C09CCF8E5B17C460F85638B627::get_offset_of_dataBytes_0(),
	BlockPair_t883A80F1975479C09CCF8E5B17C460F85638B627::get_offset_of_errorCorrectionBytes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6473 = { sizeof (ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6473[3] = 
{
	ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8::get_offset_of_bytes_0(),
	ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8::get_offset_of_width_1(),
	ByteMatrix_tF9DC0A40ACF26CA79F43048903B06E555B91B0F8::get_offset_of_height_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6474 = { sizeof (Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5), -1, sizeof(Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6474[2] = 
{
	Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_StaticFields::get_offset_of_ALPHANUMERIC_TABLE_0(),
	Encoder_tE63B7EB26A6FEE1B30E501AF1936F21BDA02E7D5_StaticFields::get_offset_of_DEFAULT_BYTE_MODE_ENCODING_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6475 = { sizeof (MaskUtil_tCD316E65E1A74F24EBE3B0E1A9E165F6829D9409), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6476 = { sizeof (MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D), -1, sizeof(MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6476[4] = 
{
	MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields::get_offset_of_POSITION_DETECTION_PATTERN_0(),
	MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields::get_offset_of_POSITION_ADJUSTMENT_PATTERN_1(),
	MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields::get_offset_of_POSITION_ADJUSTMENT_PATTERN_COORDINATE_TABLE_2(),
	MatrixUtil_tE13307CA93A76F507F8CBE25D71D0B0B959F316D_StaticFields::get_offset_of_TYPE_INFO_COORDINATES_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6477 = { sizeof (QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B), -1, sizeof(QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6477[6] = 
{
	QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B_StaticFields::get_offset_of_NUM_MASK_PATTERNS_0(),
	QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B::get_offset_of_U3CModeU3Ek__BackingField_1(),
	QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B::get_offset_of_U3CECLevelU3Ek__BackingField_2(),
	QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B::get_offset_of_U3CVersionU3Ek__BackingField_3(),
	QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B::get_offset_of_U3CMaskPatternU3Ek__BackingField_4(),
	QRCode_tD603DF4ACFB98D2888F453012084047B0FBB7B0B::get_offset_of_U3CMatrixU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6478 = { sizeof (PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32), -1, sizeof(PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6478[11] = 
{
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_INVALID_CODEWORD_0(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_NUMBER_OF_CODEWORDS_1(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_MAX_CODEWORDS_IN_BARCODE_2(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_MIN_ROWS_IN_BARCODE_3(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_MAX_ROWS_IN_BARCODE_4(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_MODULES_IN_CODEWORD_5(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_MODULES_IN_STOP_PATTERN_6(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_BARS_IN_MODULE_7(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_EMPTY_INT_ARRAY_8(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_SYMBOL_TABLE_9(),
	PDF417Common_tE0FC7FFB8A17297646426570713B605036D70C32_StaticFields::get_offset_of_CODEWORD_TABLE_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6479 = { sizeof (PDF417Reader_t5652665E03131800768344065D8FA07DEB44F66B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6480 = { sizeof (PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6480[4] = 
{
	PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F::get_offset_of_U3CSegmentIndexU3Ek__BackingField_0(),
	PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F::get_offset_of_U3CFileIdU3Ek__BackingField_1(),
	PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F::get_offset_of_U3COptionalDataU3Ek__BackingField_2(),
	PDF417ResultMetadata_t9608B7F614ECACA21BF737740E526A4E0FAD930F::get_offset_of_U3CIsLastSegmentU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6481 = { sizeof (BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6481[5] = 
{
	BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A::get_offset_of_U3CColumnCountU3Ek__BackingField_0(),
	BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A::get_offset_of_U3CErrorCorrectionLevelU3Ek__BackingField_1(),
	BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A::get_offset_of_U3CRowCountUpperU3Ek__BackingField_2(),
	BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A::get_offset_of_U3CRowCountLowerU3Ek__BackingField_3(),
	BarcodeMetadata_tF3EEE8FE516C577460E35B108452BF404A24728A::get_offset_of_U3CRowCountU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6482 = { sizeof (BarcodeValue_t33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6482[1] = 
{
	BarcodeValue_t33288DA91DD68D97EBA5A214CDAD35FCFF4BBA6F::get_offset_of_values_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6483 = { sizeof (BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6483[9] = 
{
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_image_0(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CTopLeftU3Ek__BackingField_1(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CTopRightU3Ek__BackingField_2(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CBottomLeftU3Ek__BackingField_3(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CBottomRightU3Ek__BackingField_4(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CMinXU3Ek__BackingField_5(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CMaxXU3Ek__BackingField_6(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CMinYU3Ek__BackingField_7(),
	BoundingBox_t08AA711EC82C223AA81B9069390385E08A355D56::get_offset_of_U3CMaxYU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6484 = { sizeof (Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974), -1, sizeof(Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6484[6] = 
{
	Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974_StaticFields::get_offset_of_BARCODE_ROW_UNKNOWN_0(),
	Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974::get_offset_of_U3CStartXU3Ek__BackingField_1(),
	Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974::get_offset_of_U3CEndXU3Ek__BackingField_2(),
	Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974::get_offset_of_U3CBucketU3Ek__BackingField_3(),
	Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974::get_offset_of_U3CValueU3Ek__BackingField_4(),
	Codeword_tD90F903011EE5D063DD3BA2A4D90E6449F688974::get_offset_of_U3CRowNumberU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6485 = { sizeof (DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980), -1, sizeof(DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6485[3] = 
{
	DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields::get_offset_of_PUNCT_CHARS_0(),
	DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields::get_offset_of_MIXED_CHARS_1(),
	DecodedBitStreamParser_t0CE552A81A342DE8A2F2C99F8F6FEE84D785E980_StaticFields::get_offset_of_EXP900_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6486 = { sizeof (Mode_tDA24C1E76C9ADDB731E06F476107059880B93EA3)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6486[7] = 
{
	Mode_tDA24C1E76C9ADDB731E06F476107059880B93EA3::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6487 = { sizeof (DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6487[4] = 
{
	DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E::get_offset_of_U3CMetadataU3Ek__BackingField_0(),
	DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E::get_offset_of_U3CDetectionResultColumnsU3Ek__BackingField_1(),
	DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E::get_offset_of_U3CBoxU3Ek__BackingField_2(),
	DetectionResult_t6582585457566AD9445F4C08102CB688440BB54E::get_offset_of_U3CColumnCountU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6488 = { sizeof (DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6488[2] = 
{
	DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3::get_offset_of_U3CBoxU3Ek__BackingField_0(),
	DetectionResultColumn_tE9087A29135703042A8D509F68778284E6F46EF3::get_offset_of_U3CCodewordsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6489 = { sizeof (DetectionResultRowIndicatorColumn_tFE78E0F364DAAB13092052C991910C850F6B26BA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6489[1] = 
{
	DetectionResultRowIndicatorColumn_tFE78E0F364DAAB13092052C991910C850F6B26BA::get_offset_of_U3CIsLeftU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6490 = { sizeof (PDF417CodewordDecoder_tD708BEB97EF160311AE2C6DA2140650FA5A9E665), -1, sizeof(PDF417CodewordDecoder_tD708BEB97EF160311AE2C6DA2140650FA5A9E665_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6490[1] = 
{
	PDF417CodewordDecoder_tD708BEB97EF160311AE2C6DA2140650FA5A9E665_StaticFields::get_offset_of_RATIOS_TABLE_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6491 = { sizeof (PDF417ScanningDecoder_t62267070A08A1CD992B20CD982CF7F3E068C300C), -1, sizeof(PDF417ScanningDecoder_t62267070A08A1CD992B20CD982CF7F3E068C300C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6491[1] = 
{
	PDF417ScanningDecoder_t62267070A08A1CD992B20CD982CF7F3E068C300C_StaticFields::get_offset_of_errorCorrection_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6492 = { sizeof (Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E), -1, sizeof(Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6492[4] = 
{
	Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields::get_offset_of_INDEXES_START_PATTERN_0(),
	Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields::get_offset_of_INDEXES_STOP_PATTERN_1(),
	Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields::get_offset_of_START_PATTERN_2(),
	Detector_tC3C9DEF66D12EF6A5869D91A688D9997865D167E_StaticFields::get_offset_of_STOP_PATTERN_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6493 = { sizeof (PDF417DetectorResult_t1EA56AD672F48F88BF30365C6D1A053369E6B514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6493[2] = 
{
	PDF417DetectorResult_t1EA56AD672F48F88BF30365C6D1A053369E6B514::get_offset_of_U3CBitsU3Ek__BackingField_0(),
	PDF417DetectorResult_t1EA56AD672F48F88BF30365C6D1A053369E6B514::get_offset_of_U3CPointsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6494 = { sizeof (PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595), -1, sizeof(PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6494[5] = 
{
	PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields::get_offset_of_TEXT_MIXED_RAW_0(),
	PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields::get_offset_of_TEXT_PUNCTUATION_RAW_1(),
	PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields::get_offset_of_MIXED_2(),
	PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields::get_offset_of_PUNCTUATION_3(),
	PDF417HighLevelEncoder_tB57B785770A4EA26D37AA70EFFB548704A7D5595_StaticFields::get_offset_of_DEFAULT_ENCODING_NAME_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6495 = { sizeof (ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6495[1] = 
{
	ErrorCorrection_t3D0F6DF238C51631A755DB5CCB1629E9613E28B7::get_offset_of_field_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6496 = { sizeof (ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2), -1, sizeof(ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6496[6] = 
{
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2_StaticFields::get_offset_of_PDF417_GF_0(),
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2::get_offset_of_expTable_1(),
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2::get_offset_of_logTable_2(),
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2::get_offset_of_U3CZeroU3Ek__BackingField_3(),
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2::get_offset_of_U3COneU3Ek__BackingField_4(),
	ModulusGF_t51A31A9598800B20BDBB473FB376AE23CAD8D2F2::get_offset_of_modulus_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6497 = { sizeof (ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6497[2] = 
{
	ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E::get_offset_of_field_0(),
	ModulusPoly_t440945E805EC44D0CEED65FABFFA405C8482B31E::get_offset_of_coefficients_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6498 = { sizeof (CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC), -1, sizeof(CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6498[8] = 
{
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields::get_offset_of_MAX_ACCEPTABLE_2(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields::get_offset_of_PADDING_3(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields::get_offset_of_ALPHABET_4(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields::get_offset_of_CHARACTER_ENCODINGS_5(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC_StaticFields::get_offset_of_STARTEND_ENCODING_6(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC::get_offset_of_decodeRowResult_7(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC::get_offset_of_counters_8(),
	CodaBarReader_tA7AB4DC416E54075745D7AB53B7344FA1DAE91BC::get_offset_of_counterLength_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6499 = { sizeof (Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C), -1, sizeof(Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6499[3] = 
{
	Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields::get_offset_of_CODE_PATTERNS_2(),
	Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields::get_offset_of_MAX_AVG_VARIANCE_3(),
	Code128Reader_t279C9EDF71B08071A4E0DA45EE1A888D014C409C_StaticFields::get_offset_of_MAX_INDIVIDUAL_VARIANCE_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
