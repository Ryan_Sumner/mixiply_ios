﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// MPAR.AssetLoader.IAssets.SoundAsset
struct SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11;
// MPAR.AssetLoader.IAssets.VideoAsset
struct VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A;
// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader
struct IPlatformDependentAssetLoader_t4191A469DA1A9ECC9F8F2F7E37DAF82A3B2DBB2E;
// MPAR.Common.Collections.ObjectCollection
struct ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB;
// MPAR.Common.Entities.HologramData
struct HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9;
// MPAR.Common.Entities.HologramData[]
struct HologramDataU5BU5D_t444FCA3DFA3FF771A13845C3574D9A4296C3C8DB;
// MPAR.Common.GameObjects.ExhibitBox
struct ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A;
// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory
struct ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13;
// MPAR.Common.GameObjects.Factories.PortableMenuFactory
struct PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D;
// MPAR.Common.GameObjects.Factories.PortableMenuItemFactory
struct PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D;
// MPAR.Common.GameObjects.Factories.ScriptPageFactory
struct ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344;
// MPAR.Common.GameObjects.Factories.VideoControlsFactory
struct VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6;
// MPAR.Common.GameObjects.Utility.PrefabUtility
struct PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2;
// MPAR.Common.Input.IGameObjectClickListener
struct IGameObjectClickListener_tB81337C0A40E48B4BB7DCDF5EB2F552F271806D4;
// MPAR.Common.Input.IPlatformDependentInput
struct IPlatformDependentInput_t257971A94452FDB854A26995CA0C13950AE0FF70;
// MPAR.Common.Input.Utility.GestureManager
struct GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909;
// MPAR.Common.Persistence.Anchor
struct Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A;
// MPAR.Common.Persistence.CacheManager
struct CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B;
// MPAR.Common.Persistence.IPlatformDependentAnchorManager
struct IPlatformDependentAnchorManager_tE48A8D2E8F48E1BE9F313FD03CC6D3798785CB1E;
// MPAR.Common.Scripting.API.Objects.MixiplyObject
struct MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B;
// MPAR.Common.Scripting.API.Objects.Vector
struct Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207;
// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass1_0
struct U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4;
// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44;
// MPAR.Common.UI.IPlatformDependentMenu
struct IPlatformDependentMenu_tA672B9C6B89D3534056F68BAD92F194958F6AE7D;
// MPAR.Common.UI.IPlatformDependentScriptMenu
struct IPlatformDependentScriptMenu_tBBB6BBD89701461F2C659808C67378235B3D1AA0;
// MPAR.Common.UI.PortableMenu.PortableMenu
struct PortableMenu_t1464619C66BEEE803774636017C9557890D6327B;
// MPAR.Common.UI.PortableMenu.PortableMenuItem
struct PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867;
// MPAR.Common.Utility.Interpolator
struct Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E;
// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_0
struct U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520;
// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_1
struct U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<MPAR.Common.AssetLoader.IAsset>
struct Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject>>
struct Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.HashSet`1<MPAR.Common.UI.PortableMenu.PortableMenuItem>
struct HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533;
// System.Collections.Generic.IEnumerator`1<System.String>
struct IEnumerator_1_tE14471B9BA58E22CC2B0F85AA521BEBB7F04E004;
// System.Collections.Generic.List`1<MPAR.Common.Input.IGameObjectClickListener>
struct List_1_t5BB3C8E8E3C6307C5ED37D3E4521E35F0A1366B9;
// System.Collections.Generic.List`1<MPAR.Common.Scripting.Loader.FileGuidRef>
struct List_1_t2A9593FC04B87AC1C4F1AB3B4879CC4ACA6FED73;
// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script>
struct List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.EventHandler`1<MPAR.Common.Input.Utility.ManipulationDeltaEventArgs>
struct EventHandler_1_tD38F5C2F93D4125643F8527AB5BC06F9748AB8A3;
// System.EventHandler`1<MPAR.Common.UI.EventActionArgs>
struct EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Func`2<UnityEngine.Collider,UnityEngine.GameObject>
struct Func_2_t6B4B7AF0BB8E2D432CB1AFD435341742F85376AC;
// System.Func`2<UnityEngine.EventSystems.RaycastResult,System.Boolean>
struct Func_2_t2FF4E83B7220C4808010133EAC6C531BA7F906D0;
// System.Func`2<UnityEngine.Material,System.Boolean>
struct Func_2_tD9E9003345ACB766DDD09BC2730CAE2FC3463635;
// System.Func`2<UnityEngine.Renderer,UnityEngine.Material>
struct Func_2_t588206846C9EF87E419BFAADF4BA341F2F1E6817;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// TMPro.TextMeshPro
struct TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2;
// UnityEngine.AudioClip
struct AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051;
// UnityEngine.AudioSource
struct AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C;
// UnityEngine.BoxCollider
struct BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.UI.GraphicRaycaster
struct GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.Video.VideoPlayer
struct VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2;
// UnityEngine.WWW
struct WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef SOUNDASSET_TAEE27F320584677F41B40DBB483365DB9332FF11_H
#define SOUNDASSET_TAEE27F320584677F41B40DBB483365DB9332FF11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.AssetLoader.IAssets.SoundAsset
struct  SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11  : public RuntimeObject
{
public:
	// UnityEngine.AudioClip MPAR.AssetLoader.IAssets.SoundAsset::<AudioClip>k__BackingField
	AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * ___U3CAudioClipU3Ek__BackingField_0;
	// System.String MPAR.AssetLoader.IAssets.SoundAsset::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CAudioClipU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11, ___U3CAudioClipU3Ek__BackingField_0)); }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * get_U3CAudioClipU3Ek__BackingField_0() const { return ___U3CAudioClipU3Ek__BackingField_0; }
	inline AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 ** get_address_of_U3CAudioClipU3Ek__BackingField_0() { return &___U3CAudioClipU3Ek__BackingField_0; }
	inline void set_U3CAudioClipU3Ek__BackingField_0(AudioClip_tCC3C35F579203CE2601243585AB3D6953C3BA051 * value)
	{
		___U3CAudioClipU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAudioClipU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SOUNDASSET_TAEE27F320584677F41B40DBB483365DB9332FF11_H
#ifndef U3CLOADU3ED__10_T25BE1F4F27DF36B8288802065357A43DA1A1DD31_H
#define U3CLOADU3ED__10_T25BE1F4F27DF36B8288802065357A43DA1A1DD31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.AssetLoader.IAssets.SoundAsset/<Load>d__10
struct  U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31  : public RuntimeObject
{
public:
	// System.Int32 MPAR.AssetLoader.IAssets.SoundAsset/<Load>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.AssetLoader.IAssets.SoundAsset/<Load>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.AssetLoader.IAssets.SoundAsset MPAR.AssetLoader.IAssets.SoundAsset/<Load>d__10::<>4__this
	SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 * ___U3CU3E4__this_2;
	// System.String MPAR.AssetLoader.IAssets.SoundAsset/<Load>d__10::path
	String_t* ___path_3;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.AssetLoader.IAssets.SoundAsset/<Load>d__10::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31, ___U3CU3E4__this_2)); }
	inline SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31, ___callback_4)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_4() const { return ___callback_4; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__10_T25BE1F4F27DF36B8288802065357A43DA1A1DD31_H
#ifndef U3CLOADCOROUTINEU3ED__12_T156F2DCE7FB94D04E831B4A2790A223F63A430B8_H
#define U3CLOADCOROUTINEU3ED__12_T156F2DCE7FB94D04E831B4A2790A223F63A430B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.AssetLoader.IAssets.SoundAsset/<LoadCoroutine>d__12
struct  U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8  : public RuntimeObject
{
public:
	// System.Int32 MPAR.AssetLoader.IAssets.SoundAsset/<LoadCoroutine>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.AssetLoader.IAssets.SoundAsset/<LoadCoroutine>d__12::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.String MPAR.AssetLoader.IAssets.SoundAsset/<LoadCoroutine>d__12::path
	String_t* ___path_2;
	// MPAR.AssetLoader.IAssets.SoundAsset MPAR.AssetLoader.IAssets.SoundAsset/<LoadCoroutine>d__12::<>4__this
	SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 * ___U3CU3E4__this_3;
	// UnityEngine.WWW MPAR.AssetLoader.IAssets.SoundAsset/<LoadCoroutine>d__12::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_path_2() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8, ___path_2)); }
	inline String_t* get_path_2() const { return ___path_2; }
	inline String_t** get_address_of_path_2() { return &___path_2; }
	inline void set_path_2(String_t* value)
	{
		___path_2 = value;
		Il2CppCodeGenWriteBarrier((&___path_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8, ___U3CU3E4__this_3)); }
	inline SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8, ___U3CwwwU3E5__2_4)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_4() const { return ___U3CwwwU3E5__2_4; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_4() { return &___U3CwwwU3E5__2_4; }
	inline void set_U3CwwwU3E5__2_4(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCOROUTINEU3ED__12_T156F2DCE7FB94D04E831B4A2790A223F63A430B8_H
#ifndef U3CLOADU3ED__20_TEC765CA18A9DD0716930DB0E3575545F168C875C_H
#define U3CLOADU3ED__20_TEC765CA18A9DD0716930DB0E3575545F168C875C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.AssetLoader.IAssets.VideoAsset/<Load>d__20
struct  U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C  : public RuntimeObject
{
public:
	// System.Int32 MPAR.AssetLoader.IAssets.VideoAsset/<Load>d__20::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.AssetLoader.IAssets.VideoAsset/<Load>d__20::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.AssetLoader.IAssets.VideoAsset MPAR.AssetLoader.IAssets.VideoAsset/<Load>d__20::<>4__this
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A * ___U3CU3E4__this_2;
	// System.String MPAR.AssetLoader.IAssets.VideoAsset/<Load>d__20::path
	String_t* ___path_3;
	// System.Action`1<MPAR.Common.AssetLoader.IAsset> MPAR.AssetLoader.IAssets.VideoAsset/<Load>d__20::callback
	Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * ___callback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C, ___U3CU3E4__this_2)); }
	inline VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_path_3() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C, ___path_3)); }
	inline String_t* get_path_3() const { return ___path_3; }
	inline String_t** get_address_of_path_3() { return &___path_3; }
	inline void set_path_3(String_t* value)
	{
		___path_3 = value;
		Il2CppCodeGenWriteBarrier((&___path_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C, ___callback_4)); }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * get_callback_4() const { return ___callback_4; }
	inline Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_1_tB10097BA4917302F8F192F7FDED0F9BC19266A22 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADU3ED__20_TEC765CA18A9DD0716930DB0E3575545F168C875C_H
#ifndef U3CLOADVIDEOU3ED__23_TA75497F5D50A8444941F162D1A4838E0FDCAEACD_H
#define U3CLOADVIDEOU3ED__23_TA75497F5D50A8444941F162D1A4838E0FDCAEACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.AssetLoader.IAssets.VideoAsset/<LoadVideo>d__23
struct  U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD  : public RuntimeObject
{
public:
	// System.Int32 MPAR.AssetLoader.IAssets.VideoAsset/<LoadVideo>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.AssetLoader.IAssets.VideoAsset/<LoadVideo>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.AssetLoader.IAssets.VideoAsset MPAR.AssetLoader.IAssets.VideoAsset/<LoadVideo>d__23::<>4__this
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD, ___U3CU3E4__this_2)); }
	inline VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADVIDEOU3ED__23_TA75497F5D50A8444941F162D1A4838E0FDCAEACD_H
#ifndef CONFIG_T907A4A74290AFDE6CE4567874473D26415967929_H
#define CONFIG_T907A4A74290AFDE6CE4567874473D26415967929_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Controllers.Config
struct  Config_t907A4A74290AFDE6CE4567874473D26415967929  : public RuntimeObject
{
public:

public:
};

struct Config_t907A4A74290AFDE6CE4567874473D26415967929_StaticFields
{
public:
	// System.String MPAR.Common.Controllers.Config::BaseUrl
	String_t* ___BaseUrl_0;

public:
	inline static int32_t get_offset_of_BaseUrl_0() { return static_cast<int32_t>(offsetof(Config_t907A4A74290AFDE6CE4567874473D26415967929_StaticFields, ___BaseUrl_0)); }
	inline String_t* get_BaseUrl_0() const { return ___BaseUrl_0; }
	inline String_t** get_address_of_BaseUrl_0() { return &___BaseUrl_0; }
	inline void set_BaseUrl_0(String_t* value)
	{
		___BaseUrl_0 = value;
		Il2CppCodeGenWriteBarrier((&___BaseUrl_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIG_T907A4A74290AFDE6CE4567874473D26415967929_H
#ifndef HOLOGRAMDATA_T8A6628915B2D60864E68D360E64DA1F9BE9A58D9_H
#define HOLOGRAMDATA_T8A6628915B2D60864E68D360E64DA1F9BE9A58D9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Entities.HologramData
struct  HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Entities.HologramData::id
	String_t* ___id_0;
	// System.Single MPAR.Common.Entities.HologramData::xPosition
	float ___xPosition_1;
	// System.Single MPAR.Common.Entities.HologramData::yPosition
	float ___yPosition_2;
	// System.Single MPAR.Common.Entities.HologramData::zPosition
	float ___zPosition_3;
	// System.Single MPAR.Common.Entities.HologramData::xRotation
	float ___xRotation_4;
	// System.Single MPAR.Common.Entities.HologramData::yRotation
	float ___yRotation_5;
	// System.Single MPAR.Common.Entities.HologramData::zRotation
	float ___zRotation_6;
	// System.Single MPAR.Common.Entities.HologramData::xScale
	float ___xScale_7;
	// System.Single MPAR.Common.Entities.HologramData::yScale
	float ___yScale_8;
	// System.Single MPAR.Common.Entities.HologramData::zScale
	float ___zScale_9;

public:
	inline static int32_t get_offset_of_id_0() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___id_0)); }
	inline String_t* get_id_0() const { return ___id_0; }
	inline String_t** get_address_of_id_0() { return &___id_0; }
	inline void set_id_0(String_t* value)
	{
		___id_0 = value;
		Il2CppCodeGenWriteBarrier((&___id_0), value);
	}

	inline static int32_t get_offset_of_xPosition_1() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___xPosition_1)); }
	inline float get_xPosition_1() const { return ___xPosition_1; }
	inline float* get_address_of_xPosition_1() { return &___xPosition_1; }
	inline void set_xPosition_1(float value)
	{
		___xPosition_1 = value;
	}

	inline static int32_t get_offset_of_yPosition_2() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___yPosition_2)); }
	inline float get_yPosition_2() const { return ___yPosition_2; }
	inline float* get_address_of_yPosition_2() { return &___yPosition_2; }
	inline void set_yPosition_2(float value)
	{
		___yPosition_2 = value;
	}

	inline static int32_t get_offset_of_zPosition_3() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___zPosition_3)); }
	inline float get_zPosition_3() const { return ___zPosition_3; }
	inline float* get_address_of_zPosition_3() { return &___zPosition_3; }
	inline void set_zPosition_3(float value)
	{
		___zPosition_3 = value;
	}

	inline static int32_t get_offset_of_xRotation_4() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___xRotation_4)); }
	inline float get_xRotation_4() const { return ___xRotation_4; }
	inline float* get_address_of_xRotation_4() { return &___xRotation_4; }
	inline void set_xRotation_4(float value)
	{
		___xRotation_4 = value;
	}

	inline static int32_t get_offset_of_yRotation_5() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___yRotation_5)); }
	inline float get_yRotation_5() const { return ___yRotation_5; }
	inline float* get_address_of_yRotation_5() { return &___yRotation_5; }
	inline void set_yRotation_5(float value)
	{
		___yRotation_5 = value;
	}

	inline static int32_t get_offset_of_zRotation_6() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___zRotation_6)); }
	inline float get_zRotation_6() const { return ___zRotation_6; }
	inline float* get_address_of_zRotation_6() { return &___zRotation_6; }
	inline void set_zRotation_6(float value)
	{
		___zRotation_6 = value;
	}

	inline static int32_t get_offset_of_xScale_7() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___xScale_7)); }
	inline float get_xScale_7() const { return ___xScale_7; }
	inline float* get_address_of_xScale_7() { return &___xScale_7; }
	inline void set_xScale_7(float value)
	{
		___xScale_7 = value;
	}

	inline static int32_t get_offset_of_yScale_8() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___yScale_8)); }
	inline float get_yScale_8() const { return ___yScale_8; }
	inline float* get_address_of_yScale_8() { return &___yScale_8; }
	inline void set_yScale_8(float value)
	{
		___yScale_8 = value;
	}

	inline static int32_t get_offset_of_zScale_9() { return static_cast<int32_t>(offsetof(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9, ___zScale_9)); }
	inline float get_zScale_9() const { return ___zScale_9; }
	inline float* get_address_of_zScale_9() { return &___zScale_9; }
	inline void set_zScale_9(float value)
	{
		___zScale_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOGRAMDATA_T8A6628915B2D60864E68D360E64DA1F9BE9A58D9_H
#ifndef SCENEDATA_TDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94_H
#define SCENEDATA_TDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Entities.SceneData
struct  SceneData_tDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Entities.SceneData::centerWorldAnchorName
	String_t* ___centerWorldAnchorName_0;
	// MPAR.Common.Entities.HologramData[] MPAR.Common.Entities.SceneData::holograms
	HologramDataU5BU5D_t444FCA3DFA3FF771A13845C3574D9A4296C3C8DB* ___holograms_1;

public:
	inline static int32_t get_offset_of_centerWorldAnchorName_0() { return static_cast<int32_t>(offsetof(SceneData_tDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94, ___centerWorldAnchorName_0)); }
	inline String_t* get_centerWorldAnchorName_0() const { return ___centerWorldAnchorName_0; }
	inline String_t** get_address_of_centerWorldAnchorName_0() { return &___centerWorldAnchorName_0; }
	inline void set_centerWorldAnchorName_0(String_t* value)
	{
		___centerWorldAnchorName_0 = value;
		Il2CppCodeGenWriteBarrier((&___centerWorldAnchorName_0), value);
	}

	inline static int32_t get_offset_of_holograms_1() { return static_cast<int32_t>(offsetof(SceneData_tDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94, ___holograms_1)); }
	inline HologramDataU5BU5D_t444FCA3DFA3FF771A13845C3574D9A4296C3C8DB* get_holograms_1() const { return ___holograms_1; }
	inline HologramDataU5BU5D_t444FCA3DFA3FF771A13845C3574D9A4296C3C8DB** get_address_of_holograms_1() { return &___holograms_1; }
	inline void set_holograms_1(HologramDataU5BU5D_t444FCA3DFA3FF771A13845C3574D9A4296C3C8DB* value)
	{
		___holograms_1 = value;
		Il2CppCodeGenWriteBarrier((&___holograms_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEDATA_TDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94_H
#ifndef U3CU3EC_TAA5386B539E4B7CA134E15C55C6062891613B2B2_H
#define U3CU3EC_TAA5386B539E4B7CA134E15C55C6062891613B2B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.EditHandle/<>c
struct  U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2_StaticFields
{
public:
	// MPAR.Common.GameObjects.EditHandle/<>c MPAR.Common.GameObjects.EditHandle/<>c::<>9
	U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Collider,UnityEngine.GameObject> MPAR.Common.GameObjects.EditHandle/<>c::<>9__26_0
	Func_2_t6B4B7AF0BB8E2D432CB1AFD435341742F85376AC * ___U3CU3E9__26_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__26_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2_StaticFields, ___U3CU3E9__26_0_1)); }
	inline Func_2_t6B4B7AF0BB8E2D432CB1AFD435341742F85376AC * get_U3CU3E9__26_0_1() const { return ___U3CU3E9__26_0_1; }
	inline Func_2_t6B4B7AF0BB8E2D432CB1AFD435341742F85376AC ** get_address_of_U3CU3E9__26_0_1() { return &___U3CU3E9__26_0_1; }
	inline void set_U3CU3E9__26_0_1(Func_2_t6B4B7AF0BB8E2D432CB1AFD435341742F85376AC * value)
	{
		___U3CU3E9__26_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__26_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TAA5386B539E4B7CA134E15C55C6062891613B2B2_H
#ifndef U3CU3EC__DISPLAYCLASS26_0_TFA3904F923F73553B470AC6F744107C95EBCE625_H
#define U3CU3EC__DISPLAYCLASS26_0_TFA3904F923F73553B470AC6F744107C95EBCE625_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.EditHandle/<>c__DisplayClass26_0
struct  U3CU3Ec__DisplayClass26_0_tFA3904F923F73553B470AC6F744107C95EBCE625  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MPAR.Common.GameObjects.EditHandle/<>c__DisplayClass26_0::hitObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hitObject_0;

public:
	inline static int32_t get_offset_of_hitObject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass26_0_tFA3904F923F73553B470AC6F744107C95EBCE625, ___hitObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hitObject_0() const { return ___hitObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hitObject_0() { return &___hitObject_0; }
	inline void set_hitObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hitObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___hitObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS26_0_TFA3904F923F73553B470AC6F744107C95EBCE625_H
#ifndef GAMEOBJECTUTILITY_TBD9E90EDD43468EE35579EFC7712B7D7B3E5738B_H
#define GAMEOBJECTUTILITY_TBD9E90EDD43468EE35579EFC7712B7D7B3E5738B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility
struct  GameObjectUtility_tBD9E90EDD43468EE35579EFC7712B7D7B3E5738B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTUTILITY_TBD9E90EDD43468EE35579EFC7712B7D7B3E5738B_H
#ifndef U3CSCALESIDEWAYSFROMTOU3ED__4_T73C28E9B6597C7276FFD8F7C9096E5905044AB6D_H
#define U3CSCALESIDEWAYSFROMTOU3ED__4_T73C28E9B6597C7276FFD8F7C9096E5905044AB6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4
struct  U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Transform MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::objectToScale
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objectToScale_2;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::xFrom
	float ___xFrom_3;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::xTo
	float ___xTo_4;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::speed
	float ___speed_5;
	// System.Action MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__4::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_objectToScale_2() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___objectToScale_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objectToScale_2() const { return ___objectToScale_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objectToScale_2() { return &___objectToScale_2; }
	inline void set_objectToScale_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objectToScale_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectToScale_2), value);
	}

	inline static int32_t get_offset_of_xFrom_3() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___xFrom_3)); }
	inline float get_xFrom_3() const { return ___xFrom_3; }
	inline float* get_address_of_xFrom_3() { return &___xFrom_3; }
	inline void set_xFrom_3(float value)
	{
		___xFrom_3 = value;
	}

	inline static int32_t get_offset_of_xTo_4() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___xTo_4)); }
	inline float get_xTo_4() const { return ___xTo_4; }
	inline float* get_address_of_xTo_4() { return &___xTo_4; }
	inline void set_xTo_4(float value)
	{
		___xTo_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D, ___callback_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_6() const { return ___callback_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALESIDEWAYSFROMTOU3ED__4_T73C28E9B6597C7276FFD8F7C9096E5905044AB6D_H
#ifndef U3CSCALESIDEWAYSFROMTOU3ED__5_TB67DD25745103FB64743632BB0448766C4EABBD3_H
#define U3CSCALESIDEWAYSFROMTOU3ED__5_TB67DD25745103FB64743632BB0448766C4EABBD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5
struct  U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::speed
	float ___speed_2;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::xFrom
	float ___xFrom_3;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::xTo
	float ___xTo_4;
	// UnityEngine.Transform MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::objectToScale
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objectToScale_5;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::<step>5__2
	float ___U3CstepU3E5__2_6;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleSidewaysFromTo>d__5::<t>5__3
	float ___U3CtU3E5__3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_xFrom_3() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___xFrom_3)); }
	inline float get_xFrom_3() const { return ___xFrom_3; }
	inline float* get_address_of_xFrom_3() { return &___xFrom_3; }
	inline void set_xFrom_3(float value)
	{
		___xFrom_3 = value;
	}

	inline static int32_t get_offset_of_xTo_4() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___xTo_4)); }
	inline float get_xTo_4() const { return ___xTo_4; }
	inline float* get_address_of_xTo_4() { return &___xTo_4; }
	inline void set_xTo_4(float value)
	{
		___xTo_4 = value;
	}

	inline static int32_t get_offset_of_objectToScale_5() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___objectToScale_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objectToScale_5() const { return ___objectToScale_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objectToScale_5() { return &___objectToScale_5; }
	inline void set_objectToScale_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objectToScale_5 = value;
		Il2CppCodeGenWriteBarrier((&___objectToScale_5), value);
	}

	inline static int32_t get_offset_of_U3CstepU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___U3CstepU3E5__2_6)); }
	inline float get_U3CstepU3E5__2_6() const { return ___U3CstepU3E5__2_6; }
	inline float* get_address_of_U3CstepU3E5__2_6() { return &___U3CstepU3E5__2_6; }
	inline void set_U3CstepU3E5__2_6(float value)
	{
		___U3CstepU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3, ___U3CtU3E5__3_7)); }
	inline float get_U3CtU3E5__3_7() const { return ___U3CtU3E5__3_7; }
	inline float* get_address_of_U3CtU3E5__3_7() { return &___U3CtU3E5__3_7; }
	inline void set_U3CtU3E5__3_7(float value)
	{
		___U3CtU3E5__3_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALESIDEWAYSFROMTOU3ED__5_TB67DD25745103FB64743632BB0448766C4EABBD3_H
#ifndef ADJUSTMENTCONSTANTS_TD466F164137F79A88F079CF0738FBFFFD0D71BA4_H
#define ADJUSTMENTCONSTANTS_TD466F164137F79A88F079CF0738FBFFFD0D71BA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Utility.AdjustmentConstants
struct  AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4  : public RuntimeObject
{
public:

public:
};

struct AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields
{
public:
	// System.Single MPAR.Common.Input.Utility.AdjustmentConstants::RotateHandleMultiplier
	float ___RotateHandleMultiplier_0;
	// System.Single MPAR.Common.Input.Utility.AdjustmentConstants::TranslateHandleMultiplier
	float ___TranslateHandleMultiplier_1;
	// System.Single MPAR.Common.Input.Utility.AdjustmentConstants::ScaleHandleMultiplier
	float ___ScaleHandleMultiplier_2;

public:
	inline static int32_t get_offset_of_RotateHandleMultiplier_0() { return static_cast<int32_t>(offsetof(AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields, ___RotateHandleMultiplier_0)); }
	inline float get_RotateHandleMultiplier_0() const { return ___RotateHandleMultiplier_0; }
	inline float* get_address_of_RotateHandleMultiplier_0() { return &___RotateHandleMultiplier_0; }
	inline void set_RotateHandleMultiplier_0(float value)
	{
		___RotateHandleMultiplier_0 = value;
	}

	inline static int32_t get_offset_of_TranslateHandleMultiplier_1() { return static_cast<int32_t>(offsetof(AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields, ___TranslateHandleMultiplier_1)); }
	inline float get_TranslateHandleMultiplier_1() const { return ___TranslateHandleMultiplier_1; }
	inline float* get_address_of_TranslateHandleMultiplier_1() { return &___TranslateHandleMultiplier_1; }
	inline void set_TranslateHandleMultiplier_1(float value)
	{
		___TranslateHandleMultiplier_1 = value;
	}

	inline static int32_t get_offset_of_ScaleHandleMultiplier_2() { return static_cast<int32_t>(offsetof(AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields, ___ScaleHandleMultiplier_2)); }
	inline float get_ScaleHandleMultiplier_2() const { return ___ScaleHandleMultiplier_2; }
	inline float* get_address_of_ScaleHandleMultiplier_2() { return &___ScaleHandleMultiplier_2; }
	inline void set_ScaleHandleMultiplier_2(float value)
	{
		___ScaleHandleMultiplier_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADJUSTMENTCONSTANTS_TD466F164137F79A88F079CF0738FBFFFD0D71BA4_H
#ifndef U3CU3EC_T1911CBDE20C811C0D61136F31183C0E10ED51E18_H
#define U3CU3EC_T1911CBDE20C811C0D61136F31183C0E10ED51E18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Utility.GestureManager/<>c
struct  U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18_StaticFields
{
public:
	// MPAR.Common.Input.Utility.GestureManager/<>c MPAR.Common.Input.Utility.GestureManager/<>c::<>9
	U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.EventSystems.RaycastResult,System.Boolean> MPAR.Common.Input.Utility.GestureManager/<>c::<>9__28_0
	Func_2_t2FF4E83B7220C4808010133EAC6C531BA7F906D0 * ___U3CU3E9__28_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18_StaticFields, ___U3CU3E9__28_0_1)); }
	inline Func_2_t2FF4E83B7220C4808010133EAC6C531BA7F906D0 * get_U3CU3E9__28_0_1() const { return ___U3CU3E9__28_0_1; }
	inline Func_2_t2FF4E83B7220C4808010133EAC6C531BA7F906D0 ** get_address_of_U3CU3E9__28_0_1() { return &___U3CU3E9__28_0_1; }
	inline void set_U3CU3E9__28_0_1(Func_2_t2FF4E83B7220C4808010133EAC6C531BA7F906D0 * value)
	{
		___U3CU3E9__28_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1911CBDE20C811C0D61136F31183C0E10ED51E18_H
#ifndef SCALECONSTANTS_TF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_H
#define SCALECONSTANTS_TF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Utility.ScaleConstants
struct  ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F  : public RuntimeObject
{
public:

public:
};

struct ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_StaticFields
{
public:
	// System.Single MPAR.Common.Input.Utility.ScaleConstants::DefaultObjectSize
	float ___DefaultObjectSize_0;
	// System.Single MPAR.Common.Input.Utility.ScaleConstants::HandleDistanceScaleModifier
	float ___HandleDistanceScaleModifier_1;

public:
	inline static int32_t get_offset_of_DefaultObjectSize_0() { return static_cast<int32_t>(offsetof(ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_StaticFields, ___DefaultObjectSize_0)); }
	inline float get_DefaultObjectSize_0() const { return ___DefaultObjectSize_0; }
	inline float* get_address_of_DefaultObjectSize_0() { return &___DefaultObjectSize_0; }
	inline void set_DefaultObjectSize_0(float value)
	{
		___DefaultObjectSize_0 = value;
	}

	inline static int32_t get_offset_of_HandleDistanceScaleModifier_1() { return static_cast<int32_t>(offsetof(ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_StaticFields, ___HandleDistanceScaleModifier_1)); }
	inline float get_HandleDistanceScaleModifier_1() const { return ___HandleDistanceScaleModifier_1; }
	inline float* get_address_of_HandleDistanceScaleModifier_1() { return &___HandleDistanceScaleModifier_1; }
	inline void set_HandleDistanceScaleModifier_1(float value)
	{
		___HandleDistanceScaleModifier_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALECONSTANTS_TF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_H
#ifndef U3CU3EC_T6BF1A05C579673058690535A832897270841AAE8_H
#define U3CU3EC_T6BF1A05C579673058690535A832897270841AAE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.CacheManager/<>c
struct  U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields
{
public:
	// MPAR.Common.Persistence.CacheManager/<>c MPAR.Common.Persistence.CacheManager/<>c::<>9
	U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8 * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> MPAR.Common.Persistence.CacheManager/<>c::<>9__28_0
	Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * ___U3CU3E9__28_0_1;
	// System.Func`2<System.String,System.String> MPAR.Common.Persistence.CacheManager/<>c::<>9__28_1
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CU3E9__28_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields, ___U3CU3E9__28_0_1)); }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * get_U3CU3E9__28_0_1() const { return ___U3CU3E9__28_0_1; }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 ** get_address_of_U3CU3E9__28_0_1() { return &___U3CU3E9__28_0_1; }
	inline void set_U3CU3E9__28_0_1(Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * value)
	{
		___U3CU3E9__28_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__28_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields, ___U3CU3E9__28_1_2)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CU3E9__28_1_2() const { return ___U3CU3E9__28_1_2; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CU3E9__28_1_2() { return &___U3CU3E9__28_1_2; }
	inline void set_U3CU3E9__28_1_2(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CU3E9__28_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__28_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6BF1A05C579673058690535A832897270841AAE8_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T774F71FDC59FF558B8027AAEBE31ADE91DE0BF93_H
#define U3CU3EC__DISPLAYCLASS32_0_T774F71FDC59FF558B8027AAEBE31ADE91DE0BF93_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.CacheManager/<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t774F71FDC59FF558B8027AAEBE31ADE91DE0BF93  : public RuntimeObject
{
public:
	// System.String MPAR.Common.Persistence.CacheManager/<>c__DisplayClass32_0::filePath
	String_t* ___filePath_0;

public:
	inline static int32_t get_offset_of_filePath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t774F71FDC59FF558B8027AAEBE31ADE91DE0BF93, ___filePath_0)); }
	inline String_t* get_filePath_0() const { return ___filePath_0; }
	inline String_t** get_address_of_filePath_0() { return &___filePath_0; }
	inline void set_filePath_0(String_t* value)
	{
		___filePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___filePath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T774F71FDC59FF558B8027AAEBE31ADE91DE0BF93_H
#ifndef U3CCACHEFILEFROMURLU3ED__28_T3C3D32B597C726B027AED087E921C16A38C9C6F5_H
#define U3CCACHEFILEFROMURLU3ED__28_T3C3D32B597C726B027AED087E921C16A38C9C6F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28
struct  U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Persistence.CacheManager MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<>4__this
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * ___U3CU3E4__this_2;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::url
	String_t* ___url_3;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::extension
	String_t* ___extension_4;
	// UnityEngine.WWW MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<www>5__2
	WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * ___U3CwwwU3E5__2_5;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<baseUrl>5__3
	String_t* ___U3CbaseUrlU3E5__3_6;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<folder>5__4
	String_t* ___U3CfolderU3E5__4_7;
	// System.Collections.Generic.IEnumerator`1<System.String> MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<>7__wrap4
	RuntimeObject* ___U3CU3E7__wrap4_8;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<dependency>5__6
	String_t* ___U3CdependencyU3E5__6_9;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__28::<dependencyUrl>5__7
	String_t* ___U3CdependencyUrlU3E5__7_10;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CU3E4__this_2)); }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}

	inline static int32_t get_offset_of_extension_4() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___extension_4)); }
	inline String_t* get_extension_4() const { return ___extension_4; }
	inline String_t** get_address_of_extension_4() { return &___extension_4; }
	inline void set_extension_4(String_t* value)
	{
		___extension_4 = value;
		Il2CppCodeGenWriteBarrier((&___extension_4), value);
	}

	inline static int32_t get_offset_of_U3CwwwU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CwwwU3E5__2_5)); }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * get_U3CwwwU3E5__2_5() const { return ___U3CwwwU3E5__2_5; }
	inline WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 ** get_address_of_U3CwwwU3E5__2_5() { return &___U3CwwwU3E5__2_5; }
	inline void set_U3CwwwU3E5__2_5(WWW_tA50AFB5DE276783409B4CE88FE9B772322EE5664 * value)
	{
		___U3CwwwU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwwwU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CbaseUrlU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CbaseUrlU3E5__3_6)); }
	inline String_t* get_U3CbaseUrlU3E5__3_6() const { return ___U3CbaseUrlU3E5__3_6; }
	inline String_t** get_address_of_U3CbaseUrlU3E5__3_6() { return &___U3CbaseUrlU3E5__3_6; }
	inline void set_U3CbaseUrlU3E5__3_6(String_t* value)
	{
		___U3CbaseUrlU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CbaseUrlU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CfolderU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CfolderU3E5__4_7)); }
	inline String_t* get_U3CfolderU3E5__4_7() const { return ___U3CfolderU3E5__4_7; }
	inline String_t** get_address_of_U3CfolderU3E5__4_7() { return &___U3CfolderU3E5__4_7; }
	inline void set_U3CfolderU3E5__4_7(String_t* value)
	{
		___U3CfolderU3E5__4_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CfolderU3E5__4_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap4_8() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CU3E7__wrap4_8)); }
	inline RuntimeObject* get_U3CU3E7__wrap4_8() const { return ___U3CU3E7__wrap4_8; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap4_8() { return &___U3CU3E7__wrap4_8; }
	inline void set_U3CU3E7__wrap4_8(RuntimeObject* value)
	{
		___U3CU3E7__wrap4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap4_8), value);
	}

	inline static int32_t get_offset_of_U3CdependencyU3E5__6_9() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CdependencyU3E5__6_9)); }
	inline String_t* get_U3CdependencyU3E5__6_9() const { return ___U3CdependencyU3E5__6_9; }
	inline String_t** get_address_of_U3CdependencyU3E5__6_9() { return &___U3CdependencyU3E5__6_9; }
	inline void set_U3CdependencyU3E5__6_9(String_t* value)
	{
		___U3CdependencyU3E5__6_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdependencyU3E5__6_9), value);
	}

	inline static int32_t get_offset_of_U3CdependencyUrlU3E5__7_10() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5, ___U3CdependencyUrlU3E5__7_10)); }
	inline String_t* get_U3CdependencyUrlU3E5__7_10() const { return ___U3CdependencyUrlU3E5__7_10; }
	inline String_t** get_address_of_U3CdependencyUrlU3E5__7_10() { return &___U3CdependencyUrlU3E5__7_10; }
	inline void set_U3CdependencyUrlU3E5__7_10(String_t* value)
	{
		___U3CdependencyUrlU3E5__7_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CdependencyUrlU3E5__7_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCACHEFILEFROMURLU3ED__28_T3C3D32B597C726B027AED087E921C16A38C9C6F5_H
#ifndef U3CCACHEFILEFROMURLU3ED__30_TEF8952CC0E73EF175E18A97DBA0BF227D26D99B5_H
#define U3CCACHEFILEFROMURLU3ED__30_TEF8952CC0E73EF175E18A97DBA0BF227D26D99B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__30
struct  U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__30::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__30::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Persistence.CacheManager MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__30::<>4__this
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * ___U3CU3E4__this_2;
	// System.String MPAR.Common.Persistence.CacheManager/<CacheFileFromUrl>d__30::url
	String_t* ___url_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5, ___U3CU3E4__this_2)); }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_url_3() { return static_cast<int32_t>(offsetof(U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5, ___url_3)); }
	inline String_t* get_url_3() const { return ___url_3; }
	inline String_t** get_address_of_url_3() { return &___url_3; }
	inline void set_url_3(String_t* value)
	{
		___url_3 = value;
		Il2CppCodeGenWriteBarrier((&___url_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCACHEFILEFROMURLU3ED__30_TEF8952CC0E73EF175E18A97DBA0BF227D26D99B5_H
#ifndef CACHEREF_TA734049DBB5504EC88C8B92340D936C2C7B12CA2_H
#define CACHEREF_TA734049DBB5504EC88C8B92340D936C2C7B12CA2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.CacheRef
struct  CacheRef_tA734049DBB5504EC88C8B92340D936C2C7B12CA2  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Loader.FileGuidRef> MPAR.Common.Persistence.CacheRef::<FileGuidReferences>k__BackingField
	List_1_t2A9593FC04B87AC1C4F1AB3B4879CC4ACA6FED73 * ___U3CFileGuidReferencesU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CFileGuidReferencesU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(CacheRef_tA734049DBB5504EC88C8B92340D936C2C7B12CA2, ___U3CFileGuidReferencesU3Ek__BackingField_0)); }
	inline List_1_t2A9593FC04B87AC1C4F1AB3B4879CC4ACA6FED73 * get_U3CFileGuidReferencesU3Ek__BackingField_0() const { return ___U3CFileGuidReferencesU3Ek__BackingField_0; }
	inline List_1_t2A9593FC04B87AC1C4F1AB3B4879CC4ACA6FED73 ** get_address_of_U3CFileGuidReferencesU3Ek__BackingField_0() { return &___U3CFileGuidReferencesU3Ek__BackingField_0; }
	inline void set_U3CFileGuidReferencesU3Ek__BackingField_0(List_1_t2A9593FC04B87AC1C4F1AB3B4879CC4ACA6FED73 * value)
	{
		___U3CFileGuidReferencesU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFileGuidReferencesU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEREF_TA734049DBB5504EC88C8B92340D936C2C7B12CA2_H
#ifndef MENUTRANSITIONS_T9BC3AA8497BE1A5EB2CC3A49453589F6CFF0311D_H
#define MENUTRANSITIONS_T9BC3AA8497BE1A5EB2CC3A49453589F6CFF0311D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.Effects.MenuTransitions
struct  MenuTransitions_t9BC3AA8497BE1A5EB2CC3A49453589F6CFF0311D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUTRANSITIONS_T9BC3AA8497BE1A5EB2CC3A49453589F6CFF0311D_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T555D92B3BA25A958C33020FB594D2F6E11F1C387_H
#define U3CU3EC__DISPLAYCLASS0_0_T555D92B3BA25A958C33020FB594D2F6E11F1C387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387  : public RuntimeObject
{
public:
	// UnityEngine.GameObject MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass0_0::scriptPageObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___scriptPageObject_0;
	// MPAR.Common.GameObjects.Factories.ScriptPageFactory MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass0_0::factory
	ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344 * ___factory_1;
	// UnityEngine.GameObject MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass0_0::scriptMenuItem
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___scriptMenuItem_2;
	// MPAR.Common.UI.IPlatformDependentScriptMenu MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass0_0::menu
	RuntimeObject* ___menu_3;
	// MPAR.Common.Collections.ObjectCollection MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass0_0::objectCollection
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * ___objectCollection_4;

public:
	inline static int32_t get_offset_of_scriptPageObject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387, ___scriptPageObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_scriptPageObject_0() const { return ___scriptPageObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_scriptPageObject_0() { return &___scriptPageObject_0; }
	inline void set_scriptPageObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___scriptPageObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___scriptPageObject_0), value);
	}

	inline static int32_t get_offset_of_factory_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387, ___factory_1)); }
	inline ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344 * get_factory_1() const { return ___factory_1; }
	inline ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344 ** get_address_of_factory_1() { return &___factory_1; }
	inline void set_factory_1(ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344 * value)
	{
		___factory_1 = value;
		Il2CppCodeGenWriteBarrier((&___factory_1), value);
	}

	inline static int32_t get_offset_of_scriptMenuItem_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387, ___scriptMenuItem_2)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_scriptMenuItem_2() const { return ___scriptMenuItem_2; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_scriptMenuItem_2() { return &___scriptMenuItem_2; }
	inline void set_scriptMenuItem_2(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___scriptMenuItem_2 = value;
		Il2CppCodeGenWriteBarrier((&___scriptMenuItem_2), value);
	}

	inline static int32_t get_offset_of_menu_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387, ___menu_3)); }
	inline RuntimeObject* get_menu_3() const { return ___menu_3; }
	inline RuntimeObject** get_address_of_menu_3() { return &___menu_3; }
	inline void set_menu_3(RuntimeObject* value)
	{
		___menu_3 = value;
		Il2CppCodeGenWriteBarrier((&___menu_3), value);
	}

	inline static int32_t get_offset_of_objectCollection_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387, ___objectCollection_4)); }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * get_objectCollection_4() const { return ___objectCollection_4; }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB ** get_address_of_objectCollection_4() { return &___objectCollection_4; }
	inline void set_objectCollection_4(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * value)
	{
		___objectCollection_4 = value;
		Il2CppCodeGenWriteBarrier((&___objectCollection_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T555D92B3BA25A958C33020FB594D2F6E11F1C387_H
#ifndef U3CU3EC__DISPLAYCLASS1_0_TEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4_H
#define U3CU3EC__DISPLAYCLASS1_0_TEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass1_0
struct  U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass1_0::doneCount
	int32_t ___doneCount_0;
	// System.Action MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass1_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_doneCount_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4, ___doneCount_0)); }
	inline int32_t get_doneCount_0() const { return ___doneCount_0; }
	inline int32_t* get_address_of_doneCount_0() { return &___doneCount_0; }
	inline void set_doneCount_0(int32_t value)
	{
		___doneCount_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4, ___U3CU3E9__0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS1_0_TEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T9DF7A7295DF78645B7F16F5D2E285C01E67CCF44_H
#define U3CU3EC__DISPLAYCLASS4_0_T9DF7A7295DF78645B7F16F5D2E285C01E67CCF44_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass4_0::doneCount
	int32_t ___doneCount_0;
	// System.Action MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass4_0::<>9__0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_doneCount_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44, ___doneCount_0)); }
	inline int32_t get_doneCount_0() const { return ___doneCount_0; }
	inline int32_t* get_address_of_doneCount_0() { return &___doneCount_0; }
	inline void set_doneCount_0(int32_t value)
	{
		___doneCount_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44, ___U3CU3E9__0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T9DF7A7295DF78645B7F16F5D2E285C01E67CCF44_H
#ifndef U3CMINIMIZEOTHERSCRIPTSU3ED__4_T65DA679E7269F15C7A3ECC0257568C7C3E12CBFA_H
#define U3CMINIMIZEOTHERSCRIPTSU3ED__4_T65DA679E7269F15C7A3ECC0257568C7C3E12CBFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4
struct  U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Collections.ObjectCollection MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::objectCollection
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * ___objectCollection_2;
	// UnityEngine.GameObject MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::scriptMenuItem
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___scriptMenuItem_3;
	// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass4_0 MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::<>8__1
	U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44 * ___U3CU3E8__1_4;
	// System.Action MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_5;
	// System.Int32 MPAR.Common.UI.Effects.MenuTransitions/<MinimizeOtherScripts>d__4::<numToDo>5__2
	int32_t ___U3CnumToDoU3E5__2_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_objectCollection_2() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___objectCollection_2)); }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * get_objectCollection_2() const { return ___objectCollection_2; }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB ** get_address_of_objectCollection_2() { return &___objectCollection_2; }
	inline void set_objectCollection_2(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * value)
	{
		___objectCollection_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectCollection_2), value);
	}

	inline static int32_t get_offset_of_scriptMenuItem_3() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___scriptMenuItem_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_scriptMenuItem_3() const { return ___scriptMenuItem_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_scriptMenuItem_3() { return &___scriptMenuItem_3; }
	inline void set_scriptMenuItem_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___scriptMenuItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___scriptMenuItem_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_4() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___U3CU3E8__1_4)); }
	inline U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44 * get_U3CU3E8__1_4() const { return ___U3CU3E8__1_4; }
	inline U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44 ** get_address_of_U3CU3E8__1_4() { return &___U3CU3E8__1_4; }
	inline void set_U3CU3E8__1_4(U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44 * value)
	{
		___U3CU3E8__1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_4), value);
	}

	inline static int32_t get_offset_of_callback_5() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___callback_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_5() const { return ___callback_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_5() { return &___callback_5; }
	inline void set_callback_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_5 = value;
		Il2CppCodeGenWriteBarrier((&___callback_5), value);
	}

	inline static int32_t get_offset_of_U3CnumToDoU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA, ___U3CnumToDoU3E5__2_6)); }
	inline int32_t get_U3CnumToDoU3E5__2_6() const { return ___U3CnumToDoU3E5__2_6; }
	inline int32_t* get_address_of_U3CnumToDoU3E5__2_6() { return &___U3CnumToDoU3E5__2_6; }
	inline void set_U3CnumToDoU3E5__2_6(int32_t value)
	{
		___U3CnumToDoU3E5__2_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMINIMIZEOTHERSCRIPTSU3ED__4_T65DA679E7269F15C7A3ECC0257568C7C3E12CBFA_H
#ifndef U3CMOVEALLTILESTOONETILEU3ED__1_T4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456_H
#define U3CMOVEALLTILESTOONETILEU3ED__1_T4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1
struct  U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Common.Collections.ObjectCollection MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::objectCollection
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * ___objectCollection_2;
	// UnityEngine.GameObject MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::scriptMenuItem
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___scriptMenuItem_3;
	// System.Single MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::speed
	float ___speed_4;
	// MPAR.Common.UI.Effects.MenuTransitions/<>c__DisplayClass1_0 MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::<>8__1
	U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4 * ___U3CU3E8__1_5;
	// System.Action MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_6;
	// System.Int32 MPAR.Common.UI.Effects.MenuTransitions/<MoveAllTilesToOneTile>d__1::<numToDo>5__2
	int32_t ___U3CnumToDoU3E5__2_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_objectCollection_2() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___objectCollection_2)); }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * get_objectCollection_2() const { return ___objectCollection_2; }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB ** get_address_of_objectCollection_2() { return &___objectCollection_2; }
	inline void set_objectCollection_2(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * value)
	{
		___objectCollection_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectCollection_2), value);
	}

	inline static int32_t get_offset_of_scriptMenuItem_3() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___scriptMenuItem_3)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_scriptMenuItem_3() const { return ___scriptMenuItem_3; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_scriptMenuItem_3() { return &___scriptMenuItem_3; }
	inline void set_scriptMenuItem_3(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___scriptMenuItem_3 = value;
		Il2CppCodeGenWriteBarrier((&___scriptMenuItem_3), value);
	}

	inline static int32_t get_offset_of_speed_4() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___speed_4)); }
	inline float get_speed_4() const { return ___speed_4; }
	inline float* get_address_of_speed_4() { return &___speed_4; }
	inline void set_speed_4(float value)
	{
		___speed_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E8__1_5() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___U3CU3E8__1_5)); }
	inline U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4 * get_U3CU3E8__1_5() const { return ___U3CU3E8__1_5; }
	inline U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4 ** get_address_of_U3CU3E8__1_5() { return &___U3CU3E8__1_5; }
	inline void set_U3CU3E8__1_5(U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4 * value)
	{
		___U3CU3E8__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_5), value);
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___callback_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_6() const { return ___callback_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}

	inline static int32_t get_offset_of_U3CnumToDoU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456, ___U3CnumToDoU3E5__2_7)); }
	inline int32_t get_U3CnumToDoU3E5__2_7() const { return ___U3CnumToDoU3E5__2_7; }
	inline int32_t* get_address_of_U3CnumToDoU3E5__2_7() { return &___U3CnumToDoU3E5__2_7; }
	inline void set_U3CnumToDoU3E5__2_7(int32_t value)
	{
		___U3CnumToDoU3E5__2_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEALLTILESTOONETILEU3ED__1_T4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456_H
#ifndef INTERPOLATIONUTILITIES_TB3BEE2E66A63F05A5DBA9E478390EFF20083BCB7_H
#define INTERPOLATIONUTILITIES_TB3BEE2E66A63F05A5DBA9E478390EFF20083BCB7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utilities.InterpolationUtilities
struct  InterpolationUtilities_tB3BEE2E66A63F05A5DBA9E478390EFF20083BCB7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATIONUTILITIES_TB3BEE2E66A63F05A5DBA9E478390EFF20083BCB7_H
#ifndef CAMERAEXTENSIONS_T601F1DACF4D8AECE26B5FD007561AFB3E612892D_H
#define CAMERAEXTENSIONS_T601F1DACF4D8AECE26B5FD007561AFB3E612892D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.CameraExtensions
struct  CameraExtensions_t601F1DACF4D8AECE26B5FD007561AFB3E612892D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAEXTENSIONS_T601F1DACF4D8AECE26B5FD007561AFB3E612892D_H
#ifndef INPUTMANAGERWRAPPER_T8490AF07B31C7938294B081EA9720E1E03CFD356_H
#define INPUTMANAGERWRAPPER_T8490AF07B31C7938294B081EA9720E1E03CFD356_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.InputManagerWrapper
struct  InputManagerWrapper_t8490AF07B31C7938294B081EA9720E1E03CFD356  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INPUTMANAGERWRAPPER_T8490AF07B31C7938294B081EA9720E1E03CFD356_H
#ifndef MATHUTILS_T1A5DF035BC45C36A96CD52D3619C162FF6F8DAF1_H
#define MATHUTILS_T1A5DF035BC45C36A96CD52D3619C162FF6F8DAF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.MathUtils
struct  MathUtils_t1A5DF035BC45C36A96CD52D3619C162FF6F8DAF1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATHUTILS_T1A5DF035BC45C36A96CD52D3619C162FF6F8DAF1_H
#ifndef U3CU3EC__DISPLAYCLASS31_1_TC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71_H
#define U3CU3EC__DISPLAYCLASS31_1_TC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_1
struct  U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71  : public RuntimeObject
{
public:
	// MPAR.Common.Entities.HologramData MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_1::hologram
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9 * ___hologram_0;
	// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_0 MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_hologram_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71, ___hologram_0)); }
	inline HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9 * get_hologram_0() const { return ___hologram_0; }
	inline HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9 ** get_address_of_hologram_0() { return &___hologram_0; }
	inline void set_hologram_0(HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9 * value)
	{
		___hologram_0 = value;
		Il2CppCodeGenWriteBarrier((&___hologram_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_1_TC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T4205219D8CAEF7B4716D57488B2A633CD7656F22_H
#define U3CU3EC__DISPLAYCLASS38_0_T4205219D8CAEF7B4716D57488B2A633CD7656F22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t4205219D8CAEF7B4716D57488B2A633CD7656F22  : public RuntimeObject
{
public:
	// System.Boolean MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass38_0::loadThumbnails
	bool ___loadThumbnails_0;

public:
	inline static int32_t get_offset_of_loadThumbnails_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t4205219D8CAEF7B4716D57488B2A633CD7656F22, ___loadThumbnails_0)); }
	inline bool get_loadThumbnails_0() const { return ___loadThumbnails_0; }
	inline bool* get_address_of_loadThumbnails_0() { return &___loadThumbnails_0; }
	inline void set_loadThumbnails_0(bool value)
	{
		___loadThumbnails_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T4205219D8CAEF7B4716D57488B2A633CD7656F22_H
#ifndef U3CLOADCACHEDSCRIPTTHUMBNAILSU3ED__37_TB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21_H
#define U3CLOADCACHEDSCRIPTTHUMBNAILSU3ED__37_TB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager/<LoadCachedScriptThumbnails>d__37
struct  U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Desktop.Persistence.BaseSaveLoadManager/<LoadCachedScriptThumbnails>d__37::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Desktop.Persistence.BaseSaveLoadManager/<LoadCachedScriptThumbnails>d__37::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> MPAR.Desktop.Persistence.BaseSaveLoadManager/<LoadCachedScriptThumbnails>d__37::scripts
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___scripts_2;
	// System.Int32 MPAR.Desktop.Persistence.BaseSaveLoadManager/<LoadCachedScriptThumbnails>d__37::<i>5__2
	int32_t ___U3CiU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_scripts_2() { return static_cast<int32_t>(offsetof(U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21, ___scripts_2)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_scripts_2() const { return ___scripts_2; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_scripts_2() { return &___scripts_2; }
	inline void set_scripts_2(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___scripts_2 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_2), value);
	}

	inline static int32_t get_offset_of_U3CiU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21, ___U3CiU3E5__2_3)); }
	inline int32_t get_U3CiU3E5__2_3() const { return ___U3CiU3E5__2_3; }
	inline int32_t* get_address_of_U3CiU3E5__2_3() { return &___U3CiU3E5__2_3; }
	inline void set_U3CiU3E5__2_3(int32_t value)
	{
		___U3CiU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCACHEDSCRIPTTHUMBNAILSU3ED__37_TB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21_H
#ifndef U3CU3EC_T9FC6E2AFD1025F99F2A742A03CBD1951A479C859_H
#define U3CU3EC_T9FC6E2AFD1025F99F2A742A03CBD1951A479C859_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.GameObjects.Utility.MaterialUtility/<>c
struct  U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields
{
public:
	// MPAR.GameObjects.Utility.MaterialUtility/<>c MPAR.GameObjects.Utility.MaterialUtility/<>c::<>9
	U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Renderer,UnityEngine.Material> MPAR.GameObjects.Utility.MaterialUtility/<>c::<>9__11_0
	Func_2_t588206846C9EF87E419BFAADF4BA341F2F1E6817 * ___U3CU3E9__11_0_1;
	// System.Func`2<UnityEngine.Material,System.Boolean> MPAR.GameObjects.Utility.MaterialUtility/<>c::<>9__11_1
	Func_2_tD9E9003345ACB766DDD09BC2730CAE2FC3463635 * ___U3CU3E9__11_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields, ___U3CU3E9__11_0_1)); }
	inline Func_2_t588206846C9EF87E419BFAADF4BA341F2F1E6817 * get_U3CU3E9__11_0_1() const { return ___U3CU3E9__11_0_1; }
	inline Func_2_t588206846C9EF87E419BFAADF4BA341F2F1E6817 ** get_address_of_U3CU3E9__11_0_1() { return &___U3CU3E9__11_0_1; }
	inline void set_U3CU3E9__11_0_1(Func_2_t588206846C9EF87E419BFAADF4BA341F2F1E6817 * value)
	{
		___U3CU3E9__11_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__11_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields, ___U3CU3E9__11_1_2)); }
	inline Func_2_tD9E9003345ACB766DDD09BC2730CAE2FC3463635 * get_U3CU3E9__11_1_2() const { return ___U3CU3E9__11_1_2; }
	inline Func_2_tD9E9003345ACB766DDD09BC2730CAE2FC3463635 ** get_address_of_U3CU3E9__11_1_2() { return &___U3CU3E9__11_1_2; }
	inline void set_U3CU3E9__11_1_2(Func_2_tD9E9003345ACB766DDD09BC2730CAE2FC3463635 * value)
	{
		___U3CU3E9__11_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__11_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T9FC6E2AFD1025F99F2A742A03CBD1951A479C859_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef GAMEOBJECTFACTORY_T69C0DC65BD450BDDD99982D281023992E254FDC1_H
#define GAMEOBJECTFACTORY_T69C0DC65BD450BDDD99982D281023992E254FDC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectFactory
struct  GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.GameObjectFactory::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// UnityEngine.Object Zenject.GameObjectFactory::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__prefab_1() { return static_cast<int32_t>(offsetof(GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1, ____prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_1() const { return ____prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_1() { return &____prefab_1; }
	inline void set__prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTFACTORY_T69C0DC65BD450BDDD99982D281023992E254FDC1_H
#ifndef CANVASFACTORY_TAF9EC33C5A509E003E1163CE29E768DB33C1585E_H
#define CANVASFACTORY_TAF9EC33C5A509E003E1163CE29E768DB33C1585E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.CanvasFactory
struct  CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANVASFACTORY_TAF9EC33C5A509E003E1163CE29E768DB33C1585E_H
#ifndef EXHIBITBOXFACTORY_TF11A6EDB9BE288A54C29171DCC3059BCE31EAA13_H
#define EXHIBITBOXFACTORY_TF11A6EDB9BE288A54C29171DCC3059BCE31EAA13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory
struct  ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.ExhibitBoxFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXHIBITBOXFACTORY_TF11A6EDB9BE288A54C29171DCC3059BCE31EAA13_H
#ifndef INGAMECONSOLEFACTORY_TFE253A8056DABE48E94F01733DF37C5187F622CF_H
#define INGAMECONSOLEFACTORY_TFE253A8056DABE48E94F01733DF37C5187F622CF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.InGameConsoleFactory
struct  InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMECONSOLEFACTORY_TFE253A8056DABE48E94F01733DF37C5187F622CF_H
#ifndef INSCRIPTIMAGEMENUITEMFACTORY_TFC549B35BCD50C2B24F5451E89B5F6062E4D5831_H
#define INSCRIPTIMAGEMENUITEMFACTORY_TFC549B35BCD50C2B24F5451E89B5F6062E4D5831_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.InScriptImageMenuItemFactory
struct  InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.InScriptImageMenuItemFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSCRIPTIMAGEMENUITEMFACTORY_TFC549B35BCD50C2B24F5451E89B5F6062E4D5831_H
#ifndef INSCRIPTMENUFACTORY_TE28604EF9A7748F0689F833E699CABF93F4DA7B9_H
#define INSCRIPTMENUFACTORY_TE28604EF9A7748F0689F833E699CABF93F4DA7B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.InScriptMenuFactory
struct  InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.InScriptMenuFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSCRIPTMENUFACTORY_TE28604EF9A7748F0689F833E699CABF93F4DA7B9_H
#ifndef INSCRIPTTEXTMENUITEMFACTORY_T6A9C1B69050FE7444D622E3B696778A0A0B9F632_H
#define INSCRIPTTEXTMENUITEMFACTORY_T6A9C1B69050FE7444D622E3B696778A0A0B9F632_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.InScriptTextMenuItemFactory
struct  InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.InScriptTextMenuItemFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSCRIPTTEXTMENUITEMFACTORY_T6A9C1B69050FE7444D622E3B696778A0A0B9F632_H
#ifndef PLANEGENERATORFACTORY_T5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D_H
#define PLANEGENERATORFACTORY_T5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.PlaneGeneratorFactory
struct  PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANEGENERATORFACTORY_T5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D_H
#ifndef POINTCLOUDPARTICLESFACTORY_T528EF437395FFF08E3C638E390B8EEA242CECAB6_H
#define POINTCLOUDPARTICLESFACTORY_T528EF437395FFF08E3C638E390B8EEA242CECAB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.PointCloudParticlesFactory
struct  PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POINTCLOUDPARTICLESFACTORY_T528EF437395FFF08E3C638E390B8EEA242CECAB6_H
#ifndef PORTABLEMENUFACTORY_TAA97D5D2F11B3C26CBC04242D63940BFFBE0070D_H
#define PORTABLEMENUFACTORY_TAA97D5D2F11B3C26CBC04242D63940BFFBE0070D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.PortableMenuFactory
struct  PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.PortableMenuFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PORTABLEMENUFACTORY_TAA97D5D2F11B3C26CBC04242D63940BFFBE0070D_H
#ifndef PORTABLEMENUITEMFACTORY_T1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D_H
#define PORTABLEMENUITEMFACTORY_T1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.PortableMenuItemFactory
struct  PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.PortableMenuItemFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PORTABLEMENUITEMFACTORY_T1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D_H
#ifndef SCRIPTMENUITEMFACTORY_TCF9F400F562098E16C15EAAF14FF009406EAD8F3_H
#define SCRIPTMENUITEMFACTORY_TCF9F400F562098E16C15EAAF14FF009406EAD8F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.ScriptMenuItemFactory
struct  ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.ScriptMenuItemFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTMENUITEMFACTORY_TCF9F400F562098E16C15EAAF14FF009406EAD8F3_H
#ifndef SCRIPTPAGEFACTORY_TA40077659A74AE7D8F8ADA11A5269F63ED46D344_H
#define SCRIPTPAGEFACTORY_TA40077659A74AE7D8F8ADA11A5269F63ED46D344_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.ScriptPageFactory
struct  ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.ScriptPageFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTPAGEFACTORY_TA40077659A74AE7D8F8ADA11A5269F63ED46D344_H
#ifndef VIDEOCONTROLSFACTORY_TADFD460F3C6A69B25A0948738BEB1B2BC5692FD6_H
#define VIDEOCONTROLSFACTORY_TADFD460F3C6A69B25A0948738BEB1B2BC5692FD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Factories.VideoControlsFactory
struct  VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6  : public GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Common.GameObjects.Factories.VideoControlsFactory::<RootAnchor>k__BackingField
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___U3CRootAnchorU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CRootAnchorU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6, ___U3CRootAnchorU3Ek__BackingField_2)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_U3CRootAnchorU3Ek__BackingField_2() const { return ___U3CRootAnchorU3Ek__BackingField_2; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_U3CRootAnchorU3Ek__BackingField_2() { return &___U3CRootAnchorU3Ek__BackingField_2; }
	inline void set_U3CRootAnchorU3Ek__BackingField_2(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___U3CRootAnchorU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootAnchorU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOCONTROLSFACTORY_TADFD460F3C6A69B25A0948738BEB1B2BC5692FD6_H
#ifndef EVENTACTIONARGS_T1D1EB1848FA322AD46D3A3B630236952613C7C66_H
#define EVENTACTIONARGS_T1D1EB1848FA322AD46D3A3B630236952613C7C66_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.EventActionArgs
struct  EventActionArgs_t1D1EB1848FA322AD46D3A3B630236952613C7C66  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTACTIONARGS_T1D1EB1848FA322AD46D3A3B630236952613C7C66_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef U3CMOVEFROMTOU3ED__0_TE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A_H
#define U3CMOVEFROMTOU3ED__0_TE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0
struct  U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::speed
	float ___speed_2;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from_3;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to_4;
	// UnityEngine.Transform MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::objectToMove
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objectToMove_5;
	// System.Boolean MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::disappearAtEnd
	bool ___disappearAtEnd_6;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::<step>5__2
	float ___U3CstepU3E5__2_7;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__0::<t>5__3
	float ___U3CtU3E5__3_8;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_from_3() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___from_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_from_3() const { return ___from_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_from_3() { return &___from_3; }
	inline void set_from_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___from_3 = value;
	}

	inline static int32_t get_offset_of_to_4() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___to_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_to_4() const { return ___to_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_to_4() { return &___to_4; }
	inline void set_to_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___to_4 = value;
	}

	inline static int32_t get_offset_of_objectToMove_5() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___objectToMove_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objectToMove_5() const { return ___objectToMove_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objectToMove_5() { return &___objectToMove_5; }
	inline void set_objectToMove_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objectToMove_5 = value;
		Il2CppCodeGenWriteBarrier((&___objectToMove_5), value);
	}

	inline static int32_t get_offset_of_disappearAtEnd_6() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___disappearAtEnd_6)); }
	inline bool get_disappearAtEnd_6() const { return ___disappearAtEnd_6; }
	inline bool* get_address_of_disappearAtEnd_6() { return &___disappearAtEnd_6; }
	inline void set_disappearAtEnd_6(bool value)
	{
		___disappearAtEnd_6 = value;
	}

	inline static int32_t get_offset_of_U3CstepU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___U3CstepU3E5__2_7)); }
	inline float get_U3CstepU3E5__2_7() const { return ___U3CstepU3E5__2_7; }
	inline float* get_address_of_U3CstepU3E5__2_7() { return &___U3CstepU3E5__2_7; }
	inline void set_U3CstepU3E5__2_7(float value)
	{
		___U3CstepU3E5__2_7 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_8() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A, ___U3CtU3E5__3_8)); }
	inline float get_U3CtU3E5__3_8() const { return ___U3CtU3E5__3_8; }
	inline float* get_address_of_U3CtU3E5__3_8() { return &___U3CtU3E5__3_8; }
	inline void set_U3CtU3E5__3_8(float value)
	{
		___U3CtU3E5__3_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEFROMTOU3ED__0_TE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A_H
#ifndef U3CMOVEFROMTOU3ED__1_TCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9_H
#define U3CMOVEFROMTOU3ED__1_TCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1
struct  U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Transform MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::objectToMove
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objectToMove_2;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from_3;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to_4;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::speed
	float ___speed_5;
	// System.Boolean MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::disappearAtEnd
	bool ___disappearAtEnd_6;
	// System.Action MPAR.Common.GameObjects.GameObjectUtility/<MoveFromTo>d__1::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_objectToMove_2() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___objectToMove_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objectToMove_2() const { return ___objectToMove_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objectToMove_2() { return &___objectToMove_2; }
	inline void set_objectToMove_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objectToMove_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectToMove_2), value);
	}

	inline static int32_t get_offset_of_from_3() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___from_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_from_3() const { return ___from_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_from_3() { return &___from_3; }
	inline void set_from_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___from_3 = value;
	}

	inline static int32_t get_offset_of_to_4() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___to_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_to_4() const { return ___to_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_to_4() { return &___to_4; }
	inline void set_to_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___to_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_disappearAtEnd_6() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___disappearAtEnd_6)); }
	inline bool get_disappearAtEnd_6() const { return ___disappearAtEnd_6; }
	inline bool* get_address_of_disappearAtEnd_6() { return &___disappearAtEnd_6; }
	inline void set_disappearAtEnd_6(bool value)
	{
		___disappearAtEnd_6 = value;
	}

	inline static int32_t get_offset_of_callback_7() { return static_cast<int32_t>(offsetof(U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9, ___callback_7)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_7() const { return ___callback_7; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_7() { return &___callback_7; }
	inline void set_callback_7(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_7 = value;
		Il2CppCodeGenWriteBarrier((&___callback_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMOVEFROMTOU3ED__1_TCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9_H
#ifndef U3CSCALEFROMTOU3ED__2_TEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0_H
#define U3CSCALEFROMTOU3ED__2_TEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2
struct  U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::speed
	float ___speed_2;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from_3;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to_4;
	// UnityEngine.Transform MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::objectToScale
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objectToScale_5;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::<step>5__2
	float ___U3CstepU3E5__2_6;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__2::<t>5__3
	float ___U3CtU3E5__3_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_speed_2() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___speed_2)); }
	inline float get_speed_2() const { return ___speed_2; }
	inline float* get_address_of_speed_2() { return &___speed_2; }
	inline void set_speed_2(float value)
	{
		___speed_2 = value;
	}

	inline static int32_t get_offset_of_from_3() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___from_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_from_3() const { return ___from_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_from_3() { return &___from_3; }
	inline void set_from_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___from_3 = value;
	}

	inline static int32_t get_offset_of_to_4() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___to_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_to_4() const { return ___to_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_to_4() { return &___to_4; }
	inline void set_to_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___to_4 = value;
	}

	inline static int32_t get_offset_of_objectToScale_5() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___objectToScale_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objectToScale_5() const { return ___objectToScale_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objectToScale_5() { return &___objectToScale_5; }
	inline void set_objectToScale_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objectToScale_5 = value;
		Il2CppCodeGenWriteBarrier((&___objectToScale_5), value);
	}

	inline static int32_t get_offset_of_U3CstepU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___U3CstepU3E5__2_6)); }
	inline float get_U3CstepU3E5__2_6() const { return ___U3CstepU3E5__2_6; }
	inline float* get_address_of_U3CstepU3E5__2_6() { return &___U3CstepU3E5__2_6; }
	inline void set_U3CstepU3E5__2_6(float value)
	{
		___U3CstepU3E5__2_6 = value;
	}

	inline static int32_t get_offset_of_U3CtU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0, ___U3CtU3E5__3_7)); }
	inline float get_U3CtU3E5__3_7() const { return ___U3CtU3E5__3_7; }
	inline float* get_address_of_U3CtU3E5__3_7() { return &___U3CtU3E5__3_7; }
	inline void set_U3CtU3E5__3_7(float value)
	{
		___U3CtU3E5__3_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALEFROMTOU3ED__2_TEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0_H
#ifndef U3CSCALEFROMTOU3ED__3_TBA73FBB25597211A6B63220B64586C64D46FD7CD_H
#define U3CSCALEFROMTOU3ED__3_TBA73FBB25597211A6B63220B64586C64D46FD7CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3
struct  U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityEngine.Transform MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::objectToScale
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objectToScale_2;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::from
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___from_3;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::to
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___to_4;
	// System.Single MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::speed
	float ___speed_5;
	// System.Action MPAR.Common.GameObjects.GameObjectUtility/<ScaleFromTo>d__3::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_objectToScale_2() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___objectToScale_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objectToScale_2() const { return ___objectToScale_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objectToScale_2() { return &___objectToScale_2; }
	inline void set_objectToScale_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objectToScale_2 = value;
		Il2CppCodeGenWriteBarrier((&___objectToScale_2), value);
	}

	inline static int32_t get_offset_of_from_3() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___from_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_from_3() const { return ___from_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_from_3() { return &___from_3; }
	inline void set_from_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___from_3 = value;
	}

	inline static int32_t get_offset_of_to_4() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___to_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_to_4() const { return ___to_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_to_4() { return &___to_4; }
	inline void set_to_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___to_4 = value;
	}

	inline static int32_t get_offset_of_speed_5() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___speed_5)); }
	inline float get_speed_5() const { return ___speed_5; }
	inline float* get_address_of_speed_5() { return &___speed_5; }
	inline void set_speed_5(float value)
	{
		___speed_5 = value;
	}

	inline static int32_t get_offset_of_callback_6() { return static_cast<int32_t>(offsetof(U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD, ___callback_6)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_6() const { return ___callback_6; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_6() { return &___callback_6; }
	inline void set_callback_6(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_6 = value;
		Il2CppCodeGenWriteBarrier((&___callback_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSCALEFROMTOU3ED__3_TBA73FBB25597211A6B63220B64586C64D46FD7CD_H
#ifndef MANIPULATIONDELTAEVENTARGS_T4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8_H
#define MANIPULATIONDELTAEVENTARGS_T4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Utility.ManipulationDeltaEventArgs
struct  ManipulationDeltaEventArgs_t4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// UnityEngine.Vector3 MPAR.Common.Input.Utility.ManipulationDeltaEventArgs::<ManipulationDelta>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CManipulationDeltaU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CManipulationDeltaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ManipulationDeltaEventArgs_t4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8, ___U3CManipulationDeltaU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CManipulationDeltaU3Ek__BackingField_1() const { return ___U3CManipulationDeltaU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CManipulationDeltaU3Ek__BackingField_1() { return &___U3CManipulationDeltaU3Ek__BackingField_1; }
	inline void set_U3CManipulationDeltaU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CManipulationDeltaU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MANIPULATIONDELTAEVENTARGS_T4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8_H
#ifndef U3CU3EC__DISPLAYCLASS22_0_T0245DB3C28FE1B8A105445DF98A432ED35962EC9_H
#define U3CU3EC__DISPLAYCLASS22_0_T0245DB3C28FE1B8A105445DF98A432ED35962EC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.MathUtils/<>c__DisplayClass22_0
struct  U3CU3Ec__DisplayClass22_0_t0245DB3C28FE1B8A105445DF98A432ED35962EC9  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 MPAR.Common.Utility.MathUtils/<>c__DisplayClass22_0::nearestPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___nearestPoint_0;
	// System.Single MPAR.Common.Utility.MathUtils/<>c__DisplayClass22_0::ransac_threshold
	float ___ransac_threshold_1;

public:
	inline static int32_t get_offset_of_nearestPoint_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t0245DB3C28FE1B8A105445DF98A432ED35962EC9, ___nearestPoint_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_nearestPoint_0() const { return ___nearestPoint_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_nearestPoint_0() { return &___nearestPoint_0; }
	inline void set_nearestPoint_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___nearestPoint_0 = value;
	}

	inline static int32_t get_offset_of_ransac_threshold_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass22_0_t0245DB3C28FE1B8A105445DF98A432ED35962EC9, ___ransac_threshold_1)); }
	inline float get_ransac_threshold_1() const { return ___ransac_threshold_1; }
	inline float* get_address_of_ransac_threshold_1() { return &___ransac_threshold_1; }
	inline void set_ransac_threshold_1(float value)
	{
		___ransac_threshold_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS22_0_T0245DB3C28FE1B8A105445DF98A432ED35962EC9_H
#ifndef PIVOTAXIS_TD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE_H
#define PIVOTAXIS_TD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.PivotAxis
struct  PivotAxis_tD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE 
{
public:
	// System.Int32 MPAR.Common.Utility.PivotAxis::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PivotAxis_tD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PIVOTAXIS_TD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE_H
#ifndef U3CU3EC__DISPLAYCLASS31_2_TEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358_H
#define U3CU3EC__DISPLAYCLASS31_2_TEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2
struct  U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358  : public RuntimeObject
{
public:
	// MPAR.Common.GameObjects.ExhibitBox MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2::box
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A * ___box_0;
	// UnityEngine.Vector3 MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2::pos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___pos_1;
	// UnityEngine.Vector3 MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2::euler
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___euler_2;
	// UnityEngine.Vector3 MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2::scale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scale_3;
	// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_1 MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2::CS$<>8__locals2
	U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71 * ___CSU24U3CU3E8__locals2_4;
	// System.Action MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_2::<>9__2
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__2_5;

public:
	inline static int32_t get_offset_of_box_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358, ___box_0)); }
	inline ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A * get_box_0() const { return ___box_0; }
	inline ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A ** get_address_of_box_0() { return &___box_0; }
	inline void set_box_0(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A * value)
	{
		___box_0 = value;
		Il2CppCodeGenWriteBarrier((&___box_0), value);
	}

	inline static int32_t get_offset_of_pos_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358, ___pos_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_pos_1() const { return ___pos_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_pos_1() { return &___pos_1; }
	inline void set_pos_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___pos_1 = value;
	}

	inline static int32_t get_offset_of_euler_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358, ___euler_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_euler_2() const { return ___euler_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_euler_2() { return &___euler_2; }
	inline void set_euler_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___euler_2 = value;
	}

	inline static int32_t get_offset_of_scale_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358, ___scale_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_scale_3() const { return ___scale_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_scale_3() { return &___scale_3; }
	inline void set_scale_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___scale_3 = value;
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358, ___CSU24U3CU3E8__locals2_4)); }
	inline U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71 * get_CSU24U3CU3E8__locals2_4() const { return ___CSU24U3CU3E8__locals2_4; }
	inline U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71 ** get_address_of_CSU24U3CU3E8__locals2_4() { return &___CSU24U3CU3E8__locals2_4; }
	inline void set_CSU24U3CU3E8__locals2_4(U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71 * value)
	{
		___CSU24U3CU3E8__locals2_4 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_5() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358, ___U3CU3E9__2_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__2_5() const { return ___U3CU3E9__2_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__2_5() { return &___U3CU3E9__2_5; }
	inline void set_U3CU3E9__2_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_2_TEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358_H
#ifndef BLENDMODE_T7D8C79146EA953B79703692B14F9929DB3234852_H
#define BLENDMODE_T7D8C79146EA953B79703692B14F9929DB3234852_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.GameObjects.Utility.MaterialUtility/BlendMode
struct  BlendMode_t7D8C79146EA953B79703692B14F9929DB3234852 
{
public:
	// System.Int32 MPAR.GameObjects.Utility.MaterialUtility/BlendMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BlendMode_t7D8C79146EA953B79703692B14F9929DB3234852, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLENDMODE_T7D8C79146EA953B79703692B14F9929DB3234852_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#define VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Video.VideoSource
struct  VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A 
{
public:
	// System.Int32 UnityEngine.Video.VideoSource::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(VideoSource_t32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOSOURCE_T32501B57EA7F9CF835FBA8184C9AF427CBBEFD0A_H
#ifndef RAYCASTINFO_T1126512F6856BDC86C39EFC3F683FC189343EF25_H
#define RAYCASTINFO_T1126512F6856BDC86C39EFC3F683FC189343EF25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Utility.RaycastInfo
struct  RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 MPAR.Common.Input.Utility.RaycastInfo::<Origin>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3COriginU3Ek__BackingField_0;
	// UnityEngine.Vector3 MPAR.Common.Input.Utility.RaycastInfo::<Direction>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CDirectionU3Ek__BackingField_1;
	// System.Single MPAR.Common.Input.Utility.RaycastInfo::<Distance>k__BackingField
	float ___U3CDistanceU3Ek__BackingField_2;
	// UnityEngine.RaycastHit MPAR.Common.Input.Utility.RaycastInfo::<Hit>k__BackingField
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___U3CHitU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25, ___U3COriginU3Ek__BackingField_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3COriginU3Ek__BackingField_0() const { return ___U3COriginU3Ek__BackingField_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3COriginU3Ek__BackingField_0() { return &___U3COriginU3Ek__BackingField_0; }
	inline void set_U3COriginU3Ek__BackingField_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3COriginU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25, ___U3CDirectionU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CDirectionU3Ek__BackingField_1() const { return ___U3CDirectionU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CDirectionU3Ek__BackingField_1() { return &___U3CDirectionU3Ek__BackingField_1; }
	inline void set_U3CDirectionU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CDirectionU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDistanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25, ___U3CDistanceU3Ek__BackingField_2)); }
	inline float get_U3CDistanceU3Ek__BackingField_2() const { return ___U3CDistanceU3Ek__BackingField_2; }
	inline float* get_address_of_U3CDistanceU3Ek__BackingField_2() { return &___U3CDistanceU3Ek__BackingField_2; }
	inline void set_U3CDistanceU3Ek__BackingField_2(float value)
	{
		___U3CDistanceU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CHitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25, ___U3CHitU3Ek__BackingField_3)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_U3CHitU3Ek__BackingField_3() const { return ___U3CHitU3Ek__BackingField_3; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_U3CHitU3Ek__BackingField_3() { return &___U3CHitU3Ek__BackingField_3; }
	inline void set_U3CHitU3Ek__BackingField_3(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___U3CHitU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTINFO_T1126512F6856BDC86C39EFC3F683FC189343EF25_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef VIDEOASSET_T47354AA1EA515717B61E4026900A7B6121A7C45A_H
#define VIDEOASSET_T47354AA1EA515717B61E4026900A7B6121A7C45A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.AssetLoader.IAssets.VideoAsset
struct  VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MPAR.AssetLoader.IAssets.VideoAsset::videoDisplayObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___videoDisplayObject_4;
	// UnityEngine.UI.RawImage MPAR.AssetLoader.IAssets.VideoAsset::image
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___image_5;
	// System.String MPAR.AssetLoader.IAssets.VideoAsset::videoUrl
	String_t* ___videoUrl_6;
	// System.String MPAR.AssetLoader.IAssets.VideoAsset::streamingVideoUrl
	String_t* ___streamingVideoUrl_7;
	// UnityEngine.Video.VideoPlayer MPAR.AssetLoader.IAssets.VideoAsset::videoPlayer
	VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * ___videoPlayer_8;
	// UnityEngine.Video.VideoSource MPAR.AssetLoader.IAssets.VideoAsset::videoSource
	int32_t ___videoSource_9;
	// UnityEngine.AudioSource MPAR.AssetLoader.IAssets.VideoAsset::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_10;
	// System.Boolean MPAR.AssetLoader.IAssets.VideoAsset::<ReadyToPlay>k__BackingField
	bool ___U3CReadyToPlayU3Ek__BackingField_11;
	// System.Boolean MPAR.AssetLoader.IAssets.VideoAsset::<IsPlaying>k__BackingField
	bool ___U3CIsPlayingU3Ek__BackingField_12;
	// System.Boolean MPAR.AssetLoader.IAssets.VideoAsset::justPlayedOrPaused
	bool ___justPlayedOrPaused_13;
	// System.Boolean MPAR.AssetLoader.IAssets.VideoAsset::hasSetVideo
	bool ___hasSetVideo_14;
	// System.Single MPAR.AssetLoader.IAssets.VideoAsset::startY
	float ___startY_15;
	// System.String MPAR.AssetLoader.IAssets.VideoAsset::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_videoDisplayObject_4() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___videoDisplayObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_videoDisplayObject_4() const { return ___videoDisplayObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_videoDisplayObject_4() { return &___videoDisplayObject_4; }
	inline void set_videoDisplayObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___videoDisplayObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___videoDisplayObject_4), value);
	}

	inline static int32_t get_offset_of_image_5() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___image_5)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_image_5() const { return ___image_5; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_image_5() { return &___image_5; }
	inline void set_image_5(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___image_5 = value;
		Il2CppCodeGenWriteBarrier((&___image_5), value);
	}

	inline static int32_t get_offset_of_videoUrl_6() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___videoUrl_6)); }
	inline String_t* get_videoUrl_6() const { return ___videoUrl_6; }
	inline String_t** get_address_of_videoUrl_6() { return &___videoUrl_6; }
	inline void set_videoUrl_6(String_t* value)
	{
		___videoUrl_6 = value;
		Il2CppCodeGenWriteBarrier((&___videoUrl_6), value);
	}

	inline static int32_t get_offset_of_streamingVideoUrl_7() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___streamingVideoUrl_7)); }
	inline String_t* get_streamingVideoUrl_7() const { return ___streamingVideoUrl_7; }
	inline String_t** get_address_of_streamingVideoUrl_7() { return &___streamingVideoUrl_7; }
	inline void set_streamingVideoUrl_7(String_t* value)
	{
		___streamingVideoUrl_7 = value;
		Il2CppCodeGenWriteBarrier((&___streamingVideoUrl_7), value);
	}

	inline static int32_t get_offset_of_videoPlayer_8() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___videoPlayer_8)); }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * get_videoPlayer_8() const { return ___videoPlayer_8; }
	inline VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 ** get_address_of_videoPlayer_8() { return &___videoPlayer_8; }
	inline void set_videoPlayer_8(VideoPlayer_tFC1C27AF83D59A5213B2AC561B43FD7E19FE02F2 * value)
	{
		___videoPlayer_8 = value;
		Il2CppCodeGenWriteBarrier((&___videoPlayer_8), value);
	}

	inline static int32_t get_offset_of_videoSource_9() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___videoSource_9)); }
	inline int32_t get_videoSource_9() const { return ___videoSource_9; }
	inline int32_t* get_address_of_videoSource_9() { return &___videoSource_9; }
	inline void set_videoSource_9(int32_t value)
	{
		___videoSource_9 = value;
	}

	inline static int32_t get_offset_of_audioSource_10() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___audioSource_10)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_10() const { return ___audioSource_10; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_10() { return &___audioSource_10; }
	inline void set_audioSource_10(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_10 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_10), value);
	}

	inline static int32_t get_offset_of_U3CReadyToPlayU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___U3CReadyToPlayU3Ek__BackingField_11)); }
	inline bool get_U3CReadyToPlayU3Ek__BackingField_11() const { return ___U3CReadyToPlayU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CReadyToPlayU3Ek__BackingField_11() { return &___U3CReadyToPlayU3Ek__BackingField_11; }
	inline void set_U3CReadyToPlayU3Ek__BackingField_11(bool value)
	{
		___U3CReadyToPlayU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CIsPlayingU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___U3CIsPlayingU3Ek__BackingField_12)); }
	inline bool get_U3CIsPlayingU3Ek__BackingField_12() const { return ___U3CIsPlayingU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CIsPlayingU3Ek__BackingField_12() { return &___U3CIsPlayingU3Ek__BackingField_12; }
	inline void set_U3CIsPlayingU3Ek__BackingField_12(bool value)
	{
		___U3CIsPlayingU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_justPlayedOrPaused_13() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___justPlayedOrPaused_13)); }
	inline bool get_justPlayedOrPaused_13() const { return ___justPlayedOrPaused_13; }
	inline bool* get_address_of_justPlayedOrPaused_13() { return &___justPlayedOrPaused_13; }
	inline void set_justPlayedOrPaused_13(bool value)
	{
		___justPlayedOrPaused_13 = value;
	}

	inline static int32_t get_offset_of_hasSetVideo_14() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___hasSetVideo_14)); }
	inline bool get_hasSetVideo_14() const { return ___hasSetVideo_14; }
	inline bool* get_address_of_hasSetVideo_14() { return &___hasSetVideo_14; }
	inline void set_hasSetVideo_14(bool value)
	{
		___hasSetVideo_14 = value;
	}

	inline static int32_t get_offset_of_startY_15() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___startY_15)); }
	inline float get_startY_15() const { return ___startY_15; }
	inline float* get_address_of_startY_15() { return &___startY_15; }
	inline void set_startY_15(float value)
	{
		___startY_15 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A, ___U3CNameU3Ek__BackingField_16)); }
	inline String_t* get_U3CNameU3Ek__BackingField_16() const { return ___U3CNameU3Ek__BackingField_16; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_16() { return &___U3CNameU3Ek__BackingField_16; }
	inline void set_U3CNameU3Ek__BackingField_16(String_t* value)
	{
		___U3CNameU3Ek__BackingField_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIDEOASSET_T47354AA1EA515717B61E4026900A7B6121A7C45A_H
#ifndef EDITHANDLE_TC9BD695B96B73D220F79439CBB7DB559FDD5F482_H
#define EDITHANDLE_TC9BD695B96B73D220F79439CBB7DB559FDD5F482_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.EditHandle
struct  EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform MPAR.Common.GameObjects.EditHandle::mainBox
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___mainBox_4;
	// UnityEngine.Color MPAR.Common.GameObjects.EditHandle::highlightColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___highlightColor_5;
	// UnityEngine.Color MPAR.Common.GameObjects.EditHandle::originalColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___originalColor_6;
	// System.Boolean MPAR.Common.GameObjects.EditHandle::isManipulating
	bool ___isManipulating_7;
	// UnityEngine.AudioSource MPAR.Common.GameObjects.EditHandle::audioSource
	AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * ___audioSource_8;
	// System.Boolean MPAR.Common.GameObjects.EditHandle::playedGrabSound
	bool ___playedGrabSound_9;
	// System.Boolean MPAR.Common.GameObjects.EditHandle::playedReleaseSound
	bool ___playedReleaseSound_10;
	// UnityEngine.Material MPAR.Common.GameObjects.EditHandle::material
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___material_11;
	// UnityEngine.Collider MPAR.Common.GameObjects.EditHandle::ourCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___ourCollider_12;
	// UnityEngine.Renderer MPAR.Common.GameObjects.EditHandle::ourRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___ourRenderer_13;
	// MPAR.Common.Input.IPlatformDependentInput MPAR.Common.GameObjects.EditHandle::<Input>k__BackingField
	RuntimeObject* ___U3CInputU3Ek__BackingField_14;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.GameObjects.EditHandle::<GestureManager>k__BackingField
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___U3CGestureManagerU3Ek__BackingField_15;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.EditHandle::<ManipulationDelta>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CManipulationDeltaU3Ek__BackingField_16;

public:
	inline static int32_t get_offset_of_mainBox_4() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___mainBox_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_mainBox_4() const { return ___mainBox_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_mainBox_4() { return &___mainBox_4; }
	inline void set_mainBox_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___mainBox_4 = value;
		Il2CppCodeGenWriteBarrier((&___mainBox_4), value);
	}

	inline static int32_t get_offset_of_highlightColor_5() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___highlightColor_5)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_highlightColor_5() const { return ___highlightColor_5; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_highlightColor_5() { return &___highlightColor_5; }
	inline void set_highlightColor_5(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___highlightColor_5 = value;
	}

	inline static int32_t get_offset_of_originalColor_6() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___originalColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_originalColor_6() const { return ___originalColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_originalColor_6() { return &___originalColor_6; }
	inline void set_originalColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___originalColor_6 = value;
	}

	inline static int32_t get_offset_of_isManipulating_7() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___isManipulating_7)); }
	inline bool get_isManipulating_7() const { return ___isManipulating_7; }
	inline bool* get_address_of_isManipulating_7() { return &___isManipulating_7; }
	inline void set_isManipulating_7(bool value)
	{
		___isManipulating_7 = value;
	}

	inline static int32_t get_offset_of_audioSource_8() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___audioSource_8)); }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * get_audioSource_8() const { return ___audioSource_8; }
	inline AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C ** get_address_of_audioSource_8() { return &___audioSource_8; }
	inline void set_audioSource_8(AudioSource_t5196F862B4E60F404613361C90D87FBDD041E93C * value)
	{
		___audioSource_8 = value;
		Il2CppCodeGenWriteBarrier((&___audioSource_8), value);
	}

	inline static int32_t get_offset_of_playedGrabSound_9() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___playedGrabSound_9)); }
	inline bool get_playedGrabSound_9() const { return ___playedGrabSound_9; }
	inline bool* get_address_of_playedGrabSound_9() { return &___playedGrabSound_9; }
	inline void set_playedGrabSound_9(bool value)
	{
		___playedGrabSound_9 = value;
	}

	inline static int32_t get_offset_of_playedReleaseSound_10() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___playedReleaseSound_10)); }
	inline bool get_playedReleaseSound_10() const { return ___playedReleaseSound_10; }
	inline bool* get_address_of_playedReleaseSound_10() { return &___playedReleaseSound_10; }
	inline void set_playedReleaseSound_10(bool value)
	{
		___playedReleaseSound_10 = value;
	}

	inline static int32_t get_offset_of_material_11() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___material_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_material_11() const { return ___material_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_material_11() { return &___material_11; }
	inline void set_material_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___material_11 = value;
		Il2CppCodeGenWriteBarrier((&___material_11), value);
	}

	inline static int32_t get_offset_of_ourCollider_12() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___ourCollider_12)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_ourCollider_12() const { return ___ourCollider_12; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_ourCollider_12() { return &___ourCollider_12; }
	inline void set_ourCollider_12(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___ourCollider_12 = value;
		Il2CppCodeGenWriteBarrier((&___ourCollider_12), value);
	}

	inline static int32_t get_offset_of_ourRenderer_13() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___ourRenderer_13)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get_ourRenderer_13() const { return ___ourRenderer_13; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of_ourRenderer_13() { return &___ourRenderer_13; }
	inline void set_ourRenderer_13(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		___ourRenderer_13 = value;
		Il2CppCodeGenWriteBarrier((&___ourRenderer_13), value);
	}

	inline static int32_t get_offset_of_U3CInputU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___U3CInputU3Ek__BackingField_14)); }
	inline RuntimeObject* get_U3CInputU3Ek__BackingField_14() const { return ___U3CInputU3Ek__BackingField_14; }
	inline RuntimeObject** get_address_of_U3CInputU3Ek__BackingField_14() { return &___U3CInputU3Ek__BackingField_14; }
	inline void set_U3CInputU3Ek__BackingField_14(RuntimeObject* value)
	{
		___U3CInputU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CGestureManagerU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___U3CGestureManagerU3Ek__BackingField_15)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_U3CGestureManagerU3Ek__BackingField_15() const { return ___U3CGestureManagerU3Ek__BackingField_15; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_U3CGestureManagerU3Ek__BackingField_15() { return &___U3CGestureManagerU3Ek__BackingField_15; }
	inline void set_U3CGestureManagerU3Ek__BackingField_15(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___U3CGestureManagerU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGestureManagerU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CManipulationDeltaU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482, ___U3CManipulationDeltaU3Ek__BackingField_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CManipulationDeltaU3Ek__BackingField_16() const { return ___U3CManipulationDeltaU3Ek__BackingField_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CManipulationDeltaU3Ek__BackingField_16() { return &___U3CManipulationDeltaU3Ek__BackingField_16; }
	inline void set_U3CManipulationDeltaU3Ek__BackingField_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CManipulationDeltaU3Ek__BackingField_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDITHANDLE_TC9BD695B96B73D220F79439CBB7DB559FDD5F482_H
#ifndef EXHIBITBOX_T889C2D58A95ED32320CE1B582E8389B95D72DF2A_H
#define EXHIBITBOX_T889C2D58A95ED32320CE1B582E8389B95D72DF2A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.ExhibitBox
struct  ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Color MPAR.Common.GameObjects.ExhibitBox::frameColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___frameColor_4;
	// System.String MPAR.Common.GameObjects.ExhibitBox::frameGroupName
	String_t* ___frameGroupName_5;
	// UnityEngine.Color MPAR.Common.GameObjects.ExhibitBox::cornerColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___cornerColor_6;
	// System.String MPAR.Common.GameObjects.ExhibitBox::cornerGroupName
	String_t* ___cornerGroupName_7;
	// UnityEngine.Color MPAR.Common.GameObjects.ExhibitBox::rotateHandleColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___rotateHandleColor_8;
	// System.String MPAR.Common.GameObjects.ExhibitBox::rotateHandleGroupName
	String_t* ___rotateHandleGroupName_9;
	// System.String MPAR.Common.GameObjects.ExhibitBox::faceGroupName
	String_t* ___faceGroupName_10;
	// UnityEngine.GameObject MPAR.Common.GameObjects.ExhibitBox::<Object>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CObjectU3Ek__BackingField_11;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.ExhibitBox::exhibitItemOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___exhibitItemOffset_12;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.ExhibitBox::exhibitItemBorder
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___exhibitItemBorder_13;
	// System.Boolean MPAR.Common.GameObjects.ExhibitBox::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_frameColor_4() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___frameColor_4)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_frameColor_4() const { return ___frameColor_4; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_frameColor_4() { return &___frameColor_4; }
	inline void set_frameColor_4(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___frameColor_4 = value;
	}

	inline static int32_t get_offset_of_frameGroupName_5() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___frameGroupName_5)); }
	inline String_t* get_frameGroupName_5() const { return ___frameGroupName_5; }
	inline String_t** get_address_of_frameGroupName_5() { return &___frameGroupName_5; }
	inline void set_frameGroupName_5(String_t* value)
	{
		___frameGroupName_5 = value;
		Il2CppCodeGenWriteBarrier((&___frameGroupName_5), value);
	}

	inline static int32_t get_offset_of_cornerColor_6() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___cornerColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_cornerColor_6() const { return ___cornerColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_cornerColor_6() { return &___cornerColor_6; }
	inline void set_cornerColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___cornerColor_6 = value;
	}

	inline static int32_t get_offset_of_cornerGroupName_7() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___cornerGroupName_7)); }
	inline String_t* get_cornerGroupName_7() const { return ___cornerGroupName_7; }
	inline String_t** get_address_of_cornerGroupName_7() { return &___cornerGroupName_7; }
	inline void set_cornerGroupName_7(String_t* value)
	{
		___cornerGroupName_7 = value;
		Il2CppCodeGenWriteBarrier((&___cornerGroupName_7), value);
	}

	inline static int32_t get_offset_of_rotateHandleColor_8() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___rotateHandleColor_8)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_rotateHandleColor_8() const { return ___rotateHandleColor_8; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_rotateHandleColor_8() { return &___rotateHandleColor_8; }
	inline void set_rotateHandleColor_8(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___rotateHandleColor_8 = value;
	}

	inline static int32_t get_offset_of_rotateHandleGroupName_9() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___rotateHandleGroupName_9)); }
	inline String_t* get_rotateHandleGroupName_9() const { return ___rotateHandleGroupName_9; }
	inline String_t** get_address_of_rotateHandleGroupName_9() { return &___rotateHandleGroupName_9; }
	inline void set_rotateHandleGroupName_9(String_t* value)
	{
		___rotateHandleGroupName_9 = value;
		Il2CppCodeGenWriteBarrier((&___rotateHandleGroupName_9), value);
	}

	inline static int32_t get_offset_of_faceGroupName_10() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___faceGroupName_10)); }
	inline String_t* get_faceGroupName_10() const { return ___faceGroupName_10; }
	inline String_t** get_address_of_faceGroupName_10() { return &___faceGroupName_10; }
	inline void set_faceGroupName_10(String_t* value)
	{
		___faceGroupName_10 = value;
		Il2CppCodeGenWriteBarrier((&___faceGroupName_10), value);
	}

	inline static int32_t get_offset_of_U3CObjectU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___U3CObjectU3Ek__BackingField_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CObjectU3Ek__BackingField_11() const { return ___U3CObjectU3Ek__BackingField_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CObjectU3Ek__BackingField_11() { return &___U3CObjectU3Ek__BackingField_11; }
	inline void set_U3CObjectU3Ek__BackingField_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CObjectU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_exhibitItemOffset_12() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___exhibitItemOffset_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_exhibitItemOffset_12() const { return ___exhibitItemOffset_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_exhibitItemOffset_12() { return &___exhibitItemOffset_12; }
	inline void set_exhibitItemOffset_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___exhibitItemOffset_12 = value;
	}

	inline static int32_t get_offset_of_exhibitItemBorder_13() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___exhibitItemBorder_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_exhibitItemBorder_13() const { return ___exhibitItemBorder_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_exhibitItemBorder_13() { return &___exhibitItemBorder_13; }
	inline void set_exhibitItemBorder_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___exhibitItemBorder_13 = value;
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A, ___U3CInitializedU3Ek__BackingField_14)); }
	inline bool get_U3CInitializedU3Ek__BackingField_14() const { return ___U3CInitializedU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_14() { return &___U3CInitializedU3Ek__BackingField_14; }
	inline void set_U3CInitializedU3Ek__BackingField_14(bool value)
	{
		___U3CInitializedU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXHIBITBOX_T889C2D58A95ED32320CE1B582E8389B95D72DF2A_H
#ifndef EXHIBITBOXMENU_T8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22_H
#define EXHIBITBOXMENU_T8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.ExhibitBoxMenu
struct  ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D MPAR.Common.GameObjects.ExhibitBoxMenu::doneIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___doneIcon_4;
	// UnityEngine.Texture2D MPAR.Common.GameObjects.ExhibitBoxMenu::doneExhibitIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___doneExhibitIcon_5;
	// UnityEngine.Texture2D MPAR.Common.GameObjects.ExhibitBoxMenu::adjustIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___adjustIcon_6;
	// UnityEngine.Texture2D MPAR.Common.GameObjects.ExhibitBoxMenu::removeIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___removeIcon_7;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.GameObjects.ExhibitBoxMenu::doneButton
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___doneButton_8;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.GameObjects.ExhibitBoxMenu::doneExhibitButton
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___doneExhibitButton_9;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.GameObjects.ExhibitBoxMenu::adjustButton
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___adjustButton_10;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.GameObjects.ExhibitBoxMenu::removeButton
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___removeButton_11;
	// MPAR.Common.UI.PortableMenu.PortableMenu MPAR.Common.GameObjects.ExhibitBoxMenu::menu
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B * ___menu_12;
	// MPAR.Common.UI.PortableMenu.PortableMenu MPAR.Common.GameObjects.ExhibitBoxMenu::menuPrefab
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B * ___menuPrefab_13;
	// MPAR.Common.GameObjects.ExhibitBox MPAR.Common.GameObjects.ExhibitBoxMenu::mainBox
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A * ___mainBox_14;
	// UnityEngine.Transform[] MPAR.Common.GameObjects.ExhibitBoxMenu::potentialMenuPositions
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___potentialMenuPositions_15;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.ExhibitBoxMenu::moveVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveVelocity_16;
	// System.Boolean MPAR.Common.GameObjects.ExhibitBoxMenu::initialized
	bool ___initialized_17;
	// MPAR.Common.GameObjects.Factories.PortableMenuFactory MPAR.Common.GameObjects.ExhibitBoxMenu::<MenuFactory>k__BackingField
	PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D * ___U3CMenuFactoryU3Ek__BackingField_18;

public:
	inline static int32_t get_offset_of_doneIcon_4() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___doneIcon_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_doneIcon_4() const { return ___doneIcon_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_doneIcon_4() { return &___doneIcon_4; }
	inline void set_doneIcon_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___doneIcon_4 = value;
		Il2CppCodeGenWriteBarrier((&___doneIcon_4), value);
	}

	inline static int32_t get_offset_of_doneExhibitIcon_5() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___doneExhibitIcon_5)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_doneExhibitIcon_5() const { return ___doneExhibitIcon_5; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_doneExhibitIcon_5() { return &___doneExhibitIcon_5; }
	inline void set_doneExhibitIcon_5(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___doneExhibitIcon_5 = value;
		Il2CppCodeGenWriteBarrier((&___doneExhibitIcon_5), value);
	}

	inline static int32_t get_offset_of_adjustIcon_6() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___adjustIcon_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_adjustIcon_6() const { return ___adjustIcon_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_adjustIcon_6() { return &___adjustIcon_6; }
	inline void set_adjustIcon_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___adjustIcon_6 = value;
		Il2CppCodeGenWriteBarrier((&___adjustIcon_6), value);
	}

	inline static int32_t get_offset_of_removeIcon_7() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___removeIcon_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_removeIcon_7() const { return ___removeIcon_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_removeIcon_7() { return &___removeIcon_7; }
	inline void set_removeIcon_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___removeIcon_7 = value;
		Il2CppCodeGenWriteBarrier((&___removeIcon_7), value);
	}

	inline static int32_t get_offset_of_doneButton_8() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___doneButton_8)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_doneButton_8() const { return ___doneButton_8; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_doneButton_8() { return &___doneButton_8; }
	inline void set_doneButton_8(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___doneButton_8 = value;
		Il2CppCodeGenWriteBarrier((&___doneButton_8), value);
	}

	inline static int32_t get_offset_of_doneExhibitButton_9() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___doneExhibitButton_9)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_doneExhibitButton_9() const { return ___doneExhibitButton_9; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_doneExhibitButton_9() { return &___doneExhibitButton_9; }
	inline void set_doneExhibitButton_9(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___doneExhibitButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___doneExhibitButton_9), value);
	}

	inline static int32_t get_offset_of_adjustButton_10() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___adjustButton_10)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_adjustButton_10() const { return ___adjustButton_10; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_adjustButton_10() { return &___adjustButton_10; }
	inline void set_adjustButton_10(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___adjustButton_10 = value;
		Il2CppCodeGenWriteBarrier((&___adjustButton_10), value);
	}

	inline static int32_t get_offset_of_removeButton_11() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___removeButton_11)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_removeButton_11() const { return ___removeButton_11; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_removeButton_11() { return &___removeButton_11; }
	inline void set_removeButton_11(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___removeButton_11 = value;
		Il2CppCodeGenWriteBarrier((&___removeButton_11), value);
	}

	inline static int32_t get_offset_of_menu_12() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___menu_12)); }
	inline PortableMenu_t1464619C66BEEE803774636017C9557890D6327B * get_menu_12() const { return ___menu_12; }
	inline PortableMenu_t1464619C66BEEE803774636017C9557890D6327B ** get_address_of_menu_12() { return &___menu_12; }
	inline void set_menu_12(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B * value)
	{
		___menu_12 = value;
		Il2CppCodeGenWriteBarrier((&___menu_12), value);
	}

	inline static int32_t get_offset_of_menuPrefab_13() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___menuPrefab_13)); }
	inline PortableMenu_t1464619C66BEEE803774636017C9557890D6327B * get_menuPrefab_13() const { return ___menuPrefab_13; }
	inline PortableMenu_t1464619C66BEEE803774636017C9557890D6327B ** get_address_of_menuPrefab_13() { return &___menuPrefab_13; }
	inline void set_menuPrefab_13(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B * value)
	{
		___menuPrefab_13 = value;
		Il2CppCodeGenWriteBarrier((&___menuPrefab_13), value);
	}

	inline static int32_t get_offset_of_mainBox_14() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___mainBox_14)); }
	inline ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A * get_mainBox_14() const { return ___mainBox_14; }
	inline ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A ** get_address_of_mainBox_14() { return &___mainBox_14; }
	inline void set_mainBox_14(ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A * value)
	{
		___mainBox_14 = value;
		Il2CppCodeGenWriteBarrier((&___mainBox_14), value);
	}

	inline static int32_t get_offset_of_potentialMenuPositions_15() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___potentialMenuPositions_15)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_potentialMenuPositions_15() const { return ___potentialMenuPositions_15; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_potentialMenuPositions_15() { return &___potentialMenuPositions_15; }
	inline void set_potentialMenuPositions_15(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___potentialMenuPositions_15 = value;
		Il2CppCodeGenWriteBarrier((&___potentialMenuPositions_15), value);
	}

	inline static int32_t get_offset_of_moveVelocity_16() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___moveVelocity_16)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveVelocity_16() const { return ___moveVelocity_16; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveVelocity_16() { return &___moveVelocity_16; }
	inline void set_moveVelocity_16(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveVelocity_16 = value;
	}

	inline static int32_t get_offset_of_initialized_17() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___initialized_17)); }
	inline bool get_initialized_17() const { return ___initialized_17; }
	inline bool* get_address_of_initialized_17() { return &___initialized_17; }
	inline void set_initialized_17(bool value)
	{
		___initialized_17 = value;
	}

	inline static int32_t get_offset_of_U3CMenuFactoryU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22, ___U3CMenuFactoryU3Ek__BackingField_18)); }
	inline PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D * get_U3CMenuFactoryU3Ek__BackingField_18() const { return ___U3CMenuFactoryU3Ek__BackingField_18; }
	inline PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D ** get_address_of_U3CMenuFactoryU3Ek__BackingField_18() { return &___U3CMenuFactoryU3Ek__BackingField_18; }
	inline void set_U3CMenuFactoryU3Ek__BackingField_18(PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D * value)
	{
		___U3CMenuFactoryU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuFactoryU3Ek__BackingField_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXHIBITBOXMENU_T8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22_H
#ifndef FIXEDSCALE_TD0A844F7D78D0917F8439BBFF615ADF303447300_H
#define FIXEDSCALE_TD0A844F7D78D0917F8439BBFF615ADF303447300_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.FixedScale
struct  FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform MPAR.Common.GameObjects.FixedScale::parentToIgnore
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___parentToIgnore_4;
	// UnityEngine.Vector3 MPAR.Common.GameObjects.FixedScale::scaleOnAwake
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___scaleOnAwake_5;

public:
	inline static int32_t get_offset_of_parentToIgnore_4() { return static_cast<int32_t>(offsetof(FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300, ___parentToIgnore_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_parentToIgnore_4() const { return ___parentToIgnore_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_parentToIgnore_4() { return &___parentToIgnore_4; }
	inline void set_parentToIgnore_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___parentToIgnore_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentToIgnore_4), value);
	}

	inline static int32_t get_offset_of_scaleOnAwake_5() { return static_cast<int32_t>(offsetof(FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300, ___scaleOnAwake_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_scaleOnAwake_5() const { return ___scaleOnAwake_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_scaleOnAwake_5() { return &___scaleOnAwake_5; }
	inline void set_scaleOnAwake_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___scaleOnAwake_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDSCALE_TD0A844F7D78D0917F8439BBFF615ADF303447300_H
#ifndef SINGLETON_1_T0ECB789469C68CCD30B77CE85E9AEA1234499C45_H
#define SINGLETON_1_T0ECB789469C68CCD30B77CE85E9AEA1234499C45_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Singleton`1<MPAR.Common.GameObjects.Utility.PrefabUtility>
struct  Singleton_1_t0ECB789469C68CCD30B77CE85E9AEA1234499C45  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t0ECB789469C68CCD30B77CE85E9AEA1234499C45_StaticFields
{
public:
	// T MPAR.Common.GameObjects.Singleton`1::instance
	PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2 * ___instance_4;
	// System.Boolean MPAR.Common.GameObjects.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t0ECB789469C68CCD30B77CE85E9AEA1234499C45_StaticFields, ___instance_4)); }
	inline PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2 * get_instance_4() const { return ___instance_4; }
	inline PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t0ECB789469C68CCD30B77CE85E9AEA1234499C45_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T0ECB789469C68CCD30B77CE85E9AEA1234499C45_H
#ifndef CLICKABLE_T03DB358E3736E8AF9287E385A55E6842DB6ECBFA_H
#define CLICKABLE_T03DB358E3736E8AF9287E385A55E6842DB6ECBFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Clickable
struct  Clickable_t03DB358E3736E8AF9287E385A55E6842DB6ECBFA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Input.IPlatformDependentInput MPAR.Common.Input.Clickable::<Input>k__BackingField
	RuntimeObject* ___U3CInputU3Ek__BackingField_4;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.Input.Clickable::<Gestures>k__BackingField
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___U3CGesturesU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CInputU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Clickable_t03DB358E3736E8AF9287E385A55E6842DB6ECBFA, ___U3CInputU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CInputU3Ek__BackingField_4() const { return ___U3CInputU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CInputU3Ek__BackingField_4() { return &___U3CInputU3Ek__BackingField_4; }
	inline void set_U3CInputU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CInputU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CGesturesU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Clickable_t03DB358E3736E8AF9287E385A55E6842DB6ECBFA, ___U3CGesturesU3Ek__BackingField_5)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_U3CGesturesU3Ek__BackingField_5() const { return ___U3CGesturesU3Ek__BackingField_5; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_U3CGesturesU3Ek__BackingField_5() { return &___U3CGesturesU3Ek__BackingField_5; }
	inline void set_U3CGesturesU3Ek__BackingField_5(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___U3CGesturesU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGesturesU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLICKABLE_T03DB358E3736E8AF9287E385A55E6842DB6ECBFA_H
#ifndef GESTUREMANAGER_T99772F3FD58568F0F15CF073D77397C18B0C1909_H
#define GESTUREMANAGER_T99772F3FD58568F0F15CF073D77397C18B0C1909_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Input.Utility.GestureManager
struct  GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean MPAR.Common.Input.Utility.GestureManager::<IsManipulating>k__BackingField
	bool ___U3CIsManipulatingU3Ek__BackingField_4;
	// UnityEngine.Vector3 MPAR.Common.Input.Utility.GestureManager::<ManipulationDelta>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CManipulationDeltaU3Ek__BackingField_5;
	// System.EventHandler`1<MPAR.Common.Input.Utility.ManipulationDeltaEventArgs> MPAR.Common.Input.Utility.GestureManager::DragUpdatedEvent
	EventHandler_1_tD38F5C2F93D4125643F8527AB5BC06F9748AB8A3 * ___DragUpdatedEvent_6;
	// System.Collections.Generic.List`1<MPAR.Common.Input.IGameObjectClickListener> MPAR.Common.Input.Utility.GestureManager::<ObjectClickListeners>k__BackingField
	List_1_t5BB3C8E8E3C6307C5ED37D3E4521E35F0A1366B9 * ___U3CObjectClickListenersU3Ek__BackingField_7;
	// MPAR.Common.Input.IGameObjectClickListener MPAR.Common.Input.Utility.GestureManager::scriptManagerClickListener
	RuntimeObject* ___scriptManagerClickListener_8;
	// MPAR.Common.Input.IPlatformDependentInput MPAR.Common.Input.Utility.GestureManager::<_input>k__BackingField
	RuntimeObject* ___U3C_inputU3Ek__BackingField_9;
	// UnityEngine.UI.GraphicRaycaster MPAR.Common.Input.Utility.GestureManager::graphicRaycaster
	GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * ___graphicRaycaster_10;

public:
	inline static int32_t get_offset_of_U3CIsManipulatingU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___U3CIsManipulatingU3Ek__BackingField_4)); }
	inline bool get_U3CIsManipulatingU3Ek__BackingField_4() const { return ___U3CIsManipulatingU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CIsManipulatingU3Ek__BackingField_4() { return &___U3CIsManipulatingU3Ek__BackingField_4; }
	inline void set_U3CIsManipulatingU3Ek__BackingField_4(bool value)
	{
		___U3CIsManipulatingU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CManipulationDeltaU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___U3CManipulationDeltaU3Ek__BackingField_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CManipulationDeltaU3Ek__BackingField_5() const { return ___U3CManipulationDeltaU3Ek__BackingField_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CManipulationDeltaU3Ek__BackingField_5() { return &___U3CManipulationDeltaU3Ek__BackingField_5; }
	inline void set_U3CManipulationDeltaU3Ek__BackingField_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CManipulationDeltaU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_DragUpdatedEvent_6() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___DragUpdatedEvent_6)); }
	inline EventHandler_1_tD38F5C2F93D4125643F8527AB5BC06F9748AB8A3 * get_DragUpdatedEvent_6() const { return ___DragUpdatedEvent_6; }
	inline EventHandler_1_tD38F5C2F93D4125643F8527AB5BC06F9748AB8A3 ** get_address_of_DragUpdatedEvent_6() { return &___DragUpdatedEvent_6; }
	inline void set_DragUpdatedEvent_6(EventHandler_1_tD38F5C2F93D4125643F8527AB5BC06F9748AB8A3 * value)
	{
		___DragUpdatedEvent_6 = value;
		Il2CppCodeGenWriteBarrier((&___DragUpdatedEvent_6), value);
	}

	inline static int32_t get_offset_of_U3CObjectClickListenersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___U3CObjectClickListenersU3Ek__BackingField_7)); }
	inline List_1_t5BB3C8E8E3C6307C5ED37D3E4521E35F0A1366B9 * get_U3CObjectClickListenersU3Ek__BackingField_7() const { return ___U3CObjectClickListenersU3Ek__BackingField_7; }
	inline List_1_t5BB3C8E8E3C6307C5ED37D3E4521E35F0A1366B9 ** get_address_of_U3CObjectClickListenersU3Ek__BackingField_7() { return &___U3CObjectClickListenersU3Ek__BackingField_7; }
	inline void set_U3CObjectClickListenersU3Ek__BackingField_7(List_1_t5BB3C8E8E3C6307C5ED37D3E4521E35F0A1366B9 * value)
	{
		___U3CObjectClickListenersU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectClickListenersU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_scriptManagerClickListener_8() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___scriptManagerClickListener_8)); }
	inline RuntimeObject* get_scriptManagerClickListener_8() const { return ___scriptManagerClickListener_8; }
	inline RuntimeObject** get_address_of_scriptManagerClickListener_8() { return &___scriptManagerClickListener_8; }
	inline void set_scriptManagerClickListener_8(RuntimeObject* value)
	{
		___scriptManagerClickListener_8 = value;
		Il2CppCodeGenWriteBarrier((&___scriptManagerClickListener_8), value);
	}

	inline static int32_t get_offset_of_U3C_inputU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___U3C_inputU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3C_inputU3Ek__BackingField_9() const { return ___U3C_inputU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3C_inputU3Ek__BackingField_9() { return &___U3C_inputU3Ek__BackingField_9; }
	inline void set_U3C_inputU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3C_inputU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3C_inputU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_graphicRaycaster_10() { return static_cast<int32_t>(offsetof(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909, ___graphicRaycaster_10)); }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * get_graphicRaycaster_10() const { return ___graphicRaycaster_10; }
	inline GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 ** get_address_of_graphicRaycaster_10() { return &___graphicRaycaster_10; }
	inline void set_graphicRaycaster_10(GraphicRaycaster_t9AA334998113578A7FC0B27D7D6FEF19E74B9D83 * value)
	{
		___graphicRaycaster_10 = value;
		Il2CppCodeGenWriteBarrier((&___graphicRaycaster_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GESTUREMANAGER_T99772F3FD58568F0F15CF073D77397C18B0C1909_H
#ifndef ANCHOR_TB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A_H
#define ANCHOR_TB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.Anchor
struct  Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Persistence.IPlatformDependentAnchorManager MPAR.Common.Persistence.Anchor::<AnchorManager>k__BackingField
	RuntimeObject* ___U3CAnchorManagerU3Ek__BackingField_4;
	// System.String MPAR.Common.Persistence.Anchor::<AnchorName>k__BackingField
	String_t* ___U3CAnchorNameU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CAnchorManagerU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A, ___U3CAnchorManagerU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CAnchorManagerU3Ek__BackingField_4() const { return ___U3CAnchorManagerU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CAnchorManagerU3Ek__BackingField_4() { return &___U3CAnchorManagerU3Ek__BackingField_4; }
	inline void set_U3CAnchorManagerU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CAnchorManagerU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorManagerU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CAnchorNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A, ___U3CAnchorNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CAnchorNameU3Ek__BackingField_5() const { return ___U3CAnchorNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CAnchorNameU3Ek__BackingField_5() { return &___U3CAnchorNameU3Ek__BackingField_5; }
	inline void set_U3CAnchorNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CAnchorNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAnchorNameU3Ek__BackingField_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ANCHOR_TB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A_H
#ifndef CACHEMANAGER_T93F8C94EAD497E054A8ED4B29E5470DC2017007B_H
#define CACHEMANAGER_T93F8C94EAD497E054A8ED4B29E5470DC2017007B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Persistence.CacheManager
struct  CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<System.String> MPAR.Common.Persistence.CacheManager::<ToCache>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CToCacheU3Ek__BackingField_4;
	// System.Collections.Generic.List`1<System.String> MPAR.Common.Persistence.CacheManager::<CurrentlyCaching>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CCurrentlyCachingU3Ek__BackingField_5;
	// System.String MPAR.Common.Persistence.CacheManager::<CacheDirectory>k__BackingField
	String_t* ___U3CCacheDirectoryU3Ek__BackingField_6;
	// System.String MPAR.Common.Persistence.CacheManager::<CacheRefPath>k__BackingField
	String_t* ___U3CCacheRefPathU3Ek__BackingField_7;
	// System.Boolean MPAR.Common.Persistence.CacheManager::overwriteNextItem
	bool ___overwriteNextItem_8;
	// System.String MPAR.Common.Persistence.CacheManager::scriptId
	String_t* ___scriptId_9;

public:
	inline static int32_t get_offset_of_U3CToCacheU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B, ___U3CToCacheU3Ek__BackingField_4)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CToCacheU3Ek__BackingField_4() const { return ___U3CToCacheU3Ek__BackingField_4; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CToCacheU3Ek__BackingField_4() { return &___U3CToCacheU3Ek__BackingField_4; }
	inline void set_U3CToCacheU3Ek__BackingField_4(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CToCacheU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToCacheU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CCurrentlyCachingU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B, ___U3CCurrentlyCachingU3Ek__BackingField_5)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CCurrentlyCachingU3Ek__BackingField_5() const { return ___U3CCurrentlyCachingU3Ek__BackingField_5; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CCurrentlyCachingU3Ek__BackingField_5() { return &___U3CCurrentlyCachingU3Ek__BackingField_5; }
	inline void set_U3CCurrentlyCachingU3Ek__BackingField_5(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CCurrentlyCachingU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentlyCachingU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCacheDirectoryU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B, ___U3CCacheDirectoryU3Ek__BackingField_6)); }
	inline String_t* get_U3CCacheDirectoryU3Ek__BackingField_6() const { return ___U3CCacheDirectoryU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CCacheDirectoryU3Ek__BackingField_6() { return &___U3CCacheDirectoryU3Ek__BackingField_6; }
	inline void set_U3CCacheDirectoryU3Ek__BackingField_6(String_t* value)
	{
		___U3CCacheDirectoryU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheDirectoryU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CCacheRefPathU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B, ___U3CCacheRefPathU3Ek__BackingField_7)); }
	inline String_t* get_U3CCacheRefPathU3Ek__BackingField_7() const { return ___U3CCacheRefPathU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CCacheRefPathU3Ek__BackingField_7() { return &___U3CCacheRefPathU3Ek__BackingField_7; }
	inline void set_U3CCacheRefPathU3Ek__BackingField_7(String_t* value)
	{
		___U3CCacheRefPathU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheRefPathU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_overwriteNextItem_8() { return static_cast<int32_t>(offsetof(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B, ___overwriteNextItem_8)); }
	inline bool get_overwriteNextItem_8() const { return ___overwriteNextItem_8; }
	inline bool* get_address_of_overwriteNextItem_8() { return &___overwriteNextItem_8; }
	inline void set_overwriteNextItem_8(bool value)
	{
		___overwriteNextItem_8 = value;
	}

	inline static int32_t get_offset_of_scriptId_9() { return static_cast<int32_t>(offsetof(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B, ___scriptId_9)); }
	inline String_t* get_scriptId_9() const { return ___scriptId_9; }
	inline String_t** get_address_of_scriptId_9() { return &___scriptId_9; }
	inline void set_scriptId_9(String_t* value)
	{
		___scriptId_9 = value;
		Il2CppCodeGenWriteBarrier((&___scriptId_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEMANAGER_T93F8C94EAD497E054A8ED4B29E5470DC2017007B_H
#ifndef PLANE_T02B7D4B67352F067291AC498C9478D33B3C97BA8_H
#define PLANE_T02B7D4B67352F067291AC498C9478D33B3C97BA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.PlaneDetection.Plane
struct  Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Scripting.API.Objects.Vector MPAR.Common.PlaneDetection.Plane::<position>k__BackingField
	Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * ___U3CpositionU3Ek__BackingField_4;
	// MPAR.Common.Scripting.API.Objects.Vector MPAR.Common.PlaneDetection.Plane::<rotation>k__BackingField
	Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * ___U3CrotationU3Ek__BackingField_5;
	// MPAR.Common.Scripting.API.Objects.Vector MPAR.Common.PlaneDetection.Plane::<size>k__BackingField
	Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * ___U3CsizeU3Ek__BackingField_6;
	// MPAR.Common.Scripting.API.Objects.Vector MPAR.Common.PlaneDetection.Plane::<forward>k__BackingField
	Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * ___U3CforwardU3Ek__BackingField_7;
	// MPAR.Common.Scripting.API.Objects.MixiplyObject MPAR.Common.PlaneDetection.Plane::<planeObject>k__BackingField
	MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * ___U3CplaneObjectU3Ek__BackingField_8;
	// UnityEngine.Transform MPAR.Common.PlaneDetection.Plane::anchorTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___anchorTransform_9;

public:
	inline static int32_t get_offset_of_U3CpositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8, ___U3CpositionU3Ek__BackingField_4)); }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * get_U3CpositionU3Ek__BackingField_4() const { return ___U3CpositionU3Ek__BackingField_4; }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 ** get_address_of_U3CpositionU3Ek__BackingField_4() { return &___U3CpositionU3Ek__BackingField_4; }
	inline void set_U3CpositionU3Ek__BackingField_4(Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * value)
	{
		___U3CpositionU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CpositionU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CrotationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8, ___U3CrotationU3Ek__BackingField_5)); }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * get_U3CrotationU3Ek__BackingField_5() const { return ___U3CrotationU3Ek__BackingField_5; }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 ** get_address_of_U3CrotationU3Ek__BackingField_5() { return &___U3CrotationU3Ek__BackingField_5; }
	inline void set_U3CrotationU3Ek__BackingField_5(Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * value)
	{
		___U3CrotationU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrotationU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CsizeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8, ___U3CsizeU3Ek__BackingField_6)); }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * get_U3CsizeU3Ek__BackingField_6() const { return ___U3CsizeU3Ek__BackingField_6; }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 ** get_address_of_U3CsizeU3Ek__BackingField_6() { return &___U3CsizeU3Ek__BackingField_6; }
	inline void set_U3CsizeU3Ek__BackingField_6(Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * value)
	{
		___U3CsizeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsizeU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CforwardU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8, ___U3CforwardU3Ek__BackingField_7)); }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * get_U3CforwardU3Ek__BackingField_7() const { return ___U3CforwardU3Ek__BackingField_7; }
	inline Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 ** get_address_of_U3CforwardU3Ek__BackingField_7() { return &___U3CforwardU3Ek__BackingField_7; }
	inline void set_U3CforwardU3Ek__BackingField_7(Vector_t00A95500B27F53ADE6532C18B38E4E68ADBE4207 * value)
	{
		___U3CforwardU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CforwardU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CplaneObjectU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8, ___U3CplaneObjectU3Ek__BackingField_8)); }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * get_U3CplaneObjectU3Ek__BackingField_8() const { return ___U3CplaneObjectU3Ek__BackingField_8; }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B ** get_address_of_U3CplaneObjectU3Ek__BackingField_8() { return &___U3CplaneObjectU3Ek__BackingField_8; }
	inline void set_U3CplaneObjectU3Ek__BackingField_8(MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * value)
	{
		___U3CplaneObjectU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplaneObjectU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_anchorTransform_9() { return static_cast<int32_t>(offsetof(Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8, ___anchorTransform_9)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_anchorTransform_9() const { return ___anchorTransform_9; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_anchorTransform_9() { return &___anchorTransform_9; }
	inline void set_anchorTransform_9(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___anchorTransform_9 = value;
		Il2CppCodeGenWriteBarrier((&___anchorTransform_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLANE_T02B7D4B67352F067291AC498C9478D33B3C97BA8_H
#ifndef EVENTITEM_TAEF9C49CC34F9E274BD47CD031F87A64E33E478D_H
#define EVENTITEM_TAEF9C49CC34F9E274BD47CD031F87A64E33E478D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.EventItem
struct  EventItem_tAEF9C49CC34F9E274BD47CD031F87A64E33E478D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTITEM_TAEF9C49CC34F9E274BD47CD031F87A64E33E478D_H
#ifndef MENUMANAGER_T03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6_H
#define MENUMANAGER_T03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.MenuManager
struct  MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.UI.IPlatformDependentMenu MPAR.Common.UI.MenuManager::menu
	RuntimeObject* ___menu_4;

public:
	inline static int32_t get_offset_of_menu_4() { return static_cast<int32_t>(offsetof(MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6, ___menu_4)); }
	inline RuntimeObject* get_menu_4() const { return ___menu_4; }
	inline RuntimeObject** get_address_of_menu_4() { return &___menu_4; }
	inline void set_menu_4(RuntimeObject* value)
	{
		___menu_4 = value;
		Il2CppCodeGenWriteBarrier((&___menu_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUMANAGER_T03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6_H
#ifndef ONCLICKEVENT_TF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9_H
#define ONCLICKEVENT_TF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.OnClickEvent
struct  OnClickEvent_tF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action MPAR.Common.UI.OnClickEvent::ClickAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___ClickAction_4;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.UI.OnClickEvent::Gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___Gestures_5;

public:
	inline static int32_t get_offset_of_ClickAction_4() { return static_cast<int32_t>(offsetof(OnClickEvent_tF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9, ___ClickAction_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_ClickAction_4() const { return ___ClickAction_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_ClickAction_4() { return &___ClickAction_4; }
	inline void set_ClickAction_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___ClickAction_4 = value;
		Il2CppCodeGenWriteBarrier((&___ClickAction_4), value);
	}

	inline static int32_t get_offset_of_Gestures_5() { return static_cast<int32_t>(offsetof(OnClickEvent_tF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9, ___Gestures_5)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_Gestures_5() const { return ___Gestures_5; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_Gestures_5() { return &___Gestures_5; }
	inline void set_Gestures_5(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___Gestures_5 = value;
		Il2CppCodeGenWriteBarrier((&___Gestures_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKEVENT_TF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9_H
#ifndef ONSCREENLOGGER_T18A5079BF4094BF025136ECA260BA5429FC61E23_H
#define ONSCREENLOGGER_T18A5079BF4094BF025136ECA260BA5429FC61E23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.OnScreenLogger
struct  OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// TMPro.TextMeshPro MPAR.Common.UI.OnScreenLogger::<TextMeshPro>k__BackingField
	TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * ___U3CTextMeshProU3Ek__BackingField_4;
	// System.String MPAR.Common.UI.OnScreenLogger::logFilePath
	String_t* ___logFilePath_5;
	// System.Boolean MPAR.Common.UI.OnScreenLogger::FileLoggingEnabled
	bool ___FileLoggingEnabled_6;

public:
	inline static int32_t get_offset_of_U3CTextMeshProU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23, ___U3CTextMeshProU3Ek__BackingField_4)); }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * get_U3CTextMeshProU3Ek__BackingField_4() const { return ___U3CTextMeshProU3Ek__BackingField_4; }
	inline TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 ** get_address_of_U3CTextMeshProU3Ek__BackingField_4() { return &___U3CTextMeshProU3Ek__BackingField_4; }
	inline void set_U3CTextMeshProU3Ek__BackingField_4(TextMeshPro_t6FF60D9DCAF295045FE47C014CC855C5784752E2 * value)
	{
		___U3CTextMeshProU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextMeshProU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_logFilePath_5() { return static_cast<int32_t>(offsetof(OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23, ___logFilePath_5)); }
	inline String_t* get_logFilePath_5() const { return ___logFilePath_5; }
	inline String_t** get_address_of_logFilePath_5() { return &___logFilePath_5; }
	inline void set_logFilePath_5(String_t* value)
	{
		___logFilePath_5 = value;
		Il2CppCodeGenWriteBarrier((&___logFilePath_5), value);
	}

	inline static int32_t get_offset_of_FileLoggingEnabled_6() { return static_cast<int32_t>(offsetof(OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23, ___FileLoggingEnabled_6)); }
	inline bool get_FileLoggingEnabled_6() const { return ___FileLoggingEnabled_6; }
	inline bool* get_address_of_FileLoggingEnabled_6() { return &___FileLoggingEnabled_6; }
	inline void set_FileLoggingEnabled_6(bool value)
	{
		___FileLoggingEnabled_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONSCREENLOGGER_T18A5079BF4094BF025136ECA260BA5429FC61E23_H
#ifndef PORTABLEMENU_T1464619C66BEEE803774636017C9557890D6327B_H
#define PORTABLEMENU_T1464619C66BEEE803774636017C9557890D6327B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.PortableMenu.PortableMenu
struct  PortableMenu_t1464619C66BEEE803774636017C9557890D6327B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.HashSet`1<MPAR.Common.UI.PortableMenu.PortableMenuItem> MPAR.Common.UI.PortableMenu.PortableMenu::menuItems
	HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * ___menuItems_4;
	// System.Collections.Generic.HashSet`1<MPAR.Common.UI.PortableMenu.PortableMenuItem> MPAR.Common.UI.PortableMenu.PortableMenu::activeMenuItems
	HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * ___activeMenuItems_5;
	// System.Collections.Generic.HashSet`1<MPAR.Common.UI.PortableMenu.PortableMenuItem> MPAR.Common.UI.PortableMenu.PortableMenu::minimizedItems
	HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * ___minimizedItems_6;
	// System.Int32 MPAR.Common.UI.PortableMenu.PortableMenu::previousActiveSize
	int32_t ___previousActiveSize_7;
	// UnityEngine.Vector2 MPAR.Common.UI.PortableMenu.PortableMenu::menuItemSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___menuItemSize_8;
	// UnityEngine.Texture2D MPAR.Common.UI.PortableMenu.PortableMenu::expandMenuIcon
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___expandMenuIcon_9;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.UI.PortableMenu.PortableMenu::expandMenuItem
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___expandMenuItem_10;
	// MPAR.Common.UI.PortableMenu.PortableMenuItem MPAR.Common.UI.PortableMenu.PortableMenu::menuItemPrefab
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * ___menuItemPrefab_11;
	// MPAR.Common.GameObjects.Factories.PortableMenuItemFactory MPAR.Common.UI.PortableMenu.PortableMenu::<MenuItemFactory>k__BackingField
	PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D * ___U3CMenuItemFactoryU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_menuItems_4() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___menuItems_4)); }
	inline HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * get_menuItems_4() const { return ___menuItems_4; }
	inline HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 ** get_address_of_menuItems_4() { return &___menuItems_4; }
	inline void set_menuItems_4(HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * value)
	{
		___menuItems_4 = value;
		Il2CppCodeGenWriteBarrier((&___menuItems_4), value);
	}

	inline static int32_t get_offset_of_activeMenuItems_5() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___activeMenuItems_5)); }
	inline HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * get_activeMenuItems_5() const { return ___activeMenuItems_5; }
	inline HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 ** get_address_of_activeMenuItems_5() { return &___activeMenuItems_5; }
	inline void set_activeMenuItems_5(HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * value)
	{
		___activeMenuItems_5 = value;
		Il2CppCodeGenWriteBarrier((&___activeMenuItems_5), value);
	}

	inline static int32_t get_offset_of_minimizedItems_6() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___minimizedItems_6)); }
	inline HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * get_minimizedItems_6() const { return ___minimizedItems_6; }
	inline HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 ** get_address_of_minimizedItems_6() { return &___minimizedItems_6; }
	inline void set_minimizedItems_6(HashSet_1_tBB5ACA8AFCD8ED4236846706C4C76416F56AE533 * value)
	{
		___minimizedItems_6 = value;
		Il2CppCodeGenWriteBarrier((&___minimizedItems_6), value);
	}

	inline static int32_t get_offset_of_previousActiveSize_7() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___previousActiveSize_7)); }
	inline int32_t get_previousActiveSize_7() const { return ___previousActiveSize_7; }
	inline int32_t* get_address_of_previousActiveSize_7() { return &___previousActiveSize_7; }
	inline void set_previousActiveSize_7(int32_t value)
	{
		___previousActiveSize_7 = value;
	}

	inline static int32_t get_offset_of_menuItemSize_8() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___menuItemSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_menuItemSize_8() const { return ___menuItemSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_menuItemSize_8() { return &___menuItemSize_8; }
	inline void set_menuItemSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___menuItemSize_8 = value;
	}

	inline static int32_t get_offset_of_expandMenuIcon_9() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___expandMenuIcon_9)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_expandMenuIcon_9() const { return ___expandMenuIcon_9; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_expandMenuIcon_9() { return &___expandMenuIcon_9; }
	inline void set_expandMenuIcon_9(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___expandMenuIcon_9 = value;
		Il2CppCodeGenWriteBarrier((&___expandMenuIcon_9), value);
	}

	inline static int32_t get_offset_of_expandMenuItem_10() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___expandMenuItem_10)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_expandMenuItem_10() const { return ___expandMenuItem_10; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_expandMenuItem_10() { return &___expandMenuItem_10; }
	inline void set_expandMenuItem_10(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___expandMenuItem_10 = value;
		Il2CppCodeGenWriteBarrier((&___expandMenuItem_10), value);
	}

	inline static int32_t get_offset_of_menuItemPrefab_11() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___menuItemPrefab_11)); }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * get_menuItemPrefab_11() const { return ___menuItemPrefab_11; }
	inline PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 ** get_address_of_menuItemPrefab_11() { return &___menuItemPrefab_11; }
	inline void set_menuItemPrefab_11(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867 * value)
	{
		___menuItemPrefab_11 = value;
		Il2CppCodeGenWriteBarrier((&___menuItemPrefab_11), value);
	}

	inline static int32_t get_offset_of_U3CMenuItemFactoryU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(PortableMenu_t1464619C66BEEE803774636017C9557890D6327B, ___U3CMenuItemFactoryU3Ek__BackingField_12)); }
	inline PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D * get_U3CMenuItemFactoryU3Ek__BackingField_12() const { return ___U3CMenuItemFactoryU3Ek__BackingField_12; }
	inline PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D ** get_address_of_U3CMenuItemFactoryU3Ek__BackingField_12() { return &___U3CMenuItemFactoryU3Ek__BackingField_12; }
	inline void set_U3CMenuItemFactoryU3Ek__BackingField_12(PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D * value)
	{
		___U3CMenuItemFactoryU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuItemFactoryU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PORTABLEMENU_T1464619C66BEEE803774636017C9557890D6327B_H
#ifndef PORTABLEMENUITEM_T0E428F2A88C511B96FCEC372E47F64C08EF32867_H
#define PORTABLEMENUITEM_T0E428F2A88C511B96FCEC372E47F64C08EF32867_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.UI.PortableMenu.PortableMenuItem
struct  PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Texture2D MPAR.Common.UI.PortableMenu.PortableMenuItem::<icon>k__BackingField
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___U3CiconU3Ek__BackingField_4;
	// System.String MPAR.Common.UI.PortableMenu.PortableMenuItem::text
	String_t* ___text_5;
	// UnityEngine.Texture2D MPAR.Common.UI.PortableMenu.PortableMenuItem::bgTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___bgTexture_6;
	// UnityEngine.Texture2D MPAR.Common.UI.PortableMenu.PortableMenuItem::bgTextureHighlighted
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___bgTextureHighlighted_7;
	// UnityEngine.Material MPAR.Common.UI.PortableMenu.PortableMenuItem::bgMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___bgMaterial_8;
	// UnityEngine.Color MPAR.Common.UI.PortableMenu.PortableMenuItem::emissionHighlighted
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___emissionHighlighted_9;
	// UnityEngine.Color MPAR.Common.UI.PortableMenu.PortableMenuItem::emissionNormal
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___emissionNormal_10;
	// System.Action MPAR.Common.UI.PortableMenu.PortableMenuItem::<action>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CactionU3Ek__BackingField_11;
	// System.EventHandler`1<MPAR.Common.UI.EventActionArgs> MPAR.Common.UI.PortableMenu.PortableMenuItem::ActionPerformed
	EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 * ___ActionPerformed_12;
	// MPAR.Common.Input.IPlatformDependentInput MPAR.Common.UI.PortableMenu.PortableMenuItem::<Input>k__BackingField
	RuntimeObject* ___U3CInputU3Ek__BackingField_13;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Common.UI.PortableMenu.PortableMenuItem::<Gestures>k__BackingField
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___U3CGesturesU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_U3CiconU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___U3CiconU3Ek__BackingField_4)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_U3CiconU3Ek__BackingField_4() const { return ___U3CiconU3Ek__BackingField_4; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_U3CiconU3Ek__BackingField_4() { return &___U3CiconU3Ek__BackingField_4; }
	inline void set_U3CiconU3Ek__BackingField_4(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___U3CiconU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CiconU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_text_5() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___text_5)); }
	inline String_t* get_text_5() const { return ___text_5; }
	inline String_t** get_address_of_text_5() { return &___text_5; }
	inline void set_text_5(String_t* value)
	{
		___text_5 = value;
		Il2CppCodeGenWriteBarrier((&___text_5), value);
	}

	inline static int32_t get_offset_of_bgTexture_6() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___bgTexture_6)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_bgTexture_6() const { return ___bgTexture_6; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_bgTexture_6() { return &___bgTexture_6; }
	inline void set_bgTexture_6(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___bgTexture_6 = value;
		Il2CppCodeGenWriteBarrier((&___bgTexture_6), value);
	}

	inline static int32_t get_offset_of_bgTextureHighlighted_7() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___bgTextureHighlighted_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_bgTextureHighlighted_7() const { return ___bgTextureHighlighted_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_bgTextureHighlighted_7() { return &___bgTextureHighlighted_7; }
	inline void set_bgTextureHighlighted_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___bgTextureHighlighted_7 = value;
		Il2CppCodeGenWriteBarrier((&___bgTextureHighlighted_7), value);
	}

	inline static int32_t get_offset_of_bgMaterial_8() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___bgMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_bgMaterial_8() const { return ___bgMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_bgMaterial_8() { return &___bgMaterial_8; }
	inline void set_bgMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___bgMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((&___bgMaterial_8), value);
	}

	inline static int32_t get_offset_of_emissionHighlighted_9() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___emissionHighlighted_9)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_emissionHighlighted_9() const { return ___emissionHighlighted_9; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_emissionHighlighted_9() { return &___emissionHighlighted_9; }
	inline void set_emissionHighlighted_9(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___emissionHighlighted_9 = value;
	}

	inline static int32_t get_offset_of_emissionNormal_10() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___emissionNormal_10)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_emissionNormal_10() const { return ___emissionNormal_10; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_emissionNormal_10() { return &___emissionNormal_10; }
	inline void set_emissionNormal_10(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___emissionNormal_10 = value;
	}

	inline static int32_t get_offset_of_U3CactionU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___U3CactionU3Ek__BackingField_11)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CactionU3Ek__BackingField_11() const { return ___U3CactionU3Ek__BackingField_11; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CactionU3Ek__BackingField_11() { return &___U3CactionU3Ek__BackingField_11; }
	inline void set_U3CactionU3Ek__BackingField_11(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CactionU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CactionU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_ActionPerformed_12() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___ActionPerformed_12)); }
	inline EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 * get_ActionPerformed_12() const { return ___ActionPerformed_12; }
	inline EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 ** get_address_of_ActionPerformed_12() { return &___ActionPerformed_12; }
	inline void set_ActionPerformed_12(EventHandler_1_t28058BB22451F0F583F3F63C15CBF591BCD18E50 * value)
	{
		___ActionPerformed_12 = value;
		Il2CppCodeGenWriteBarrier((&___ActionPerformed_12), value);
	}

	inline static int32_t get_offset_of_U3CInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___U3CInputU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CInputU3Ek__BackingField_13() const { return ___U3CInputU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CInputU3Ek__BackingField_13() { return &___U3CInputU3Ek__BackingField_13; }
	inline void set_U3CInputU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CInputU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInputU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CGesturesU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867, ___U3CGesturesU3Ek__BackingField_14)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_U3CGesturesU3Ek__BackingField_14() const { return ___U3CGesturesU3Ek__BackingField_14; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_U3CGesturesU3Ek__BackingField_14() { return &___U3CGesturesU3Ek__BackingField_14; }
	inline void set_U3CGesturesU3Ek__BackingField_14(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___U3CGesturesU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGesturesU3Ek__BackingField_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PORTABLEMENUITEM_T0E428F2A88C511B96FCEC372E47F64C08EF32867_H
#ifndef BILLBOARD_TD4D6D6E7B30BB56685DFE762B93BF784A75644DD_H
#define BILLBOARD_TD4D6D6E7B30BB56685DFE762B93BF784A75644DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.Billboard
struct  Billboard_tD4D6D6E7B30BB56685DFE762B93BF784A75644DD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Utility.PivotAxis MPAR.Common.Utility.Billboard::PivotAxis
	int32_t ___PivotAxis_4;
	// UnityEngine.Transform MPAR.Common.Utility.Billboard::TargetTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___TargetTransform_5;

public:
	inline static int32_t get_offset_of_PivotAxis_4() { return static_cast<int32_t>(offsetof(Billboard_tD4D6D6E7B30BB56685DFE762B93BF784A75644DD, ___PivotAxis_4)); }
	inline int32_t get_PivotAxis_4() const { return ___PivotAxis_4; }
	inline int32_t* get_address_of_PivotAxis_4() { return &___PivotAxis_4; }
	inline void set_PivotAxis_4(int32_t value)
	{
		___PivotAxis_4 = value;
	}

	inline static int32_t get_offset_of_TargetTransform_5() { return static_cast<int32_t>(offsetof(Billboard_tD4D6D6E7B30BB56685DFE762B93BF784A75644DD, ___TargetTransform_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_TargetTransform_5() const { return ___TargetTransform_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_TargetTransform_5() { return &___TargetTransform_5; }
	inline void set_TargetTransform_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___TargetTransform_5 = value;
		Il2CppCodeGenWriteBarrier((&___TargetTransform_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BILLBOARD_TD4D6D6E7B30BB56685DFE762B93BF784A75644DD_H
#ifndef FIXEDANGULARSIZE_TB822C9FDA10A7411779C35C76B76612D527F459C_H
#define FIXEDANGULARSIZE_TB822C9FDA10A7411779C35C76B76612D527F459C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.FixedAngularSize
struct  FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MPAR.Common.Utility.FixedAngularSize::SizeRatio
	float ___SizeRatio_4;
	// System.Single MPAR.Common.Utility.FixedAngularSize::startingDistance
	float ___startingDistance_5;
	// UnityEngine.Vector3 MPAR.Common.Utility.FixedAngularSize::startingScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___startingScale_6;

public:
	inline static int32_t get_offset_of_SizeRatio_4() { return static_cast<int32_t>(offsetof(FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C, ___SizeRatio_4)); }
	inline float get_SizeRatio_4() const { return ___SizeRatio_4; }
	inline float* get_address_of_SizeRatio_4() { return &___SizeRatio_4; }
	inline void set_SizeRatio_4(float value)
	{
		___SizeRatio_4 = value;
	}

	inline static int32_t get_offset_of_startingDistance_5() { return static_cast<int32_t>(offsetof(FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C, ___startingDistance_5)); }
	inline float get_startingDistance_5() const { return ___startingDistance_5; }
	inline float* get_address_of_startingDistance_5() { return &___startingDistance_5; }
	inline void set_startingDistance_5(float value)
	{
		___startingDistance_5 = value;
	}

	inline static int32_t get_offset_of_startingScale_6() { return static_cast<int32_t>(offsetof(FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C, ___startingScale_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_startingScale_6() const { return ___startingScale_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_startingScale_6() { return &___startingScale_6; }
	inline void set_startingScale_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___startingScale_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDANGULARSIZE_TB822C9FDA10A7411779C35C76B76612D527F459C_H
#ifndef INTERPOLATOR_TC80908B5E9A91C57719F0F850FB297B39FDA9D0E_H
#define INTERPOLATOR_TC80908B5E9A91C57719F0F850FB297B39FDA9D0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.Interpolator
struct  Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean MPAR.Common.Utility.Interpolator::UseUnscaledTime
	bool ___UseUnscaledTime_4;
	// System.Single MPAR.Common.Utility.Interpolator::PositionPerSecond
	float ___PositionPerSecond_6;
	// System.Single MPAR.Common.Utility.Interpolator::RotationDegreesPerSecond
	float ___RotationDegreesPerSecond_7;
	// System.Single MPAR.Common.Utility.Interpolator::RotationSpeedScaler
	float ___RotationSpeedScaler_8;
	// System.Single MPAR.Common.Utility.Interpolator::ScalePerSecond
	float ___ScalePerSecond_9;
	// System.Boolean MPAR.Common.Utility.Interpolator::SmoothLerpToTarget
	bool ___SmoothLerpToTarget_10;
	// System.Single MPAR.Common.Utility.Interpolator::SmoothPositionLerpRatio
	float ___SmoothPositionLerpRatio_11;
	// System.Single MPAR.Common.Utility.Interpolator::SmoothRotationLerpRatio
	float ___SmoothRotationLerpRatio_12;
	// System.Single MPAR.Common.Utility.Interpolator::SmoothScaleLerpRatio
	float ___SmoothScaleLerpRatio_13;
	// UnityEngine.Vector3 MPAR.Common.Utility.Interpolator::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_14;
	// System.Boolean MPAR.Common.Utility.Interpolator::<AnimatingPosition>k__BackingField
	bool ___U3CAnimatingPositionU3Ek__BackingField_15;
	// UnityEngine.Quaternion MPAR.Common.Utility.Interpolator::targetRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetRotation_16;
	// System.Boolean MPAR.Common.Utility.Interpolator::<AnimatingRotation>k__BackingField
	bool ___U3CAnimatingRotationU3Ek__BackingField_17;
	// UnityEngine.Quaternion MPAR.Common.Utility.Interpolator::targetLocalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___targetLocalRotation_18;
	// System.Boolean MPAR.Common.Utility.Interpolator::<AnimatingLocalRotation>k__BackingField
	bool ___U3CAnimatingLocalRotationU3Ek__BackingField_19;
	// UnityEngine.Vector3 MPAR.Common.Utility.Interpolator::targetLocalScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetLocalScale_20;
	// System.Boolean MPAR.Common.Utility.Interpolator::<AnimatingLocalScale>k__BackingField
	bool ___U3CAnimatingLocalScaleU3Ek__BackingField_21;
	// System.Action MPAR.Common.Utility.Interpolator::InterpolationStarted
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___InterpolationStarted_22;
	// System.Action MPAR.Common.Utility.Interpolator::InterpolationDone
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___InterpolationDone_23;
	// UnityEngine.Vector3 MPAR.Common.Utility.Interpolator::<PositionVelocity>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CPositionVelocityU3Ek__BackingField_24;
	// UnityEngine.Vector3 MPAR.Common.Utility.Interpolator::oldPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oldPosition_25;

public:
	inline static int32_t get_offset_of_UseUnscaledTime_4() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___UseUnscaledTime_4)); }
	inline bool get_UseUnscaledTime_4() const { return ___UseUnscaledTime_4; }
	inline bool* get_address_of_UseUnscaledTime_4() { return &___UseUnscaledTime_4; }
	inline void set_UseUnscaledTime_4(bool value)
	{
		___UseUnscaledTime_4 = value;
	}

	inline static int32_t get_offset_of_PositionPerSecond_6() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___PositionPerSecond_6)); }
	inline float get_PositionPerSecond_6() const { return ___PositionPerSecond_6; }
	inline float* get_address_of_PositionPerSecond_6() { return &___PositionPerSecond_6; }
	inline void set_PositionPerSecond_6(float value)
	{
		___PositionPerSecond_6 = value;
	}

	inline static int32_t get_offset_of_RotationDegreesPerSecond_7() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___RotationDegreesPerSecond_7)); }
	inline float get_RotationDegreesPerSecond_7() const { return ___RotationDegreesPerSecond_7; }
	inline float* get_address_of_RotationDegreesPerSecond_7() { return &___RotationDegreesPerSecond_7; }
	inline void set_RotationDegreesPerSecond_7(float value)
	{
		___RotationDegreesPerSecond_7 = value;
	}

	inline static int32_t get_offset_of_RotationSpeedScaler_8() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___RotationSpeedScaler_8)); }
	inline float get_RotationSpeedScaler_8() const { return ___RotationSpeedScaler_8; }
	inline float* get_address_of_RotationSpeedScaler_8() { return &___RotationSpeedScaler_8; }
	inline void set_RotationSpeedScaler_8(float value)
	{
		___RotationSpeedScaler_8 = value;
	}

	inline static int32_t get_offset_of_ScalePerSecond_9() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___ScalePerSecond_9)); }
	inline float get_ScalePerSecond_9() const { return ___ScalePerSecond_9; }
	inline float* get_address_of_ScalePerSecond_9() { return &___ScalePerSecond_9; }
	inline void set_ScalePerSecond_9(float value)
	{
		___ScalePerSecond_9 = value;
	}

	inline static int32_t get_offset_of_SmoothLerpToTarget_10() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___SmoothLerpToTarget_10)); }
	inline bool get_SmoothLerpToTarget_10() const { return ___SmoothLerpToTarget_10; }
	inline bool* get_address_of_SmoothLerpToTarget_10() { return &___SmoothLerpToTarget_10; }
	inline void set_SmoothLerpToTarget_10(bool value)
	{
		___SmoothLerpToTarget_10 = value;
	}

	inline static int32_t get_offset_of_SmoothPositionLerpRatio_11() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___SmoothPositionLerpRatio_11)); }
	inline float get_SmoothPositionLerpRatio_11() const { return ___SmoothPositionLerpRatio_11; }
	inline float* get_address_of_SmoothPositionLerpRatio_11() { return &___SmoothPositionLerpRatio_11; }
	inline void set_SmoothPositionLerpRatio_11(float value)
	{
		___SmoothPositionLerpRatio_11 = value;
	}

	inline static int32_t get_offset_of_SmoothRotationLerpRatio_12() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___SmoothRotationLerpRatio_12)); }
	inline float get_SmoothRotationLerpRatio_12() const { return ___SmoothRotationLerpRatio_12; }
	inline float* get_address_of_SmoothRotationLerpRatio_12() { return &___SmoothRotationLerpRatio_12; }
	inline void set_SmoothRotationLerpRatio_12(float value)
	{
		___SmoothRotationLerpRatio_12 = value;
	}

	inline static int32_t get_offset_of_SmoothScaleLerpRatio_13() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___SmoothScaleLerpRatio_13)); }
	inline float get_SmoothScaleLerpRatio_13() const { return ___SmoothScaleLerpRatio_13; }
	inline float* get_address_of_SmoothScaleLerpRatio_13() { return &___SmoothScaleLerpRatio_13; }
	inline void set_SmoothScaleLerpRatio_13(float value)
	{
		___SmoothScaleLerpRatio_13 = value;
	}

	inline static int32_t get_offset_of_targetPosition_14() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___targetPosition_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_14() const { return ___targetPosition_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_14() { return &___targetPosition_14; }
	inline void set_targetPosition_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_14 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingPositionU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___U3CAnimatingPositionU3Ek__BackingField_15)); }
	inline bool get_U3CAnimatingPositionU3Ek__BackingField_15() const { return ___U3CAnimatingPositionU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CAnimatingPositionU3Ek__BackingField_15() { return &___U3CAnimatingPositionU3Ek__BackingField_15; }
	inline void set_U3CAnimatingPositionU3Ek__BackingField_15(bool value)
	{
		___U3CAnimatingPositionU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_targetRotation_16() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___targetRotation_16)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetRotation_16() const { return ___targetRotation_16; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetRotation_16() { return &___targetRotation_16; }
	inline void set_targetRotation_16(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetRotation_16 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingRotationU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___U3CAnimatingRotationU3Ek__BackingField_17)); }
	inline bool get_U3CAnimatingRotationU3Ek__BackingField_17() const { return ___U3CAnimatingRotationU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CAnimatingRotationU3Ek__BackingField_17() { return &___U3CAnimatingRotationU3Ek__BackingField_17; }
	inline void set_U3CAnimatingRotationU3Ek__BackingField_17(bool value)
	{
		___U3CAnimatingRotationU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_targetLocalRotation_18() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___targetLocalRotation_18)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_targetLocalRotation_18() const { return ___targetLocalRotation_18; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_targetLocalRotation_18() { return &___targetLocalRotation_18; }
	inline void set_targetLocalRotation_18(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___targetLocalRotation_18 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___U3CAnimatingLocalRotationU3Ek__BackingField_19)); }
	inline bool get_U3CAnimatingLocalRotationU3Ek__BackingField_19() const { return ___U3CAnimatingLocalRotationU3Ek__BackingField_19; }
	inline bool* get_address_of_U3CAnimatingLocalRotationU3Ek__BackingField_19() { return &___U3CAnimatingLocalRotationU3Ek__BackingField_19; }
	inline void set_U3CAnimatingLocalRotationU3Ek__BackingField_19(bool value)
	{
		___U3CAnimatingLocalRotationU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_targetLocalScale_20() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___targetLocalScale_20)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetLocalScale_20() const { return ___targetLocalScale_20; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetLocalScale_20() { return &___targetLocalScale_20; }
	inline void set_targetLocalScale_20(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetLocalScale_20 = value;
	}

	inline static int32_t get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___U3CAnimatingLocalScaleU3Ek__BackingField_21)); }
	inline bool get_U3CAnimatingLocalScaleU3Ek__BackingField_21() const { return ___U3CAnimatingLocalScaleU3Ek__BackingField_21; }
	inline bool* get_address_of_U3CAnimatingLocalScaleU3Ek__BackingField_21() { return &___U3CAnimatingLocalScaleU3Ek__BackingField_21; }
	inline void set_U3CAnimatingLocalScaleU3Ek__BackingField_21(bool value)
	{
		___U3CAnimatingLocalScaleU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_InterpolationStarted_22() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___InterpolationStarted_22)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_InterpolationStarted_22() const { return ___InterpolationStarted_22; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_InterpolationStarted_22() { return &___InterpolationStarted_22; }
	inline void set_InterpolationStarted_22(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___InterpolationStarted_22 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolationStarted_22), value);
	}

	inline static int32_t get_offset_of_InterpolationDone_23() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___InterpolationDone_23)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_InterpolationDone_23() const { return ___InterpolationDone_23; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_InterpolationDone_23() { return &___InterpolationDone_23; }
	inline void set_InterpolationDone_23(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___InterpolationDone_23 = value;
		Il2CppCodeGenWriteBarrier((&___InterpolationDone_23), value);
	}

	inline static int32_t get_offset_of_U3CPositionVelocityU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___U3CPositionVelocityU3Ek__BackingField_24)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CPositionVelocityU3Ek__BackingField_24() const { return ___U3CPositionVelocityU3Ek__BackingField_24; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CPositionVelocityU3Ek__BackingField_24() { return &___U3CPositionVelocityU3Ek__BackingField_24; }
	inline void set_U3CPositionVelocityU3Ek__BackingField_24(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CPositionVelocityU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_oldPosition_25() { return static_cast<int32_t>(offsetof(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E, ___oldPosition_25)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oldPosition_25() const { return ___oldPosition_25; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oldPosition_25() { return &___oldPosition_25; }
	inline void set_oldPosition_25(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oldPosition_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTERPOLATOR_TC80908B5E9A91C57719F0F850FB297B39FDA9D0E_H
#ifndef SIMPLETAGALONG_TA1603A02F0AF5AD8047A335785D7D0C29F581495_H
#define SIMPLETAGALONG_TA1603A02F0AF5AD8047A335785D7D0C29F581495_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.SimpleTagalong
struct  SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MPAR.Common.Utility.SimpleTagalong::TagalongDistance
	float ___TagalongDistance_4;
	// System.Boolean MPAR.Common.Utility.SimpleTagalong::EnforceDistance
	bool ___EnforceDistance_5;
	// System.Single MPAR.Common.Utility.SimpleTagalong::PositionUpdateSpeed
	float ___PositionUpdateSpeed_6;
	// System.Boolean MPAR.Common.Utility.SimpleTagalong::SmoothMotion
	bool ___SmoothMotion_7;
	// System.Single MPAR.Common.Utility.SimpleTagalong::SmoothingFactor
	float ___SmoothingFactor_8;
	// UnityEngine.BoxCollider MPAR.Common.Utility.SimpleTagalong::tagalongCollider
	BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * ___tagalongCollider_9;
	// MPAR.Common.Utility.Interpolator MPAR.Common.Utility.SimpleTagalong::interpolator
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E * ___interpolator_10;
	// UnityEngine.Plane[] MPAR.Common.Utility.SimpleTagalong::frustumPlanes
	PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* ___frustumPlanes_11;

public:
	inline static int32_t get_offset_of_TagalongDistance_4() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___TagalongDistance_4)); }
	inline float get_TagalongDistance_4() const { return ___TagalongDistance_4; }
	inline float* get_address_of_TagalongDistance_4() { return &___TagalongDistance_4; }
	inline void set_TagalongDistance_4(float value)
	{
		___TagalongDistance_4 = value;
	}

	inline static int32_t get_offset_of_EnforceDistance_5() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___EnforceDistance_5)); }
	inline bool get_EnforceDistance_5() const { return ___EnforceDistance_5; }
	inline bool* get_address_of_EnforceDistance_5() { return &___EnforceDistance_5; }
	inline void set_EnforceDistance_5(bool value)
	{
		___EnforceDistance_5 = value;
	}

	inline static int32_t get_offset_of_PositionUpdateSpeed_6() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___PositionUpdateSpeed_6)); }
	inline float get_PositionUpdateSpeed_6() const { return ___PositionUpdateSpeed_6; }
	inline float* get_address_of_PositionUpdateSpeed_6() { return &___PositionUpdateSpeed_6; }
	inline void set_PositionUpdateSpeed_6(float value)
	{
		___PositionUpdateSpeed_6 = value;
	}

	inline static int32_t get_offset_of_SmoothMotion_7() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___SmoothMotion_7)); }
	inline bool get_SmoothMotion_7() const { return ___SmoothMotion_7; }
	inline bool* get_address_of_SmoothMotion_7() { return &___SmoothMotion_7; }
	inline void set_SmoothMotion_7(bool value)
	{
		___SmoothMotion_7 = value;
	}

	inline static int32_t get_offset_of_SmoothingFactor_8() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___SmoothingFactor_8)); }
	inline float get_SmoothingFactor_8() const { return ___SmoothingFactor_8; }
	inline float* get_address_of_SmoothingFactor_8() { return &___SmoothingFactor_8; }
	inline void set_SmoothingFactor_8(float value)
	{
		___SmoothingFactor_8 = value;
	}

	inline static int32_t get_offset_of_tagalongCollider_9() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___tagalongCollider_9)); }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * get_tagalongCollider_9() const { return ___tagalongCollider_9; }
	inline BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA ** get_address_of_tagalongCollider_9() { return &___tagalongCollider_9; }
	inline void set_tagalongCollider_9(BoxCollider_t2DF257BBBFCABE0B9D78B21D238298D1942BFBAA * value)
	{
		___tagalongCollider_9 = value;
		Il2CppCodeGenWriteBarrier((&___tagalongCollider_9), value);
	}

	inline static int32_t get_offset_of_interpolator_10() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___interpolator_10)); }
	inline Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E * get_interpolator_10() const { return ___interpolator_10; }
	inline Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E ** get_address_of_interpolator_10() { return &___interpolator_10; }
	inline void set_interpolator_10(Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E * value)
	{
		___interpolator_10 = value;
		Il2CppCodeGenWriteBarrier((&___interpolator_10), value);
	}

	inline static int32_t get_offset_of_frustumPlanes_11() { return static_cast<int32_t>(offsetof(SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495, ___frustumPlanes_11)); }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* get_frustumPlanes_11() const { return ___frustumPlanes_11; }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA** get_address_of_frustumPlanes_11() { return &___frustumPlanes_11; }
	inline void set_frustumPlanes_11(PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* value)
	{
		___frustumPlanes_11 = value;
		Il2CppCodeGenWriteBarrier((&___frustumPlanes_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLETAGALONG_TA1603A02F0AF5AD8047A335785D7D0C29F581495_H
#ifndef BASESAVELOADMANAGER_TB73A6EC5834C76C54E79BF0F10241ADB883530EB_H
#define BASESAVELOADMANAGER_TB73A6EC5834C76C54E79BF0F10241ADB883530EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager
struct  BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::BasePath
	String_t* ___BasePath_4;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::ScriptsSuffix
	String_t* ___ScriptsSuffix_5;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::CacheSuffix
	String_t* ___CacheSuffix_6;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::SaveDataSiffux
	String_t* ___SaveDataSiffux_7;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::<ScriptId>k__BackingField
	String_t* ___U3CScriptIdU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MPAR.Desktop.Persistence.BaseSaveLoadManager::<AssetMapping>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CAssetMappingU3Ek__BackingField_9;
	// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader MPAR.Desktop.Persistence.BaseSaveLoadManager::<AssetLoader>k__BackingField
	RuntimeObject* ___U3CAssetLoaderU3Ek__BackingField_10;
	// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory MPAR.Desktop.Persistence.BaseSaveLoadManager::<ExhibitBoxFactory>k__BackingField
	ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * ___U3CExhibitBoxFactoryU3Ek__BackingField_11;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Desktop.Persistence.BaseSaveLoadManager::Gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___Gestures_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject>> MPAR.Desktop.Persistence.BaseSaveLoadManager::<Primitives>k__BackingField
	Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 * ___U3CPrimitivesU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_BasePath_4() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___BasePath_4)); }
	inline String_t* get_BasePath_4() const { return ___BasePath_4; }
	inline String_t** get_address_of_BasePath_4() { return &___BasePath_4; }
	inline void set_BasePath_4(String_t* value)
	{
		___BasePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___BasePath_4), value);
	}

	inline static int32_t get_offset_of_ScriptsSuffix_5() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___ScriptsSuffix_5)); }
	inline String_t* get_ScriptsSuffix_5() const { return ___ScriptsSuffix_5; }
	inline String_t** get_address_of_ScriptsSuffix_5() { return &___ScriptsSuffix_5; }
	inline void set_ScriptsSuffix_5(String_t* value)
	{
		___ScriptsSuffix_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScriptsSuffix_5), value);
	}

	inline static int32_t get_offset_of_CacheSuffix_6() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___CacheSuffix_6)); }
	inline String_t* get_CacheSuffix_6() const { return ___CacheSuffix_6; }
	inline String_t** get_address_of_CacheSuffix_6() { return &___CacheSuffix_6; }
	inline void set_CacheSuffix_6(String_t* value)
	{
		___CacheSuffix_6 = value;
		Il2CppCodeGenWriteBarrier((&___CacheSuffix_6), value);
	}

	inline static int32_t get_offset_of_SaveDataSiffux_7() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___SaveDataSiffux_7)); }
	inline String_t* get_SaveDataSiffux_7() const { return ___SaveDataSiffux_7; }
	inline String_t** get_address_of_SaveDataSiffux_7() { return &___SaveDataSiffux_7; }
	inline void set_SaveDataSiffux_7(String_t* value)
	{
		___SaveDataSiffux_7 = value;
		Il2CppCodeGenWriteBarrier((&___SaveDataSiffux_7), value);
	}

	inline static int32_t get_offset_of_U3CScriptIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CScriptIdU3Ek__BackingField_8)); }
	inline String_t* get_U3CScriptIdU3Ek__BackingField_8() const { return ___U3CScriptIdU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CScriptIdU3Ek__BackingField_8() { return &___U3CScriptIdU3Ek__BackingField_8; }
	inline void set_U3CScriptIdU3Ek__BackingField_8(String_t* value)
	{
		___U3CScriptIdU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptIdU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CAssetMappingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CAssetMappingU3Ek__BackingField_9)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CAssetMappingU3Ek__BackingField_9() const { return ___U3CAssetMappingU3Ek__BackingField_9; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CAssetMappingU3Ek__BackingField_9() { return &___U3CAssetMappingU3Ek__BackingField_9; }
	inline void set_U3CAssetMappingU3Ek__BackingField_9(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CAssetMappingU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetMappingU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CAssetLoaderU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CAssetLoaderU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CAssetLoaderU3Ek__BackingField_10() const { return ___U3CAssetLoaderU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CAssetLoaderU3Ek__BackingField_10() { return &___U3CAssetLoaderU3Ek__BackingField_10; }
	inline void set_U3CAssetLoaderU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CAssetLoaderU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetLoaderU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CExhibitBoxFactoryU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CExhibitBoxFactoryU3Ek__BackingField_11)); }
	inline ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * get_U3CExhibitBoxFactoryU3Ek__BackingField_11() const { return ___U3CExhibitBoxFactoryU3Ek__BackingField_11; }
	inline ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 ** get_address_of_U3CExhibitBoxFactoryU3Ek__BackingField_11() { return &___U3CExhibitBoxFactoryU3Ek__BackingField_11; }
	inline void set_U3CExhibitBoxFactoryU3Ek__BackingField_11(ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * value)
	{
		___U3CExhibitBoxFactoryU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExhibitBoxFactoryU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_Gestures_12() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___Gestures_12)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_Gestures_12() const { return ___Gestures_12; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_Gestures_12() { return &___Gestures_12; }
	inline void set_Gestures_12(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___Gestures_12 = value;
		Il2CppCodeGenWriteBarrier((&___Gestures_12), value);
	}

	inline static int32_t get_offset_of_U3CPrimitivesU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CPrimitivesU3Ek__BackingField_13)); }
	inline Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 * get_U3CPrimitivesU3Ek__BackingField_13() const { return ___U3CPrimitivesU3Ek__BackingField_13; }
	inline Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 ** get_address_of_U3CPrimitivesU3Ek__BackingField_13() { return &___U3CPrimitivesU3Ek__BackingField_13; }
	inline void set_U3CPrimitivesU3Ek__BackingField_13(Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 * value)
	{
		___U3CPrimitivesU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitivesU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESAVELOADMANAGER_TB73A6EC5834C76C54E79BF0F10241ADB883530EB_H
#ifndef DESKTOPANCHORMANAGER_TABE3C6779963A86EE306BC56B5422CA1B40FC40F_H
#define DESKTOPANCHORMANAGER_TABE3C6779963A86EE306BC56B5422CA1B40FC40F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.DesktopAnchorManager
struct  DesktopAnchorManager_tABE3C6779963A86EE306BC56B5422CA1B40FC40F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPANCHORMANAGER_TABE3C6779963A86EE306BC56B5422CA1B40FC40F_H
#ifndef MATERIALUTILITY_T3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_H
#define MATERIALUTILITY_T3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.GameObjects.Utility.MaterialUtility
struct  MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Material MPAR.GameObjects.Utility.MaterialUtility::BaseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___BaseMaterial_4;
	// UnityEngine.Material MPAR.GameObjects.Utility.MaterialUtility::BaseModelMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___BaseModelMaterial_5;

public:
	inline static int32_t get_offset_of_BaseMaterial_4() { return static_cast<int32_t>(offsetof(MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1, ___BaseMaterial_4)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_BaseMaterial_4() const { return ___BaseMaterial_4; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_BaseMaterial_4() { return &___BaseMaterial_4; }
	inline void set_BaseMaterial_4(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___BaseMaterial_4 = value;
		Il2CppCodeGenWriteBarrier((&___BaseMaterial_4), value);
	}

	inline static int32_t get_offset_of_BaseModelMaterial_5() { return static_cast<int32_t>(offsetof(MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1, ___BaseModelMaterial_5)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_BaseModelMaterial_5() const { return ___BaseModelMaterial_5; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_BaseModelMaterial_5() { return &___BaseModelMaterial_5; }
	inline void set_BaseModelMaterial_5(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___BaseModelMaterial_5 = value;
		Il2CppCodeGenWriteBarrier((&___BaseModelMaterial_5), value);
	}
};

struct MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_StaticFields
{
public:
	// UnityEngine.Material MPAR.GameObjects.Utility.MaterialUtility::StaticBaseMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StaticBaseMaterial_6;
	// UnityEngine.Material MPAR.GameObjects.Utility.MaterialUtility::StaticBaseModelMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___StaticBaseModelMaterial_7;

public:
	inline static int32_t get_offset_of_StaticBaseMaterial_6() { return static_cast<int32_t>(offsetof(MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_StaticFields, ___StaticBaseMaterial_6)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StaticBaseMaterial_6() const { return ___StaticBaseMaterial_6; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StaticBaseMaterial_6() { return &___StaticBaseMaterial_6; }
	inline void set_StaticBaseMaterial_6(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StaticBaseMaterial_6 = value;
		Il2CppCodeGenWriteBarrier((&___StaticBaseMaterial_6), value);
	}

	inline static int32_t get_offset_of_StaticBaseModelMaterial_7() { return static_cast<int32_t>(offsetof(MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_StaticFields, ___StaticBaseModelMaterial_7)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_StaticBaseModelMaterial_7() const { return ___StaticBaseModelMaterial_7; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_StaticBaseModelMaterial_7() { return &___StaticBaseModelMaterial_7; }
	inline void set_StaticBaseModelMaterial_7(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___StaticBaseModelMaterial_7 = value;
		Il2CppCodeGenWriteBarrier((&___StaticBaseModelMaterial_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MATERIALUTILITY_T3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_H
#ifndef FIXEDSIZEONSCREEN_T594F7D0BB1AAAAD1FCE8BCA2DD19DF70BF099792_H
#define FIXEDSIZEONSCREEN_T594F7D0BB1AAAAD1FCE8BCA2DD19DF70BF099792_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.FixedSizeOnScreen
struct  FixedSizeOnScreen_t594F7D0BB1AAAAD1FCE8BCA2DD19DF70BF099792  : public FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDSIZEONSCREEN_T594F7D0BB1AAAAD1FCE8BCA2DD19DF70BF099792_H
#ifndef ROTATEHANDLE_TEDA7A218D238A0E62FC8505434D0883763C450FC_H
#define ROTATEHANDLE_TEDA7A218D238A0E62FC8505434D0883763C450FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.RotateHandle
struct  RotateHandle_tEDA7A218D238A0E62FC8505434D0883763C450FC  : public EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482
{
public:
	// UnityEngine.Vector3 MPAR.Common.GameObjects.RotateHandle::axis
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___axis_17;

public:
	inline static int32_t get_offset_of_axis_17() { return static_cast<int32_t>(offsetof(RotateHandle_tEDA7A218D238A0E62FC8505434D0883763C450FC, ___axis_17)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_axis_17() const { return ___axis_17; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_axis_17() { return &___axis_17; }
	inline void set_axis_17(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___axis_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATEHANDLE_TEDA7A218D238A0E62FC8505434D0883763C450FC_H
#ifndef SCALEHANDLE_T10290945CE09A85D295DDD8F21573B991DEE686F_H
#define SCALEHANDLE_T10290945CE09A85D295DDD8F21573B991DEE686F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.ScaleHandle
struct  ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F  : public EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482
{
public:
	// UnityEngine.Transform MPAR.Common.GameObjects.ScaleHandle::objects
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___objects_17;
	// UnityEngine.Transform MPAR.Common.GameObjects.ScaleHandle::origin
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___origin_18;
	// System.Single MPAR.Common.GameObjects.ScaleHandle::maxScalePerSecond
	float ___maxScalePerSecond_19;

public:
	inline static int32_t get_offset_of_objects_17() { return static_cast<int32_t>(offsetof(ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F, ___objects_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_objects_17() const { return ___objects_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_objects_17() { return &___objects_17; }
	inline void set_objects_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___objects_17 = value;
		Il2CppCodeGenWriteBarrier((&___objects_17), value);
	}

	inline static int32_t get_offset_of_origin_18() { return static_cast<int32_t>(offsetof(ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F, ___origin_18)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_origin_18() const { return ___origin_18; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_origin_18() { return &___origin_18; }
	inline void set_origin_18(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___origin_18 = value;
		Il2CppCodeGenWriteBarrier((&___origin_18), value);
	}

	inline static int32_t get_offset_of_maxScalePerSecond_19() { return static_cast<int32_t>(offsetof(ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F, ___maxScalePerSecond_19)); }
	inline float get_maxScalePerSecond_19() const { return ___maxScalePerSecond_19; }
	inline float* get_address_of_maxScalePerSecond_19() { return &___maxScalePerSecond_19; }
	inline void set_maxScalePerSecond_19(float value)
	{
		___maxScalePerSecond_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCALEHANDLE_T10290945CE09A85D295DDD8F21573B991DEE686F_H
#ifndef TRANSLATEHANDLE_TBE476299B49CB94E8B12FEC82A4E64321E7FD2AB_H
#define TRANSLATEHANDLE_TBE476299B49CB94E8B12FEC82A4E64321E7FD2AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.TranslateHandle
struct  TranslateHandle_tBE476299B49CB94E8B12FEC82A4E64321E7FD2AB  : public EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSLATEHANDLE_TBE476299B49CB94E8B12FEC82A4E64321E7FD2AB_H
#ifndef PREFABUTILITY_T0933A572FCD91DB00F96CCD1DE333BC8728B00B2_H
#define PREFABUTILITY_T0933A572FCD91DB00F96CCD1DE333BC8728B00B2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Utility.PrefabUtility
struct  PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2  : public Singleton_1_t0ECB789469C68CCD30B77CE85E9AEA1234499C45
{
public:
	// MPAR.Common.GameObjects.Factories.VideoControlsFactory MPAR.Common.GameObjects.Utility.PrefabUtility::VideoControlsFactory
	VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6 * ___VideoControlsFactory_6;

public:
	inline static int32_t get_offset_of_VideoControlsFactory_6() { return static_cast<int32_t>(offsetof(PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2, ___VideoControlsFactory_6)); }
	inline VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6 * get_VideoControlsFactory_6() const { return ___VideoControlsFactory_6; }
	inline VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6 ** get_address_of_VideoControlsFactory_6() { return &___VideoControlsFactory_6; }
	inline void set_VideoControlsFactory_6(VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6 * value)
	{
		___VideoControlsFactory_6 = value;
		Il2CppCodeGenWriteBarrier((&___VideoControlsFactory_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABUTILITY_T0933A572FCD91DB00F96CCD1DE333BC8728B00B2_H
#ifndef TAGALONG_T10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51_H
#define TAGALONG_T10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.Utility.Tagalong
struct  Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51  : public SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495
{
public:
	// System.Single MPAR.Common.Utility.Tagalong::MinimumHorizontalOverlap
	float ___MinimumHorizontalOverlap_16;
	// System.Single MPAR.Common.Utility.Tagalong::TargetHorizontalOverlap
	float ___TargetHorizontalOverlap_17;
	// System.Single MPAR.Common.Utility.Tagalong::MinimumVerticalOverlap
	float ___MinimumVerticalOverlap_18;
	// System.Single MPAR.Common.Utility.Tagalong::TargetVerticalOverlap
	float ___TargetVerticalOverlap_19;
	// System.Int32 MPAR.Common.Utility.Tagalong::HorizontalRayCount
	int32_t ___HorizontalRayCount_20;
	// System.Int32 MPAR.Common.Utility.Tagalong::VerticalRayCount
	int32_t ___VerticalRayCount_21;
	// System.Single MPAR.Common.Utility.Tagalong::MinimumTagalongDistance
	float ___MinimumTagalongDistance_22;
	// System.Boolean MPAR.Common.Utility.Tagalong::MaintainFixedSize
	bool ___MaintainFixedSize_23;
	// System.Single MPAR.Common.Utility.Tagalong::DepthUpdateSpeed
	float ___DepthUpdateSpeed_24;
	// System.Single MPAR.Common.Utility.Tagalong::defaultTagalongDistance
	float ___defaultTagalongDistance_25;
	// System.Boolean MPAR.Common.Utility.Tagalong::DebugDrawLines
	bool ___DebugDrawLines_26;
	// UnityEngine.Light MPAR.Common.Utility.Tagalong::DebugPointLight
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___DebugPointLight_27;

public:
	inline static int32_t get_offset_of_MinimumHorizontalOverlap_16() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___MinimumHorizontalOverlap_16)); }
	inline float get_MinimumHorizontalOverlap_16() const { return ___MinimumHorizontalOverlap_16; }
	inline float* get_address_of_MinimumHorizontalOverlap_16() { return &___MinimumHorizontalOverlap_16; }
	inline void set_MinimumHorizontalOverlap_16(float value)
	{
		___MinimumHorizontalOverlap_16 = value;
	}

	inline static int32_t get_offset_of_TargetHorizontalOverlap_17() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___TargetHorizontalOverlap_17)); }
	inline float get_TargetHorizontalOverlap_17() const { return ___TargetHorizontalOverlap_17; }
	inline float* get_address_of_TargetHorizontalOverlap_17() { return &___TargetHorizontalOverlap_17; }
	inline void set_TargetHorizontalOverlap_17(float value)
	{
		___TargetHorizontalOverlap_17 = value;
	}

	inline static int32_t get_offset_of_MinimumVerticalOverlap_18() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___MinimumVerticalOverlap_18)); }
	inline float get_MinimumVerticalOverlap_18() const { return ___MinimumVerticalOverlap_18; }
	inline float* get_address_of_MinimumVerticalOverlap_18() { return &___MinimumVerticalOverlap_18; }
	inline void set_MinimumVerticalOverlap_18(float value)
	{
		___MinimumVerticalOverlap_18 = value;
	}

	inline static int32_t get_offset_of_TargetVerticalOverlap_19() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___TargetVerticalOverlap_19)); }
	inline float get_TargetVerticalOverlap_19() const { return ___TargetVerticalOverlap_19; }
	inline float* get_address_of_TargetVerticalOverlap_19() { return &___TargetVerticalOverlap_19; }
	inline void set_TargetVerticalOverlap_19(float value)
	{
		___TargetVerticalOverlap_19 = value;
	}

	inline static int32_t get_offset_of_HorizontalRayCount_20() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___HorizontalRayCount_20)); }
	inline int32_t get_HorizontalRayCount_20() const { return ___HorizontalRayCount_20; }
	inline int32_t* get_address_of_HorizontalRayCount_20() { return &___HorizontalRayCount_20; }
	inline void set_HorizontalRayCount_20(int32_t value)
	{
		___HorizontalRayCount_20 = value;
	}

	inline static int32_t get_offset_of_VerticalRayCount_21() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___VerticalRayCount_21)); }
	inline int32_t get_VerticalRayCount_21() const { return ___VerticalRayCount_21; }
	inline int32_t* get_address_of_VerticalRayCount_21() { return &___VerticalRayCount_21; }
	inline void set_VerticalRayCount_21(int32_t value)
	{
		___VerticalRayCount_21 = value;
	}

	inline static int32_t get_offset_of_MinimumTagalongDistance_22() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___MinimumTagalongDistance_22)); }
	inline float get_MinimumTagalongDistance_22() const { return ___MinimumTagalongDistance_22; }
	inline float* get_address_of_MinimumTagalongDistance_22() { return &___MinimumTagalongDistance_22; }
	inline void set_MinimumTagalongDistance_22(float value)
	{
		___MinimumTagalongDistance_22 = value;
	}

	inline static int32_t get_offset_of_MaintainFixedSize_23() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___MaintainFixedSize_23)); }
	inline bool get_MaintainFixedSize_23() const { return ___MaintainFixedSize_23; }
	inline bool* get_address_of_MaintainFixedSize_23() { return &___MaintainFixedSize_23; }
	inline void set_MaintainFixedSize_23(bool value)
	{
		___MaintainFixedSize_23 = value;
	}

	inline static int32_t get_offset_of_DepthUpdateSpeed_24() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___DepthUpdateSpeed_24)); }
	inline float get_DepthUpdateSpeed_24() const { return ___DepthUpdateSpeed_24; }
	inline float* get_address_of_DepthUpdateSpeed_24() { return &___DepthUpdateSpeed_24; }
	inline void set_DepthUpdateSpeed_24(float value)
	{
		___DepthUpdateSpeed_24 = value;
	}

	inline static int32_t get_offset_of_defaultTagalongDistance_25() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___defaultTagalongDistance_25)); }
	inline float get_defaultTagalongDistance_25() const { return ___defaultTagalongDistance_25; }
	inline float* get_address_of_defaultTagalongDistance_25() { return &___defaultTagalongDistance_25; }
	inline void set_defaultTagalongDistance_25(float value)
	{
		___defaultTagalongDistance_25 = value;
	}

	inline static int32_t get_offset_of_DebugDrawLines_26() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___DebugDrawLines_26)); }
	inline bool get_DebugDrawLines_26() const { return ___DebugDrawLines_26; }
	inline bool* get_address_of_DebugDrawLines_26() { return &___DebugDrawLines_26; }
	inline void set_DebugDrawLines_26(bool value)
	{
		___DebugDrawLines_26 = value;
	}

	inline static int32_t get_offset_of_DebugPointLight_27() { return static_cast<int32_t>(offsetof(Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51, ___DebugPointLight_27)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_DebugPointLight_27() const { return ___DebugPointLight_27; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_DebugPointLight_27() { return &___DebugPointLight_27; }
	inline void set_DebugPointLight_27(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___DebugPointLight_27 = value;
		Il2CppCodeGenWriteBarrier((&___DebugPointLight_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAGALONG_T10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51_H
#ifndef DESKTOPSAVELOADMANAGER_TC09854EC62A852877888FC05982D139E2E123360_H
#define DESKTOPSAVELOADMANAGER_TC09854EC62A852877888FC05982D139E2E123360_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.DesktopSaveLoadManager
struct  DesktopSaveLoadManager_tC09854EC62A852877888FC05982D139E2E123360  : public BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPSAVELOADMANAGER_TC09854EC62A852877888FC05982D139E2E123360_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8000 = { sizeof (U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8000[2] = 
{
	U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71::get_offset_of_hologram_0(),
	U3CU3Ec__DisplayClass31_1_tC7325EFDD01AF27BE202340BD2D21EA2DFFFFF71::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8001 = { sizeof (U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8001[6] = 
{
	U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358::get_offset_of_box_0(),
	U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358::get_offset_of_pos_1(),
	U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358::get_offset_of_euler_2(),
	U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358::get_offset_of_scale_3(),
	U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358::get_offset_of_CSU24U3CU3E8__locals2_4(),
	U3CU3Ec__DisplayClass31_2_tEE9391ABC356A1E9E4AA6AAA52CBC36DC1DD9358::get_offset_of_U3CU3E9__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8002 = { sizeof (U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8002[4] = 
{
	U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21::get_offset_of_U3CU3E1__state_0(),
	U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21::get_offset_of_U3CU3E2__current_1(),
	U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21::get_offset_of_scripts_2(),
	U3CLoadCachedScriptThumbnailsU3Ed__37_tB3DAE1A4A24723763BE8CC306F8F28ED38AF2E21::get_offset_of_U3CiU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8003 = { sizeof (U3CU3Ec__DisplayClass38_0_t4205219D8CAEF7B4716D57488B2A633CD7656F22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8003[1] = 
{
	U3CU3Ec__DisplayClass38_0_t4205219D8CAEF7B4716D57488B2A633CD7656F22::get_offset_of_loadThumbnails_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8004 = { sizeof (DesktopAnchorManager_tABE3C6779963A86EE306BC56B5422CA1B40FC40F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8005 = { sizeof (DesktopSaveLoadManager_tC09854EC62A852877888FC05982D139E2E123360), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8006 = { sizeof (MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1), -1, sizeof(MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8006[4] = 
{
	MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1::get_offset_of_BaseMaterial_4(),
	MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1::get_offset_of_BaseModelMaterial_5(),
	MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_StaticFields::get_offset_of_StaticBaseMaterial_6(),
	MaterialUtility_t3F72F49DCC8471ADEF95C15E9CAA6E94F7D554A1_StaticFields::get_offset_of_StaticBaseModelMaterial_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8007 = { sizeof (BlendMode_t7D8C79146EA953B79703692B14F9929DB3234852)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8007[5] = 
{
	BlendMode_t7D8C79146EA953B79703692B14F9929DB3234852::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8008 = { sizeof (U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859), -1, sizeof(U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8008[3] = 
{
	U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields::get_offset_of_U3CU3E9__11_0_1(),
	U3CU3Ec_t9FC6E2AFD1025F99F2A742A03CBD1951A479C859_StaticFields::get_offset_of_U3CU3E9__11_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8009 = { sizeof (SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8009[2] = 
{
	SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11::get_offset_of_U3CAudioClipU3Ek__BackingField_0(),
	SoundAsset_tAEE27F320584677F41B40DBB483365DB9332FF11::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8010 = { sizeof (U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8010[5] = 
{
	U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31::get_offset_of_path_3(),
	U3CLoadU3Ed__10_t25BE1F4F27DF36B8288802065357A43DA1A1DD31::get_offset_of_callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8011 = { sizeof (U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8011[5] = 
{
	U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8::get_offset_of_U3CU3E1__state_0(),
	U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8::get_offset_of_U3CU3E2__current_1(),
	U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8::get_offset_of_path_2(),
	U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8::get_offset_of_U3CU3E4__this_3(),
	U3CLoadCoroutineU3Ed__12_t156F2DCE7FB94D04E831B4A2790A223F63A430B8::get_offset_of_U3CwwwU3E5__2_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8012 = { sizeof (VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8012[13] = 
{
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_videoDisplayObject_4(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_image_5(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_videoUrl_6(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_streamingVideoUrl_7(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_videoPlayer_8(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_videoSource_9(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_audioSource_10(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_U3CReadyToPlayU3Ek__BackingField_11(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_U3CIsPlayingU3Ek__BackingField_12(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_justPlayedOrPaused_13(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_hasSetVideo_14(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_startY_15(),
	VideoAsset_t47354AA1EA515717B61E4026900A7B6121A7C45A::get_offset_of_U3CNameU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8013 = { sizeof (U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8013[5] = 
{
	U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C::get_offset_of_U3CU3E1__state_0(),
	U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C::get_offset_of_U3CU3E2__current_1(),
	U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C::get_offset_of_U3CU3E4__this_2(),
	U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C::get_offset_of_path_3(),
	U3CLoadU3Ed__20_tEC765CA18A9DD0716930DB0E3575545F168C875C::get_offset_of_callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8014 = { sizeof (U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8014[3] = 
{
	U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD::get_offset_of_U3CU3E1__state_0(),
	U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD::get_offset_of_U3CU3E2__current_1(),
	U3CLoadVideoU3Ed__23_tA75497F5D50A8444941F162D1A4838E0FDCAEACD::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8015 = { sizeof (InterpolationUtilities_tB3BEE2E66A63F05A5DBA9E478390EFF20083BCB7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8016 = { sizeof (PivotAxis_tD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable8016[3] = 
{
	PivotAxis_tD519351ECBC5A42573D8C15ACDDE294FEDC3CCAE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8017 = { sizeof (Billboard_tD4D6D6E7B30BB56685DFE762B93BF784A75644DD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8017[2] = 
{
	Billboard_tD4D6D6E7B30BB56685DFE762B93BF784A75644DD::get_offset_of_PivotAxis_4(),
	Billboard_tD4D6D6E7B30BB56685DFE762B93BF784A75644DD::get_offset_of_TargetTransform_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8018 = { sizeof (CameraExtensions_t601F1DACF4D8AECE26B5FD007561AFB3E612892D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8019 = { sizeof (FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8019[3] = 
{
	FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C::get_offset_of_SizeRatio_4(),
	FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C::get_offset_of_startingDistance_5(),
	FixedAngularSize_tB822C9FDA10A7411779C35C76B76612D527F459C::get_offset_of_startingScale_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8020 = { sizeof (InputManagerWrapper_t8490AF07B31C7938294B081EA9720E1E03CFD356), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8021 = { sizeof (Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8021[22] = 
{
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_UseUnscaledTime_4(),
	0,
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_PositionPerSecond_6(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_RotationDegreesPerSecond_7(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_RotationSpeedScaler_8(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_ScalePerSecond_9(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_SmoothLerpToTarget_10(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_SmoothPositionLerpRatio_11(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_SmoothRotationLerpRatio_12(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_SmoothScaleLerpRatio_13(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_targetPosition_14(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_U3CAnimatingPositionU3Ek__BackingField_15(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_targetRotation_16(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_U3CAnimatingRotationU3Ek__BackingField_17(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_targetLocalRotation_18(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_U3CAnimatingLocalRotationU3Ek__BackingField_19(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_targetLocalScale_20(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_U3CAnimatingLocalScaleU3Ek__BackingField_21(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_InterpolationStarted_22(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_InterpolationDone_23(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_U3CPositionVelocityU3Ek__BackingField_24(),
	Interpolator_tC80908B5E9A91C57719F0F850FB297B39FDA9D0E::get_offset_of_oldPosition_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8022 = { sizeof (MathUtils_t1A5DF035BC45C36A96CD52D3619C162FF6F8DAF1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8023 = { sizeof (U3CU3Ec__DisplayClass22_0_t0245DB3C28FE1B8A105445DF98A432ED35962EC9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8023[2] = 
{
	U3CU3Ec__DisplayClass22_0_t0245DB3C28FE1B8A105445DF98A432ED35962EC9::get_offset_of_nearestPoint_0(),
	U3CU3Ec__DisplayClass22_0_t0245DB3C28FE1B8A105445DF98A432ED35962EC9::get_offset_of_ransac_threshold_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8024 = { sizeof (SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8024[12] = 
{
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_TagalongDistance_4(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_EnforceDistance_5(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_PositionUpdateSpeed_6(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_SmoothMotion_7(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_SmoothingFactor_8(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_tagalongCollider_9(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_interpolator_10(),
	SimpleTagalong_tA1603A02F0AF5AD8047A335785D7D0C29F581495::get_offset_of_frustumPlanes_11(),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8025 = { sizeof (Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8025[12] = 
{
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_MinimumHorizontalOverlap_16(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_TargetHorizontalOverlap_17(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_MinimumVerticalOverlap_18(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_TargetVerticalOverlap_19(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_HorizontalRayCount_20(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_VerticalRayCount_21(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_MinimumTagalongDistance_22(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_MaintainFixedSize_23(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_DepthUpdateSpeed_24(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_defaultTagalongDistance_25(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_DebugDrawLines_26(),
	Tagalong_t10D4B9C92ACCFA1AC80704F7672EB3B2B21BCA51::get_offset_of_DebugPointLight_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8026 = { sizeof (EventItem_tAEF9C49CC34F9E274BD47CD031F87A64E33E478D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8027 = { sizeof (EventActionArgs_t1D1EB1848FA322AD46D3A3B630236952613C7C66), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8028 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8029 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8030 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8031 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8032 = { sizeof (MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8032[1] = 
{
	MenuManager_t03B7ED0689D55E85C85D1E0E239C0CF9AA5E93D6::get_offset_of_menu_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8033 = { sizeof (OnClickEvent_tF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8033[2] = 
{
	OnClickEvent_tF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9::get_offset_of_ClickAction_4(),
	OnClickEvent_tF6032FAC56EC64F6E9F1309B924C7ADB3CDFF6A9::get_offset_of_Gestures_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8034 = { sizeof (OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8034[3] = 
{
	OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23::get_offset_of_U3CTextMeshProU3Ek__BackingField_4(),
	OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23::get_offset_of_logFilePath_5(),
	OnScreenLogger_t18A5079BF4094BF025136ECA260BA5429FC61E23::get_offset_of_FileLoggingEnabled_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8035 = { sizeof (PortableMenu_t1464619C66BEEE803774636017C9557890D6327B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8035[9] = 
{
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_menuItems_4(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_activeMenuItems_5(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_minimizedItems_6(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_previousActiveSize_7(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_menuItemSize_8(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_expandMenuIcon_9(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_expandMenuItem_10(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_menuItemPrefab_11(),
	PortableMenu_t1464619C66BEEE803774636017C9557890D6327B::get_offset_of_U3CMenuItemFactoryU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8036 = { sizeof (PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8036[11] = 
{
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_U3CiconU3Ek__BackingField_4(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_text_5(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_bgTexture_6(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_bgTextureHighlighted_7(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_bgMaterial_8(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_emissionHighlighted_9(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_emissionNormal_10(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_U3CactionU3Ek__BackingField_11(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_ActionPerformed_12(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_U3CInputU3Ek__BackingField_13(),
	PortableMenuItem_t0E428F2A88C511B96FCEC372E47F64C08EF32867::get_offset_of_U3CGesturesU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8037 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8038 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8039 = { sizeof (MenuTransitions_t9BC3AA8497BE1A5EB2CC3A49453589F6CFF0311D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8040 = { sizeof (U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8040[5] = 
{
	U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387::get_offset_of_scriptPageObject_0(),
	U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387::get_offset_of_factory_1(),
	U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387::get_offset_of_scriptMenuItem_2(),
	U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387::get_offset_of_menu_3(),
	U3CU3Ec__DisplayClass0_0_t555D92B3BA25A958C33020FB594D2F6E11F1C387::get_offset_of_objectCollection_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8041 = { sizeof (U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8041[2] = 
{
	U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4::get_offset_of_doneCount_0(),
	U3CU3Ec__DisplayClass1_0_tEFD18B6E4F802F454A28F9A73BF3C0579FC61FB4::get_offset_of_U3CU3E9__0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8042 = { sizeof (U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8042[8] = 
{
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_U3CU3E1__state_0(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_U3CU3E2__current_1(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_objectCollection_2(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_scriptMenuItem_3(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_speed_4(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_U3CU3E8__1_5(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_callback_6(),
	U3CMoveAllTilesToOneTileU3Ed__1_t4E3AEE85BA14E6D98AF036A9179B5DCEEC3F9456::get_offset_of_U3CnumToDoU3E5__2_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8043 = { sizeof (U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8043[2] = 
{
	U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44::get_offset_of_doneCount_0(),
	U3CU3Ec__DisplayClass4_0_t9DF7A7295DF78645B7F16F5D2E285C01E67CCF44::get_offset_of_U3CU3E9__0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8044 = { sizeof (U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8044[7] = 
{
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_U3CU3E1__state_0(),
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_U3CU3E2__current_1(),
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_objectCollection_2(),
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_scriptMenuItem_3(),
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_U3CU3E8__1_4(),
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_callback_5(),
	U3CMinimizeOtherScriptsU3Ed__4_t65DA679E7269F15C7A3ECC0257568C7C3E12CBFA::get_offset_of_U3CnumToDoU3E5__2_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8045 = { sizeof (Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8045[6] = 
{
	Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8::get_offset_of_U3CpositionU3Ek__BackingField_4(),
	Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8::get_offset_of_U3CrotationU3Ek__BackingField_5(),
	Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8::get_offset_of_U3CsizeU3Ek__BackingField_6(),
	Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8::get_offset_of_U3CforwardU3Ek__BackingField_7(),
	Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8::get_offset_of_U3CplaneObjectU3Ek__BackingField_8(),
	Plane_t02B7D4B67352F067291AC498C9478D33B3C97BA8::get_offset_of_anchorTransform_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8046 = { sizeof (Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8046[2] = 
{
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A::get_offset_of_U3CAnchorManagerU3Ek__BackingField_4(),
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A::get_offset_of_U3CAnchorNameU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8047 = { sizeof (CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8047[6] = 
{
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B::get_offset_of_U3CToCacheU3Ek__BackingField_4(),
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B::get_offset_of_U3CCurrentlyCachingU3Ek__BackingField_5(),
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B::get_offset_of_U3CCacheDirectoryU3Ek__BackingField_6(),
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B::get_offset_of_U3CCacheRefPathU3Ek__BackingField_7(),
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B::get_offset_of_overwriteNextItem_8(),
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B::get_offset_of_scriptId_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8048 = { sizeof (U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8), -1, sizeof(U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8048[3] = 
{
	U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
	U3CU3Ec_t6BF1A05C579673058690535A832897270841AAE8_StaticFields::get_offset_of_U3CU3E9__28_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8049 = { sizeof (U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8049[11] = 
{
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CU3E1__state_0(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CU3E2__current_1(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CU3E4__this_2(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_url_3(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_extension_4(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CwwwU3E5__2_5(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CbaseUrlU3E5__3_6(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CfolderU3E5__4_7(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CU3E7__wrap4_8(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CdependencyU3E5__6_9(),
	U3CCacheFileFromUrlU3Ed__28_t3C3D32B597C726B027AED087E921C16A38C9C6F5::get_offset_of_U3CdependencyUrlU3E5__7_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8050 = { sizeof (U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8050[4] = 
{
	U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5::get_offset_of_U3CU3E1__state_0(),
	U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5::get_offset_of_U3CU3E2__current_1(),
	U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5::get_offset_of_U3CU3E4__this_2(),
	U3CCacheFileFromUrlU3Ed__30_tEF8952CC0E73EF175E18A97DBA0BF227D26D99B5::get_offset_of_url_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8051 = { sizeof (U3CU3Ec__DisplayClass32_0_t774F71FDC59FF558B8027AAEBE31ADE91DE0BF93), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8051[1] = 
{
	U3CU3Ec__DisplayClass32_0_t774F71FDC59FF558B8027AAEBE31ADE91DE0BF93::get_offset_of_filePath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8052 = { sizeof (CacheRef_tA734049DBB5504EC88C8B92340D936C2C7B12CA2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8052[1] = 
{
	CacheRef_tA734049DBB5504EC88C8B92340D936C2C7B12CA2::get_offset_of_U3CFileGuidReferencesU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8053 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8054 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8055 = { sizeof (Clickable_t03DB358E3736E8AF9287E385A55E6842DB6ECBFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8055[2] = 
{
	Clickable_t03DB358E3736E8AF9287E385A55E6842DB6ECBFA::get_offset_of_U3CInputU3Ek__BackingField_4(),
	Clickable_t03DB358E3736E8AF9287E385A55E6842DB6ECBFA::get_offset_of_U3CGesturesU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8056 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8058 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8059 = { sizeof (AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4), -1, sizeof(AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8059[3] = 
{
	AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields::get_offset_of_RotateHandleMultiplier_0(),
	AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields::get_offset_of_TranslateHandleMultiplier_1(),
	AdjustmentConstants_tD466F164137F79A88F079CF0738FBFFFD0D71BA4_StaticFields::get_offset_of_ScaleHandleMultiplier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8060 = { sizeof (GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8060[7] = 
{
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_U3CIsManipulatingU3Ek__BackingField_4(),
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_U3CManipulationDeltaU3Ek__BackingField_5(),
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_DragUpdatedEvent_6(),
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_U3CObjectClickListenersU3Ek__BackingField_7(),
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_scriptManagerClickListener_8(),
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_U3C_inputU3Ek__BackingField_9(),
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909::get_offset_of_graphicRaycaster_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8061 = { sizeof (U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18), -1, sizeof(U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8061[2] = 
{
	U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1911CBDE20C811C0D61136F31183C0E10ED51E18_StaticFields::get_offset_of_U3CU3E9__28_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8062 = { sizeof (ManipulationDeltaEventArgs_t4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8062[1] = 
{
	ManipulationDeltaEventArgs_t4E24C2180ACC462E90FDF0ED0AF487FC77FFD7B8::get_offset_of_U3CManipulationDeltaU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8063 = { sizeof (RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8063[4] = 
{
	RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25::get_offset_of_U3COriginU3Ek__BackingField_0(),
	RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25::get_offset_of_U3CDirectionU3Ek__BackingField_1(),
	RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25::get_offset_of_U3CDistanceU3Ek__BackingField_2(),
	RaycastInfo_t1126512F6856BDC86C39EFC3F683FC189343EF25::get_offset_of_U3CHitU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8064 = { sizeof (ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F), -1, sizeof(ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8064[2] = 
{
	ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_StaticFields::get_offset_of_DefaultObjectSize_0(),
	ScaleConstants_tF03C26B145C753664A2C46B59E2FBFA6BA1CCF6F_StaticFields::get_offset_of_HandleDistanceScaleModifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8065 = { sizeof (EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8065[13] = 
{
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_mainBox_4(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_highlightColor_5(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_originalColor_6(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_isManipulating_7(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_audioSource_8(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_playedGrabSound_9(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_playedReleaseSound_10(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_material_11(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_ourCollider_12(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_ourRenderer_13(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_U3CInputU3Ek__BackingField_14(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_U3CGestureManagerU3Ek__BackingField_15(),
	EditHandle_tC9BD695B96B73D220F79439CBB7DB559FDD5F482::get_offset_of_U3CManipulationDeltaU3Ek__BackingField_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8066 = { sizeof (U3CU3Ec__DisplayClass26_0_tFA3904F923F73553B470AC6F744107C95EBCE625), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8066[1] = 
{
	U3CU3Ec__DisplayClass26_0_tFA3904F923F73553B470AC6F744107C95EBCE625::get_offset_of_hitObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8067 = { sizeof (U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2), -1, sizeof(U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8067[2] = 
{
	U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tAA5386B539E4B7CA134E15C55C6062891613B2B2_StaticFields::get_offset_of_U3CU3E9__26_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8068 = { sizeof (ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8068[11] = 
{
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_frameColor_4(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_frameGroupName_5(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_cornerColor_6(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_cornerGroupName_7(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_rotateHandleColor_8(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_rotateHandleGroupName_9(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_faceGroupName_10(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_U3CObjectU3Ek__BackingField_11(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_exhibitItemOffset_12(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_exhibitItemBorder_13(),
	ExhibitBox_t889C2D58A95ED32320CE1B582E8389B95D72DF2A::get_offset_of_U3CInitializedU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8069 = { sizeof (ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8069[15] = 
{
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_doneIcon_4(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_doneExhibitIcon_5(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_adjustIcon_6(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_removeIcon_7(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_doneButton_8(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_doneExhibitButton_9(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_adjustButton_10(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_removeButton_11(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_menu_12(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_menuPrefab_13(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_mainBox_14(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_potentialMenuPositions_15(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_moveVelocity_16(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_initialized_17(),
	ExhibitBoxMenu_t8D38DF0ED97F1E7AEF9AD856D9C7FF9B12890F22::get_offset_of_U3CMenuFactoryU3Ek__BackingField_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8070 = { sizeof (RotateHandle_tEDA7A218D238A0E62FC8505434D0883763C450FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8070[1] = 
{
	RotateHandle_tEDA7A218D238A0E62FC8505434D0883763C450FC::get_offset_of_axis_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8071 = { sizeof (ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8071[3] = 
{
	ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F::get_offset_of_objects_17(),
	ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F::get_offset_of_origin_18(),
	ScaleHandle_t10290945CE09A85D295DDD8F21573B991DEE686F::get_offset_of_maxScalePerSecond_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8072 = { sizeof (TranslateHandle_tBE476299B49CB94E8B12FEC82A4E64321E7FD2AB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8073 = { sizeof (FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8073[2] = 
{
	FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300::get_offset_of_parentToIgnore_4(),
	FixedScale_tD0A844F7D78D0917F8439BBFF615ADF303447300::get_offset_of_scaleOnAwake_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8074 = { sizeof (FixedSizeOnScreen_t594F7D0BB1AAAAD1FCE8BCA2DD19DF70BF099792), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8075 = { sizeof (GameObjectUtility_tBD9E90EDD43468EE35579EFC7712B7D7B3E5738B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8076 = { sizeof (U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8076[9] = 
{
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_U3CU3E1__state_0(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_U3CU3E2__current_1(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_speed_2(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_from_3(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_to_4(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_objectToMove_5(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_disappearAtEnd_6(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_U3CstepU3E5__2_7(),
	U3CMoveFromToU3Ed__0_tE7B3A0A5BE8F70BBF61523AA7D98F41E9630038A::get_offset_of_U3CtU3E5__3_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8077 = { sizeof (U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8077[8] = 
{
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_U3CU3E1__state_0(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_U3CU3E2__current_1(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_objectToMove_2(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_from_3(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_to_4(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_speed_5(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_disappearAtEnd_6(),
	U3CMoveFromToU3Ed__1_tCCA8FDA0BD51E870D82FFCA1A9C1C012CF5D4DB9::get_offset_of_callback_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8078 = { sizeof (U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8078[8] = 
{
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_U3CU3E1__state_0(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_U3CU3E2__current_1(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_speed_2(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_from_3(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_to_4(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_objectToScale_5(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_U3CstepU3E5__2_6(),
	U3CScaleFromToU3Ed__2_tEF2191590C396B55DA4F6E1B0A93C3C0DA41D7B0::get_offset_of_U3CtU3E5__3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8079 = { sizeof (U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8079[7] = 
{
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_U3CU3E1__state_0(),
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_U3CU3E2__current_1(),
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_objectToScale_2(),
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_from_3(),
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_to_4(),
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_speed_5(),
	U3CScaleFromToU3Ed__3_tBA73FBB25597211A6B63220B64586C64D46FD7CD::get_offset_of_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8080 = { sizeof (U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8080[7] = 
{
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_U3CU3E1__state_0(),
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_U3CU3E2__current_1(),
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_objectToScale_2(),
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_xFrom_3(),
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_xTo_4(),
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_speed_5(),
	U3CScaleSidewaysFromToU3Ed__4_t73C28E9B6597C7276FFD8F7C9096E5905044AB6D::get_offset_of_callback_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8081 = { sizeof (U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8081[8] = 
{
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_U3CU3E1__state_0(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_U3CU3E2__current_1(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_speed_2(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_xFrom_3(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_xTo_4(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_objectToScale_5(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_U3CstepU3E5__2_6(),
	U3CScaleSidewaysFromToU3Ed__5_tB67DD25745103FB64743632BB0448766C4EABBD3::get_offset_of_U3CtU3E5__3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8082 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable8082[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8083 = { sizeof (PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8083[1] = 
{
	PrefabUtility_t0933A572FCD91DB00F96CCD1DE333BC8728B00B2::get_offset_of_VideoControlsFactory_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8084 = { sizeof (CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8085 = { sizeof (ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8085[1] = 
{
	ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8086 = { sizeof (InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8087 = { sizeof (InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8087[1] = 
{
	InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8088 = { sizeof (InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8088[1] = 
{
	InScriptMenuFactory_tE28604EF9A7748F0689F833E699CABF93F4DA7B9::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8089 = { sizeof (InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8089[1] = 
{
	InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8090 = { sizeof (PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8091 = { sizeof (PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8092 = { sizeof (PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8092[1] = 
{
	PortableMenuFactory_tAA97D5D2F11B3C26CBC04242D63940BFFBE0070D::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8093 = { sizeof (PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8093[1] = 
{
	PortableMenuItemFactory_t1FC8A3AFAEBDFF9681B68E5868C9CBC3D5A6229D::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8094 = { sizeof (ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8094[1] = 
{
	ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8095 = { sizeof (ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8095[1] = 
{
	ScriptPageFactory_tA40077659A74AE7D8F8ADA11A5269F63ED46D344::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8096 = { sizeof (VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8096[1] = 
{
	VideoControlsFactory_tADFD460F3C6A69B25A0948738BEB1B2BC5692FD6::get_offset_of_U3CRootAnchorU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8097 = { sizeof (HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8097[10] = 
{
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_id_0(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_xPosition_1(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_yPosition_2(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_zPosition_3(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_xRotation_4(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_yRotation_5(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_zRotation_6(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_xScale_7(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_yScale_8(),
	HologramData_t8A6628915B2D60864E68D360E64DA1F9BE9A58D9::get_offset_of_zScale_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8098 = { sizeof (SceneData_tDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable8098[2] = 
{
	SceneData_tDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94::get_offset_of_centerWorldAnchorName_0(),
	SceneData_tDDD3EC42791F60AE1CF1432A9DE1D183F73DAD94::get_offset_of_holograms_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize8099 = { sizeof (Config_t907A4A74290AFDE6CE4567874473D26415967929), -1, sizeof(Config_t907A4A74290AFDE6CE4567874473D26415967929_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable8099[1] = 
{
	Config_t907A4A74290AFDE6CE4567874473D26415967929_StaticFields::get_offset_of_BaseUrl_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
