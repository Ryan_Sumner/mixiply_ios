﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<Zenject.DiContainer>
struct Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>>
struct Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer>
struct IEnumerable_1_t501BEE581587B657C1C54D40EC56F233F40B12CE;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext>
struct IEnumerator_1_tD162132E5E26B75AD30F440257FC3EA585E18C16;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour>
struct List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7;
// System.Collections.Generic.List`1<Zenject.DiContainer>
struct List_1_t613607F896640D94824672D01078AB4BBFA62166;
// System.Collections.Generic.List`1<Zenject.IBindingFinalizer>
struct List_1_tDB9537C32346F69532603D22AD9870A14679F4E4;
// System.Collections.Generic.List`1<Zenject.ILazy>
struct List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10;
// System.Collections.Generic.List`1<Zenject.InstallerBase>
struct List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595;
// System.Collections.Generic.List`1<Zenject.MonoInstaller>
struct List_1_tED6028C19769161B40D318A4663FA052CF5836D2;
// System.Collections.Generic.List`1<Zenject.SceneDecoratorContext>
struct List_1_t965894C1D41921F28AA28474913EA8675F5FB4E7;
// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller>
struct List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7;
// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer>
struct Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686;
// System.Collections.Generic.Stack`1<Zenject.DiContainer/LookupId>
struct Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,System.Boolean>
struct Func_2_t72AA8553B326A6D89596D55394159592E77AC54F;
// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,System.Int32>
struct Func_2_t54167D1E48BBD4811E4C2594B578819F857FE6A6;
// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,Zenject.DiContainer/ProviderPair>
struct Func_2_tC4BAAA151142D94B7DBD34C3CBF4C78D05087F78;
// System.Func`2<System.Object,Zenject.TypeValuePair>
struct Func_2_t39486D74B56C590C2243B9DBD80C5CFE7948E3DB;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0;
// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>>
struct Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF;
// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneDecoratorContext>>
struct Func_2_tED73F70B9027353D18292133FC6DB6C7F48EA37C;
// System.Func`2<UnityEngine.SceneManagement.Scene,System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>>
struct Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46;
// System.Func`2<Zenject.DiContainer,System.Collections.Generic.IEnumerable`1<Zenject.IBindingFinalizer>>
struct Func_2_t9623780109677053A67A8E994E57A384DC9A508D;
// System.Func`2<Zenject.DiContainer/ProviderPair,Zenject.IProvider>
struct Func_2_t6EE65B479AFB2180EFB9468EC189EE29DFB56F0B;
// System.Func`2<Zenject.SceneContext,Zenject.DiContainer>
struct Func_2_t816D7F752AC8679BB34EFE304A349306C22C5C35;
// System.Func`2<Zenject.TypeValuePair,System.String>
struct Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Component[]
struct ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.BindingCondition
struct BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3;
// Zenject.BindingId
struct BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1;
// Zenject.Context
struct Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;
// Zenject.DiContainer/ProviderInfo
struct ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F;
// Zenject.IProvider
struct IProvider_t5CB6F6DB730623507AEEA8FF558BFF4690973231;
// Zenject.InjectContext
struct InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649;
// Zenject.LazyInstanceInjector
struct LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E;
// Zenject.MonoKernel
struct MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751;
// Zenject.SingletonProviderCreator
struct SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BINDINGID_TCC0244F0D309405E44E49E79ECDC84102D127DB1_H
#define BINDINGID_TCC0244F0D309405E44E49E79ECDC84102D127DB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingId
struct  BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1  : public RuntimeObject
{
public:
	// System.Type Zenject.BindingId::Type
	Type_t * ___Type_0;
	// System.Object Zenject.BindingId::Identifier
	RuntimeObject * ___Identifier_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1, ___Identifier_1)); }
	inline RuntimeObject * get_Identifier_1() const { return ___Identifier_1; }
	inline RuntimeObject ** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(RuntimeObject * value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGID_TCC0244F0D309405E44E49E79ECDC84102D127DB1_H
#ifndef U3CU3EC_T5F7A3E096183BC2033F0FB24FF078D95F21F8735_H
#define U3CU3EC_T5F7A3E096183BC2033F0FB24FF078D95F21F8735_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Context/<>c
struct  U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735_StaticFields
{
public:
	// Zenject.Context/<>c Zenject.Context/<>c::<>9
	U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735 * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> Zenject.Context/<>c::<>9__16_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__16_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735_StaticFields, ___U3CU3E9__16_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__16_0_1() const { return ___U3CU3E9__16_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__16_0_1() { return &___U3CU3E9__16_0_1; }
	inline void set_U3CU3E9__16_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__16_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T5F7A3E096183BC2033F0FB24FF078D95F21F8735_H
#ifndef DICONTAINER_TBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9_H
#define DICONTAINER_TBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer
struct  DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>> Zenject.DiContainer::_providers
	Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 * ____providers_1;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_parentContainers
	List_1_t613607F896640D94824672D01078AB4BBFA62166 * ____parentContainers_2;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_ancestorContainers
	List_1_t613607F896640D94824672D01078AB4BBFA62166 * ____ancestorContainers_3;
	// System.Collections.Generic.Stack`1<Zenject.DiContainer/LookupId> Zenject.DiContainer::_resolvesInProgress
	Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F * ____resolvesInProgress_4;
	// Zenject.SingletonProviderCreator Zenject.DiContainer::_singletonProviderCreator
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D * ____singletonProviderCreator_5;
	// Zenject.SingletonMarkRegistry Zenject.DiContainer::_singletonMarkRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____singletonMarkRegistry_6;
	// Zenject.LazyInstanceInjector Zenject.DiContainer::_lazyInjector
	LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E * ____lazyInjector_7;
	// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_currentBindings
	Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 * ____currentBindings_8;
	// System.Collections.Generic.List`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_childBindings
	List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 * ____childBindings_9;
	// System.Collections.Generic.List`1<Zenject.ILazy> Zenject.DiContainer::_lateBindingsToValidate
	List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 * ____lateBindingsToValidate_10;
	// System.Boolean Zenject.DiContainer::_isFinalizingBinding
	bool ____isFinalizingBinding_11;
	// System.Boolean Zenject.DiContainer::_isValidating
	bool ____isValidating_12;
	// System.Boolean Zenject.DiContainer::_isInstalling
	bool ____isInstalling_13;
	// System.Boolean Zenject.DiContainer::_hasDisplayedInstallWarning
	bool ____hasDisplayedInstallWarning_14;
	// System.Boolean Zenject.DiContainer::<ShouldCheckForInstallWarning>k__BackingField
	bool ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15;
	// System.Boolean Zenject.DiContainer::<AssertOnNewGameObjects>k__BackingField
	bool ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16;
	// UnityEngine.Transform Zenject.DiContainer::<DefaultParent>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CDefaultParentU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of__providers_1() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____providers_1)); }
	inline Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 * get__providers_1() const { return ____providers_1; }
	inline Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 ** get_address_of__providers_1() { return &____providers_1; }
	inline void set__providers_1(Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 * value)
	{
		____providers_1 = value;
		Il2CppCodeGenWriteBarrier((&____providers_1), value);
	}

	inline static int32_t get_offset_of__parentContainers_2() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____parentContainers_2)); }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 * get__parentContainers_2() const { return ____parentContainers_2; }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 ** get_address_of__parentContainers_2() { return &____parentContainers_2; }
	inline void set__parentContainers_2(List_1_t613607F896640D94824672D01078AB4BBFA62166 * value)
	{
		____parentContainers_2 = value;
		Il2CppCodeGenWriteBarrier((&____parentContainers_2), value);
	}

	inline static int32_t get_offset_of__ancestorContainers_3() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____ancestorContainers_3)); }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 * get__ancestorContainers_3() const { return ____ancestorContainers_3; }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 ** get_address_of__ancestorContainers_3() { return &____ancestorContainers_3; }
	inline void set__ancestorContainers_3(List_1_t613607F896640D94824672D01078AB4BBFA62166 * value)
	{
		____ancestorContainers_3 = value;
		Il2CppCodeGenWriteBarrier((&____ancestorContainers_3), value);
	}

	inline static int32_t get_offset_of__resolvesInProgress_4() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____resolvesInProgress_4)); }
	inline Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F * get__resolvesInProgress_4() const { return ____resolvesInProgress_4; }
	inline Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F ** get_address_of__resolvesInProgress_4() { return &____resolvesInProgress_4; }
	inline void set__resolvesInProgress_4(Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F * value)
	{
		____resolvesInProgress_4 = value;
		Il2CppCodeGenWriteBarrier((&____resolvesInProgress_4), value);
	}

	inline static int32_t get_offset_of__singletonProviderCreator_5() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____singletonProviderCreator_5)); }
	inline SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D * get__singletonProviderCreator_5() const { return ____singletonProviderCreator_5; }
	inline SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D ** get_address_of__singletonProviderCreator_5() { return &____singletonProviderCreator_5; }
	inline void set__singletonProviderCreator_5(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D * value)
	{
		____singletonProviderCreator_5 = value;
		Il2CppCodeGenWriteBarrier((&____singletonProviderCreator_5), value);
	}

	inline static int32_t get_offset_of__singletonMarkRegistry_6() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____singletonMarkRegistry_6)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__singletonMarkRegistry_6() const { return ____singletonMarkRegistry_6; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__singletonMarkRegistry_6() { return &____singletonMarkRegistry_6; }
	inline void set__singletonMarkRegistry_6(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____singletonMarkRegistry_6 = value;
		Il2CppCodeGenWriteBarrier((&____singletonMarkRegistry_6), value);
	}

	inline static int32_t get_offset_of__lazyInjector_7() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____lazyInjector_7)); }
	inline LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E * get__lazyInjector_7() const { return ____lazyInjector_7; }
	inline LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E ** get_address_of__lazyInjector_7() { return &____lazyInjector_7; }
	inline void set__lazyInjector_7(LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E * value)
	{
		____lazyInjector_7 = value;
		Il2CppCodeGenWriteBarrier((&____lazyInjector_7), value);
	}

	inline static int32_t get_offset_of__currentBindings_8() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____currentBindings_8)); }
	inline Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 * get__currentBindings_8() const { return ____currentBindings_8; }
	inline Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 ** get_address_of__currentBindings_8() { return &____currentBindings_8; }
	inline void set__currentBindings_8(Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 * value)
	{
		____currentBindings_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentBindings_8), value);
	}

	inline static int32_t get_offset_of__childBindings_9() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____childBindings_9)); }
	inline List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 * get__childBindings_9() const { return ____childBindings_9; }
	inline List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 ** get_address_of__childBindings_9() { return &____childBindings_9; }
	inline void set__childBindings_9(List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 * value)
	{
		____childBindings_9 = value;
		Il2CppCodeGenWriteBarrier((&____childBindings_9), value);
	}

	inline static int32_t get_offset_of__lateBindingsToValidate_10() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____lateBindingsToValidate_10)); }
	inline List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 * get__lateBindingsToValidate_10() const { return ____lateBindingsToValidate_10; }
	inline List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 ** get_address_of__lateBindingsToValidate_10() { return &____lateBindingsToValidate_10; }
	inline void set__lateBindingsToValidate_10(List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 * value)
	{
		____lateBindingsToValidate_10 = value;
		Il2CppCodeGenWriteBarrier((&____lateBindingsToValidate_10), value);
	}

	inline static int32_t get_offset_of__isFinalizingBinding_11() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____isFinalizingBinding_11)); }
	inline bool get__isFinalizingBinding_11() const { return ____isFinalizingBinding_11; }
	inline bool* get_address_of__isFinalizingBinding_11() { return &____isFinalizingBinding_11; }
	inline void set__isFinalizingBinding_11(bool value)
	{
		____isFinalizingBinding_11 = value;
	}

	inline static int32_t get_offset_of__isValidating_12() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____isValidating_12)); }
	inline bool get__isValidating_12() const { return ____isValidating_12; }
	inline bool* get_address_of__isValidating_12() { return &____isValidating_12; }
	inline void set__isValidating_12(bool value)
	{
		____isValidating_12 = value;
	}

	inline static int32_t get_offset_of__isInstalling_13() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____isInstalling_13)); }
	inline bool get__isInstalling_13() const { return ____isInstalling_13; }
	inline bool* get_address_of__isInstalling_13() { return &____isInstalling_13; }
	inline void set__isInstalling_13(bool value)
	{
		____isInstalling_13 = value;
	}

	inline static int32_t get_offset_of__hasDisplayedInstallWarning_14() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____hasDisplayedInstallWarning_14)); }
	inline bool get__hasDisplayedInstallWarning_14() const { return ____hasDisplayedInstallWarning_14; }
	inline bool* get_address_of__hasDisplayedInstallWarning_14() { return &____hasDisplayedInstallWarning_14; }
	inline void set__hasDisplayedInstallWarning_14(bool value)
	{
		____hasDisplayedInstallWarning_14 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15)); }
	inline bool get_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() const { return ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return &___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline void set_U3CShouldCheckForInstallWarningU3Ek__BackingField_15(bool value)
	{
		___U3CShouldCheckForInstallWarningU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16)); }
	inline bool get_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() const { return ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return &___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline void set_U3CAssertOnNewGameObjectsU3Ek__BackingField_16(bool value)
	{
		___U3CAssertOnNewGameObjectsU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultParentU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ___U3CDefaultParentU3Ek__BackingField_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CDefaultParentU3Ek__BackingField_17() const { return ___U3CDefaultParentU3Ek__BackingField_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CDefaultParentU3Ek__BackingField_17() { return &___U3CDefaultParentU3Ek__BackingField_17; }
	inline void set_U3CDefaultParentU3Ek__BackingField_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CDefaultParentU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultParentU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICONTAINER_TBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9_H
#ifndef U3CU3EC_T8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_H
#define U3CU3EC_T8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c
struct  U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields
{
public:
	// Zenject.DiContainer/<>c Zenject.DiContainer/<>c::<>9
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2 * ___U3CU3E9_0;
	// System.Func`2<Zenject.DiContainer,System.Collections.Generic.IEnumerable`1<Zenject.IBindingFinalizer>> Zenject.DiContainer/<>c::<>9__19_0
	Func_2_t9623780109677053A67A8E994E57A384DC9A508D * ___U3CU3E9__19_0_1;
	// System.Func`2<Zenject.DiContainer/ProviderPair,Zenject.IProvider> Zenject.DiContainer/<>c::<>9__56_0
	Func_2_t6EE65B479AFB2180EFB9468EC189EE29DFB56F0B * ___U3CU3E9__56_0_2;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer/<>c::<>9__72_1
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__72_1_3;
	// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,System.Int32> Zenject.DiContainer/<>c::<>9__73_1
	Func_2_t54167D1E48BBD4811E4C2594B578819F857FE6A6 * ___U3CU3E9__73_1_4;
	// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,System.Boolean> Zenject.DiContainer/<>c::<>9__73_3
	Func_2_t72AA8553B326A6D89596D55394159592E77AC54F * ___U3CU3E9__73_3_5;
	// System.Func`2<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>,Zenject.DiContainer/ProviderPair> Zenject.DiContainer/<>c::<>9__73_4
	Func_2_tC4BAAA151142D94B7DBD34C3CBF4C78D05087F78 * ___U3CU3E9__73_4_6;
	// System.Func`2<Zenject.TypeValuePair,System.String> Zenject.DiContainer/<>c::<>9__81_0
	Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD * ___U3CU3E9__81_0_7;
	// System.Func`2<Zenject.TypeValuePair,System.String> Zenject.DiContainer/<>c::<>9__83_0
	Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD * ___U3CU3E9__83_0_8;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer/<>c::<>9__171_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__171_0_9;
	// System.Func`2<System.Type,System.Boolean> Zenject.DiContainer/<>c::<>9__172_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__172_0_10;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__19_0_1)); }
	inline Func_2_t9623780109677053A67A8E994E57A384DC9A508D * get_U3CU3E9__19_0_1() const { return ___U3CU3E9__19_0_1; }
	inline Func_2_t9623780109677053A67A8E994E57A384DC9A508D ** get_address_of_U3CU3E9__19_0_1() { return &___U3CU3E9__19_0_1; }
	inline void set_U3CU3E9__19_0_1(Func_2_t9623780109677053A67A8E994E57A384DC9A508D * value)
	{
		___U3CU3E9__19_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__56_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__56_0_2)); }
	inline Func_2_t6EE65B479AFB2180EFB9468EC189EE29DFB56F0B * get_U3CU3E9__56_0_2() const { return ___U3CU3E9__56_0_2; }
	inline Func_2_t6EE65B479AFB2180EFB9468EC189EE29DFB56F0B ** get_address_of_U3CU3E9__56_0_2() { return &___U3CU3E9__56_0_2; }
	inline void set_U3CU3E9__56_0_2(Func_2_t6EE65B479AFB2180EFB9468EC189EE29DFB56F0B * value)
	{
		___U3CU3E9__56_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__56_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__72_1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__72_1_3)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__72_1_3() const { return ___U3CU3E9__72_1_3; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__72_1_3() { return &___U3CU3E9__72_1_3; }
	inline void set_U3CU3E9__72_1_3(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__72_1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__72_1_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__73_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__73_1_4)); }
	inline Func_2_t54167D1E48BBD4811E4C2594B578819F857FE6A6 * get_U3CU3E9__73_1_4() const { return ___U3CU3E9__73_1_4; }
	inline Func_2_t54167D1E48BBD4811E4C2594B578819F857FE6A6 ** get_address_of_U3CU3E9__73_1_4() { return &___U3CU3E9__73_1_4; }
	inline void set_U3CU3E9__73_1_4(Func_2_t54167D1E48BBD4811E4C2594B578819F857FE6A6 * value)
	{
		___U3CU3E9__73_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__73_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__73_3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__73_3_5)); }
	inline Func_2_t72AA8553B326A6D89596D55394159592E77AC54F * get_U3CU3E9__73_3_5() const { return ___U3CU3E9__73_3_5; }
	inline Func_2_t72AA8553B326A6D89596D55394159592E77AC54F ** get_address_of_U3CU3E9__73_3_5() { return &___U3CU3E9__73_3_5; }
	inline void set_U3CU3E9__73_3_5(Func_2_t72AA8553B326A6D89596D55394159592E77AC54F * value)
	{
		___U3CU3E9__73_3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__73_3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__73_4_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__73_4_6)); }
	inline Func_2_tC4BAAA151142D94B7DBD34C3CBF4C78D05087F78 * get_U3CU3E9__73_4_6() const { return ___U3CU3E9__73_4_6; }
	inline Func_2_tC4BAAA151142D94B7DBD34C3CBF4C78D05087F78 ** get_address_of_U3CU3E9__73_4_6() { return &___U3CU3E9__73_4_6; }
	inline void set_U3CU3E9__73_4_6(Func_2_tC4BAAA151142D94B7DBD34C3CBF4C78D05087F78 * value)
	{
		___U3CU3E9__73_4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__73_4_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__81_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__81_0_7)); }
	inline Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD * get_U3CU3E9__81_0_7() const { return ___U3CU3E9__81_0_7; }
	inline Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD ** get_address_of_U3CU3E9__81_0_7() { return &___U3CU3E9__81_0_7; }
	inline void set_U3CU3E9__81_0_7(Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD * value)
	{
		___U3CU3E9__81_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__81_0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__83_0_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__83_0_8)); }
	inline Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD * get_U3CU3E9__83_0_8() const { return ___U3CU3E9__83_0_8; }
	inline Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD ** get_address_of_U3CU3E9__83_0_8() { return &___U3CU3E9__83_0_8; }
	inline void set_U3CU3E9__83_0_8(Func_2_tCE54115E63E4136824F0D1E6440680470AF82BDD * value)
	{
		___U3CU3E9__83_0_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__83_0_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__171_0_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__171_0_9)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__171_0_9() const { return ___U3CU3E9__171_0_9; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__171_0_9() { return &___U3CU3E9__171_0_9; }
	inline void set_U3CU3E9__171_0_9(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__171_0_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__171_0_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__172_0_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields, ___U3CU3E9__172_0_10)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__172_0_10() const { return ___U3CU3E9__172_0_10; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__172_0_10() { return &___U3CU3E9__172_0_10; }
	inline void set_U3CU3E9__172_0_10(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__172_0_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__172_0_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_H
#ifndef U3CU3EC__DISPLAYCLASS49_0_TE952E94AC2D9F07E2D4E3987967341CF44AC9C77_H
#define U3CU3EC__DISPLAYCLASS49_0_TE952E94AC2D9F07E2D4E3987967341CF44AC9C77_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass49_0
struct  U3CU3Ec__DisplayClass49_0_tE952E94AC2D9F07E2D4E3987967341CF44AC9C77  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer/<>c__DisplayClass49_0::injectContext
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___injectContext_0;

public:
	inline static int32_t get_offset_of_injectContext_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass49_0_tE952E94AC2D9F07E2D4E3987967341CF44AC9C77, ___injectContext_0)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_injectContext_0() const { return ___injectContext_0; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_injectContext_0() { return &___injectContext_0; }
	inline void set_injectContext_0(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___injectContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___injectContext_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS49_0_TE952E94AC2D9F07E2D4E3987967341CF44AC9C77_H
#ifndef U3CU3EC__DISPLAYCLASS57_0_T993B926C458D0C3939726A9A3D488AA0D414229A_H
#define U3CU3EC__DISPLAYCLASS57_0_T993B926C458D0C3939726A9A3D488AA0D414229A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass57_0
struct  U3CU3Ec__DisplayClass57_0_t993B926C458D0C3939726A9A3D488AA0D414229A  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer/<>c__DisplayClass57_0::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass57_0_t993B926C458D0C3939726A9A3D488AA0D414229A, ___context_0)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_0() const { return ___context_0; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS57_0_T993B926C458D0C3939726A9A3D488AA0D414229A_H
#ifndef PROVIDERINFO_T8A37C11119853185A108C7C91EB7A5841246D76F_H
#define PROVIDERINFO_T8A37C11119853185A108C7C91EB7A5841246D76F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/ProviderInfo
struct  ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F  : public RuntimeObject
{
public:
	// Zenject.IProvider Zenject.DiContainer/ProviderInfo::<Provider>k__BackingField
	RuntimeObject* ___U3CProviderU3Ek__BackingField_0;
	// Zenject.BindingCondition Zenject.DiContainer/ProviderInfo::<Condition>k__BackingField
	BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 * ___U3CConditionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CProviderU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F, ___U3CProviderU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CProviderU3Ek__BackingField_0() const { return ___U3CProviderU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CProviderU3Ek__BackingField_0() { return &___U3CProviderU3Ek__BackingField_0; }
	inline void set_U3CProviderU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CProviderU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CConditionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F, ___U3CConditionU3Ek__BackingField_1)); }
	inline BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 * get_U3CConditionU3Ek__BackingField_1() const { return ___U3CConditionU3Ek__BackingField_1; }
	inline BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 ** get_address_of_U3CConditionU3Ek__BackingField_1() { return &___U3CConditionU3Ek__BackingField_1; }
	inline void set_U3CConditionU3Ek__BackingField_1(BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 * value)
	{
		___U3CConditionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERINFO_T8A37C11119853185A108C7C91EB7A5841246D76F_H
#ifndef PROVIDERPAIR_T7634A1A771CA27C61BF8290B1C7AFA9214A89828_H
#define PROVIDERPAIR_T7634A1A771CA27C61BF8290B1C7AFA9214A89828_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/ProviderPair
struct  ProviderPair_t7634A1A771CA27C61BF8290B1C7AFA9214A89828  : public RuntimeObject
{
public:
	// Zenject.DiContainer/ProviderInfo Zenject.DiContainer/ProviderPair::<ProviderInfo>k__BackingField
	ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F * ___U3CProviderInfoU3Ek__BackingField_0;
	// Zenject.DiContainer Zenject.DiContainer/ProviderPair::<Container>k__BackingField
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CContainerU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CProviderInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderPair_t7634A1A771CA27C61BF8290B1C7AFA9214A89828, ___U3CProviderInfoU3Ek__BackingField_0)); }
	inline ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F * get_U3CProviderInfoU3Ek__BackingField_0() const { return ___U3CProviderInfoU3Ek__BackingField_0; }
	inline ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F ** get_address_of_U3CProviderInfoU3Ek__BackingField_0() { return &___U3CProviderInfoU3Ek__BackingField_0; }
	inline void set_U3CProviderInfoU3Ek__BackingField_0(ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F * value)
	{
		___U3CProviderInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderInfoU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProviderPair_t7634A1A771CA27C61BF8290B1C7AFA9214A89828, ___U3CContainerU3Ek__BackingField_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CContainerU3Ek__BackingField_1() const { return ___U3CContainerU3Ek__BackingField_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CContainerU3Ek__BackingField_1() { return &___U3CContainerU3Ek__BackingField_1; }
	inline void set_U3CContainerU3Ek__BackingField_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CContainerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContainerU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERPAIR_T7634A1A771CA27C61BF8290B1C7AFA9214A89828_H
#ifndef INJECTARGS_T8A0EB8658CA9A35034FFFFA08D1DCB03051E521E_H
#define INJECTARGS_T8A0EB8658CA9A35034FFFFA08D1DCB03051E521E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectArgs
struct  InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InjectArgs::ExtraArgs
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___ExtraArgs_0;
	// Zenject.InjectContext Zenject.InjectArgs::Context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___Context_1;
	// System.Object Zenject.InjectArgs::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_2;

public:
	inline static int32_t get_offset_of_ExtraArgs_0() { return static_cast<int32_t>(offsetof(InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E, ___ExtraArgs_0)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_ExtraArgs_0() const { return ___ExtraArgs_0; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_ExtraArgs_0() { return &___ExtraArgs_0; }
	inline void set_ExtraArgs_0(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___ExtraArgs_0 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraArgs_0), value);
	}

	inline static int32_t get_offset_of_Context_1() { return static_cast<int32_t>(offsetof(InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E, ___Context_1)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_Context_1() const { return ___Context_1; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_Context_1() { return &___Context_1; }
	inline void set_Context_1(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___Context_1 = value;
		Il2CppCodeGenWriteBarrier((&___Context_1), value);
	}

	inline static int32_t get_offset_of_ConcreteIdentifier_2() { return static_cast<int32_t>(offsetof(InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E, ___ConcreteIdentifier_2)); }
	inline RuntimeObject * get_ConcreteIdentifier_2() const { return ___ConcreteIdentifier_2; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_2() { return &___ConcreteIdentifier_2; }
	inline void set_ConcreteIdentifier_2(RuntimeObject * value)
	{
		___ConcreteIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTARGS_T8A0EB8658CA9A35034FFFFA08D1DCB03051E521E_H
#ifndef U3CGET_ALLOBJECTTYPESU3ED__53_T3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC_H
#define U3CGET_ALLOBJECTTYPESU3ED__53_T3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext/<get_AllObjectTypes>d__53
struct  U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InjectContext/<get_AllObjectTypes>d__53::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Zenject.InjectContext/<get_AllObjectTypes>d__53::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Zenject.InjectContext/<get_AllObjectTypes>d__53::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectContext Zenject.InjectContext/<get_AllObjectTypes>d__53::<>4__this
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext/<get_AllObjectTypes>d__53::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC, ___U3CU3E4__this_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_ALLOBJECTTYPESU3ED__53_T3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC_H
#ifndef U3CGET_PARENTCONTEXTSU3ED__49_TF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6_H
#define U3CGET_PARENTCONTEXTSU3ED__49_TF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext/<get_ParentContexts>d__49
struct  U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InjectContext/<get_ParentContexts>d__49::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectContext Zenject.InjectContext/<get_ParentContexts>d__49::<>2__current
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.InjectContext/<get_ParentContexts>d__49::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectContext Zenject.InjectContext/<get_ParentContexts>d__49::<>4__this
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext/<get_ParentContexts>d__49::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6, ___U3CU3E2__current_1)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6, ___U3CU3E4__this_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_PARENTCONTEXTSU3ED__49_TF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6_H
#ifndef U3CGET_PARENTCONTEXTSANDSELFU3ED__51_T5936E328E865BC460057DEB846AE78FECE86356B_H
#define U3CGET_PARENTCONTEXTSANDSELFU3ED__51_T5936E328E865BC460057DEB846AE78FECE86356B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext/<get_ParentContextsAndSelf>d__51
struct  U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InjectContext/<get_ParentContextsAndSelf>d__51::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.InjectContext Zenject.InjectContext/<get_ParentContextsAndSelf>d__51::<>2__current
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.InjectContext/<get_ParentContextsAndSelf>d__51::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectContext Zenject.InjectContext/<get_ParentContextsAndSelf>d__51::<>4__this
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectContext> Zenject.InjectContext/<get_ParentContextsAndSelf>d__51::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B, ___U3CU3E2__current_1)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B, ___U3CU3E4__this_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_4() { return static_cast<int32_t>(offsetof(U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B, ___U3CU3E7__wrap1_4)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_4() const { return ___U3CU3E7__wrap1_4; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_4() { return &___U3CU3E7__wrap1_4; }
	inline void set_U3CU3E7__wrap1_4(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGET_PARENTCONTEXTSANDSELFU3ED__51_T5936E328E865BC460057DEB846AE78FECE86356B_H
#ifndef INJECTUTIL_T55C782AB1EDC721C1D22DE6E52E1577BCDDB13E7_H
#define INJECTUTIL_T55C782AB1EDC721C1D22DE6E52E1577BCDDB13E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil
struct  InjectUtil_t55C782AB1EDC721C1D22DE6E52E1577BCDDB13E7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTUTIL_T55C782AB1EDC721C1D22DE6E52E1577BCDDB13E7_H
#ifndef U3CU3EC_TB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_H
#define U3CU3EC_TB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil/<>c
struct  U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_StaticFields
{
public:
	// Zenject.InjectUtil/<>c Zenject.InjectUtil/<>c::<>9
	U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D * ___U3CU3E9_0;
	// System.Func`2<System.Object,Zenject.TypeValuePair> Zenject.InjectUtil/<>c::<>9__0_0
	Func_2_t39486D74B56C590C2243B9DBD80C5CFE7948E3DB * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_t39486D74B56C590C2243B9DBD80C5CFE7948E3DB * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_t39486D74B56C590C2243B9DBD80C5CFE7948E3DB ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_t39486D74B56C590C2243B9DBD80C5CFE7948E3DB * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TD716375087C0E416B99073E073BC2CB3C6982E1D_H
#define U3CU3EC__DISPLAYCLASS8_0_TD716375087C0E416B99073E073BC2CB3C6982E1D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectUtil/<>c__DisplayClass8_0
struct  U3CU3Ec__DisplayClass8_0_tD716375087C0E416B99073E073BC2CB3C6982E1D  : public RuntimeObject
{
public:
	// System.Type Zenject.InjectUtil/<>c__DisplayClass8_0::injectedFieldType
	Type_t * ___injectedFieldType_0;

public:
	inline static int32_t get_offset_of_injectedFieldType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tD716375087C0E416B99073E073BC2CB3C6982E1D, ___injectedFieldType_0)); }
	inline Type_t * get_injectedFieldType_0() const { return ___injectedFieldType_0; }
	inline Type_t ** get_address_of_injectedFieldType_0() { return &___injectedFieldType_0; }
	inline void set_injectedFieldType_0(Type_t * value)
	{
		___injectedFieldType_0 = value;
		Il2CppCodeGenWriteBarrier((&___injectedFieldType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TD716375087C0E416B99073E073BC2CB3C6982E1D_H
#ifndef INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#define INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifndef MONOINSTALLERUTIL_TD3CDCDBD575D81610D4A9B18111BC1717D0D0EA5_H
#define MONOINSTALLERUTIL_TD3CDCDBD575D81610D4A9B18111BC1717D0D0EA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstallerUtil
struct  MonoInstallerUtil_tD3CDCDBD575D81610D4A9B18111BC1717D0D0EA5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLERUTIL_TD3CDCDBD575D81610D4A9B18111BC1717D0D0EA5_H
#ifndef U3CU3EC_T0AFD5242D659125E0BF890935FE20FD3257ED9F0_H
#define U3CU3EC_T0AFD5242D659125E0BF890935FE20FD3257ED9F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext/<>c
struct  U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields
{
public:
	// Zenject.SceneContext/<>c Zenject.SceneContext/<>c::<>9
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.SceneManagement.Scene,System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>> Zenject.SceneContext/<>c::<>9__29_0
	Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * ___U3CU3E9__29_0_1;
	// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneContext>> Zenject.SceneContext/<>c::<>9__29_1
	Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF * ___U3CU3E9__29_1_2;
	// System.Func`2<Zenject.SceneContext,Zenject.DiContainer> Zenject.SceneContext/<>c::<>9__29_3
	Func_2_t816D7F752AC8679BB34EFE304A349306C22C5C35 * ___U3CU3E9__29_3_3;
	// System.Func`2<UnityEngine.SceneManagement.Scene,System.Collections.Generic.IEnumerable`1<UnityEngine.GameObject>> Zenject.SceneContext/<>c::<>9__30_0
	Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * ___U3CU3E9__30_0_4;
	// System.Func`2<UnityEngine.GameObject,System.Collections.Generic.IEnumerable`1<Zenject.SceneDecoratorContext>> Zenject.SceneContext/<>c::<>9__30_1
	Func_2_tED73F70B9027353D18292133FC6DB6C7F48EA37C * ___U3CU3E9__30_1_5;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields, ___U3CU3E9__29_0_1)); }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * get_U3CU3E9__29_0_1() const { return ___U3CU3E9__29_0_1; }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 ** get_address_of_U3CU3E9__29_0_1() { return &___U3CU3E9__29_0_1; }
	inline void set_U3CU3E9__29_0_1(Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * value)
	{
		___U3CU3E9__29_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields, ___U3CU3E9__29_1_2)); }
	inline Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF * get_U3CU3E9__29_1_2() const { return ___U3CU3E9__29_1_2; }
	inline Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF ** get_address_of_U3CU3E9__29_1_2() { return &___U3CU3E9__29_1_2; }
	inline void set_U3CU3E9__29_1_2(Func_2_tC515A1EBB1A79A89392E42BD4A197686B1557DFF * value)
	{
		___U3CU3E9__29_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_3_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields, ___U3CU3E9__29_3_3)); }
	inline Func_2_t816D7F752AC8679BB34EFE304A349306C22C5C35 * get_U3CU3E9__29_3_3() const { return ___U3CU3E9__29_3_3; }
	inline Func_2_t816D7F752AC8679BB34EFE304A349306C22C5C35 ** get_address_of_U3CU3E9__29_3_3() { return &___U3CU3E9__29_3_3; }
	inline void set_U3CU3E9__29_3_3(Func_2_t816D7F752AC8679BB34EFE304A349306C22C5C35 * value)
	{
		___U3CU3E9__29_3_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_3_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields, ___U3CU3E9__30_0_4)); }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * get_U3CU3E9__30_0_4() const { return ___U3CU3E9__30_0_4; }
	inline Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 ** get_address_of_U3CU3E9__30_0_4() { return &___U3CU3E9__30_0_4; }
	inline void set_U3CU3E9__30_0_4(Func_2_tFAFA393FCFA2B792ED7E14FB5E6990E11CBBAF46 * value)
	{
		___U3CU3E9__30_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_1_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields, ___U3CU3E9__30_1_5)); }
	inline Func_2_tED73F70B9027353D18292133FC6DB6C7F48EA37C * get_U3CU3E9__30_1_5() const { return ___U3CU3E9__30_1_5; }
	inline Func_2_tED73F70B9027353D18292133FC6DB6C7F48EA37C ** get_address_of_U3CU3E9__30_1_5() { return &___U3CU3E9__30_1_5; }
	inline void set_U3CU3E9__30_1_5(Func_2_tED73F70B9027353D18292133FC6DB6C7F48EA37C * value)
	{
		___U3CU3E9__30_1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_1_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0AFD5242D659125E0BF890935FE20FD3257ED9F0_H
#ifndef U3CU3EC__DISPLAYCLASS29_0_T33E981059E541FB0A518849563350F400EEB2EBC_H
#define U3CU3EC__DISPLAYCLASS29_0_T33E981059E541FB0A518849563350F400EEB2EBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext/<>c__DisplayClass29_0
struct  U3CU3Ec__DisplayClass29_0_t33E981059E541FB0A518849563350F400EEB2EBC  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<System.String> Zenject.SceneContext/<>c__DisplayClass29_0::parentContractNames
	RuntimeObject* ___parentContractNames_0;
	// System.Func`2<System.String,System.Boolean> Zenject.SceneContext/<>c__DisplayClass29_0::<>9__4
	Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * ___U3CU3E9__4_1;

public:
	inline static int32_t get_offset_of_parentContractNames_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t33E981059E541FB0A518849563350F400EEB2EBC, ___parentContractNames_0)); }
	inline RuntimeObject* get_parentContractNames_0() const { return ___parentContractNames_0; }
	inline RuntimeObject** get_address_of_parentContractNames_0() { return &___parentContractNames_0; }
	inline void set_parentContractNames_0(RuntimeObject* value)
	{
		___parentContractNames_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentContractNames_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass29_0_t33E981059E541FB0A518849563350F400EEB2EBC, ___U3CU3E9__4_1)); }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * get_U3CU3E9__4_1() const { return ___U3CU3E9__4_1; }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 ** get_address_of_U3CU3E9__4_1() { return &___U3CU3E9__4_1; }
	inline void set_U3CU3E9__4_1(Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * value)
	{
		___U3CU3E9__4_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS29_0_T33E981059E541FB0A518849563350F400EEB2EBC_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A_H
#define U3CU3EC__DISPLAYCLASS31_0_T01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A  : public RuntimeObject
{
public:
	// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer> Zenject.SceneContext/<>c__DisplayClass31_0::parents
	RuntimeObject* ___parents_0;

public:
	inline static int32_t get_offset_of_parents_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A, ___parents_0)); }
	inline RuntimeObject* get_parents_0() const { return ___parents_0; }
	inline RuntimeObject** get_address_of_parents_0() { return &___parents_0; }
	inline void set_parents_0(RuntimeObject* value)
	{
		___parents_0 = value;
		Il2CppCodeGenWriteBarrier((&___parents_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A_H
#ifndef SCRIPTABLEOBJECTINSTALLERUTIL_TF70646D34A7AC2E4DE8774A737623F8B89E11483_H
#define SCRIPTABLEOBJECTINSTALLERUTIL_TF70646D34A7AC2E4DE8774A737623F8B89E11483_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstallerUtil
struct  ScriptableObjectInstallerUtil_tF70646D34A7AC2E4DE8774A737623F8B89E11483  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLERUTIL_TF70646D34A7AC2E4DE8774A737623F8B89E11483_H
#ifndef STATICCONTEXT_T6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_H
#define STATICCONTEXT_T6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.StaticContext
struct  StaticContext_t6CE037680BBC8B0C5E79BF04FD72E21E7D47B578  : public RuntimeObject
{
public:

public:
};

struct StaticContext_t6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_StaticFields
{
public:
	// Zenject.DiContainer Zenject.StaticContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(StaticContext_t6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_StaticFields, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATICCONTEXT_T6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_H
#ifndef TYPEVALUEPAIR_T4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B_H
#define TYPEVALUEPAIR_T4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeValuePair
struct  TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B  : public RuntimeObject
{
public:
	// System.Type Zenject.TypeValuePair::Type
	Type_t * ___Type_0;
	// System.Object Zenject.TypeValuePair::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEVALUEPAIR_T4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef LOOKUPID_T1FB6A138D9CD34D34E75CE606DCB31620C030DF8_H
#define LOOKUPID_T1FB6A138D9CD34D34E75CE606DCB31620C030DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/LookupId
struct  LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8 
{
public:
	// Zenject.IProvider Zenject.DiContainer/LookupId::Provider
	RuntimeObject* ___Provider_0;
	// Zenject.BindingId Zenject.DiContainer/LookupId::BindingId
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___BindingId_1;

public:
	inline static int32_t get_offset_of_Provider_0() { return static_cast<int32_t>(offsetof(LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8, ___Provider_0)); }
	inline RuntimeObject* get_Provider_0() const { return ___Provider_0; }
	inline RuntimeObject** get_address_of_Provider_0() { return &___Provider_0; }
	inline void set_Provider_0(RuntimeObject* value)
	{
		___Provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___Provider_0), value);
	}

	inline static int32_t get_offset_of_BindingId_1() { return static_cast<int32_t>(offsetof(LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8, ___BindingId_1)); }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * get_BindingId_1() const { return ___BindingId_1; }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 ** get_address_of_BindingId_1() { return &___BindingId_1; }
	inline void set_BindingId_1(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * value)
	{
		___BindingId_1 = value;
		Il2CppCodeGenWriteBarrier((&___BindingId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Zenject.DiContainer/LookupId
struct LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8_marshaled_pinvoke
{
	RuntimeObject* ___Provider_0;
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___BindingId_1;
};
// Native definition for COM marshalling of Zenject.DiContainer/LookupId
struct LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8_marshaled_com
{
	RuntimeObject* ___Provider_0;
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___BindingId_1;
};
#endif // LOOKUPID_T1FB6A138D9CD34D34E75CE606DCB31620C030DF8_H
#ifndef INSTALLER_T2E2802A812A854AEA82893477CE51FADCBA0E05E_H
#define INSTALLER_T2E2802A812A854AEA82893477CE51FADCBA0E05E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer
struct  Installer_t2E2802A812A854AEA82893477CE51FADCBA0E05E  : public InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_T2E2802A812A854AEA82893477CE51FADCBA0E05E_H
#ifndef POOLEXCEEDEDFIXEDSIZEEXCEPTION_TC6A405670C21F3E58156A408AE032B8B53E40145_H
#define POOLEXCEEDEDFIXEDSIZEEXCEPTION_TC6A405670C21F3E58156A408AE032B8B53E40145_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExceededFixedSizeException
struct  PoolExceededFixedSizeException_tC6A405670C21F3E58156A408AE032B8B53E40145  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXCEEDEDFIXEDSIZEEXCEPTION_TC6A405670C21F3E58156A408AE032B8B53E40145_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef PROVIDERLOOKUPRESULT_TC9F048169581A94D7FB97D56AD4A28079C5ECD84_H
#define PROVIDERLOOKUPRESULT_TC9F048169581A94D7FB97D56AD4A28079C5ECD84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/ProviderLookupResult
struct  ProviderLookupResult_tC9F048169581A94D7FB97D56AD4A28079C5ECD84 
{
public:
	// System.Int32 Zenject.DiContainer/ProviderLookupResult::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ProviderLookupResult_tC9F048169581A94D7FB97D56AD4A28079C5ECD84, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERLOOKUPRESULT_TC9F048169581A94D7FB97D56AD4A28079C5ECD84_H
#ifndef INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#define INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t05F31227C87E0653D0F785E74E838313F2158963 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t05F31227C87E0653D0F785E74E838313F2158963, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#ifndef POOLEXPANDMETHODS_T778C4D7478B1DC979FE2B16031F3C6695A2E81D4_H
#define POOLEXPANDMETHODS_T778C4D7478B1DC979FE2B16031F3C6695A2E81D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExpandMethods
struct  PoolExpandMethods_t778C4D7478B1DC979FE2B16031F3C6695A2E81D4 
{
public:
	// System.Int32 Zenject.PoolExpandMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PoolExpandMethods_t778C4D7478B1DC979FE2B16031F3C6695A2E81D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXPANDMETHODS_T778C4D7478B1DC979FE2B16031F3C6695A2E81D4_H
#ifndef BINDTYPES_TB4EB04A4559867A1121813DC1479CCB26BD3E04E_H
#define BINDTYPES_TB4EB04A4559867A1121813DC1479CCB26BD3E04E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectBinding/BindTypes
struct  BindTypes_tB4EB04A4559867A1121813DC1479CCB26BD3E04E 
{
public:
	// System.Int32 Zenject.ZenjectBinding/BindTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindTypes_tB4EB04A4559867A1121813DC1479CCB26BD3E04E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDTYPES_TB4EB04A4559867A1121813DC1479CCB26BD3E04E_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#define SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.ScriptableObject
struct  ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_pinvoke : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
};
// Native definition for COM marshalling of UnityEngine.ScriptableObject
struct ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734_marshaled_com : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
};
#endif // SCRIPTABLEOBJECT_TAB015486CEAB714DA0D5C1BA389B84FB90427734_H
#ifndef INJECTCONTEXT_T1C8120A2BE158ECE706FD7574401F8655F5CE649_H
#define INJECTCONTEXT_T1C8120A2BE158ECE706FD7574401F8655F5CE649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectContext
struct  InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649  : public RuntimeObject
{
public:
	// System.Type Zenject.InjectContext::<ObjectType>k__BackingField
	Type_t * ___U3CObjectTypeU3Ek__BackingField_0;
	// Zenject.InjectContext Zenject.InjectContext::<ParentContext>k__BackingField
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CParentContextU3Ek__BackingField_1;
	// System.Object Zenject.InjectContext::<ObjectInstance>k__BackingField
	RuntimeObject * ___U3CObjectInstanceU3Ek__BackingField_2;
	// System.Object Zenject.InjectContext::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_3;
	// System.Object Zenject.InjectContext::<ConcreteIdentifier>k__BackingField
	RuntimeObject * ___U3CConcreteIdentifierU3Ek__BackingField_4;
	// System.String Zenject.InjectContext::<MemberName>k__BackingField
	String_t* ___U3CMemberNameU3Ek__BackingField_5;
	// System.Type Zenject.InjectContext::<MemberType>k__BackingField
	Type_t * ___U3CMemberTypeU3Ek__BackingField_6;
	// System.Boolean Zenject.InjectContext::<Optional>k__BackingField
	bool ___U3COptionalU3Ek__BackingField_7;
	// Zenject.InjectSources Zenject.InjectContext::<SourceType>k__BackingField
	int32_t ___U3CSourceTypeU3Ek__BackingField_8;
	// System.Object Zenject.InjectContext::<FallBackValue>k__BackingField
	RuntimeObject * ___U3CFallBackValueU3Ek__BackingField_9;
	// Zenject.DiContainer Zenject.InjectContext::<Container>k__BackingField
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CContainerU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CObjectTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CObjectTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CObjectTypeU3Ek__BackingField_0() const { return ___U3CObjectTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CObjectTypeU3Ek__BackingField_0() { return &___U3CObjectTypeU3Ek__BackingField_0; }
	inline void set_U3CObjectTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CObjectTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CParentContextU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CParentContextU3Ek__BackingField_1)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CParentContextU3Ek__BackingField_1() const { return ___U3CParentContextU3Ek__BackingField_1; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CParentContextU3Ek__BackingField_1() { return &___U3CParentContextU3Ek__BackingField_1; }
	inline void set_U3CParentContextU3Ek__BackingField_1(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CParentContextU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentContextU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CObjectInstanceU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CObjectInstanceU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CObjectInstanceU3Ek__BackingField_2() const { return ___U3CObjectInstanceU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CObjectInstanceU3Ek__BackingField_2() { return &___U3CObjectInstanceU3Ek__BackingField_2; }
	inline void set_U3CObjectInstanceU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CObjectInstanceU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectInstanceU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CIdentifierU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_3() const { return ___U3CIdentifierU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_3() { return &___U3CIdentifierU3Ek__BackingField_3; }
	inline void set_U3CIdentifierU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CConcreteIdentifierU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CConcreteIdentifierU3Ek__BackingField_4() const { return ___U3CConcreteIdentifierU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CConcreteIdentifierU3Ek__BackingField_4() { return &___U3CConcreteIdentifierU3Ek__BackingField_4; }
	inline void set_U3CConcreteIdentifierU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CConcreteIdentifierU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConcreteIdentifierU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMemberNameU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CMemberNameU3Ek__BackingField_5)); }
	inline String_t* get_U3CMemberNameU3Ek__BackingField_5() const { return ___U3CMemberNameU3Ek__BackingField_5; }
	inline String_t** get_address_of_U3CMemberNameU3Ek__BackingField_5() { return &___U3CMemberNameU3Ek__BackingField_5; }
	inline void set_U3CMemberNameU3Ek__BackingField_5(String_t* value)
	{
		___U3CMemberNameU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberNameU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CMemberTypeU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CMemberTypeU3Ek__BackingField_6)); }
	inline Type_t * get_U3CMemberTypeU3Ek__BackingField_6() const { return ___U3CMemberTypeU3Ek__BackingField_6; }
	inline Type_t ** get_address_of_U3CMemberTypeU3Ek__BackingField_6() { return &___U3CMemberTypeU3Ek__BackingField_6; }
	inline void set_U3CMemberTypeU3Ek__BackingField_6(Type_t * value)
	{
		___U3CMemberTypeU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberTypeU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3COptionalU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3COptionalU3Ek__BackingField_7)); }
	inline bool get_U3COptionalU3Ek__BackingField_7() const { return ___U3COptionalU3Ek__BackingField_7; }
	inline bool* get_address_of_U3COptionalU3Ek__BackingField_7() { return &___U3COptionalU3Ek__BackingField_7; }
	inline void set_U3COptionalU3Ek__BackingField_7(bool value)
	{
		___U3COptionalU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CSourceTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CSourceTypeU3Ek__BackingField_8)); }
	inline int32_t get_U3CSourceTypeU3Ek__BackingField_8() const { return ___U3CSourceTypeU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CSourceTypeU3Ek__BackingField_8() { return &___U3CSourceTypeU3Ek__BackingField_8; }
	inline void set_U3CSourceTypeU3Ek__BackingField_8(int32_t value)
	{
		___U3CSourceTypeU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CFallBackValueU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CFallBackValueU3Ek__BackingField_9)); }
	inline RuntimeObject * get_U3CFallBackValueU3Ek__BackingField_9() const { return ___U3CFallBackValueU3Ek__BackingField_9; }
	inline RuntimeObject ** get_address_of_U3CFallBackValueU3Ek__BackingField_9() { return &___U3CFallBackValueU3Ek__BackingField_9; }
	inline void set_U3CFallBackValueU3Ek__BackingField_9(RuntimeObject * value)
	{
		___U3CFallBackValueU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFallBackValueU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CContainerU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649, ___U3CContainerU3Ek__BackingField_10)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CContainerU3Ek__BackingField_10() const { return ___U3CContainerU3Ek__BackingField_10; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CContainerU3Ek__BackingField_10() { return &___U3CContainerU3Ek__BackingField_10; }
	inline void set_U3CContainerU3Ek__BackingField_10(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CContainerU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContainerU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTCONTEXT_T1C8120A2BE158ECE706FD7574401F8655F5CE649_H
#ifndef INJECTABLEINFO_TFF42BECAE45D87AABB240ED89EA8D72A4E061DD4_H
#define INJECTABLEINFO_TFF42BECAE45D87AABB240ED89EA8D72A4E061DD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectableInfo
struct  InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4  : public RuntimeObject
{
public:
	// System.Boolean Zenject.InjectableInfo::Optional
	bool ___Optional_0;
	// System.Object Zenject.InjectableInfo::Identifier
	RuntimeObject * ___Identifier_1;
	// Zenject.InjectSources Zenject.InjectableInfo::SourceType
	int32_t ___SourceType_2;
	// System.String Zenject.InjectableInfo::MemberName
	String_t* ___MemberName_3;
	// System.Type Zenject.InjectableInfo::MemberType
	Type_t * ___MemberType_4;
	// System.Type Zenject.InjectableInfo::ObjectType
	Type_t * ___ObjectType_5;
	// System.Action`2<System.Object,System.Object> Zenject.InjectableInfo::Setter
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___Setter_6;
	// System.Object Zenject.InjectableInfo::DefaultValue
	RuntimeObject * ___DefaultValue_7;

public:
	inline static int32_t get_offset_of_Optional_0() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___Optional_0)); }
	inline bool get_Optional_0() const { return ___Optional_0; }
	inline bool* get_address_of_Optional_0() { return &___Optional_0; }
	inline void set_Optional_0(bool value)
	{
		___Optional_0 = value;
	}

	inline static int32_t get_offset_of_Identifier_1() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___Identifier_1)); }
	inline RuntimeObject * get_Identifier_1() const { return ___Identifier_1; }
	inline RuntimeObject ** get_address_of_Identifier_1() { return &___Identifier_1; }
	inline void set_Identifier_1(RuntimeObject * value)
	{
		___Identifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___Identifier_1), value);
	}

	inline static int32_t get_offset_of_SourceType_2() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___SourceType_2)); }
	inline int32_t get_SourceType_2() const { return ___SourceType_2; }
	inline int32_t* get_address_of_SourceType_2() { return &___SourceType_2; }
	inline void set_SourceType_2(int32_t value)
	{
		___SourceType_2 = value;
	}

	inline static int32_t get_offset_of_MemberName_3() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___MemberName_3)); }
	inline String_t* get_MemberName_3() const { return ___MemberName_3; }
	inline String_t** get_address_of_MemberName_3() { return &___MemberName_3; }
	inline void set_MemberName_3(String_t* value)
	{
		___MemberName_3 = value;
		Il2CppCodeGenWriteBarrier((&___MemberName_3), value);
	}

	inline static int32_t get_offset_of_MemberType_4() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___MemberType_4)); }
	inline Type_t * get_MemberType_4() const { return ___MemberType_4; }
	inline Type_t ** get_address_of_MemberType_4() { return &___MemberType_4; }
	inline void set_MemberType_4(Type_t * value)
	{
		___MemberType_4 = value;
		Il2CppCodeGenWriteBarrier((&___MemberType_4), value);
	}

	inline static int32_t get_offset_of_ObjectType_5() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___ObjectType_5)); }
	inline Type_t * get_ObjectType_5() const { return ___ObjectType_5; }
	inline Type_t ** get_address_of_ObjectType_5() { return &___ObjectType_5; }
	inline void set_ObjectType_5(Type_t * value)
	{
		___ObjectType_5 = value;
		Il2CppCodeGenWriteBarrier((&___ObjectType_5), value);
	}

	inline static int32_t get_offset_of_Setter_6() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___Setter_6)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_Setter_6() const { return ___Setter_6; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_Setter_6() { return &___Setter_6; }
	inline void set_Setter_6(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___Setter_6 = value;
		Il2CppCodeGenWriteBarrier((&___Setter_6), value);
	}

	inline static int32_t get_offset_of_DefaultValue_7() { return static_cast<int32_t>(offsetof(InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4, ___DefaultValue_7)); }
	inline RuntimeObject * get_DefaultValue_7() const { return ___DefaultValue_7; }
	inline RuntimeObject ** get_address_of_DefaultValue_7() { return &___DefaultValue_7; }
	inline void set_DefaultValue_7(RuntimeObject * value)
	{
		___DefaultValue_7 = value;
		Il2CppCodeGenWriteBarrier((&___DefaultValue_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTABLEINFO_TFF42BECAE45D87AABB240ED89EA8D72A4E061DD4_H
#ifndef MEMORYPOOLSETTINGS_TED0CF8C73689E0D56DC6788EAFB302160BB63511_H
#define MEMORYPOOLSETTINGS_TED0CF8C73689E0D56DC6788EAFB302160BB63511_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolSettings
struct  MemoryPoolSettings_tED0CF8C73689E0D56DC6788EAFB302160BB63511  : public RuntimeObject
{
public:
	// System.Int32 Zenject.MemoryPoolSettings::InitialSize
	int32_t ___InitialSize_0;
	// Zenject.PoolExpandMethods Zenject.MemoryPoolSettings::ExpandMethod
	int32_t ___ExpandMethod_1;

public:
	inline static int32_t get_offset_of_InitialSize_0() { return static_cast<int32_t>(offsetof(MemoryPoolSettings_tED0CF8C73689E0D56DC6788EAFB302160BB63511, ___InitialSize_0)); }
	inline int32_t get_InitialSize_0() const { return ___InitialSize_0; }
	inline int32_t* get_address_of_InitialSize_0() { return &___InitialSize_0; }
	inline void set_InitialSize_0(int32_t value)
	{
		___InitialSize_0 = value;
	}

	inline static int32_t get_offset_of_ExpandMethod_1() { return static_cast<int32_t>(offsetof(MemoryPoolSettings_tED0CF8C73689E0D56DC6788EAFB302160BB63511, ___ExpandMethod_1)); }
	inline int32_t get_ExpandMethod_1() const { return ___ExpandMethod_1; }
	inline int32_t* get_address_of_ExpandMethod_1() { return &___ExpandMethod_1; }
	inline void set_ExpandMethod_1(int32_t value)
	{
		___ExpandMethod_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLSETTINGS_TED0CF8C73689E0D56DC6788EAFB302160BB63511_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef BINDINGCONDITION_TC316B7CC19C4C70BF281CED3512B1E88CE20BCA3_H
#define BINDINGCONDITION_TC316B7CC19C4C70BF281CED3512B1E88CE20BCA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingCondition
struct  BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGCONDITION_TC316B7CC19C4C70BF281CED3512B1E88CE20BCA3_H
#ifndef SCRIPTABLEOBJECTINSTALLERBASE_TA8EF4D1EDAF141574B49C2AC416CA782472D89C9_H
#define SCRIPTABLEOBJECTINSTALLERBASE_TA8EF4D1EDAF141574B49C2AC416CA782472D89C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstallerBase
struct  ScriptableObjectInstallerBase_tA8EF4D1EDAF141574B49C2AC416CA782472D89C9  : public ScriptableObject_tAB015486CEAB714DA0D5C1BA389B84FB90427734
{
public:
	// Zenject.DiContainer Zenject.ScriptableObjectInstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(ScriptableObjectInstallerBase_tA8EF4D1EDAF141574B49C2AC416CA782472D89C9, ____container_4)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_4() const { return ____container_4; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLERBASE_TA8EF4D1EDAF141574B49C2AC416CA782472D89C9_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef SCRIPTABLEOBJECTINSTALLER_TC387A44505BCD83E13820829F1C493DCB41E49A5_H
#define SCRIPTABLEOBJECTINSTALLER_TC387A44505BCD83E13820829F1C493DCB41E49A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectInstaller
struct  ScriptableObjectInstaller_tC387A44505BCD83E13820829F1C493DCB41E49A5  : public ScriptableObjectInstallerBase_tA8EF4D1EDAF141574B49C2AC416CA782472D89C9
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTINSTALLER_TC387A44505BCD83E13820829F1C493DCB41E49A5_H
#ifndef CONTEXT_T3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7_H
#define CONTEXT_T3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Context
struct  Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installers
	List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * ____installers_4;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installerPrefabs
	List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * ____installerPrefabs_5;
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.Context::_scriptableObjectInstallers
	List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * ____scriptableObjectInstallers_6;
	// System.Collections.Generic.List`1<Zenject.InstallerBase> Zenject.Context::_normalInstallers
	List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 * ____normalInstallers_7;
	// System.Collections.Generic.List`1<System.Type> Zenject.Context::_normalInstallerTypes
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____normalInstallerTypes_8;

public:
	inline static int32_t get_offset_of__installers_4() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____installers_4)); }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * get__installers_4() const { return ____installers_4; }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 ** get_address_of__installers_4() { return &____installers_4; }
	inline void set__installers_4(List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * value)
	{
		____installers_4 = value;
		Il2CppCodeGenWriteBarrier((&____installers_4), value);
	}

	inline static int32_t get_offset_of__installerPrefabs_5() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____installerPrefabs_5)); }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * get__installerPrefabs_5() const { return ____installerPrefabs_5; }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 ** get_address_of__installerPrefabs_5() { return &____installerPrefabs_5; }
	inline void set__installerPrefabs_5(List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * value)
	{
		____installerPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&____installerPrefabs_5), value);
	}

	inline static int32_t get_offset_of__scriptableObjectInstallers_6() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____scriptableObjectInstallers_6)); }
	inline List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * get__scriptableObjectInstallers_6() const { return ____scriptableObjectInstallers_6; }
	inline List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 ** get_address_of__scriptableObjectInstallers_6() { return &____scriptableObjectInstallers_6; }
	inline void set__scriptableObjectInstallers_6(List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * value)
	{
		____scriptableObjectInstallers_6 = value;
		Il2CppCodeGenWriteBarrier((&____scriptableObjectInstallers_6), value);
	}

	inline static int32_t get_offset_of__normalInstallers_7() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____normalInstallers_7)); }
	inline List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 * get__normalInstallers_7() const { return ____normalInstallers_7; }
	inline List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 ** get_address_of__normalInstallers_7() { return &____normalInstallers_7; }
	inline void set__normalInstallers_7(List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 * value)
	{
		____normalInstallers_7 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallers_7), value);
	}

	inline static int32_t get_offset_of__normalInstallerTypes_8() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____normalInstallerTypes_8)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__normalInstallerTypes_8() const { return ____normalInstallerTypes_8; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__normalInstallerTypes_8() { return &____normalInstallerTypes_8; }
	inline void set__normalInstallerTypes_8(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____normalInstallerTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallerTypes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7_H
#ifndef MONOINSTALLERBASE_TC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF_H
#define MONOINSTALLERBASE_TC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstallerBase
struct  MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Zenject.DiContainer Zenject.MonoInstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_4;

public:
	inline static int32_t get_offset_of__container_4() { return static_cast<int32_t>(offsetof(MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF, ____container_4)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_4() const { return ____container_4; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_4() { return &____container_4; }
	inline void set__container_4(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_4 = value;
		Il2CppCodeGenWriteBarrier((&____container_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLERBASE_TC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF_H
#ifndef ZENJECTBINDING_T6F8DF0683477500C50AFA2FD9B0B85A4DC143F79_H
#define ZENJECTBINDING_T6F8DF0683477500C50AFA2FD9B0B85A4DC143F79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ZenjectBinding
struct  ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Component[] Zenject.ZenjectBinding::_components
	ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* ____components_4;
	// System.String Zenject.ZenjectBinding::_identifier
	String_t* ____identifier_5;
	// Zenject.Context Zenject.ZenjectBinding::_context
	Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 * ____context_6;
	// Zenject.ZenjectBinding/BindTypes Zenject.ZenjectBinding::_bindType
	int32_t ____bindType_7;

public:
	inline static int32_t get_offset_of__components_4() { return static_cast<int32_t>(offsetof(ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79, ____components_4)); }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* get__components_4() const { return ____components_4; }
	inline ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155** get_address_of__components_4() { return &____components_4; }
	inline void set__components_4(ComponentU5BU5D_t7BE50AFB6301C06D990819B3D8F35CA326CDD155* value)
	{
		____components_4 = value;
		Il2CppCodeGenWriteBarrier((&____components_4), value);
	}

	inline static int32_t get_offset_of__identifier_5() { return static_cast<int32_t>(offsetof(ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79, ____identifier_5)); }
	inline String_t* get__identifier_5() const { return ____identifier_5; }
	inline String_t** get_address_of__identifier_5() { return &____identifier_5; }
	inline void set__identifier_5(String_t* value)
	{
		____identifier_5 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_5), value);
	}

	inline static int32_t get_offset_of__context_6() { return static_cast<int32_t>(offsetof(ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79, ____context_6)); }
	inline Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 * get__context_6() const { return ____context_6; }
	inline Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 ** get_address_of__context_6() { return &____context_6; }
	inline void set__context_6(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 * value)
	{
		____context_6 = value;
		Il2CppCodeGenWriteBarrier((&____context_6), value);
	}

	inline static int32_t get_offset_of__bindType_7() { return static_cast<int32_t>(offsetof(ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79, ____bindType_7)); }
	inline int32_t get__bindType_7() const { return ____bindType_7; }
	inline int32_t* get_address_of__bindType_7() { return &____bindType_7; }
	inline void set__bindType_7(int32_t value)
	{
		____bindType_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZENJECTBINDING_T6F8DF0683477500C50AFA2FD9B0B85A4DC143F79_H
#ifndef MONOINSTALLER_T5D8FF9EC635C3C146F50E3490F8B81803B1688EC_H
#define MONOINSTALLER_T5D8FF9EC635C3C146F50E3490F8B81803B1688EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MonoInstaller
struct  MonoInstaller_t5D8FF9EC635C3C146F50E3490F8B81803B1688EC  : public MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOINSTALLER_T5D8FF9EC635C3C146F50E3490F8B81803B1688EC_H
#ifndef PROJECTCONTEXT_T9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_H
#define PROJECTCONTEXT_T9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProjectContext
struct  ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A  : public Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7
{
public:
	// Zenject.DiContainer Zenject.ProjectContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_12;
	// System.Collections.Generic.List`1<System.Object> Zenject.ProjectContext::_dependencyRoots
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____dependencyRoots_13;

public:
	inline static int32_t get_offset_of__container_12() { return static_cast<int32_t>(offsetof(ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A, ____container_12)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_12() const { return ____container_12; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_12() { return &____container_12; }
	inline void set__container_12(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_12 = value;
		Il2CppCodeGenWriteBarrier((&____container_12), value);
	}

	inline static int32_t get_offset_of__dependencyRoots_13() { return static_cast<int32_t>(offsetof(ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A, ____dependencyRoots_13)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__dependencyRoots_13() const { return ____dependencyRoots_13; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__dependencyRoots_13() { return &____dependencyRoots_13; }
	inline void set__dependencyRoots_13(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____dependencyRoots_13 = value;
		Il2CppCodeGenWriteBarrier((&____dependencyRoots_13), value);
	}
};

struct ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_StaticFields
{
public:
	// Zenject.ProjectContext Zenject.ProjectContext::_instance
	ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A * ____instance_11;

public:
	inline static int32_t get_offset_of__instance_11() { return static_cast<int32_t>(offsetof(ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_StaticFields, ____instance_11)); }
	inline ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A * get__instance_11() const { return ____instance_11; }
	inline ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A ** get_address_of__instance_11() { return &____instance_11; }
	inline void set__instance_11(ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A * value)
	{
		____instance_11 = value;
		Il2CppCodeGenWriteBarrier((&____instance_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTCONTEXT_T9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_H
#ifndef RUNNABLECONTEXT_TCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_H
#define RUNNABLECONTEXT_TCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.RunnableContext
struct  RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2  : public Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7
{
public:
	// System.Boolean Zenject.RunnableContext::_autoRun
	bool ____autoRun_9;
	// System.Boolean Zenject.RunnableContext::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__autoRun_9() { return static_cast<int32_t>(offsetof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2, ____autoRun_9)); }
	inline bool get__autoRun_9() const { return ____autoRun_9; }
	inline bool* get_address_of__autoRun_9() { return &____autoRun_9; }
	inline void set__autoRun_9(bool value)
	{
		____autoRun_9 = value;
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2, ___U3CInitializedU3Ek__BackingField_11)); }
	inline bool get_U3CInitializedU3Ek__BackingField_11() const { return ___U3CInitializedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_11() { return &___U3CInitializedU3Ek__BackingField_11; }
	inline void set_U3CInitializedU3Ek__BackingField_11(bool value)
	{
		___U3CInitializedU3Ek__BackingField_11 = value;
	}
};

struct RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_StaticFields
{
public:
	// System.Boolean Zenject.RunnableContext::_staticAutoRun
	bool ____staticAutoRun_10;

public:
	inline static int32_t get_offset_of__staticAutoRun_10() { return static_cast<int32_t>(offsetof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_StaticFields, ____staticAutoRun_10)); }
	inline bool get__staticAutoRun_10() const { return ____staticAutoRun_10; }
	inline bool* get_address_of__staticAutoRun_10() { return &____staticAutoRun_10; }
	inline void set__staticAutoRun_10(bool value)
	{
		____staticAutoRun_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNABLECONTEXT_TCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_H
#ifndef SCENEDECORATORCONTEXT_T2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E_H
#define SCENEDECORATORCONTEXT_T2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneDecoratorContext
struct  SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E  : public Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7
{
public:
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.SceneDecoratorContext::_lateInstallers
	List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * ____lateInstallers_9;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.SceneDecoratorContext::_lateInstallerPrefabs
	List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * ____lateInstallerPrefabs_10;
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.SceneDecoratorContext::_lateScriptableObjectInstallers
	List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * ____lateScriptableObjectInstallers_11;
	// System.String Zenject.SceneDecoratorContext::_decoratedContractName
	String_t* ____decoratedContractName_12;
	// Zenject.DiContainer Zenject.SceneDecoratorContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_13;
	// System.Collections.Generic.List`1<UnityEngine.MonoBehaviour> Zenject.SceneDecoratorContext::_injectableMonoBehaviours
	List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 * ____injectableMonoBehaviours_14;

public:
	inline static int32_t get_offset_of__lateInstallers_9() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E, ____lateInstallers_9)); }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * get__lateInstallers_9() const { return ____lateInstallers_9; }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 ** get_address_of__lateInstallers_9() { return &____lateInstallers_9; }
	inline void set__lateInstallers_9(List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * value)
	{
		____lateInstallers_9 = value;
		Il2CppCodeGenWriteBarrier((&____lateInstallers_9), value);
	}

	inline static int32_t get_offset_of__lateInstallerPrefabs_10() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E, ____lateInstallerPrefabs_10)); }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * get__lateInstallerPrefabs_10() const { return ____lateInstallerPrefabs_10; }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 ** get_address_of__lateInstallerPrefabs_10() { return &____lateInstallerPrefabs_10; }
	inline void set__lateInstallerPrefabs_10(List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * value)
	{
		____lateInstallerPrefabs_10 = value;
		Il2CppCodeGenWriteBarrier((&____lateInstallerPrefabs_10), value);
	}

	inline static int32_t get_offset_of__lateScriptableObjectInstallers_11() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E, ____lateScriptableObjectInstallers_11)); }
	inline List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * get__lateScriptableObjectInstallers_11() const { return ____lateScriptableObjectInstallers_11; }
	inline List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 ** get_address_of__lateScriptableObjectInstallers_11() { return &____lateScriptableObjectInstallers_11; }
	inline void set__lateScriptableObjectInstallers_11(List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * value)
	{
		____lateScriptableObjectInstallers_11 = value;
		Il2CppCodeGenWriteBarrier((&____lateScriptableObjectInstallers_11), value);
	}

	inline static int32_t get_offset_of__decoratedContractName_12() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E, ____decoratedContractName_12)); }
	inline String_t* get__decoratedContractName_12() const { return ____decoratedContractName_12; }
	inline String_t** get_address_of__decoratedContractName_12() { return &____decoratedContractName_12; }
	inline void set__decoratedContractName_12(String_t* value)
	{
		____decoratedContractName_12 = value;
		Il2CppCodeGenWriteBarrier((&____decoratedContractName_12), value);
	}

	inline static int32_t get_offset_of__container_13() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E, ____container_13)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_13() const { return ____container_13; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_13() { return &____container_13; }
	inline void set__container_13(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_13 = value;
		Il2CppCodeGenWriteBarrier((&____container_13), value);
	}

	inline static int32_t get_offset_of__injectableMonoBehaviours_14() { return static_cast<int32_t>(offsetof(SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E, ____injectableMonoBehaviours_14)); }
	inline List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 * get__injectableMonoBehaviours_14() const { return ____injectableMonoBehaviours_14; }
	inline List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 ** get_address_of__injectableMonoBehaviours_14() { return &____injectableMonoBehaviours_14; }
	inline void set__injectableMonoBehaviours_14(List_1_tA826461A2C9E99A4825BFEC4BE4DBF2F1A142DA7 * value)
	{
		____injectableMonoBehaviours_14 = value;
		Il2CppCodeGenWriteBarrier((&____injectableMonoBehaviours_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENEDECORATORCONTEXT_T2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E_H
#ifndef GAMEOBJECTCONTEXT_T83A87C55F6FA3BFBE6D04A3261653BE69685FD94_H
#define GAMEOBJECTCONTEXT_T83A87C55F6FA3BFBE6D04A3261653BE69685FD94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectContext
struct  GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94  : public RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2
{
public:
	// System.Collections.Generic.List`1<System.Object> Zenject.GameObjectContext::_dependencyRoots
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____dependencyRoots_12;
	// Zenject.MonoKernel Zenject.GameObjectContext::_kernel
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF * ____kernel_13;
	// Zenject.DiContainer Zenject.GameObjectContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_14;

public:
	inline static int32_t get_offset_of__dependencyRoots_12() { return static_cast<int32_t>(offsetof(GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94, ____dependencyRoots_12)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__dependencyRoots_12() const { return ____dependencyRoots_12; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__dependencyRoots_12() { return &____dependencyRoots_12; }
	inline void set__dependencyRoots_12(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____dependencyRoots_12 = value;
		Il2CppCodeGenWriteBarrier((&____dependencyRoots_12), value);
	}

	inline static int32_t get_offset_of__kernel_13() { return static_cast<int32_t>(offsetof(GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94, ____kernel_13)); }
	inline MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF * get__kernel_13() const { return ____kernel_13; }
	inline MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF ** get_address_of__kernel_13() { return &____kernel_13; }
	inline void set__kernel_13(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF * value)
	{
		____kernel_13 = value;
		Il2CppCodeGenWriteBarrier((&____kernel_13), value);
	}

	inline static int32_t get_offset_of__container_14() { return static_cast<int32_t>(offsetof(GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94, ____container_14)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_14() const { return ____container_14; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_14() { return &____container_14; }
	inline void set__container_14(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_14 = value;
		Il2CppCodeGenWriteBarrier((&____container_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCONTEXT_T83A87C55F6FA3BFBE6D04A3261653BE69685FD94_H
#ifndef SCENECONTEXT_T3193D2C9AC4379B4497C947D7F840CEE0DBE341F_H
#define SCENECONTEXT_T3193D2C9AC4379B4497C947D7F840CEE0DBE341F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SceneContext
struct  SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F  : public RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2
{
public:
	// System.Boolean Zenject.SceneContext::_parentNewObjectsUnderRoot
	bool ____parentNewObjectsUnderRoot_15;
	// System.Collections.Generic.List`1<System.String> Zenject.SceneContext::_contractNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____contractNames_16;
	// System.String Zenject.SceneContext::_parentContractName
	String_t* ____parentContractName_17;
	// System.Collections.Generic.List`1<System.String> Zenject.SceneContext::_parentContractNames
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ____parentContractNames_18;
	// Zenject.DiContainer Zenject.SceneContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_19;
	// System.Collections.Generic.List`1<System.Object> Zenject.SceneContext::_dependencyRoots
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____dependencyRoots_20;
	// System.Collections.Generic.List`1<Zenject.SceneDecoratorContext> Zenject.SceneContext::_decoratorContexts
	List_1_t965894C1D41921F28AA28474913EA8675F5FB4E7 * ____decoratorContexts_21;
	// System.Boolean Zenject.SceneContext::_hasInstalled
	bool ____hasInstalled_22;
	// System.Boolean Zenject.SceneContext::_hasResolved
	bool ____hasResolved_23;

public:
	inline static int32_t get_offset_of__parentNewObjectsUnderRoot_15() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____parentNewObjectsUnderRoot_15)); }
	inline bool get__parentNewObjectsUnderRoot_15() const { return ____parentNewObjectsUnderRoot_15; }
	inline bool* get_address_of__parentNewObjectsUnderRoot_15() { return &____parentNewObjectsUnderRoot_15; }
	inline void set__parentNewObjectsUnderRoot_15(bool value)
	{
		____parentNewObjectsUnderRoot_15 = value;
	}

	inline static int32_t get_offset_of__contractNames_16() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____contractNames_16)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__contractNames_16() const { return ____contractNames_16; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__contractNames_16() { return &____contractNames_16; }
	inline void set__contractNames_16(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____contractNames_16 = value;
		Il2CppCodeGenWriteBarrier((&____contractNames_16), value);
	}

	inline static int32_t get_offset_of__parentContractName_17() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____parentContractName_17)); }
	inline String_t* get__parentContractName_17() const { return ____parentContractName_17; }
	inline String_t** get_address_of__parentContractName_17() { return &____parentContractName_17; }
	inline void set__parentContractName_17(String_t* value)
	{
		____parentContractName_17 = value;
		Il2CppCodeGenWriteBarrier((&____parentContractName_17), value);
	}

	inline static int32_t get_offset_of__parentContractNames_18() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____parentContractNames_18)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get__parentContractNames_18() const { return ____parentContractNames_18; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of__parentContractNames_18() { return &____parentContractNames_18; }
	inline void set__parentContractNames_18(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		____parentContractNames_18 = value;
		Il2CppCodeGenWriteBarrier((&____parentContractNames_18), value);
	}

	inline static int32_t get_offset_of__container_19() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____container_19)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_19() const { return ____container_19; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_19() { return &____container_19; }
	inline void set__container_19(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_19 = value;
		Il2CppCodeGenWriteBarrier((&____container_19), value);
	}

	inline static int32_t get_offset_of__dependencyRoots_20() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____dependencyRoots_20)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__dependencyRoots_20() const { return ____dependencyRoots_20; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__dependencyRoots_20() { return &____dependencyRoots_20; }
	inline void set__dependencyRoots_20(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____dependencyRoots_20 = value;
		Il2CppCodeGenWriteBarrier((&____dependencyRoots_20), value);
	}

	inline static int32_t get_offset_of__decoratorContexts_21() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____decoratorContexts_21)); }
	inline List_1_t965894C1D41921F28AA28474913EA8675F5FB4E7 * get__decoratorContexts_21() const { return ____decoratorContexts_21; }
	inline List_1_t965894C1D41921F28AA28474913EA8675F5FB4E7 ** get_address_of__decoratorContexts_21() { return &____decoratorContexts_21; }
	inline void set__decoratorContexts_21(List_1_t965894C1D41921F28AA28474913EA8675F5FB4E7 * value)
	{
		____decoratorContexts_21 = value;
		Il2CppCodeGenWriteBarrier((&____decoratorContexts_21), value);
	}

	inline static int32_t get_offset_of__hasInstalled_22() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____hasInstalled_22)); }
	inline bool get__hasInstalled_22() const { return ____hasInstalled_22; }
	inline bool* get_address_of__hasInstalled_22() { return &____hasInstalled_22; }
	inline void set__hasInstalled_22(bool value)
	{
		____hasInstalled_22 = value;
	}

	inline static int32_t get_offset_of__hasResolved_23() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F, ____hasResolved_23)); }
	inline bool get__hasResolved_23() const { return ____hasResolved_23; }
	inline bool* get_address_of__hasResolved_23() { return &____hasResolved_23; }
	inline void set__hasResolved_23(bool value)
	{
		____hasResolved_23 = value;
	}
};

struct SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.SceneContext::ExtraBindingsInstallMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ___ExtraBindingsInstallMethod_12;
	// System.Action`1<Zenject.DiContainer> Zenject.SceneContext::ExtraBindingsLateInstallMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ___ExtraBindingsLateInstallMethod_13;
	// System.Collections.Generic.IEnumerable`1<Zenject.DiContainer> Zenject.SceneContext::ParentContainers
	RuntimeObject* ___ParentContainers_14;

public:
	inline static int32_t get_offset_of_ExtraBindingsInstallMethod_12() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields, ___ExtraBindingsInstallMethod_12)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get_ExtraBindingsInstallMethod_12() const { return ___ExtraBindingsInstallMethod_12; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of_ExtraBindingsInstallMethod_12() { return &___ExtraBindingsInstallMethod_12; }
	inline void set_ExtraBindingsInstallMethod_12(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		___ExtraBindingsInstallMethod_12 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraBindingsInstallMethod_12), value);
	}

	inline static int32_t get_offset_of_ExtraBindingsLateInstallMethod_13() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields, ___ExtraBindingsLateInstallMethod_13)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get_ExtraBindingsLateInstallMethod_13() const { return ___ExtraBindingsLateInstallMethod_13; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of_ExtraBindingsLateInstallMethod_13() { return &___ExtraBindingsLateInstallMethod_13; }
	inline void set_ExtraBindingsLateInstallMethod_13(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		___ExtraBindingsLateInstallMethod_13 = value;
		Il2CppCodeGenWriteBarrier((&___ExtraBindingsLateInstallMethod_13), value);
	}

	inline static int32_t get_offset_of_ParentContainers_14() { return static_cast<int32_t>(offsetof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields, ___ParentContainers_14)); }
	inline RuntimeObject* get_ParentContainers_14() const { return ___ParentContainers_14; }
	inline RuntimeObject** get_address_of_ParentContainers_14() { return &___ParentContainers_14; }
	inline void set_ParentContainers_14(RuntimeObject* value)
	{
		___ParentContainers_14 = value;
		Il2CppCodeGenWriteBarrier((&___ParentContainers_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCENECONTEXT_T3193D2C9AC4379B4497C947D7F840CEE0DBE341F_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6900 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6901 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6902 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6903 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6903[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6904 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6904[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6905 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6906 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6907 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6908 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6909 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6910 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6911 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6911[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6912 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6913 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6914 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6915 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6916 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6917 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6918 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6919 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6920 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6921 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6922 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6923 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6924 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6925 = { sizeof (PoolExceededFixedSizeException_tC6A405670C21F3E58156A408AE032B8B53E40145), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6926 = { sizeof (MemoryPoolSettings_tED0CF8C73689E0D56DC6788EAFB302160BB63511), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6926[2] = 
{
	MemoryPoolSettings_tED0CF8C73689E0D56DC6788EAFB302160BB63511::get_offset_of_InitialSize_0(),
	MemoryPoolSettings_tED0CF8C73689E0D56DC6788EAFB302160BB63511::get_offset_of_ExpandMethod_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6927 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6927[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6928 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6929 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6930 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6931 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6932 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6933 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6934 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6934[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6935 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6935[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6936 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6936[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6937 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6937[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6938 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6938[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6939 = { sizeof (InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6939[8] = 
{
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_Optional_0(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_Identifier_1(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_SourceType_2(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_MemberName_3(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_MemberType_4(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_ObjectType_5(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_Setter_6(),
	InjectableInfo_tFF42BECAE45D87AABB240ED89EA8D72A4E061DD4::get_offset_of_DefaultValue_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6940 = { sizeof (InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6940[11] = 
{
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CObjectTypeU3Ek__BackingField_0(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CParentContextU3Ek__BackingField_1(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CObjectInstanceU3Ek__BackingField_2(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CIdentifierU3Ek__BackingField_3(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_4(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CMemberNameU3Ek__BackingField_5(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CMemberTypeU3Ek__BackingField_6(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3COptionalU3Ek__BackingField_7(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CSourceTypeU3Ek__BackingField_8(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CFallBackValueU3Ek__BackingField_9(),
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649::get_offset_of_U3CContainerU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6941 = { sizeof (U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6941[5] = 
{
	U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6::get_offset_of_U3CU3E4__this_3(),
	U3Cget_ParentContextsU3Ed__49_tF2C54A86AF974D97D55A8D9C3E0678EB7A9FE7A6::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6942 = { sizeof (U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6942[5] = 
{
	U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B::get_offset_of_U3CU3E1__state_0(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B::get_offset_of_U3CU3E2__current_1(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B::get_offset_of_U3CU3E4__this_3(),
	U3Cget_ParentContextsAndSelfU3Ed__51_t5936E328E865BC460057DEB846AE78FECE86356B::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6943 = { sizeof (U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6943[5] = 
{
	U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC::get_offset_of_U3CU3E1__state_0(),
	U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC::get_offset_of_U3CU3E2__current_1(),
	U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC::get_offset_of_U3CU3El__initialThreadId_2(),
	U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC::get_offset_of_U3CU3E4__this_3(),
	U3Cget_AllObjectTypesU3Ed__53_t3943171CEF22BDA21D99487A6FE7A7CD39B8D8FC::get_offset_of_U3CU3E7__wrap1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6944 = { sizeof (TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6944[2] = 
{
	TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B::get_offset_of_Type_0(),
	TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B::get_offset_of_Value_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6945 = { sizeof (InjectUtil_t55C782AB1EDC721C1D22DE6E52E1577BCDDB13E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6946 = { sizeof (U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D), -1, sizeof(U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6946[2] = 
{
	U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tB4B06BDEFC78D7552DC10E5BAFB8EEF27B40287D_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6947 = { sizeof (U3CU3Ec__DisplayClass8_0_tD716375087C0E416B99073E073BC2CB3C6982E1D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6947[1] = 
{
	U3CU3Ec__DisplayClass8_0_tD716375087C0E416B99073E073BC2CB3C6982E1D::get_offset_of_injectedFieldType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6948 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6949 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6949[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6950 = { sizeof (Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6950[5] = 
{
	Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7::get_offset_of__installers_4(),
	Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7::get_offset_of__installerPrefabs_5(),
	Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7::get_offset_of__scriptableObjectInstallers_6(),
	Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7::get_offset_of__normalInstallers_7(),
	Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7::get_offset_of__normalInstallerTypes_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6951 = { sizeof (U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735), -1, sizeof(U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6951[2] = 
{
	U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t5F7A3E096183BC2033F0FB24FF078D95F21F8735_StaticFields::get_offset_of_U3CU3E9__16_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6952 = { sizeof (GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6952[3] = 
{
	GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94::get_offset_of__dependencyRoots_12(),
	GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94::get_offset_of__kernel_13(),
	GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94::get_offset_of__container_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6953 = { sizeof (ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A), -1, sizeof(ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6953[5] = 
{
	0,
	0,
	ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A_StaticFields::get_offset_of__instance_11(),
	ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A::get_offset_of__container_12(),
	ProjectContext_t9F053C699DA41DBF71E4F3CAEA739E9C3B461E4A::get_offset_of__dependencyRoots_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6954 = { sizeof (RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2), -1, sizeof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6954[3] = 
{
	RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2::get_offset_of__autoRun_9(),
	RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_StaticFields::get_offset_of__staticAutoRun_10(),
	RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2::get_offset_of_U3CInitializedU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6955 = { sizeof (SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F), -1, sizeof(SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6955[12] = 
{
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields::get_offset_of_ExtraBindingsInstallMethod_12(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields::get_offset_of_ExtraBindingsLateInstallMethod_13(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F_StaticFields::get_offset_of_ParentContainers_14(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__parentNewObjectsUnderRoot_15(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__contractNames_16(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__parentContractName_17(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__parentContractNames_18(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__container_19(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__dependencyRoots_20(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__decoratorContexts_21(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__hasInstalled_22(),
	SceneContext_t3193D2C9AC4379B4497C947D7F840CEE0DBE341F::get_offset_of__hasResolved_23(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6956 = { sizeof (U3CU3Ec__DisplayClass29_0_t33E981059E541FB0A518849563350F400EEB2EBC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6956[2] = 
{
	U3CU3Ec__DisplayClass29_0_t33E981059E541FB0A518849563350F400EEB2EBC::get_offset_of_parentContractNames_0(),
	U3CU3Ec__DisplayClass29_0_t33E981059E541FB0A518849563350F400EEB2EBC::get_offset_of_U3CU3E9__4_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6957 = { sizeof (U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0), -1, sizeof(U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6957[6] = 
{
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields::get_offset_of_U3CU3E9__29_0_1(),
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields::get_offset_of_U3CU3E9__29_1_2(),
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields::get_offset_of_U3CU3E9__29_3_3(),
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields::get_offset_of_U3CU3E9__30_0_4(),
	U3CU3Ec_t0AFD5242D659125E0BF890935FE20FD3257ED9F0_StaticFields::get_offset_of_U3CU3E9__30_1_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6958 = { sizeof (U3CU3Ec__DisplayClass31_0_t01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6958[1] = 
{
	U3CU3Ec__DisplayClass31_0_t01F8BCB973363A9BAC1680ADBDDDA1201D14ED1A::get_offset_of_parents_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6959 = { sizeof (SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6959[6] = 
{
	SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E::get_offset_of__lateInstallers_9(),
	SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E::get_offset_of__lateInstallerPrefabs_10(),
	SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E::get_offset_of__lateScriptableObjectInstallers_11(),
	SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E::get_offset_of__decoratedContractName_12(),
	SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E::get_offset_of__container_13(),
	SceneDecoratorContext_t2FF8D761F8EED82ECFD78B59BDE4566B229B8D8E::get_offset_of__injectableMonoBehaviours_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6960 = { sizeof (StaticContext_t6CE037680BBC8B0C5E79BF04FD72E21E7D47B578), -1, sizeof(StaticContext_t6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6960[1] = 
{
	StaticContext_t6CE037680BBC8B0C5E79BF04FD72E21E7D47B578_StaticFields::get_offset_of__container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6961 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6962 = { sizeof (Installer_t2E2802A812A854AEA82893477CE51FADCBA0E05E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6963 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6964 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6965 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6966 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6967 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6968 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6969 = { sizeof (InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6969[1] = 
{
	InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F::get_offset_of__container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6970 = { sizeof (MonoInstaller_t5D8FF9EC635C3C146F50E3490F8B81803B1688EC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6971 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6972 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6973 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6974 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6975 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6976 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6977 = { sizeof (MonoInstallerUtil_tD3CDCDBD575D81610D4A9B18111BC1717D0D0EA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6978 = { sizeof (MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6978[1] = 
{
	MonoInstallerBase_tC2BC7AF08360C9F8D2703E955C0D5EC9AF4C62DF::get_offset_of__container_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6979 = { sizeof (ScriptableObjectInstaller_tC387A44505BCD83E13820829F1C493DCB41E49A5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6980 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6981 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6982 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6983 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6984 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6985 = { sizeof (ScriptableObjectInstallerUtil_tF70646D34A7AC2E4DE8774A737623F8B89E11483), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6986 = { sizeof (ScriptableObjectInstallerBase_tA8EF4D1EDAF141574B49C2AC416CA782472D89C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6986[1] = 
{
	ScriptableObjectInstallerBase_tA8EF4D1EDAF141574B49C2AC416CA782472D89C9::get_offset_of__container_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6987 = { sizeof (ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6987[4] = 
{
	ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79::get_offset_of__components_4(),
	ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79::get_offset_of__identifier_5(),
	ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79::get_offset_of__context_6(),
	ZenjectBinding_t6F8DF0683477500C50AFA2FD9B0B85A4DC143F79::get_offset_of__bindType_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6988 = { sizeof (BindTypes_tB4EB04A4559867A1121813DC1479CCB26BD3E04E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6988[5] = 
{
	BindTypes_tB4EB04A4559867A1121813DC1479CCB26BD3E04E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6989 = { sizeof (BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6989[2] = 
{
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1::get_offset_of_Type_0(),
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1::get_offset_of_Identifier_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6990 = { sizeof (BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6991 = { sizeof (InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6991[3] = 
{
	InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E::get_offset_of_ExtraArgs_0(),
	InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E::get_offset_of_Context_1(),
	InjectArgs_t8A0EB8658CA9A35034FFFFA08D1DCB03051E521E::get_offset_of_ConcreteIdentifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6992 = { sizeof (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6992[18] = 
{
	0,
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__providers_1(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__parentContainers_2(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__ancestorContainers_3(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__resolvesInProgress_4(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__singletonProviderCreator_5(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__singletonMarkRegistry_6(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__lazyInjector_7(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__currentBindings_8(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__childBindings_9(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__lateBindingsToValidate_10(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__isFinalizingBinding_11(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__isValidating_12(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__isInstalling_13(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of__hasDisplayedInstallWarning_14(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16(),
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9::get_offset_of_U3CDefaultParentU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6993 = { sizeof (ProviderPair_t7634A1A771CA27C61BF8290B1C7AFA9214A89828), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6993[2] = 
{
	ProviderPair_t7634A1A771CA27C61BF8290B1C7AFA9214A89828::get_offset_of_U3CProviderInfoU3Ek__BackingField_0(),
	ProviderPair_t7634A1A771CA27C61BF8290B1C7AFA9214A89828::get_offset_of_U3CContainerU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6994 = { sizeof (ProviderLookupResult_tC9F048169581A94D7FB97D56AD4A28079C5ECD84)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6994[4] = 
{
	ProviderLookupResult_tC9F048169581A94D7FB97D56AD4A28079C5ECD84::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6995 = { sizeof (LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6995[2] = 
{
	LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8::get_offset_of_Provider_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8::get_offset_of_BindingId_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6996 = { sizeof (ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6996[2] = 
{
	ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F::get_offset_of_U3CProviderU3Ek__BackingField_0(),
	ProviderInfo_t8A37C11119853185A108C7C91EB7A5841246D76F::get_offset_of_U3CConditionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6997 = { sizeof (U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2), -1, sizeof(U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6997[11] = 
{
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__19_0_1(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__56_0_2(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__72_1_3(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__73_1_4(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__73_3_5(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__73_4_6(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__81_0_7(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__83_0_8(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__171_0_9(),
	U3CU3Ec_t8265ECB7267C32E72FAB3D880D4EEDADD706ECD2_StaticFields::get_offset_of_U3CU3E9__172_0_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6998 = { sizeof (U3CU3Ec__DisplayClass49_0_tE952E94AC2D9F07E2D4E3987967341CF44AC9C77), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6998[1] = 
{
	U3CU3Ec__DisplayClass49_0_tE952E94AC2D9F07E2D4E3987967341CF44AC9C77::get_offset_of_injectContext_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6999 = { sizeof (U3CU3Ec__DisplayClass57_0_t993B926C458D0C3939726A9A3D488AA0D414229A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6999[1] = 
{
	U3CU3Ec__DisplayClass57_0_t993B926C458D0C3939726A9A3D488AA0D414229A::get_offset_of_context_0(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
