﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Func`2<System.Type,System.Boolean>
struct Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9;
// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>
struct Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16;
// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider>
struct Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9;
// System.String
struct String_t;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6;
// Zenject.BindInfo
struct BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1;
// Zenject.ConventionBindInfo
struct ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757;
// Zenject.FromBinder
struct FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_TD16C322A15B288604DD4E6FF444DBE9A6C0ED007_H
#define U3CU3EC__DISPLAYCLASS15_0_TD16C322A15B288604DD4E6FF444DBE9A6C0ED007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_tD16C322A15B288604DD4E6FF444DBE9A6C0ED007  : public RuntimeObject
{
public:
	// System.String Zenject.ConventionFilterTypesBinder/<>c__DisplayClass15_0::prefix
	String_t* ___prefix_0;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_tD16C322A15B288604DD4E6FF444DBE9A6C0ED007, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_TD16C322A15B288604DD4E6FF444DBE9A6C0ED007_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_TDBA5163D18AF2474161FD05ACA2C1251658145AC_H
#define U3CU3EC__DISPLAYCLASS18_0_TDBA5163D18AF2474161FD05ACA2C1251658145AC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionFilterTypesBinder/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_tDBA5163D18AF2474161FD05ACA2C1251658145AC  : public RuntimeObject
{
public:
	// System.Text.RegularExpressions.Regex Zenject.ConventionFilterTypesBinder/<>c__DisplayClass18_0::regex
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___regex_0;

public:
	inline static int32_t get_offset_of_regex_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_tDBA5163D18AF2474161FD05ACA2C1251658145AC, ___regex_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_regex_0() const { return ___regex_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_regex_0() { return &___regex_0; }
	inline void set_regex_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___regex_0 = value;
		Il2CppCodeGenWriteBarrier((&___regex_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_TDBA5163D18AF2474161FD05ACA2C1251658145AC_H
#ifndef CONVENTIONSELECTTYPESBINDER_T3CE32644BED8F3F7502EF32CC41E470BDC49B590_H
#define CONVENTIONSELECTTYPESBINDER_T3CE32644BED8F3F7502EF32CC41E470BDC49B590_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionSelectTypesBinder
struct  ConventionSelectTypesBinder_t3CE32644BED8F3F7502EF32CC41E470BDC49B590  : public RuntimeObject
{
public:
	// Zenject.ConventionBindInfo Zenject.ConventionSelectTypesBinder::_bindInfo
	ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 * ____bindInfo_0;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(ConventionSelectTypesBinder_t3CE32644BED8F3F7502EF32CC41E470BDC49B590, ____bindInfo_0)); }
	inline ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(ConventionBindInfo_t0C1BF3F6EB989B628E3D30925957E0CDC7629757 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONVENTIONSELECTTYPESBINDER_T3CE32644BED8F3F7502EF32CC41E470BDC49B590_H
#ifndef U3CU3EC_T0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_H
#define U3CU3EC_T0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConventionSelectTypesBinder/<>c
struct  U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields
{
public:
	// Zenject.ConventionSelectTypesBinder/<>c Zenject.ConventionSelectTypesBinder/<>c::<>9
	U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder/<>c::<>9__4_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__4_0_1;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder/<>c::<>9__5_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__5_0_2;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder/<>c::<>9__6_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__6_0_3;
	// System.Func`2<System.Type,System.Boolean> Zenject.ConventionSelectTypesBinder/<>c::<>9__7_0
	Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * ___U3CU3E9__7_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__5_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields, ___U3CU3E9__5_0_2)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__5_0_2() const { return ___U3CU3E9__5_0_2; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__5_0_2() { return &___U3CU3E9__5_0_2; }
	inline void set_U3CU3E9__5_0_2(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__5_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__5_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields, ___U3CU3E9__6_0_3)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__6_0_3() const { return ___U3CU3E9__6_0_3; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__6_0_3() { return &___U3CU3E9__6_0_3; }
	inline void set_U3CU3E9__6_0_3(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__6_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields, ___U3CU3E9__7_0_4)); }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * get_U3CU3E9__7_0_4() const { return ___U3CU3E9__7_0_4; }
	inline Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 ** get_address_of_U3CU3E9__7_0_4() { return &___U3CU3E9__7_0_4; }
	inline void set_U3CU3E9__7_0_4(Func_2_t9C37D30F5B2BC1C1DCC8FF2E2ECC57D997D9FDD9 * value)
	{
		___U3CU3E9__7_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_H
#ifndef U3CU3EC_T65A57E639C4660F06DDB3BB273A10216CB4C812D_H
#define U3CU3EC_T65A57E639C4660F06DDB3BB273A10216CB4C812D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c
struct  U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields
{
public:
	// Zenject.FromBinder/<>c Zenject.FromBinder/<>c::<>9
	U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D * ___U3CU3E9_0;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder/<>c::<>9__23_0
	Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * ___U3CU3E9__23_0_1;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder/<>c::<>9__25_0
	Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * ___U3CU3E9__25_0_2;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder/<>c::<>9__27_0
	Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * ___U3CU3E9__27_0_3;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.FromBinder/<>c::<>9__29_0
	Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * ___U3CU3E9__29_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__23_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields, ___U3CU3E9__23_0_1)); }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * get_U3CU3E9__23_0_1() const { return ___U3CU3E9__23_0_1; }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 ** get_address_of_U3CU3E9__23_0_1() { return &___U3CU3E9__23_0_1; }
	inline void set_U3CU3E9__23_0_1(Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * value)
	{
		___U3CU3E9__23_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__23_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__25_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields, ___U3CU3E9__25_0_2)); }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * get_U3CU3E9__25_0_2() const { return ___U3CU3E9__25_0_2; }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 ** get_address_of_U3CU3E9__25_0_2() { return &___U3CU3E9__25_0_2; }
	inline void set_U3CU3E9__25_0_2(Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * value)
	{
		___U3CU3E9__25_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__25_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__27_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields, ___U3CU3E9__27_0_3)); }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * get_U3CU3E9__27_0_3() const { return ___U3CU3E9__27_0_3; }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 ** get_address_of_U3CU3E9__27_0_3() { return &___U3CU3E9__27_0_3; }
	inline void set_U3CU3E9__27_0_3(Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * value)
	{
		___U3CU3E9__27_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__27_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields, ___U3CU3E9__29_0_4)); }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * get_U3CU3E9__29_0_4() const { return ___U3CU3E9__29_0_4; }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 ** get_address_of_U3CU3E9__29_0_4() { return &___U3CU3E9__29_0_4; }
	inline void set_U3CU3E9__29_0_4(Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * value)
	{
		___U3CU3E9__29_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T65A57E639C4660F06DDB3BB273A10216CB4C812D_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T50AC0308586AF24D3787ABB81A8D7313E5479635_H
#define U3CU3EC__DISPLAYCLASS13_0_T50AC0308586AF24D3787ABB81A8D7313E5479635_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t50AC0308586AF24D3787ABB81A8D7313E5479635  : public RuntimeObject
{
public:
	// System.Object Zenject.FromBinder/<>c__DisplayClass13_0::subIdentifier
	RuntimeObject * ___subIdentifier_0;

public:
	inline static int32_t get_offset_of_subIdentifier_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t50AC0308586AF24D3787ABB81A8D7313E5479635, ___subIdentifier_0)); }
	inline RuntimeObject * get_subIdentifier_0() const { return ___subIdentifier_0; }
	inline RuntimeObject ** get_address_of_subIdentifier_0() { return &___subIdentifier_0; }
	inline void set_subIdentifier_0(RuntimeObject * value)
	{
		___subIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___subIdentifier_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T50AC0308586AF24D3787ABB81A8D7313E5479635_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T30B3E88388EB4702655E9214FB629DABD0C5AFCA_H
#define U3CU3EC__DISPLAYCLASS16_0_T30B3E88388EB4702655E9214FB629DABD0C5AFCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t30B3E88388EB4702655E9214FB629DABD0C5AFCA  : public RuntimeObject
{
public:
	// System.Type Zenject.FromBinder/<>c__DisplayClass16_0::factoryType
	Type_t * ___factoryType_0;
	// Zenject.FromBinder Zenject.FromBinder/<>c__DisplayClass16_0::<>4__this
	FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_factoryType_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t30B3E88388EB4702655E9214FB629DABD0C5AFCA, ___factoryType_0)); }
	inline Type_t * get_factoryType_0() const { return ___factoryType_0; }
	inline Type_t ** get_address_of_factoryType_0() { return &___factoryType_0; }
	inline void set_factoryType_0(Type_t * value)
	{
		___factoryType_0 = value;
		Il2CppCodeGenWriteBarrier((&___factoryType_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t30B3E88388EB4702655E9214FB629DABD0C5AFCA, ___U3CU3E4__this_1)); }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T30B3E88388EB4702655E9214FB629DABD0C5AFCA_H
#ifndef U3CU3EC__DISPLAYCLASS17_0_T2D694B9105F0CF043C4675B4F23555766B0C4552_H
#define U3CU3EC__DISPLAYCLASS17_0_T2D694B9105F0CF043C4675B4F23555766B0C4552_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass17_0
struct  U3CU3Ec__DisplayClass17_0_t2D694B9105F0CF043C4675B4F23555766B0C4552  : public RuntimeObject
{
public:
	// UnityEngine.GameObject Zenject.FromBinder/<>c__DisplayClass17_0::gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___gameObject_0;
	// Zenject.FromBinder Zenject.FromBinder/<>c__DisplayClass17_0::<>4__this
	FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_gameObject_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t2D694B9105F0CF043C4675B4F23555766B0C4552, ___gameObject_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_gameObject_0() const { return ___gameObject_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_gameObject_0() { return &___gameObject_0; }
	inline void set_gameObject_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___gameObject_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObject_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass17_0_t2D694B9105F0CF043C4675B4F23555766B0C4552, ___U3CU3E4__this_1)); }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS17_0_T2D694B9105F0CF043C4675B4F23555766B0C4552_H
#ifndef U3CU3EC__DISPLAYCLASS18_0_T2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934_H
#define U3CU3EC__DISPLAYCLASS18_0_T2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass18_0
struct  U3CU3Ec__DisplayClass18_0_t2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934  : public RuntimeObject
{
public:
	// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject> Zenject.FromBinder/<>c__DisplayClass18_0::gameObjectGetter
	Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 * ___gameObjectGetter_0;
	// Zenject.FromBinder Zenject.FromBinder/<>c__DisplayClass18_0::<>4__this
	FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_gameObjectGetter_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934, ___gameObjectGetter_0)); }
	inline Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 * get_gameObjectGetter_0() const { return ___gameObjectGetter_0; }
	inline Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 ** get_address_of_gameObjectGetter_0() { return &___gameObjectGetter_0; }
	inline void set_gameObjectGetter_0(Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 * value)
	{
		___gameObjectGetter_0 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectGetter_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass18_0_t2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934, ___U3CU3E4__this_1)); }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS18_0_T2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934_H
#ifndef U3CU3EC__DISPLAYCLASS21_0_TAF0FA454F82786A8B6F6291A310D4BBC17D3CD27_H
#define U3CU3EC__DISPLAYCLASS21_0_TAF0FA454F82786A8B6F6291A310D4BBC17D3CD27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass21_0
struct  U3CU3Ec__DisplayClass21_0_tAF0FA454F82786A8B6F6291A310D4BBC17D3CD27  : public RuntimeObject
{
public:
	// Zenject.FromBinder Zenject.FromBinder/<>c__DisplayClass21_0::<>4__this
	FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * ___U3CU3E4__this_0;
	// Zenject.GameObjectCreationParameters Zenject.FromBinder/<>c__DisplayClass21_0::gameObjectInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___gameObjectInfo_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tAF0FA454F82786A8B6F6291A310D4BBC17D3CD27, ___U3CU3E4__this_0)); }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_gameObjectInfo_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass21_0_tAF0FA454F82786A8B6F6291A310D4BBC17D3CD27, ___gameObjectInfo_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_gameObjectInfo_1() const { return ___gameObjectInfo_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_gameObjectInfo_1() { return &___gameObjectInfo_1; }
	inline void set_gameObjectInfo_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___gameObjectInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&___gameObjectInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS21_0_TAF0FA454F82786A8B6F6291A310D4BBC17D3CD27_H
#ifndef NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#define NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifndef COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#define COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#define CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7  : public CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifndef ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#define ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgConditionCopyNonLazyBinder
struct  ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327  : public ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#ifndef SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#define SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgConditionCopyNonLazyBinder
struct  ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F  : public ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#ifndef FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#define FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder
struct  FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493  : public ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F
{
public:
	// Zenject.BindFinalizerWrapper Zenject.FromBinder::<FinalizerWrapper>k__BackingField
	BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * ___U3CFinalizerWrapperU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493, ___U3CFinalizerWrapperU3Ek__BackingField_1)); }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * get_U3CFinalizerWrapperU3Ek__BackingField_1() const { return ___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 ** get_address_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return &___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline void set_U3CFinalizerWrapperU3Ek__BackingField_1(BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * value)
	{
		___U3CFinalizerWrapperU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinalizerWrapperU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6700 = { sizeof (U3CU3Ec__DisplayClass15_0_tD16C322A15B288604DD4E6FF444DBE9A6C0ED007), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6700[1] = 
{
	U3CU3Ec__DisplayClass15_0_tD16C322A15B288604DD4E6FF444DBE9A6C0ED007::get_offset_of_prefix_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6701 = { sizeof (U3CU3Ec__DisplayClass18_0_tDBA5163D18AF2474161FD05ACA2C1251658145AC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6701[1] = 
{
	U3CU3Ec__DisplayClass18_0_tDBA5163D18AF2474161FD05ACA2C1251658145AC::get_offset_of_regex_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6702 = { sizeof (ConventionSelectTypesBinder_t3CE32644BED8F3F7502EF32CC41E470BDC49B590), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6702[1] = 
{
	ConventionSelectTypesBinder_t3CE32644BED8F3F7502EF32CC41E470BDC49B590::get_offset_of__bindInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6703 = { sizeof (U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F), -1, sizeof(U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6703[5] = 
{
	U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
	U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields::get_offset_of_U3CU3E9__5_0_2(),
	U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields::get_offset_of_U3CU3E9__6_0_3(),
	U3CU3Ec_t0C73F65A3127D478D63D81FDFD8CEBBF7D76632F_StaticFields::get_offset_of_U3CU3E9__7_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6704 = { sizeof (CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6705 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6706 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6706[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6707 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6707[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6708 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6708[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6709 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6709[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6710 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6710[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6711 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6711[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6712 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6713 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6713[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6714 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6714[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6715 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6715[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6716 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6717 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6717[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6718 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6718[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6719 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6719[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6720 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6721 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6721[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6722 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6722[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6723 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6723[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6724 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6725 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6725[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6726 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6726[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6727 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6727[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6728 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6729 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6729[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6730 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6730[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6731 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6731[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6732 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6733 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6733[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6734 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6734[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6735 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6735[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6736 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6736[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6737 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6737[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6738 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6739 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6739[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6740 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6740[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6741 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6741[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6742 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6743 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6743[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6744 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6744[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6745 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6745[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6746 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6747 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6747[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6748 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6748[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6749 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6749[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6750 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6751 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6751[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6752 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6752[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6753 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6753[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6754 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6755 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6755[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6756 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6756[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6757 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6757[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6758 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6758[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6759 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6759[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6760 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6760[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6761 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6761[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6762 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6763 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6763[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6764 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6764[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6765 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6765[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6766 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6766[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6767 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6767[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6768 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6768[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6769 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6769[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6770 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6770[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6771 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6771[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6772 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6772[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6773 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6773[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6774 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6774[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6775 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6775[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6776 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6776[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6777 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6778 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6779 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6780 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6781 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6782 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6783 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6784 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6785 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6786 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6787 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6788 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6789 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6789[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6790 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6790[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6791 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6791[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6792 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6793 = { sizeof (FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6793[1] = 
{
	FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493::get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6794 = { sizeof (U3CU3Ec__DisplayClass13_0_t50AC0308586AF24D3787ABB81A8D7313E5479635), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6794[1] = 
{
	U3CU3Ec__DisplayClass13_0_t50AC0308586AF24D3787ABB81A8D7313E5479635::get_offset_of_subIdentifier_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6795 = { sizeof (U3CU3Ec__DisplayClass16_0_t30B3E88388EB4702655E9214FB629DABD0C5AFCA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6795[2] = 
{
	U3CU3Ec__DisplayClass16_0_t30B3E88388EB4702655E9214FB629DABD0C5AFCA::get_offset_of_factoryType_0(),
	U3CU3Ec__DisplayClass16_0_t30B3E88388EB4702655E9214FB629DABD0C5AFCA::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6796 = { sizeof (U3CU3Ec__DisplayClass17_0_t2D694B9105F0CF043C4675B4F23555766B0C4552), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6796[2] = 
{
	U3CU3Ec__DisplayClass17_0_t2D694B9105F0CF043C4675B4F23555766B0C4552::get_offset_of_gameObject_0(),
	U3CU3Ec__DisplayClass17_0_t2D694B9105F0CF043C4675B4F23555766B0C4552::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6797 = { sizeof (U3CU3Ec__DisplayClass18_0_t2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6797[2] = 
{
	U3CU3Ec__DisplayClass18_0_t2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934::get_offset_of_gameObjectGetter_0(),
	U3CU3Ec__DisplayClass18_0_t2F8B9D5A2D978D935E90FD4B1E87D43C5F5BC934::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6798 = { sizeof (U3CU3Ec__DisplayClass21_0_tAF0FA454F82786A8B6F6291A310D4BBC17D3CD27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6798[2] = 
{
	U3CU3Ec__DisplayClass21_0_tAF0FA454F82786A8B6F6291A310D4BBC17D3CD27::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass21_0_tAF0FA454F82786A8B6F6291A310D4BBC17D3CD27::get_offset_of_gameObjectInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6799 = { sizeof (U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D), -1, sizeof(U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6799[5] = 
{
	U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields::get_offset_of_U3CU3E9__23_0_1(),
	U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields::get_offset_of_U3CU3E9__25_0_2(),
	U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields::get_offset_of_U3CU3E9__27_0_3(),
	U3CU3Ec_t65A57E639C4660F06DDB3BB273A10216CB4C812D_StaticFields::get_offset_of_U3CU3E9__29_0_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
