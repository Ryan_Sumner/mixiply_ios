﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// ModestTree.Util.Action`5<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object>
struct Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376;
// ModestTree.Util.Action`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`1<Zenject.DiContainer>
struct Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A;
// System.Action`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>>
struct Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169;
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct IEnumerable_1_t56523080378086E675AF5FBD2FB1A082207E798D;
// System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA;
// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.DiContainer>
struct List_1_t613607F896640D94824672D01078AB4BBFA62166;
// System.Collections.Generic.List`1<Zenject.IBindingFinalizer>
struct List_1_tDB9537C32346F69532603D22AD9870A14679F4E4;
// System.Collections.Generic.List`1<Zenject.ILazy>
struct List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10;
// System.Collections.Generic.List`1<Zenject.InstallerBase>
struct List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595;
// System.Collections.Generic.List`1<Zenject.MonoInstaller>
struct List_1_tED6028C19769161B40D318A4663FA052CF5836D2;
// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller>
struct List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2;
// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct List_1_t825D392F76C74F64539B9A7837BB72D0215D2744;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7;
// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer>
struct Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686;
// System.Collections.Generic.Stack`1<Zenject.DiContainer/LookupId>
struct Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Func`2<Zenject.InjectContext,UnityEngine.Transform>
struct Func_2_t4ED720590592A8375BE8E36276393417F557F933;
// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Object>
struct Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Reflection.Binder
struct Binder_t4D5CB06963501D32847C057B57157D6DC49CA759;
// System.Reflection.MemberFilter
struct MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.ActionInstaller
struct ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3;
// Zenject.Context
struct Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;
// Zenject.GameObjectContext
struct GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4;
// Zenject.IPrefabProvider
struct IPrefabProvider_t03CCCDA76ECC8346AB1BF4B6BEB0BAD1209DABB0;
// Zenject.InstallerBase
struct InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F;
// Zenject.LazyInstanceInjector
struct LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E;
// Zenject.MonoKernel
struct MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751;
// Zenject.SingletonProviderCreator
struct SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D;
// Zenject.SubContainerCreatorByNewPrefabDynamicContext
struct SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167;
// Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0<System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116;
// Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98;
// Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228;
// Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604;
// Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>
struct U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7;
// Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD;
// Zenject.TaskUpdater`1/<>c<System.Object>
struct U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C;
// Zenject.TaskUpdater`1/<>c__DisplayClass8_0<System.Object>
struct U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED;
// Zenject.TaskUpdater`1/TaskInfo<System.Object>[]
struct TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2;
// Zenject.TaskUpdater`1<System.Object>
struct TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22;
// Zenject.TypeValuePair
struct TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B;
// Zenject.TypeValuePair[]
struct TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3;

extern RuntimeClass* ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_il2cpp_TypeInfo_var;
extern RuntimeClass* Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A_il2cpp_TypeInfo_var;
extern RuntimeClass* Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var;
extern String_t* _stringLiteral25458F856CEB4BFBD8F8627119406BC1F8394D8E;
extern String_t* _stringLiteral9C9C4ECE5F3F5332F9E4B20785827F8F41DEDADB;
extern String_t* _stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8;
extern const RuntimeMethod* Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC_RuntimeMethod_var;
extern const RuntimeMethod* List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var;
extern const uint32_t SubContainerCreatorByNewPrefabMethod_3_AddInstallers_mAB9F3C7AE1977173FC4C8E70B4A05F83EBE694B2_MetadataUsageId;
extern const uint32_t SubContainerCreatorByNewPrefabMethod_4_AddInstallers_m9828A2F23798CE6551A53409A549664002BCC116_MetadataUsageId;
extern const uint32_t SubContainerCreatorByNewPrefabMethod_5_AddInstallers_m838FB805DAC17A8AB2C6311738EA1FD0381A0A55_MetadataUsageId;
extern const uint32_t TaskUpdater_1_AddTaskInternal_m6E6872801E66FCE21D7831606B8BA2C3770EC321_MetadataUsageId;
extern const uint32_t TaskUpdater_1_RemoveTask_mBC9BFA51A7EB4AB0F0B0A9FB1C505E63C1EF3082_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m3F0E32C6062FB1633DC7DD6F6D3F28970E850B36_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mB304DF70CC2A8B8945E7707215B6FD2D64DA8655_MetadataUsageId;
extern const uint32_t U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mE590FC7D03145BDD8A2184F07A1B0FD98847F70E_MetadataUsageId;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
struct Il2CppArrayBounds;
#ifndef RUNTIMEARRAY_H
#define RUNTIMEARRAY_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Array

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEARRAY_H
#ifndef LINKEDLISTNODE_1_T5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA_H
#define LINKEDLISTNODE_1_T5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedListNode`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<T> System.Collections.Generic.LinkedListNode`1::list
	LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * ___list_0;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::next
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * ___next_1;
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedListNode`1::prev
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * ___prev_2;
	// T System.Collections.Generic.LinkedListNode`1::item
	TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * ___item_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA, ___list_0)); }
	inline LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * get_list_0() const { return ___list_0; }
	inline LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA, ___next_1)); }
	inline LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * get_next_1() const { return ___next_1; }
	inline LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}

	inline static int32_t get_offset_of_prev_2() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA, ___prev_2)); }
	inline LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * get_prev_2() const { return ___prev_2; }
	inline LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA ** get_address_of_prev_2() { return &___prev_2; }
	inline void set_prev_2(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * value)
	{
		___prev_2 = value;
		Il2CppCodeGenWriteBarrier((&___prev_2), value);
	}

	inline static int32_t get_offset_of_item_3() { return static_cast<int32_t>(offsetof(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA, ___item_3)); }
	inline TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * get_item_3() const { return ___item_3; }
	inline TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED ** get_address_of_item_3() { return &___item_3; }
	inline void set_item_3(TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * value)
	{
		___item_3 = value;
		Il2CppCodeGenWriteBarrier((&___item_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDLISTNODE_1_T5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA_H
#ifndef LINKEDLIST_1_T89AC263A7FE9EAA4CCCAA082F939484AB926CC04_H
#define LINKEDLIST_1_T89AC263A7FE9EAA4CCCAA082F939484AB926CC04_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct  LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedListNode`1<T> System.Collections.Generic.LinkedList`1::head
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * ___head_0;
	// System.Int32 System.Collections.Generic.LinkedList`1::count
	int32_t ___count_1;
	// System.Int32 System.Collections.Generic.LinkedList`1::version
	int32_t ___version_2;
	// System.Object System.Collections.Generic.LinkedList`1::_syncRoot
	RuntimeObject * ____syncRoot_3;
	// System.Runtime.Serialization.SerializationInfo System.Collections.Generic.LinkedList`1::_siInfo
	SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * ____siInfo_4;

public:
	inline static int32_t get_offset_of_head_0() { return static_cast<int32_t>(offsetof(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04, ___head_0)); }
	inline LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * get_head_0() const { return ___head_0; }
	inline LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA ** get_address_of_head_0() { return &___head_0; }
	inline void set_head_0(LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * value)
	{
		___head_0 = value;
		Il2CppCodeGenWriteBarrier((&___head_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of__syncRoot_3() { return static_cast<int32_t>(offsetof(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04, ____syncRoot_3)); }
	inline RuntimeObject * get__syncRoot_3() const { return ____syncRoot_3; }
	inline RuntimeObject ** get_address_of__syncRoot_3() { return &____syncRoot_3; }
	inline void set__syncRoot_3(RuntimeObject * value)
	{
		____syncRoot_3 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_3), value);
	}

	inline static int32_t get_offset_of__siInfo_4() { return static_cast<int32_t>(offsetof(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04, ____siInfo_4)); }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * get__siInfo_4() const { return ____siInfo_4; }
	inline SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 ** get_address_of__siInfo_4() { return &____siInfo_4; }
	inline void set__siInfo_4(SerializationInfo_t1BB80E9C9DEA52DBF464487234B045E2930ADA26 * value)
	{
		____siInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&____siInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINKEDLIST_1_T89AC263A7FE9EAA4CCCAA082F939484AB926CC04_H
#ifndef LIST_1_T825D392F76C74F64539B9A7837BB72D0215D2744_H
#define LIST_1_T825D392F76C74F64539B9A7837BB72D0215D2744_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<System.Object>>
struct  List_1_t825D392F76C74F64539B9A7837BB72D0215D2744  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t825D392F76C74F64539B9A7837BB72D0215D2744, ____items_1)); }
	inline TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2* get__items_1() const { return ____items_1; }
	inline TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t825D392F76C74F64539B9A7837BB72D0215D2744, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t825D392F76C74F64539B9A7837BB72D0215D2744, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t825D392F76C74F64539B9A7837BB72D0215D2744, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t825D392F76C74F64539B9A7837BB72D0215D2744_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t825D392F76C74F64539B9A7837BB72D0215D2744_StaticFields, ____emptyArray_5)); }
	inline TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TaskInfoU5BU5D_t9963178AB4B485EB84D1C53EE163D43ADDBFC1E2* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T825D392F76C74F64539B9A7837BB72D0215D2744_H
#ifndef LIST_1_T0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7_H
#define LIST_1_T0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct  List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7  : public RuntimeObject
{
public:
	// T[] System.Collections.Generic.List`1::_items
	TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3* ____items_1;
	// System.Int32 System.Collections.Generic.List`1::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1::_version
	int32_t ____version_3;
	// System.Object System.Collections.Generic.List`1::_syncRoot
	RuntimeObject * ____syncRoot_4;

public:
	inline static int32_t get_offset_of__items_1() { return static_cast<int32_t>(offsetof(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7, ____items_1)); }
	inline TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3* get__items_1() const { return ____items_1; }
	inline TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3** get_address_of__items_1() { return &____items_1; }
	inline void set__items_1(TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3* value)
	{
		____items_1 = value;
		Il2CppCodeGenWriteBarrier((&____items_1), value);
	}

	inline static int32_t get_offset_of__size_2() { return static_cast<int32_t>(offsetof(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7, ____size_2)); }
	inline int32_t get__size_2() const { return ____size_2; }
	inline int32_t* get_address_of__size_2() { return &____size_2; }
	inline void set__size_2(int32_t value)
	{
		____size_2 = value;
	}

	inline static int32_t get_offset_of__version_3() { return static_cast<int32_t>(offsetof(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7, ____version_3)); }
	inline int32_t get__version_3() const { return ____version_3; }
	inline int32_t* get_address_of__version_3() { return &____version_3; }
	inline void set__version_3(int32_t value)
	{
		____version_3 = value;
	}

	inline static int32_t get_offset_of__syncRoot_4() { return static_cast<int32_t>(offsetof(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7, ____syncRoot_4)); }
	inline RuntimeObject * get__syncRoot_4() const { return ____syncRoot_4; }
	inline RuntimeObject ** get_address_of__syncRoot_4() { return &____syncRoot_4; }
	inline void set__syncRoot_4(RuntimeObject * value)
	{
		____syncRoot_4 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_4), value);
	}
};

struct List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7_StaticFields
{
public:
	// T[] System.Collections.Generic.List`1::_emptyArray
	TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3* ____emptyArray_5;

public:
	inline static int32_t get_offset_of__emptyArray_5() { return static_cast<int32_t>(offsetof(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7_StaticFields, ____emptyArray_5)); }
	inline TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3* get__emptyArray_5() const { return ____emptyArray_5; }
	inline TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3** get_address_of__emptyArray_5() { return &____emptyArray_5; }
	inline void set__emptyArray_5(TypeValuePairU5BU5D_t04FBA751A2DB56DCF45CE21F8245D03D9F8D17B3* value)
	{
		____emptyArray_5 = value;
		Il2CppCodeGenWriteBarrier((&____emptyArray_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LIST_1_T0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7_H
#ifndef MEMBERINFO_T_H
#define MEMBERINFO_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.MemberInfo
struct  MemberInfo_t  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERINFO_T_H
#ifndef STRING_T_H
#define STRING_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.String
struct  String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRING_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef DICONTAINER_TBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9_H
#define DICONTAINER_TBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer
struct  DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Zenject.BindingId,System.Collections.Generic.List`1<Zenject.DiContainer/ProviderInfo>> Zenject.DiContainer::_providers
	Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 * ____providers_1;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_parentContainers
	List_1_t613607F896640D94824672D01078AB4BBFA62166 * ____parentContainers_2;
	// System.Collections.Generic.List`1<Zenject.DiContainer> Zenject.DiContainer::_ancestorContainers
	List_1_t613607F896640D94824672D01078AB4BBFA62166 * ____ancestorContainers_3;
	// System.Collections.Generic.Stack`1<Zenject.DiContainer/LookupId> Zenject.DiContainer::_resolvesInProgress
	Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F * ____resolvesInProgress_4;
	// Zenject.SingletonProviderCreator Zenject.DiContainer::_singletonProviderCreator
	SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D * ____singletonProviderCreator_5;
	// Zenject.SingletonMarkRegistry Zenject.DiContainer::_singletonMarkRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____singletonMarkRegistry_6;
	// Zenject.LazyInstanceInjector Zenject.DiContainer::_lazyInjector
	LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E * ____lazyInjector_7;
	// System.Collections.Generic.Queue`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_currentBindings
	Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 * ____currentBindings_8;
	// System.Collections.Generic.List`1<Zenject.IBindingFinalizer> Zenject.DiContainer::_childBindings
	List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 * ____childBindings_9;
	// System.Collections.Generic.List`1<Zenject.ILazy> Zenject.DiContainer::_lateBindingsToValidate
	List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 * ____lateBindingsToValidate_10;
	// System.Boolean Zenject.DiContainer::_isFinalizingBinding
	bool ____isFinalizingBinding_11;
	// System.Boolean Zenject.DiContainer::_isValidating
	bool ____isValidating_12;
	// System.Boolean Zenject.DiContainer::_isInstalling
	bool ____isInstalling_13;
	// System.Boolean Zenject.DiContainer::_hasDisplayedInstallWarning
	bool ____hasDisplayedInstallWarning_14;
	// System.Boolean Zenject.DiContainer::<ShouldCheckForInstallWarning>k__BackingField
	bool ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15;
	// System.Boolean Zenject.DiContainer::<AssertOnNewGameObjects>k__BackingField
	bool ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16;
	// UnityEngine.Transform Zenject.DiContainer::<DefaultParent>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CDefaultParentU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of__providers_1() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____providers_1)); }
	inline Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 * get__providers_1() const { return ____providers_1; }
	inline Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 ** get_address_of__providers_1() { return &____providers_1; }
	inline void set__providers_1(Dictionary_2_t808A8DCB705CDA9F1CC26A8FF71AE0413DC9E169 * value)
	{
		____providers_1 = value;
		Il2CppCodeGenWriteBarrier((&____providers_1), value);
	}

	inline static int32_t get_offset_of__parentContainers_2() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____parentContainers_2)); }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 * get__parentContainers_2() const { return ____parentContainers_2; }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 ** get_address_of__parentContainers_2() { return &____parentContainers_2; }
	inline void set__parentContainers_2(List_1_t613607F896640D94824672D01078AB4BBFA62166 * value)
	{
		____parentContainers_2 = value;
		Il2CppCodeGenWriteBarrier((&____parentContainers_2), value);
	}

	inline static int32_t get_offset_of__ancestorContainers_3() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____ancestorContainers_3)); }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 * get__ancestorContainers_3() const { return ____ancestorContainers_3; }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 ** get_address_of__ancestorContainers_3() { return &____ancestorContainers_3; }
	inline void set__ancestorContainers_3(List_1_t613607F896640D94824672D01078AB4BBFA62166 * value)
	{
		____ancestorContainers_3 = value;
		Il2CppCodeGenWriteBarrier((&____ancestorContainers_3), value);
	}

	inline static int32_t get_offset_of__resolvesInProgress_4() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____resolvesInProgress_4)); }
	inline Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F * get__resolvesInProgress_4() const { return ____resolvesInProgress_4; }
	inline Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F ** get_address_of__resolvesInProgress_4() { return &____resolvesInProgress_4; }
	inline void set__resolvesInProgress_4(Stack_1_t6FC65F3AA205E98B45903AE89EFEEA75A171FB4F * value)
	{
		____resolvesInProgress_4 = value;
		Il2CppCodeGenWriteBarrier((&____resolvesInProgress_4), value);
	}

	inline static int32_t get_offset_of__singletonProviderCreator_5() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____singletonProviderCreator_5)); }
	inline SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D * get__singletonProviderCreator_5() const { return ____singletonProviderCreator_5; }
	inline SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D ** get_address_of__singletonProviderCreator_5() { return &____singletonProviderCreator_5; }
	inline void set__singletonProviderCreator_5(SingletonProviderCreator_t9E289C081C7F5C38D3431B63AEA3079E327F703D * value)
	{
		____singletonProviderCreator_5 = value;
		Il2CppCodeGenWriteBarrier((&____singletonProviderCreator_5), value);
	}

	inline static int32_t get_offset_of__singletonMarkRegistry_6() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____singletonMarkRegistry_6)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__singletonMarkRegistry_6() const { return ____singletonMarkRegistry_6; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__singletonMarkRegistry_6() { return &____singletonMarkRegistry_6; }
	inline void set__singletonMarkRegistry_6(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____singletonMarkRegistry_6 = value;
		Il2CppCodeGenWriteBarrier((&____singletonMarkRegistry_6), value);
	}

	inline static int32_t get_offset_of__lazyInjector_7() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____lazyInjector_7)); }
	inline LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E * get__lazyInjector_7() const { return ____lazyInjector_7; }
	inline LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E ** get_address_of__lazyInjector_7() { return &____lazyInjector_7; }
	inline void set__lazyInjector_7(LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E * value)
	{
		____lazyInjector_7 = value;
		Il2CppCodeGenWriteBarrier((&____lazyInjector_7), value);
	}

	inline static int32_t get_offset_of__currentBindings_8() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____currentBindings_8)); }
	inline Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 * get__currentBindings_8() const { return ____currentBindings_8; }
	inline Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 ** get_address_of__currentBindings_8() { return &____currentBindings_8; }
	inline void set__currentBindings_8(Queue_1_tDEAF6399ECE4280CA325BF07024A63CEA326E686 * value)
	{
		____currentBindings_8 = value;
		Il2CppCodeGenWriteBarrier((&____currentBindings_8), value);
	}

	inline static int32_t get_offset_of__childBindings_9() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____childBindings_9)); }
	inline List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 * get__childBindings_9() const { return ____childBindings_9; }
	inline List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 ** get_address_of__childBindings_9() { return &____childBindings_9; }
	inline void set__childBindings_9(List_1_tDB9537C32346F69532603D22AD9870A14679F4E4 * value)
	{
		____childBindings_9 = value;
		Il2CppCodeGenWriteBarrier((&____childBindings_9), value);
	}

	inline static int32_t get_offset_of__lateBindingsToValidate_10() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____lateBindingsToValidate_10)); }
	inline List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 * get__lateBindingsToValidate_10() const { return ____lateBindingsToValidate_10; }
	inline List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 ** get_address_of__lateBindingsToValidate_10() { return &____lateBindingsToValidate_10; }
	inline void set__lateBindingsToValidate_10(List_1_t2357FDBAFB05DD726882BB60A2C7BFF083BECA10 * value)
	{
		____lateBindingsToValidate_10 = value;
		Il2CppCodeGenWriteBarrier((&____lateBindingsToValidate_10), value);
	}

	inline static int32_t get_offset_of__isFinalizingBinding_11() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____isFinalizingBinding_11)); }
	inline bool get__isFinalizingBinding_11() const { return ____isFinalizingBinding_11; }
	inline bool* get_address_of__isFinalizingBinding_11() { return &____isFinalizingBinding_11; }
	inline void set__isFinalizingBinding_11(bool value)
	{
		____isFinalizingBinding_11 = value;
	}

	inline static int32_t get_offset_of__isValidating_12() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____isValidating_12)); }
	inline bool get__isValidating_12() const { return ____isValidating_12; }
	inline bool* get_address_of__isValidating_12() { return &____isValidating_12; }
	inline void set__isValidating_12(bool value)
	{
		____isValidating_12 = value;
	}

	inline static int32_t get_offset_of__isInstalling_13() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____isInstalling_13)); }
	inline bool get__isInstalling_13() const { return ____isInstalling_13; }
	inline bool* get_address_of__isInstalling_13() { return &____isInstalling_13; }
	inline void set__isInstalling_13(bool value)
	{
		____isInstalling_13 = value;
	}

	inline static int32_t get_offset_of__hasDisplayedInstallWarning_14() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ____hasDisplayedInstallWarning_14)); }
	inline bool get__hasDisplayedInstallWarning_14() const { return ____hasDisplayedInstallWarning_14; }
	inline bool* get_address_of__hasDisplayedInstallWarning_14() { return &____hasDisplayedInstallWarning_14; }
	inline void set__hasDisplayedInstallWarning_14(bool value)
	{
		____hasDisplayedInstallWarning_14 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15)); }
	inline bool get_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() const { return ___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CShouldCheckForInstallWarningU3Ek__BackingField_15() { return &___U3CShouldCheckForInstallWarningU3Ek__BackingField_15; }
	inline void set_U3CShouldCheckForInstallWarningU3Ek__BackingField_15(bool value)
	{
		___U3CShouldCheckForInstallWarningU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16)); }
	inline bool get_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() const { return ___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CAssertOnNewGameObjectsU3Ek__BackingField_16() { return &___U3CAssertOnNewGameObjectsU3Ek__BackingField_16; }
	inline void set_U3CAssertOnNewGameObjectsU3Ek__BackingField_16(bool value)
	{
		___U3CAssertOnNewGameObjectsU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultParentU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9, ___U3CDefaultParentU3Ek__BackingField_17)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CDefaultParentU3Ek__BackingField_17() const { return ___U3CDefaultParentU3Ek__BackingField_17; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CDefaultParentU3Ek__BackingField_17() { return &___U3CDefaultParentU3Ek__BackingField_17; }
	inline void set_U3CDefaultParentU3Ek__BackingField_17(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CDefaultParentU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultParentU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DICONTAINER_TBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9_H
#ifndef INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#define INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstallerBase
struct  InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.InstallerBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLERBASE_TAD3E3395B906666822EB47EAC578547EEB7DC56F_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_TC409E09A3C80E147D7A66BF53D5E858F2049E167_H
#define SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_TC409E09A3C80E147D7A66BF53D5E858F2049E167_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabDynamicContext
struct  SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167  : public RuntimeObject
{
public:
	// Zenject.GameObjectCreationParameters Zenject.SubContainerCreatorByNewPrefabDynamicContext::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_0;
	// Zenject.IPrefabProvider Zenject.SubContainerCreatorByNewPrefabDynamicContext::_prefabProvider
	RuntimeObject* ____prefabProvider_1;
	// Zenject.DiContainer Zenject.SubContainerCreatorByNewPrefabDynamicContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167, ____gameObjectBindInfo_0)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_0() const { return ____gameObjectBindInfo_0; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_0() { return &____gameObjectBindInfo_0; }
	inline void set__gameObjectBindInfo_0(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_0), value);
	}

	inline static int32_t get_offset_of__prefabProvider_1() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167, ____prefabProvider_1)); }
	inline RuntimeObject* get__prefabProvider_1() const { return ____prefabProvider_1; }
	inline RuntimeObject** get_address_of__prefabProvider_1() { return &____prefabProvider_1; }
	inline void set__prefabProvider_1(RuntimeObject* value)
	{
		____prefabProvider_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABDYNAMICCONTEXT_TC409E09A3C80E147D7A66BF53D5E858F2049E167_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TAF8DCF78BAA89C71DA98B77B709B301DA177B116_H
#define U3CU3EC__DISPLAYCLASS2_0_TAF8DCF78BAA89C71DA98B77B709B301DA177B116_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0<System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`3<TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116, ___args_1)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_1() const { return ___args_1; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TAF8DCF78BAA89C71DA98B77B709B301DA177B116_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_TA1D56E22F3C877086C887309B527C11A8C8EA228_H
#define U3CU3EC__DISPLAYCLASS2_0_TA1D56E22F3C877086C887309B527C11A8C8EA228_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`4<TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228, ___args_1)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_1() const { return ___args_1; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_TA1D56E22F3C877086C887309B527C11A8C8EA228_H
#ifndef U3CU3EC__DISPLAYCLASS2_0_T5EE6244949A29B790BA1F15BC7B90541CE36B2F7_H
#define U3CU3EC__DISPLAYCLASS2_0_T5EE6244949A29B790BA1F15BC7B90541CE36B2F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorByNewPrefabMethod`5<TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0::<>4__this
	SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD * ___U3CU3E4__this_0;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7, ___U3CU3E4__this_0)); }
	inline SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_args_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7, ___args_1)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_1() const { return ___args_1; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_1() { return &___args_1; }
	inline void set_args_1(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_1 = value;
		Il2CppCodeGenWriteBarrier((&___args_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS2_0_T5EE6244949A29B790BA1F15BC7B90541CE36B2F7_H
#ifndef U3CU3EC_T26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_H
#define U3CU3EC_T26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1/<>c<System.Object>
struct  U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields
{
public:
	// Zenject.TaskUpdater`1/<>c<TTask> Zenject.TaskUpdater`1/<>c::<>9
	U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * ___U3CU3E9_0;
	// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<TTask>,TTask> Zenject.TaskUpdater`1/<>c::<>9__7_0
	Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * ___U3CU3E9__7_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_H
#ifndef U3CU3EC__DISPLAYCLASS8_0_TA70D285BF0171219D582B6433359C45672A04383_H
#define U3CU3EC__DISPLAYCLASS8_0_TA70D285BF0171219D582B6433359C45672A04383_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1/<>c__DisplayClass8_0<System.Object>
struct  U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383  : public RuntimeObject
{
public:
	// TTask Zenject.TaskUpdater`1/<>c__DisplayClass8_0::task
	RuntimeObject * ___task_0;

public:
	inline static int32_t get_offset_of_task_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383, ___task_0)); }
	inline RuntimeObject * get_task_0() const { return ___task_0; }
	inline RuntimeObject ** get_address_of_task_0() { return &___task_0; }
	inline void set_task_0(RuntimeObject * value)
	{
		___task_0 = value;
		Il2CppCodeGenWriteBarrier((&___task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS8_0_TA70D285BF0171219D582B6433359C45672A04383_H
#ifndef TASKINFO_T7C162B3928420C19773BBA3A44C79FCAB82232ED_H
#define TASKINFO_T7C162B3928420C19773BBA3A44C79FCAB82232ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1/TaskInfo<System.Object>
struct  TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED  : public RuntimeObject
{
public:
	// TTask Zenject.TaskUpdater`1/TaskInfo::Task
	RuntimeObject * ___Task_0;
	// System.Int32 Zenject.TaskUpdater`1/TaskInfo::Priority
	int32_t ___Priority_1;
	// System.Boolean Zenject.TaskUpdater`1/TaskInfo::IsRemoved
	bool ___IsRemoved_2;

public:
	inline static int32_t get_offset_of_Task_0() { return static_cast<int32_t>(offsetof(TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED, ___Task_0)); }
	inline RuntimeObject * get_Task_0() const { return ___Task_0; }
	inline RuntimeObject ** get_address_of_Task_0() { return &___Task_0; }
	inline void set_Task_0(RuntimeObject * value)
	{
		___Task_0 = value;
		Il2CppCodeGenWriteBarrier((&___Task_0), value);
	}

	inline static int32_t get_offset_of_Priority_1() { return static_cast<int32_t>(offsetof(TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED, ___Priority_1)); }
	inline int32_t get_Priority_1() const { return ___Priority_1; }
	inline int32_t* get_address_of_Priority_1() { return &___Priority_1; }
	inline void set_Priority_1(int32_t value)
	{
		___Priority_1 = value;
	}

	inline static int32_t get_offset_of_IsRemoved_2() { return static_cast<int32_t>(offsetof(TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED, ___IsRemoved_2)); }
	inline bool get_IsRemoved_2() const { return ___IsRemoved_2; }
	inline bool* get_address_of_IsRemoved_2() { return &___IsRemoved_2; }
	inline void set_IsRemoved_2(bool value)
	{
		___IsRemoved_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKINFO_T7C162B3928420C19773BBA3A44C79FCAB82232ED_H
#ifndef TASKUPDATER_1_TD6EF0E920EDE4720A490536F92E1B3E4DBD67C22_H
#define TASKUPDATER_1_TD6EF0E920EDE4720A490536F92E1B3E4DBD67C22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TaskUpdater`1<System.Object>
struct  TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22  : public RuntimeObject
{
public:
	// System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_tasks
	LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * ____tasks_0;
	// System.Collections.Generic.List`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1::_queuedTasks
	List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * ____queuedTasks_1;

public:
	inline static int32_t get_offset_of__tasks_0() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22, ____tasks_0)); }
	inline LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * get__tasks_0() const { return ____tasks_0; }
	inline LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 ** get_address_of__tasks_0() { return &____tasks_0; }
	inline void set__tasks_0(LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * value)
	{
		____tasks_0 = value;
		Il2CppCodeGenWriteBarrier((&____tasks_0), value);
	}

	inline static int32_t get_offset_of__queuedTasks_1() { return static_cast<int32_t>(offsetof(TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22, ____queuedTasks_1)); }
	inline List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * get__queuedTasks_1() const { return ____queuedTasks_1; }
	inline List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 ** get_address_of__queuedTasks_1() { return &____queuedTasks_1; }
	inline void set__queuedTasks_1(List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * value)
	{
		____queuedTasks_1 = value;
		Il2CppCodeGenWriteBarrier((&____queuedTasks_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKUPDATER_1_TD6EF0E920EDE4720A490536F92E1B3E4DBD67C22_H
#ifndef TYPEVALUEPAIR_T4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B_H
#define TYPEVALUEPAIR_T4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TypeValuePair
struct  TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B  : public RuntimeObject
{
public:
	// System.Type Zenject.TypeValuePair::Type
	Type_t * ___Type_0;
	// System.Object Zenject.TypeValuePair::Value
	RuntimeObject * ___Value_1;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B, ___Type_0)); }
	inline Type_t * get_Type_0() const { return ___Type_0; }
	inline Type_t ** get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(Type_t * value)
	{
		___Type_0 = value;
		Il2CppCodeGenWriteBarrier((&___Type_0), value);
	}

	inline static int32_t get_offset_of_Value_1() { return static_cast<int32_t>(offsetof(TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B, ___Value_1)); }
	inline RuntimeObject * get_Value_1() const { return ___Value_1; }
	inline RuntimeObject ** get_address_of_Value_1() { return &___Value_1; }
	inline void set_Value_1(RuntimeObject * value)
	{
		___Value_1 = value;
		Il2CppCodeGenWriteBarrier((&___Value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEVALUEPAIR_T4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#define INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Int32
struct  Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT32_T585191389E07734F19F3156FF88FB3EF4800D102_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef INSTALLER_1_T7B0D6B74E152A6A77ECC9A94F10159F721157919_H
#define INSTALLER_1_T7B0D6B74E152A6A77ECC9A94F10159F721157919_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Installer`1<Zenject.ActionInstaller>
struct  Installer_1_t7B0D6B74E152A6A77ECC9A94F10159F721157919  : public InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTALLER_1_T7B0D6B74E152A6A77ECC9A94F10159F721157919_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABMETHOD_3_T99BB0964B449F8CFB6F77F9FB03D13399D3C1E98_H
#define SUBCONTAINERCREATORBYNEWPREFABMETHOD_3_T99BB0964B449F8CFB6F77F9FB03D13399D3C1E98_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98  : public SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167
{
public:
	// System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3> Zenject.SubContainerCreatorByNewPrefabMethod`3::_installerMethod
	Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98, ____installerMethod_3)); }
	inline Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerMethod_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABMETHOD_3_T99BB0964B449F8CFB6F77F9FB03D13399D3C1E98_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABMETHOD_4_T65BB6C835823BCD50EFA80A6F36E0CB98A471604_H
#define SUBCONTAINERCREATORBYNEWPREFABMETHOD_4_T65BB6C835823BCD50EFA80A6F36E0CB98A471604_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604  : public SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167
{
public:
	// ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4> Zenject.SubContainerCreatorByNewPrefabMethod`4::_installerMethod
	Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604, ____installerMethod_3)); }
	inline Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerMethod_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABMETHOD_4_T65BB6C835823BCD50EFA80A6F36E0CB98A471604_H
#ifndef SUBCONTAINERCREATORBYNEWPREFABMETHOD_5_T276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD_H
#define SUBCONTAINERCREATORBYNEWPREFABMETHOD_5_T276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>
struct  SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD  : public SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167
{
public:
	// ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5> Zenject.SubContainerCreatorByNewPrefabMethod`5::_installerMethod
	Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E * ____installerMethod_3;

public:
	inline static int32_t get_offset_of__installerMethod_3() { return static_cast<int32_t>(offsetof(SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD, ____installerMethod_3)); }
	inline Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E * get__installerMethod_3() const { return ____installerMethod_3; }
	inline Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E ** get_address_of__installerMethod_3() { return &____installerMethod_3; }
	inline void set__installerMethod_3(Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E * value)
	{
		____installerMethod_3 = value;
		Il2CppCodeGenWriteBarrier((&____installerMethod_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERCREATORBYNEWPREFABMETHOD_5_T276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#define NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Quaternion>
struct  Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41 
{
public:
	// T System.Nullable`1::value
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41, ___value_0)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_value_0() const { return ___value_0; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#ifndef NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#define NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#define RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.RuntimeTypeHandle
struct  RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMETYPEHANDLE_T7B542280A22F0EC4EAC2061C29178845847A8B2D_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef ACTIONINSTALLER_T208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_H
#define ACTIONINSTALLER_T208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ActionInstaller
struct  ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3  : public Installer_1_t7B0D6B74E152A6A77ECC9A94F10159F721157919
{
public:
	// System.Action`1<Zenject.DiContainer> Zenject.ActionInstaller::_installMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ____installMethod_1;

public:
	inline static int32_t get_offset_of__installMethod_1() { return static_cast<int32_t>(offsetof(ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3, ____installMethod_1)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get__installMethod_1() const { return ____installMethod_1; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of__installMethod_1() { return &____installMethod_1; }
	inline void set__installMethod_1(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		____installMethod_1 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIONINSTALLER_T208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef TYPE_T_H
#define TYPE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Type
struct  Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_t7B542280A22F0EC4EAC2061C29178845847A8B2D  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((&___FilterAttribute_0), value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((&___FilterName_1), value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t25C1BD92C42BE94426E300787C13C452CB89B381 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((&___FilterNameIgnoreCase_2), value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((&___Missing_3), value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyTypes_5), value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t4D5CB06963501D32847C057B57157D6DC49CA759 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultBinder_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_T_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef GAMEOBJECTCREATIONPARAMETERS_TA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_H
#define GAMEOBJECTCREATIONPARAMETERS_TA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectCreationParameters
struct  GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4  : public RuntimeObject
{
public:
	// System.String Zenject.GameObjectCreationParameters::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String Zenject.GameObjectCreationParameters::<GroupName>k__BackingField
	String_t* ___U3CGroupNameU3Ek__BackingField_1;
	// UnityEngine.Transform Zenject.GameObjectCreationParameters::<ParentTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CParentTransformU3Ek__BackingField_2;
	// System.Func`2<Zenject.InjectContext,UnityEngine.Transform> Zenject.GameObjectCreationParameters::<ParentTransformGetter>k__BackingField
	Func_2_t4ED720590592A8375BE8E36276393417F557F933 * ___U3CParentTransformGetterU3Ek__BackingField_3;
	// System.Nullable`1<UnityEngine.Vector3> Zenject.GameObjectCreationParameters::<Position>k__BackingField
	Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  ___U3CPositionU3Ek__BackingField_4;
	// System.Nullable`1<UnityEngine.Quaternion> Zenject.GameObjectCreationParameters::<Rotation>k__BackingField
	Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  ___U3CRotationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGroupNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CGroupNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CGroupNameU3Ek__BackingField_1() const { return ___U3CGroupNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CGroupNameU3Ek__BackingField_1() { return &___U3CGroupNameU3Ek__BackingField_1; }
	inline void set_U3CGroupNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CGroupNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CParentTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CParentTransformU3Ek__BackingField_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CParentTransformU3Ek__BackingField_2() const { return ___U3CParentTransformU3Ek__BackingField_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CParentTransformU3Ek__BackingField_2() { return &___U3CParentTransformU3Ek__BackingField_2; }
	inline void set_U3CParentTransformU3Ek__BackingField_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CParentTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentTransformU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParentTransformGetterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CParentTransformGetterU3Ek__BackingField_3)); }
	inline Func_2_t4ED720590592A8375BE8E36276393417F557F933 * get_U3CParentTransformGetterU3Ek__BackingField_3() const { return ___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline Func_2_t4ED720590592A8375BE8E36276393417F557F933 ** get_address_of_U3CParentTransformGetterU3Ek__BackingField_3() { return &___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline void set_U3CParentTransformGetterU3Ek__BackingField_3(Func_2_t4ED720590592A8375BE8E36276393417F557F933 * value)
	{
		___U3CParentTransformGetterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentTransformGetterU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CPositionU3Ek__BackingField_4)); }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  get_U3CPositionU3Ek__BackingField_4() const { return ___U3CPositionU3Ek__BackingField_4; }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 * get_address_of_U3CPositionU3Ek__BackingField_4() { return &___U3CPositionU3Ek__BackingField_4; }
	inline void set_U3CPositionU3Ek__BackingField_4(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  value)
	{
		___U3CPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CRotationU3Ek__BackingField_5)); }
	inline Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  get_U3CRotationU3Ek__BackingField_5() const { return ___U3CRotationU3Ek__BackingField_5; }
	inline Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41 * get_address_of_U3CRotationU3Ek__BackingField_5() { return &___U3CRotationU3Ek__BackingField_5; }
	inline void set_U3CRotationU3Ek__BackingField_5(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  value)
	{
		___U3CRotationU3Ek__BackingField_5 = value;
	}
};

struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_StaticFields
{
public:
	// Zenject.GameObjectCreationParameters Zenject.GameObjectCreationParameters::Default
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___Default_6;

public:
	inline static int32_t get_offset_of_Default_6() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_StaticFields, ___Default_6)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_Default_6() const { return ___Default_6; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_Default_6() { return &___Default_6; }
	inline void set_Default_6(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___Default_6 = value;
		Il2CppCodeGenWriteBarrier((&___Default_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCREATIONPARAMETERS_TA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_H
#ifndef ACTION_5_T5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376_H
#define ACTION_5_T5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.Action`5<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object>
struct  Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_5_T5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376_H
#ifndef ACTION_6_TA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E_H
#define ACTION_6_TA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ModestTree.Util.Action`6<Zenject.DiContainer,System.Object,System.Object,System.Object,System.Object,System.Object>
struct  Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_6_TA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E_H
#ifndef ACTION_1_T3DD18704E712DAA288588BFF42EDE09A39BC678A_H
#define ACTION_1_T3DD18704E712DAA288588BFF42EDE09A39BC678A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`1<Zenject.DiContainer>
struct  Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_1_T3DD18704E712DAA288588BFF42EDE09A39BC678A_H
#ifndef ACTION_4_TD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E_H
#define ACTION_4_TD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Action`4<Zenject.DiContainer,System.Object,System.Object,System.Object>
struct  Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_4_TD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E_H
#ifndef FUNC_2_T866D39A8460031F2EB12123A560587B20B412DAA_H
#define FUNC_2_T866D39A8460031F2EB12123A560587B20B412DAA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Boolean>
struct  Func_2_t866D39A8460031F2EB12123A560587B20B412DAA  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T866D39A8460031F2EB12123A560587B20B412DAA_H
#ifndef FUNC_2_T54DDB24A7270803EC8DB16E309DD7F50AE88A724_H
#define FUNC_2_T54DDB24A7270803EC8DB16E309DD7F50AE88A724_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Func`2<Zenject.TaskUpdater`1/TaskInfo<System.Object>,System.Object>
struct  Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNC_2_T54DDB24A7270803EC8DB16E309DD7F50AE88A724_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef CONTEXT_T3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7_H
#define CONTEXT_T3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.Context
struct  Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installers
	List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * ____installers_4;
	// System.Collections.Generic.List`1<Zenject.MonoInstaller> Zenject.Context::_installerPrefabs
	List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * ____installerPrefabs_5;
	// System.Collections.Generic.List`1<Zenject.ScriptableObjectInstaller> Zenject.Context::_scriptableObjectInstallers
	List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * ____scriptableObjectInstallers_6;
	// System.Collections.Generic.List`1<Zenject.InstallerBase> Zenject.Context::_normalInstallers
	List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 * ____normalInstallers_7;
	// System.Collections.Generic.List`1<System.Type> Zenject.Context::_normalInstallerTypes
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ____normalInstallerTypes_8;

public:
	inline static int32_t get_offset_of__installers_4() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____installers_4)); }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * get__installers_4() const { return ____installers_4; }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 ** get_address_of__installers_4() { return &____installers_4; }
	inline void set__installers_4(List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * value)
	{
		____installers_4 = value;
		Il2CppCodeGenWriteBarrier((&____installers_4), value);
	}

	inline static int32_t get_offset_of__installerPrefabs_5() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____installerPrefabs_5)); }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * get__installerPrefabs_5() const { return ____installerPrefabs_5; }
	inline List_1_tED6028C19769161B40D318A4663FA052CF5836D2 ** get_address_of__installerPrefabs_5() { return &____installerPrefabs_5; }
	inline void set__installerPrefabs_5(List_1_tED6028C19769161B40D318A4663FA052CF5836D2 * value)
	{
		____installerPrefabs_5 = value;
		Il2CppCodeGenWriteBarrier((&____installerPrefabs_5), value);
	}

	inline static int32_t get_offset_of__scriptableObjectInstallers_6() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____scriptableObjectInstallers_6)); }
	inline List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * get__scriptableObjectInstallers_6() const { return ____scriptableObjectInstallers_6; }
	inline List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 ** get_address_of__scriptableObjectInstallers_6() { return &____scriptableObjectInstallers_6; }
	inline void set__scriptableObjectInstallers_6(List_1_t1B8CAAF3CFCCF0FD5E59967DDE6B73EA71FFB1B2 * value)
	{
		____scriptableObjectInstallers_6 = value;
		Il2CppCodeGenWriteBarrier((&____scriptableObjectInstallers_6), value);
	}

	inline static int32_t get_offset_of__normalInstallers_7() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____normalInstallers_7)); }
	inline List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 * get__normalInstallers_7() const { return ____normalInstallers_7; }
	inline List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 ** get_address_of__normalInstallers_7() { return &____normalInstallers_7; }
	inline void set__normalInstallers_7(List_1_tF352DCA153489EBDF0DBAD93C7FB34C344303595 * value)
	{
		____normalInstallers_7 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallers_7), value);
	}

	inline static int32_t get_offset_of__normalInstallerTypes_8() { return static_cast<int32_t>(offsetof(Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7, ____normalInstallerTypes_8)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get__normalInstallerTypes_8() const { return ____normalInstallerTypes_8; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of__normalInstallerTypes_8() { return &____normalInstallerTypes_8; }
	inline void set__normalInstallerTypes_8(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		____normalInstallerTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&____normalInstallerTypes_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTEXT_T3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7_H
#ifndef RUNNABLECONTEXT_TCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_H
#define RUNNABLECONTEXT_TCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.RunnableContext
struct  RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2  : public Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7
{
public:
	// System.Boolean Zenject.RunnableContext::_autoRun
	bool ____autoRun_9;
	// System.Boolean Zenject.RunnableContext::<Initialized>k__BackingField
	bool ___U3CInitializedU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of__autoRun_9() { return static_cast<int32_t>(offsetof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2, ____autoRun_9)); }
	inline bool get__autoRun_9() const { return ____autoRun_9; }
	inline bool* get_address_of__autoRun_9() { return &____autoRun_9; }
	inline void set__autoRun_9(bool value)
	{
		____autoRun_9 = value;
	}

	inline static int32_t get_offset_of_U3CInitializedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2, ___U3CInitializedU3Ek__BackingField_11)); }
	inline bool get_U3CInitializedU3Ek__BackingField_11() const { return ___U3CInitializedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CInitializedU3Ek__BackingField_11() { return &___U3CInitializedU3Ek__BackingField_11; }
	inline void set_U3CInitializedU3Ek__BackingField_11(bool value)
	{
		___U3CInitializedU3Ek__BackingField_11 = value;
	}
};

struct RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_StaticFields
{
public:
	// System.Boolean Zenject.RunnableContext::_staticAutoRun
	bool ____staticAutoRun_10;

public:
	inline static int32_t get_offset_of__staticAutoRun_10() { return static_cast<int32_t>(offsetof(RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_StaticFields, ____staticAutoRun_10)); }
	inline bool get__staticAutoRun_10() const { return ____staticAutoRun_10; }
	inline bool* get_address_of__staticAutoRun_10() { return &____staticAutoRun_10; }
	inline void set__staticAutoRun_10(bool value)
	{
		____staticAutoRun_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNNABLECONTEXT_TCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2_H
#ifndef GAMEOBJECTCONTEXT_T83A87C55F6FA3BFBE6D04A3261653BE69685FD94_H
#define GAMEOBJECTCONTEXT_T83A87C55F6FA3BFBE6D04A3261653BE69685FD94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectContext
struct  GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94  : public RunnableContext_tCDD59FBC80C1A5DFF01761E2E76BC3E4660DE3C2
{
public:
	// System.Collections.Generic.List`1<System.Object> Zenject.GameObjectContext::_dependencyRoots
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____dependencyRoots_12;
	// Zenject.MonoKernel Zenject.GameObjectContext::_kernel
	MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF * ____kernel_13;
	// Zenject.DiContainer Zenject.GameObjectContext::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_14;

public:
	inline static int32_t get_offset_of__dependencyRoots_12() { return static_cast<int32_t>(offsetof(GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94, ____dependencyRoots_12)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__dependencyRoots_12() const { return ____dependencyRoots_12; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__dependencyRoots_12() { return &____dependencyRoots_12; }
	inline void set__dependencyRoots_12(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____dependencyRoots_12 = value;
		Il2CppCodeGenWriteBarrier((&____dependencyRoots_12), value);
	}

	inline static int32_t get_offset_of__kernel_13() { return static_cast<int32_t>(offsetof(GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94, ____kernel_13)); }
	inline MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF * get__kernel_13() const { return ____kernel_13; }
	inline MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF ** get_address_of__kernel_13() { return &____kernel_13; }
	inline void set__kernel_13(MonoKernel_t3D3BEA41127F91BE872C20F5CED9B5C105FE4DFF * value)
	{
		____kernel_13 = value;
		Il2CppCodeGenWriteBarrier((&____kernel_13), value);
	}

	inline static int32_t get_offset_of__container_14() { return static_cast<int32_t>(offsetof(GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94, ____container_14)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_14() const { return ____container_14; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_14() { return &____container_14; }
	inline void set__container_14(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_14 = value;
		Il2CppCodeGenWriteBarrier((&____container_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCONTEXT_T83A87C55F6FA3BFBE6D04A3261653BE69685FD94_H


// !0 System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, int32_t p0, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" IL2CPP_METHOD_ATTR int32_t List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared (List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * __this, const RuntimeMethod* method);
// System.Void System.Action`1<System.Object>::.ctor(System.Object,System.IntPtr)
extern "C" IL2CPP_METHOD_ATTR void Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared (Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method);

// System.Void System.Object::.ctor()
extern "C" IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// !0 System.Collections.Generic.List`1<Zenject.TypeValuePair>::get_Item(System.Int32)
inline TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8 (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * __this, int32_t p0, const RuntimeMethod* method)
{
	return ((  TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * (*) (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *, int32_t, const RuntimeMethod*))List_1_get_Item_mFDB8AD680C600072736579BBF5F38F7416396588_gshared)(__this, p0, method);
}
// System.Void Zenject.SubContainerCreatorByNewPrefabDynamicContext::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabDynamicContext__ctor_m451752261E8EDFA7C198E077D809235BE9280182 (SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___gameObjectBindInfo2, const RuntimeMethod* method);
// System.Int32 System.Collections.Generic.List`1<Zenject.TypeValuePair>::get_Count()
inline int32_t List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * __this, const RuntimeMethod* method)
{
	return ((  int32_t (*) (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *, const RuntimeMethod*))List_1_get_Count_m507C9149FF7F83AAC72C29091E745D557DA47D22_gshared)(__this, method);
}
// System.Void ModestTree.Assert::IsEqual(System.Object,System.Object)
extern "C" IL2CPP_METHOD_ATTR void Assert_IsEqual_mF249F73E32D5C10C1AE24C878851F98BC7CA27AE (RuntimeObject * ___left0, RuntimeObject * ___right1, const RuntimeMethod* method);
// System.Void ModestTree.Assert::That(System.Boolean)
extern "C" IL2CPP_METHOD_ATTR void Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6 (bool ___condition0, const RuntimeMethod* method);
// System.Void System.Action`1<Zenject.DiContainer>::.ctor(System.Object,System.IntPtr)
inline void Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968 (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * __this, RuntimeObject * p0, intptr_t p1, const RuntimeMethod* method)
{
	((  void (*) (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *, RuntimeObject *, intptr_t, const RuntimeMethod*))Action_1__ctor_mAFC7442D9D3CEC6701C3C5599F8CF12476095510_gshared)(__this, p0, p1, method);
}
// System.Void Zenject.ActionInstaller::.ctor(System.Action`1<Zenject.DiContainer>)
extern "C" IL2CPP_METHOD_ATTR void ActionInstaller__ctor_mA01C9AF7C311C0EA3C2FB97F354AFA268647F3BF (ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 * __this, Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ___installMethod0, const RuntimeMethod* method);
// System.Void Zenject.Context::AddNormalInstaller(Zenject.InstallerBase)
extern "C" IL2CPP_METHOD_ATTR void Context_AddNormalInstaller_m1E194815AD50F9211417043CDE093F676E8914A3 (Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 * __this, InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F * ___installer0, const RuntimeMethod* method);
// System.Type System.Object::GetType()
extern "C" IL2CPP_METHOD_ATTR Type_t * Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60 (RuntimeObject * __this, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mF4626905368D6558695A823466A1AF65EADB9923 (String_t* p0, String_t* p1, String_t* p2, const RuntimeMethod* method);
// System.Void ModestTree.Assert::That(System.Boolean,System.String)
extern "C" IL2CPP_METHOD_ATTR void Assert_That_m49CBA8D50781293964F2DE8BE4D3B47F877F0DE0 (bool ___condition0, String_t* ___message1, const RuntimeMethod* method);
// System.String System.String::Concat(System.String,System.String)
extern "C" IL2CPP_METHOD_ATTR String_t* String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE (String_t* p0, String_t* p1, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0<System.Object,System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mF0C7AABFABE09E85F1B10794D6345C615E617930_gshared (U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3/<>c__DisplayClass2_0<System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mB304DF70CC2A8B8945E7707215B6FD2D64DA8655_gshared (U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mB304DF70CC2A8B8945E7707215B6FD2D64DA8655_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 * L_0 = (SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E * L_1 = (Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E *)L_0->get__installerMethod_3();
		DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * L_2 = ___subContainer0;
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_3 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_3);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_4 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_4);
		RuntimeObject * L_5 = (RuntimeObject *)L_4->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_6 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_6);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_7 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_7);
		RuntimeObject * L_8 = (RuntimeObject *)L_7->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_9 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_9);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_10 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_10);
		RuntimeObject * L_11 = (RuntimeObject *)L_10->get_Value_1();
		NullCheck((Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E *)L_1);
		((  void (*) (Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E *, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E *)L_1, (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,System.Action`4<Zenject.DiContainer,TParam1,TParam2,TParam3>)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_3__ctor_m444A8E02618615AFF9E3F5C4BD2FED004496EC03_gshared (SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___gameObjectBindInfo2, Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m451752261E8EDFA7C198E077D809235BE9280182((SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 *)__this, (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 *)L_2, /*hidden argument*/NULL);
		Action_4_tD76E3EBEE5B7F3834C17C670E54D1CC92A434A9E * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`3<System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_3_AddInstallers_mAB9F3C7AE1977173FC4C8E70B4A05F83EBE694B2_gshared (SubContainerCreatorByNewPrefabMethod_3_t99BB0964B449F8CFB6F77F9FB03D13399D3C1E98 * __this, List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args0, GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_3_AddInstallers_mAB9F3C7AE1977173FC4C8E70B4A05F83EBE694B2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_0 = (U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 *)L_0;
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_2 = V_0;
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_5 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_4->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_5);
		int32_t L_6 = List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_5, /*hidden argument*/List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 3;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mF249F73E32D5C10C1AE24C878851F98BC7CA27AE((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_12 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_11->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_13 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_13);
		Type_t * L_14 = (Type_t *)L_13->get_Type_0();
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_17 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_16->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_17);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_18 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_18);
		Type_t * L_19 = (Type_t *)L_18->get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_22 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_21->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_22);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_23 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_23);
		Type_t * L_24 = (Type_t *)L_23->get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_25, /*hidden argument*/NULL);
		GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94 * L_26 = ___context1;
		U3CU3Ec__DisplayClass2_0_tAF8DCF78BAA89C71DA98B77B709B301DA177B116 * L_27 = V_0;
		Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * L_28 = (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *)il2cpp_codegen_object_new(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A_il2cpp_TypeInfo_var);
		Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968(L_28, (RuntimeObject *)L_27, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968_RuntimeMethod_var);
		ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 * L_29 = (ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 *)il2cpp_codegen_object_new(ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_mA01C9AF7C311C0EA3C2FB97F354AFA268647F3BF(L_29, (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *)L_28, /*hidden argument*/NULL);
		NullCheck((Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 *)L_26);
		Context_AddNormalInstaller_m1E194815AD50F9211417043CDE093F676E8914A3((Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 *)L_26, (InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F *)L_29, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_m24A62E963EAEE648F6D4990BC7730ABDF8836FF3_gshared (U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m3F0E32C6062FB1633DC7DD6F6D3F28970E850B36_gshared (U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_m3F0E32C6062FB1633DC7DD6F6D3F28970E850B36_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 * L_0 = (SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 * L_1 = (Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 *)L_0->get__installerMethod_3();
		DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * L_2 = ___subContainer0;
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_3 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_3);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_4 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_4);
		RuntimeObject * L_5 = (RuntimeObject *)L_4->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_6 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_6);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_7 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_7);
		RuntimeObject * L_8 = (RuntimeObject *)L_7->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_9 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_9);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_10 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_10);
		RuntimeObject * L_11 = (RuntimeObject *)L_10->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_12 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_13 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_13);
		RuntimeObject * L_14 = (RuntimeObject *)L_13->get_Value_1();
		NullCheck((Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 *)L_1);
		((  void (*) (Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 *, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 *)L_1, (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`5<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4>)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_4__ctor_m0D5D4D68B1B1EDA07C263AF9F80419C0700CE1F4_gshared (SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___gameObjectBindInfo2, Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m451752261E8EDFA7C198E077D809235BE9280182((SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 *)__this, (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 *)L_2, /*hidden argument*/NULL);
		Action_5_t5CA9D4A1E7C27ABD1DA2C4A5A6FD629D0C929376 * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`4<System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_4_AddInstallers_m9828A2F23798CE6551A53409A549664002BCC116_gshared (SubContainerCreatorByNewPrefabMethod_4_t65BB6C835823BCD50EFA80A6F36E0CB98A471604 * __this, List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args0, GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_4_AddInstallers_m9828A2F23798CE6551A53409A549664002BCC116_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_0 = (U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 *)L_0;
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_2 = V_0;
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_5 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_4->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_5);
		int32_t L_6 = List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_5, /*hidden argument*/List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 4;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mF249F73E32D5C10C1AE24C878851F98BC7CA27AE((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_12 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_11->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_13 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_13);
		Type_t * L_14 = (Type_t *)L_13->get_Type_0();
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_17 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_16->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_17);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_18 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_18);
		Type_t * L_19 = (Type_t *)L_18->get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_22 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_21->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_22);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_23 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_23);
		Type_t * L_24 = (Type_t *)L_23->get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_27 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_26->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_27);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_28 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_28);
		Type_t * L_29 = (Type_t *)L_28->get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_30, /*hidden argument*/NULL);
		GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94 * L_31 = ___context1;
		U3CU3Ec__DisplayClass2_0_tA1D56E22F3C877086C887309B527C11A8C8EA228 * L_32 = V_0;
		Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * L_33 = (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *)il2cpp_codegen_object_new(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A_il2cpp_TypeInfo_var);
		Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968(L_33, (RuntimeObject *)L_32, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)), /*hidden argument*/Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968_RuntimeMethod_var);
		ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 * L_34 = (ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 *)il2cpp_codegen_object_new(ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_mA01C9AF7C311C0EA3C2FB97F354AFA268647F3BF(L_34, (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *)L_33, /*hidden argument*/NULL);
		NullCheck((Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 *)L_31);
		Context_AddNormalInstaller_m1E194815AD50F9211417043CDE093F676E8914A3((Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 *)L_31, (InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F *)L_34, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0__ctor_mCECE361B6C9DEC4A7AADB9D0637919A05BE6AAC3_gshared (U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5/<>c__DisplayClass2_0<System.Object,System.Object,System.Object,System.Object,System.Object>::<AddInstallers>b__0(Zenject.DiContainer)
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mE590FC7D03145BDD8A2184F07A1B0FD98847F70E_gshared (U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___subContainer0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (U3CU3Ec__DisplayClass2_0_U3CAddInstallersU3Eb__0_mE590FC7D03145BDD8A2184F07A1B0FD98847F70E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD * L_0 = (SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD *)__this->get_U3CU3E4__this_0();
		NullCheck(L_0);
		Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E * L_1 = (Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E *)L_0->get__installerMethod_3();
		DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * L_2 = ___subContainer0;
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_3 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_3);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_4 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_3, (int32_t)0, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_4);
		RuntimeObject * L_5 = (RuntimeObject *)L_4->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_6 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_6);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_7 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_6, (int32_t)1, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_7);
		RuntimeObject * L_8 = (RuntimeObject *)L_7->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_9 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_9);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_10 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_9, (int32_t)2, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_10);
		RuntimeObject * L_11 = (RuntimeObject *)L_10->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_12 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_13 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12, (int32_t)3, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_13);
		RuntimeObject * L_14 = (RuntimeObject *)L_13->get_Value_1();
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_15 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)__this->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_15);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_16 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_15, (int32_t)4, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_16);
		RuntimeObject * L_17 = (RuntimeObject *)L_16->get_Value_1();
		NullCheck((Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E *)L_1);
		((  void (*) (Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E *, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E *)L_1, (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *)L_2, (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_5, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_8, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 1))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_11, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 2))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_14, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 3))), (RuntimeObject *)((RuntimeObject *)Castclass((RuntimeObject*)L_17, IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4))), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::.ctor(Zenject.DiContainer,Zenject.IPrefabProvider,Zenject.GameObjectCreationParameters,ModestTree.Util.Action`6<Zenject.DiContainer,TParam1,TParam2,TParam3,TParam4,TParam5>)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_5__ctor_m58A8094CEB7D2151D8ECBA779223768B749D1EA8_gshared (SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD * __this, DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container0, RuntimeObject* ___prefabProvider1, GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___gameObjectBindInfo2, Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E * ___installerMethod3, const RuntimeMethod* method)
{
	{
		DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * L_0 = ___container0;
		RuntimeObject* L_1 = ___prefabProvider1;
		GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * L_2 = ___gameObjectBindInfo2;
		NullCheck((SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 *)__this);
		SubContainerCreatorByNewPrefabDynamicContext__ctor_m451752261E8EDFA7C198E077D809235BE9280182((SubContainerCreatorByNewPrefabDynamicContext_tC409E09A3C80E147D7A66BF53D5E858F2049E167 *)__this, (DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 *)L_0, (RuntimeObject*)L_1, (GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 *)L_2, /*hidden argument*/NULL);
		Action_6_tA2DC4DD008D7D7743BB602F8D5CBAD7FE43DDA6E * L_3 = ___installerMethod3;
		__this->set__installerMethod_3(L_3);
		return;
	}
}
// System.Void Zenject.SubContainerCreatorByNewPrefabMethod`5<System.Object,System.Object,System.Object,System.Object,System.Object>::AddInstallers(System.Collections.Generic.List`1<Zenject.TypeValuePair>,Zenject.GameObjectContext)
extern "C" IL2CPP_METHOD_ATTR void SubContainerCreatorByNewPrefabMethod_5_AddInstallers_m838FB805DAC17A8AB2C6311738EA1FD0381A0A55_gshared (SubContainerCreatorByNewPrefabMethod_5_t276E95C2DA0145CCF066CE7D69B1AD9B3F88AACD * __this, List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args0, GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94 * ___context1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SubContainerCreatorByNewPrefabMethod_5_AddInstallers_m838FB805DAC17A8AB2C6311738EA1FD0381A0A55_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_0 = (U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 0));
		((  void (*) (U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		V_0 = (U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 *)L_0;
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_1 = V_0;
		NullCheck(L_1);
		L_1->set_U3CU3E4__this_0(__this);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_2 = V_0;
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_3 = ___args0;
		NullCheck(L_2);
		L_2->set_args_1(L_3);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_4 = V_0;
		NullCheck(L_4);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_5 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_4->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_5);
		int32_t L_6 = List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_5, /*hidden argument*/List_1_get_Count_m106F6E2541FA5B78C04EDB3D6717F1972DD251CC_RuntimeMethod_var);
		int32_t L_7 = L_6;
		RuntimeObject * L_8 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_7);
		int32_t L_9 = 5;
		RuntimeObject * L_10 = Box(Int32_t585191389E07734F19F3156FF88FB3EF4800D102_il2cpp_TypeInfo_var, &L_9);
		Assert_IsEqual_mF249F73E32D5C10C1AE24C878851F98BC7CA27AE((RuntimeObject *)L_8, (RuntimeObject *)L_10, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_11 = V_0;
		NullCheck(L_11);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_12 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_11->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_13 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_12, (int32_t)0, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_13);
		Type_t * L_14 = (Type_t *)L_13->get_Type_0();
		bool L_15 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((Type_t *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_15, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_16 = V_0;
		NullCheck(L_16);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_17 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_16->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_17);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_18 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_17, (int32_t)1, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_18);
		Type_t * L_19 = (Type_t *)L_18->get_Type_0();
		bool L_20 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((Type_t *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_20, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_21 = V_0;
		NullCheck(L_21);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_22 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_21->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_22);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_23 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_22, (int32_t)2, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_23);
		Type_t * L_24 = (Type_t *)L_23->get_Type_0();
		bool L_25 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4)->methodPointer)((Type_t *)L_24, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 4));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_25, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_26 = V_0;
		NullCheck(L_26);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_27 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_26->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_27);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_28 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_27, (int32_t)3, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_28);
		Type_t * L_29 = (Type_t *)L_28->get_Type_0();
		bool L_30 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)->methodPointer)((Type_t *)L_29, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_30, /*hidden argument*/NULL);
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_31 = V_0;
		NullCheck(L_31);
		List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * L_32 = (List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_31->get_args_1();
		NullCheck((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_32);
		TypeValuePair_t4D0A272F00CA376E6BF0C9D5391DCCEFC7DBB21B * L_33 = List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8((List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 *)L_32, (int32_t)4, /*hidden argument*/List_1_get_Item_mE517E6AF183B8933C39CCC67C172103180DECCC8_RuntimeMethod_var);
		NullCheck(L_33);
		Type_t * L_34 = (Type_t *)L_33->get_Type_0();
		bool L_35 = ((  bool (*) (Type_t *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6)->methodPointer)((Type_t *)L_34, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 6));
		Assert_That_m2C499A4D1BEB56CF1C109A89D9E6D00AACA36EE6((bool)L_35, /*hidden argument*/NULL);
		GameObjectContext_t83A87C55F6FA3BFBE6D04A3261653BE69685FD94 * L_36 = ___context1;
		U3CU3Ec__DisplayClass2_0_t5EE6244949A29B790BA1F15BC7B90541CE36B2F7 * L_37 = V_0;
		Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * L_38 = (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *)il2cpp_codegen_object_new(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A_il2cpp_TypeInfo_var);
		Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968(L_38, (RuntimeObject *)L_37, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)), /*hidden argument*/Action_1__ctor_m97256909182C01E7166471CFE90CF42BA478D968_RuntimeMethod_var);
		ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 * L_39 = (ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3 *)il2cpp_codegen_object_new(ActionInstaller_t208DD56E3EF89CE3D74246CBDC1E3F9C002086D3_il2cpp_TypeInfo_var);
		ActionInstaller__ctor_mA01C9AF7C311C0EA3C2FB97F354AFA268647F3BF(L_39, (Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A *)L_38, /*hidden argument*/NULL);
		NullCheck((Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 *)L_36);
		Context_AddNormalInstaller_m1E194815AD50F9211417043CDE093F676E8914A3((Context_t3D067A0DD2B83CDE9A211388F4AB4B806F78C7B7 *)L_36, (InstallerBase_tAD3E3395B906666822EB47EAC578547EEB7DC56F *)L_39, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.TaskUpdater`1/<>c<System.Object>::.cctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__cctor_mAAD041B1C8DB671672AADA90BE690425EF1E735C_gshared (const RuntimeMethod* method)
{
	{
		U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * L_0 = (U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 0));
		((  void (*) (U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(InitializedTypeInfo(method->klass)->rgctx_data, 1));
		((U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(InitializedTypeInfo(method->klass)->rgctx_data, 2)))->set_U3CU3E9_0(L_0);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1/<>c<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__ctor_m66DB04F1EDF2DEF1DDC4A6B493314FF55A76A953_gshared (U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// TTask Zenject.TaskUpdater`1/<>c<System.Object>::<AddTaskInternal>b__7_0(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C" IL2CPP_METHOD_ATTR RuntimeObject * U3CU3Ec_U3CAddTaskInternalU3Eb__7_0_m0D4FA477BDE1A4453AFC369A6636301BD9259579_gshared (U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * __this, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * ___x0, const RuntimeMethod* method)
{
	{
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_0 = ___x0;
		NullCheck(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_Task_0();
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.TaskUpdater`1/<>c__DisplayClass8_0<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void U3CU3Ec__DisplayClass8_0__ctor_m9A8708F54F7BE76058A71E8C4E3C0284A6826387_gshared (U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean Zenject.TaskUpdater`1/<>c__DisplayClass8_0<System.Object>::<RemoveTask>b__0(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C" IL2CPP_METHOD_ATTR bool U3CU3Ec__DisplayClass8_0_U3CRemoveTaskU3Eb__0_m7C25299784ABFEC5361A979A3D78CFFD8D9D4651_gshared (U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * __this, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * ___x0, const RuntimeMethod* method)
{
	{
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_0 = ___x0;
		NullCheck(L_0);
		RuntimeObject * L_1 = (RuntimeObject *)L_0->get_Task_0();
		RuntimeObject * L_2 = (RuntimeObject *)__this->get_task_0();
		return (bool)((((RuntimeObject*)(RuntimeObject *)L_1) == ((RuntimeObject*)(RuntimeObject *)L_2))? 1 : 0);
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Zenject.TaskUpdater`1/TaskInfo<System.Object>::.ctor(TTask,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TaskInfo__ctor_m70215FA992976B5668EDFFC5F07571A698AE071C_gshared (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * __this, RuntimeObject * ___task0, int32_t ___priority1, const RuntimeMethod* method)
{
	{
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		RuntimeObject * L_0 = ___task0;
		__this->set_Task_0(L_0);
		int32_t L_1 = ___priority1;
		__this->set_Priority_1(L_1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_AllTasks()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TaskUpdater_1_get_AllTasks_mA4BDB3F764E584B55DF5960C1A9AF170798C9979_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 0));
		List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * L_1 = (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)__this->get__queuedTasks_1();
		RuntimeObject* L_2 = ((  RuntimeObject* (*) (RuntimeObject*, RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1)->methodPointer)((RuntimeObject*)L_0, (RuntimeObject*)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 1));
		return L_2;
	}
}
// System.Collections.Generic.IEnumerable`1<Zenject.TaskUpdater`1/TaskInfo<TTask>> Zenject.TaskUpdater`1<System.Object>::get_ActiveTasks()
extern "C" IL2CPP_METHOD_ATTR RuntimeObject* TaskUpdater_1_get_ActiveTasks_m15CE0EE21BC5A90795ABB37435622C0C5202C927_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, const RuntimeMethod* method)
{
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_0 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)__this->get__tasks_0();
		return L_0;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTask(TTask,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_AddTask_mBAF88C824071DEFE4D790F3DA77009F27A51BE5C_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, RuntimeObject * ___task0, int32_t ___priority1, const RuntimeMethod* method)
{
	{
		RuntimeObject * L_0 = ___task0;
		int32_t L_1 = ___priority1;
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		((  void (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, (RuntimeObject *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 2));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddTaskInternal(TTask,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_AddTaskInternal_m6E6872801E66FCE21D7831606B8BA2C3770EC321_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, RuntimeObject * ___task0, int32_t ___priority1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_AddTaskInternal_m6E6872801E66FCE21D7831606B8BA2C3770EC321_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * G_B2_0 = NULL;
	RuntimeObject* G_B2_1 = NULL;
	Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * G_B1_0 = NULL;
	RuntimeObject* G_B1_1 = NULL;
	{
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		RuntimeObject* L_0 = ((  RuntimeObject* (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * L_1 = ((U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4)))->get_U3CU3E9__7_0_1();
		Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * L_2 = (Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 *)L_1;
		G_B1_0 = L_2;
		G_B1_1 = L_0;
		if (L_2)
		{
			G_B2_0 = L_2;
			G_B2_1 = L_0;
			goto IL_0025;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4));
		U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C * L_3 = ((U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4)))->get_U3CU3E9_0();
		Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * L_4 = (Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 6));
		((  void (*) (Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7)->methodPointer)(L_4, (RuntimeObject *)L_3, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 5)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 7));
		Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 * L_5 = (Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 *)L_4;
		((U3CU3Ec_t26D2BE66C20F579F4BE2152DA3FBF4CE9119B36C_StaticFields*)il2cpp_codegen_static_fields_for(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 4)))->set_U3CU3E9__7_0_1(L_5);
		G_B2_0 = L_5;
		G_B2_1 = G_B1_1;
	}

IL_0025:
	{
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8)->methodPointer)((RuntimeObject*)G_B2_1, (Func_2_t54DDB24A7270803EC8DB16E309DD7F50AE88A724 *)G_B2_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 8));
		RuntimeObject * L_7 = ___task0;
		bool L_8 = ((  bool (*) (RuntimeObject*, RuntimeObject *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9)->methodPointer)((RuntimeObject*)L_6, (RuntimeObject *)L_7, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 9));
		NullCheck((RuntimeObject *)(___task0));
		Type_t * L_9 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60((RuntimeObject *)(___task0), /*hidden argument*/NULL);
		NullCheck((Type_t *)L_9);
		String_t* L_10 = VirtFuncInvoker0< String_t* >::Invoke(27 /* System.String System.Type::get_FullName() */, (Type_t *)L_9);
		String_t* L_11 = String_Concat_mF4626905368D6558695A823466A1AF65EADB9923((String_t*)_stringLiteral25458F856CEB4BFBD8F8627119406BC1F8394D8E, (String_t*)L_10, (String_t*)_stringLiteralBB589D0621E5472F470FA3425A234C74B1E202E8, /*hidden argument*/NULL);
		Assert_That_m49CBA8D50781293964F2DE8BE4D3B47F877F0DE0((bool)((((int32_t)L_8) == ((int32_t)0))? 1 : 0), (String_t*)L_11, /*hidden argument*/NULL);
		List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * L_12 = (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)__this->get__queuedTasks_1();
		RuntimeObject * L_13 = ___task0;
		int32_t L_14 = ___priority1;
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_15 = (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 11));
		((  void (*) (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *, RuntimeObject *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12)->methodPointer)(L_15, (RuntimeObject *)L_13, (int32_t)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 12));
		NullCheck((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_12);
		((  void (*) (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13)->methodPointer)((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_12, (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_15, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 13));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::RemoveTask(TTask)
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_RemoveTask_mBC9BFA51A7EB4AB0F0B0A9FB1C505E63C1EF3082_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, RuntimeObject * ___task0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TaskUpdater_1_RemoveTask_mBC9BFA51A7EB4AB0F0B0A9FB1C505E63C1EF3082_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * V_0 = NULL;
	{
		U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * L_0 = (U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 14));
		((  void (*) (U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 15));
		V_0 = (U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 *)L_0;
		U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * L_1 = V_0;
		RuntimeObject * L_2 = ___task0;
		NullCheck(L_1);
		L_1->set_task_0(L_2);
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		RuntimeObject* L_3 = ((  RuntimeObject* (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 3));
		U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * L_4 = V_0;
		Func_2_t866D39A8460031F2EB12123A560587B20B412DAA * L_5 = (Func_2_t866D39A8460031F2EB12123A560587B20B412DAA *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 17));
		((  void (*) (Func_2_t866D39A8460031F2EB12123A560587B20B412DAA *, RuntimeObject *, intptr_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18)->methodPointer)(L_5, (RuntimeObject *)L_4, (intptr_t)((intptr_t)IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 16)), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 18));
		RuntimeObject* L_6 = ((  RuntimeObject* (*) (RuntimeObject*, Func_2_t866D39A8460031F2EB12123A560587B20B412DAA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19)->methodPointer)((RuntimeObject*)L_3, (Func_2_t866D39A8460031F2EB12123A560587B20B412DAA *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 19));
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_7 = ((  TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * (*) (RuntimeObject*, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20)->methodPointer)((RuntimeObject*)L_6, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 20));
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_8 = (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_7;
		NullCheck(L_8);
		bool L_9 = (bool)L_8->get_IsRemoved_2();
		U3CU3Ec__DisplayClass8_0_tA70D285BF0171219D582B6433359C45672A04383 * L_10 = V_0;
		NullCheck(L_10);
		RuntimeObject ** L_11 = (RuntimeObject **)L_10->get_address_of_task_0();
		NullCheck((RuntimeObject *)(*L_11));
		Type_t * L_12 = Object_GetType_m2E0B62414ECCAA3094B703790CE88CBB2F83EA60((RuntimeObject *)(*L_11), /*hidden argument*/NULL);
		NullCheck((MemberInfo_t *)L_12);
		String_t* L_13 = VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, (MemberInfo_t *)L_12);
		String_t* L_14 = String_Concat_mB78D0094592718DA6D5DB6C712A9C225631666BE((String_t*)_stringLiteral9C9C4ECE5F3F5332F9E4B20785827F8F41DEDADB, (String_t*)L_13, /*hidden argument*/NULL);
		Assert_That_m49CBA8D50781293964F2DE8BE4D3B47F877F0DE0((bool)((((int32_t)L_9) == ((int32_t)0))? 1 : 0), (String_t*)L_14, /*hidden argument*/NULL);
		NullCheck(L_8);
		L_8->set_IsRemoved_2((bool)1);
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::OnFrameStart()
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_OnFrameStart_m85FA44AD3CE3525A6345C96274D75584E17C9484_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		((  void (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 21));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateAll()
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_UpdateAll_m2BDBC902C43ACF4D88B89B6EF93FF654ACFE6D8F_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, const RuntimeMethod* method)
{
	{
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		((  void (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, int32_t, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, (int32_t)((int32_t)-2147483648LL), (int32_t)((int32_t)2147483647LL), /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 22));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::UpdateRange(System.Int32,System.Int32)
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_UpdateRange_m1B13B8ACA59DE1AA41C14109285F795F5E40892B_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, int32_t ___minPriority0, int32_t ___maxPriority1, const RuntimeMethod* method)
{
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * V_0 = NULL;
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * V_1 = NULL;
	TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * V_2 = NULL;
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_0 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)__this->get__tasks_0();
		NullCheck((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_0);
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_1 = ((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_1;
		goto IL_004c;
	}

IL_000e:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_2 = V_0;
		NullCheck((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_2);
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_3 = ((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		V_1 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_3;
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_4 = V_0;
		NullCheck((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_4);
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_5 = ((  TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * (*) (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		V_2 = (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_5;
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_6 = V_2;
		NullCheck(L_6);
		bool L_7 = (bool)L_6->get_IsRemoved_2();
		if (L_7)
		{
			goto IL_004a;
		}
	}
	{
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_8 = V_2;
		NullCheck(L_8);
		int32_t L_9 = (int32_t)L_8->get_Priority_1();
		int32_t L_10 = ___minPriority0;
		if ((((int32_t)L_9) < ((int32_t)L_10)))
		{
			goto IL_004a;
		}
	}
	{
		int32_t L_11 = ___maxPriority1;
		if ((((int32_t)L_11) == ((int32_t)((int32_t)2147483647LL))))
		{
			goto IL_003e;
		}
	}
	{
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_12 = V_2;
		NullCheck(L_12);
		int32_t L_13 = (int32_t)L_12->get_Priority_1();
		int32_t L_14 = ___maxPriority1;
		if ((((int32_t)L_13) >= ((int32_t)L_14)))
		{
			goto IL_004a;
		}
	}

IL_003e:
	{
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_15 = V_2;
		NullCheck(L_15);
		RuntimeObject * L_16 = (RuntimeObject *)L_15->get_Task_0();
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		VirtActionInvoker1< RuntimeObject * >::Invoke(4 /* System.Void Zenject.TaskUpdater`1<System.Object>::UpdateItem(TTask) */, (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, (RuntimeObject *)L_16);
	}

IL_004a:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_17 = V_1;
		V_0 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_17;
	}

IL_004c:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_18 = V_0;
		if (L_18)
		{
			goto IL_000e;
		}
	}
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_19 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)__this->get__tasks_0();
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		((  void (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_19, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 27));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::ClearRemovedTasks(System.Collections.Generic.LinkedList`1<Zenject.TaskUpdater`1/TaskInfo<TTask>>)
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_ClearRemovedTasks_mB69CDC58AFA56E14146D66BACDC559E233CAB813_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * ___tasks0, const RuntimeMethod* method)
{
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * V_0 = NULL;
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * G_B3_0 = NULL;
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * G_B2_0 = NULL;
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_0 = ___tasks0;
		NullCheck((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_0);
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_1 = ((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_1;
		goto IL_0024;
	}

IL_0009:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_2 = V_0;
		NullCheck((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_2);
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_3 = ((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_4 = V_0;
		NullCheck((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_4);
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_5 = ((  TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * (*) (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_4, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		NullCheck(L_5);
		bool L_6 = (bool)L_5->get_IsRemoved_2();
		G_B2_0 = L_3;
		if (!L_6)
		{
			G_B3_0 = L_3;
			goto IL_0023;
		}
	}
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_7 = ___tasks0;
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_8 = V_0;
		NullCheck((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_7);
		((  void (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28)->methodPointer)((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_7, (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 28));
		G_B3_0 = G_B2_0;
	}

IL_0023:
	{
		V_0 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)G_B3_0;
	}

IL_0024:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_9 = V_0;
		if (L_9)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::AddQueuedTasks()
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_AddQueuedTasks_mF94486781C4F1C367997A01CDEF265A4802C8AD8_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * V_1 = NULL;
	{
		V_0 = (int32_t)0;
		goto IL_0024;
	}

IL_0004:
	{
		List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * L_0 = (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)__this->get__queuedTasks_1();
		int32_t L_1 = V_0;
		NullCheck((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_0);
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_2 = ((  TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * (*) (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *, int32_t, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29)->methodPointer)((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_0, (int32_t)L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 29));
		V_1 = (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_2;
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_3 = V_1;
		NullCheck(L_3);
		bool L_4 = (bool)L_3->get_IsRemoved_2();
		if (L_4)
		{
			goto IL_0020;
		}
	}
	{
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_5 = V_1;
		NullCheck((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this);
		((  void (*) (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30)->methodPointer)((TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 *)__this, (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_5, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 30));
	}

IL_0020:
	{
		int32_t L_6 = V_0;
		V_0 = (int32_t)((int32_t)il2cpp_codegen_add((int32_t)L_6, (int32_t)1));
	}

IL_0024:
	{
		int32_t L_7 = V_0;
		List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * L_8 = (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)__this->get__queuedTasks_1();
		NullCheck((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_8);
		int32_t L_9 = ((  int32_t (*) (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31)->methodPointer)((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_8, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 31));
		if ((((int32_t)L_7) < ((int32_t)L_9)))
		{
			goto IL_0004;
		}
	}
	{
		List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * L_10 = (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)__this->get__queuedTasks_1();
		NullCheck((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_10);
		((  void (*) (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32)->methodPointer)((List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 32));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::InsertTaskSorted(Zenject.TaskUpdater`1/TaskInfo<TTask>)
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1_InsertTaskSorted_mB125EBD1435815711D4E85CD5409636246C2226C_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * ___task0, const RuntimeMethod* method)
{
	LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * V_0 = NULL;
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_0 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)__this->get__tasks_0();
		NullCheck((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_0);
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_1 = ((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23)->methodPointer)((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 23));
		V_0 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_1;
		goto IL_0037;
	}

IL_000e:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_2 = V_0;
		NullCheck((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_2);
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_3 = ((  TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * (*) (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25)->methodPointer)((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_2, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 25));
		NullCheck(L_3);
		int32_t L_4 = (int32_t)L_3->get_Priority_1();
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_5 = ___task0;
		NullCheck(L_5);
		int32_t L_6 = (int32_t)L_5->get_Priority_1();
		if ((((int32_t)L_4) <= ((int32_t)L_6)))
		{
			goto IL_0030;
		}
	}
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_7 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)__this->get__tasks_0();
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_8 = V_0;
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_9 = ___task0;
		NullCheck((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_7);
		((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33)->methodPointer)((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_7, (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_8, (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_9, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 33));
		return;
	}

IL_0030:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_10 = V_0;
		NullCheck((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_10);
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_11 = ((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24)->methodPointer)((LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_10, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 24));
		V_0 = (LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA *)L_11;
	}

IL_0037:
	{
		LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * L_12 = V_0;
		if (L_12)
		{
			goto IL_000e;
		}
	}
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_13 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)__this->get__tasks_0();
		TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED * L_14 = ___task0;
		NullCheck((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_13);
		((  LinkedListNode_1_t5ADC9D5B9607CB680FD8AC441F7C7E49B7F320EA * (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34)->methodPointer)((LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)L_13, (TaskInfo_t7C162B3928420C19773BBA3A44C79FCAB82232ED *)L_14, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 34));
		return;
	}
}
// System.Void Zenject.TaskUpdater`1<System.Object>::.ctor()
extern "C" IL2CPP_METHOD_ATTR void TaskUpdater_1__ctor_m49240EE37C262E6EC9E05141AB1F6A16F30A42B6_gshared (TaskUpdater_1_tD6EF0E920EDE4720A490536F92E1B3E4DBD67C22 * __this, const RuntimeMethod* method)
{
	{
		LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 * L_0 = (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 35));
		((  void (*) (LinkedList_1_t89AC263A7FE9EAA4CCCAA082F939484AB926CC04 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 36)->methodPointer)(L_0, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 36));
		__this->set__tasks_0(L_0);
		List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 * L_1 = (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *)il2cpp_codegen_object_new(IL2CPP_RGCTX_DATA(method->klass->rgctx_data, 37));
		((  void (*) (List_1_t825D392F76C74F64539B9A7837BB72D0215D2744 *, const RuntimeMethod*))IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38)->methodPointer)(L_1, /*hidden argument*/IL2CPP_RGCTX_METHOD_INFO(method->klass->rgctx_data, 38));
		__this->set__queuedTasks_1(L_1);
		NullCheck((RuntimeObject *)__this);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0((RuntimeObject *)__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
