﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Assets.ContentPrefabs
struct ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0;
// Assets._Project.Scripts.Common.Entities.ScriptLineInfoData[]
struct ScriptLineInfoDataU5BU5D_t34E739382D94242A2832DD8254FE9137F64A64DB;
// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector
struct MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3;
// Assets._Project.Scripts.Common.SceneControllers.GameSceneInit
struct GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E;
// Assets._Project.Scripts.Common.Scripting.API.ICustomMenu
struct ICustomMenu_tF20A4D033B5A006D930F4CE89B5DC6050A7ED7E2;
// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo
struct HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A;
// Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler
struct ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924;
// Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader
struct QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8;
// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler
struct ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC;
// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_0
struct U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D;
// Assets._Project.Scripts.Common.UI.ScriptCanvas
struct ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634;
// BarcodeScanner.IScanner
struct IScanner_tCDB666A914BDE195F61772CB9DB0B84E7C67D001;
// Jint.Engine
struct Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D;
// Jint.Native.JsValue
struct JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19;
// Jint.Native.JsValue[]
struct JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88;
// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader
struct IPlatformDependentAssetLoader_t4191A469DA1A9ECC9F8F2F7E37DAF82A3B2DBB2E;
// MPAR.Common.Collections.ObjectCollection
struct ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB;
// MPAR.Common.GameObjects.Factories.CanvasFactory
struct CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E;
// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory
struct ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13;
// MPAR.Common.GameObjects.Factories.InGameConsoleFactory
struct InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF;
// MPAR.Common.GameObjects.Factories.InScriptImageMenuItemFactory
struct InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831;
// MPAR.Common.GameObjects.Factories.InScriptTextMenuItemFactory
struct InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632;
// MPAR.Common.GameObjects.Factories.PlaneGeneratorFactory
struct PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D;
// MPAR.Common.GameObjects.Factories.PointCloudParticlesFactory
struct PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6;
// MPAR.Common.GameObjects.Factories.ScriptMenuItemFactory
struct ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3;
// MPAR.Common.Input.Utility.GestureManager
struct GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909;
// MPAR.Common.Persistence.Anchor
struct Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A;
// MPAR.Common.Persistence.CacheManager
struct CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B;
// MPAR.Common.Persistence.ISaveLoadManager
struct ISaveLoadManager_tFCCBBA0EEB7591B57A7AA74ED9EF18A0073086B5;
// MPAR.Common.Scripting.API.Objects.MixiplyObject
struct MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B;
// MPAR.Common.Scripting.Script
struct Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC;
// MPAR.Common.Scripting.ScriptManager
struct ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C;
// MPAR.Common.Scripting.Web.WebRequestBuilder
struct WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7;
// MPAR.Common.UI.IScriptMenuItem
struct IScriptMenuItem_tDFAC9B175B479F69EC904638EDD3EEE8C75ECF27;
// MPAR.Desktop.Input.DesktopInput
struct DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48;
// MPAR.Desktop.Persistence.BaseSaveLoadManager
struct BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB;
// MPAR.Desktop.UI.DesktopScriptMenu
struct DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0;
// MPAR.Desktop.UI.DesktopScriptMenuItem
struct DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9;
// MPAR.Desktop.UI.DesktopScriptMenuItem/<>c__DisplayClass15_0
struct U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512;
// PlaneDetectionController
struct PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<Assets._Project.Scripts.Common.UI.TabSwipeManager/SwipeDirection>
struct Action_1_t636307F25CD6F89E930F8A340F1FB43974250E58;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<MPAR.Common.Scripting.API.Objects.MixiplyObject,Jint.Native.JsValue>
struct Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550;
// System.Collections.Generic.Dictionary`2<System.Guid,Assets._Project.Scripts.Common.Scripting.Behaviours.MxBehaviour>
struct Dictionary_2_t4F245103ADEAA13DF8D05B40D4381D70EF8E9431;
// System.Collections.Generic.Dictionary`2<System.String,AssetBundles.LoadedAssetBundle>
struct Dictionary_2_t423F9AC35E72449025C6C437DF2B805B95B1E4C0;
// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Jint.Native.JsValue>>
struct Dictionary_2_t8EA4CA008CF6EF1794BF92CFA606FDCA3DF6ABDB;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>>
struct Dictionary_2_t136B03A279E7BC961938C204EAC598835C08E560;
// System.Collections.Generic.Dictionary`2<System.String,System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject>>
struct Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909;
// System.Collections.Generic.Dictionary`2<System.String,System.Object[]>
struct Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0;
// System.Collections.Generic.Dictionary`2<System.String,System.String>
struct Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC;
// System.Collections.Generic.Dictionary`2<System.String,System.String[]>
struct Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072;
// System.Collections.Generic.Dictionary`2<System.String,System.Type>
struct Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject>
struct Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material>
struct Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC;
// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.WWW>
struct Dictionary_2_tBA261CA38D4514CCDC4740A3A55FFF31DDDB9B9E;
// System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.XR.TrackableId,MPAR.Common.PlaneDetection.Plane>
struct Dictionary_2_t0E2365C5C5573E791ACD987D4CFEEA8778FE2C6A;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.Boolean>
struct Dictionary_2_tCFF0B4C05676E7570538CD56789338F5BC7FAF41;
// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color>
struct Dictionary_2_t1108955CD85FB42D96647EEDD618002FB5F4EF0F;
// System.Collections.Generic.List`1<AssetBundles.AssetBundleLoadOperation>
struct List_1_t1B89723079ECBCFC2B8933A3A4EC51797BF343F3;
// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.InGameConsole/Log>
struct List_1_t13A4D277A972555D2D69C11B8C7C37B220CF6A15;
// System.Collections.Generic.List`1<MPAR.Common.Input.IPlatformDependentInputListener>
struct List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C;
// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script>
struct List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1;
// System.Collections.Generic.List`1<System.Single>
struct List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.GameObject>
struct List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0;
// System.Func`2<MPAR.Common.GameObjects.ExhibitBox,MPAR.Common.Entities.HologramData>
struct Func_2_tE2AAA0D7FBA4B1D0FA95518D48F76AB4FF2FDE51;
// System.Func`2<MPAR.Common.Scripting.API.Assets.ParameterDefinition,System.Boolean>
struct Func_2_tCBC6468A0955C6F2D90E115A4CCD791605A4C3FF;
// System.Func`2<MPAR.Common.Scripting.Script,System.Boolean>
struct Func_2_t953C24A29A3DD69F4675555FA28FF262C040B25D;
// System.Func`2<MPAR.Common.Scripting.Script,System.DateTime>
struct Func_2_tD5A461907EDB5D0D51BFEC6B71995C8DB3CA888C;
// System.Func`2<MPAR.Desktop.UI.DesktopScriptMenuItem,UnityEngine.GameObject>
struct Func_2_tFB19A62CB3654B73689DB02DF5FB0DA4DF193686;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<MPAR.Common.Scripting.API.Objects.MixiplyObject,Jint.Native.JsValue>,System.Boolean>
struct Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String>
struct Func_2_tD3469CDD9F41AA3F08B722F856A37CF73F3F5464;
// System.Func`2<System.IO.DirectoryInfo,System.Boolean>
struct Func_2_t10EAEF36A632678AF1E6F40890696E96D09270BB;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0;
// System.Func`2<UnityEngine.XR.ARFoundation.ARPlane,MPAR.Common.PlaneDetection.Plane>
struct Func_2_t55F82ADC887A07EBAC73B9287BB2AF522478275B;
// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>
struct Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039;
// System.Func`3<System.String,System.String,System.Collections.IEnumerator>
struct Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653;
// System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject>
struct Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF;
// System.Security.Cryptography.RandomNumberGenerator
struct RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Type
struct Type_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.AssetBundle
struct AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78;
// UnityEngine.AssetBundleManifest
struct AssetBundleManifest_t9EC1369A72D8DA0E0DECA5B63F9CF25E1053D52E;
// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962;
// UnityEngine.Canvas
struct Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591;
// UnityEngine.GUIContent
struct GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.Plane[]
struct PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.UI.Button[]
struct ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B;
// UnityEngine.UI.RawImage
struct RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityEngine.XR.ARFoundation.ARPlane
struct ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E;
// UnityEngine.XR.ARFoundation.ARReferencePointManager
struct ARReferencePointManager_t56502A19AD560012C1DBFF29BA70341D79A9604E;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef ASSETBUNDLELOADOPERATION_T4E376230379EB84B5F4A6D7F957B791348E72141_H
#define ASSETBUNDLELOADOPERATION_T4E376230379EB84B5F4A6D7F957B791348E72141_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadOperation
struct  AssetBundleLoadOperation_t4E376230379EB84B5F4A6D7F957B791348E72141  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADOPERATION_T4E376230379EB84B5F4A6D7F957B791348E72141_H
#ifndef LOADEDASSETBUNDLE_TE0CCCFBAB56DF219CD81343D8CC540113976831A_H
#define LOADEDASSETBUNDLE_TE0CCCFBAB56DF219CD81343D8CC540113976831A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.LoadedAssetBundle
struct  LoadedAssetBundle_tE0CCCFBAB56DF219CD81343D8CC540113976831A  : public RuntimeObject
{
public:
	// UnityEngine.AssetBundle AssetBundles.LoadedAssetBundle::m_AssetBundle
	AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * ___m_AssetBundle_0;
	// System.Int32 AssetBundles.LoadedAssetBundle::m_ReferencedCount
	int32_t ___m_ReferencedCount_1;

public:
	inline static int32_t get_offset_of_m_AssetBundle_0() { return static_cast<int32_t>(offsetof(LoadedAssetBundle_tE0CCCFBAB56DF219CD81343D8CC540113976831A, ___m_AssetBundle_0)); }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * get_m_AssetBundle_0() const { return ___m_AssetBundle_0; }
	inline AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 ** get_address_of_m_AssetBundle_0() { return &___m_AssetBundle_0; }
	inline void set_m_AssetBundle_0(AssetBundle_tCE287BAB693894C3991CDD308B92A8C9C4BD4C78 * value)
	{
		___m_AssetBundle_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetBundle_0), value);
	}

	inline static int32_t get_offset_of_m_ReferencedCount_1() { return static_cast<int32_t>(offsetof(LoadedAssetBundle_tE0CCCFBAB56DF219CD81343D8CC540113976831A, ___m_ReferencedCount_1)); }
	inline int32_t get_m_ReferencedCount_1() const { return ___m_ReferencedCount_1; }
	inline int32_t* get_address_of_m_ReferencedCount_1() { return &___m_ReferencedCount_1; }
	inline void set_m_ReferencedCount_1(int32_t value)
	{
		___m_ReferencedCount_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOADEDASSETBUNDLE_TE0CCCFBAB56DF219CD81343D8CC540113976831A_H
#ifndef UTILITY_T0F3CA8FDC4DD6AA5306377F96F999773CD6E73F9_H
#define UTILITY_T0F3CA8FDC4DD6AA5306377F96F999773CD6E73F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.Utility
struct  Utility_t0F3CA8FDC4DD6AA5306377F96F999773CD6E73F9  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UTILITY_T0F3CA8FDC4DD6AA5306377F96F999773CD6E73F9_H
#ifndef DEBUGGING_T29990C468C298B106B7894B1E5B224C59357A26E_H
#define DEBUGGING_T29990C468C298B106B7894B1E5B224C59357A26E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Debugging
struct  Debugging_t29990C468C298B106B7894B1E5B224C59357A26E  : public RuntimeObject
{
public:

public:
};

struct Debugging_t29990C468C298B106B7894B1E5B224C59357A26E_StaticFields
{
public:
	// MPAR.Common.Scripting.Web.WebRequestBuilder Assets.Debugging::NextDebugRequest
	WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 * ___NextDebugRequest_0;
	// System.Boolean Assets.Debugging::SendingRequest
	bool ___SendingRequest_1;

public:
	inline static int32_t get_offset_of_NextDebugRequest_0() { return static_cast<int32_t>(offsetof(Debugging_t29990C468C298B106B7894B1E5B224C59357A26E_StaticFields, ___NextDebugRequest_0)); }
	inline WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 * get_NextDebugRequest_0() const { return ___NextDebugRequest_0; }
	inline WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 ** get_address_of_NextDebugRequest_0() { return &___NextDebugRequest_0; }
	inline void set_NextDebugRequest_0(WebRequestBuilder_tDDA774CA523A4255E9D0A503835562FE63C398C7 * value)
	{
		___NextDebugRequest_0 = value;
		Il2CppCodeGenWriteBarrier((&___NextDebugRequest_0), value);
	}

	inline static int32_t get_offset_of_SendingRequest_1() { return static_cast<int32_t>(offsetof(Debugging_t29990C468C298B106B7894B1E5B224C59357A26E_StaticFields, ___SendingRequest_1)); }
	inline bool get_SendingRequest_1() const { return ___SendingRequest_1; }
	inline bool* get_address_of_SendingRequest_1() { return &___SendingRequest_1; }
	inline void set_SendingRequest_1(bool value)
	{
		___SendingRequest_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGING_T29990C468C298B106B7894B1E5B224C59357A26E_H
#ifndef U3CU3EC_T45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_H
#define U3CU3EC_T45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Debugging/<>c
struct  U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_StaticFields
{
public:
	// Assets.Debugging/<>c Assets.Debugging/<>c::<>9
	U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_H
#ifndef U3CU3CDEBUGU3EG__CALLBACKU7C5_0U3ED_TF05704AA4D35F56C4B4A29E36B288E0236B0777B_H
#define U3CU3CDEBUGU3EG__CALLBACKU7C5_0U3ED_TF05704AA4D35F56C4B4A29E36B288E0236B0777B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Debugging/<>c/<<Debug>g__callback|5_0>d
struct  U3CU3CDebugU3Eg__callbackU7C5_0U3Ed_tF05704AA4D35F56C4B4A29E36B288E0236B0777B  : public RuntimeObject
{
public:
	// System.Int32 Assets.Debugging/<>c/<<Debug>g__callback|5_0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Assets.Debugging/<>c/<<Debug>g__callback|5_0>d::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CDebugU3Eg__callbackU7C5_0U3Ed_tF05704AA4D35F56C4B4A29E36B288E0236B0777B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CU3CDebugU3Eg__callbackU7C5_0U3Ed_tF05704AA4D35F56C4B4A29E36B288E0236B0777B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CDEBUGU3EG__CALLBACKU7C5_0U3ED_TF05704AA4D35F56C4B4A29E36B288E0236B0777B_H
#ifndef U3CU3CEXECUTEREQUESTCHAINU3EG__CALLBACKU7C10_0U3ED_T8A469D321751D7D1D424788DF50E77EF55E39C19_H
#define U3CU3CEXECUTEREQUESTCHAINU3EG__CALLBACKU7C10_0U3ED_T8A469D321751D7D1D424788DF50E77EF55E39C19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.Debugging/<>c/<<ExecuteRequestChain>g__callback|10_0>d
struct  U3CU3CExecuteRequestChainU3Eg__callbackU7C10_0U3Ed_t8A469D321751D7D1D424788DF50E77EF55E39C19  : public RuntimeObject
{
public:
	// System.Int32 Assets.Debugging/<>c/<<ExecuteRequestChain>g__callback|10_0>d::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Assets.Debugging/<>c/<<ExecuteRequestChain>g__callback|10_0>d::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CU3CExecuteRequestChainU3Eg__callbackU7C10_0U3Ed_t8A469D321751D7D1D424788DF50E77EF55E39C19, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CU3CExecuteRequestChainU3Eg__callbackU7C10_0U3Ed_t8A469D321751D7D1D424788DF50E77EF55E39C19, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3CEXECUTEREQUESTCHAINU3EG__CALLBACKU7C10_0U3ED_T8A469D321751D7D1D424788DF50E77EF55E39C19_H
#ifndef U3CU3EC_T3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_H
#define U3CU3EC_T3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.ShowDirectionToMenu/<>c
struct  U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_StaticFields
{
public:
	// Assets.ShowDirectionToMenu/<>c Assets.ShowDirectionToMenu/<>c::<>9
	U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27 * ___U3CU3E9_0;
	// System.Func`2<MPAR.Desktop.UI.DesktopScriptMenuItem,UnityEngine.GameObject> Assets.ShowDirectionToMenu/<>c::<>9__6_0
	Func_2_tFB19A62CB3654B73689DB02DF5FB0DA4DF193686 * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_tFB19A62CB3654B73689DB02DF5FB0DA4DF193686 * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_tFB19A62CB3654B73689DB02DF5FB0DA4DF193686 ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_tFB19A62CB3654B73689DB02DF5FB0DA4DF193686 * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_TB2ECB44664EF864B42334177B7C4A4F19DF2D46C_H
#define U3CU3EC__DISPLAYCLASS7_0_TB2ECB44664EF864B42334177B7C4A4F19DF2D46C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.ShowDirectionToMenu/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_tB2ECB44664EF864B42334177B7C4A4F19DF2D46C  : public RuntimeObject
{
public:
	// UnityEngine.Plane[] Assets.ShowDirectionToMenu/<>c__DisplayClass7_0::planes
	PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* ___planes_0;

public:
	inline static int32_t get_offset_of_planes_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_tB2ECB44664EF864B42334177B7C4A4F19DF2D46C, ___planes_0)); }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* get_planes_0() const { return ___planes_0; }
	inline PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA** get_address_of_planes_0() { return &___planes_0; }
	inline void set_planes_0(PlaneU5BU5D_t79471E0ABE147C3018D88A036897B6DB49A782AA* value)
	{
		___planes_0 = value;
		Il2CppCodeGenWriteBarrier((&___planes_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_TB2ECB44664EF864B42334177B7C4A4F19DF2D46C_H
#ifndef SCRIPTLINEINFODATA_T8EA9BD0DF47D633F8A78D484C2199A8A396B9B30_H
#define SCRIPTLINEINFODATA_T8EA9BD0DF47D633F8A78D484C2199A8A396B9B30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Entities.ScriptLineInfoData
struct  ScriptLineInfoData_t8EA9BD0DF47D633F8A78D484C2199A8A396B9B30  : public RuntimeObject
{
public:
	// System.Int32 Assets._Project.Scripts.Common.Entities.ScriptLineInfoData::start
	int32_t ___start_0;
	// System.String Assets._Project.Scripts.Common.Entities.ScriptLineInfoData::path
	String_t* ___path_1;

public:
	inline static int32_t get_offset_of_start_0() { return static_cast<int32_t>(offsetof(ScriptLineInfoData_t8EA9BD0DF47D633F8A78D484C2199A8A396B9B30, ___start_0)); }
	inline int32_t get_start_0() const { return ___start_0; }
	inline int32_t* get_address_of_start_0() { return &___start_0; }
	inline void set_start_0(int32_t value)
	{
		___start_0 = value;
	}

	inline static int32_t get_offset_of_path_1() { return static_cast<int32_t>(offsetof(ScriptLineInfoData_t8EA9BD0DF47D633F8A78D484C2199A8A396B9B30, ___path_1)); }
	inline String_t* get_path_1() const { return ___path_1; }
	inline String_t** get_address_of_path_1() { return &___path_1; }
	inline void set_path_1(String_t* value)
	{
		___path_1 = value;
		Il2CppCodeGenWriteBarrier((&___path_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTLINEINFODATA_T8EA9BD0DF47D633F8A78D484C2199A8A396B9B30_H
#ifndef SCRIPTLINEINFODATAROOT_T7F1F035EB5F1A4891703C740A914D25C27F17825_H
#define SCRIPTLINEINFODATAROOT_T7F1F035EB5F1A4891703C740A914D25C27F17825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Entities.ScriptLineInfoDataRoot
struct  ScriptLineInfoDataRoot_t7F1F035EB5F1A4891703C740A914D25C27F17825  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Entities.ScriptLineInfoData[] Assets._Project.Scripts.Common.Entities.ScriptLineInfoDataRoot::scripts
	ScriptLineInfoDataU5BU5D_t34E739382D94242A2832DD8254FE9137F64A64DB* ___scripts_0;

public:
	inline static int32_t get_offset_of_scripts_0() { return static_cast<int32_t>(offsetof(ScriptLineInfoDataRoot_t7F1F035EB5F1A4891703C740A914D25C27F17825, ___scripts_0)); }
	inline ScriptLineInfoDataU5BU5D_t34E739382D94242A2832DD8254FE9137F64A64DB* get_scripts_0() const { return ___scripts_0; }
	inline ScriptLineInfoDataU5BU5D_t34E739382D94242A2832DD8254FE9137F64A64DB** get_address_of_scripts_0() { return &___scripts_0; }
	inline void set_scripts_0(ScriptLineInfoDataU5BU5D_t34E739382D94242A2832DD8254FE9137F64A64DB* value)
	{
		___scripts_0 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTLINEINFODATAROOT_T7F1F035EB5F1A4891703C740A914D25C27F17825_H
#ifndef U3CU3EC_T181511CCAE25217D5CCEF5432307759A0D2FEA71_H
#define U3CU3EC_T181511CCAE25217D5CCEF5432307759A0D2FEA71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c
struct  U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71_StaticFields
{
public:
	// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c::<>9
	U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.XR.ARFoundation.ARPlane,MPAR.Common.PlaneDetection.Plane> Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c::<>9__4_0
	Func_2_t55F82ADC887A07EBAC73B9287BB2AF522478275B * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t55F82ADC887A07EBAC73B9287BB2AF522478275B * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t55F82ADC887A07EBAC73B9287BB2AF522478275B ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t55F82ADC887A07EBAC73B9287BB2AF522478275B * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T181511CCAE25217D5CCEF5432307759A0D2FEA71_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_TCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC_H
#define U3CU3EC__DISPLAYCLASS4_0_TCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_tCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC  : public RuntimeObject
{
public:
	// UnityEngine.XR.ARFoundation.ARPlane Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c__DisplayClass4_0::arPlane
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___arPlane_0;

public:
	inline static int32_t get_offset_of_arPlane_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_tCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC, ___arPlane_0)); }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * get_arPlane_0() const { return ___arPlane_0; }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E ** get_address_of_arPlane_0() { return &___arPlane_0; }
	inline void set_arPlane_0(ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * value)
	{
		___arPlane_0 = value;
		Il2CppCodeGenWriteBarrier((&___arPlane_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_TCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC_H
#ifndef BEHAVIOURREGISTRY_T72AD109D3C135D3C6105EE8F5CB9D983CD522197_H
#define BEHAVIOURREGISTRY_T72AD109D3C135D3C6105EE8F5CB9D983CD522197_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Behaviours.BehaviourRegistry
struct  BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197  : public RuntimeObject
{
public:

public:
};

struct BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue>> Assets._Project.Scripts.Common.Scripting.Behaviours.BehaviourRegistry::behaviours
	Dictionary_2_t136B03A279E7BC961938C204EAC598835C08E560 * ___behaviours_0;
	// System.Collections.Generic.Dictionary`2<System.Guid,Assets._Project.Scripts.Common.Scripting.Behaviours.MxBehaviour> Assets._Project.Scripts.Common.Scripting.Behaviours.BehaviourRegistry::behaviourInstances
	Dictionary_2_t4F245103ADEAA13DF8D05B40D4381D70EF8E9431 * ___behaviourInstances_1;

public:
	inline static int32_t get_offset_of_behaviours_0() { return static_cast<int32_t>(offsetof(BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197_StaticFields, ___behaviours_0)); }
	inline Dictionary_2_t136B03A279E7BC961938C204EAC598835C08E560 * get_behaviours_0() const { return ___behaviours_0; }
	inline Dictionary_2_t136B03A279E7BC961938C204EAC598835C08E560 ** get_address_of_behaviours_0() { return &___behaviours_0; }
	inline void set_behaviours_0(Dictionary_2_t136B03A279E7BC961938C204EAC598835C08E560 * value)
	{
		___behaviours_0 = value;
		Il2CppCodeGenWriteBarrier((&___behaviours_0), value);
	}

	inline static int32_t get_offset_of_behaviourInstances_1() { return static_cast<int32_t>(offsetof(BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197_StaticFields, ___behaviourInstances_1)); }
	inline Dictionary_2_t4F245103ADEAA13DF8D05B40D4381D70EF8E9431 * get_behaviourInstances_1() const { return ___behaviourInstances_1; }
	inline Dictionary_2_t4F245103ADEAA13DF8D05B40D4381D70EF8E9431 ** get_address_of_behaviourInstances_1() { return &___behaviourInstances_1; }
	inline void set_behaviourInstances_1(Dictionary_2_t4F245103ADEAA13DF8D05B40D4381D70EF8E9431 * value)
	{
		___behaviourInstances_1 = value;
		Il2CppCodeGenWriteBarrier((&___behaviourInstances_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOURREGISTRY_T72AD109D3C135D3C6105EE8F5CB9D983CD522197_H
#ifndef U3CU3EC_T3341450C5E2F459D7BEC343BC8424C680BB5850E_H
#define U3CU3EC_T3341450C5E2F459D7BEC343BC8424C680BB5850E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler/<>c
struct  U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E_StaticFields
{
public:
	// Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler/<>c Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler/<>c::<>9
	U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E * ___U3CU3E9_0;
	// System.Action Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler/<>c::<>9__0_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T3341450C5E2F459D7BEC343BC8424C680BB5850E_H
#ifndef U3CU3EC_T6A3CA5EF339718244604EE794ED744E7EF9D32EA_H
#define U3CU3EC_T6A3CA5EF339718244604EE794ED744E7EF9D32EA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<>c
struct  U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields
{
public:
	// Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<>c Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<>c::<>9
	U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA * ___U3CU3E9_0;
	// System.Action Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<>c::<>9__8_1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__8_1_1;
	// System.Action Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<>c::<>9__12_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__12_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields, ___U3CU3E9__8_1_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__8_1_1() const { return ___U3CU3E9__8_1_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__8_1_1() { return &___U3CU3E9__8_1_1; }
	inline void set_U3CU3E9__8_1_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__8_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_1_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__12_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields, ___U3CU3E9__12_0_2)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__12_0_2() const { return ___U3CU3E9__12_0_2; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__12_0_2() { return &___U3CU3E9__12_0_2; }
	inline void set_U3CU3E9__12_0_2(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__12_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__12_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T6A3CA5EF339718244604EE794ED744E7EF9D32EA_H
#ifndef U3CSTOPCAMERAU3ED__13_TE309B618F67AB4403EE48BC2B0A7D5A38BF0E423_H
#define U3CSTOPCAMERAU3ED__13_TE309B618F67AB4403EE48BC2B0A7D5A38BF0E423_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<StopCamera>d__13
struct  U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423  : public RuntimeObject
{
public:
	// System.Int32 Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<StopCamera>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<StopCamera>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<StopCamera>d__13::<>4__this
	QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8 * ___U3CU3E4__this_2;
	// System.Action Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader/<StopCamera>d__13::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423, ___U3CU3E4__this_2)); }
	inline QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_callback_3() { return static_cast<int32_t>(offsetof(U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423, ___callback_3)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_3() const { return ___callback_3; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_3() { return &___callback_3; }
	inline void set_callback_3(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_3 = value;
		Il2CppCodeGenWriteBarrier((&___callback_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTOPCAMERAU3ED__13_TE309B618F67AB4403EE48BC2B0A7D5A38BF0E423_H
#ifndef JAVASCRIPTAPIHANDLER_TBC6372A11FD451DB7FACA66B68D5AA37E3B294E0_H
#define JAVASCRIPTAPIHANDLER_TBC6372A11FD451DB7FACA66B68D5AA37E3B294E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.JavascriptApiHandler
struct  JavascriptApiHandler_tBC6372A11FD451DB7FACA66B68D5AA37E3B294E0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTAPIHANDLER_TBC6372A11FD451DB7FACA66B68D5AA37E3B294E0_H
#ifndef U3CU3EC_TCA585659C7C87B3CD969AD0899FB181F8F1E854C_H
#define U3CU3EC_TCA585659C7C87B3CD969AD0899FB181F8F1E854C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.JavascriptApiHandler/<>c
struct  U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields
{
public:
	// Assets._Project.Scripts.Common.Scripting.Mix.JavascriptApiHandler/<>c Assets._Project.Scripts.Common.Scripting.Mix.JavascriptApiHandler/<>c::<>9
	U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C * ___U3CU3E9_0;
	// System.Action`1<System.Object> Assets._Project.Scripts.Common.Scripting.Mix.JavascriptApiHandler/<>c::<>9__2_0
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___U3CU3E9__2_0_1;
	// System.Action`1<System.Object> Assets._Project.Scripts.Common.Scripting.Mix.JavascriptApiHandler/<>c::<>9__2_1
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___U3CU3E9__2_1_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields, ___U3CU3E9__2_0_1)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_U3CU3E9__2_0_1() const { return ___U3CU3E9__2_0_1; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_U3CU3E9__2_0_1() { return &___U3CU3E9__2_0_1; }
	inline void set_U3CU3E9__2_0_1(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___U3CU3E9__2_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__2_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields, ___U3CU3E9__2_1_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_U3CU3E9__2_1_2() const { return ___U3CU3E9__2_1_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_U3CU3E9__2_1_2() { return &___U3CU3E9__2_1_2; }
	inline void set_U3CU3E9__2_1_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___U3CU3E9__2_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__2_1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCA585659C7C87B3CD969AD0899FB181F8F1E854C_H
#ifndef SCRIPTEVENTHANDLER_T6818D0F175ECC12B599C5FE2A33346CECCD49AC7_H
#define SCRIPTEVENTHANDLER_T6818D0F175ECC12B599C5FE2A33346CECCD49AC7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler
struct  ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Collections.Generic.List`1<Jint.Native.JsValue>> Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler::messageSubscribers
	Dictionary_2_t8EA4CA008CF6EF1794BF92CFA606FDCA3DF6ABDB * ___messageSubscribers_0;

public:
	inline static int32_t get_offset_of_messageSubscribers_0() { return static_cast<int32_t>(offsetof(ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7, ___messageSubscribers_0)); }
	inline Dictionary_2_t8EA4CA008CF6EF1794BF92CFA606FDCA3DF6ABDB * get_messageSubscribers_0() const { return ___messageSubscribers_0; }
	inline Dictionary_2_t8EA4CA008CF6EF1794BF92CFA606FDCA3DF6ABDB ** get_address_of_messageSubscribers_0() { return &___messageSubscribers_0; }
	inline void set_messageSubscribers_0(Dictionary_2_t8EA4CA008CF6EF1794BF92CFA606FDCA3DF6ABDB * value)
	{
		___messageSubscribers_0 = value;
		Il2CppCodeGenWriteBarrier((&___messageSubscribers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTEVENTHANDLER_T6818D0F175ECC12B599C5FE2A33346CECCD49AC7_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB_H
#define U3CU3EC__DISPLAYCLASS5_0_TA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.API.Objects.MixiplyObject Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler/<>c__DisplayClass5_0::obj
	MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * ___obj_0;
	// Jint.Native.JsValue Assets._Project.Scripts.Common.Scripting.Mix.ScriptEventHandler/<>c__DisplayClass5_0::collisionEvent
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___collisionEvent_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB, ___obj_0)); }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * get_obj_0() const { return ___obj_0; }
	inline MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(MixiplyObject_t13708040370D4F8E9A32AA4F1A17E08D988C7E7B * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_collisionEvent_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB, ___collisionEvent_1)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_collisionEvent_1() const { return ___collisionEvent_1; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_collisionEvent_1() { return &___collisionEvent_1; }
	inline void set_collisionEvent_1(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___collisionEvent_1 = value;
		Il2CppCodeGenWriteBarrier((&___collisionEvent_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB_H
#ifndef SCRIPTINPUTHANDLER_TC70863BE85E492598590C8D3D441A7C5FA096EDC_H
#define SCRIPTINPUTHANDLER_TC70863BE85E492598590C8D3D441A7C5FA096EDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler
struct  ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<OnGameObjectClickEvents>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3COnGameObjectClickEventsU3Ek__BackingField_0;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object[]> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<OnGameObjectClickArgs>k__BackingField
	Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 * ___U3COnGameObjectClickArgsU3Ek__BackingField_1;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<OnGameObjectReleaseEvents>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3COnGameObjectReleaseEventsU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,System.Object[]> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<OnGameObjectReleaseArgs>k__BackingField
	Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 * ___U3COnGameObjectReleaseArgsU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<MPAR.Common.Scripting.API.Objects.MixiplyObject,Jint.Native.JsValue> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<OnGameObjectClickJsEvents>k__BackingField
	Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 * ___U3COnGameObjectClickJsEventsU3Ek__BackingField_4;
	// System.Collections.Generic.Dictionary`2<MPAR.Common.Scripting.API.Objects.MixiplyObject,Jint.Native.JsValue> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<OnGameObjectReleaseJsEvents>k__BackingField
	Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 * ___U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5;
	// System.Collections.Generic.List`1<System.String> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::<ClickCallbacks>k__BackingField
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___U3CClickCallbacksU3Ek__BackingField_6;
	// System.Boolean Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler::isInputEnabled
	bool ___isInputEnabled_7;

public:
	inline static int32_t get_offset_of_U3COnGameObjectClickEventsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3COnGameObjectClickEventsU3Ek__BackingField_0)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3COnGameObjectClickEventsU3Ek__BackingField_0() const { return ___U3COnGameObjectClickEventsU3Ek__BackingField_0; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3COnGameObjectClickEventsU3Ek__BackingField_0() { return &___U3COnGameObjectClickEventsU3Ek__BackingField_0; }
	inline void set_U3COnGameObjectClickEventsU3Ek__BackingField_0(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3COnGameObjectClickEventsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameObjectClickEventsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3COnGameObjectClickArgsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3COnGameObjectClickArgsU3Ek__BackingField_1)); }
	inline Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 * get_U3COnGameObjectClickArgsU3Ek__BackingField_1() const { return ___U3COnGameObjectClickArgsU3Ek__BackingField_1; }
	inline Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 ** get_address_of_U3COnGameObjectClickArgsU3Ek__BackingField_1() { return &___U3COnGameObjectClickArgsU3Ek__BackingField_1; }
	inline void set_U3COnGameObjectClickArgsU3Ek__BackingField_1(Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 * value)
	{
		___U3COnGameObjectClickArgsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameObjectClickArgsU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COnGameObjectReleaseEventsU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3COnGameObjectReleaseEventsU3Ek__BackingField_2)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3COnGameObjectReleaseEventsU3Ek__BackingField_2() const { return ___U3COnGameObjectReleaseEventsU3Ek__BackingField_2; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3COnGameObjectReleaseEventsU3Ek__BackingField_2() { return &___U3COnGameObjectReleaseEventsU3Ek__BackingField_2; }
	inline void set_U3COnGameObjectReleaseEventsU3Ek__BackingField_2(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3COnGameObjectReleaseEventsU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameObjectReleaseEventsU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3COnGameObjectReleaseArgsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3COnGameObjectReleaseArgsU3Ek__BackingField_3)); }
	inline Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 * get_U3COnGameObjectReleaseArgsU3Ek__BackingField_3() const { return ___U3COnGameObjectReleaseArgsU3Ek__BackingField_3; }
	inline Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 ** get_address_of_U3COnGameObjectReleaseArgsU3Ek__BackingField_3() { return &___U3COnGameObjectReleaseArgsU3Ek__BackingField_3; }
	inline void set_U3COnGameObjectReleaseArgsU3Ek__BackingField_3(Dictionary_2_t1C73BB114F74AF09129EFD4D94CFA57E85C968D0 * value)
	{
		___U3COnGameObjectReleaseArgsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameObjectReleaseArgsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3COnGameObjectClickJsEventsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3COnGameObjectClickJsEventsU3Ek__BackingField_4)); }
	inline Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 * get_U3COnGameObjectClickJsEventsU3Ek__BackingField_4() const { return ___U3COnGameObjectClickJsEventsU3Ek__BackingField_4; }
	inline Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 ** get_address_of_U3COnGameObjectClickJsEventsU3Ek__BackingField_4() { return &___U3COnGameObjectClickJsEventsU3Ek__BackingField_4; }
	inline void set_U3COnGameObjectClickJsEventsU3Ek__BackingField_4(Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 * value)
	{
		___U3COnGameObjectClickJsEventsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameObjectClickJsEventsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5)); }
	inline Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 * get_U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5() const { return ___U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5; }
	inline Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 ** get_address_of_U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5() { return &___U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5; }
	inline void set_U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5(Dictionary_2_t01DA5F93EFB6282A8FC85482CC6B52A53E1A2550 * value)
	{
		___U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CClickCallbacksU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___U3CClickCallbacksU3Ek__BackingField_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_U3CClickCallbacksU3Ek__BackingField_6() const { return ___U3CClickCallbacksU3Ek__BackingField_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_U3CClickCallbacksU3Ek__BackingField_6() { return &___U3CClickCallbacksU3Ek__BackingField_6; }
	inline void set_U3CClickCallbacksU3Ek__BackingField_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___U3CClickCallbacksU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClickCallbacksU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_isInputEnabled_7() { return static_cast<int32_t>(offsetof(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC, ___isInputEnabled_7)); }
	inline bool get_isInputEnabled_7() const { return ___isInputEnabled_7; }
	inline bool* get_address_of_isInputEnabled_7() { return &___isInputEnabled_7; }
	inline void set_isInputEnabled_7(bool value)
	{
		___isInputEnabled_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTINPUTHANDLER_TC70863BE85E492598590C8D3D441A7C5FA096EDC_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T22F36F94EB17F09176DD3351C1275CFFBD7F581D_H
#define U3CU3EC__DISPLAYCLASS35_0_T22F36F94EB17F09176DD3351C1275CFFBD7F581D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_0::<>4__this
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * ___U3CU3E4__this_0;
	// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_0::hit
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * ___hit_1;
	// Jint.Native.JsValue Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_0::clickedObjectEvent
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___clickedObjectEvent_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D, ___U3CU3E4__this_0)); }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_hit_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D, ___hit_1)); }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * get_hit_1() const { return ___hit_1; }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A ** get_address_of_hit_1() { return &___hit_1; }
	inline void set_hit_1(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * value)
	{
		___hit_1 = value;
		Il2CppCodeGenWriteBarrier((&___hit_1), value);
	}

	inline static int32_t get_offset_of_clickedObjectEvent_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D, ___clickedObjectEvent_2)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_clickedObjectEvent_2() const { return ___clickedObjectEvent_2; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_clickedObjectEvent_2() { return &___clickedObjectEvent_2; }
	inline void set_clickedObjectEvent_2(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___clickedObjectEvent_2 = value;
		Il2CppCodeGenWriteBarrier((&___clickedObjectEvent_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T22F36F94EB17F09176DD3351C1275CFFBD7F581D_H
#ifndef U3CU3EC__DISPLAYCLASS35_1_TC2250CC5A8835495D99DDCDB485DC2EE624E8E6F_H
#define U3CU3EC__DISPLAYCLASS35_1_TC2250CC5A8835495D99DDCDB485DC2EE624E8E6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_1
struct  U3CU3Ec__DisplayClass35_1_tC2250CC5A8835495D99DDCDB485DC2EE624E8E6F  : public RuntimeObject
{
public:
	// System.String Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_1::functionName
	String_t* ___functionName_0;
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_0 Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass35_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_functionName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_tC2250CC5A8835495D99DDCDB485DC2EE624E8E6F, ___functionName_0)); }
	inline String_t* get_functionName_0() const { return ___functionName_0; }
	inline String_t** get_address_of_functionName_0() { return &___functionName_0; }
	inline void set_functionName_0(String_t* value)
	{
		___functionName_0 = value;
		Il2CppCodeGenWriteBarrier((&___functionName_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_1_tC2250CC5A8835495D99DDCDB485DC2EE624E8E6F, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_1_TC2250CC5A8835495D99DDCDB485DC2EE624E8E6F_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_TF1514C54402A97B5AC84300244FE50217A9AA70F_H
#define U3CU3EC__DISPLAYCLASS36_0_TF1514C54402A97B5AC84300244FE50217A9AA70F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass36_0::hit
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * ___hit_0;
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass36_0::<>4__this
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * ___U3CU3E4__this_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<MPAR.Common.Scripting.API.Objects.MixiplyObject,Jint.Native.JsValue>,System.Boolean> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass36_0::<>9__1
	Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 * ___U3CU3E9__1_2;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F, ___hit_0)); }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * get_hit_0() const { return ___hit_0; }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A ** get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * value)
	{
		___hit_0 = value;
		Il2CppCodeGenWriteBarrier((&___hit_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F, ___U3CU3E4__this_1)); }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F, ___U3CU3E9__1_2)); }
	inline Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 * get_U3CU3E9__1_2() const { return ___U3CU3E9__1_2; }
	inline Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 ** get_address_of_U3CU3E9__1_2() { return &___U3CU3E9__1_2; }
	inline void set_U3CU3E9__1_2(Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 * value)
	{
		___U3CU3E9__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_TF1514C54402A97B5AC84300244FE50217A9AA70F_H
#ifndef U3CU3EC__DISPLAYCLASS37_0_T16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA_H
#define U3CU3EC__DISPLAYCLASS37_0_T16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass37_0
struct  U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass37_0::hit
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * ___hit_0;
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass37_0::<>4__this
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * ___U3CU3E4__this_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<MPAR.Common.Scripting.API.Objects.MixiplyObject,Jint.Native.JsValue>,System.Boolean> Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass37_0::<>9__1
	Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 * ___U3CU3E9__1_2;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA, ___hit_0)); }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * get_hit_0() const { return ___hit_0; }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A ** get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * value)
	{
		___hit_0 = value;
		Il2CppCodeGenWriteBarrier((&___hit_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA, ___U3CU3E4__this_1)); }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA, ___U3CU3E9__1_2)); }
	inline Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 * get_U3CU3E9__1_2() const { return ___U3CU3E9__1_2; }
	inline Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 ** get_address_of_U3CU3E9__1_2() { return &___U3CU3E9__1_2; }
	inline void set_U3CU3E9__1_2(Func_2_tD951040315FE5BFFF8583B6D68F70943E9CEDD36 * value)
	{
		___U3CU3E9__1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__1_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS37_0_T16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_TAB02B270DED9ACA9B3504E120DD4A68046EA2118_H
#define U3CU3EC__DISPLAYCLASS39_0_TAB02B270DED9ACA9B3504E120DD4A68046EA2118_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_tAB02B270DED9ACA9B3504E120DD4A68046EA2118  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo Assets._Project.Scripts.Common.Scripting.Mix.ScriptInputHandler/<>c__DisplayClass39_0::hit
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * ___hit_0;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_tAB02B270DED9ACA9B3504E120DD4A68046EA2118, ___hit_0)); }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * get_hit_0() const { return ___hit_0; }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A ** get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * value)
	{
		___hit_0 = value;
		Il2CppCodeGenWriteBarrier((&___hit_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_TAB02B270DED9ACA9B3504E120DD4A68046EA2118_H
#ifndef SCRIPTUTILITY_TB6B19DD8C0F93DA2C853312A654BEAB1194627EF_H
#define SCRIPTUTILITY_TB6B19DD8C0F93DA2C853312A654BEAB1194627EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptUtility
struct  ScriptUtility_tB6B19DD8C0F93DA2C853312A654BEAB1194627EF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTUTILITY_TB6B19DD8C0F93DA2C853312A654BEAB1194627EF_H
#ifndef U3CU3EC_T0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_H
#define U3CU3EC_T0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Mix.ScriptUtility/<>c
struct  U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_StaticFields
{
public:
	// Assets._Project.Scripts.Common.Scripting.Mix.ScriptUtility/<>c Assets._Project.Scripts.Common.Scripting.Mix.ScriptUtility/<>c::<>9
	U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100 * ___U3CU3E9_0;
	// System.Func`2<MPAR.Common.Scripting.API.Assets.ParameterDefinition,System.Boolean> Assets._Project.Scripts.Common.Scripting.Mix.ScriptUtility/<>c::<>9__0_0
	Func_2_tCBC6468A0955C6F2D90E115A4CCD791605A4C3FF * ___U3CU3E9__0_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_StaticFields, ___U3CU3E9__0_0_1)); }
	inline Func_2_tCBC6468A0955C6F2D90E115A4CCD791605A4C3FF * get_U3CU3E9__0_0_1() const { return ___U3CU3E9__0_0_1; }
	inline Func_2_tCBC6468A0955C6F2D90E115A4CCD791605A4C3FF ** get_address_of_U3CU3E9__0_0_1() { return &___U3CU3E9__0_0_1; }
	inline void set_U3CU3E9__0_0_1(Func_2_tCBC6468A0955C6F2D90E115A4CCD791605A4C3FF * value)
	{
		___U3CU3E9__0_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_H
#ifndef SCRIPTCONSTANTS_T4AB349FC4A71ACE45E61639B88BC82698A3778F3_H
#define SCRIPTCONSTANTS_T4AB349FC4A71ACE45E61639B88BC82698A3778F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.ScriptConstants
struct  ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3  : public RuntimeObject
{
public:

public:
};

struct ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3_StaticFields
{
public:
	// System.String Assets._Project.Scripts.Common.Scripting.ScriptConstants::helperFunctions
	String_t* ___helperFunctions_0;
	// System.String Assets._Project.Scripts.Common.Scripting.ScriptConstants::arrayPrototypes
	String_t* ___arrayPrototypes_1;

public:
	inline static int32_t get_offset_of_helperFunctions_0() { return static_cast<int32_t>(offsetof(ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3_StaticFields, ___helperFunctions_0)); }
	inline String_t* get_helperFunctions_0() const { return ___helperFunctions_0; }
	inline String_t** get_address_of_helperFunctions_0() { return &___helperFunctions_0; }
	inline void set_helperFunctions_0(String_t* value)
	{
		___helperFunctions_0 = value;
		Il2CppCodeGenWriteBarrier((&___helperFunctions_0), value);
	}

	inline static int32_t get_offset_of_arrayPrototypes_1() { return static_cast<int32_t>(offsetof(ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3_StaticFields, ___arrayPrototypes_1)); }
	inline String_t* get_arrayPrototypes_1() const { return ___arrayPrototypes_1; }
	inline String_t** get_address_of_arrayPrototypes_1() { return &___arrayPrototypes_1; }
	inline void set_arrayPrototypes_1(String_t* value)
	{
		___arrayPrototypes_1 = value;
		Il2CppCodeGenWriteBarrier((&___arrayPrototypes_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTCONSTANTS_T4AB349FC4A71ACE45E61639B88BC82698A3778F3_H
#ifndef SCRIPTTEMPLATE_T00A546D156E110E6A75DFA65041BBC57FCB1FCAE_H
#define SCRIPTTEMPLATE_T00A546D156E110E6A75DFA65041BBC57FCB1FCAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Templates.ScriptTemplate
struct  ScriptTemplate_t00A546D156E110E6A75DFA65041BBC57FCB1FCAE  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTTEMPLATE_T00A546D156E110E6A75DFA65041BBC57FCB1FCAE_H
#ifndef OVERLOADMATCHER_TF1420F17B201A68AD6A75DE2043AE9D85282C7E0_H
#define OVERLOADMATCHER_TF1420F17B201A68AD6A75DE2043AE9D85282C7E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Utility.OverloadMatcher
struct  OverloadMatcher_tF1420F17B201A68AD6A75DE2043AE9D85282C7E0  : public RuntimeObject
{
public:
	// Jint.Native.JsValue[] Assets._Project.Scripts.Common.Scripting.Utility.OverloadMatcher::jsParameters
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___jsParameters_0;

public:
	inline static int32_t get_offset_of_jsParameters_0() { return static_cast<int32_t>(offsetof(OverloadMatcher_tF1420F17B201A68AD6A75DE2043AE9D85282C7E0, ___jsParameters_0)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_jsParameters_0() const { return ___jsParameters_0; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_jsParameters_0() { return &___jsParameters_0; }
	inline void set_jsParameters_0(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___jsParameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___jsParameters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OVERLOADMATCHER_TF1420F17B201A68AD6A75DE2043AE9D85282C7E0_H
#ifndef U3CU3EC__DISPLAYCLASS13_0_T2FF13F947CE8E8A46D781DFE294CC98E86396C27_H
#define U3CU3EC__DISPLAYCLASS13_0_T2FF13F947CE8E8A46D781DFE294CC98E86396C27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem/<>c__DisplayClass13_0
struct  U3CU3Ec__DisplayClass13_0_t2FF13F947CE8E8A46D781DFE294CC98E86396C27  : public RuntimeObject
{
public:
	// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue> Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem/<>c__DisplayClass13_0::callback
	Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * ___callback_0;

public:
	inline static int32_t get_offset_of_callback_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass13_0_t2FF13F947CE8E8A46D781DFE294CC98E86396C27, ___callback_0)); }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * get_callback_0() const { return ___callback_0; }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 ** get_address_of_callback_0() { return &___callback_0; }
	inline void set_callback_0(Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * value)
	{
		___callback_0 = value;
		Il2CppCodeGenWriteBarrier((&___callback_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS13_0_T2FF13F947CE8E8A46D781DFE294CC98E86396C27_H
#ifndef FOCUSMANAGERWRAPPER_T92F274FA35DE2DC44DDE6837069964C235147EB1_H
#define FOCUSMANAGERWRAPPER_T92F274FA35DE2DC44DDE6837069964C235147EB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Utility.FocusManagerWrapper
struct  FocusManagerWrapper_t92F274FA35DE2DC44DDE6837069964C235147EB1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOCUSMANAGERWRAPPER_T92F274FA35DE2DC44DDE6837069964C235147EB1_H
#ifndef GAZEMANAGERWRAPPER_TF805EC3993C73F17F9915BAA20CCD76210101FC8_H
#define GAZEMANAGERWRAPPER_TF805EC3993C73F17F9915BAA20CCD76210101FC8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Utility.GazeManagerWrapper
struct  GazeManagerWrapper_tF805EC3993C73F17F9915BAA20CCD76210101FC8  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAZEMANAGERWRAPPER_TF805EC3993C73F17F9915BAA20CCD76210101FC8_H
#ifndef U3CU3EC_TA834516095E63DA9ADBFCB1713FDCE0599B0982C_H
#define U3CU3EC_TA834516095E63DA9ADBFCB1713FDCE0599B0982C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Mobile.UI.MobileScriptMenu/<>c
struct  U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C_StaticFields
{
public:
	// Assets._Project.Scripts.Mobile.UI.MobileScriptMenu/<>c Assets._Project.Scripts.Mobile.UI.MobileScriptMenu/<>c::<>9
	U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C * ___U3CU3E9_0;
	// System.Action Assets._Project.Scripts.Mobile.UI.MobileScriptMenu/<>c::<>9__3_0
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__3_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__3_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C_StaticFields, ___U3CU3E9__3_0_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__3_0_1() const { return ___U3CU3E9__3_0_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__3_0_1() { return &___U3CU3E9__3_0_1; }
	inline void set_U3CU3E9__3_0_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__3_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__3_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TA834516095E63DA9ADBFCB1713FDCE0599B0982C_H
#ifndef HOLOLENSDEPENDENCIES_T760AD08728DDB4C9DD391FF8068098E16CA358F1_H
#define HOLOLENSDEPENDENCIES_T760AD08728DDB4C9DD391FF8068098E16CA358F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.InputModule.HololensDependencies
struct  HololensDependencies_t760AD08728DDB4C9DD391FF8068098E16CA358F1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOLOLENSDEPENDENCIES_T760AD08728DDB4C9DD391FF8068098E16CA358F1_H
#ifndef U3CSETCAMERACONTROLSWHENREADYU3ED__15_T3099093EC54A41C74DD607245BA3D9EA1F66221B_H
#define U3CSETCAMERACONTROLSWHENREADYU3ED__15_T3099093EC54A41C74DD607245BA3D9EA1F66221B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Input.DesktopInput/<SetCameraControlsWhenReady>d__15
struct  U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Desktop.Input.DesktopInput/<SetCameraControlsWhenReady>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Desktop.Input.DesktopInput/<SetCameraControlsWhenReady>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Desktop.Input.DesktopInput MPAR.Desktop.Input.DesktopInput/<SetCameraControlsWhenReady>d__15::<>4__this
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B, ___U3CU3E4__this_2)); }
	inline DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSETCAMERACONTROLSWHENREADYU3ED__15_T3099093EC54A41C74DD607245BA3D9EA1F66221B_H
#ifndef U3CU3EC_T2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_H
#define U3CU3EC_T2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c
struct  U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields
{
public:
	// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053 * ___U3CU3E9_0;
	// System.Func`2<MPAR.Common.GameObjects.ExhibitBox,MPAR.Common.Entities.HologramData> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__30_0
	Func_2_tE2AAA0D7FBA4B1D0FA95518D48F76AB4FF2FDE51 * ___U3CU3E9__30_0_1;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,System.String>,System.String> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__35_0
	Func_2_tD3469CDD9F41AA3F08B722F856A37CF73F3F5464 * ___U3CU3E9__35_0_2;
	// System.Func`2<System.IO.DirectoryInfo,System.Boolean> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__38_0
	Func_2_t10EAEF36A632678AF1E6F40890696E96D09270BB * ___U3CU3E9__38_0_3;
	// System.Func`2<MPAR.Common.Scripting.Script,System.Boolean> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__38_2
	Func_2_t953C24A29A3DD69F4675555FA28FF262C040B25D * ___U3CU3E9__38_2_4;
	// System.Func`2<MPAR.Common.Scripting.Script,System.DateTime> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__38_3
	Func_2_tD5A461907EDB5D0D51BFEC6B71995C8DB3CA888C * ___U3CU3E9__38_3_5;
	// System.Func`2<System.String,System.Boolean> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__40_0
	Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * ___U3CU3E9__40_0_6;
	// System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__41_0
	Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * ___U3CU3E9__41_0_7;
	// System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__41_1
	Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * ___U3CU3E9__41_1_8;
	// System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject> MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c::<>9__41_2
	Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * ___U3CU3E9__41_2_9;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__30_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__30_0_1)); }
	inline Func_2_tE2AAA0D7FBA4B1D0FA95518D48F76AB4FF2FDE51 * get_U3CU3E9__30_0_1() const { return ___U3CU3E9__30_0_1; }
	inline Func_2_tE2AAA0D7FBA4B1D0FA95518D48F76AB4FF2FDE51 ** get_address_of_U3CU3E9__30_0_1() { return &___U3CU3E9__30_0_1; }
	inline void set_U3CU3E9__30_0_1(Func_2_tE2AAA0D7FBA4B1D0FA95518D48F76AB4FF2FDE51 * value)
	{
		___U3CU3E9__30_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__30_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__35_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__35_0_2)); }
	inline Func_2_tD3469CDD9F41AA3F08B722F856A37CF73F3F5464 * get_U3CU3E9__35_0_2() const { return ___U3CU3E9__35_0_2; }
	inline Func_2_tD3469CDD9F41AA3F08B722F856A37CF73F3F5464 ** get_address_of_U3CU3E9__35_0_2() { return &___U3CU3E9__35_0_2; }
	inline void set_U3CU3E9__35_0_2(Func_2_tD3469CDD9F41AA3F08B722F856A37CF73F3F5464 * value)
	{
		___U3CU3E9__35_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__35_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__38_0_3)); }
	inline Func_2_t10EAEF36A632678AF1E6F40890696E96D09270BB * get_U3CU3E9__38_0_3() const { return ___U3CU3E9__38_0_3; }
	inline Func_2_t10EAEF36A632678AF1E6F40890696E96D09270BB ** get_address_of_U3CU3E9__38_0_3() { return &___U3CU3E9__38_0_3; }
	inline void set_U3CU3E9__38_0_3(Func_2_t10EAEF36A632678AF1E6F40890696E96D09270BB * value)
	{
		___U3CU3E9__38_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_2_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__38_2_4)); }
	inline Func_2_t953C24A29A3DD69F4675555FA28FF262C040B25D * get_U3CU3E9__38_2_4() const { return ___U3CU3E9__38_2_4; }
	inline Func_2_t953C24A29A3DD69F4675555FA28FF262C040B25D ** get_address_of_U3CU3E9__38_2_4() { return &___U3CU3E9__38_2_4; }
	inline void set_U3CU3E9__38_2_4(Func_2_t953C24A29A3DD69F4675555FA28FF262C040B25D * value)
	{
		___U3CU3E9__38_2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_2_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__38_3_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__38_3_5)); }
	inline Func_2_tD5A461907EDB5D0D51BFEC6B71995C8DB3CA888C * get_U3CU3E9__38_3_5() const { return ___U3CU3E9__38_3_5; }
	inline Func_2_tD5A461907EDB5D0D51BFEC6B71995C8DB3CA888C ** get_address_of_U3CU3E9__38_3_5() { return &___U3CU3E9__38_3_5; }
	inline void set_U3CU3E9__38_3_5(Func_2_tD5A461907EDB5D0D51BFEC6B71995C8DB3CA888C * value)
	{
		___U3CU3E9__38_3_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__38_3_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__40_0_6)); }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * get_U3CU3E9__40_0_6() const { return ___U3CU3E9__40_0_6; }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 ** get_address_of_U3CU3E9__40_0_6() { return &___U3CU3E9__40_0_6; }
	inline void set_U3CU3E9__40_0_6(Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * value)
	{
		___U3CU3E9__40_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__41_0_7)); }
	inline Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * get_U3CU3E9__41_0_7() const { return ___U3CU3E9__41_0_7; }
	inline Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF ** get_address_of_U3CU3E9__41_0_7() { return &___U3CU3E9__41_0_7; }
	inline void set_U3CU3E9__41_0_7(Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * value)
	{
		___U3CU3E9__41_0_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__41_1_8)); }
	inline Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * get_U3CU3E9__41_1_8() const { return ___U3CU3E9__41_1_8; }
	inline Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF ** get_address_of_U3CU3E9__41_1_8() { return &___U3CU3E9__41_1_8; }
	inline void set_U3CU3E9__41_1_8(Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * value)
	{
		___U3CU3E9__41_1_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_2_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields, ___U3CU3E9__41_2_9)); }
	inline Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * get_U3CU3E9__41_2_9() const { return ___U3CU3E9__41_2_9; }
	inline Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF ** get_address_of_U3CU3E9__41_2_9() { return &___U3CU3E9__41_2_9; }
	inline void set_U3CU3E9__41_2_9(Func_5_t0386F599E8ABF45ED7F07B4171B23D97433DE7CF * value)
	{
		___U3CU3E9__41_2_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_2_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T565FB00CF4B8399BD98C1A25F7207E1A49E80520_H
#define U3CU3EC__DISPLAYCLASS31_0_T565FB00CF4B8399BD98C1A25F7207E1A49E80520_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520  : public RuntimeObject
{
public:
	// MPAR.Common.Persistence.Anchor MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_0::anchor
	Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * ___anchor_0;
	// MPAR.Desktop.Persistence.BaseSaveLoadManager MPAR.Desktop.Persistence.BaseSaveLoadManager/<>c__DisplayClass31_0::<>4__this
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_anchor_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520, ___anchor_0)); }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * get_anchor_0() const { return ___anchor_0; }
	inline Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A ** get_address_of_anchor_0() { return &___anchor_0; }
	inline void set_anchor_0(Anchor_tB46CBB8AB8C6EC2B8EA492E1DCF35A9A525BC26A * value)
	{
		___anchor_0 = value;
		Il2CppCodeGenWriteBarrier((&___anchor_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520, ___U3CU3E4__this_1)); }
	inline BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T565FB00CF4B8399BD98C1A25F7207E1A49E80520_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_TFA2354BC3A84EE97CBC265A441FC12F60B40A37F_H
#define U3CU3EC__DISPLAYCLASS38_0_TFA2354BC3A84EE97CBC265A441FC12F60B40A37F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopInScriptMenu/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_tFA2354BC3A84EE97CBC265A441FC12F60B40A37F  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo MPAR.Desktop.UI.DesktopInScriptMenu/<>c__DisplayClass38_0::hit
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * ___hit_0;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_tFA2354BC3A84EE97CBC265A441FC12F60B40A37F, ___hit_0)); }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * get_hit_0() const { return ___hit_0; }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A ** get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * value)
	{
		___hit_0 = value;
		Il2CppCodeGenWriteBarrier((&___hit_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_TFA2354BC3A84EE97CBC265A441FC12F60B40A37F_H
#ifndef U3CU3EC_T8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_H
#define U3CU3EC_T8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenu/<>c
struct  U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_StaticFields
{
public:
	// MPAR.Desktop.UI.DesktopScriptMenu/<>c MPAR.Desktop.UI.DesktopScriptMenu/<>c::<>9
	U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699 * ___U3CU3E9_0;
	// System.Action MPAR.Desktop.UI.DesktopScriptMenu/<>c::<>9__31_1
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CU3E9__31_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__31_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_StaticFields, ___U3CU3E9__31_1_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CU3E9__31_1_1() const { return ___U3CU3E9__31_1_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CU3E9__31_1_1() { return &___U3CU3E9__31_1_1; }
	inline void set_U3CU3E9__31_1_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CU3E9__31_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__31_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_H
#ifndef U3CU3EC__DISPLAYCLASS28_0_T155264DCD7C45052F90FBF57790FEFBCDEC227B0_H
#define U3CU3EC__DISPLAYCLASS28_0_T155264DCD7C45052F90FBF57790FEFBCDEC227B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass28_0
struct  U3CU3Ec__DisplayClass28_0_t155264DCD7C45052F90FBF57790FEFBCDEC227B0  : public RuntimeObject
{
public:
	// MPAR.Common.Scripting.Script MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass28_0::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_0;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass28_0_t155264DCD7C45052F90FBF57790FEFBCDEC227B0, ___script_0)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_0() const { return ___script_0; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS28_0_T155264DCD7C45052F90FBF57790FEFBCDEC227B0_H
#ifndef U3CU3EC__DISPLAYCLASS31_0_T8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C_H
#define U3CU3EC__DISPLAYCLASS31_0_T8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass31_0
struct  U3CU3Ec__DisplayClass31_0_t8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C  : public RuntimeObject
{
public:
	// MPAR.Common.UI.IScriptMenuItem MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass31_0::script
	RuntimeObject* ___script_0;
	// MPAR.Desktop.UI.DesktopScriptMenu MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass31_0::<>4__this
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_script_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C, ___script_0)); }
	inline RuntimeObject* get_script_0() const { return ___script_0; }
	inline RuntimeObject** get_address_of_script_0() { return &___script_0; }
	inline void set_script_0(RuntimeObject* value)
	{
		___script_0 = value;
		Il2CppCodeGenWriteBarrier((&___script_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass31_0_t8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C, ___U3CU3E4__this_1)); }
	inline DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS31_0_T8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_TAFB515B86BCCC111A68F37A2130B25D3E40226E0_H
#define U3CU3EC__DISPLAYCLASS36_0_TAFB515B86BCCC111A68F37A2130B25D3E40226E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_tAFB515B86BCCC111A68F37A2130B25D3E40226E0  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo MPAR.Desktop.UI.DesktopScriptMenu/<>c__DisplayClass36_0::hit
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * ___hit_0;

public:
	inline static int32_t get_offset_of_hit_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_tAFB515B86BCCC111A68F37A2130B25D3E40226E0, ___hit_0)); }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * get_hit_0() const { return ___hit_0; }
	inline HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A ** get_address_of_hit_0() { return &___hit_0; }
	inline void set_hit_0(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A * value)
	{
		___hit_0 = value;
		Il2CppCodeGenWriteBarrier((&___hit_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_TAFB515B86BCCC111A68F37A2130B25D3E40226E0_H
#ifndef U3CU3EC__DISPLAYCLASS15_0_T18FFC7E93357D278C29B6D1880247147A8BA0512_H
#define U3CU3EC__DISPLAYCLASS15_0_T18FFC7E93357D278C29B6D1880247147A8BA0512_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenuItem/<>c__DisplayClass15_0
struct  U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512  : public RuntimeObject
{
public:
	// System.Boolean MPAR.Desktop.UI.DesktopScriptMenuItem/<>c__DisplayClass15_0::doneTop
	bool ___doneTop_0;
	// System.Boolean MPAR.Desktop.UI.DesktopScriptMenuItem/<>c__DisplayClass15_0::doneBot
	bool ___doneBot_1;

public:
	inline static int32_t get_offset_of_doneTop_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512, ___doneTop_0)); }
	inline bool get_doneTop_0() const { return ___doneTop_0; }
	inline bool* get_address_of_doneTop_0() { return &___doneTop_0; }
	inline void set_doneTop_0(bool value)
	{
		___doneTop_0 = value;
	}

	inline static int32_t get_offset_of_doneBot_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512, ___doneBot_1)); }
	inline bool get_doneBot_1() const { return ___doneBot_1; }
	inline bool* get_address_of_doneBot_1() { return &___doneBot_1; }
	inline void set_doneBot_1(bool value)
	{
		___doneBot_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS15_0_T18FFC7E93357D278C29B6D1880247147A8BA0512_H
#ifndef U3CMINIMIZEU3ED__15_T807F878E8B294E30B7F7968959FB273B5948E42B_H
#define U3CMINIMIZEU3ED__15_T807F878E8B294E30B7F7968959FB273B5948E42B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenuItem/<Minimize>d__15
struct  U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B  : public RuntimeObject
{
public:
	// System.Int32 MPAR.Desktop.UI.DesktopScriptMenuItem/<Minimize>d__15::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.Desktop.UI.DesktopScriptMenuItem/<Minimize>d__15::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// MPAR.Desktop.UI.DesktopScriptMenuItem MPAR.Desktop.UI.DesktopScriptMenuItem/<Minimize>d__15::<>4__this
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9 * ___U3CU3E4__this_2;
	// MPAR.Desktop.UI.DesktopScriptMenuItem/<>c__DisplayClass15_0 MPAR.Desktop.UI.DesktopScriptMenuItem/<Minimize>d__15::<>8__1
	U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512 * ___U3CU3E8__1_3;
	// System.Action MPAR.Desktop.UI.DesktopScriptMenuItem/<Minimize>d__15::callback
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___callback_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B, ___U3CU3E4__this_2)); }
	inline DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E8__1_3() { return static_cast<int32_t>(offsetof(U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B, ___U3CU3E8__1_3)); }
	inline U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512 * get_U3CU3E8__1_3() const { return ___U3CU3E8__1_3; }
	inline U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512 ** get_address_of_U3CU3E8__1_3() { return &___U3CU3E8__1_3; }
	inline void set_U3CU3E8__1_3(U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512 * value)
	{
		___U3CU3E8__1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E8__1_3), value);
	}

	inline static int32_t get_offset_of_callback_4() { return static_cast<int32_t>(offsetof(U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B, ___callback_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_callback_4() const { return ___callback_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_callback_4() { return &___callback_4; }
	inline void set_callback_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___callback_4 = value;
		Il2CppCodeGenWriteBarrier((&___callback_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMINIMIZEU3ED__15_T807F878E8B294E30B7F7968959FB273B5948E42B_H
#ifndef MPARHOLOLENSINPUTNAMESPACE_TEF06A42F49C571240196FE25A9CAEBE6EC3B24D3_H
#define MPARHOLOLENSINPUTNAMESPACE_TEF06A42F49C571240196FE25A9CAEBE6EC3B24D3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Hololens.Input.MPARHololensInputNamespace
struct  MPARHololensInputNamespace_tEF06A42F49C571240196FE25A9CAEBE6EC3B24D3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MPARHOLOLENSINPUTNAMESPACE_TEF06A42F49C571240196FE25A9CAEBE6EC3B24D3_H
#ifndef STRINGPROCESSING_T8FE20CC531054C78698F86E8FED36BE57FE00EB1_H
#define STRINGPROCESSING_T8FE20CC531054C78698F86E8FED36BE57FE00EB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Scripts.Common.Utility.StringProcessing
struct  StringProcessing_t8FE20CC531054C78698F86E8FED36BE57FE00EB1  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGPROCESSING_T8FE20CC531054C78698F86E8FED36BE57FE00EB1_H
#ifndef U3CU3EC__DISPLAYCLASS0_0_T7FC4D0FB48949358288F60EE613498DC5F38A585_H
#define U3CU3EC__DISPLAYCLASS0_0_T7FC4D0FB48949358288F60EE613498DC5F38A585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Scripts.Common.Utility.StringProcessing/<>c__DisplayClass0_0
struct  U3CU3Ec__DisplayClass0_0_t7FC4D0FB48949358288F60EE613498DC5F38A585  : public RuntimeObject
{
public:
	// System.String MPAR.Scripts.Common.Utility.StringProcessing/<>c__DisplayClass0_0::text
	String_t* ___text_0;

public:
	inline static int32_t get_offset_of_text_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass0_0_t7FC4D0FB48949358288F60EE613498DC5F38A585, ___text_0)); }
	inline String_t* get_text_0() const { return ___text_0; }
	inline String_t** get_address_of_text_0() { return &___text_0; }
	inline void set_text_0(String_t* value)
	{
		___text_0 = value;
		Il2CppCodeGenWriteBarrier((&___text_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS0_0_T7FC4D0FB48949358288F60EE613498DC5F38A585_H
#ifndef IOSSAVELOADMANAGER_T7EDB06D6FE87AC270BF12C3086950199501D8E2D_H
#define IOSSAVELOADMANAGER_T7EDB06D6FE87AC270BF12C3086950199501D8E2D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.iOS.Persistence.iOSSaveLoadManager
struct  iOSSaveLoadManager_t7EDB06D6FE87AC270BF12C3086950199501D8E2D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSAVELOADMANAGER_T7EDB06D6FE87AC270BF12C3086950199501D8E2D_H
#ifndef U3CLOADCACHEDSCRIPTTHUMBNAILSU3ED__7_T2DFCC15F515726A92AA41F92C59BCDEEB2163811_H
#define U3CLOADCACHEDSCRIPTTHUMBNAILSU3ED__7_T2DFCC15F515726A92AA41F92C59BCDEEB2163811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.iOS.Persistence.iOSSaveLoadManager/<LoadCachedScriptThumbnails>d__7
struct  U3CLoadCachedScriptThumbnailsU3Ed__7_t2DFCC15F515726A92AA41F92C59BCDEEB2163811  : public RuntimeObject
{
public:
	// System.Int32 MPAR.iOS.Persistence.iOSSaveLoadManager/<LoadCachedScriptThumbnails>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object MPAR.iOS.Persistence.iOSSaveLoadManager/<LoadCachedScriptThumbnails>d__7::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadCachedScriptThumbnailsU3Ed__7_t2DFCC15F515726A92AA41F92C59BCDEEB2163811, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadCachedScriptThumbnailsU3Ed__7_t2DFCC15F515726A92AA41F92C59BCDEEB2163811, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADCACHEDSCRIPTTHUMBNAILSU3ED__7_T2DFCC15F515726A92AA41F92C59BCDEEB2163811_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ASSETBUNDLELOADASSETOPERATION_T5A1401110A453E5BC738720B9A4E596235815463_H
#define ASSETBUNDLELOADASSETOPERATION_T5A1401110A453E5BC738720B9A4E596235815463_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadAssetOperation
struct  AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463  : public AssetBundleLoadOperation_t4E376230379EB84B5F4A6D7F957B791348E72141
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADASSETOPERATION_T5A1401110A453E5BC738720B9A4E596235815463_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef GUID_T_H
#define GUID_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Guid
struct  Guid_t 
{
public:
	// System.Int32 System.Guid::_a
	int32_t ____a_1;
	// System.Int16 System.Guid::_b
	int16_t ____b_2;
	// System.Int16 System.Guid::_c
	int16_t ____c_3;
	// System.Byte System.Guid::_d
	uint8_t ____d_4;
	// System.Byte System.Guid::_e
	uint8_t ____e_5;
	// System.Byte System.Guid::_f
	uint8_t ____f_6;
	// System.Byte System.Guid::_g
	uint8_t ____g_7;
	// System.Byte System.Guid::_h
	uint8_t ____h_8;
	// System.Byte System.Guid::_i
	uint8_t ____i_9;
	// System.Byte System.Guid::_j
	uint8_t ____j_10;
	// System.Byte System.Guid::_k
	uint8_t ____k_11;

public:
	inline static int32_t get_offset_of__a_1() { return static_cast<int32_t>(offsetof(Guid_t, ____a_1)); }
	inline int32_t get__a_1() const { return ____a_1; }
	inline int32_t* get_address_of__a_1() { return &____a_1; }
	inline void set__a_1(int32_t value)
	{
		____a_1 = value;
	}

	inline static int32_t get_offset_of__b_2() { return static_cast<int32_t>(offsetof(Guid_t, ____b_2)); }
	inline int16_t get__b_2() const { return ____b_2; }
	inline int16_t* get_address_of__b_2() { return &____b_2; }
	inline void set__b_2(int16_t value)
	{
		____b_2 = value;
	}

	inline static int32_t get_offset_of__c_3() { return static_cast<int32_t>(offsetof(Guid_t, ____c_3)); }
	inline int16_t get__c_3() const { return ____c_3; }
	inline int16_t* get_address_of__c_3() { return &____c_3; }
	inline void set__c_3(int16_t value)
	{
		____c_3 = value;
	}

	inline static int32_t get_offset_of__d_4() { return static_cast<int32_t>(offsetof(Guid_t, ____d_4)); }
	inline uint8_t get__d_4() const { return ____d_4; }
	inline uint8_t* get_address_of__d_4() { return &____d_4; }
	inline void set__d_4(uint8_t value)
	{
		____d_4 = value;
	}

	inline static int32_t get_offset_of__e_5() { return static_cast<int32_t>(offsetof(Guid_t, ____e_5)); }
	inline uint8_t get__e_5() const { return ____e_5; }
	inline uint8_t* get_address_of__e_5() { return &____e_5; }
	inline void set__e_5(uint8_t value)
	{
		____e_5 = value;
	}

	inline static int32_t get_offset_of__f_6() { return static_cast<int32_t>(offsetof(Guid_t, ____f_6)); }
	inline uint8_t get__f_6() const { return ____f_6; }
	inline uint8_t* get_address_of__f_6() { return &____f_6; }
	inline void set__f_6(uint8_t value)
	{
		____f_6 = value;
	}

	inline static int32_t get_offset_of__g_7() { return static_cast<int32_t>(offsetof(Guid_t, ____g_7)); }
	inline uint8_t get__g_7() const { return ____g_7; }
	inline uint8_t* get_address_of__g_7() { return &____g_7; }
	inline void set__g_7(uint8_t value)
	{
		____g_7 = value;
	}

	inline static int32_t get_offset_of__h_8() { return static_cast<int32_t>(offsetof(Guid_t, ____h_8)); }
	inline uint8_t get__h_8() const { return ____h_8; }
	inline uint8_t* get_address_of__h_8() { return &____h_8; }
	inline void set__h_8(uint8_t value)
	{
		____h_8 = value;
	}

	inline static int32_t get_offset_of__i_9() { return static_cast<int32_t>(offsetof(Guid_t, ____i_9)); }
	inline uint8_t get__i_9() const { return ____i_9; }
	inline uint8_t* get_address_of__i_9() { return &____i_9; }
	inline void set__i_9(uint8_t value)
	{
		____i_9 = value;
	}

	inline static int32_t get_offset_of__j_10() { return static_cast<int32_t>(offsetof(Guid_t, ____j_10)); }
	inline uint8_t get__j_10() const { return ____j_10; }
	inline uint8_t* get_address_of__j_10() { return &____j_10; }
	inline void set__j_10(uint8_t value)
	{
		____j_10 = value;
	}

	inline static int32_t get_offset_of__k_11() { return static_cast<int32_t>(offsetof(Guid_t, ____k_11)); }
	inline uint8_t get__k_11() const { return ____k_11; }
	inline uint8_t* get_address_of__k_11() { return &____k_11; }
	inline void set__k_11(uint8_t value)
	{
		____k_11 = value;
	}
};

struct Guid_t_StaticFields
{
public:
	// System.Guid System.Guid::Empty
	Guid_t  ___Empty_0;
	// System.Object System.Guid::_rngAccess
	RuntimeObject * ____rngAccess_12;
	// System.Security.Cryptography.RandomNumberGenerator System.Guid::_rng
	RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * ____rng_13;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ___Empty_0)); }
	inline Guid_t  get_Empty_0() const { return ___Empty_0; }
	inline Guid_t * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Guid_t  value)
	{
		___Empty_0 = value;
	}

	inline static int32_t get_offset_of__rngAccess_12() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rngAccess_12)); }
	inline RuntimeObject * get__rngAccess_12() const { return ____rngAccess_12; }
	inline RuntimeObject ** get_address_of__rngAccess_12() { return &____rngAccess_12; }
	inline void set__rngAccess_12(RuntimeObject * value)
	{
		____rngAccess_12 = value;
		Il2CppCodeGenWriteBarrier((&____rngAccess_12), value);
	}

	inline static int32_t get_offset_of__rng_13() { return static_cast<int32_t>(offsetof(Guid_t_StaticFields, ____rng_13)); }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * get__rng_13() const { return ____rng_13; }
	inline RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 ** get_address_of__rng_13() { return &____rng_13; }
	inline void set__rng_13(RandomNumberGenerator_t12277F7F965BA79C54E4B3BFABD27A5FFB725EE2 * value)
	{
		____rng_13 = value;
		Il2CppCodeGenWriteBarrier((&____rng_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUID_T_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARPLANEADDEDEVENTARGS_T90BDD222940131699290887385D2D531C4AC0BBC_H
#define ARPLANEADDEDEVENTARGS_T90BDD222940131699290887385D2D531C4AC0BBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs
struct  ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC 
{
public:
	// UnityEngine.XR.ARFoundation.ARPlane UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs::<plane>k__BackingField
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___U3CplaneU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CplaneU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC, ___U3CplaneU3Ek__BackingField_0)); }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * get_U3CplaneU3Ek__BackingField_0() const { return ___U3CplaneU3Ek__BackingField_0; }
	inline ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E ** get_address_of_U3CplaneU3Ek__BackingField_0() { return &___U3CplaneU3Ek__BackingField_0; }
	inline void set_U3CplaneU3Ek__BackingField_0(ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * value)
	{
		___U3CplaneU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CplaneU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs
struct ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC_marshaled_pinvoke
{
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___U3CplaneU3Ek__BackingField_0;
};
// Native definition for COM marshalling of UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs
struct ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC_marshaled_com
{
	ARPlane_t01E416A9DFD24A6A280437A9B8D1CECE0CF3DF0E * ___U3CplaneU3Ek__BackingField_0;
};
#endif // ARPLANEADDEDEVENTARGS_T90BDD222940131699290887385D2D531C4AC0BBC_H
#ifndef ASSETBUNDLELOADASSETOPERATIONFULL_T999E5BC7463088826907F2CEFB7FB10A35EA96A0_H
#define ASSETBUNDLELOADASSETOPERATIONFULL_T999E5BC7463088826907F2CEFB7FB10A35EA96A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadAssetOperationFull
struct  AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0  : public AssetBundleLoadAssetOperation_t5A1401110A453E5BC738720B9A4E596235815463
{
public:
	// System.String AssetBundles.AssetBundleLoadAssetOperationFull::m_AssetBundleName
	String_t* ___m_AssetBundleName_0;
	// System.String AssetBundles.AssetBundleLoadAssetOperationFull::m_AssetName
	String_t* ___m_AssetName_1;
	// System.String AssetBundles.AssetBundleLoadAssetOperationFull::m_DownloadingError
	String_t* ___m_DownloadingError_2;
	// System.Type AssetBundles.AssetBundleLoadAssetOperationFull::m_Type
	Type_t * ___m_Type_3;
	// UnityEngine.AssetBundleRequest AssetBundles.AssetBundleLoadAssetOperationFull::m_Request
	AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * ___m_Request_4;

public:
	inline static int32_t get_offset_of_m_AssetBundleName_0() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_AssetBundleName_0)); }
	inline String_t* get_m_AssetBundleName_0() const { return ___m_AssetBundleName_0; }
	inline String_t** get_address_of_m_AssetBundleName_0() { return &___m_AssetBundleName_0; }
	inline void set_m_AssetBundleName_0(String_t* value)
	{
		___m_AssetBundleName_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetBundleName_0), value);
	}

	inline static int32_t get_offset_of_m_AssetName_1() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_AssetName_1)); }
	inline String_t* get_m_AssetName_1() const { return ___m_AssetName_1; }
	inline String_t** get_address_of_m_AssetName_1() { return &___m_AssetName_1; }
	inline void set_m_AssetName_1(String_t* value)
	{
		___m_AssetName_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetName_1), value);
	}

	inline static int32_t get_offset_of_m_DownloadingError_2() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_DownloadingError_2)); }
	inline String_t* get_m_DownloadingError_2() const { return ___m_DownloadingError_2; }
	inline String_t** get_address_of_m_DownloadingError_2() { return &___m_DownloadingError_2; }
	inline void set_m_DownloadingError_2(String_t* value)
	{
		___m_DownloadingError_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadingError_2), value);
	}

	inline static int32_t get_offset_of_m_Type_3() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_Type_3)); }
	inline Type_t * get_m_Type_3() const { return ___m_Type_3; }
	inline Type_t ** get_address_of_m_Type_3() { return &___m_Type_3; }
	inline void set_m_Type_3(Type_t * value)
	{
		___m_Type_3 = value;
		Il2CppCodeGenWriteBarrier((&___m_Type_3), value);
	}

	inline static int32_t get_offset_of_m_Request_4() { return static_cast<int32_t>(offsetof(AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0, ___m_Request_4)); }
	inline AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * get_m_Request_4() const { return ___m_Request_4; }
	inline AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 ** get_address_of_m_Request_4() { return &___m_Request_4; }
	inline void set_m_Request_4(AssetBundleRequest_tF8897443AF1FC6C3E04911FAA9EDAE89B0A0A962 * value)
	{
		___m_Request_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_Request_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADASSETOPERATIONFULL_T999E5BC7463088826907F2CEFB7FB10A35EA96A0_H
#ifndef LOGMODE_TCB0B04DDADD0871432F2D368D237ED071FE60785_H
#define LOGMODE_TCB0B04DDADD0871432F2D368D237ED071FE60785_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/LogMode
struct  LogMode_tCB0B04DDADD0871432F2D368D237ED071FE60785 
{
public:
	// System.Int32 AssetBundles.AssetBundleManager/LogMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogMode_tCB0B04DDADD0871432F2D368D237ED071FE60785, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGMODE_TCB0B04DDADD0871432F2D368D237ED071FE60785_H
#ifndef LOGTYPE_T223D0A938BDA956016978CB18B9065559DAF0123_H
#define LOGTYPE_T223D0A938BDA956016978CB18B9065559DAF0123_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager/LogType
struct  LogType_t223D0A938BDA956016978CB18B9065559DAF0123 
{
public:
	// System.Int32 AssetBundles.AssetBundleManager/LogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogType_t223D0A938BDA956016978CB18B9065559DAF0123, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T223D0A938BDA956016978CB18B9065559DAF0123_H
#ifndef MODERATIONSTATUS_T792C26BC188A4F75004604C2E67FFDBE186F1158_H
#define MODERATIONSTATUS_T792C26BC188A4F75004604C2E67FFDBE186F1158_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Entities.ModerationStatus
struct  ModerationStatus_t792C26BC188A4F75004604C2E67FFDBE186F1158 
{
public:
	// System.Int32 Assets._Project.Scripts.Common.Entities.ModerationStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ModerationStatus_t792C26BC188A4F75004604C2E67FFDBE186F1158, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODERATIONSTATUS_T792C26BC188A4F75004604C2E67FFDBE186F1158_H
#ifndef U3CU3EC__DISPLAYCLASS3_0_T93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427_H
#define U3CU3EC__DISPLAYCLASS3_0_T93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c__DisplayClass3_0
struct  U3CU3Ec__DisplayClass3_0_t93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427  : public RuntimeObject
{
public:
	// UnityEngine.XR.ARFoundation.ARPlaneAddedEventArgs Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c__DisplayClass3_0::arPlane
	ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC  ___arPlane_0;
	// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector/<>c__DisplayClass3_0::<>4__this
	MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_arPlane_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427, ___arPlane_0)); }
	inline ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC  get_arPlane_0() const { return ___arPlane_0; }
	inline ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC * get_address_of_arPlane_0() { return &___arPlane_0; }
	inline void set_arPlane_0(ARPlaneAddedEventArgs_t90BDD222940131699290887385D2D531C4AC0BBC  value)
	{
		___arPlane_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass3_0_t93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427, ___U3CU3E4__this_1)); }
	inline MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS3_0_T93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427_H
#ifndef MXBEHAVIOUR_T3E68364A53773E8F9171FB9E596724746F3B7B79_H
#define MXBEHAVIOUR_T3E68364A53773E8F9171FB9E596724746F3B7B79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Behaviours.MxBehaviour
struct  MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79  : public RuntimeObject
{
public:
	// Jint.Native.JsValue Assets._Project.Scripts.Common.Scripting.Behaviours.MxBehaviour::<JsObject>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CJsObjectU3Ek__BackingField_0;
	// System.Guid Assets._Project.Scripts.Common.Scripting.Behaviours.MxBehaviour::<uniqueId>k__BackingField
	Guid_t  ___U3CuniqueIdU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CJsObjectU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79, ___U3CJsObjectU3Ek__BackingField_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CJsObjectU3Ek__BackingField_0() const { return ___U3CJsObjectU3Ek__BackingField_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CJsObjectU3Ek__BackingField_0() { return &___U3CJsObjectU3Ek__BackingField_0; }
	inline void set_U3CJsObjectU3Ek__BackingField_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CJsObjectU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJsObjectU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CuniqueIdU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79, ___U3CuniqueIdU3Ek__BackingField_2)); }
	inline Guid_t  get_U3CuniqueIdU3Ek__BackingField_2() const { return ___U3CuniqueIdU3Ek__BackingField_2; }
	inline Guid_t * get_address_of_U3CuniqueIdU3Ek__BackingField_2() { return &___U3CuniqueIdU3Ek__BackingField_2; }
	inline void set_U3CuniqueIdU3Ek__BackingField_2(Guid_t  value)
	{
		___U3CuniqueIdU3Ek__BackingField_2 = value;
	}
};

struct MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79_StaticFields
{
public:
	// Jint.Engine Assets._Project.Scripts.Common.Scripting.Behaviours.MxBehaviour::engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___engine_1;

public:
	inline static int32_t get_offset_of_engine_1() { return static_cast<int32_t>(offsetof(MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79_StaticFields, ___engine_1)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_engine_1() const { return ___engine_1; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_engine_1() { return &___engine_1; }
	inline void set_engine_1(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___engine_1 = value;
		Il2CppCodeGenWriteBarrier((&___engine_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MXBEHAVIOUR_T3E68364A53773E8F9171FB9E596724746F3B7B79_H
#ifndef SWIPEDIRECTION_TD34EFC5A16E92E67EA1CD5BC23329CB864DED19E_H
#define SWIPEDIRECTION_TD34EFC5A16E92E67EA1CD5BC23329CB864DED19E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.TabSwipeManager/SwipeDirection
struct  SwipeDirection_tD34EFC5A16E92E67EA1CD5BC23329CB864DED19E 
{
public:
	// System.Int32 Assets._Project.Scripts.Common.UI.TabSwipeManager/SwipeDirection::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SwipeDirection_tD34EFC5A16E92E67EA1CD5BC23329CB864DED19E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWIPEDIRECTION_TD34EFC5A16E92E67EA1CD5BC23329CB864DED19E_H
#ifndef DESKTOPINPUTMODE_T57A0297EC964512EA2A48822CFDD58120B29EB39_H
#define DESKTOPINPUTMODE_T57A0297EC964512EA2A48822CFDD58120B29EB39_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Input.DesktopInput/DesktopInputMode
struct  DesktopInputMode_t57A0297EC964512EA2A48822CFDD58120B29EB39 
{
public:
	// System.Int32 MPAR.Desktop.Input.DesktopInput/DesktopInputMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DesktopInputMode_t57A0297EC964512EA2A48822CFDD58120B29EB39, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPINPUTMODE_T57A0297EC964512EA2A48822CFDD58120B29EB39_H
#ifndef ROTATIONAXES_T26EFFD53B50428B38246BFBAC1D2F52E90286B14_H
#define ROTATIONAXES_T26EFFD53B50428B38246BFBAC1D2F52E90286B14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Input.SmoothMouseLook/RotationAxes
struct  RotationAxes_t26EFFD53B50428B38246BFBAC1D2F52E90286B14 
{
public:
	// System.Int32 MPAR.Desktop.Input.SmoothMouseLook/RotationAxes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RotationAxes_t26EFFD53B50428B38246BFBAC1D2F52E90286B14, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ROTATIONAXES_T26EFFD53B50428B38246BFBAC1D2F52E90286B14_H
#ifndef NULLABLE_1_TAF01623AB359AB6D460A6F432BF98EA08C7F9C60_H
#define NULLABLE_1_TAF01623AB359AB6D460A6F432BF98EA08C7F9C60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Color>
struct  Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60 
{
public:
	// T System.Nullable`1::value
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60, ___value_0)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_value_0() const { return ___value_0; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TAF01623AB359AB6D460A6F432BF98EA08C7F9C60_H
#ifndef KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#define KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.KeyCode
struct  KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C 
{
public:
	// System.Int32 UnityEngine.KeyCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(KeyCode_tC93EA87C5A6901160B583ADFCD3EF6726570DC3C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYCODE_TC93EA87C5A6901160B583ADFCD3EF6726570DC3C_H
#ifndef LOGTYPE_T6B6C6234E8B44B73937581ACFBE15DE28227849D_H
#define LOGTYPE_T6B6C6234E8B44B73937581ACFBE15DE28227849D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LogType
struct  LogType_t6B6C6234E8B44B73937581ACFBE15DE28227849D 
{
public:
	// System.Int32 UnityEngine.LogType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogType_t6B6C6234E8B44B73937581ACFBE15DE28227849D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGTYPE_T6B6C6234E8B44B73937581ACFBE15DE28227849D_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#define RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.RaycastHit
struct  RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 
{
public:
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Point
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Point_0;
	// UnityEngine.Vector3 UnityEngine.RaycastHit::m_Normal
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_Normal_1;
	// System.UInt32 UnityEngine.RaycastHit::m_FaceID
	uint32_t ___m_FaceID_2;
	// System.Single UnityEngine.RaycastHit::m_Distance
	float ___m_Distance_3;
	// UnityEngine.Vector2 UnityEngine.RaycastHit::m_UV
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___m_UV_4;
	// System.Int32 UnityEngine.RaycastHit::m_Collider
	int32_t ___m_Collider_5;

public:
	inline static int32_t get_offset_of_m_Point_0() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Point_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Point_0() const { return ___m_Point_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Point_0() { return &___m_Point_0; }
	inline void set_m_Point_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Point_0 = value;
	}

	inline static int32_t get_offset_of_m_Normal_1() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Normal_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_Normal_1() const { return ___m_Normal_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_Normal_1() { return &___m_Normal_1; }
	inline void set_m_Normal_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_Normal_1 = value;
	}

	inline static int32_t get_offset_of_m_FaceID_2() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_FaceID_2)); }
	inline uint32_t get_m_FaceID_2() const { return ___m_FaceID_2; }
	inline uint32_t* get_address_of_m_FaceID_2() { return &___m_FaceID_2; }
	inline void set_m_FaceID_2(uint32_t value)
	{
		___m_FaceID_2 = value;
	}

	inline static int32_t get_offset_of_m_Distance_3() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Distance_3)); }
	inline float get_m_Distance_3() const { return ___m_Distance_3; }
	inline float* get_address_of_m_Distance_3() { return &___m_Distance_3; }
	inline void set_m_Distance_3(float value)
	{
		___m_Distance_3 = value;
	}

	inline static int32_t get_offset_of_m_UV_4() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_UV_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_m_UV_4() const { return ___m_UV_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_m_UV_4() { return &___m_UV_4; }
	inline void set_m_UV_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___m_UV_4 = value;
	}

	inline static int32_t get_offset_of_m_Collider_5() { return static_cast<int32_t>(offsetof(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3, ___m_Collider_5)); }
	inline int32_t get_m_Collider_5() const { return ___m_Collider_5; }
	inline int32_t* get_address_of_m_Collider_5() { return &___m_Collider_5; }
	inline void set_m_Collider_5(int32_t value)
	{
		___m_Collider_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RAYCASTHIT_T19695F18F9265FE5425062BBA6A4D330480538C3_H
#ifndef ASSETBUNDLELOADMANIFESTOPERATION_T6E6263963D4775195C2ACC0196BABEF3678BE214_H
#define ASSETBUNDLELOADMANIFESTOPERATION_T6E6263963D4775195C2ACC0196BABEF3678BE214_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleLoadManifestOperation
struct  AssetBundleLoadManifestOperation_t6E6263963D4775195C2ACC0196BABEF3678BE214  : public AssetBundleLoadAssetOperationFull_t999E5BC7463088826907F2CEFB7FB10A35EA96A0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLELOADMANIFESTOPERATION_T6E6263963D4775195C2ACC0196BABEF3678BE214_H
#ifndef HITINFO_TF328EFA7278C1055AD8E27406D8BF952B00FF96A_H
#define HITINFO_TF328EFA7278C1055AD8E27406D8BF952B00FF96A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo
struct  HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A  : public RuntimeObject
{
public:
	// UnityEngine.RaycastHit Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo::<Hit>k__BackingField
	RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  ___U3CHitU3Ek__BackingField_0;
	// UnityEngine.Vector3 Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo::<Origin>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3COriginU3Ek__BackingField_1;
	// UnityEngine.Vector3 Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo::<Direction>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CDirectionU3Ek__BackingField_2;
	// System.Boolean Assets._Project.Scripts.Common.Scripting.API.Physics.HitInfo::<DidHit>k__BackingField
	bool ___U3CDidHitU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CHitU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A, ___U3CHitU3Ek__BackingField_0)); }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  get_U3CHitU3Ek__BackingField_0() const { return ___U3CHitU3Ek__BackingField_0; }
	inline RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3 * get_address_of_U3CHitU3Ek__BackingField_0() { return &___U3CHitU3Ek__BackingField_0; }
	inline void set_U3CHitU3Ek__BackingField_0(RaycastHit_t19695F18F9265FE5425062BBA6A4D330480538C3  value)
	{
		___U3CHitU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3COriginU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A, ___U3COriginU3Ek__BackingField_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3COriginU3Ek__BackingField_1() const { return ___U3COriginU3Ek__BackingField_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3COriginU3Ek__BackingField_1() { return &___U3COriginU3Ek__BackingField_1; }
	inline void set_U3COriginU3Ek__BackingField_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3COriginU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDirectionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A, ___U3CDirectionU3Ek__BackingField_2)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CDirectionU3Ek__BackingField_2() const { return ___U3CDirectionU3Ek__BackingField_2; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CDirectionU3Ek__BackingField_2() { return &___U3CDirectionU3Ek__BackingField_2; }
	inline void set_U3CDirectionU3Ek__BackingField_2(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CDirectionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDidHitU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A, ___U3CDidHitU3Ek__BackingField_3)); }
	inline bool get_U3CDidHitU3Ek__BackingField_3() const { return ___U3CDidHitU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CDidHitU3Ek__BackingField_3() { return &___U3CDidHitU3Ek__BackingField_3; }
	inline void set_U3CDidHitU3Ek__BackingField_3(bool value)
	{
		___U3CDidHitU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HITINFO_TF328EFA7278C1055AD8E27406D8BF952B00FF96A_H
#ifndef LOG_TD2E7630AA73F4471F582812E4BC4DFAB6BA79788_H
#define LOG_TD2E7630AA73F4471F582812E4BC4DFAB6BA79788_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.InGameConsole/Log
struct  Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788 
{
public:
	// System.String Assets._Project.Scripts.Common.UI.InGameConsole/Log::message
	String_t* ___message_0;
	// System.String Assets._Project.Scripts.Common.UI.InGameConsole/Log::stackTrace
	String_t* ___stackTrace_1;
	// UnityEngine.LogType Assets._Project.Scripts.Common.UI.InGameConsole/Log::type
	int32_t ___type_2;

public:
	inline static int32_t get_offset_of_message_0() { return static_cast<int32_t>(offsetof(Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788, ___message_0)); }
	inline String_t* get_message_0() const { return ___message_0; }
	inline String_t** get_address_of_message_0() { return &___message_0; }
	inline void set_message_0(String_t* value)
	{
		___message_0 = value;
		Il2CppCodeGenWriteBarrier((&___message_0), value);
	}

	inline static int32_t get_offset_of_stackTrace_1() { return static_cast<int32_t>(offsetof(Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788, ___stackTrace_1)); }
	inline String_t* get_stackTrace_1() const { return ___stackTrace_1; }
	inline String_t** get_address_of_stackTrace_1() { return &___stackTrace_1; }
	inline void set_stackTrace_1(String_t* value)
	{
		___stackTrace_1 = value;
		Il2CppCodeGenWriteBarrier((&___stackTrace_1), value);
	}

	inline static int32_t get_offset_of_type_2() { return static_cast<int32_t>(offsetof(Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788, ___type_2)); }
	inline int32_t get_type_2() const { return ___type_2; }
	inline int32_t* get_address_of_type_2() { return &___type_2; }
	inline void set_type_2(int32_t value)
	{
		___type_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Assets._Project.Scripts.Common.UI.InGameConsole/Log
struct Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788_marshaled_pinvoke
{
	char* ___message_0;
	char* ___stackTrace_1;
	int32_t ___type_2;
};
// Native definition for COM marshalling of Assets._Project.Scripts.Common.UI.InGameConsole/Log
struct Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788_marshaled_com
{
	Il2CppChar* ___message_0;
	Il2CppChar* ___stackTrace_1;
	int32_t ___type_2;
};
#endif // LOG_TD2E7630AA73F4471F582812E4BC4DFAB6BA79788_H
#ifndef SCRIPTMENUITEM_T09C9AA1C566D046542CC5EF6008079EF4BBF2488_H
#define SCRIPTMENUITEM_T09C9AA1C566D046542CC5EF6008079EF4BBF2488_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem
struct  ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488  : public RuntimeObject
{
public:
	// Assets._Project.Scripts.Common.Scripting.API.ICustomMenu Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::menu
	RuntimeObject* ___menu_0;
	// System.String Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::_key
	String_t* ____key_1;
	// System.String Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::_text
	String_t* ____text_2;
	// System.Nullable`1<UnityEngine.Color> Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::_color
	Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60  ____color_3;
	// System.String Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::_icon
	String_t* ____icon_4;
	// System.Action Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::rawAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___rawAction_5;
	// System.Func`3<Jint.Native.JsValue,Jint.Native.JsValue[],Jint.Native.JsValue> Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::_action
	Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * ____action_6;
	// UnityEngine.Texture2D Assets._Project.Scripts.Common.UI.InScriptMenu.ScriptMenuItem::iconTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___iconTexture_7;

public:
	inline static int32_t get_offset_of_menu_0() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ___menu_0)); }
	inline RuntimeObject* get_menu_0() const { return ___menu_0; }
	inline RuntimeObject** get_address_of_menu_0() { return &___menu_0; }
	inline void set_menu_0(RuntimeObject* value)
	{
		___menu_0 = value;
		Il2CppCodeGenWriteBarrier((&___menu_0), value);
	}

	inline static int32_t get_offset_of__key_1() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ____key_1)); }
	inline String_t* get__key_1() const { return ____key_1; }
	inline String_t** get_address_of__key_1() { return &____key_1; }
	inline void set__key_1(String_t* value)
	{
		____key_1 = value;
		Il2CppCodeGenWriteBarrier((&____key_1), value);
	}

	inline static int32_t get_offset_of__text_2() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ____text_2)); }
	inline String_t* get__text_2() const { return ____text_2; }
	inline String_t** get_address_of__text_2() { return &____text_2; }
	inline void set__text_2(String_t* value)
	{
		____text_2 = value;
		Il2CppCodeGenWriteBarrier((&____text_2), value);
	}

	inline static int32_t get_offset_of__color_3() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ____color_3)); }
	inline Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60  get__color_3() const { return ____color_3; }
	inline Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60 * get_address_of__color_3() { return &____color_3; }
	inline void set__color_3(Nullable_1_tAF01623AB359AB6D460A6F432BF98EA08C7F9C60  value)
	{
		____color_3 = value;
	}

	inline static int32_t get_offset_of__icon_4() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ____icon_4)); }
	inline String_t* get__icon_4() const { return ____icon_4; }
	inline String_t** get_address_of__icon_4() { return &____icon_4; }
	inline void set__icon_4(String_t* value)
	{
		____icon_4 = value;
		Il2CppCodeGenWriteBarrier((&____icon_4), value);
	}

	inline static int32_t get_offset_of_rawAction_5() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ___rawAction_5)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_rawAction_5() const { return ___rawAction_5; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_rawAction_5() { return &___rawAction_5; }
	inline void set_rawAction_5(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___rawAction_5 = value;
		Il2CppCodeGenWriteBarrier((&___rawAction_5), value);
	}

	inline static int32_t get_offset_of__action_6() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ____action_6)); }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * get__action_6() const { return ____action_6; }
	inline Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 ** get_address_of__action_6() { return &____action_6; }
	inline void set__action_6(Func_3_t322849DE09CA471F3BD5C1D8017DA45DB3053039 * value)
	{
		____action_6 = value;
		Il2CppCodeGenWriteBarrier((&____action_6), value);
	}

	inline static int32_t get_offset_of_iconTexture_7() { return static_cast<int32_t>(offsetof(ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488, ___iconTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get_iconTexture_7() const { return ___iconTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of_iconTexture_7() { return &___iconTexture_7; }
	inline void set_iconTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		___iconTexture_7 = value;
		Il2CppCodeGenWriteBarrier((&___iconTexture_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTMENUITEM_T09C9AA1C566D046542CC5EF6008079EF4BBF2488_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef ASSETBUNDLEMANAGER_T258145CFBD9046E0F391FC4D4866BB084F75F36E_H
#define ASSETBUNDLEMANAGER_T258145CFBD9046E0F391FC4D4866BB084F75F36E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AssetBundles.AssetBundleManager
struct  AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields
{
public:
	// AssetBundles.AssetBundleManager/LogMode AssetBundles.AssetBundleManager::m_LogMode
	int32_t ___m_LogMode_4;
	// System.String AssetBundles.AssetBundleManager::m_BaseDownloadingURL
	String_t* ___m_BaseDownloadingURL_5;
	// System.String[] AssetBundles.AssetBundleManager::m_ActiveVariants
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___m_ActiveVariants_6;
	// UnityEngine.AssetBundleManifest AssetBundles.AssetBundleManager::m_AssetBundleManifest
	AssetBundleManifest_t9EC1369A72D8DA0E0DECA5B63F9CF25E1053D52E * ___m_AssetBundleManifest_7;
	// System.Collections.Generic.Dictionary`2<System.String,AssetBundles.LoadedAssetBundle> AssetBundles.AssetBundleManager::m_LoadedAssetBundles
	Dictionary_2_t423F9AC35E72449025C6C437DF2B805B95B1E4C0 * ___m_LoadedAssetBundles_8;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.WWW> AssetBundles.AssetBundleManager::m_DownloadingWWWs
	Dictionary_2_tBA261CA38D4514CCDC4740A3A55FFF31DDDB9B9E * ___m_DownloadingWWWs_9;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> AssetBundles.AssetBundleManager::m_DownloadingErrors
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___m_DownloadingErrors_10;
	// System.Collections.Generic.List`1<AssetBundles.AssetBundleLoadOperation> AssetBundles.AssetBundleManager::m_InProgressOperations
	List_1_t1B89723079ECBCFC2B8933A3A4EC51797BF343F3 * ___m_InProgressOperations_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> AssetBundles.AssetBundleManager::m_Dependencies
	Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * ___m_Dependencies_12;

public:
	inline static int32_t get_offset_of_m_LogMode_4() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_LogMode_4)); }
	inline int32_t get_m_LogMode_4() const { return ___m_LogMode_4; }
	inline int32_t* get_address_of_m_LogMode_4() { return &___m_LogMode_4; }
	inline void set_m_LogMode_4(int32_t value)
	{
		___m_LogMode_4 = value;
	}

	inline static int32_t get_offset_of_m_BaseDownloadingURL_5() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_BaseDownloadingURL_5)); }
	inline String_t* get_m_BaseDownloadingURL_5() const { return ___m_BaseDownloadingURL_5; }
	inline String_t** get_address_of_m_BaseDownloadingURL_5() { return &___m_BaseDownloadingURL_5; }
	inline void set_m_BaseDownloadingURL_5(String_t* value)
	{
		___m_BaseDownloadingURL_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_BaseDownloadingURL_5), value);
	}

	inline static int32_t get_offset_of_m_ActiveVariants_6() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_ActiveVariants_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_m_ActiveVariants_6() const { return ___m_ActiveVariants_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_m_ActiveVariants_6() { return &___m_ActiveVariants_6; }
	inline void set_m_ActiveVariants_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___m_ActiveVariants_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_ActiveVariants_6), value);
	}

	inline static int32_t get_offset_of_m_AssetBundleManifest_7() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_AssetBundleManifest_7)); }
	inline AssetBundleManifest_t9EC1369A72D8DA0E0DECA5B63F9CF25E1053D52E * get_m_AssetBundleManifest_7() const { return ___m_AssetBundleManifest_7; }
	inline AssetBundleManifest_t9EC1369A72D8DA0E0DECA5B63F9CF25E1053D52E ** get_address_of_m_AssetBundleManifest_7() { return &___m_AssetBundleManifest_7; }
	inline void set_m_AssetBundleManifest_7(AssetBundleManifest_t9EC1369A72D8DA0E0DECA5B63F9CF25E1053D52E * value)
	{
		___m_AssetBundleManifest_7 = value;
		Il2CppCodeGenWriteBarrier((&___m_AssetBundleManifest_7), value);
	}

	inline static int32_t get_offset_of_m_LoadedAssetBundles_8() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_LoadedAssetBundles_8)); }
	inline Dictionary_2_t423F9AC35E72449025C6C437DF2B805B95B1E4C0 * get_m_LoadedAssetBundles_8() const { return ___m_LoadedAssetBundles_8; }
	inline Dictionary_2_t423F9AC35E72449025C6C437DF2B805B95B1E4C0 ** get_address_of_m_LoadedAssetBundles_8() { return &___m_LoadedAssetBundles_8; }
	inline void set_m_LoadedAssetBundles_8(Dictionary_2_t423F9AC35E72449025C6C437DF2B805B95B1E4C0 * value)
	{
		___m_LoadedAssetBundles_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_LoadedAssetBundles_8), value);
	}

	inline static int32_t get_offset_of_m_DownloadingWWWs_9() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_DownloadingWWWs_9)); }
	inline Dictionary_2_tBA261CA38D4514CCDC4740A3A55FFF31DDDB9B9E * get_m_DownloadingWWWs_9() const { return ___m_DownloadingWWWs_9; }
	inline Dictionary_2_tBA261CA38D4514CCDC4740A3A55FFF31DDDB9B9E ** get_address_of_m_DownloadingWWWs_9() { return &___m_DownloadingWWWs_9; }
	inline void set_m_DownloadingWWWs_9(Dictionary_2_tBA261CA38D4514CCDC4740A3A55FFF31DDDB9B9E * value)
	{
		___m_DownloadingWWWs_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadingWWWs_9), value);
	}

	inline static int32_t get_offset_of_m_DownloadingErrors_10() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_DownloadingErrors_10)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_m_DownloadingErrors_10() const { return ___m_DownloadingErrors_10; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_m_DownloadingErrors_10() { return &___m_DownloadingErrors_10; }
	inline void set_m_DownloadingErrors_10(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___m_DownloadingErrors_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_DownloadingErrors_10), value);
	}

	inline static int32_t get_offset_of_m_InProgressOperations_11() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_InProgressOperations_11)); }
	inline List_1_t1B89723079ECBCFC2B8933A3A4EC51797BF343F3 * get_m_InProgressOperations_11() const { return ___m_InProgressOperations_11; }
	inline List_1_t1B89723079ECBCFC2B8933A3A4EC51797BF343F3 ** get_address_of_m_InProgressOperations_11() { return &___m_InProgressOperations_11; }
	inline void set_m_InProgressOperations_11(List_1_t1B89723079ECBCFC2B8933A3A4EC51797BF343F3 * value)
	{
		___m_InProgressOperations_11 = value;
		Il2CppCodeGenWriteBarrier((&___m_InProgressOperations_11), value);
	}

	inline static int32_t get_offset_of_m_Dependencies_12() { return static_cast<int32_t>(offsetof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields, ___m_Dependencies_12)); }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * get_m_Dependencies_12() const { return ___m_Dependencies_12; }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 ** get_address_of_m_Dependencies_12() { return &___m_Dependencies_12; }
	inline void set_m_Dependencies_12(Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * value)
	{
		___m_Dependencies_12 = value;
		Il2CppCodeGenWriteBarrier((&___m_Dependencies_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSETBUNDLEMANAGER_T258145CFBD9046E0F391FC4D4866BB084F75F36E_H
#ifndef SHOWDIRECTIONTOMENU_T2529D91B48E100BC55129AAA64EA295E26968E15_H
#define SHOWDIRECTIONTOMENU_T2529D91B48E100BC55129AAA64EA295E26968E15_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.ShowDirectionToMenu
struct  ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Assets.ShowDirectionToMenu::Arrow
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Arrow_4;
	// UnityEngine.GameObject Assets.ShowDirectionToMenu::target
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___target_5;
	// System.Boolean Assets.ShowDirectionToMenu::found
	bool ___found_6;
	// MPAR.Common.Scripting.ScriptManager Assets.ShowDirectionToMenu::scriptManager
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * ___scriptManager_7;
	// UnityEngine.GameObject[] Assets.ShowDirectionToMenu::scriptMenuItems
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___scriptMenuItems_8;

public:
	inline static int32_t get_offset_of_Arrow_4() { return static_cast<int32_t>(offsetof(ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15, ___Arrow_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Arrow_4() const { return ___Arrow_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Arrow_4() { return &___Arrow_4; }
	inline void set_Arrow_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Arrow_4 = value;
		Il2CppCodeGenWriteBarrier((&___Arrow_4), value);
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15, ___target_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_target_5() const { return ___target_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_found_6() { return static_cast<int32_t>(offsetof(ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15, ___found_6)); }
	inline bool get_found_6() const { return ___found_6; }
	inline bool* get_address_of_found_6() { return &___found_6; }
	inline void set_found_6(bool value)
	{
		___found_6 = value;
	}

	inline static int32_t get_offset_of_scriptManager_7() { return static_cast<int32_t>(offsetof(ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15, ___scriptManager_7)); }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * get_scriptManager_7() const { return ___scriptManager_7; }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C ** get_address_of_scriptManager_7() { return &___scriptManager_7; }
	inline void set_scriptManager_7(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * value)
	{
		___scriptManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___scriptManager_7), value);
	}

	inline static int32_t get_offset_of_scriptMenuItems_8() { return static_cast<int32_t>(offsetof(ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15, ___scriptMenuItems_8)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_scriptMenuItems_8() const { return ___scriptMenuItems_8; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_scriptMenuItems_8() { return &___scriptMenuItems_8; }
	inline void set_scriptMenuItems_8(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___scriptMenuItems_8 = value;
		Il2CppCodeGenWriteBarrier((&___scriptMenuItems_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOWDIRECTIONTOMENU_T2529D91B48E100BC55129AAA64EA295E26968E15_H
#ifndef MOBILEINPUT_TDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0_H
#define MOBILEINPUT_TDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.MobileInput.MobileInput
struct  MobileInput_tDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Input.IPlatformDependentInputListener> Assets._Project.Scripts.Common.MobileInput.MobileInput::listeners
	List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * ___listeners_4;
	// System.Boolean Assets._Project.Scripts.Common.MobileInput.MobileInput::dragging
	bool ___dragging_5;

public:
	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(MobileInput_tDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0, ___listeners_4)); }
	inline List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * get_listeners_4() const { return ___listeners_4; }
	inline List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_4), value);
	}

	inline static int32_t get_offset_of_dragging_5() { return static_cast<int32_t>(offsetof(MobileInput_tDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0, ___dragging_5)); }
	inline bool get_dragging_5() const { return ___dragging_5; }
	inline bool* get_address_of_dragging_5() { return &___dragging_5; }
	inline void set_dragging_5(bool value)
	{
		___dragging_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEINPUT_TDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0_H
#ifndef PLATFORMDEPENDENTPLANEDETECTOR_TA059BB399BF4739A6CA16E50E3B2581C47FB6C1F_H
#define PLATFORMDEPENDENTPLANEDETECTOR_TA059BB399BF4739A6CA16E50E3B2581C47FB6C1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.PlaneDetection.PlatformDependentPlaneDetector
struct  PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMDEPENDENTPLANEDETECTOR_TA059BB399BF4739A6CA16E50E3B2581C47FB6C1F_H
#ifndef EMPTYPROJECTLOADER_T3A80B5D6896126CAF14DFBF7E63CA6B741EB94E0_H
#define EMPTYPROJECTLOADER_T3A80B5D6896126CAF14DFBF7E63CA6B741EB94E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Loader.EmptyProjectLoader
struct  EmptyProjectLoader_t3A80B5D6896126CAF14DFBF7E63CA6B741EB94E0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYPROJECTLOADER_T3A80B5D6896126CAF14DFBF7E63CA6B741EB94E0_H
#ifndef QRCODEPROJECTLOADER_T18953784222E70AB5CBA0919B6ACF25D587A88D8_H
#define QRCODEPROJECTLOADER_T18953784222E70AB5CBA0919B6ACF25D587A88D8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader
struct  QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// BarcodeScanner.IScanner Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader::BarcodeScanner
	RuntimeObject* ___BarcodeScanner_4;
	// System.Single Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader::RestartTime
	float ___RestartTime_5;
	// System.Func`3<System.String,System.String,System.Collections.IEnumerator> Assets._Project.Scripts.Common.Scripting.Loader.QRCodeProjectLoader::<ProjectUrlCallback>k__BackingField
	Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 * ___U3CProjectUrlCallbackU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_BarcodeScanner_4() { return static_cast<int32_t>(offsetof(QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8, ___BarcodeScanner_4)); }
	inline RuntimeObject* get_BarcodeScanner_4() const { return ___BarcodeScanner_4; }
	inline RuntimeObject** get_address_of_BarcodeScanner_4() { return &___BarcodeScanner_4; }
	inline void set_BarcodeScanner_4(RuntimeObject* value)
	{
		___BarcodeScanner_4 = value;
		Il2CppCodeGenWriteBarrier((&___BarcodeScanner_4), value);
	}

	inline static int32_t get_offset_of_RestartTime_5() { return static_cast<int32_t>(offsetof(QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8, ___RestartTime_5)); }
	inline float get_RestartTime_5() const { return ___RestartTime_5; }
	inline float* get_address_of_RestartTime_5() { return &___RestartTime_5; }
	inline void set_RestartTime_5(float value)
	{
		___RestartTime_5 = value;
	}

	inline static int32_t get_offset_of_U3CProjectUrlCallbackU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8, ___U3CProjectUrlCallbackU3Ek__BackingField_6)); }
	inline Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 * get_U3CProjectUrlCallbackU3Ek__BackingField_6() const { return ___U3CProjectUrlCallbackU3Ek__BackingField_6; }
	inline Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 ** get_address_of_U3CProjectUrlCallbackU3Ek__BackingField_6() { return &___U3CProjectUrlCallbackU3Ek__BackingField_6; }
	inline void set_U3CProjectUrlCallbackU3Ek__BackingField_6(Func_3_t5D19D2F5D175E4953F2471BB6690A6BF7579C653 * value)
	{
		___U3CProjectUrlCallbackU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProjectUrlCallbackU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QRCODEPROJECTLOADER_T18953784222E70AB5CBA0919B6ACF25D587A88D8_H
#ifndef FEATURES_T453CBFF75AFDC525F02DB5DD8B6BBB55273EED10_H
#define FEATURES_T453CBFF75AFDC525F02DB5DD8B6BBB55273EED10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features
struct  Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Object Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features::androidFeatures
	RuntimeObject * ___androidFeatures_4;
	// System.Object Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features::iOSFeatures
	RuntimeObject * ___iOSFeatures_5;
	// System.Object Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features::holoLensFeatures
	RuntimeObject * ___holoLensFeatures_6;
	// System.Object Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features::mixedRealityFeatures
	RuntimeObject * ___mixedRealityFeatures_7;
	// System.Object Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features::standaloneFeatures
	RuntimeObject * ___standaloneFeatures_8;
	// System.Object Assets._Project.Scripts.Common.Scripting.PlatformFeatures.Features::defaultFeatures
	RuntimeObject * ___defaultFeatures_9;

public:
	inline static int32_t get_offset_of_androidFeatures_4() { return static_cast<int32_t>(offsetof(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10, ___androidFeatures_4)); }
	inline RuntimeObject * get_androidFeatures_4() const { return ___androidFeatures_4; }
	inline RuntimeObject ** get_address_of_androidFeatures_4() { return &___androidFeatures_4; }
	inline void set_androidFeatures_4(RuntimeObject * value)
	{
		___androidFeatures_4 = value;
		Il2CppCodeGenWriteBarrier((&___androidFeatures_4), value);
	}

	inline static int32_t get_offset_of_iOSFeatures_5() { return static_cast<int32_t>(offsetof(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10, ___iOSFeatures_5)); }
	inline RuntimeObject * get_iOSFeatures_5() const { return ___iOSFeatures_5; }
	inline RuntimeObject ** get_address_of_iOSFeatures_5() { return &___iOSFeatures_5; }
	inline void set_iOSFeatures_5(RuntimeObject * value)
	{
		___iOSFeatures_5 = value;
		Il2CppCodeGenWriteBarrier((&___iOSFeatures_5), value);
	}

	inline static int32_t get_offset_of_holoLensFeatures_6() { return static_cast<int32_t>(offsetof(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10, ___holoLensFeatures_6)); }
	inline RuntimeObject * get_holoLensFeatures_6() const { return ___holoLensFeatures_6; }
	inline RuntimeObject ** get_address_of_holoLensFeatures_6() { return &___holoLensFeatures_6; }
	inline void set_holoLensFeatures_6(RuntimeObject * value)
	{
		___holoLensFeatures_6 = value;
		Il2CppCodeGenWriteBarrier((&___holoLensFeatures_6), value);
	}

	inline static int32_t get_offset_of_mixedRealityFeatures_7() { return static_cast<int32_t>(offsetof(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10, ___mixedRealityFeatures_7)); }
	inline RuntimeObject * get_mixedRealityFeatures_7() const { return ___mixedRealityFeatures_7; }
	inline RuntimeObject ** get_address_of_mixedRealityFeatures_7() { return &___mixedRealityFeatures_7; }
	inline void set_mixedRealityFeatures_7(RuntimeObject * value)
	{
		___mixedRealityFeatures_7 = value;
		Il2CppCodeGenWriteBarrier((&___mixedRealityFeatures_7), value);
	}

	inline static int32_t get_offset_of_standaloneFeatures_8() { return static_cast<int32_t>(offsetof(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10, ___standaloneFeatures_8)); }
	inline RuntimeObject * get_standaloneFeatures_8() const { return ___standaloneFeatures_8; }
	inline RuntimeObject ** get_address_of_standaloneFeatures_8() { return &___standaloneFeatures_8; }
	inline void set_standaloneFeatures_8(RuntimeObject * value)
	{
		___standaloneFeatures_8 = value;
		Il2CppCodeGenWriteBarrier((&___standaloneFeatures_8), value);
	}

	inline static int32_t get_offset_of_defaultFeatures_9() { return static_cast<int32_t>(offsetof(Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10, ___defaultFeatures_9)); }
	inline RuntimeObject * get_defaultFeatures_9() const { return ___defaultFeatures_9; }
	inline RuntimeObject ** get_address_of_defaultFeatures_9() { return &___defaultFeatures_9; }
	inline void set_defaultFeatures_9(RuntimeObject * value)
	{
		___defaultFeatures_9 = value;
		Il2CppCodeGenWriteBarrier((&___defaultFeatures_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FEATURES_T453CBFF75AFDC525F02DB5DD8B6BBB55273EED10_H
#ifndef INGAMECONSOLE_T030E829913D6809AD92C108AA1F6AAEC7DED1C28_H
#define INGAMECONSOLE_T030E829913D6809AD92C108AA1F6AAEC7DED1C28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.InGameConsole
struct  InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.KeyCode Assets._Project.Scripts.Common.UI.InGameConsole::toggleKey
	int32_t ___toggleKey_4;
	// System.Boolean Assets._Project.Scripts.Common.UI.InGameConsole::shakeToOpen
	bool ___shakeToOpen_5;
	// System.Single Assets._Project.Scripts.Common.UI.InGameConsole::shakeAcceleration
	float ___shakeAcceleration_6;
	// System.Boolean Assets._Project.Scripts.Common.UI.InGameConsole::restrictLogCount
	bool ___restrictLogCount_7;
	// System.Int32 Assets._Project.Scripts.Common.UI.InGameConsole::maxLogs
	int32_t ___maxLogs_8;
	// System.Collections.Generic.List`1<Assets._Project.Scripts.Common.UI.InGameConsole/Log> Assets._Project.Scripts.Common.UI.InGameConsole::logs
	List_1_t13A4D277A972555D2D69C11B8C7C37B220CF6A15 * ___logs_9;
	// UnityEngine.Vector2 Assets._Project.Scripts.Common.UI.InGameConsole::scrollPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___scrollPosition_10;
	// System.Boolean Assets._Project.Scripts.Common.UI.InGameConsole::visible
	bool ___visible_11;
	// System.Boolean Assets._Project.Scripts.Common.UI.InGameConsole::collapse
	bool ___collapse_12;
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,System.Boolean> Assets._Project.Scripts.Common.UI.InGameConsole::logTypeFilters
	Dictionary_2_tCFF0B4C05676E7570538CD56789338F5BC7FAF41 * ___logTypeFilters_13;
	// UnityEngine.Rect Assets._Project.Scripts.Common.UI.InGameConsole::titleBarRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___titleBarRect_19;
	// UnityEngine.Rect Assets._Project.Scripts.Common.UI.InGameConsole::windowRect
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___windowRect_20;

public:
	inline static int32_t get_offset_of_toggleKey_4() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___toggleKey_4)); }
	inline int32_t get_toggleKey_4() const { return ___toggleKey_4; }
	inline int32_t* get_address_of_toggleKey_4() { return &___toggleKey_4; }
	inline void set_toggleKey_4(int32_t value)
	{
		___toggleKey_4 = value;
	}

	inline static int32_t get_offset_of_shakeToOpen_5() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___shakeToOpen_5)); }
	inline bool get_shakeToOpen_5() const { return ___shakeToOpen_5; }
	inline bool* get_address_of_shakeToOpen_5() { return &___shakeToOpen_5; }
	inline void set_shakeToOpen_5(bool value)
	{
		___shakeToOpen_5 = value;
	}

	inline static int32_t get_offset_of_shakeAcceleration_6() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___shakeAcceleration_6)); }
	inline float get_shakeAcceleration_6() const { return ___shakeAcceleration_6; }
	inline float* get_address_of_shakeAcceleration_6() { return &___shakeAcceleration_6; }
	inline void set_shakeAcceleration_6(float value)
	{
		___shakeAcceleration_6 = value;
	}

	inline static int32_t get_offset_of_restrictLogCount_7() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___restrictLogCount_7)); }
	inline bool get_restrictLogCount_7() const { return ___restrictLogCount_7; }
	inline bool* get_address_of_restrictLogCount_7() { return &___restrictLogCount_7; }
	inline void set_restrictLogCount_7(bool value)
	{
		___restrictLogCount_7 = value;
	}

	inline static int32_t get_offset_of_maxLogs_8() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___maxLogs_8)); }
	inline int32_t get_maxLogs_8() const { return ___maxLogs_8; }
	inline int32_t* get_address_of_maxLogs_8() { return &___maxLogs_8; }
	inline void set_maxLogs_8(int32_t value)
	{
		___maxLogs_8 = value;
	}

	inline static int32_t get_offset_of_logs_9() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___logs_9)); }
	inline List_1_t13A4D277A972555D2D69C11B8C7C37B220CF6A15 * get_logs_9() const { return ___logs_9; }
	inline List_1_t13A4D277A972555D2D69C11B8C7C37B220CF6A15 ** get_address_of_logs_9() { return &___logs_9; }
	inline void set_logs_9(List_1_t13A4D277A972555D2D69C11B8C7C37B220CF6A15 * value)
	{
		___logs_9 = value;
		Il2CppCodeGenWriteBarrier((&___logs_9), value);
	}

	inline static int32_t get_offset_of_scrollPosition_10() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___scrollPosition_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_scrollPosition_10() const { return ___scrollPosition_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_scrollPosition_10() { return &___scrollPosition_10; }
	inline void set_scrollPosition_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___scrollPosition_10 = value;
	}

	inline static int32_t get_offset_of_visible_11() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___visible_11)); }
	inline bool get_visible_11() const { return ___visible_11; }
	inline bool* get_address_of_visible_11() { return &___visible_11; }
	inline void set_visible_11(bool value)
	{
		___visible_11 = value;
	}

	inline static int32_t get_offset_of_collapse_12() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___collapse_12)); }
	inline bool get_collapse_12() const { return ___collapse_12; }
	inline bool* get_address_of_collapse_12() { return &___collapse_12; }
	inline void set_collapse_12(bool value)
	{
		___collapse_12 = value;
	}

	inline static int32_t get_offset_of_logTypeFilters_13() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___logTypeFilters_13)); }
	inline Dictionary_2_tCFF0B4C05676E7570538CD56789338F5BC7FAF41 * get_logTypeFilters_13() const { return ___logTypeFilters_13; }
	inline Dictionary_2_tCFF0B4C05676E7570538CD56789338F5BC7FAF41 ** get_address_of_logTypeFilters_13() { return &___logTypeFilters_13; }
	inline void set_logTypeFilters_13(Dictionary_2_tCFF0B4C05676E7570538CD56789338F5BC7FAF41 * value)
	{
		___logTypeFilters_13 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeFilters_13), value);
	}

	inline static int32_t get_offset_of_titleBarRect_19() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___titleBarRect_19)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_titleBarRect_19() const { return ___titleBarRect_19; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_titleBarRect_19() { return &___titleBarRect_19; }
	inline void set_titleBarRect_19(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___titleBarRect_19 = value;
	}

	inline static int32_t get_offset_of_windowRect_20() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28, ___windowRect_20)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_windowRect_20() const { return ___windowRect_20; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_windowRect_20() { return &___windowRect_20; }
	inline void set_windowRect_20(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___windowRect_20 = value;
	}
};

struct InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<UnityEngine.LogType,UnityEngine.Color> Assets._Project.Scripts.Common.UI.InGameConsole::logTypeColors
	Dictionary_2_t1108955CD85FB42D96647EEDD618002FB5F4EF0F * ___logTypeColors_14;
	// UnityEngine.GUIContent Assets._Project.Scripts.Common.UI.InGameConsole::clearLabel
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___clearLabel_17;
	// UnityEngine.GUIContent Assets._Project.Scripts.Common.UI.InGameConsole::collapseLabel
	GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * ___collapseLabel_18;

public:
	inline static int32_t get_offset_of_logTypeColors_14() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields, ___logTypeColors_14)); }
	inline Dictionary_2_t1108955CD85FB42D96647EEDD618002FB5F4EF0F * get_logTypeColors_14() const { return ___logTypeColors_14; }
	inline Dictionary_2_t1108955CD85FB42D96647EEDD618002FB5F4EF0F ** get_address_of_logTypeColors_14() { return &___logTypeColors_14; }
	inline void set_logTypeColors_14(Dictionary_2_t1108955CD85FB42D96647EEDD618002FB5F4EF0F * value)
	{
		___logTypeColors_14 = value;
		Il2CppCodeGenWriteBarrier((&___logTypeColors_14), value);
	}

	inline static int32_t get_offset_of_clearLabel_17() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields, ___clearLabel_17)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_clearLabel_17() const { return ___clearLabel_17; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_clearLabel_17() { return &___clearLabel_17; }
	inline void set_clearLabel_17(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___clearLabel_17 = value;
		Il2CppCodeGenWriteBarrier((&___clearLabel_17), value);
	}

	inline static int32_t get_offset_of_collapseLabel_18() { return static_cast<int32_t>(offsetof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields, ___collapseLabel_18)); }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * get_collapseLabel_18() const { return ___collapseLabel_18; }
	inline GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 ** get_address_of_collapseLabel_18() { return &___collapseLabel_18; }
	inline void set_collapseLabel_18(GUIContent_t2A00F8961C69C0A382168840CFB2111FB00B5EA0 * value)
	{
		___collapseLabel_18 = value;
		Il2CppCodeGenWriteBarrier((&___collapseLabel_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INGAMECONSOLE_T030E829913D6809AD92C108AA1F6AAEC7DED1C28_H
#ifndef SCRIPTTILE_T03518EB9580168E192A9D9615ED20099809D1E7E_H
#define SCRIPTTILE_T03518EB9580168E192A9D9615ED20099809D1E7E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.ScriptTile
struct  ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.RawImage Assets._Project.Scripts.Common.UI.ScriptTile::thumbnail
	RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * ___thumbnail_4;
	// UnityEngine.UI.Text Assets._Project.Scripts.Common.UI.ScriptTile::title
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___title_5;
	// UnityEngine.UI.Text Assets._Project.Scripts.Common.UI.ScriptTile::author
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___author_6;
	// UnityEngine.UI.Text Assets._Project.Scripts.Common.UI.ScriptTile::description
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___description_7;
	// MPAR.Common.Scripting.Script Assets._Project.Scripts.Common.UI.ScriptTile::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_8;
	// System.Boolean Assets._Project.Scripts.Common.UI.ScriptTile::isLocalScript
	bool ___isLocalScript_9;

public:
	inline static int32_t get_offset_of_thumbnail_4() { return static_cast<int32_t>(offsetof(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E, ___thumbnail_4)); }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * get_thumbnail_4() const { return ___thumbnail_4; }
	inline RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 ** get_address_of_thumbnail_4() { return &___thumbnail_4; }
	inline void set_thumbnail_4(RawImage_t68991514DB8F48442D614E7904A298C936B3C7C8 * value)
	{
		___thumbnail_4 = value;
		Il2CppCodeGenWriteBarrier((&___thumbnail_4), value);
	}

	inline static int32_t get_offset_of_title_5() { return static_cast<int32_t>(offsetof(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E, ___title_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_title_5() const { return ___title_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_title_5() { return &___title_5; }
	inline void set_title_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___title_5 = value;
		Il2CppCodeGenWriteBarrier((&___title_5), value);
	}

	inline static int32_t get_offset_of_author_6() { return static_cast<int32_t>(offsetof(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E, ___author_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_author_6() const { return ___author_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_author_6() { return &___author_6; }
	inline void set_author_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___author_6 = value;
		Il2CppCodeGenWriteBarrier((&___author_6), value);
	}

	inline static int32_t get_offset_of_description_7() { return static_cast<int32_t>(offsetof(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E, ___description_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_description_7() const { return ___description_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_description_7() { return &___description_7; }
	inline void set_description_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___description_7 = value;
		Il2CppCodeGenWriteBarrier((&___description_7), value);
	}

	inline static int32_t get_offset_of_script_8() { return static_cast<int32_t>(offsetof(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E, ___script_8)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_8() const { return ___script_8; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_8() { return &___script_8; }
	inline void set_script_8(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_8 = value;
		Il2CppCodeGenWriteBarrier((&___script_8), value);
	}

	inline static int32_t get_offset_of_isLocalScript_9() { return static_cast<int32_t>(offsetof(ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E, ___isLocalScript_9)); }
	inline bool get_isLocalScript_9() const { return ___isLocalScript_9; }
	inline bool* get_address_of_isLocalScript_9() { return &___isLocalScript_9; }
	inline void set_isLocalScript_9(bool value)
	{
		___isLocalScript_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTTILE_T03518EB9580168E192A9D9615ED20099809D1E7E_H
#ifndef TABSWIPEMANAGER_T5E37889E3F7397C3D7B30B467EBC775EA74BFF82_H
#define TABSWIPEMANAGER_T5E37889E3F7397C3D7B30B467EBC775EA74BFF82_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.TabSwipeManager
struct  TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Button[] Assets._Project.Scripts.Common.UI.TabSwipeManager::tabs
	ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* ___tabs_4;
	// UnityEngine.GameObject[] Assets._Project.Scripts.Common.UI.TabSwipeManager::tabObjectsToCheck
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___tabObjectsToCheck_5;
	// System.Int32 Assets._Project.Scripts.Common.UI.TabSwipeManager::clickedButtonIndex
	int32_t ___clickedButtonIndex_6;
	// System.Action`1<Assets._Project.Scripts.Common.UI.TabSwipeManager/SwipeDirection> Assets._Project.Scripts.Common.UI.TabSwipeManager::Swipe
	Action_1_t636307F25CD6F89E930F8A340F1FB43974250E58 * ___Swipe_7;
	// System.Boolean Assets._Project.Scripts.Common.UI.TabSwipeManager::swiping
	bool ___swiping_8;
	// System.Boolean Assets._Project.Scripts.Common.UI.TabSwipeManager::eventSent
	bool ___eventSent_9;
	// UnityEngine.Vector2 Assets._Project.Scripts.Common.UI.TabSwipeManager::lastPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___lastPosition_10;
	// UnityEngine.Vector2 Assets._Project.Scripts.Common.UI.TabSwipeManager::touchDownPosition
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___touchDownPosition_11;

public:
	inline static int32_t get_offset_of_tabs_4() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___tabs_4)); }
	inline ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* get_tabs_4() const { return ___tabs_4; }
	inline ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B** get_address_of_tabs_4() { return &___tabs_4; }
	inline void set_tabs_4(ButtonU5BU5D_t363EC059ECDBD3DE56740518F34D8C070331649B* value)
	{
		___tabs_4 = value;
		Il2CppCodeGenWriteBarrier((&___tabs_4), value);
	}

	inline static int32_t get_offset_of_tabObjectsToCheck_5() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___tabObjectsToCheck_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_tabObjectsToCheck_5() const { return ___tabObjectsToCheck_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_tabObjectsToCheck_5() { return &___tabObjectsToCheck_5; }
	inline void set_tabObjectsToCheck_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___tabObjectsToCheck_5 = value;
		Il2CppCodeGenWriteBarrier((&___tabObjectsToCheck_5), value);
	}

	inline static int32_t get_offset_of_clickedButtonIndex_6() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___clickedButtonIndex_6)); }
	inline int32_t get_clickedButtonIndex_6() const { return ___clickedButtonIndex_6; }
	inline int32_t* get_address_of_clickedButtonIndex_6() { return &___clickedButtonIndex_6; }
	inline void set_clickedButtonIndex_6(int32_t value)
	{
		___clickedButtonIndex_6 = value;
	}

	inline static int32_t get_offset_of_Swipe_7() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___Swipe_7)); }
	inline Action_1_t636307F25CD6F89E930F8A340F1FB43974250E58 * get_Swipe_7() const { return ___Swipe_7; }
	inline Action_1_t636307F25CD6F89E930F8A340F1FB43974250E58 ** get_address_of_Swipe_7() { return &___Swipe_7; }
	inline void set_Swipe_7(Action_1_t636307F25CD6F89E930F8A340F1FB43974250E58 * value)
	{
		___Swipe_7 = value;
		Il2CppCodeGenWriteBarrier((&___Swipe_7), value);
	}

	inline static int32_t get_offset_of_swiping_8() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___swiping_8)); }
	inline bool get_swiping_8() const { return ___swiping_8; }
	inline bool* get_address_of_swiping_8() { return &___swiping_8; }
	inline void set_swiping_8(bool value)
	{
		___swiping_8 = value;
	}

	inline static int32_t get_offset_of_eventSent_9() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___eventSent_9)); }
	inline bool get_eventSent_9() const { return ___eventSent_9; }
	inline bool* get_address_of_eventSent_9() { return &___eventSent_9; }
	inline void set_eventSent_9(bool value)
	{
		___eventSent_9 = value;
	}

	inline static int32_t get_offset_of_lastPosition_10() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___lastPosition_10)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_lastPosition_10() const { return ___lastPosition_10; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_lastPosition_10() { return &___lastPosition_10; }
	inline void set_lastPosition_10(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___lastPosition_10 = value;
	}

	inline static int32_t get_offset_of_touchDownPosition_11() { return static_cast<int32_t>(offsetof(TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82, ___touchDownPosition_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_touchDownPosition_11() const { return ___touchDownPosition_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_touchDownPosition_11() { return &___touchDownPosition_11; }
	inline void set_touchDownPosition_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___touchDownPosition_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TABSWIPEMANAGER_T5E37889E3F7397C3D7B30B467EBC775EA74BFF82_H
#ifndef MOBILESCRIPTMENU_T20F9D9B5584E0A9624457D7BEAE94BEDE27173D4_H
#define MOBILESCRIPTMENU_T20F9D9B5584E0A9624457D7BEAE94BEDE27173D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Mobile.UI.MobileScriptMenu
struct  MobileScriptMenu_t20F9D9B5584E0A9624457D7BEAE94BEDE27173D4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILESCRIPTMENU_T20F9D9B5584E0A9624457D7BEAE94BEDE27173D4_H
#ifndef IOSINPUT_T85E39945A4B6B18A94599EB4BF776BE7FBA4AED0_H
#define IOSINPUT_T85E39945A4B6B18A94599EB4BF776BE7FBA4AED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.iOS.Input.iOSInput
struct  iOSInput_t85E39945A4B6B18A94599EB4BF776BE7FBA4AED0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Input.IPlatformDependentInputListener> Assets._Project.Scripts.iOS.Input.iOSInput::listeners
	List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * ___listeners_4;
	// System.Boolean Assets._Project.Scripts.iOS.Input.iOSInput::dragging
	bool ___dragging_5;

public:
	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(iOSInput_t85E39945A4B6B18A94599EB4BF776BE7FBA4AED0, ___listeners_4)); }
	inline List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * get_listeners_4() const { return ___listeners_4; }
	inline List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_4), value);
	}

	inline static int32_t get_offset_of_dragging_5() { return static_cast<int32_t>(offsetof(iOSInput_t85E39945A4B6B18A94599EB4BF776BE7FBA4AED0, ___dragging_5)); }
	inline bool get_dragging_5() const { return ___dragging_5; }
	inline bool* get_address_of_dragging_5() { return &___dragging_5; }
	inline void set_dragging_5(bool value)
	{
		___dragging_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSINPUT_T85E39945A4B6B18A94599EB4BF776BE7FBA4AED0_H
#ifndef IOSSCRIPTMENUITEM_T03701E3CF7DC42E3E0BA8361542ADC00AEB465E6_H
#define IOSSCRIPTMENUITEM_T03701E3CF7DC42E3E0BA8361542ADC00AEB465E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.iOS.UI.iOSScriptMenuItem
struct  iOSScriptMenuItem_t03701E3CF7DC42E3E0BA8361542ADC00AEB465E6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Scripting.Script Assets._Project.Scripts.iOS.UI.iOSScriptMenuItem::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_4;

public:
	inline static int32_t get_offset_of_script_4() { return static_cast<int32_t>(offsetof(iOSScriptMenuItem_t03701E3CF7DC42E3E0BA8361542ADC00AEB465E6, ___script_4)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_4() const { return ___script_4; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_4() { return &___script_4; }
	inline void set_script_4(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_4 = value;
		Il2CppCodeGenWriteBarrier((&___script_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSCRIPTMENUITEM_T03701E3CF7DC42E3E0BA8361542ADC00AEB465E6_H
#ifndef SINGLETON_1_TD42AA6D2C7456D43DD09A7910761A0A624E8D069_H
#define SINGLETON_1_TD42AA6D2C7456D43DD09A7910761A0A624E8D069_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler>
struct  Singleton_1_tD42AA6D2C7456D43DD09A7910761A0A624E8D069  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tD42AA6D2C7456D43DD09A7910761A0A624E8D069_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_tD42AA6D2C7456D43DD09A7910761A0A624E8D069_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TD42AA6D2C7456D43DD09A7910761A0A624E8D069_H
#ifndef CALIBRATIONSPACE_TCA0BD13D13077D99ECC4CB1148F303B1386278E2_H
#define CALIBRATIONSPACE_TCA0BD13D13077D99ECC4CB1148F303B1386278E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.CalibrationSpace
struct  CalibrationSpace_tCA0BD13D13077D99ECC4CB1148F303B1386278E2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALIBRATIONSPACE_TCA0BD13D13077D99ECC4CB1148F303B1386278E2_H
#ifndef SPHEREBASEDTAGALONG_T0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED_H
#define SPHEREBASEDTAGALONG_T0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HoloToolkit.Unity.SphereBasedTagalong
struct  SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::SphereRadius
	float ___SphereRadius_4;
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::MoveSpeed
	float ___MoveSpeed_5;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::useUnscaledTime
	bool ___useUnscaledTime_6;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::hideOnStart
	bool ___hideOnStart_7;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::debugDisplaySphere
	bool ___debugDisplaySphere_8;
	// System.Boolean HoloToolkit.Unity.SphereBasedTagalong::debugDisplayTargetPosition
	bool ___debugDisplayTargetPosition_9;
	// UnityEngine.Vector3 HoloToolkit.Unity.SphereBasedTagalong::targetPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___targetPosition_10;
	// UnityEngine.Vector3 HoloToolkit.Unity.SphereBasedTagalong::optimalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___optimalPosition_11;
	// System.Single HoloToolkit.Unity.SphereBasedTagalong::initialDistanceToCamera
	float ___initialDistanceToCamera_12;

public:
	inline static int32_t get_offset_of_SphereRadius_4() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___SphereRadius_4)); }
	inline float get_SphereRadius_4() const { return ___SphereRadius_4; }
	inline float* get_address_of_SphereRadius_4() { return &___SphereRadius_4; }
	inline void set_SphereRadius_4(float value)
	{
		___SphereRadius_4 = value;
	}

	inline static int32_t get_offset_of_MoveSpeed_5() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___MoveSpeed_5)); }
	inline float get_MoveSpeed_5() const { return ___MoveSpeed_5; }
	inline float* get_address_of_MoveSpeed_5() { return &___MoveSpeed_5; }
	inline void set_MoveSpeed_5(float value)
	{
		___MoveSpeed_5 = value;
	}

	inline static int32_t get_offset_of_useUnscaledTime_6() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___useUnscaledTime_6)); }
	inline bool get_useUnscaledTime_6() const { return ___useUnscaledTime_6; }
	inline bool* get_address_of_useUnscaledTime_6() { return &___useUnscaledTime_6; }
	inline void set_useUnscaledTime_6(bool value)
	{
		___useUnscaledTime_6 = value;
	}

	inline static int32_t get_offset_of_hideOnStart_7() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___hideOnStart_7)); }
	inline bool get_hideOnStart_7() const { return ___hideOnStart_7; }
	inline bool* get_address_of_hideOnStart_7() { return &___hideOnStart_7; }
	inline void set_hideOnStart_7(bool value)
	{
		___hideOnStart_7 = value;
	}

	inline static int32_t get_offset_of_debugDisplaySphere_8() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___debugDisplaySphere_8)); }
	inline bool get_debugDisplaySphere_8() const { return ___debugDisplaySphere_8; }
	inline bool* get_address_of_debugDisplaySphere_8() { return &___debugDisplaySphere_8; }
	inline void set_debugDisplaySphere_8(bool value)
	{
		___debugDisplaySphere_8 = value;
	}

	inline static int32_t get_offset_of_debugDisplayTargetPosition_9() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___debugDisplayTargetPosition_9)); }
	inline bool get_debugDisplayTargetPosition_9() const { return ___debugDisplayTargetPosition_9; }
	inline bool* get_address_of_debugDisplayTargetPosition_9() { return &___debugDisplayTargetPosition_9; }
	inline void set_debugDisplayTargetPosition_9(bool value)
	{
		___debugDisplayTargetPosition_9 = value;
	}

	inline static int32_t get_offset_of_targetPosition_10() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___targetPosition_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_targetPosition_10() const { return ___targetPosition_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_targetPosition_10() { return &___targetPosition_10; }
	inline void set_targetPosition_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___targetPosition_10 = value;
	}

	inline static int32_t get_offset_of_optimalPosition_11() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___optimalPosition_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_optimalPosition_11() const { return ___optimalPosition_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_optimalPosition_11() { return &___optimalPosition_11; }
	inline void set_optimalPosition_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___optimalPosition_11 = value;
	}

	inline static int32_t get_offset_of_initialDistanceToCamera_12() { return static_cast<int32_t>(offsetof(SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED, ___initialDistanceToCamera_12)); }
	inline float get_initialDistanceToCamera_12() const { return ___initialDistanceToCamera_12; }
	inline float* get_address_of_initialDistanceToCamera_12() { return &___initialDistanceToCamera_12; }
	inline void set_initialDistanceToCamera_12(float value)
	{
		___initialDistanceToCamera_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPHEREBASEDTAGALONG_T0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED_H
#ifndef ABSTRACTASSETLOADER_T8FE1E80CC3316AB57156FE25E056F61190B9F4C2_H
#define ABSTRACTASSETLOADER_T8FE1E80CC3316AB57156FE25E056F61190B9F4C2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.AssetLoader.AbstractAssetLoader
struct  AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Common.AssetLoader.AbstractAssetLoader::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_5;
	// MPAR.Common.Persistence.CacheManager MPAR.Common.AssetLoader.AbstractAssetLoader::<CacheManager>k__BackingField
	CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * ___U3CCacheManagerU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2, ___U3CSaveLoadManagerU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_5() const { return ___U3CSaveLoadManagerU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_5() { return &___U3CSaveLoadManagerU3Ek__BackingField_5; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CCacheManagerU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2, ___U3CCacheManagerU3Ek__BackingField_6)); }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * get_U3CCacheManagerU3Ek__BackingField_6() const { return ___U3CCacheManagerU3Ek__BackingField_6; }
	inline CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B ** get_address_of_U3CCacheManagerU3Ek__BackingField_6() { return &___U3CCacheManagerU3Ek__BackingField_6; }
	inline void set_U3CCacheManagerU3Ek__BackingField_6(CacheManager_t93F8C94EAD497E054A8ED4B29E5470DC2017007B * value)
	{
		___U3CCacheManagerU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCacheManagerU3Ek__BackingField_6), value);
	}
};

struct AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> MPAR.Common.AssetLoader.AbstractAssetLoader::<SupportedAssets>k__BackingField
	Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * ___U3CSupportedAssetsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CSupportedAssetsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2_StaticFields, ___U3CSupportedAssetsU3Ek__BackingField_4)); }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * get_U3CSupportedAssetsU3Ek__BackingField_4() const { return ___U3CSupportedAssetsU3Ek__BackingField_4; }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A ** get_address_of_U3CSupportedAssetsU3Ek__BackingField_4() { return &___U3CSupportedAssetsU3Ek__BackingField_4; }
	inline void set_U3CSupportedAssetsU3Ek__BackingField_4(Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * value)
	{
		___U3CSupportedAssetsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSupportedAssetsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ABSTRACTASSETLOADER_T8FE1E80CC3316AB57156FE25E056F61190B9F4C2_H
#ifndef SINGLETON_1_T1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB_H
#define SINGLETON_1_T1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Singleton`1<Assets.ContentPrefabs>
struct  Singleton_1_t1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB_StaticFields
{
public:
	// T MPAR.Common.GameObjects.Singleton`1::instance
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0 * ___instance_4;
	// System.Boolean MPAR.Common.GameObjects.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB_StaticFields, ___instance_4)); }
	inline ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0 * get_instance_4() const { return ___instance_4; }
	inline ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB_H
#ifndef SINGLETON_1_T17131FB73A6E9FBC553D4B01F768CFAD10AE66BF_H
#define SINGLETON_1_T17131FB73A6E9FBC553D4B01F768CFAD10AE66BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Singleton`1<Assets._Project.Scripts.Common.SceneControllers.GameSceneInit>
struct  Singleton_1_t17131FB73A6E9FBC553D4B01F768CFAD10AE66BF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t17131FB73A6E9FBC553D4B01F768CFAD10AE66BF_StaticFields
{
public:
	// T MPAR.Common.GameObjects.Singleton`1::instance
	GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E * ___instance_4;
	// System.Boolean MPAR.Common.GameObjects.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t17131FB73A6E9FBC553D4B01F768CFAD10AE66BF_StaticFields, ___instance_4)); }
	inline GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E * get_instance_4() const { return ___instance_4; }
	inline GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t17131FB73A6E9FBC553D4B01F768CFAD10AE66BF_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T17131FB73A6E9FBC553D4B01F768CFAD10AE66BF_H
#ifndef SINGLETON_1_T4119C92670D5A7655597C8B5A609FDC46FB66416_H
#define SINGLETON_1_T4119C92670D5A7655597C8B5A609FDC46FB66416_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Common.GameObjects.Singleton`1<Assets._Project.Scripts.Common.UI.ScriptCanvas>
struct  Singleton_1_t4119C92670D5A7655597C8B5A609FDC46FB66416  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t4119C92670D5A7655597C8B5A609FDC46FB66416_StaticFields
{
public:
	// T MPAR.Common.GameObjects.Singleton`1::instance
	ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634 * ___instance_4;
	// System.Boolean MPAR.Common.GameObjects.Singleton`1::searchForInstance
	bool ___searchForInstance_5;

public:
	inline static int32_t get_offset_of_instance_4() { return static_cast<int32_t>(offsetof(Singleton_1_t4119C92670D5A7655597C8B5A609FDC46FB66416_StaticFields, ___instance_4)); }
	inline ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634 * get_instance_4() const { return ___instance_4; }
	inline ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634 ** get_address_of_instance_4() { return &___instance_4; }
	inline void set_instance_4(ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634 * value)
	{
		___instance_4 = value;
		Il2CppCodeGenWriteBarrier((&___instance_4), value);
	}

	inline static int32_t get_offset_of_searchForInstance_5() { return static_cast<int32_t>(offsetof(Singleton_1_t4119C92670D5A7655597C8B5A609FDC46FB66416_StaticFields, ___searchForInstance_5)); }
	inline bool get_searchForInstance_5() const { return ___searchForInstance_5; }
	inline bool* get_address_of_searchForInstance_5() { return &___searchForInstance_5; }
	inline void set_searchForInstance_5(bool value)
	{
		___searchForInstance_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T4119C92670D5A7655597C8B5A609FDC46FB66416_H
#ifndef DESKTOPINPUT_T49BB33778B1D74FF7013720E2408808FE3D7BF48_H
#define DESKTOPINPUT_T49BB33778B1D74FF7013720E2408808FE3D7BF48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Input.DesktopInput
struct  DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Input.IPlatformDependentInputListener> MPAR.Desktop.Input.DesktopInput::listeners
	List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * ___listeners_4;
	// UnityEngine.Vector3 MPAR.Desktop.Input.DesktopInput::<MouseDownPosition>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CMouseDownPositionU3Ek__BackingField_5;
	// System.Boolean MPAR.Desktop.Input.DesktopInput::mouseDown
	bool ___mouseDown_6;
	// System.Boolean MPAR.Desktop.Input.DesktopInput::dragging
	bool ___dragging_7;
	// UnityEngine.Vector3 MPAR.Desktop.Input.DesktopInput::lastDelta
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastDelta_8;
	// UnityEngine.Vector3 MPAR.Desktop.Input.DesktopInput::lastMovementDelta
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastMovementDelta_9;
	// UnityEngine.Transform MPAR.Desktop.Input.DesktopInput::_cursor
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ____cursor_10;
	// MPAR.Desktop.Input.DesktopInput/DesktopInputMode MPAR.Desktop.Input.DesktopInput::inputMode
	int32_t ___inputMode_11;

public:
	inline static int32_t get_offset_of_listeners_4() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___listeners_4)); }
	inline List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * get_listeners_4() const { return ___listeners_4; }
	inline List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C ** get_address_of_listeners_4() { return &___listeners_4; }
	inline void set_listeners_4(List_1_t433AB2199DFD3ECCED8B8A60A30292F83A42453C * value)
	{
		___listeners_4 = value;
		Il2CppCodeGenWriteBarrier((&___listeners_4), value);
	}

	inline static int32_t get_offset_of_U3CMouseDownPositionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___U3CMouseDownPositionU3Ek__BackingField_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CMouseDownPositionU3Ek__BackingField_5() const { return ___U3CMouseDownPositionU3Ek__BackingField_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CMouseDownPositionU3Ek__BackingField_5() { return &___U3CMouseDownPositionU3Ek__BackingField_5; }
	inline void set_U3CMouseDownPositionU3Ek__BackingField_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CMouseDownPositionU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_mouseDown_6() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___mouseDown_6)); }
	inline bool get_mouseDown_6() const { return ___mouseDown_6; }
	inline bool* get_address_of_mouseDown_6() { return &___mouseDown_6; }
	inline void set_mouseDown_6(bool value)
	{
		___mouseDown_6 = value;
	}

	inline static int32_t get_offset_of_dragging_7() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___dragging_7)); }
	inline bool get_dragging_7() const { return ___dragging_7; }
	inline bool* get_address_of_dragging_7() { return &___dragging_7; }
	inline void set_dragging_7(bool value)
	{
		___dragging_7 = value;
	}

	inline static int32_t get_offset_of_lastDelta_8() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___lastDelta_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastDelta_8() const { return ___lastDelta_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastDelta_8() { return &___lastDelta_8; }
	inline void set_lastDelta_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastDelta_8 = value;
	}

	inline static int32_t get_offset_of_lastMovementDelta_9() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___lastMovementDelta_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastMovementDelta_9() const { return ___lastMovementDelta_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastMovementDelta_9() { return &___lastMovementDelta_9; }
	inline void set_lastMovementDelta_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastMovementDelta_9 = value;
	}

	inline static int32_t get_offset_of__cursor_10() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ____cursor_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get__cursor_10() const { return ____cursor_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of__cursor_10() { return &____cursor_10; }
	inline void set__cursor_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		____cursor_10 = value;
		Il2CppCodeGenWriteBarrier((&____cursor_10), value);
	}

	inline static int32_t get_offset_of_inputMode_11() { return static_cast<int32_t>(offsetof(DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48, ___inputMode_11)); }
	inline int32_t get_inputMode_11() const { return ___inputMode_11; }
	inline int32_t* get_address_of_inputMode_11() { return &___inputMode_11; }
	inline void set_inputMode_11(int32_t value)
	{
		___inputMode_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPINPUT_T49BB33778B1D74FF7013720E2408808FE3D7BF48_H
#ifndef FIRSTPERSONCONTROLS_T9AF2FCDFA9310818E8CBAB77D20BACFF39128A96_H
#define FIRSTPERSONCONTROLS_T9AF2FCDFA9310818E8CBAB77D20BACFF39128A96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Input.FirstPersonControls
struct  FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single MPAR.Desktop.Input.FirstPersonControls::forwardSpeed
	float ___forwardSpeed_4;
	// System.Single MPAR.Desktop.Input.FirstPersonControls::sideSpeed
	float ___sideSpeed_5;
	// System.Boolean MPAR.Desktop.Input.FirstPersonControls::walking
	bool ___walking_6;

public:
	inline static int32_t get_offset_of_forwardSpeed_4() { return static_cast<int32_t>(offsetof(FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96, ___forwardSpeed_4)); }
	inline float get_forwardSpeed_4() const { return ___forwardSpeed_4; }
	inline float* get_address_of_forwardSpeed_4() { return &___forwardSpeed_4; }
	inline void set_forwardSpeed_4(float value)
	{
		___forwardSpeed_4 = value;
	}

	inline static int32_t get_offset_of_sideSpeed_5() { return static_cast<int32_t>(offsetof(FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96, ___sideSpeed_5)); }
	inline float get_sideSpeed_5() const { return ___sideSpeed_5; }
	inline float* get_address_of_sideSpeed_5() { return &___sideSpeed_5; }
	inline void set_sideSpeed_5(float value)
	{
		___sideSpeed_5 = value;
	}

	inline static int32_t get_offset_of_walking_6() { return static_cast<int32_t>(offsetof(FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96, ___walking_6)); }
	inline bool get_walking_6() const { return ___walking_6; }
	inline bool* get_address_of_walking_6() { return &___walking_6; }
	inline void set_walking_6(bool value)
	{
		___walking_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIRSTPERSONCONTROLS_T9AF2FCDFA9310818E8CBAB77D20BACFF39128A96_H
#ifndef SMOOTHMOUSELOOK_T02D0AFDBEE9391D8EFD43CC04D38419CF6521A51_H
#define SMOOTHMOUSELOOK_T02D0AFDBEE9391D8EFD43CC04D38419CF6521A51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Input.SmoothMouseLook
struct  SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Desktop.Input.SmoothMouseLook/RotationAxes MPAR.Desktop.Input.SmoothMouseLook::axes
	int32_t ___axes_4;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::sensitivityX
	float ___sensitivityX_5;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::sensitivityY
	float ___sensitivityY_6;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::minimumX
	float ___minimumX_7;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::maximumX
	float ___maximumX_8;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::minimumY
	float ___minimumY_9;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::maximumY
	float ___maximumY_10;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::rotationX
	float ___rotationX_11;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::rotationY
	float ___rotationY_12;
	// System.Collections.Generic.List`1<System.Single> MPAR.Desktop.Input.SmoothMouseLook::rotArrayX
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___rotArrayX_13;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::rotAverageX
	float ___rotAverageX_14;
	// System.Collections.Generic.List`1<System.Single> MPAR.Desktop.Input.SmoothMouseLook::rotArrayY
	List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * ___rotArrayY_15;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::rotAverageY
	float ___rotAverageY_16;
	// System.Single MPAR.Desktop.Input.SmoothMouseLook::frameCounter
	float ___frameCounter_17;
	// UnityEngine.Quaternion MPAR.Desktop.Input.SmoothMouseLook::originalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___originalRotation_18;

public:
	inline static int32_t get_offset_of_axes_4() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___axes_4)); }
	inline int32_t get_axes_4() const { return ___axes_4; }
	inline int32_t* get_address_of_axes_4() { return &___axes_4; }
	inline void set_axes_4(int32_t value)
	{
		___axes_4 = value;
	}

	inline static int32_t get_offset_of_sensitivityX_5() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___sensitivityX_5)); }
	inline float get_sensitivityX_5() const { return ___sensitivityX_5; }
	inline float* get_address_of_sensitivityX_5() { return &___sensitivityX_5; }
	inline void set_sensitivityX_5(float value)
	{
		___sensitivityX_5 = value;
	}

	inline static int32_t get_offset_of_sensitivityY_6() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___sensitivityY_6)); }
	inline float get_sensitivityY_6() const { return ___sensitivityY_6; }
	inline float* get_address_of_sensitivityY_6() { return &___sensitivityY_6; }
	inline void set_sensitivityY_6(float value)
	{
		___sensitivityY_6 = value;
	}

	inline static int32_t get_offset_of_minimumX_7() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___minimumX_7)); }
	inline float get_minimumX_7() const { return ___minimumX_7; }
	inline float* get_address_of_minimumX_7() { return &___minimumX_7; }
	inline void set_minimumX_7(float value)
	{
		___minimumX_7 = value;
	}

	inline static int32_t get_offset_of_maximumX_8() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___maximumX_8)); }
	inline float get_maximumX_8() const { return ___maximumX_8; }
	inline float* get_address_of_maximumX_8() { return &___maximumX_8; }
	inline void set_maximumX_8(float value)
	{
		___maximumX_8 = value;
	}

	inline static int32_t get_offset_of_minimumY_9() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___minimumY_9)); }
	inline float get_minimumY_9() const { return ___minimumY_9; }
	inline float* get_address_of_minimumY_9() { return &___minimumY_9; }
	inline void set_minimumY_9(float value)
	{
		___minimumY_9 = value;
	}

	inline static int32_t get_offset_of_maximumY_10() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___maximumY_10)); }
	inline float get_maximumY_10() const { return ___maximumY_10; }
	inline float* get_address_of_maximumY_10() { return &___maximumY_10; }
	inline void set_maximumY_10(float value)
	{
		___maximumY_10 = value;
	}

	inline static int32_t get_offset_of_rotationX_11() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___rotationX_11)); }
	inline float get_rotationX_11() const { return ___rotationX_11; }
	inline float* get_address_of_rotationX_11() { return &___rotationX_11; }
	inline void set_rotationX_11(float value)
	{
		___rotationX_11 = value;
	}

	inline static int32_t get_offset_of_rotationY_12() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___rotationY_12)); }
	inline float get_rotationY_12() const { return ___rotationY_12; }
	inline float* get_address_of_rotationY_12() { return &___rotationY_12; }
	inline void set_rotationY_12(float value)
	{
		___rotationY_12 = value;
	}

	inline static int32_t get_offset_of_rotArrayX_13() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___rotArrayX_13)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_rotArrayX_13() const { return ___rotArrayX_13; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_rotArrayX_13() { return &___rotArrayX_13; }
	inline void set_rotArrayX_13(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___rotArrayX_13 = value;
		Il2CppCodeGenWriteBarrier((&___rotArrayX_13), value);
	}

	inline static int32_t get_offset_of_rotAverageX_14() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___rotAverageX_14)); }
	inline float get_rotAverageX_14() const { return ___rotAverageX_14; }
	inline float* get_address_of_rotAverageX_14() { return &___rotAverageX_14; }
	inline void set_rotAverageX_14(float value)
	{
		___rotAverageX_14 = value;
	}

	inline static int32_t get_offset_of_rotArrayY_15() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___rotArrayY_15)); }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * get_rotArrayY_15() const { return ___rotArrayY_15; }
	inline List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E ** get_address_of_rotArrayY_15() { return &___rotArrayY_15; }
	inline void set_rotArrayY_15(List_1_tC02C2993D5A6DDB73B1126E4EECDEB641C54A03E * value)
	{
		___rotArrayY_15 = value;
		Il2CppCodeGenWriteBarrier((&___rotArrayY_15), value);
	}

	inline static int32_t get_offset_of_rotAverageY_16() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___rotAverageY_16)); }
	inline float get_rotAverageY_16() const { return ___rotAverageY_16; }
	inline float* get_address_of_rotAverageY_16() { return &___rotAverageY_16; }
	inline void set_rotAverageY_16(float value)
	{
		___rotAverageY_16 = value;
	}

	inline static int32_t get_offset_of_frameCounter_17() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___frameCounter_17)); }
	inline float get_frameCounter_17() const { return ___frameCounter_17; }
	inline float* get_address_of_frameCounter_17() { return &___frameCounter_17; }
	inline void set_frameCounter_17(float value)
	{
		___frameCounter_17 = value;
	}

	inline static int32_t get_offset_of_originalRotation_18() { return static_cast<int32_t>(offsetof(SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51, ___originalRotation_18)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_originalRotation_18() const { return ___originalRotation_18; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_originalRotation_18() { return &___originalRotation_18; }
	inline void set_originalRotation_18(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___originalRotation_18 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHMOUSELOOK_T02D0AFDBEE9391D8EFD43CC04D38419CF6521A51_H
#ifndef BASESAVELOADMANAGER_TB73A6EC5834C76C54E79BF0F10241ADB883530EB_H
#define BASESAVELOADMANAGER_TB73A6EC5834C76C54E79BF0F10241ADB883530EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.Persistence.BaseSaveLoadManager
struct  BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::BasePath
	String_t* ___BasePath_4;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::ScriptsSuffix
	String_t* ___ScriptsSuffix_5;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::CacheSuffix
	String_t* ___CacheSuffix_6;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::SaveDataSiffux
	String_t* ___SaveDataSiffux_7;
	// System.String MPAR.Desktop.Persistence.BaseSaveLoadManager::<ScriptId>k__BackingField
	String_t* ___U3CScriptIdU3Ek__BackingField_8;
	// System.Collections.Generic.Dictionary`2<System.String,System.String> MPAR.Desktop.Persistence.BaseSaveLoadManager::<AssetMapping>k__BackingField
	Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * ___U3CAssetMappingU3Ek__BackingField_9;
	// MPAR.Common.AssetLoader.IPlatformDependentAssetLoader MPAR.Desktop.Persistence.BaseSaveLoadManager::<AssetLoader>k__BackingField
	RuntimeObject* ___U3CAssetLoaderU3Ek__BackingField_10;
	// MPAR.Common.GameObjects.Factories.ExhibitBoxFactory MPAR.Desktop.Persistence.BaseSaveLoadManager::<ExhibitBoxFactory>k__BackingField
	ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * ___U3CExhibitBoxFactoryU3Ek__BackingField_11;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Desktop.Persistence.BaseSaveLoadManager::Gestures
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___Gestures_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Func`5<System.String,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.GameObject>> MPAR.Desktop.Persistence.BaseSaveLoadManager::<Primitives>k__BackingField
	Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 * ___U3CPrimitivesU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_BasePath_4() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___BasePath_4)); }
	inline String_t* get_BasePath_4() const { return ___BasePath_4; }
	inline String_t** get_address_of_BasePath_4() { return &___BasePath_4; }
	inline void set_BasePath_4(String_t* value)
	{
		___BasePath_4 = value;
		Il2CppCodeGenWriteBarrier((&___BasePath_4), value);
	}

	inline static int32_t get_offset_of_ScriptsSuffix_5() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___ScriptsSuffix_5)); }
	inline String_t* get_ScriptsSuffix_5() const { return ___ScriptsSuffix_5; }
	inline String_t** get_address_of_ScriptsSuffix_5() { return &___ScriptsSuffix_5; }
	inline void set_ScriptsSuffix_5(String_t* value)
	{
		___ScriptsSuffix_5 = value;
		Il2CppCodeGenWriteBarrier((&___ScriptsSuffix_5), value);
	}

	inline static int32_t get_offset_of_CacheSuffix_6() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___CacheSuffix_6)); }
	inline String_t* get_CacheSuffix_6() const { return ___CacheSuffix_6; }
	inline String_t** get_address_of_CacheSuffix_6() { return &___CacheSuffix_6; }
	inline void set_CacheSuffix_6(String_t* value)
	{
		___CacheSuffix_6 = value;
		Il2CppCodeGenWriteBarrier((&___CacheSuffix_6), value);
	}

	inline static int32_t get_offset_of_SaveDataSiffux_7() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___SaveDataSiffux_7)); }
	inline String_t* get_SaveDataSiffux_7() const { return ___SaveDataSiffux_7; }
	inline String_t** get_address_of_SaveDataSiffux_7() { return &___SaveDataSiffux_7; }
	inline void set_SaveDataSiffux_7(String_t* value)
	{
		___SaveDataSiffux_7 = value;
		Il2CppCodeGenWriteBarrier((&___SaveDataSiffux_7), value);
	}

	inline static int32_t get_offset_of_U3CScriptIdU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CScriptIdU3Ek__BackingField_8)); }
	inline String_t* get_U3CScriptIdU3Ek__BackingField_8() const { return ___U3CScriptIdU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CScriptIdU3Ek__BackingField_8() { return &___U3CScriptIdU3Ek__BackingField_8; }
	inline void set_U3CScriptIdU3Ek__BackingField_8(String_t* value)
	{
		___U3CScriptIdU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptIdU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CAssetMappingU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CAssetMappingU3Ek__BackingField_9)); }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * get_U3CAssetMappingU3Ek__BackingField_9() const { return ___U3CAssetMappingU3Ek__BackingField_9; }
	inline Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC ** get_address_of_U3CAssetMappingU3Ek__BackingField_9() { return &___U3CAssetMappingU3Ek__BackingField_9; }
	inline void set_U3CAssetMappingU3Ek__BackingField_9(Dictionary_2_t931BF283048C4E74FC063C3036E5F3FE328861FC * value)
	{
		___U3CAssetMappingU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetMappingU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CAssetLoaderU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CAssetLoaderU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CAssetLoaderU3Ek__BackingField_10() const { return ___U3CAssetLoaderU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CAssetLoaderU3Ek__BackingField_10() { return &___U3CAssetLoaderU3Ek__BackingField_10; }
	inline void set_U3CAssetLoaderU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CAssetLoaderU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAssetLoaderU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CExhibitBoxFactoryU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CExhibitBoxFactoryU3Ek__BackingField_11)); }
	inline ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * get_U3CExhibitBoxFactoryU3Ek__BackingField_11() const { return ___U3CExhibitBoxFactoryU3Ek__BackingField_11; }
	inline ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 ** get_address_of_U3CExhibitBoxFactoryU3Ek__BackingField_11() { return &___U3CExhibitBoxFactoryU3Ek__BackingField_11; }
	inline void set_U3CExhibitBoxFactoryU3Ek__BackingField_11(ExhibitBoxFactory_tF11A6EDB9BE288A54C29171DCC3059BCE31EAA13 * value)
	{
		___U3CExhibitBoxFactoryU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExhibitBoxFactoryU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_Gestures_12() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___Gestures_12)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_Gestures_12() const { return ___Gestures_12; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_Gestures_12() { return &___Gestures_12; }
	inline void set_Gestures_12(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___Gestures_12 = value;
		Il2CppCodeGenWriteBarrier((&___Gestures_12), value);
	}

	inline static int32_t get_offset_of_U3CPrimitivesU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB, ___U3CPrimitivesU3Ek__BackingField_13)); }
	inline Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 * get_U3CPrimitivesU3Ek__BackingField_13() const { return ___U3CPrimitivesU3Ek__BackingField_13; }
	inline Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 ** get_address_of_U3CPrimitivesU3Ek__BackingField_13() { return &___U3CPrimitivesU3Ek__BackingField_13; }
	inline void set_U3CPrimitivesU3Ek__BackingField_13(Dictionary_2_t83DAED981E1B06578CAA17F0CB3187DA58D37909 * value)
	{
		___U3CPrimitivesU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrimitivesU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASESAVELOADMANAGER_TB73A6EC5834C76C54E79BF0F10241ADB883530EB_H
#ifndef DESKTOPINSCRIPTMENU_TD2EFCDB4E715E7882700D8AA0DE43557CB018F8E_H
#define DESKTOPINSCRIPTMENU_TD2EFCDB4E715E7882700D8AA0DE43557CB018F8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopInScriptMenu
struct  DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.GameObjects.Factories.InScriptImageMenuItemFactory MPAR.Desktop.UI.DesktopInScriptMenu::<ImageMenuFactory>k__BackingField
	InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831 * ___U3CImageMenuFactoryU3Ek__BackingField_4;
	// MPAR.Common.GameObjects.Factories.InScriptTextMenuItemFactory MPAR.Desktop.UI.DesktopInScriptMenu::<TextMenuFactory>k__BackingField
	InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632 * ___U3CTextMenuFactoryU3Ek__BackingField_5;
	// MPAR.Common.Collections.ObjectCollection MPAR.Desktop.UI.DesktopInScriptMenu::<ObjectCollection>k__BackingField
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * ___U3CObjectCollectionU3Ek__BackingField_6;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MPAR.Desktop.UI.DesktopInScriptMenu::<MenuItems>k__BackingField
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___U3CMenuItemsU3Ek__BackingField_7;
	// System.Collections.Generic.List`1<UnityEngine.GameObject> MPAR.Desktop.UI.DesktopInScriptMenu::<BlankMenuItems>k__BackingField
	List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * ___U3CBlankMenuItemsU3Ek__BackingField_8;
	// System.Single MPAR.Desktop.UI.DesktopInScriptMenu::<Columns>k__BackingField
	float ___U3CColumnsU3Ek__BackingField_9;
	// System.String MPAR.Desktop.UI.DesktopInScriptMenu::<Title>k__BackingField
	String_t* ___U3CTitleU3Ek__BackingField_10;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopInScriptMenu::TopBar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___TopBar_11;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopInScriptMenu::BottomBar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BottomBar_12;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopInScriptMenu::Background
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Background_13;

public:
	inline static int32_t get_offset_of_U3CImageMenuFactoryU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CImageMenuFactoryU3Ek__BackingField_4)); }
	inline InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831 * get_U3CImageMenuFactoryU3Ek__BackingField_4() const { return ___U3CImageMenuFactoryU3Ek__BackingField_4; }
	inline InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831 ** get_address_of_U3CImageMenuFactoryU3Ek__BackingField_4() { return &___U3CImageMenuFactoryU3Ek__BackingField_4; }
	inline void set_U3CImageMenuFactoryU3Ek__BackingField_4(InScriptImageMenuItemFactory_tFC549B35BCD50C2B24F5451E89B5F6062E4D5831 * value)
	{
		___U3CImageMenuFactoryU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CImageMenuFactoryU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CTextMenuFactoryU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CTextMenuFactoryU3Ek__BackingField_5)); }
	inline InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632 * get_U3CTextMenuFactoryU3Ek__BackingField_5() const { return ___U3CTextMenuFactoryU3Ek__BackingField_5; }
	inline InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632 ** get_address_of_U3CTextMenuFactoryU3Ek__BackingField_5() { return &___U3CTextMenuFactoryU3Ek__BackingField_5; }
	inline void set_U3CTextMenuFactoryU3Ek__BackingField_5(InScriptTextMenuItemFactory_t6A9C1B69050FE7444D622E3B696778A0A0B9F632 * value)
	{
		___U3CTextMenuFactoryU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextMenuFactoryU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CObjectCollectionU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CObjectCollectionU3Ek__BackingField_6)); }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * get_U3CObjectCollectionU3Ek__BackingField_6() const { return ___U3CObjectCollectionU3Ek__BackingField_6; }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB ** get_address_of_U3CObjectCollectionU3Ek__BackingField_6() { return &___U3CObjectCollectionU3Ek__BackingField_6; }
	inline void set_U3CObjectCollectionU3Ek__BackingField_6(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * value)
	{
		___U3CObjectCollectionU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectCollectionU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CMenuItemsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CMenuItemsU3Ek__BackingField_7)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_U3CMenuItemsU3Ek__BackingField_7() const { return ___U3CMenuItemsU3Ek__BackingField_7; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_U3CMenuItemsU3Ek__BackingField_7() { return &___U3CMenuItemsU3Ek__BackingField_7; }
	inline void set_U3CMenuItemsU3Ek__BackingField_7(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___U3CMenuItemsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMenuItemsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CBlankMenuItemsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CBlankMenuItemsU3Ek__BackingField_8)); }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * get_U3CBlankMenuItemsU3Ek__BackingField_8() const { return ___U3CBlankMenuItemsU3Ek__BackingField_8; }
	inline List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 ** get_address_of_U3CBlankMenuItemsU3Ek__BackingField_8() { return &___U3CBlankMenuItemsU3Ek__BackingField_8; }
	inline void set_U3CBlankMenuItemsU3Ek__BackingField_8(List_1_t99909CDEDA6D21189884AEA74B1FD99FC9C6A4C0 * value)
	{
		___U3CBlankMenuItemsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBlankMenuItemsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CColumnsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CColumnsU3Ek__BackingField_9)); }
	inline float get_U3CColumnsU3Ek__BackingField_9() const { return ___U3CColumnsU3Ek__BackingField_9; }
	inline float* get_address_of_U3CColumnsU3Ek__BackingField_9() { return &___U3CColumnsU3Ek__BackingField_9; }
	inline void set_U3CColumnsU3Ek__BackingField_9(float value)
	{
		___U3CColumnsU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CTitleU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___U3CTitleU3Ek__BackingField_10)); }
	inline String_t* get_U3CTitleU3Ek__BackingField_10() const { return ___U3CTitleU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CTitleU3Ek__BackingField_10() { return &___U3CTitleU3Ek__BackingField_10; }
	inline void set_U3CTitleU3Ek__BackingField_10(String_t* value)
	{
		___U3CTitleU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTitleU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_TopBar_11() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___TopBar_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_TopBar_11() const { return ___TopBar_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_TopBar_11() { return &___TopBar_11; }
	inline void set_TopBar_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___TopBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___TopBar_11), value);
	}

	inline static int32_t get_offset_of_BottomBar_12() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___BottomBar_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BottomBar_12() const { return ___BottomBar_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BottomBar_12() { return &___BottomBar_12; }
	inline void set_BottomBar_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BottomBar_12 = value;
		Il2CppCodeGenWriteBarrier((&___BottomBar_12), value);
	}

	inline static int32_t get_offset_of_Background_13() { return static_cast<int32_t>(offsetof(DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E, ___Background_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Background_13() const { return ___Background_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Background_13() { return &___Background_13; }
	inline void set_Background_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Background_13 = value;
		Il2CppCodeGenWriteBarrier((&___Background_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPINSCRIPTMENU_TD2EFCDB4E715E7882700D8AA0DE43557CB018F8E_H
#ifndef DESKTOPINSCRIPTMENUITEM_TE2315C52652D59C2ADC1FB7F6236EFAF876A0996_H
#define DESKTOPINSCRIPTMENUITEM_TE2315C52652D59C2ADC1FB7F6236EFAF876A0996_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopInScriptMenuItem
struct  DesktopInScriptMenuItem_tE2315C52652D59C2ADC1FB7F6236EFAF876A0996  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action MPAR.Desktop.UI.DesktopInScriptMenuItem::<Callback>k__BackingField
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___U3CCallbackU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCallbackU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DesktopInScriptMenuItem_tE2315C52652D59C2ADC1FB7F6236EFAF876A0996, ___U3CCallbackU3Ek__BackingField_4)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_U3CCallbackU3Ek__BackingField_4() const { return ___U3CCallbackU3Ek__BackingField_4; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_U3CCallbackU3Ek__BackingField_4() { return &___U3CCallbackU3Ek__BackingField_4; }
	inline void set_U3CCallbackU3Ek__BackingField_4(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___U3CCallbackU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallbackU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPINSCRIPTMENUITEM_TE2315C52652D59C2ADC1FB7F6236EFAF876A0996_H
#ifndef DESKTOPMENU_TDFEDD80EFF32E3F27F71E4927F9E8A01336813C3_H
#define DESKTOPMENU_TDFEDD80EFF32E3F27F71E4927F9E8A01336813C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopMenu
struct  DesktopMenu_tDFEDD80EFF32E3F27F71E4927F9E8A01336813C3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPMENU_TDFEDD80EFF32E3F27F71E4927F9E8A01336813C3_H
#ifndef DESKTOPSCRIPTMENU_T08E6D0D858EB826624E6CD4963C25EB3583011B0_H
#define DESKTOPSCRIPTMENU_T08E6D0D858EB826624E6CD4963C25EB3583011B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenu
struct  DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> MPAR.Desktop.UI.DesktopScriptMenu::<Scripts>k__BackingField
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___U3CScriptsU3Ek__BackingField_4;
	// MPAR.Common.Collections.ObjectCollection MPAR.Desktop.UI.DesktopScriptMenu::<ObjectCollection>k__BackingField
	ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * ___U3CObjectCollectionU3Ek__BackingField_5;
	// MPAR.Common.GameObjects.Factories.ScriptMenuItemFactory MPAR.Desktop.UI.DesktopScriptMenu::<ScriptMenuItemFactory>k__BackingField
	ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 * ___U3CScriptMenuItemFactoryU3Ek__BackingField_6;
	// MPAR.Common.Input.Utility.GestureManager MPAR.Desktop.UI.DesktopScriptMenu::<Gestures>k__BackingField
	GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * ___U3CGesturesU3Ek__BackingField_7;
	// MPAR.Common.Persistence.ISaveLoadManager MPAR.Desktop.UI.DesktopScriptMenu::<SaveLoadManager>k__BackingField
	RuntimeObject* ___U3CSaveLoadManagerU3Ek__BackingField_8;
	// System.Single MPAR.Desktop.UI.DesktopScriptMenu::holdClickTime
	float ___holdClickTime_9;
	// System.Single MPAR.Desktop.UI.DesktopScriptMenu::clickDownTime
	float ___clickDownTime_10;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenu::clickObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___clickObject_11;
	// System.Boolean MPAR.Desktop.UI.DesktopScriptMenu::didHoldClick
	bool ___didHoldClick_12;

public:
	inline static int32_t get_offset_of_U3CScriptsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___U3CScriptsU3Ek__BackingField_4)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_U3CScriptsU3Ek__BackingField_4() const { return ___U3CScriptsU3Ek__BackingField_4; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_U3CScriptsU3Ek__BackingField_4() { return &___U3CScriptsU3Ek__BackingField_4; }
	inline void set_U3CScriptsU3Ek__BackingField_4(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___U3CScriptsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptsU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CObjectCollectionU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___U3CObjectCollectionU3Ek__BackingField_5)); }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * get_U3CObjectCollectionU3Ek__BackingField_5() const { return ___U3CObjectCollectionU3Ek__BackingField_5; }
	inline ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB ** get_address_of_U3CObjectCollectionU3Ek__BackingField_5() { return &___U3CObjectCollectionU3Ek__BackingField_5; }
	inline void set_U3CObjectCollectionU3Ek__BackingField_5(ObjectCollection_t6FB07C8809401FD9FEEBD867A1F4044A022417EB * value)
	{
		___U3CObjectCollectionU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CObjectCollectionU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CScriptMenuItemFactoryU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___U3CScriptMenuItemFactoryU3Ek__BackingField_6)); }
	inline ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 * get_U3CScriptMenuItemFactoryU3Ek__BackingField_6() const { return ___U3CScriptMenuItemFactoryU3Ek__BackingField_6; }
	inline ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 ** get_address_of_U3CScriptMenuItemFactoryU3Ek__BackingField_6() { return &___U3CScriptMenuItemFactoryU3Ek__BackingField_6; }
	inline void set_U3CScriptMenuItemFactoryU3Ek__BackingField_6(ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 * value)
	{
		___U3CScriptMenuItemFactoryU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptMenuItemFactoryU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CGesturesU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___U3CGesturesU3Ek__BackingField_7)); }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * get_U3CGesturesU3Ek__BackingField_7() const { return ___U3CGesturesU3Ek__BackingField_7; }
	inline GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 ** get_address_of_U3CGesturesU3Ek__BackingField_7() { return &___U3CGesturesU3Ek__BackingField_7; }
	inline void set_U3CGesturesU3Ek__BackingField_7(GestureManager_t99772F3FD58568F0F15CF073D77397C18B0C1909 * value)
	{
		___U3CGesturesU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGesturesU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___U3CSaveLoadManagerU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CSaveLoadManagerU3Ek__BackingField_8() const { return ___U3CSaveLoadManagerU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CSaveLoadManagerU3Ek__BackingField_8() { return &___U3CSaveLoadManagerU3Ek__BackingField_8; }
	inline void set_U3CSaveLoadManagerU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CSaveLoadManagerU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSaveLoadManagerU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_holdClickTime_9() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___holdClickTime_9)); }
	inline float get_holdClickTime_9() const { return ___holdClickTime_9; }
	inline float* get_address_of_holdClickTime_9() { return &___holdClickTime_9; }
	inline void set_holdClickTime_9(float value)
	{
		___holdClickTime_9 = value;
	}

	inline static int32_t get_offset_of_clickDownTime_10() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___clickDownTime_10)); }
	inline float get_clickDownTime_10() const { return ___clickDownTime_10; }
	inline float* get_address_of_clickDownTime_10() { return &___clickDownTime_10; }
	inline void set_clickDownTime_10(float value)
	{
		___clickDownTime_10 = value;
	}

	inline static int32_t get_offset_of_clickObject_11() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___clickObject_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_clickObject_11() const { return ___clickObject_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_clickObject_11() { return &___clickObject_11; }
	inline void set_clickObject_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___clickObject_11 = value;
		Il2CppCodeGenWriteBarrier((&___clickObject_11), value);
	}

	inline static int32_t get_offset_of_didHoldClick_12() { return static_cast<int32_t>(offsetof(DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0, ___didHoldClick_12)); }
	inline bool get_didHoldClick_12() const { return ___didHoldClick_12; }
	inline bool* get_address_of_didHoldClick_12() { return &___didHoldClick_12; }
	inline void set_didHoldClick_12(bool value)
	{
		___didHoldClick_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPSCRIPTMENU_T08E6D0D858EB826624E6CD4963C25EB3583011B0_H
#ifndef DESKTOPSCRIPTMENUITEM_T2CED641364E3DEC56958ED2D91E24FDEE99B6FF9_H
#define DESKTOPSCRIPTMENUITEM_T2CED641364E3DEC56958ED2D91E24FDEE99B6FF9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.UI.DesktopScriptMenuItem
struct  DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::ThumbnailObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ThumbnailObject_4;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::TopPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___TopPanel_5;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::BottomPanel
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BottomPanel_6;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::Display
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Display_7;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::DisplayObjects
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DisplayObjects_8;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::RunButton
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___RunButton_9;
	// UnityEngine.GameObject MPAR.Desktop.UI.DesktopScriptMenuItem::BlockedOverlay
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___BlockedOverlay_10;
	// System.Boolean MPAR.Desktop.UI.DesktopScriptMenuItem::<IsSelected>k__BackingField
	bool ___U3CIsSelectedU3Ek__BackingField_11;
	// MPAR.Common.Scripting.Script MPAR.Desktop.UI.DesktopScriptMenuItem::script
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___script_12;

public:
	inline static int32_t get_offset_of_ThumbnailObject_4() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___ThumbnailObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ThumbnailObject_4() const { return ___ThumbnailObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ThumbnailObject_4() { return &___ThumbnailObject_4; }
	inline void set_ThumbnailObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ThumbnailObject_4 = value;
		Il2CppCodeGenWriteBarrier((&___ThumbnailObject_4), value);
	}

	inline static int32_t get_offset_of_TopPanel_5() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___TopPanel_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_TopPanel_5() const { return ___TopPanel_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_TopPanel_5() { return &___TopPanel_5; }
	inline void set_TopPanel_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___TopPanel_5 = value;
		Il2CppCodeGenWriteBarrier((&___TopPanel_5), value);
	}

	inline static int32_t get_offset_of_BottomPanel_6() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___BottomPanel_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BottomPanel_6() const { return ___BottomPanel_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BottomPanel_6() { return &___BottomPanel_6; }
	inline void set_BottomPanel_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BottomPanel_6 = value;
		Il2CppCodeGenWriteBarrier((&___BottomPanel_6), value);
	}

	inline static int32_t get_offset_of_Display_7() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___Display_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Display_7() const { return ___Display_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Display_7() { return &___Display_7; }
	inline void set_Display_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Display_7 = value;
		Il2CppCodeGenWriteBarrier((&___Display_7), value);
	}

	inline static int32_t get_offset_of_DisplayObjects_8() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___DisplayObjects_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DisplayObjects_8() const { return ___DisplayObjects_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DisplayObjects_8() { return &___DisplayObjects_8; }
	inline void set_DisplayObjects_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DisplayObjects_8 = value;
		Il2CppCodeGenWriteBarrier((&___DisplayObjects_8), value);
	}

	inline static int32_t get_offset_of_RunButton_9() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___RunButton_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_RunButton_9() const { return ___RunButton_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_RunButton_9() { return &___RunButton_9; }
	inline void set_RunButton_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___RunButton_9 = value;
		Il2CppCodeGenWriteBarrier((&___RunButton_9), value);
	}

	inline static int32_t get_offset_of_BlockedOverlay_10() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___BlockedOverlay_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_BlockedOverlay_10() const { return ___BlockedOverlay_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_BlockedOverlay_10() { return &___BlockedOverlay_10; }
	inline void set_BlockedOverlay_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___BlockedOverlay_10 = value;
		Il2CppCodeGenWriteBarrier((&___BlockedOverlay_10), value);
	}

	inline static int32_t get_offset_of_U3CIsSelectedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___U3CIsSelectedU3Ek__BackingField_11)); }
	inline bool get_U3CIsSelectedU3Ek__BackingField_11() const { return ___U3CIsSelectedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3CIsSelectedU3Ek__BackingField_11() { return &___U3CIsSelectedU3Ek__BackingField_11; }
	inline void set_U3CIsSelectedU3Ek__BackingField_11(bool value)
	{
		___U3CIsSelectedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_script_12() { return static_cast<int32_t>(offsetof(DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9, ___script_12)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_script_12() const { return ___script_12; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_script_12() { return &___script_12; }
	inline void set_script_12(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___script_12 = value;
		Il2CppCodeGenWriteBarrier((&___script_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPSCRIPTMENUITEM_T2CED641364E3DEC56958ED2D91E24FDEE99B6FF9_H
#ifndef MOBILESCRIPTMENUITEM_T3C50F345345CB346D993D63DC68C1083C9E5E190_H
#define MOBILESCRIPTMENUITEM_T3C50F345345CB346D993D63DC68C1083C9E5E190_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Mobile.UI.MobileScriptMenuItem
struct  MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.Scripting.Script MPAR.Mobile.UI.MobileScriptMenuItem::<Script>k__BackingField
	Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * ___U3CScriptU3Ek__BackingField_4;
	// UnityEngine.GameObject MPAR.Mobile.UI.MobileScriptMenuItem::<RunButton>k__BackingField
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CRunButtonU3Ek__BackingField_5;
	// UnityEngine.UI.Text MPAR.Mobile.UI.MobileScriptMenuItem::<NameTextField>k__BackingField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___U3CNameTextFieldU3Ek__BackingField_6;
	// UnityEngine.UI.Text MPAR.Mobile.UI.MobileScriptMenuItem::<AuthorTextField>k__BackingField
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___U3CAuthorTextFieldU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_U3CScriptU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190, ___U3CScriptU3Ek__BackingField_4)); }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * get_U3CScriptU3Ek__BackingField_4() const { return ___U3CScriptU3Ek__BackingField_4; }
	inline Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC ** get_address_of_U3CScriptU3Ek__BackingField_4() { return &___U3CScriptU3Ek__BackingField_4; }
	inline void set_U3CScriptU3Ek__BackingField_4(Script_tD382BF9FFF5EE400AD9E33ED237844FA6BE64FFC * value)
	{
		___U3CScriptU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScriptU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CRunButtonU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190, ___U3CRunButtonU3Ek__BackingField_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CRunButtonU3Ek__BackingField_5() const { return ___U3CRunButtonU3Ek__BackingField_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CRunButtonU3Ek__BackingField_5() { return &___U3CRunButtonU3Ek__BackingField_5; }
	inline void set_U3CRunButtonU3Ek__BackingField_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CRunButtonU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRunButtonU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CNameTextFieldU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190, ___U3CNameTextFieldU3Ek__BackingField_6)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_U3CNameTextFieldU3Ek__BackingField_6() const { return ___U3CNameTextFieldU3Ek__BackingField_6; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_U3CNameTextFieldU3Ek__BackingField_6() { return &___U3CNameTextFieldU3Ek__BackingField_6; }
	inline void set_U3CNameTextFieldU3Ek__BackingField_6(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___U3CNameTextFieldU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameTextFieldU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CAuthorTextFieldU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190, ___U3CAuthorTextFieldU3Ek__BackingField_7)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_U3CAuthorTextFieldU3Ek__BackingField_7() const { return ___U3CAuthorTextFieldU3Ek__BackingField_7; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_U3CAuthorTextFieldU3Ek__BackingField_7() { return &___U3CAuthorTextFieldU3Ek__BackingField_7; }
	inline void set_U3CAuthorTextFieldU3Ek__BackingField_7(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___U3CAuthorTextFieldU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthorTextFieldU3Ek__BackingField_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILESCRIPTMENUITEM_T3C50F345345CB346D993D63DC68C1083C9E5E190_H
#ifndef IOSSCRIPTMENU_T82A6900C31A748C42851FF56BA55381C30AE1D78_H
#define IOSSCRIPTMENU_T82A6900C31A748C42851FF56BA55381C30AE1D78_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.iOS.UI.iOSScriptMenu
struct  iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MPAR.Common.GameObjects.Factories.CanvasFactory MPAR.iOS.UI.iOSScriptMenu::canvasFactory
	CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E * ___canvasFactory_4;
	// MPAR.Common.GameObjects.Factories.ScriptMenuItemFactory MPAR.iOS.UI.iOSScriptMenu::scriptMenuItemFactory
	ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 * ___scriptMenuItemFactory_5;
	// MPAR.Common.GameObjects.Factories.InGameConsoleFactory MPAR.iOS.UI.iOSScriptMenu::inGameConsoleFactory
	InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF * ___inGameConsoleFactory_6;
	// MPAR.Common.Scripting.ScriptManager MPAR.iOS.UI.iOSScriptMenu::scriptManager
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * ___scriptManager_7;
	// UnityEngine.Canvas MPAR.iOS.UI.iOSScriptMenu::menuCanvas
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___menuCanvas_8;
	// MPAR.Common.GameObjects.Factories.PlaneGeneratorFactory MPAR.iOS.UI.iOSScriptMenu::planeGeneratorFactory
	PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D * ___planeGeneratorFactory_9;
	// MPAR.Common.GameObjects.Factories.PointCloudParticlesFactory MPAR.iOS.UI.iOSScriptMenu::pointCloudParticlesFactory
	PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6 * ___pointCloudParticlesFactory_10;
	// System.Collections.Generic.List`1<MPAR.Common.Scripting.Script> MPAR.iOS.UI.iOSScriptMenu::scripts
	List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * ___scripts_11;
	// System.Boolean MPAR.iOS.UI.iOSScriptMenu::initializedConsole
	bool ___initializedConsole_12;

public:
	inline static int32_t get_offset_of_canvasFactory_4() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___canvasFactory_4)); }
	inline CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E * get_canvasFactory_4() const { return ___canvasFactory_4; }
	inline CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E ** get_address_of_canvasFactory_4() { return &___canvasFactory_4; }
	inline void set_canvasFactory_4(CanvasFactory_tAF9EC33C5A509E003E1163CE29E768DB33C1585E * value)
	{
		___canvasFactory_4 = value;
		Il2CppCodeGenWriteBarrier((&___canvasFactory_4), value);
	}

	inline static int32_t get_offset_of_scriptMenuItemFactory_5() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___scriptMenuItemFactory_5)); }
	inline ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 * get_scriptMenuItemFactory_5() const { return ___scriptMenuItemFactory_5; }
	inline ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 ** get_address_of_scriptMenuItemFactory_5() { return &___scriptMenuItemFactory_5; }
	inline void set_scriptMenuItemFactory_5(ScriptMenuItemFactory_tCF9F400F562098E16C15EAAF14FF009406EAD8F3 * value)
	{
		___scriptMenuItemFactory_5 = value;
		Il2CppCodeGenWriteBarrier((&___scriptMenuItemFactory_5), value);
	}

	inline static int32_t get_offset_of_inGameConsoleFactory_6() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___inGameConsoleFactory_6)); }
	inline InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF * get_inGameConsoleFactory_6() const { return ___inGameConsoleFactory_6; }
	inline InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF ** get_address_of_inGameConsoleFactory_6() { return &___inGameConsoleFactory_6; }
	inline void set_inGameConsoleFactory_6(InGameConsoleFactory_tFE253A8056DABE48E94F01733DF37C5187F622CF * value)
	{
		___inGameConsoleFactory_6 = value;
		Il2CppCodeGenWriteBarrier((&___inGameConsoleFactory_6), value);
	}

	inline static int32_t get_offset_of_scriptManager_7() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___scriptManager_7)); }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * get_scriptManager_7() const { return ___scriptManager_7; }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C ** get_address_of_scriptManager_7() { return &___scriptManager_7; }
	inline void set_scriptManager_7(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * value)
	{
		___scriptManager_7 = value;
		Il2CppCodeGenWriteBarrier((&___scriptManager_7), value);
	}

	inline static int32_t get_offset_of_menuCanvas_8() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___menuCanvas_8)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_menuCanvas_8() const { return ___menuCanvas_8; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_menuCanvas_8() { return &___menuCanvas_8; }
	inline void set_menuCanvas_8(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___menuCanvas_8 = value;
		Il2CppCodeGenWriteBarrier((&___menuCanvas_8), value);
	}

	inline static int32_t get_offset_of_planeGeneratorFactory_9() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___planeGeneratorFactory_9)); }
	inline PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D * get_planeGeneratorFactory_9() const { return ___planeGeneratorFactory_9; }
	inline PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D ** get_address_of_planeGeneratorFactory_9() { return &___planeGeneratorFactory_9; }
	inline void set_planeGeneratorFactory_9(PlaneGeneratorFactory_t5220D4ACD7E2F8891475B1782E6C4B6A0FA7974D * value)
	{
		___planeGeneratorFactory_9 = value;
		Il2CppCodeGenWriteBarrier((&___planeGeneratorFactory_9), value);
	}

	inline static int32_t get_offset_of_pointCloudParticlesFactory_10() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___pointCloudParticlesFactory_10)); }
	inline PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6 * get_pointCloudParticlesFactory_10() const { return ___pointCloudParticlesFactory_10; }
	inline PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6 ** get_address_of_pointCloudParticlesFactory_10() { return &___pointCloudParticlesFactory_10; }
	inline void set_pointCloudParticlesFactory_10(PointCloudParticlesFactory_t528EF437395FFF08E3C638E390B8EEA242CECAB6 * value)
	{
		___pointCloudParticlesFactory_10 = value;
		Il2CppCodeGenWriteBarrier((&___pointCloudParticlesFactory_10), value);
	}

	inline static int32_t get_offset_of_scripts_11() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___scripts_11)); }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * get_scripts_11() const { return ___scripts_11; }
	inline List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 ** get_address_of_scripts_11() { return &___scripts_11; }
	inline void set_scripts_11(List_1_t5A4FB135B9E0A51301CA6A91563289B792B919A1 * value)
	{
		___scripts_11 = value;
		Il2CppCodeGenWriteBarrier((&___scripts_11), value);
	}

	inline static int32_t get_offset_of_initializedConsole_12() { return static_cast<int32_t>(offsetof(iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78, ___initializedConsole_12)); }
	inline bool get_initializedConsole_12() const { return ___initializedConsole_12; }
	inline bool* get_address_of_initializedConsole_12() { return &___initializedConsole_12; }
	inline void set_initializedConsole_12(bool value)
	{
		___initializedConsole_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSSCRIPTMENU_T82A6900C31A748C42851FF56BA55381C30AE1D78_H
#ifndef CONTENTPREFABS_TB3F5A5002ED26D8E2F9525C7A4903895047B72B0_H
#define CONTENTPREFABS_TB3F5A5002ED26D8E2F9525C7A4903895047B72B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets.ContentPrefabs
struct  ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0  : public Singleton_1_t1241BD9D5BC769A065344C3AD2E1CB65FEB7E8DB
{
public:
	// UnityEngine.GameObject Assets.ContentPrefabs::ProjectileBullet
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ProjectileBullet_6;
	// UnityEngine.GameObject Assets.ContentPrefabs::ProjectileFireball
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ProjectileFireball_7;
	// UnityEngine.GameObject Assets.ContentPrefabs::ProjectileLightningBolt
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___ProjectileLightningBolt_8;
	// UnityEngine.GameObject Assets.ContentPrefabs::EffectExplosion
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EffectExplosion_9;
	// UnityEngine.GameObject Assets.ContentPrefabs::EffectFlare
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EffectFlare_10;
	// UnityEngine.GameObject Assets.ContentPrefabs::EffectSmoke
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___EffectSmoke_11;
	// UnityEngine.GameObject Assets.ContentPrefabs::DebugFastPlayer
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___DebugFastPlayer_12;
	// UnityEngine.GameObject Assets.ContentPrefabs::HealthBar
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___HealthBar_13;
	// UnityEngine.GameObject Assets.ContentPrefabs::Zombie
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Zombie_14;
	// UnityEngine.GameObject Assets.ContentPrefabs::Drone
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___Drone_15;
	// UnityEngine.Material Assets.ContentPrefabs::MaterialShield
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___MaterialShield_16;
	// System.Type Assets.ContentPrefabs::behaviourSpaceInvadersAi
	Type_t * ___behaviourSpaceInvadersAi_17;
	// System.Type Assets.ContentPrefabs::behaviourSpaceInvadersEdge
	Type_t * ___behaviourSpaceInvadersEdge_18;
	// System.Type Assets.ContentPrefabs::behaviourSpaceInvadersInvasion
	Type_t * ___behaviourSpaceInvadersInvasion_19;
	// System.Type Assets.ContentPrefabs::behaviourSpaceInvadersEvents
	Type_t * ___behaviourSpaceInvadersEvents_20;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.GameObject> Assets.ContentPrefabs::effectsByName
	Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * ___effectsByName_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> Assets.ContentPrefabs::effectSynonyms
	Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * ___effectSynonyms_22;
	// System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Material> Assets.ContentPrefabs::materialsByName
	Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * ___materialsByName_23;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> Assets.ContentPrefabs::materialSynonyms
	Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * ___materialSynonyms_24;
	// System.Collections.Generic.Dictionary`2<System.String,System.Type> Assets.ContentPrefabs::scriptsByName
	Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * ___scriptsByName_25;
	// System.Collections.Generic.Dictionary`2<System.String,System.String[]> Assets.ContentPrefabs::scriptSynonyms
	Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * ___scriptSynonyms_26;

public:
	inline static int32_t get_offset_of_ProjectileBullet_6() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___ProjectileBullet_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ProjectileBullet_6() const { return ___ProjectileBullet_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ProjectileBullet_6() { return &___ProjectileBullet_6; }
	inline void set_ProjectileBullet_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ProjectileBullet_6 = value;
		Il2CppCodeGenWriteBarrier((&___ProjectileBullet_6), value);
	}

	inline static int32_t get_offset_of_ProjectileFireball_7() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___ProjectileFireball_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ProjectileFireball_7() const { return ___ProjectileFireball_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ProjectileFireball_7() { return &___ProjectileFireball_7; }
	inline void set_ProjectileFireball_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ProjectileFireball_7 = value;
		Il2CppCodeGenWriteBarrier((&___ProjectileFireball_7), value);
	}

	inline static int32_t get_offset_of_ProjectileLightningBolt_8() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___ProjectileLightningBolt_8)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_ProjectileLightningBolt_8() const { return ___ProjectileLightningBolt_8; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_ProjectileLightningBolt_8() { return &___ProjectileLightningBolt_8; }
	inline void set_ProjectileLightningBolt_8(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___ProjectileLightningBolt_8 = value;
		Il2CppCodeGenWriteBarrier((&___ProjectileLightningBolt_8), value);
	}

	inline static int32_t get_offset_of_EffectExplosion_9() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___EffectExplosion_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EffectExplosion_9() const { return ___EffectExplosion_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EffectExplosion_9() { return &___EffectExplosion_9; }
	inline void set_EffectExplosion_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EffectExplosion_9 = value;
		Il2CppCodeGenWriteBarrier((&___EffectExplosion_9), value);
	}

	inline static int32_t get_offset_of_EffectFlare_10() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___EffectFlare_10)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EffectFlare_10() const { return ___EffectFlare_10; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EffectFlare_10() { return &___EffectFlare_10; }
	inline void set_EffectFlare_10(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EffectFlare_10 = value;
		Il2CppCodeGenWriteBarrier((&___EffectFlare_10), value);
	}

	inline static int32_t get_offset_of_EffectSmoke_11() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___EffectSmoke_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_EffectSmoke_11() const { return ___EffectSmoke_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_EffectSmoke_11() { return &___EffectSmoke_11; }
	inline void set_EffectSmoke_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___EffectSmoke_11 = value;
		Il2CppCodeGenWriteBarrier((&___EffectSmoke_11), value);
	}

	inline static int32_t get_offset_of_DebugFastPlayer_12() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___DebugFastPlayer_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_DebugFastPlayer_12() const { return ___DebugFastPlayer_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_DebugFastPlayer_12() { return &___DebugFastPlayer_12; }
	inline void set_DebugFastPlayer_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___DebugFastPlayer_12 = value;
		Il2CppCodeGenWriteBarrier((&___DebugFastPlayer_12), value);
	}

	inline static int32_t get_offset_of_HealthBar_13() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___HealthBar_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_HealthBar_13() const { return ___HealthBar_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_HealthBar_13() { return &___HealthBar_13; }
	inline void set_HealthBar_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___HealthBar_13 = value;
		Il2CppCodeGenWriteBarrier((&___HealthBar_13), value);
	}

	inline static int32_t get_offset_of_Zombie_14() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___Zombie_14)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Zombie_14() const { return ___Zombie_14; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Zombie_14() { return &___Zombie_14; }
	inline void set_Zombie_14(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Zombie_14 = value;
		Il2CppCodeGenWriteBarrier((&___Zombie_14), value);
	}

	inline static int32_t get_offset_of_Drone_15() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___Drone_15)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_Drone_15() const { return ___Drone_15; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_Drone_15() { return &___Drone_15; }
	inline void set_Drone_15(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___Drone_15 = value;
		Il2CppCodeGenWriteBarrier((&___Drone_15), value);
	}

	inline static int32_t get_offset_of_MaterialShield_16() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___MaterialShield_16)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get_MaterialShield_16() const { return ___MaterialShield_16; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of_MaterialShield_16() { return &___MaterialShield_16; }
	inline void set_MaterialShield_16(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		___MaterialShield_16 = value;
		Il2CppCodeGenWriteBarrier((&___MaterialShield_16), value);
	}

	inline static int32_t get_offset_of_behaviourSpaceInvadersAi_17() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___behaviourSpaceInvadersAi_17)); }
	inline Type_t * get_behaviourSpaceInvadersAi_17() const { return ___behaviourSpaceInvadersAi_17; }
	inline Type_t ** get_address_of_behaviourSpaceInvadersAi_17() { return &___behaviourSpaceInvadersAi_17; }
	inline void set_behaviourSpaceInvadersAi_17(Type_t * value)
	{
		___behaviourSpaceInvadersAi_17 = value;
		Il2CppCodeGenWriteBarrier((&___behaviourSpaceInvadersAi_17), value);
	}

	inline static int32_t get_offset_of_behaviourSpaceInvadersEdge_18() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___behaviourSpaceInvadersEdge_18)); }
	inline Type_t * get_behaviourSpaceInvadersEdge_18() const { return ___behaviourSpaceInvadersEdge_18; }
	inline Type_t ** get_address_of_behaviourSpaceInvadersEdge_18() { return &___behaviourSpaceInvadersEdge_18; }
	inline void set_behaviourSpaceInvadersEdge_18(Type_t * value)
	{
		___behaviourSpaceInvadersEdge_18 = value;
		Il2CppCodeGenWriteBarrier((&___behaviourSpaceInvadersEdge_18), value);
	}

	inline static int32_t get_offset_of_behaviourSpaceInvadersInvasion_19() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___behaviourSpaceInvadersInvasion_19)); }
	inline Type_t * get_behaviourSpaceInvadersInvasion_19() const { return ___behaviourSpaceInvadersInvasion_19; }
	inline Type_t ** get_address_of_behaviourSpaceInvadersInvasion_19() { return &___behaviourSpaceInvadersInvasion_19; }
	inline void set_behaviourSpaceInvadersInvasion_19(Type_t * value)
	{
		___behaviourSpaceInvadersInvasion_19 = value;
		Il2CppCodeGenWriteBarrier((&___behaviourSpaceInvadersInvasion_19), value);
	}

	inline static int32_t get_offset_of_behaviourSpaceInvadersEvents_20() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___behaviourSpaceInvadersEvents_20)); }
	inline Type_t * get_behaviourSpaceInvadersEvents_20() const { return ___behaviourSpaceInvadersEvents_20; }
	inline Type_t ** get_address_of_behaviourSpaceInvadersEvents_20() { return &___behaviourSpaceInvadersEvents_20; }
	inline void set_behaviourSpaceInvadersEvents_20(Type_t * value)
	{
		___behaviourSpaceInvadersEvents_20 = value;
		Il2CppCodeGenWriteBarrier((&___behaviourSpaceInvadersEvents_20), value);
	}

	inline static int32_t get_offset_of_effectsByName_21() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___effectsByName_21)); }
	inline Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * get_effectsByName_21() const { return ___effectsByName_21; }
	inline Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A ** get_address_of_effectsByName_21() { return &___effectsByName_21; }
	inline void set_effectsByName_21(Dictionary_2_tB917211DA126597937FC7D5AF12A9E1B7A763F6A * value)
	{
		___effectsByName_21 = value;
		Il2CppCodeGenWriteBarrier((&___effectsByName_21), value);
	}

	inline static int32_t get_offset_of_effectSynonyms_22() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___effectSynonyms_22)); }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * get_effectSynonyms_22() const { return ___effectSynonyms_22; }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 ** get_address_of_effectSynonyms_22() { return &___effectSynonyms_22; }
	inline void set_effectSynonyms_22(Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * value)
	{
		___effectSynonyms_22 = value;
		Il2CppCodeGenWriteBarrier((&___effectSynonyms_22), value);
	}

	inline static int32_t get_offset_of_materialsByName_23() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___materialsByName_23)); }
	inline Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * get_materialsByName_23() const { return ___materialsByName_23; }
	inline Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC ** get_address_of_materialsByName_23() { return &___materialsByName_23; }
	inline void set_materialsByName_23(Dictionary_2_tAD2222CCFCF5F2964C6F67732E9C8DAB764FE3AC * value)
	{
		___materialsByName_23 = value;
		Il2CppCodeGenWriteBarrier((&___materialsByName_23), value);
	}

	inline static int32_t get_offset_of_materialSynonyms_24() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___materialSynonyms_24)); }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * get_materialSynonyms_24() const { return ___materialSynonyms_24; }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 ** get_address_of_materialSynonyms_24() { return &___materialSynonyms_24; }
	inline void set_materialSynonyms_24(Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * value)
	{
		___materialSynonyms_24 = value;
		Il2CppCodeGenWriteBarrier((&___materialSynonyms_24), value);
	}

	inline static int32_t get_offset_of_scriptsByName_25() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___scriptsByName_25)); }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * get_scriptsByName_25() const { return ___scriptsByName_25; }
	inline Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A ** get_address_of_scriptsByName_25() { return &___scriptsByName_25; }
	inline void set_scriptsByName_25(Dictionary_2_t2DB4209C32F7303EA559E23BF9E6AEAE8F21001A * value)
	{
		___scriptsByName_25 = value;
		Il2CppCodeGenWriteBarrier((&___scriptsByName_25), value);
	}

	inline static int32_t get_offset_of_scriptSynonyms_26() { return static_cast<int32_t>(offsetof(ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0, ___scriptSynonyms_26)); }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * get_scriptSynonyms_26() const { return ___scriptSynonyms_26; }
	inline Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 ** get_address_of_scriptSynonyms_26() { return &___scriptSynonyms_26; }
	inline void set_scriptSynonyms_26(Dictionary_2_t4465ACD7C124B005D21C7A07E9C9A5247A3DC072 * value)
	{
		___scriptSynonyms_26 = value;
		Il2CppCodeGenWriteBarrier((&___scriptSynonyms_26), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTPREFABS_TB3F5A5002ED26D8E2F9525C7A4903895047B72B0_H
#ifndef MOBILEPLANEDETECTOR_TEDF662370AD6EDA2D748F13123CD36CB65125CE3_H
#define MOBILEPLANEDETECTOR_TEDF662370AD6EDA2D748F13123CD36CB65125CE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector
struct  MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3  : public PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F
{
public:
	// PlaneDetectionController Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector::detectionController
	PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD * ___detectionController_4;
	// UnityEngine.XR.ARFoundation.ARReferencePointManager Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector::referencePointManager
	ARReferencePointManager_t56502A19AD560012C1DBFF29BA70341D79A9604E * ___referencePointManager_5;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Experimental.XR.TrackableId,MPAR.Common.PlaneDetection.Plane> Assets._Project.Scripts.Common.PlaneDetection.MobilePlaneDetector::planesById
	Dictionary_2_t0E2365C5C5573E791ACD987D4CFEEA8778FE2C6A * ___planesById_6;

public:
	inline static int32_t get_offset_of_detectionController_4() { return static_cast<int32_t>(offsetof(MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3, ___detectionController_4)); }
	inline PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD * get_detectionController_4() const { return ___detectionController_4; }
	inline PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD ** get_address_of_detectionController_4() { return &___detectionController_4; }
	inline void set_detectionController_4(PlaneDetectionController_tF91A38E8CF0739882A647588DB89B4E0B91482DD * value)
	{
		___detectionController_4 = value;
		Il2CppCodeGenWriteBarrier((&___detectionController_4), value);
	}

	inline static int32_t get_offset_of_referencePointManager_5() { return static_cast<int32_t>(offsetof(MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3, ___referencePointManager_5)); }
	inline ARReferencePointManager_t56502A19AD560012C1DBFF29BA70341D79A9604E * get_referencePointManager_5() const { return ___referencePointManager_5; }
	inline ARReferencePointManager_t56502A19AD560012C1DBFF29BA70341D79A9604E ** get_address_of_referencePointManager_5() { return &___referencePointManager_5; }
	inline void set_referencePointManager_5(ARReferencePointManager_t56502A19AD560012C1DBFF29BA70341D79A9604E * value)
	{
		___referencePointManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___referencePointManager_5), value);
	}

	inline static int32_t get_offset_of_planesById_6() { return static_cast<int32_t>(offsetof(MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3, ___planesById_6)); }
	inline Dictionary_2_t0E2365C5C5573E791ACD987D4CFEEA8778FE2C6A * get_planesById_6() const { return ___planesById_6; }
	inline Dictionary_2_t0E2365C5C5573E791ACD987D4CFEEA8778FE2C6A ** get_address_of_planesById_6() { return &___planesById_6; }
	inline void set_planesById_6(Dictionary_2_t0E2365C5C5573E791ACD987D4CFEEA8778FE2C6A * value)
	{
		___planesById_6 = value;
		Il2CppCodeGenWriteBarrier((&___planesById_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOBILEPLANEDETECTOR_TEDF662370AD6EDA2D748F13123CD36CB65125CE3_H
#ifndef GAMESCENEINIT_T8A16F4477CBFBAF4C63295EE7742644E9F35869E_H
#define GAMESCENEINIT_T8A16F4477CBFBAF4C63295EE7742644E9F35869E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.SceneControllers.GameSceneInit
struct  GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E  : public Singleton_1_t17131FB73A6E9FBC553D4B01F768CFAD10AE66BF
{
public:
	// MPAR.Common.Scripting.ScriptManager Assets._Project.Scripts.Common.SceneControllers.GameSceneInit::scriptManager
	ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * ___scriptManager_6;
	// System.Boolean Assets._Project.Scripts.Common.SceneControllers.GameSceneInit::runDefaultScript
	bool ___runDefaultScript_7;
	// System.String Assets._Project.Scripts.Common.SceneControllers.GameSceneInit::defaultScript
	String_t* ___defaultScript_8;

public:
	inline static int32_t get_offset_of_scriptManager_6() { return static_cast<int32_t>(offsetof(GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E, ___scriptManager_6)); }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * get_scriptManager_6() const { return ___scriptManager_6; }
	inline ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C ** get_address_of_scriptManager_6() { return &___scriptManager_6; }
	inline void set_scriptManager_6(ScriptManager_tDDEF980C92189FD070F605511CA2EA9A04188F3C * value)
	{
		___scriptManager_6 = value;
		Il2CppCodeGenWriteBarrier((&___scriptManager_6), value);
	}

	inline static int32_t get_offset_of_runDefaultScript_7() { return static_cast<int32_t>(offsetof(GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E, ___runDefaultScript_7)); }
	inline bool get_runDefaultScript_7() const { return ___runDefaultScript_7; }
	inline bool* get_address_of_runDefaultScript_7() { return &___runDefaultScript_7; }
	inline void set_runDefaultScript_7(bool value)
	{
		___runDefaultScript_7 = value;
	}

	inline static int32_t get_offset_of_defaultScript_8() { return static_cast<int32_t>(offsetof(GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E, ___defaultScript_8)); }
	inline String_t* get_defaultScript_8() const { return ___defaultScript_8; }
	inline String_t** get_address_of_defaultScript_8() { return &___defaultScript_8; }
	inline void set_defaultScript_8(String_t* value)
	{
		___defaultScript_8 = value;
		Il2CppCodeGenWriteBarrier((&___defaultScript_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMESCENEINIT_T8A16F4477CBFBAF4C63295EE7742644E9F35869E_H
#ifndef SCRIPTERRORHANDLER_T86C54D9B0DA7092755777EFF56D5D23039FEA924_H
#define SCRIPTERRORHANDLER_T86C54D9B0DA7092755777EFF56D5D23039FEA924_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.Scripting.ErrorHandling.ScriptErrorHandler
struct  ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924  : public Singleton_1_tD42AA6D2C7456D43DD09A7910761A0A624E8D069
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTERRORHANDLER_T86C54D9B0DA7092755777EFF56D5D23039FEA924_H
#ifndef SCRIPTCANVAS_T3B28CA4BA9CD66AD0778254CDB3704D0AABBB634_H
#define SCRIPTCANVAS_T3B28CA4BA9CD66AD0778254CDB3704D0AABBB634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Assets._Project.Scripts.Common.UI.ScriptCanvas
struct  ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634  : public Singleton_1_t4119C92670D5A7655597C8B5A609FDC46FB66416
{
public:
	// UnityEngine.Canvas Assets._Project.Scripts.Common.UI.ScriptCanvas::<ScreenSpaceCanvas>k__BackingField
	Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * ___U3CScreenSpaceCanvasU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CScreenSpaceCanvasU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634, ___U3CScreenSpaceCanvasU3Ek__BackingField_6)); }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * get_U3CScreenSpaceCanvasU3Ek__BackingField_6() const { return ___U3CScreenSpaceCanvasU3Ek__BackingField_6; }
	inline Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 ** get_address_of_U3CScreenSpaceCanvasU3Ek__BackingField_6() { return &___U3CScreenSpaceCanvasU3Ek__BackingField_6; }
	inline void set_U3CScreenSpaceCanvasU3Ek__BackingField_6(Canvas_tBC28BF1DD8D8499A89B5781505833D3728CF8591 * value)
	{
		___U3CScreenSpaceCanvasU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScreenSpaceCanvasU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTCANVAS_T3B28CA4BA9CD66AD0778254CDB3704D0AABBB634_H
#ifndef DESKTOPASSETLOADER_TB37AB66B5E615BDE30906AF9D7C5878131B12EE3_H
#define DESKTOPASSETLOADER_TB37AB66B5E615BDE30906AF9D7C5878131B12EE3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.Desktop.AssetLoader.DesktopAssetLoader
struct  DesktopAssetLoader_tB37AB66B5E615BDE30906AF9D7C5878131B12EE3  : public AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESKTOPASSETLOADER_TB37AB66B5E615BDE30906AF9D7C5878131B12EE3_H
#ifndef IOSASSETLOADER_TCD84F3E98F93FCD6D2654F10FBC4D10CE9EB6ED0_H
#define IOSASSETLOADER_TCD84F3E98F93FCD6D2654F10FBC4D10CE9EB6ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MPAR.iOS.AssetLoader.iOSAssetLoader
struct  iOSAssetLoader_tCD84F3E98F93FCD6D2654F10FBC4D10CE9EB6ED0  : public AbstractAssetLoader_t8FE1E80CC3316AB57156FE25E056F61190B9F4C2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IOSASSETLOADER_TCD84F3E98F93FCD6D2654F10FBC4D10CE9EB6ED0_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7900 = { sizeof (AssetBundleLoadManifestOperation_t6E6263963D4775195C2ACC0196BABEF3678BE214), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7901 = { sizeof (LoadedAssetBundle_tE0CCCFBAB56DF219CD81343D8CC540113976831A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7901[2] = 
{
	LoadedAssetBundle_tE0CCCFBAB56DF219CD81343D8CC540113976831A::get_offset_of_m_AssetBundle_0(),
	LoadedAssetBundle_tE0CCCFBAB56DF219CD81343D8CC540113976831A::get_offset_of_m_ReferencedCount_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7902 = { sizeof (AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E), -1, sizeof(AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7902[9] = 
{
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_LogMode_4(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_BaseDownloadingURL_5(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_ActiveVariants_6(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_AssetBundleManifest_7(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_LoadedAssetBundles_8(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_DownloadingWWWs_9(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_DownloadingErrors_10(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_InProgressOperations_11(),
	AssetBundleManager_t258145CFBD9046E0F391FC4D4866BB084F75F36E_StaticFields::get_offset_of_m_Dependencies_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7903 = { sizeof (LogMode_tCB0B04DDADD0871432F2D368D237ED071FE60785)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7903[3] = 
{
	LogMode_tCB0B04DDADD0871432F2D368D237ED071FE60785::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7904 = { sizeof (LogType_t223D0A938BDA956016978CB18B9065559DAF0123)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7904[4] = 
{
	LogType_t223D0A938BDA956016978CB18B9065559DAF0123::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7905 = { sizeof (Utility_t0F3CA8FDC4DD6AA5306377F96F999773CD6E73F9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7905[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7906 = { sizeof (Debugging_t29990C468C298B106B7894B1E5B224C59357A26E), -1, sizeof(Debugging_t29990C468C298B106B7894B1E5B224C59357A26E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7906[2] = 
{
	Debugging_t29990C468C298B106B7894B1E5B224C59357A26E_StaticFields::get_offset_of_NextDebugRequest_0(),
	Debugging_t29990C468C298B106B7894B1E5B224C59357A26E_StaticFields::get_offset_of_SendingRequest_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7907 = { sizeof (U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A), -1, sizeof(U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7907[1] = 
{
	U3CU3Ec_t45EEC4F8C66A1ABC40FC90FF151BE61500F00B5A_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7908 = { sizeof (U3CU3CDebugU3Eg__callbackU7C5_0U3Ed_tF05704AA4D35F56C4B4A29E36B288E0236B0777B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7908[2] = 
{
	U3CU3CDebugU3Eg__callbackU7C5_0U3Ed_tF05704AA4D35F56C4B4A29E36B288E0236B0777B::get_offset_of_U3CU3E1__state_0(),
	U3CU3CDebugU3Eg__callbackU7C5_0U3Ed_tF05704AA4D35F56C4B4A29E36B288E0236B0777B::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7909 = { sizeof (U3CU3CExecuteRequestChainU3Eg__callbackU7C10_0U3Ed_t8A469D321751D7D1D424788DF50E77EF55E39C19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7909[2] = 
{
	U3CU3CExecuteRequestChainU3Eg__callbackU7C10_0U3Ed_t8A469D321751D7D1D424788DF50E77EF55E39C19::get_offset_of_U3CU3E1__state_0(),
	U3CU3CExecuteRequestChainU3Eg__callbackU7C10_0U3Ed_t8A469D321751D7D1D424788DF50E77EF55E39C19::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7910 = { sizeof (ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7910[5] = 
{
	ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15::get_offset_of_Arrow_4(),
	ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15::get_offset_of_target_5(),
	ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15::get_offset_of_found_6(),
	ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15::get_offset_of_scriptManager_7(),
	ShowDirectionToMenu_t2529D91B48E100BC55129AAA64EA295E26968E15::get_offset_of_scriptMenuItems_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7911 = { sizeof (U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27), -1, sizeof(U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7911[2] = 
{
	U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3E5AEA54C95A02E4A6B0277E7ADD1775A03E1C27_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7912 = { sizeof (U3CU3Ec__DisplayClass7_0_tB2ECB44664EF864B42334177B7C4A4F19DF2D46C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7912[1] = 
{
	U3CU3Ec__DisplayClass7_0_tB2ECB44664EF864B42334177B7C4A4F19DF2D46C::get_offset_of_planes_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7913 = { sizeof (ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7913[21] = 
{
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_ProjectileBullet_6(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_ProjectileFireball_7(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_ProjectileLightningBolt_8(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_EffectExplosion_9(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_EffectFlare_10(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_EffectSmoke_11(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_DebugFastPlayer_12(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_HealthBar_13(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_Zombie_14(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_Drone_15(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_MaterialShield_16(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_behaviourSpaceInvadersAi_17(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_behaviourSpaceInvadersEdge_18(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_behaviourSpaceInvadersInvasion_19(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_behaviourSpaceInvadersEvents_20(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_effectsByName_21(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_effectSynonyms_22(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_materialsByName_23(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_materialSynonyms_24(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_scriptsByName_25(),
	ContentPrefabs_tB3F5A5002ED26D8E2F9525C7A4903895047B72B0::get_offset_of_scriptSynonyms_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7914 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7914[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7915 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7915[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7916 = { sizeof (MobileScriptMenu_t20F9D9B5584E0A9624457D7BEAE94BEDE27173D4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7917 = { sizeof (U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C), -1, sizeof(U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7917[2] = 
{
	U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tA834516095E63DA9ADBFCB1713FDCE0599B0982C_StaticFields::get_offset_of_U3CU3E9__3_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7918 = { sizeof (iOSScriptMenuItem_t03701E3CF7DC42E3E0BA8361542ADC00AEB465E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7918[1] = 
{
	iOSScriptMenuItem_t03701E3CF7DC42E3E0BA8361542ADC00AEB465E6::get_offset_of_script_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7919 = { sizeof (iOSInput_t85E39945A4B6B18A94599EB4BF776BE7FBA4AED0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7919[2] = 
{
	iOSInput_t85E39945A4B6B18A94599EB4BF776BE7FBA4AED0::get_offset_of_listeners_4(),
	iOSInput_t85E39945A4B6B18A94599EB4BF776BE7FBA4AED0::get_offset_of_dragging_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7920 = { sizeof (FocusManagerWrapper_t92F274FA35DE2DC44DDE6837069964C235147EB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7921 = { sizeof (GazeManagerWrapper_tF805EC3993C73F17F9915BAA20CCD76210101FC8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7922 = { sizeof (InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28), -1, sizeof(InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7922[17] = 
{
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_toggleKey_4(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_shakeToOpen_5(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_shakeAcceleration_6(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_restrictLogCount_7(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_maxLogs_8(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_logs_9(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_scrollPosition_10(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_visible_11(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_collapse_12(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_logTypeFilters_13(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields::get_offset_of_logTypeColors_14(),
	0,
	0,
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields::get_offset_of_clearLabel_17(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28_StaticFields::get_offset_of_collapseLabel_18(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_titleBarRect_19(),
	InGameConsole_t030E829913D6809AD92C108AA1F6AAEC7DED1C28::get_offset_of_windowRect_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7923 = { sizeof (Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788)+ sizeof (RuntimeObject), sizeof(Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable7923[3] = 
{
	Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788::get_offset_of_message_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788::get_offset_of_stackTrace_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Log_tD2E7630AA73F4471F582812E4BC4DFAB6BA79788::get_offset_of_type_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7924 = { sizeof (ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7924[1] = 
{
	ScriptCanvas_t3B28CA4BA9CD66AD0778254CDB3704D0AABBB634::get_offset_of_U3CScreenSpaceCanvasU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7925 = { sizeof (ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7925[6] = 
{
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E::get_offset_of_thumbnail_4(),
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E::get_offset_of_title_5(),
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E::get_offset_of_author_6(),
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E::get_offset_of_description_7(),
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E::get_offset_of_script_8(),
	ScriptTile_t03518EB9580168E192A9D9615ED20099809D1E7E::get_offset_of_isLocalScript_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7926 = { sizeof (TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7926[8] = 
{
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_tabs_4(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_tabObjectsToCheck_5(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_clickedButtonIndex_6(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_Swipe_7(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_swiping_8(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_eventSent_9(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_lastPosition_10(),
	TabSwipeManager_t5E37889E3F7397C3D7B30B467EBC775EA74BFF82::get_offset_of_touchDownPosition_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7927 = { sizeof (SwipeDirection_tD34EFC5A16E92E67EA1CD5BC23329CB864DED19E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7927[5] = 
{
	SwipeDirection_tD34EFC5A16E92E67EA1CD5BC23329CB864DED19E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7928 = { sizeof (ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7928[8] = 
{
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of_menu_0(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of__key_1(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of__text_2(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of__color_3(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of__icon_4(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of_rawAction_5(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of__action_6(),
	ScriptMenuItem_t09C9AA1C566D046542CC5EF6008079EF4BBF2488::get_offset_of_iconTexture_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7929 = { sizeof (U3CU3Ec__DisplayClass13_0_t2FF13F947CE8E8A46D781DFE294CC98E86396C27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7929[1] = 
{
	U3CU3Ec__DisplayClass13_0_t2FF13F947CE8E8A46D781DFE294CC98E86396C27::get_offset_of_callback_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7930 = { sizeof (ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3), -1, sizeof(ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7930[2] = 
{
	ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3_StaticFields::get_offset_of_helperFunctions_0(),
	ScriptConstants_t4AB349FC4A71ACE45E61639B88BC82698A3778F3_StaticFields::get_offset_of_arrayPrototypes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7931 = { sizeof (OverloadMatcher_tF1420F17B201A68AD6A75DE2043AE9D85282C7E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7931[1] = 
{
	OverloadMatcher_tF1420F17B201A68AD6A75DE2043AE9D85282C7E0::get_offset_of_jsParameters_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7932 = { sizeof (ScriptTemplate_t00A546D156E110E6A75DFA65041BBC57FCB1FCAE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7933 = { sizeof (JavascriptApiHandler_tBC6372A11FD451DB7FACA66B68D5AA37E3B294E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7934 = { sizeof (U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C), -1, sizeof(U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7934[3] = 
{
	U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields::get_offset_of_U3CU3E9__2_0_1(),
	U3CU3Ec_tCA585659C7C87B3CD969AD0899FB181F8F1E854C_StaticFields::get_offset_of_U3CU3E9__2_1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7935 = { sizeof (ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7935[1] = 
{
	ScriptEventHandler_t6818D0F175ECC12B599C5FE2A33346CECCD49AC7::get_offset_of_messageSubscribers_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7936 = { sizeof (U3CU3Ec__DisplayClass5_0_tA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7936[2] = 
{
	U3CU3Ec__DisplayClass5_0_tA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB::get_offset_of_obj_0(),
	U3CU3Ec__DisplayClass5_0_tA5BA7E451FBBC4194126821C0B2A6EBDBE4954EB::get_offset_of_collisionEvent_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7937 = { sizeof (ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7937[8] = 
{
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3COnGameObjectClickEventsU3Ek__BackingField_0(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3COnGameObjectClickArgsU3Ek__BackingField_1(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3COnGameObjectReleaseEventsU3Ek__BackingField_2(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3COnGameObjectReleaseArgsU3Ek__BackingField_3(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3COnGameObjectClickJsEventsU3Ek__BackingField_4(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3COnGameObjectReleaseJsEventsU3Ek__BackingField_5(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_U3CClickCallbacksU3Ek__BackingField_6(),
	ScriptInputHandler_tC70863BE85E492598590C8D3D441A7C5FA096EDC::get_offset_of_isInputEnabled_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7938 = { sizeof (U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7938[3] = 
{
	U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D::get_offset_of_hit_1(),
	U3CU3Ec__DisplayClass35_0_t22F36F94EB17F09176DD3351C1275CFFBD7F581D::get_offset_of_clickedObjectEvent_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7939 = { sizeof (U3CU3Ec__DisplayClass35_1_tC2250CC5A8835495D99DDCDB485DC2EE624E8E6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7939[2] = 
{
	U3CU3Ec__DisplayClass35_1_tC2250CC5A8835495D99DDCDB485DC2EE624E8E6F::get_offset_of_functionName_0(),
	U3CU3Ec__DisplayClass35_1_tC2250CC5A8835495D99DDCDB485DC2EE624E8E6F::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7940 = { sizeof (U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7940[3] = 
{
	U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F::get_offset_of_hit_0(),
	U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass36_0_tF1514C54402A97B5AC84300244FE50217A9AA70F::get_offset_of_U3CU3E9__1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7941 = { sizeof (U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7941[3] = 
{
	U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA::get_offset_of_hit_0(),
	U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass37_0_t16B6B87AB596BDA5F8ED57EC5F05D7194C5FE0CA::get_offset_of_U3CU3E9__1_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7942 = { sizeof (U3CU3Ec__DisplayClass39_0_tAB02B270DED9ACA9B3504E120DD4A68046EA2118), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7942[1] = 
{
	U3CU3Ec__DisplayClass39_0_tAB02B270DED9ACA9B3504E120DD4A68046EA2118::get_offset_of_hit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7943 = { sizeof (ScriptUtility_tB6B19DD8C0F93DA2C853312A654BEAB1194627EF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7944 = { sizeof (U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100), -1, sizeof(U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7944[2] = 
{
	U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t0A4BE3660FDBFF4413B12BBFCFA3012CC21A5100_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7945 = { sizeof (Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7945[6] = 
{
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10::get_offset_of_androidFeatures_4(),
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10::get_offset_of_iOSFeatures_5(),
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10::get_offset_of_holoLensFeatures_6(),
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10::get_offset_of_mixedRealityFeatures_7(),
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10::get_offset_of_standaloneFeatures_8(),
	Features_t453CBFF75AFDC525F02DB5DD8B6BBB55273EED10::get_offset_of_defaultFeatures_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7946 = { sizeof (EmptyProjectLoader_t3A80B5D6896126CAF14DFBF7E63CA6B741EB94E0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7947 = { sizeof (QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7947[3] = 
{
	QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8::get_offset_of_BarcodeScanner_4(),
	QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8::get_offset_of_RestartTime_5(),
	QRCodeProjectLoader_t18953784222E70AB5CBA0919B6ACF25D587A88D8::get_offset_of_U3CProjectUrlCallbackU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7948 = { sizeof (U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA), -1, sizeof(U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7948[3] = 
{
	U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields::get_offset_of_U3CU3E9__8_1_1(),
	U3CU3Ec_t6A3CA5EF339718244604EE794ED744E7EF9D32EA_StaticFields::get_offset_of_U3CU3E9__12_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7949 = { sizeof (U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7949[4] = 
{
	U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423::get_offset_of_U3CU3E1__state_0(),
	U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423::get_offset_of_U3CU3E2__current_1(),
	U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423::get_offset_of_U3CU3E4__this_2(),
	U3CStopCameraU3Ed__13_tE309B618F67AB4403EE48BC2B0A7D5A38BF0E423::get_offset_of_callback_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7950 = { sizeof (ScriptErrorHandler_t86C54D9B0DA7092755777EFF56D5D23039FEA924), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7951 = { sizeof (U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E), -1, sizeof(U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7951[2] = 
{
	U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t3341450C5E2F459D7BEC343BC8424C680BB5850E_StaticFields::get_offset_of_U3CU3E9__0_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7952 = { sizeof (BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197), -1, sizeof(BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7952[2] = 
{
	BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197_StaticFields::get_offset_of_behaviours_0(),
	BehaviourRegistry_t72AD109D3C135D3C6105EE8F5CB9D983CD522197_StaticFields::get_offset_of_behaviourInstances_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7953 = { sizeof (MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79), -1, sizeof(MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7953[3] = 
{
	MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79::get_offset_of_U3CJsObjectU3Ek__BackingField_0(),
	MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79_StaticFields::get_offset_of_engine_1(),
	MxBehaviour_t3E68364A53773E8F9171FB9E596724746F3B7B79::get_offset_of_U3CuniqueIdU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7954 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7955 = { sizeof (HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7955[4] = 
{
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A::get_offset_of_U3CHitU3Ek__BackingField_0(),
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A::get_offset_of_U3COriginU3Ek__BackingField_1(),
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A::get_offset_of_U3CDirectionU3Ek__BackingField_2(),
	HitInfo_tF328EFA7278C1055AD8E27406D8BF952B00FF96A::get_offset_of_U3CDidHitU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7956 = { sizeof (GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7956[3] = 
{
	GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E::get_offset_of_scriptManager_6(),
	GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E::get_offset_of_runDefaultScript_7(),
	GameSceneInit_t8A16F4477CBFBAF4C63295EE7742644E9F35869E::get_offset_of_defaultScript_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7957 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7958 = { sizeof (MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7958[3] = 
{
	MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3::get_offset_of_detectionController_4(),
	MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3::get_offset_of_referencePointManager_5(),
	MobilePlaneDetector_tEDF662370AD6EDA2D748F13123CD36CB65125CE3::get_offset_of_planesById_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7959 = { sizeof (U3CU3Ec__DisplayClass3_0_t93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7959[2] = 
{
	U3CU3Ec__DisplayClass3_0_t93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427::get_offset_of_arPlane_0(),
	U3CU3Ec__DisplayClass3_0_t93CABF7E2F9DC6B4F3F5D6607B7BF879C1EC1427::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7960 = { sizeof (U3CU3Ec__DisplayClass4_0_tCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7960[1] = 
{
	U3CU3Ec__DisplayClass4_0_tCC5D883F2AA54FCD1C9196F42B05FCFC79339ACC::get_offset_of_arPlane_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7961 = { sizeof (U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71), -1, sizeof(U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7961[2] = 
{
	U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t181511CCAE25217D5CCEF5432307759A0D2FEA71_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7962 = { sizeof (PlatformDependentPlaneDetector_tA059BB399BF4739A6CA16E50E3B2581C47FB6C1F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7963 = { sizeof (MobileInput_tDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7963[2] = 
{
	MobileInput_tDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0::get_offset_of_listeners_4(),
	MobileInput_tDDBD20C1B7B59E8F13695C1D5542D578BC66B7C0::get_offset_of_dragging_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7964 = { sizeof (ModerationStatus_t792C26BC188A4F75004604C2E67FFDBE186F1158)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7964[4] = 
{
	ModerationStatus_t792C26BC188A4F75004604C2E67FFDBE186F1158::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7965 = { sizeof (ScriptLineInfoData_t8EA9BD0DF47D633F8A78D484C2199A8A396B9B30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7965[2] = 
{
	ScriptLineInfoData_t8EA9BD0DF47D633F8A78D484C2199A8A396B9B30::get_offset_of_start_0(),
	ScriptLineInfoData_t8EA9BD0DF47D633F8A78D484C2199A8A396B9B30::get_offset_of_path_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7966 = { sizeof (ScriptLineInfoDataRoot_t7F1F035EB5F1A4891703C740A914D25C27F17825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7966[1] = 
{
	ScriptLineInfoDataRoot_t7F1F035EB5F1A4891703C740A914D25C27F17825::get_offset_of_scripts_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7967 = { sizeof (SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7967[9] = 
{
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_SphereRadius_4(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_MoveSpeed_5(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_useUnscaledTime_6(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_hideOnStart_7(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_debugDisplaySphere_8(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_debugDisplayTargetPosition_9(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_targetPosition_10(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_optimalPosition_11(),
	SphereBasedTagalong_t0BE3D4B908181014BDD1CE4C054B7B5DEB2310ED::get_offset_of_initialDistanceToCamera_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7968 = { sizeof (CalibrationSpace_tCA0BD13D13077D99ECC4CB1148F303B1386278E2), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7969 = { sizeof (HololensDependencies_t760AD08728DDB4C9DD391FF8068098E16CA358F1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7970 = { sizeof (MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7970[4] = 
{
	MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190::get_offset_of_U3CScriptU3Ek__BackingField_4(),
	MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190::get_offset_of_U3CRunButtonU3Ek__BackingField_5(),
	MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190::get_offset_of_U3CNameTextFieldU3Ek__BackingField_6(),
	MobileScriptMenuItem_t3C50F345345CB346D993D63DC68C1083C9E5E190::get_offset_of_U3CAuthorTextFieldU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7971 = { sizeof (StringProcessing_t8FE20CC531054C78698F86E8FED36BE57FE00EB1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7972 = { sizeof (U3CU3Ec__DisplayClass0_0_t7FC4D0FB48949358288F60EE613498DC5F38A585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7972[1] = 
{
	U3CU3Ec__DisplayClass0_0_t7FC4D0FB48949358288F60EE613498DC5F38A585::get_offset_of_text_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7973 = { sizeof (iOSSaveLoadManager_t7EDB06D6FE87AC270BF12C3086950199501D8E2D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7974 = { sizeof (U3CLoadCachedScriptThumbnailsU3Ed__7_t2DFCC15F515726A92AA41F92C59BCDEEB2163811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7974[2] = 
{
	U3CLoadCachedScriptThumbnailsU3Ed__7_t2DFCC15F515726A92AA41F92C59BCDEEB2163811::get_offset_of_U3CU3E1__state_0(),
	U3CLoadCachedScriptThumbnailsU3Ed__7_t2DFCC15F515726A92AA41F92C59BCDEEB2163811::get_offset_of_U3CU3E2__current_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7975 = { sizeof (iOSAssetLoader_tCD84F3E98F93FCD6D2654F10FBC4D10CE9EB6ED0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7976 = { sizeof (iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7976[9] = 
{
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_canvasFactory_4(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_scriptMenuItemFactory_5(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_inGameConsoleFactory_6(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_scriptManager_7(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_menuCanvas_8(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_planeGeneratorFactory_9(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_pointCloudParticlesFactory_10(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_scripts_11(),
	iOSScriptMenu_t82A6900C31A748C42851FF56BA55381C30AE1D78::get_offset_of_initializedConsole_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7977 = { sizeof (MPARHololensInputNamespace_tEF06A42F49C571240196FE25A9CAEBE6EC3B24D3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7978 = { sizeof (DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7978[10] = 
{
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CImageMenuFactoryU3Ek__BackingField_4(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CTextMenuFactoryU3Ek__BackingField_5(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CObjectCollectionU3Ek__BackingField_6(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CMenuItemsU3Ek__BackingField_7(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CBlankMenuItemsU3Ek__BackingField_8(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CColumnsU3Ek__BackingField_9(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_U3CTitleU3Ek__BackingField_10(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_TopBar_11(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_BottomBar_12(),
	DesktopInScriptMenu_tD2EFCDB4E715E7882700D8AA0DE43557CB018F8E::get_offset_of_Background_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7979 = { sizeof (U3CU3Ec__DisplayClass38_0_tFA2354BC3A84EE97CBC265A441FC12F60B40A37F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7979[1] = 
{
	U3CU3Ec__DisplayClass38_0_tFA2354BC3A84EE97CBC265A441FC12F60B40A37F::get_offset_of_hit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7980 = { sizeof (DesktopInScriptMenuItem_tE2315C52652D59C2ADC1FB7F6236EFAF876A0996), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7980[1] = 
{
	DesktopInScriptMenuItem_tE2315C52652D59C2ADC1FB7F6236EFAF876A0996::get_offset_of_U3CCallbackU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7981 = { sizeof (DesktopMenu_tDFEDD80EFF32E3F27F71E4927F9E8A01336813C3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7982 = { sizeof (DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7982[9] = 
{
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_U3CScriptsU3Ek__BackingField_4(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_U3CObjectCollectionU3Ek__BackingField_5(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_U3CScriptMenuItemFactoryU3Ek__BackingField_6(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_U3CGesturesU3Ek__BackingField_7(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_U3CSaveLoadManagerU3Ek__BackingField_8(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_holdClickTime_9(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_clickDownTime_10(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_clickObject_11(),
	DesktopScriptMenu_t08E6D0D858EB826624E6CD4963C25EB3583011B0::get_offset_of_didHoldClick_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7983 = { sizeof (U3CU3Ec__DisplayClass28_0_t155264DCD7C45052F90FBF57790FEFBCDEC227B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7983[1] = 
{
	U3CU3Ec__DisplayClass28_0_t155264DCD7C45052F90FBF57790FEFBCDEC227B0::get_offset_of_script_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7984 = { sizeof (U3CU3Ec__DisplayClass31_0_t8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7984[2] = 
{
	U3CU3Ec__DisplayClass31_0_t8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C::get_offset_of_script_0(),
	U3CU3Ec__DisplayClass31_0_t8E6F0B9983F61DABB1FD6758F01AC1E877B0E37C::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7985 = { sizeof (U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699), -1, sizeof(U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7985[2] = 
{
	U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t8B44A0EC47FC3FA7EE88105EB76B64A8CA816699_StaticFields::get_offset_of_U3CU3E9__31_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7986 = { sizeof (U3CU3Ec__DisplayClass36_0_tAFB515B86BCCC111A68F37A2130B25D3E40226E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7986[1] = 
{
	U3CU3Ec__DisplayClass36_0_tAFB515B86BCCC111A68F37A2130B25D3E40226E0::get_offset_of_hit_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7987 = { sizeof (DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7987[9] = 
{
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_ThumbnailObject_4(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_TopPanel_5(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_BottomPanel_6(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_Display_7(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_DisplayObjects_8(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_RunButton_9(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_BlockedOverlay_10(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_U3CIsSelectedU3Ek__BackingField_11(),
	DesktopScriptMenuItem_t2CED641364E3DEC56958ED2D91E24FDEE99B6FF9::get_offset_of_script_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7988 = { sizeof (U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7988[2] = 
{
	U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512::get_offset_of_doneTop_0(),
	U3CU3Ec__DisplayClass15_0_t18FFC7E93357D278C29B6D1880247147A8BA0512::get_offset_of_doneBot_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7989 = { sizeof (U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7989[5] = 
{
	U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B::get_offset_of_U3CU3E1__state_0(),
	U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B::get_offset_of_U3CU3E2__current_1(),
	U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B::get_offset_of_U3CU3E4__this_2(),
	U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B::get_offset_of_U3CU3E8__1_3(),
	U3CMinimizeU3Ed__15_t807F878E8B294E30B7F7968959FB273B5948E42B::get_offset_of_callback_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7990 = { sizeof (DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7990[8] = 
{
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_listeners_4(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_U3CMouseDownPositionU3Ek__BackingField_5(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_mouseDown_6(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_dragging_7(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_lastDelta_8(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_lastMovementDelta_9(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of__cursor_10(),
	DesktopInput_t49BB33778B1D74FF7013720E2408808FE3D7BF48::get_offset_of_inputMode_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7991 = { sizeof (DesktopInputMode_t57A0297EC964512EA2A48822CFDD58120B29EB39)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7991[3] = 
{
	DesktopInputMode_t57A0297EC964512EA2A48822CFDD58120B29EB39::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7992 = { sizeof (U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7992[3] = 
{
	U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B::get_offset_of_U3CU3E1__state_0(),
	U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B::get_offset_of_U3CU3E2__current_1(),
	U3CSetCameraControlsWhenReadyU3Ed__15_t3099093EC54A41C74DD607245BA3D9EA1F66221B::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7993 = { sizeof (FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7993[3] = 
{
	FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96::get_offset_of_forwardSpeed_4(),
	FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96::get_offset_of_sideSpeed_5(),
	FirstPersonControls_t9AF2FCDFA9310818E8CBAB77D20BACFF39128A96::get_offset_of_walking_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7994 = { sizeof (SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7994[15] = 
{
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_axes_4(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_sensitivityX_5(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_sensitivityY_6(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_minimumX_7(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_maximumX_8(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_minimumY_9(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_maximumY_10(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_rotationX_11(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_rotationY_12(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_rotArrayX_13(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_rotAverageX_14(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_rotArrayY_15(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_rotAverageY_16(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_frameCounter_17(),
	SmoothMouseLook_t02D0AFDBEE9391D8EFD43CC04D38419CF6521A51::get_offset_of_originalRotation_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7995 = { sizeof (RotationAxes_t26EFFD53B50428B38246BFBAC1D2F52E90286B14)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7995[4] = 
{
	RotationAxes_t26EFFD53B50428B38246BFBAC1D2F52E90286B14::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7996 = { sizeof (DesktopAssetLoader_tB37AB66B5E615BDE30906AF9D7C5878131B12EE3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7997 = { sizeof (BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7997[10] = 
{
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_BasePath_4(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_ScriptsSuffix_5(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_CacheSuffix_6(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_SaveDataSiffux_7(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_U3CScriptIdU3Ek__BackingField_8(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_U3CAssetMappingU3Ek__BackingField_9(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_U3CAssetLoaderU3Ek__BackingField_10(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_U3CExhibitBoxFactoryU3Ek__BackingField_11(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_Gestures_12(),
	BaseSaveLoadManager_tB73A6EC5834C76C54E79BF0F10241ADB883530EB::get_offset_of_U3CPrimitivesU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7998 = { sizeof (U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053), -1, sizeof(U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7998[10] = 
{
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__30_0_1(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__35_0_2(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__38_0_3(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__38_2_4(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__38_3_5(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__40_0_6(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__41_0_7(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__41_1_8(),
	U3CU3Ec_t2172284CCA3E7547C1E00CDFD4A05E64EDC6E053_StaticFields::get_offset_of_U3CU3E9__41_2_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7999 = { sizeof (U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7999[2] = 
{
	U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520::get_offset_of_anchor_0(),
	U3CU3Ec__DisplayClass31_0_t565FB00CF4B8399BD98C1A25F7207E1A49E80520::get_offset_of_U3CU3E4__this_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
