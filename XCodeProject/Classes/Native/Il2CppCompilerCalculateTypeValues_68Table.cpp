﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Action`1<Zenject.DiContainer>
struct Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<System.Type>
struct List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7;
// System.Func`2<System.Type,System.String>
struct Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5;
// System.Func`2<System.Type,System.Type>
struct Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF;
// System.Func`2<Zenject.DiContainer,Zenject.IProvider>
struct Func_2_t5FD390473C0145A677D44B1CE32C9BB15F27B976;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191;
// System.Func`2<Zenject.InjectContext,UnityEngine.Transform>
struct Func_2_t4ED720590592A8375BE8E36276393417F557F933;
// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider>
struct Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9;
// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider>
struct Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// Zenject.BindFinalizerWrapper
struct BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6;
// Zenject.BindInfo
struct BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1;
// Zenject.BindingCondition
struct BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;
// Zenject.FromBinder
struct FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4;
// Zenject.IBindingFinalizer
struct IBindingFinalizer_t7B9AC91046F5BF63129A2DF921E8264BBF0D6504;
// Zenject.ISubContainerCreator
struct ISubContainerCreator_tF9496E42638FC023B3C3A79AFF45CFF044BBD281;
// Zenject.PrefabBindingFinalizer
struct PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661;
// Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3;
// Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7;
// Zenject.PrefabInstantiatorCached
struct PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943;
// Zenject.PrefabResourceBindingFinalizer
struct PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B;
// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F;
// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05;
// Zenject.ScopableBindingFinalizer
struct ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA;
// Zenject.SubContainerCreatorCached
struct SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79;
// Zenject.SubContainerInstallerBindingFinalizer
struct SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439;
// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1;
// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E;
// Zenject.SubContainerMethodBindingFinalizer
struct SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842;
// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_0
struct U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F;
// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E;
// Zenject.SubContainerPrefabBindingFinalizer
struct SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C;
// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16;
// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB;
// Zenject.SubContainerPrefabResourceBindingFinalizer
struct SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F;
// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_0
struct U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5;
// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_0
struct U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BINDFINALIZERWRAPPER_TB36EA4FC0554CF87C12522841E589B5776DE6CA6_H
#define BINDFINALIZERWRAPPER_TB36EA4FC0554CF87C12522841E589B5776DE6CA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindFinalizerWrapper
struct  BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6  : public RuntimeObject
{
public:
	// Zenject.IBindingFinalizer Zenject.BindFinalizerWrapper::_subFinalizer
	RuntimeObject* ____subFinalizer_0;

public:
	inline static int32_t get_offset_of__subFinalizer_0() { return static_cast<int32_t>(offsetof(BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6, ____subFinalizer_0)); }
	inline RuntimeObject* get__subFinalizer_0() const { return ____subFinalizer_0; }
	inline RuntimeObject** get_address_of__subFinalizer_0() { return &____subFinalizer_0; }
	inline void set__subFinalizer_0(RuntimeObject* value)
	{
		____subFinalizer_0 = value;
		Il2CppCodeGenWriteBarrier((&____subFinalizer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDFINALIZERWRAPPER_TB36EA4FC0554CF87C12522841E589B5776DE6CA6_H
#ifndef BINDINGUTIL_T8A14770ED161D915CEFD9A5AC6006B8BC90A1FA0_H
#define BINDINGUTIL_T8A14770ED161D915CEFD9A5AC6006B8BC90A1FA0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindingUtil
struct  BindingUtil_t8A14770ED161D915CEFD9A5AC6006B8BC90A1FA0  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGUTIL_T8A14770ED161D915CEFD9A5AC6006B8BC90A1FA0_H
#ifndef FACTORYBINDINFO_T7D4905F5254D3D3734F1659902724612D2D0FA5C_H
#define FACTORYBINDINFO_T7D4905F5254D3D3734F1659902724612D2D0FA5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FactoryBindInfo
struct  FactoryBindInfo_t7D4905F5254D3D3734F1659902724612D2D0FA5C  : public RuntimeObject
{
public:
	// System.Type Zenject.FactoryBindInfo::<FactoryType>k__BackingField
	Type_t * ___U3CFactoryTypeU3Ek__BackingField_0;
	// System.Func`2<Zenject.DiContainer,Zenject.IProvider> Zenject.FactoryBindInfo::<ProviderFunc>k__BackingField
	Func_2_t5FD390473C0145A677D44B1CE32C9BB15F27B976 * ___U3CProviderFuncU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFactoryTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FactoryBindInfo_t7D4905F5254D3D3734F1659902724612D2D0FA5C, ___U3CFactoryTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CFactoryTypeU3Ek__BackingField_0() const { return ___U3CFactoryTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CFactoryTypeU3Ek__BackingField_0() { return &___U3CFactoryTypeU3Ek__BackingField_0; }
	inline void set_U3CFactoryTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CFactoryTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFactoryTypeU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProviderFuncU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FactoryBindInfo_t7D4905F5254D3D3734F1659902724612D2D0FA5C, ___U3CProviderFuncU3Ek__BackingField_1)); }
	inline Func_2_t5FD390473C0145A677D44B1CE32C9BB15F27B976 * get_U3CProviderFuncU3Ek__BackingField_1() const { return ___U3CProviderFuncU3Ek__BackingField_1; }
	inline Func_2_t5FD390473C0145A677D44B1CE32C9BB15F27B976 ** get_address_of_U3CProviderFuncU3Ek__BackingField_1() { return &___U3CProviderFuncU3Ek__BackingField_1; }
	inline void set_U3CProviderFuncU3Ek__BackingField_1(Func_2_t5FD390473C0145A677D44B1CE32C9BB15F27B976 * value)
	{
		___U3CProviderFuncU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProviderFuncU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACTORYBINDINFO_T7D4905F5254D3D3734F1659902724612D2D0FA5C_H
#ifndef U3CU3EC__DISPLAYCLASS32_0_T8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F_H
#define U3CU3EC__DISPLAYCLASS32_0_T8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass32_0
struct  U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F  : public RuntimeObject
{
public:
	// System.String Zenject.FromBinder/<>c__DisplayClass32_0::resourcePath
	String_t* ___resourcePath_0;
	// Zenject.FromBinder Zenject.FromBinder/<>c__DisplayClass32_0::<>4__this
	FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * ___U3CU3E4__this_1;
	// System.Boolean Zenject.FromBinder/<>c__DisplayClass32_0::createNew
	bool ___createNew_2;

public:
	inline static int32_t get_offset_of_resourcePath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F, ___resourcePath_0)); }
	inline String_t* get_resourcePath_0() const { return ___resourcePath_0; }
	inline String_t** get_address_of_resourcePath_0() { return &___resourcePath_0; }
	inline void set_resourcePath_0(String_t* value)
	{
		___resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourcePath_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F, ___U3CU3E4__this_1)); }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}

	inline static int32_t get_offset_of_createNew_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F, ___createNew_2)); }
	inline bool get_createNew_2() const { return ___createNew_2; }
	inline bool* get_address_of_createNew_2() { return &___createNew_2; }
	inline void set_createNew_2(bool value)
	{
		___createNew_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS32_0_T8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F_H
#ifndef U3CU3EC__DISPLAYCLASS33_0_T042097CADA14CFF4319E93FD31AE55A924A1321C_H
#define U3CU3EC__DISPLAYCLASS33_0_T042097CADA14CFF4319E93FD31AE55A924A1321C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass33_0
struct  U3CU3Ec__DisplayClass33_0_t042097CADA14CFF4319E93FD31AE55A924A1321C  : public RuntimeObject
{
public:
	// System.String Zenject.FromBinder/<>c__DisplayClass33_0::resourcePath
	String_t* ___resourcePath_0;

public:
	inline static int32_t get_offset_of_resourcePath_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass33_0_t042097CADA14CFF4319E93FD31AE55A924A1321C, ___resourcePath_0)); }
	inline String_t* get_resourcePath_0() const { return ___resourcePath_0; }
	inline String_t** get_address_of_resourcePath_0() { return &___resourcePath_0; }
	inline void set_resourcePath_0(String_t* value)
	{
		___resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&___resourcePath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS33_0_T042097CADA14CFF4319E93FD31AE55A924A1321C_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_TDA1B7F70C3D36ED82ABB9E21594067B97B4ED917_H
#define U3CU3EC__DISPLAYCLASS34_0_TDA1B7F70C3D36ED82ABB9E21594067B97B4ED917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_tDA1B7F70C3D36ED82ABB9E21594067B97B4ED917  : public RuntimeObject
{
public:
	// System.Func`2<Zenject.InjectContext,System.Object> Zenject.FromBinder/<>c__DisplayClass34_0::method
	Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 * ___method_0;

public:
	inline static int32_t get_offset_of_method_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_tDA1B7F70C3D36ED82ABB9E21594067B97B4ED917, ___method_0)); }
	inline Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 * get_method_0() const { return ___method_0; }
	inline Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 ** get_address_of_method_0() { return &___method_0; }
	inline void set_method_0(Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 * value)
	{
		___method_0 = value;
		Il2CppCodeGenWriteBarrier((&___method_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_TDA1B7F70C3D36ED82ABB9E21594067B97B4ED917_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_TF2AE282E4D6A547E2F5A6F93934DCE398C6928F3_H
#define U3CU3EC__DISPLAYCLASS39_0_TF2AE282E4D6A547E2F5A6F93934DCE398C6928F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_tF2AE282E4D6A547E2F5A6F93934DCE398C6928F3  : public RuntimeObject
{
public:
	// System.Object Zenject.FromBinder/<>c__DisplayClass39_0::instance
	RuntimeObject * ___instance_0;

public:
	inline static int32_t get_offset_of_instance_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_tF2AE282E4D6A547E2F5A6F93934DCE398C6928F3, ___instance_0)); }
	inline RuntimeObject * get_instance_0() const { return ___instance_0; }
	inline RuntimeObject ** get_address_of_instance_0() { return &___instance_0; }
	inline void set_instance_0(RuntimeObject * value)
	{
		___instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_TF2AE282E4D6A547E2F5A6F93934DCE398C6928F3_H
#ifndef GAMEOBJECTFACTORY_T69C0DC65BD450BDDD99982D281023992E254FDC1_H
#define GAMEOBJECTFACTORY_T69C0DC65BD450BDDD99982D281023992E254FDC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectFactory
struct  GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.GameObjectFactory::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// UnityEngine.Object Zenject.GameObjectFactory::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__prefab_1() { return static_cast<int32_t>(offsetof(GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1, ____prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_1() const { return ____prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_1() { return &____prefab_1; }
	inline void set__prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTFACTORY_T69C0DC65BD450BDDD99982D281023992E254FDC1_H
#ifndef IDBINDER_T368E1D65DA4ADC684F688A9E721B553C0B1AD0F4_H
#define IDBINDER_T368E1D65DA4ADC684F688A9E721B553C0B1AD0F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IdBinder
struct  IdBinder_t368E1D65DA4ADC684F688A9E721B553C0B1AD0F4  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.IdBinder::_bindInfo
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ____bindInfo_0;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(IdBinder_t368E1D65DA4ADC684F688A9E721B553C0B1AD0F4, ____bindInfo_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDBINDER_T368E1D65DA4ADC684F688A9E721B553C0B1AD0F4_H
#ifndef NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#define NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NonLazyBinder
struct  NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.NonLazyBinder::<BindInfo>k__BackingField
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONLAZYBINDER_TD353CE02C951C95FD045F6C21516882D489A0B8E_H
#ifndef NULLBINDINGFINALIZER_TF7E45F0EBB0FB335628DD396747FF11A1D21A863_H
#define NULLBINDINGFINALIZER_TF7E45F0EBB0FB335628DD396747FF11A1D21A863_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NullBindingFinalizer
struct  NullBindingFinalizer_tF7E45F0EBB0FB335628DD396747FF11A1D21A863  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLBINDINGFINALIZER_TF7E45F0EBB0FB335628DD396747FF11A1D21A863_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T9E7836AA27E3F40717DC2CC7451DD6445939CAF3_H
#define U3CU3EC__DISPLAYCLASS5_0_T9E7836AA27E3F40717DC2CC7451DD6445939CAF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.PrefabBindingFinalizer Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3, ___U3CU3E4__this_1)); }
	inline PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T9E7836AA27E3F40717DC2CC7451DD6445939CAF3_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_TC874328E94584FE63035E24794D0E00246DDA60F_H
#define U3CU3EC__DISPLAYCLASS5_1_TC874328E94584FE63035E24794D0E00246DDA60F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_tC874328E94584FE63035E24794D0E00246DDA60F  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_1::prefabCreator
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * ___prefabCreator_0;
	// Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_0 Zenject.PrefabBindingFinalizer/<>c__DisplayClass5_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tC874328E94584FE63035E24794D0E00246DDA60F, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tC874328E94584FE63035E24794D0E00246DDA60F, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_TC874328E94584FE63035E24794D0E00246DDA60F_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7_H
#define U3CU3EC__DISPLAYCLASS6_0_TFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.PrefabBindingFinalizer Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_0::<>4__this
	PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7, ___U3CU3E4__this_1)); }
	inline PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T89F17D7C00C10CDBC8FEDED3948235F9FF458991_H
#define U3CU3EC__DISPLAYCLASS6_1_T89F17D7C00C10CDBC8FEDED3948235F9FF458991_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t89F17D7C00C10CDBC8FEDED3948235F9FF458991  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_1::prefabCreator
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * ___prefabCreator_0;
	// Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_0 Zenject.PrefabBindingFinalizer/<>c__DisplayClass6_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t89F17D7C00C10CDBC8FEDED3948235F9FF458991, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t89F17D7C00C10CDBC8FEDED3948235F9FF458991, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T89F17D7C00C10CDBC8FEDED3948235F9FF458991_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T9722D3DED53E53706BE00C1A1F4D1C80033AD45F_H
#define U3CU3EC__DISPLAYCLASS5_0_T9722D3DED53E53706BE00C1A1F4D1C80033AD45F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.PrefabResourceBindingFinalizer Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F, ___U3CU3E4__this_1)); }
	inline PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T9722D3DED53E53706BE00C1A1F4D1C80033AD45F_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_TD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B_H
#define U3CU3EC__DISPLAYCLASS5_1_TD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_tD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_1::prefabCreator
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * ___prefabCreator_0;
	// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_0 Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass5_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_TD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T2812AD4C754CB66BCB46CB11B38B63E87F1A2B05_H
#define U3CU3EC__DISPLAYCLASS6_0_T2812AD4C754CB66BCB46CB11B38B63E87F1A2B05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.PrefabResourceBindingFinalizer Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_0::<>4__this
	PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05, ___U3CU3E4__this_1)); }
	inline PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T2812AD4C754CB66BCB46CB11B38B63E87F1A2B05_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T2914EA28BBAC7C2030C1A28B980385064460B93E_H
#define U3CU3EC__DISPLAYCLASS6_1_T2914EA28BBAC7C2030C1A28B980385064460B93E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t2914EA28BBAC7C2030C1A28B980385064460B93E  : public RuntimeObject
{
public:
	// Zenject.PrefabInstantiatorCached Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_1::prefabCreator
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * ___prefabCreator_0;
	// Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_0 Zenject.PrefabResourceBindingFinalizer/<>c__DisplayClass6_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_prefabCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t2914EA28BBAC7C2030C1A28B980385064460B93E, ___prefabCreator_0)); }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * get_prefabCreator_0() const { return ___prefabCreator_0; }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 ** get_address_of_prefabCreator_0() { return &___prefabCreator_0; }
	inline void set_prefabCreator_0(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * value)
	{
		___prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefabCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t2914EA28BBAC7C2030C1A28B980385064460B93E, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T2914EA28BBAC7C2030C1A28B980385064460B93E_H
#ifndef PROVIDERBINDINGFINALIZER_T1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD_H
#define PROVIDERBINDINGFINALIZER_T1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer
struct  ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.ProviderBindingFinalizer::<BindInfo>k__BackingField
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ___U3CBindInfoU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CBindInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD, ___U3CBindInfoU3Ek__BackingField_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get_U3CBindInfoU3Ek__BackingField_0() const { return ___U3CBindInfoU3Ek__BackingField_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of_U3CBindInfoU3Ek__BackingField_0() { return &___U3CBindInfoU3Ek__BackingField_0; }
	inline void set_U3CBindInfoU3Ek__BackingField_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		___U3CBindInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBindInfoU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERBINDINGFINALIZER_T1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD_H
#ifndef U3CU3EC_T64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_H
#define U3CU3EC_T64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer/<>c
struct  U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields
{
public:
	// Zenject.ProviderBindingFinalizer/<>c Zenject.ProviderBindingFinalizer/<>c::<>9
	U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC * ___U3CU3E9_0;
	// System.Func`2<System.Type,System.String> Zenject.ProviderBindingFinalizer/<>c::<>9__7_0
	Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 * ___U3CU3E9__7_0_1;
	// System.Func`2<System.Type,System.Type> Zenject.ProviderBindingFinalizer/<>c::<>9__16_0
	Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF * ___U3CU3E9__16_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__7_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields, ___U3CU3E9__7_0_1)); }
	inline Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 * get_U3CU3E9__7_0_1() const { return ___U3CU3E9__7_0_1; }
	inline Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 ** get_address_of_U3CU3E9__7_0_1() { return &___U3CU3E9__7_0_1; }
	inline void set_U3CU3E9__7_0_1(Func_2_t42E15778FCD2FCC12128FA2916CE06E4E70655E5 * value)
	{
		___U3CU3E9__7_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__7_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__16_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields, ___U3CU3E9__16_0_2)); }
	inline Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF * get_U3CU3E9__16_0_2() const { return ___U3CU3E9__16_0_2; }
	inline Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF ** get_address_of_U3CU3E9__16_0_2() { return &___U3CU3E9__16_0_2; }
	inline void set_U3CU3E9__16_0_2(Func_2_t9A30598E63DAE9805F56679563D875589D41D2CF * value)
	{
		___U3CU3E9__16_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__16_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_H
#ifndef U3CU3EC__DISPLAYCLASS16_0_T3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69_H
#define U3CU3EC__DISPLAYCLASS16_0_T3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderBindingFinalizer/<>c__DisplayClass16_0
struct  U3CU3Ec__DisplayClass16_0_t3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69  : public RuntimeObject
{
public:
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.ProviderBindingFinalizer/<>c__DisplayClass16_0::providerFunc
	Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * ___providerFunc_0;
	// Zenject.DiContainer Zenject.ProviderBindingFinalizer/<>c__DisplayClass16_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_1;

public:
	inline static int32_t get_offset_of_providerFunc_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69, ___providerFunc_0)); }
	inline Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * get_providerFunc_0() const { return ___providerFunc_0; }
	inline Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D ** get_address_of_providerFunc_0() { return &___providerFunc_0; }
	inline void set_providerFunc_0(Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * value)
	{
		___providerFunc_0 = value;
		Il2CppCodeGenWriteBarrier((&___providerFunc_0), value);
	}

	inline static int32_t get_offset_of_container_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass16_0_t3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69, ___container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_1() const { return ___container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_1() { return &___container_1; }
	inline void set_container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_1 = value;
		Il2CppCodeGenWriteBarrier((&___container_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS16_0_T3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T671341B83988426E4EF0E328A156524DD033791E_H
#define U3CU3EC__DISPLAYCLASS5_0_T671341B83988426E4EF0E328A156524DD033791E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t671341B83988426E4EF0E328A156524DD033791E  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.ScopableBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.ScopableBindingFinalizer Zenject.ScopableBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t671341B83988426E4EF0E328A156524DD033791E, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t671341B83988426E4EF0E328A156524DD033791E, ___U3CU3E4__this_1)); }
	inline ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T671341B83988426E4EF0E328A156524DD033791E_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TD33DA19F7BDF68403DB522B40F2E13502CAAF94C_H
#define U3CU3EC__DISPLAYCLASS6_0_TD33DA19F7BDF68403DB522B40F2E13502CAAF94C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tD33DA19F7BDF68403DB522B40F2E13502CAAF94C  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.ScopableBindingFinalizer/<>c__DisplayClass6_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.ScopableBindingFinalizer Zenject.ScopableBindingFinalizer/<>c__DisplayClass6_0::<>4__this
	ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tD33DA19F7BDF68403DB522B40F2E13502CAAF94C, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tD33DA19F7BDF68403DB522B40F2E13502CAAF94C, ___U3CU3E4__this_1)); }
	inline ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TD33DA19F7BDF68403DB522B40F2E13502CAAF94C_H
#ifndef SUBCONTAINERBINDER_T6D608228848D682ABBD552A157DF879945304DDF_H
#define SUBCONTAINERBINDER_T6D608228848D682ABBD552A157DF879945304DDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerBinder
struct  SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF  : public RuntimeObject
{
public:
	// Zenject.BindInfo Zenject.SubContainerBinder::_bindInfo
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * ____bindInfo_0;
	// Zenject.BindFinalizerWrapper Zenject.SubContainerBinder::_finalizerWrapper
	BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * ____finalizerWrapper_1;
	// System.Object Zenject.SubContainerBinder::_subIdentifier
	RuntimeObject * ____subIdentifier_2;

public:
	inline static int32_t get_offset_of__bindInfo_0() { return static_cast<int32_t>(offsetof(SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF, ____bindInfo_0)); }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * get__bindInfo_0() const { return ____bindInfo_0; }
	inline BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 ** get_address_of__bindInfo_0() { return &____bindInfo_0; }
	inline void set__bindInfo_0(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1 * value)
	{
		____bindInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____bindInfo_0), value);
	}

	inline static int32_t get_offset_of__finalizerWrapper_1() { return static_cast<int32_t>(offsetof(SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF, ____finalizerWrapper_1)); }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * get__finalizerWrapper_1() const { return ____finalizerWrapper_1; }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 ** get_address_of__finalizerWrapper_1() { return &____finalizerWrapper_1; }
	inline void set__finalizerWrapper_1(BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * value)
	{
		____finalizerWrapper_1 = value;
		Il2CppCodeGenWriteBarrier((&____finalizerWrapper_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERBINDER_T6D608228848D682ABBD552A157DF879945304DDF_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TF55F5A9B4503D57334B5B99926A3E010BA6990E1_H
#define U3CU3EC__DISPLAYCLASS5_0_TF55F5A9B4503D57334B5B99926A3E010BA6990E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerInstallerBindingFinalizer Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1, ___U3CU3E4__this_1)); }
	inline SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TF55F5A9B4503D57334B5B99926A3E010BA6990E1_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_TD196D1B2197FC063E15A307A2C089B00170012F2_H
#define U3CU3EC__DISPLAYCLASS5_1_TD196D1B2197FC063E15A307A2C089B00170012F2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_tD196D1B2197FC063E15A307A2C089B00170012F2  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_1::containerCreator
	RuntimeObject* ___containerCreator_0;
	// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_0 Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass5_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tD196D1B2197FC063E15A307A2C089B00170012F2, ___containerCreator_0)); }
	inline RuntimeObject* get_containerCreator_0() const { return ___containerCreator_0; }
	inline RuntimeObject** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(RuntimeObject* value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_tD196D1B2197FC063E15A307A2C089B00170012F2, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_TD196D1B2197FC063E15A307A2C089B00170012F2_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T33470911219FA31544BD15AB6DFCCC7EBB9F243E_H
#define U3CU3EC__DISPLAYCLASS6_0_T33470911219FA31544BD15AB6DFCCC7EBB9F243E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerInstallerBindingFinalizer Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_0::<>4__this
	SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E, ___U3CU3E4__this_1)); }
	inline SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T33470911219FA31544BD15AB6DFCCC7EBB9F243E_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F_H
#define U3CU3EC__DISPLAYCLASS6_1_T6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F  : public RuntimeObject
{
public:
	// Zenject.ISubContainerCreator Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_1::containerCreator
	RuntimeObject* ___containerCreator_0;
	// Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_0 Zenject.SubContainerInstallerBindingFinalizer/<>c__DisplayClass6_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F, ___containerCreator_0)); }
	inline RuntimeObject* get_containerCreator_0() const { return ___containerCreator_0; }
	inline RuntimeObject** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(RuntimeObject* value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F_H
#ifndef U3CU3EC__DISPLAYCLASS4_0_T8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F_H
#define U3CU3EC__DISPLAYCLASS4_0_T8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_0
struct  U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerMethodBindingFinalizer Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_0::<>4__this
	SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F, ___U3CU3E4__this_1)); }
	inline SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_0_T8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F_H
#ifndef U3CU3EC__DISPLAYCLASS4_1_T2E3E392C160FD73D6F795057C1D96407D882289E_H
#define U3CU3EC__DISPLAYCLASS4_1_T2E3E392C160FD73D6F795057C1D96407D882289E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_1
struct  U3CU3Ec__DisplayClass4_1_t2E3E392C160FD73D6F795057C1D96407D882289E  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_1::creator
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * ___creator_0;
	// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_0 Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass4_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_creator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t2E3E392C160FD73D6F795057C1D96407D882289E, ___creator_0)); }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * get_creator_0() const { return ___creator_0; }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 ** get_address_of_creator_0() { return &___creator_0; }
	inline void set_creator_0(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * value)
	{
		___creator_0 = value;
		Il2CppCodeGenWriteBarrier((&___creator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass4_1_t2E3E392C160FD73D6F795057C1D96407D882289E, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS4_1_T2E3E392C160FD73D6F795057C1D96407D882289E_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T686763593E6D3FAFE37419486DF47CC614BBB89E_H
#define U3CU3EC__DISPLAYCLASS5_0_T686763593E6D3FAFE37419486DF47CC614BBB89E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerMethodBindingFinalizer Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E, ___U3CU3E4__this_1)); }
	inline SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T686763593E6D3FAFE37419486DF47CC614BBB89E_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T123ABD1B51B7BB69119488CAEFFBB08B57104B58_H
#define U3CU3EC__DISPLAYCLASS5_1_T123ABD1B51B7BB69119488CAEFFBB08B57104B58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t123ABD1B51B7BB69119488CAEFFBB08B57104B58  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_1::containerCreator
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * ___containerCreator_0;
	// Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_0 Zenject.SubContainerMethodBindingFinalizer/<>c__DisplayClass5_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t123ABD1B51B7BB69119488CAEFFBB08B57104B58, ___containerCreator_0)); }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t123ABD1B51B7BB69119488CAEFFBB08B57104B58, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T123ABD1B51B7BB69119488CAEFFBB08B57104B58_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_T57364071B16667E624DD84D49042B299919EDE16_H
#define U3CU3EC__DISPLAYCLASS5_0_T57364071B16667E624DD84D49042B299919EDE16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerPrefabBindingFinalizer Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_T57364071B16667E624DD84D49042B299919EDE16_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T2CE375160CE52751FA94BBD3257A3DE9FF91EF84_H
#define U3CU3EC__DISPLAYCLASS5_1_T2CE375160CE52751FA94BBD3257A3DE9FF91EF84_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t2CE375160CE52751FA94BBD3257A3DE9FF91EF84  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_1::containerCreator
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * ___containerCreator_0;
	// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_0 Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass5_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t2CE375160CE52751FA94BBD3257A3DE9FF91EF84, ___containerCreator_0)); }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t2CE375160CE52751FA94BBD3257A3DE9FF91EF84, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T2CE375160CE52751FA94BBD3257A3DE9FF91EF84_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_TE95844800F4E141486AB15308F50728AD59B04BB_H
#define U3CU3EC__DISPLAYCLASS6_0_TE95844800F4E141486AB15308F50728AD59B04BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerPrefabBindingFinalizer Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_0::<>4__this
	SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_TE95844800F4E141486AB15308F50728AD59B04BB_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T7E4FBAEDA265DB02BE0799B830292E45F88C91C1_H
#define U3CU3EC__DISPLAYCLASS6_1_T7E4FBAEDA265DB02BE0799B830292E45F88C91C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t7E4FBAEDA265DB02BE0799B830292E45F88C91C1  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_1::containerCreator
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * ___containerCreator_0;
	// Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_0 Zenject.SubContainerPrefabBindingFinalizer/<>c__DisplayClass6_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t7E4FBAEDA265DB02BE0799B830292E45F88C91C1, ___containerCreator_0)); }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t7E4FBAEDA265DB02BE0799B830292E45F88C91C1, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T7E4FBAEDA265DB02BE0799B830292E45F88C91C1_H
#ifndef U3CU3EC__DISPLAYCLASS5_0_TBC022BE40EB4177C07DF3DF7C38830398C3B7CF5_H
#define U3CU3EC__DISPLAYCLASS5_0_TBC022BE40EB4177C07DF3DF7C38830398C3B7CF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_0
struct  U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_0::<>4__this
	SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_0_TBC022BE40EB4177C07DF3DF7C38830398C3B7CF5_H
#ifndef U3CU3EC__DISPLAYCLASS5_1_T803DC2A04E0D8F018FA6A38075623FF4D2E44D0A_H
#define U3CU3EC__DISPLAYCLASS5_1_T803DC2A04E0D8F018FA6A38075623FF4D2E44D0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_1
struct  U3CU3Ec__DisplayClass5_1_t803DC2A04E0D8F018FA6A38075623FF4D2E44D0A  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_1::containerCreator
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * ___containerCreator_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_0 Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass5_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t803DC2A04E0D8F018FA6A38075623FF4D2E44D0A, ___containerCreator_0)); }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass5_1_t803DC2A04E0D8F018FA6A38075623FF4D2E44D0A, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS5_1_T803DC2A04E0D8F018FA6A38075623FF4D2E44D0A_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T3A79532E9A95BE473A4D254E6BA0490E00B7A567_H
#define U3CU3EC__DISPLAYCLASS6_0_T3A79532E9A95BE473A4D254E6BA0490E00B7A567_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_0::container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___container_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_0::<>4__this
	SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567, ___container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_container_0() const { return ___container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567, ___U3CU3E4__this_1)); }
	inline SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T3A79532E9A95BE473A4D254E6BA0490E00B7A567_H
#ifndef U3CU3EC__DISPLAYCLASS6_1_T232CADB2592F1A9746D69FBFD2FACC762311748A_H
#define U3CU3EC__DISPLAYCLASS6_1_T232CADB2592F1A9746D69FBFD2FACC762311748A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_1
struct  U3CU3Ec__DisplayClass6_1_t232CADB2592F1A9746D69FBFD2FACC762311748A  : public RuntimeObject
{
public:
	// Zenject.SubContainerCreatorCached Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_1::containerCreator
	SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * ___containerCreator_0;
	// Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_0 Zenject.SubContainerPrefabResourceBindingFinalizer/<>c__DisplayClass6_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567 * ___CSU24U3CU3E8__locals1_1;

public:
	inline static int32_t get_offset_of_containerCreator_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t232CADB2592F1A9746D69FBFD2FACC762311748A, ___containerCreator_0)); }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * get_containerCreator_0() const { return ___containerCreator_0; }
	inline SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 ** get_address_of_containerCreator_0() { return &___containerCreator_0; }
	inline void set_containerCreator_0(SubContainerCreatorCached_tFC8561A0090D43D70962FC95E8ACD20596D4DE79 * value)
	{
		___containerCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&___containerCreator_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_1_t232CADB2592F1A9746D69FBFD2FACC762311748A, ___CSU24U3CU3E8__locals1_1)); }
	inline U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567 * get_CSU24U3CU3E8__locals1_1() const { return ___CSU24U3CU3E8__locals1_1; }
	inline U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567 ** get_address_of_CSU24U3CU3E8__locals1_1() { return &___CSU24U3CU3E8__locals1_1; }
	inline void set_CSU24U3CU3E8__locals1_1(U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567 * value)
	{
		___CSU24U3CU3E8__locals1_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_1_T232CADB2592F1A9746D69FBFD2FACC762311748A_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef ARGNONLAZYBINDER_T81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F_H
#define ARGNONLAZYBINDER_T81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgNonLazyBinder
struct  ArgNonLazyBinder_t81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGNONLAZYBINDER_T81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F_H
#ifndef COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#define COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CopyNonLazyBinder
struct  CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYNONLAZYBINDER_T4C212B7E1BE267539D091F759AA6A0A9A4029A67_H
#ifndef PREFABBINDINGFINALIZER_T3A03643A17E89E86C2E57B6FFF221C4FC776E661_H
#define PREFABBINDINGFINALIZER_T3A03643A17E89E86C2E57B6FFF221C4FC776E661_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabBindingFinalizer
struct  PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// Zenject.GameObjectCreationParameters Zenject.PrefabBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_1;
	// UnityEngine.Object Zenject.PrefabBindingFinalizer::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_2;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.PrefabBindingFinalizer::_providerFactory
	Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * ____providerFactory_3;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_1), value);
	}

	inline static int32_t get_offset_of__prefab_2() { return static_cast<int32_t>(offsetof(PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661, ____prefab_2)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_2() const { return ____prefab_2; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_2() { return &____prefab_2; }
	inline void set__prefab_2(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_2), value);
	}

	inline static int32_t get_offset_of__providerFactory_3() { return static_cast<int32_t>(offsetof(PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661, ____providerFactory_3)); }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * get__providerFactory_3() const { return ____providerFactory_3; }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 ** get_address_of__providerFactory_3() { return &____providerFactory_3; }
	inline void set__providerFactory_3(Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * value)
	{
		____providerFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABBINDINGFINALIZER_T3A03643A17E89E86C2E57B6FFF221C4FC776E661_H
#ifndef PREFABRESOURCEBINDINGFINALIZER_T354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B_H
#define PREFABRESOURCEBINDINGFINALIZER_T354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceBindingFinalizer
struct  PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// Zenject.GameObjectCreationParameters Zenject.PrefabResourceBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_1;
	// System.String Zenject.PrefabResourceBindingFinalizer::_resourcePath
	String_t* ____resourcePath_2;
	// System.Func`3<System.Type,Zenject.IPrefabInstantiator,Zenject.IProvider> Zenject.PrefabResourceBindingFinalizer::_providerFactory
	Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * ____providerFactory_3;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_1), value);
	}

	inline static int32_t get_offset_of__resourcePath_2() { return static_cast<int32_t>(offsetof(PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B, ____resourcePath_2)); }
	inline String_t* get__resourcePath_2() const { return ____resourcePath_2; }
	inline String_t** get_address_of__resourcePath_2() { return &____resourcePath_2; }
	inline void set__resourcePath_2(String_t* value)
	{
		____resourcePath_2 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_2), value);
	}

	inline static int32_t get_offset_of__providerFactory_3() { return static_cast<int32_t>(offsetof(PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B, ____providerFactory_3)); }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * get__providerFactory_3() const { return ____providerFactory_3; }
	inline Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 ** get_address_of__providerFactory_3() { return &____providerFactory_3; }
	inline void set__providerFactory_3(Func_3_t15800664D7AE729E17F1E304A5985101EEDC0FE9 * value)
	{
		____providerFactory_3 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABRESOURCEBINDINGFINALIZER_T354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B_H
#ifndef SCOPENONLAZYBINDER_T0DFE5D51BD6BFEBC496546DF2F74DA58890E8048_H
#define SCOPENONLAZYBINDER_T0DFE5D51BD6BFEBC496546DF2F74DA58890E8048_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeNonLazyBinder
struct  ScopeNonLazyBinder_t0DFE5D51BD6BFEBC496546DF2F74DA58890E8048  : public NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPENONLAZYBINDER_T0DFE5D51BD6BFEBC496546DF2F74DA58890E8048_H
#ifndef SINGLEPROVIDERBINDINGFINALIZER_TDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68_H
#define SINGLEPROVIDERBINDINGFINALIZER_TDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingleProviderBindingFinalizer
struct  SingleProviderBindingFinalizer_tDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.SingleProviderBindingFinalizer::_providerFactory
	Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * ____providerFactory_1;

public:
	inline static int32_t get_offset_of__providerFactory_1() { return static_cast<int32_t>(offsetof(SingleProviderBindingFinalizer_tDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68, ____providerFactory_1)); }
	inline Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * get__providerFactory_1() const { return ____providerFactory_1; }
	inline Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D ** get_address_of__providerFactory_1() { return &____providerFactory_1; }
	inline void set__providerFactory_1(Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * value)
	{
		____providerFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEPROVIDERBINDINGFINALIZER_TDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68_H
#ifndef SUBCONTAINERINSTALLERBINDINGFINALIZER_T5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439_H
#define SUBCONTAINERINSTALLERBINDINGFINALIZER_T5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerInstallerBindingFinalizer
struct  SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// System.Object Zenject.SubContainerInstallerBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_1;
	// System.Type Zenject.SubContainerInstallerBindingFinalizer::_installerType
	Type_t * ____installerType_2;

public:
	inline static int32_t get_offset_of__subIdentifier_1() { return static_cast<int32_t>(offsetof(SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439, ____subIdentifier_1)); }
	inline RuntimeObject * get__subIdentifier_1() const { return ____subIdentifier_1; }
	inline RuntimeObject ** get_address_of__subIdentifier_1() { return &____subIdentifier_1; }
	inline void set__subIdentifier_1(RuntimeObject * value)
	{
		____subIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_1), value);
	}

	inline static int32_t get_offset_of__installerType_2() { return static_cast<int32_t>(offsetof(SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439, ____installerType_2)); }
	inline Type_t * get__installerType_2() const { return ____installerType_2; }
	inline Type_t ** get_address_of__installerType_2() { return &____installerType_2; }
	inline void set__installerType_2(Type_t * value)
	{
		____installerType_2 = value;
		Il2CppCodeGenWriteBarrier((&____installerType_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERINSTALLERBINDINGFINALIZER_T5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439_H
#ifndef SUBCONTAINERMETHODBINDINGFINALIZER_T7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842_H
#define SUBCONTAINERMETHODBINDINGFINALIZER_T7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerMethodBindingFinalizer
struct  SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// System.Object Zenject.SubContainerMethodBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_1;
	// System.Action`1<Zenject.DiContainer> Zenject.SubContainerMethodBindingFinalizer::_installMethod
	Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * ____installMethod_2;

public:
	inline static int32_t get_offset_of__subIdentifier_1() { return static_cast<int32_t>(offsetof(SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842, ____subIdentifier_1)); }
	inline RuntimeObject * get__subIdentifier_1() const { return ____subIdentifier_1; }
	inline RuntimeObject ** get_address_of__subIdentifier_1() { return &____subIdentifier_1; }
	inline void set__subIdentifier_1(RuntimeObject * value)
	{
		____subIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_1), value);
	}

	inline static int32_t get_offset_of__installMethod_2() { return static_cast<int32_t>(offsetof(SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842, ____installMethod_2)); }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * get__installMethod_2() const { return ____installMethod_2; }
	inline Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A ** get_address_of__installMethod_2() { return &____installMethod_2; }
	inline void set__installMethod_2(Action_1_t3DD18704E712DAA288588BFF42EDE09A39BC678A * value)
	{
		____installMethod_2 = value;
		Il2CppCodeGenWriteBarrier((&____installMethod_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERMETHODBINDINGFINALIZER_T7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842_H
#ifndef SUBCONTAINERPREFABBINDINGFINALIZER_TCBEFDC5A6CE5514659C82111F46E0C68C8CED45C_H
#define SUBCONTAINERPREFABBINDINGFINALIZER_TCBEFDC5A6CE5514659C82111F46E0C68C8CED45C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabBindingFinalizer
struct  SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// UnityEngine.Object Zenject.SubContainerPrefabBindingFinalizer::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_1;
	// System.Object Zenject.SubContainerPrefabBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_2;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerPrefabBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_3;

public:
	inline static int32_t get_offset_of__prefab_1() { return static_cast<int32_t>(offsetof(SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C, ____prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_1() const { return ____prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_1() { return &____prefab_1; }
	inline void set__prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERPREFABBINDINGFINALIZER_TCBEFDC5A6CE5514659C82111F46E0C68C8CED45C_H
#ifndef SUBCONTAINERPREFABRESOURCEBINDINGFINALIZER_TF73E5148915E7E8AB5FD55128292037C6A6A2A2F_H
#define SUBCONTAINERPREFABRESOURCEBINDINGFINALIZER_TF73E5148915E7E8AB5FD55128292037C6A6A2A2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SubContainerPrefabResourceBindingFinalizer
struct  SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// System.String Zenject.SubContainerPrefabResourceBindingFinalizer::_resourcePath
	String_t* ____resourcePath_1;
	// System.Object Zenject.SubContainerPrefabResourceBindingFinalizer::_subIdentifier
	RuntimeObject * ____subIdentifier_2;
	// Zenject.GameObjectCreationParameters Zenject.SubContainerPrefabResourceBindingFinalizer::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_3;

public:
	inline static int32_t get_offset_of__resourcePath_1() { return static_cast<int32_t>(offsetof(SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F, ____resourcePath_1)); }
	inline String_t* get__resourcePath_1() const { return ____resourcePath_1; }
	inline String_t** get_address_of__resourcePath_1() { return &____resourcePath_1; }
	inline void set__resourcePath_1(String_t* value)
	{
		____resourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_1), value);
	}

	inline static int32_t get_offset_of__subIdentifier_2() { return static_cast<int32_t>(offsetof(SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F, ____subIdentifier_2)); }
	inline RuntimeObject * get__subIdentifier_2() const { return ____subIdentifier_2; }
	inline RuntimeObject ** get_address_of__subIdentifier_2() { return &____subIdentifier_2; }
	inline void set__subIdentifier_2(RuntimeObject * value)
	{
		____subIdentifier_2 = value;
		Il2CppCodeGenWriteBarrier((&____subIdentifier_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SUBCONTAINERPREFABRESOURCEBINDINGFINALIZER_TF73E5148915E7E8AB5FD55128292037C6A6A2A2F_H
#ifndef NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#define NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Quaternion>
struct  Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41 
{
public:
	// T System.Nullable`1::value
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41, ___value_0)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_value_0() const { return ___value_0; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1AF22E72609C109A63AC7BE47F596B5956149D41_H
#ifndef NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#define NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<UnityEngine.Vector3>
struct  Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 
{
public:
	// T System.Nullable`1::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T75BFB9848B0C0C6930A6973335B59621D36E6203_H
#ifndef CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#define CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ConditionCopyNonLazyBinder
struct  ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7  : public CopyNonLazyBinder_t4C212B7E1BE267539D091F759AA6A0A9A4029A67
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONCOPYNONLAZYBINDER_TA82CD5BABE0526375567A1A0064DAE8035AA37C7_H
#ifndef INVALIDBINDRESPONSES_TA2A16E511A535774CC9D10EA025DA4C5B46E9D24_H
#define INVALIDBINDRESPONSES_TA2A16E511A535774CC9D10EA025DA4C5B46E9D24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InvalidBindResponses
struct  InvalidBindResponses_tA2A16E511A535774CC9D10EA025DA4C5B46E9D24 
{
public:
	// System.Int32 Zenject.InvalidBindResponses::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InvalidBindResponses_tA2A16E511A535774CC9D10EA025DA4C5B46E9D24, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVALIDBINDRESPONSES_TA2A16E511A535774CC9D10EA025DA4C5B46E9D24_H
#ifndef POOLEXPANDMETHODS_T778C4D7478B1DC979FE2B16031F3C6695A2E81D4_H
#define POOLEXPANDMETHODS_T778C4D7478B1DC979FE2B16031F3C6695A2E81D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PoolExpandMethods
struct  PoolExpandMethods_t778C4D7478B1DC979FE2B16031F3C6695A2E81D4 
{
public:
	// System.Int32 Zenject.PoolExpandMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PoolExpandMethods_t778C4D7478B1DC979FE2B16031F3C6695A2E81D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POOLEXPANDMETHODS_T778C4D7478B1DC979FE2B16031F3C6695A2E81D4_H
#ifndef SCOPEARGNONLAZYBINDER_T5BF897B041A6B79DC7F088CF0A35632CCA74D74F_H
#define SCOPEARGNONLAZYBINDER_T5BF897B041A6B79DC7F088CF0A35632CCA74D74F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgNonLazyBinder
struct  ScopeArgNonLazyBinder_t5BF897B041A6B79DC7F088CF0A35632CCA74D74F  : public ArgNonLazyBinder_t81E8AB6F5213E5C3CC7CA6087C9A98CD843DC88F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGNONLAZYBINDER_T5BF897B041A6B79DC7F088CF0A35632CCA74D74F_H
#ifndef SCOPETYPES_T2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4_H
#define SCOPETYPES_T2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeTypes
struct  ScopeTypes_t2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4 
{
public:
	// System.Int32 Zenject.ScopeTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScopeTypes_t2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPETYPES_T2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4_H
#ifndef SINGLETONTYPES_T98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E_H
#define SINGLETONTYPES_T98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonTypes
struct  SingletonTypes_t98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E 
{
public:
	// System.Int32 Zenject.SingletonTypes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SingletonTypes_t98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONTYPES_T98C89A97CD1D325BCFF10A9F7EAE7F21C16D811E_H
#ifndef TOCHOICES_TD5BE8210291AF2D3503FE46815AC2FA615A91E91_H
#define TOCHOICES_TD5BE8210291AF2D3503FE46815AC2FA615A91E91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ToChoices
struct  ToChoices_tD5BE8210291AF2D3503FE46815AC2FA615A91E91 
{
public:
	// System.Int32 Zenject.ToChoices::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ToChoices_tD5BE8210291AF2D3503FE46815AC2FA615A91E91, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOCHOICES_TD5BE8210291AF2D3503FE46815AC2FA615A91E91_H
#ifndef ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#define ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ArgConditionCopyNonLazyBinder
struct  ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327  : public ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARGCONDITIONCOPYNONLAZYBINDER_TB4B41047F4CCC5148A492F92ABD4A9DAAC588327_H
#ifndef BINDINFO_T6175BDBC1E67872F3864E2546CE028E508F29AD1_H
#define BINDINFO_T6175BDBC1E67872F3864E2546CE028E508F29AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.BindInfo
struct  BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1  : public RuntimeObject
{
public:
	// System.String Zenject.BindInfo::<ContextInfo>k__BackingField
	String_t* ___U3CContextInfoU3Ek__BackingField_0;
	// System.Boolean Zenject.BindInfo::<RequireExplicitScope>k__BackingField
	bool ___U3CRequireExplicitScopeU3Ek__BackingField_1;
	// System.Object Zenject.BindInfo::<Identifier>k__BackingField
	RuntimeObject * ___U3CIdentifierU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::<ContractTypes>k__BackingField
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___U3CContractTypesU3Ek__BackingField_3;
	// System.Boolean Zenject.BindInfo::<CopyIntoAllSubContainers>k__BackingField
	bool ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4;
	// Zenject.InvalidBindResponses Zenject.BindInfo::<InvalidBindResponse>k__BackingField
	int32_t ___U3CInvalidBindResponseU3Ek__BackingField_5;
	// System.Boolean Zenject.BindInfo::<NonLazy>k__BackingField
	bool ___U3CNonLazyU3Ek__BackingField_6;
	// Zenject.BindingCondition Zenject.BindInfo::<Condition>k__BackingField
	BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 * ___U3CConditionU3Ek__BackingField_7;
	// Zenject.ToChoices Zenject.BindInfo::<ToChoice>k__BackingField
	int32_t ___U3CToChoiceU3Ek__BackingField_8;
	// System.Collections.Generic.List`1<System.Type> Zenject.BindInfo::<ToTypes>k__BackingField
	List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * ___U3CToTypesU3Ek__BackingField_9;
	// Zenject.ScopeTypes Zenject.BindInfo::<Scope>k__BackingField
	int32_t ___U3CScopeU3Ek__BackingField_10;
	// System.Object Zenject.BindInfo::<ConcreteIdentifier>k__BackingField
	RuntimeObject * ___U3CConcreteIdentifierU3Ek__BackingField_11;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.BindInfo::<Arguments>k__BackingField
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___U3CArgumentsU3Ek__BackingField_12;

public:
	inline static int32_t get_offset_of_U3CContextInfoU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CContextInfoU3Ek__BackingField_0)); }
	inline String_t* get_U3CContextInfoU3Ek__BackingField_0() const { return ___U3CContextInfoU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CContextInfoU3Ek__BackingField_0() { return &___U3CContextInfoU3Ek__BackingField_0; }
	inline void set_U3CContextInfoU3Ek__BackingField_0(String_t* value)
	{
		___U3CContextInfoU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContextInfoU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CRequireExplicitScopeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CRequireExplicitScopeU3Ek__BackingField_1)); }
	inline bool get_U3CRequireExplicitScopeU3Ek__BackingField_1() const { return ___U3CRequireExplicitScopeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CRequireExplicitScopeU3Ek__BackingField_1() { return &___U3CRequireExplicitScopeU3Ek__BackingField_1; }
	inline void set_U3CRequireExplicitScopeU3Ek__BackingField_1(bool value)
	{
		___U3CRequireExplicitScopeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CIdentifierU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CIdentifierU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3CIdentifierU3Ek__BackingField_2() const { return ___U3CIdentifierU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3CIdentifierU3Ek__BackingField_2() { return &___U3CIdentifierU3Ek__BackingField_2; }
	inline void set_U3CIdentifierU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3CIdentifierU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdentifierU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CContractTypesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CContractTypesU3Ek__BackingField_3)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_U3CContractTypesU3Ek__BackingField_3() const { return ___U3CContractTypesU3Ek__BackingField_3; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_U3CContractTypesU3Ek__BackingField_3() { return &___U3CContractTypesU3Ek__BackingField_3; }
	inline void set_U3CContractTypesU3Ek__BackingField_3(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___U3CContractTypesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContractTypesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4)); }
	inline bool get_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() const { return ___U3CCopyIntoAllSubContainersU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4() { return &___U3CCopyIntoAllSubContainersU3Ek__BackingField_4; }
	inline void set_U3CCopyIntoAllSubContainersU3Ek__BackingField_4(bool value)
	{
		___U3CCopyIntoAllSubContainersU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CInvalidBindResponseU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CInvalidBindResponseU3Ek__BackingField_5)); }
	inline int32_t get_U3CInvalidBindResponseU3Ek__BackingField_5() const { return ___U3CInvalidBindResponseU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CInvalidBindResponseU3Ek__BackingField_5() { return &___U3CInvalidBindResponseU3Ek__BackingField_5; }
	inline void set_U3CInvalidBindResponseU3Ek__BackingField_5(int32_t value)
	{
		___U3CInvalidBindResponseU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CNonLazyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CNonLazyU3Ek__BackingField_6)); }
	inline bool get_U3CNonLazyU3Ek__BackingField_6() const { return ___U3CNonLazyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CNonLazyU3Ek__BackingField_6() { return &___U3CNonLazyU3Ek__BackingField_6; }
	inline void set_U3CNonLazyU3Ek__BackingField_6(bool value)
	{
		___U3CNonLazyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CConditionU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CConditionU3Ek__BackingField_7)); }
	inline BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 * get_U3CConditionU3Ek__BackingField_7() const { return ___U3CConditionU3Ek__BackingField_7; }
	inline BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 ** get_address_of_U3CConditionU3Ek__BackingField_7() { return &___U3CConditionU3Ek__BackingField_7; }
	inline void set_U3CConditionU3Ek__BackingField_7(BindingCondition_tC316B7CC19C4C70BF281CED3512B1E88CE20BCA3 * value)
	{
		___U3CConditionU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CToChoiceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CToChoiceU3Ek__BackingField_8)); }
	inline int32_t get_U3CToChoiceU3Ek__BackingField_8() const { return ___U3CToChoiceU3Ek__BackingField_8; }
	inline int32_t* get_address_of_U3CToChoiceU3Ek__BackingField_8() { return &___U3CToChoiceU3Ek__BackingField_8; }
	inline void set_U3CToChoiceU3Ek__BackingField_8(int32_t value)
	{
		___U3CToChoiceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CToTypesU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CToTypesU3Ek__BackingField_9)); }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * get_U3CToTypesU3Ek__BackingField_9() const { return ___U3CToTypesU3Ek__BackingField_9; }
	inline List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 ** get_address_of_U3CToTypesU3Ek__BackingField_9() { return &___U3CToTypesU3Ek__BackingField_9; }
	inline void set_U3CToTypesU3Ek__BackingField_9(List_1_tE9D3AD6B1DEE5D980D8D2F6C31987453E6E1C1E0 * value)
	{
		___U3CToTypesU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CToTypesU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CScopeU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CScopeU3Ek__BackingField_10)); }
	inline int32_t get_U3CScopeU3Ek__BackingField_10() const { return ___U3CScopeU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CScopeU3Ek__BackingField_10() { return &___U3CScopeU3Ek__BackingField_10; }
	inline void set_U3CScopeU3Ek__BackingField_10(int32_t value)
	{
		___U3CScopeU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CConcreteIdentifierU3Ek__BackingField_11)); }
	inline RuntimeObject * get_U3CConcreteIdentifierU3Ek__BackingField_11() const { return ___U3CConcreteIdentifierU3Ek__BackingField_11; }
	inline RuntimeObject ** get_address_of_U3CConcreteIdentifierU3Ek__BackingField_11() { return &___U3CConcreteIdentifierU3Ek__BackingField_11; }
	inline void set_U3CConcreteIdentifierU3Ek__BackingField_11(RuntimeObject * value)
	{
		___U3CConcreteIdentifierU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConcreteIdentifierU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CArgumentsU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1, ___U3CArgumentsU3Ek__BackingField_12)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_U3CArgumentsU3Ek__BackingField_12() const { return ___U3CArgumentsU3Ek__BackingField_12; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_U3CArgumentsU3Ek__BackingField_12() { return &___U3CArgumentsU3Ek__BackingField_12; }
	inline void set_U3CArgumentsU3Ek__BackingField_12(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___U3CArgumentsU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CArgumentsU3Ek__BackingField_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINFO_T6175BDBC1E67872F3864E2546CE028E508F29AD1_H
#ifndef GAMEOBJECTCREATIONPARAMETERS_TA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_H
#define GAMEOBJECTCREATIONPARAMETERS_TA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GameObjectCreationParameters
struct  GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4  : public RuntimeObject
{
public:
	// System.String Zenject.GameObjectCreationParameters::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String Zenject.GameObjectCreationParameters::<GroupName>k__BackingField
	String_t* ___U3CGroupNameU3Ek__BackingField_1;
	// UnityEngine.Transform Zenject.GameObjectCreationParameters::<ParentTransform>k__BackingField
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___U3CParentTransformU3Ek__BackingField_2;
	// System.Func`2<Zenject.InjectContext,UnityEngine.Transform> Zenject.GameObjectCreationParameters::<ParentTransformGetter>k__BackingField
	Func_2_t4ED720590592A8375BE8E36276393417F557F933 * ___U3CParentTransformGetterU3Ek__BackingField_3;
	// System.Nullable`1<UnityEngine.Vector3> Zenject.GameObjectCreationParameters::<Position>k__BackingField
	Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  ___U3CPositionU3Ek__BackingField_4;
	// System.Nullable`1<UnityEngine.Quaternion> Zenject.GameObjectCreationParameters::<Rotation>k__BackingField
	Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  ___U3CRotationU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CGroupNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CGroupNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CGroupNameU3Ek__BackingField_1() const { return ___U3CGroupNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CGroupNameU3Ek__BackingField_1() { return &___U3CGroupNameU3Ek__BackingField_1; }
	inline void set_U3CGroupNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CGroupNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGroupNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CParentTransformU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CParentTransformU3Ek__BackingField_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_U3CParentTransformU3Ek__BackingField_2() const { return ___U3CParentTransformU3Ek__BackingField_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_U3CParentTransformU3Ek__BackingField_2() { return &___U3CParentTransformU3Ek__BackingField_2; }
	inline void set_U3CParentTransformU3Ek__BackingField_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___U3CParentTransformU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentTransformU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParentTransformGetterU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CParentTransformGetterU3Ek__BackingField_3)); }
	inline Func_2_t4ED720590592A8375BE8E36276393417F557F933 * get_U3CParentTransformGetterU3Ek__BackingField_3() const { return ___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline Func_2_t4ED720590592A8375BE8E36276393417F557F933 ** get_address_of_U3CParentTransformGetterU3Ek__BackingField_3() { return &___U3CParentTransformGetterU3Ek__BackingField_3; }
	inline void set_U3CParentTransformGetterU3Ek__BackingField_3(Func_2_t4ED720590592A8375BE8E36276393417F557F933 * value)
	{
		___U3CParentTransformGetterU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParentTransformGetterU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPositionU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CPositionU3Ek__BackingField_4)); }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  get_U3CPositionU3Ek__BackingField_4() const { return ___U3CPositionU3Ek__BackingField_4; }
	inline Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203 * get_address_of_U3CPositionU3Ek__BackingField_4() { return &___U3CPositionU3Ek__BackingField_4; }
	inline void set_U3CPositionU3Ek__BackingField_4(Nullable_1_t75BFB9848B0C0C6930A6973335B59621D36E6203  value)
	{
		___U3CPositionU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CRotationU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4, ___U3CRotationU3Ek__BackingField_5)); }
	inline Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  get_U3CRotationU3Ek__BackingField_5() const { return ___U3CRotationU3Ek__BackingField_5; }
	inline Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41 * get_address_of_U3CRotationU3Ek__BackingField_5() { return &___U3CRotationU3Ek__BackingField_5; }
	inline void set_U3CRotationU3Ek__BackingField_5(Nullable_1_t1AF22E72609C109A63AC7BE47F596B5956149D41  value)
	{
		___U3CRotationU3Ek__BackingField_5 = value;
	}
};

struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_StaticFields
{
public:
	// Zenject.GameObjectCreationParameters Zenject.GameObjectCreationParameters::Default
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___Default_6;

public:
	inline static int32_t get_offset_of_Default_6() { return static_cast<int32_t>(offsetof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_StaticFields, ___Default_6)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_Default_6() const { return ___Default_6; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_Default_6() { return &___Default_6; }
	inline void set_Default_6(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___Default_6 = value;
		Il2CppCodeGenWriteBarrier((&___Default_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEOBJECTCREATIONPARAMETERS_TA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_H
#ifndef MEMORYPOOLBINDINFO_T89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E_H
#define MEMORYPOOLBINDINFO_T89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MemoryPoolBindInfo
struct  MemoryPoolBindInfo_t89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E  : public RuntimeObject
{
public:
	// Zenject.PoolExpandMethods Zenject.MemoryPoolBindInfo::<ExpandMethod>k__BackingField
	int32_t ___U3CExpandMethodU3Ek__BackingField_0;
	// System.Int32 Zenject.MemoryPoolBindInfo::<InitialSize>k__BackingField
	int32_t ___U3CInitialSizeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CExpandMethodU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(MemoryPoolBindInfo_t89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E, ___U3CExpandMethodU3Ek__BackingField_0)); }
	inline int32_t get_U3CExpandMethodU3Ek__BackingField_0() const { return ___U3CExpandMethodU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CExpandMethodU3Ek__BackingField_0() { return &___U3CExpandMethodU3Ek__BackingField_0; }
	inline void set_U3CExpandMethodU3Ek__BackingField_0(int32_t value)
	{
		___U3CExpandMethodU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CInitialSizeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(MemoryPoolBindInfo_t89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E, ___U3CInitialSizeU3Ek__BackingField_1)); }
	inline int32_t get_U3CInitialSizeU3Ek__BackingField_1() const { return ___U3CInitialSizeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CInitialSizeU3Ek__BackingField_1() { return &___U3CInitialSizeU3Ek__BackingField_1; }
	inline void set_U3CInitialSizeU3Ek__BackingField_1(int32_t value)
	{
		___U3CInitialSizeU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYPOOLBINDINFO_T89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E_H
#ifndef SCOPABLEBINDINGFINALIZER_T6F104B7C464DFDAE8F32C5942F74CB0E096073DA_H
#define SCOPABLEBINDINGFINALIZER_T6F104B7C464DFDAE8F32C5942F74CB0E096073DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopableBindingFinalizer
struct  ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA  : public ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD
{
public:
	// Zenject.SingletonTypes Zenject.ScopableBindingFinalizer::_singletonType
	int32_t ____singletonType_1;
	// System.Func`3<Zenject.DiContainer,System.Type,Zenject.IProvider> Zenject.ScopableBindingFinalizer::_providerFactory
	Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * ____providerFactory_2;
	// System.Object Zenject.ScopableBindingFinalizer::_singletonSpecificId
	RuntimeObject * ____singletonSpecificId_3;

public:
	inline static int32_t get_offset_of__singletonType_1() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA, ____singletonType_1)); }
	inline int32_t get__singletonType_1() const { return ____singletonType_1; }
	inline int32_t* get_address_of__singletonType_1() { return &____singletonType_1; }
	inline void set__singletonType_1(int32_t value)
	{
		____singletonType_1 = value;
	}

	inline static int32_t get_offset_of__providerFactory_2() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA, ____providerFactory_2)); }
	inline Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * get__providerFactory_2() const { return ____providerFactory_2; }
	inline Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D ** get_address_of__providerFactory_2() { return &____providerFactory_2; }
	inline void set__providerFactory_2(Func_3_t8011021FE4DCE0477D99C7B0055DB2F9A995DE3D * value)
	{
		____providerFactory_2 = value;
		Il2CppCodeGenWriteBarrier((&____providerFactory_2), value);
	}

	inline static int32_t get_offset_of__singletonSpecificId_3() { return static_cast<int32_t>(offsetof(ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA, ____singletonSpecificId_3)); }
	inline RuntimeObject * get__singletonSpecificId_3() const { return ____singletonSpecificId_3; }
	inline RuntimeObject ** get_address_of__singletonSpecificId_3() { return &____singletonSpecificId_3; }
	inline void set__singletonSpecificId_3(RuntimeObject * value)
	{
		____singletonSpecificId_3 = value;
		Il2CppCodeGenWriteBarrier((&____singletonSpecificId_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPABLEBINDINGFINALIZER_T6F104B7C464DFDAE8F32C5942F74CB0E096073DA_H
#ifndef SCOPECONDITIONCOPYNONLAZYBINDER_T322A4D615E478DDAB6579F88D71AD0328B8AF13B_H
#define SCOPECONDITIONCOPYNONLAZYBINDER_T322A4D615E478DDAB6579F88D71AD0328B8AF13B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeConditionCopyNonLazyBinder
struct  ScopeConditionCopyNonLazyBinder_t322A4D615E478DDAB6579F88D71AD0328B8AF13B  : public ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPECONDITIONCOPYNONLAZYBINDER_T322A4D615E478DDAB6579F88D71AD0328B8AF13B_H
#ifndef TRANSFORMCONDITIONCOPYNONLAZYBINDER_TDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC_H
#define TRANSFORMCONDITIONCOPYNONLAZYBINDER_TDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformConditionCopyNonLazyBinder
struct  TransformConditionCopyNonLazyBinder_tDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC  : public ConditionCopyNonLazyBinder_tA82CD5BABE0526375567A1A0064DAE8035AA37C7
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformConditionCopyNonLazyBinder_tDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMCONDITIONCOPYNONLAZYBINDER_TDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC_H
#ifndef TRANSFORMSCOPEARGNONLAZYBINDER_T8728EE391A28AA4530CE40D305EB23EEDEEF6341_H
#define TRANSFORMSCOPEARGNONLAZYBINDER_T8728EE391A28AA4530CE40D305EB23EEDEEF6341_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformScopeArgNonLazyBinder
struct  TransformScopeArgNonLazyBinder_t8728EE391A28AA4530CE40D305EB23EEDEEF6341  : public ScopeArgNonLazyBinder_t5BF897B041A6B79DC7F088CF0A35632CCA74D74F
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformScopeArgNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformScopeArgNonLazyBinder_t8728EE391A28AA4530CE40D305EB23EEDEEF6341, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSCOPEARGNONLAZYBINDER_T8728EE391A28AA4530CE40D305EB23EEDEEF6341_H
#ifndef IDSCOPECONDITIONCOPYNONLAZYBINDER_T8AB2C520254742ED74E2273A4B7AC9BF09BA05AE_H
#define IDSCOPECONDITIONCOPYNONLAZYBINDER_T8AB2C520254742ED74E2273A4B7AC9BF09BA05AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IdScopeConditionCopyNonLazyBinder
struct  IdScopeConditionCopyNonLazyBinder_t8AB2C520254742ED74E2273A4B7AC9BF09BA05AE  : public ScopeConditionCopyNonLazyBinder_t322A4D615E478DDAB6579F88D71AD0328B8AF13B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDSCOPECONDITIONCOPYNONLAZYBINDER_T8AB2C520254742ED74E2273A4B7AC9BF09BA05AE_H
#ifndef NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T8ACB78C05213954714A62E1AA18BDF3EACACB8BE_H
#define NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T8ACB78C05213954714A62E1AA18BDF3EACACB8BE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformConditionCopyNonLazyBinder
struct  NameTransformConditionCopyNonLazyBinder_t8ACB78C05213954714A62E1AA18BDF3EACACB8BE  : public TransformConditionCopyNonLazyBinder_tDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMCONDITIONCOPYNONLAZYBINDER_T8ACB78C05213954714A62E1AA18BDF3EACACB8BE_H
#ifndef NAMETRANSFORMSCOPEARGNONLAZYBINDER_T6B13DA4025DE2DD0F1A0180BF3D14B01B5614DE7_H
#define NAMETRANSFORMSCOPEARGNONLAZYBINDER_T6B13DA4025DE2DD0F1A0180BF3D14B01B5614DE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformScopeArgNonLazyBinder
struct  NameTransformScopeArgNonLazyBinder_t6B13DA4025DE2DD0F1A0180BF3D14B01B5614DE7  : public TransformScopeArgNonLazyBinder_t8728EE391A28AA4530CE40D305EB23EEDEEF6341
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMSCOPEARGNONLAZYBINDER_T6B13DA4025DE2DD0F1A0180BF3D14B01B5614DE7_H
#ifndef SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#define SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScopeArgConditionCopyNonLazyBinder
struct  ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F  : public ArgConditionCopyNonLazyBinder_tB4B41047F4CCC5148A492F92ABD4A9DAAC588327
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCOPEARGCONDITIONCOPYNONLAZYBINDER_TF12679CE42CB0E48D76D73799596B64C1A66F73F_H
#ifndef TRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T5B58F94B8996878CA2C5D15F4560299411AC6514_H
#define TRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T5B58F94B8996878CA2C5D15F4560299411AC6514_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformScopeConditionCopyNonLazyBinder
struct  TransformScopeConditionCopyNonLazyBinder_t5B58F94B8996878CA2C5D15F4560299411AC6514  : public ScopeConditionCopyNonLazyBinder_t322A4D615E478DDAB6579F88D71AD0328B8AF13B
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformScopeConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformScopeConditionCopyNonLazyBinder_t5B58F94B8996878CA2C5D15F4560299411AC6514, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T5B58F94B8996878CA2C5D15F4560299411AC6514_H
#ifndef FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#define FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinder
struct  FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493  : public ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F
{
public:
	// Zenject.BindFinalizerWrapper Zenject.FromBinder::<FinalizerWrapper>k__BackingField
	BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * ___U3CFinalizerWrapperU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493, ___U3CFinalizerWrapperU3Ek__BackingField_1)); }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * get_U3CFinalizerWrapperU3Ek__BackingField_1() const { return ___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 ** get_address_of_U3CFinalizerWrapperU3Ek__BackingField_1() { return &___U3CFinalizerWrapperU3Ek__BackingField_1; }
	inline void set_U3CFinalizerWrapperU3Ek__BackingField_1(BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6 * value)
	{
		___U3CFinalizerWrapperU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFinalizerWrapperU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDER_T888ACFF1EB1CD668F1547CE1943F1AAF2B135493_H
#ifndef NAMETRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T2555943414AEF780A26971B185D03F57034C7840_H
#define NAMETRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T2555943414AEF780A26971B185D03F57034C7840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformScopeConditionCopyNonLazyBinder
struct  NameTransformScopeConditionCopyNonLazyBinder_t2555943414AEF780A26971B185D03F57034C7840  : public TransformScopeConditionCopyNonLazyBinder_t5B58F94B8996878CA2C5D15F4560299411AC6514
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMSCOPECONDITIONCOPYNONLAZYBINDER_T2555943414AEF780A26971B185D03F57034C7840_H
#ifndef TRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T3349B38215B2BD24D0052A9101451C27DF89ED55_H
#define TRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T3349B38215B2BD24D0052A9101451C27DF89ED55_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.TransformScopeArgConditionCopyNonLazyBinder
struct  TransformScopeArgConditionCopyNonLazyBinder_t3349B38215B2BD24D0052A9101451C27DF89ED55  : public ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F
{
public:
	// Zenject.GameObjectCreationParameters Zenject.TransformScopeArgConditionCopyNonLazyBinder::<GameObjectInfo>k__BackingField
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ___U3CGameObjectInfoU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TransformScopeArgConditionCopyNonLazyBinder_t3349B38215B2BD24D0052A9101451C27DF89ED55, ___U3CGameObjectInfoU3Ek__BackingField_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get_U3CGameObjectInfoU3Ek__BackingField_1() const { return ___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of_U3CGameObjectInfoU3Ek__BackingField_1() { return &___U3CGameObjectInfoU3Ek__BackingField_1; }
	inline void set_U3CGameObjectInfoU3Ek__BackingField_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		___U3CGameObjectInfoU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGameObjectInfoU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T3349B38215B2BD24D0052A9101451C27DF89ED55_H
#ifndef FROMBINDERNONGENERIC_T3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1_H
#define FROMBINDERNONGENERIC_T3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.FromBinderNonGeneric
struct  FromBinderNonGeneric_t3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1  : public FromBinder_t888ACFF1EB1CD668F1547CE1943F1AAF2B135493
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FROMBINDERNONGENERIC_T3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1_H
#ifndef NAMETRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T651FC34B32C0D8CBA4B3EFCB4D812864908FEA91_H
#define NAMETRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T651FC34B32C0D8CBA4B3EFCB4D812864908FEA91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.NameTransformScopeArgConditionCopyNonLazyBinder
struct  NameTransformScopeArgConditionCopyNonLazyBinder_t651FC34B32C0D8CBA4B3EFCB4D812864908FEA91  : public TransformScopeArgConditionCopyNonLazyBinder_t3349B38215B2BD24D0052A9101451C27DF89ED55
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETRANSFORMSCOPEARGCONDITIONCOPYNONLAZYBINDER_T651FC34B32C0D8CBA4B3EFCB4D812864908FEA91_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6800 = { sizeof (U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6800[3] = 
{
	U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F::get_offset_of_resourcePath_0(),
	U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F::get_offset_of_U3CU3E4__this_1(),
	U3CU3Ec__DisplayClass32_0_t8C3A84B0B18CAFECAD51C1D6DDA926DB5A441A3F::get_offset_of_createNew_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6801 = { sizeof (U3CU3Ec__DisplayClass33_0_t042097CADA14CFF4319E93FD31AE55A924A1321C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6801[1] = 
{
	U3CU3Ec__DisplayClass33_0_t042097CADA14CFF4319E93FD31AE55A924A1321C::get_offset_of_resourcePath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6802 = { sizeof (U3CU3Ec__DisplayClass34_0_tDA1B7F70C3D36ED82ABB9E21594067B97B4ED917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6802[1] = 
{
	U3CU3Ec__DisplayClass34_0_tDA1B7F70C3D36ED82ABB9E21594067B97B4ED917::get_offset_of_method_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6803 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6803[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6804 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6804[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6805 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6805[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6806 = { sizeof (U3CU3Ec__DisplayClass39_0_tF2AE282E4D6A547E2F5A6F93934DCE398C6928F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6806[1] = 
{
	U3CU3Ec__DisplayClass39_0_tF2AE282E4D6A547E2F5A6F93934DCE398C6928F3::get_offset_of_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6807 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6808 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6808[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6809 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6809[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6810 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6810[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6811 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6811[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6812 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6812[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6813 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6813[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6814 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6814[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6815 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6815[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6816 = { sizeof (FromBinderNonGeneric_t3509B89300095EFE71CAC8AC15B8D2A4BDC85AF1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6817 = { sizeof (NameTransformConditionCopyNonLazyBinder_t8ACB78C05213954714A62E1AA18BDF3EACACB8BE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6818 = { sizeof (NameTransformScopeArgConditionCopyNonLazyBinder_t651FC34B32C0D8CBA4B3EFCB4D812864908FEA91), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6819 = { sizeof (NameTransformScopeArgNonLazyBinder_t6B13DA4025DE2DD0F1A0180BF3D14B01B5614DE7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6820 = { sizeof (NameTransformScopeConditionCopyNonLazyBinder_t2555943414AEF780A26971B185D03F57034C7840), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6821 = { sizeof (TransformConditionCopyNonLazyBinder_tDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6821[1] = 
{
	TransformConditionCopyNonLazyBinder_tDFAC1D9A5B5AC28069DC17601AFC45AAB86C2CDC::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6822 = { sizeof (TransformScopeArgConditionCopyNonLazyBinder_t3349B38215B2BD24D0052A9101451C27DF89ED55), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6822[1] = 
{
	TransformScopeArgConditionCopyNonLazyBinder_t3349B38215B2BD24D0052A9101451C27DF89ED55::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6823 = { sizeof (TransformScopeArgNonLazyBinder_t8728EE391A28AA4530CE40D305EB23EEDEEF6341), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6823[1] = 
{
	TransformScopeArgNonLazyBinder_t8728EE391A28AA4530CE40D305EB23EEDEEF6341::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6824 = { sizeof (TransformScopeConditionCopyNonLazyBinder_t5B58F94B8996878CA2C5D15F4560299411AC6514), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6824[1] = 
{
	TransformScopeConditionCopyNonLazyBinder_t5B58F94B8996878CA2C5D15F4560299411AC6514::get_offset_of_U3CGameObjectInfoU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6825 = { sizeof (IdBinder_t368E1D65DA4ADC684F688A9E721B553C0B1AD0F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6825[1] = 
{
	IdBinder_t368E1D65DA4ADC684F688A9E721B553C0B1AD0F4::get_offset_of__bindInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6826 = { sizeof (IdScopeConditionCopyNonLazyBinder_t8AB2C520254742ED74E2273A4B7AC9BF09BA05AE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6827 = { sizeof (NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6827[1] = 
{
	NonLazyBinder_tD353CE02C951C95FD045F6C21516882D489A0B8E::get_offset_of_U3CBindInfoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6828 = { sizeof (ScopeArgConditionCopyNonLazyBinder_tF12679CE42CB0E48D76D73799596B64C1A66F73F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6829 = { sizeof (ScopeArgNonLazyBinder_t5BF897B041A6B79DC7F088CF0A35632CCA74D74F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6830 = { sizeof (ScopeConditionCopyNonLazyBinder_t322A4D615E478DDAB6579F88D71AD0328B8AF13B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6831 = { sizeof (ScopeNonLazyBinder_t0DFE5D51BD6BFEBC496546DF2F74DA58890E8048), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6832 = { sizeof (SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6832[3] = 
{
	SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF::get_offset_of__bindInfo_0(),
	SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF::get_offset_of__finalizerWrapper_1(),
	SubContainerBinder_t6D608228848D682ABBD552A157DF879945304DDF::get_offset_of__subIdentifier_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6833 = { sizeof (ScopeTypes_t2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6833[5] = 
{
	ScopeTypes_t2E352D02B5DF130CECA8A4B6AAA19F07AC61CCB4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6834 = { sizeof (ToChoices_tD5BE8210291AF2D3503FE46815AC2FA615A91E91)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6834[3] = 
{
	ToChoices_tD5BE8210291AF2D3503FE46815AC2FA615A91E91::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6835 = { sizeof (InvalidBindResponses_tA2A16E511A535774CC9D10EA025DA4C5B46E9D24)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6835[3] = 
{
	InvalidBindResponses_tA2A16E511A535774CC9D10EA025DA4C5B46E9D24::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6836 = { sizeof (BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6836[13] = 
{
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CContextInfoU3Ek__BackingField_0(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CRequireExplicitScopeU3Ek__BackingField_1(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CIdentifierU3Ek__BackingField_2(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CContractTypesU3Ek__BackingField_3(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CCopyIntoAllSubContainersU3Ek__BackingField_4(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CInvalidBindResponseU3Ek__BackingField_5(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CNonLazyU3Ek__BackingField_6(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CConditionU3Ek__BackingField_7(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CToChoiceU3Ek__BackingField_8(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CToTypesU3Ek__BackingField_9(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CScopeU3Ek__BackingField_10(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CConcreteIdentifierU3Ek__BackingField_11(),
	BindInfo_t6175BDBC1E67872F3864E2546CE028E508F29AD1::get_offset_of_U3CArgumentsU3Ek__BackingField_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6837 = { sizeof (FactoryBindInfo_t7D4905F5254D3D3734F1659902724612D2D0FA5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6837[2] = 
{
	FactoryBindInfo_t7D4905F5254D3D3734F1659902724612D2D0FA5C::get_offset_of_U3CFactoryTypeU3Ek__BackingField_0(),
	FactoryBindInfo_t7D4905F5254D3D3734F1659902724612D2D0FA5C::get_offset_of_U3CProviderFuncU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6838 = { sizeof (GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4), -1, sizeof(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6838[7] = 
{
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4::get_offset_of_U3CNameU3Ek__BackingField_0(),
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4::get_offset_of_U3CGroupNameU3Ek__BackingField_1(),
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4::get_offset_of_U3CParentTransformU3Ek__BackingField_2(),
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4::get_offset_of_U3CParentTransformGetterU3Ek__BackingField_3(),
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4::get_offset_of_U3CPositionU3Ek__BackingField_4(),
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4::get_offset_of_U3CRotationU3Ek__BackingField_5(),
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4_StaticFields::get_offset_of_Default_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6839 = { sizeof (PoolExpandMethods_t778C4D7478B1DC979FE2B16031F3C6695A2E81D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6839[4] = 
{
	PoolExpandMethods_t778C4D7478B1DC979FE2B16031F3C6695A2E81D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6840 = { sizeof (MemoryPoolBindInfo_t89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6840[2] = 
{
	MemoryPoolBindInfo_t89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E::get_offset_of_U3CExpandMethodU3Ek__BackingField_0(),
	MemoryPoolBindInfo_t89CFCF320DB3D1CADD3DADE2C09D2BF7BF1B631E::get_offset_of_U3CInitialSizeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6841 = { sizeof (BindingUtil_t8A14770ED161D915CEFD9A5AC6006B8BC90A1FA0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6842 = { sizeof (BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6842[1] = 
{
	BindFinalizerWrapper_tB36EA4FC0554CF87C12522841E589B5776DE6CA6::get_offset_of__subFinalizer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6843 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6844 = { sizeof (NullBindingFinalizer_tF7E45F0EBB0FB335628DD396747FF11A1D21A863), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6845 = { sizeof (PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6845[3] = 
{
	PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661::get_offset_of__gameObjectBindInfo_1(),
	PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661::get_offset_of__prefab_2(),
	PrefabBindingFinalizer_t3A03643A17E89E86C2E57B6FFF221C4FC776E661::get_offset_of__providerFactory_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6846 = { sizeof (U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6846[2] = 
{
	U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t9E7836AA27E3F40717DC2CC7451DD6445939CAF3::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6847 = { sizeof (U3CU3Ec__DisplayClass5_1_tC874328E94584FE63035E24794D0E00246DDA60F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6847[2] = 
{
	U3CU3Ec__DisplayClass5_1_tC874328E94584FE63035E24794D0E00246DDA60F::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass5_1_tC874328E94584FE63035E24794D0E00246DDA60F::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6848 = { sizeof (U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6848[2] = 
{
	U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_tFFDB3D8B15B5298E6683CF575E20FCBD79FCC4B7::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6849 = { sizeof (U3CU3Ec__DisplayClass6_1_t89F17D7C00C10CDBC8FEDED3948235F9FF458991), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6849[2] = 
{
	U3CU3Ec__DisplayClass6_1_t89F17D7C00C10CDBC8FEDED3948235F9FF458991::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass6_1_t89F17D7C00C10CDBC8FEDED3948235F9FF458991::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6850 = { sizeof (PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6850[3] = 
{
	PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B::get_offset_of__gameObjectBindInfo_1(),
	PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B::get_offset_of__resourcePath_2(),
	PrefabResourceBindingFinalizer_t354FE39C76EBA51F4EA1DC10B01546CCA4C82B7B::get_offset_of__providerFactory_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6851 = { sizeof (U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6851[2] = 
{
	U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t9722D3DED53E53706BE00C1A1F4D1C80033AD45F::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6852 = { sizeof (U3CU3Ec__DisplayClass5_1_tD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6852[2] = 
{
	U3CU3Ec__DisplayClass5_1_tD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass5_1_tD84C6939F9EE0048FDE2DDCB4867E44364EA7C5B::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6853 = { sizeof (U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6853[2] = 
{
	U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t2812AD4C754CB66BCB46CB11B38B63E87F1A2B05::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6854 = { sizeof (U3CU3Ec__DisplayClass6_1_t2914EA28BBAC7C2030C1A28B980385064460B93E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6854[2] = 
{
	U3CU3Ec__DisplayClass6_1_t2914EA28BBAC7C2030C1A28B980385064460B93E::get_offset_of_prefabCreator_0(),
	U3CU3Ec__DisplayClass6_1_t2914EA28BBAC7C2030C1A28B980385064460B93E::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6855 = { sizeof (ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6855[1] = 
{
	ProviderBindingFinalizer_t1BF2C44159AA661D3E4EA3B7A9465B6E35DC96AD::get_offset_of_U3CBindInfoU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6856 = { sizeof (U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC), -1, sizeof(U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6856[3] = 
{
	U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields::get_offset_of_U3CU3E9__7_0_1(),
	U3CU3Ec_t64FE62B0F92EB0E5888240ABF4BF953D2A0BF2BC_StaticFields::get_offset_of_U3CU3E9__16_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6857 = { sizeof (U3CU3Ec__DisplayClass16_0_t3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6857[2] = 
{
	U3CU3Ec__DisplayClass16_0_t3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69::get_offset_of_providerFunc_0(),
	U3CU3Ec__DisplayClass16_0_t3707C74F9A1E5BC9E1BC86765B53CB85A73C1B69::get_offset_of_container_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6858 = { sizeof (ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6858[3] = 
{
	ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA::get_offset_of__singletonType_1(),
	ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA::get_offset_of__providerFactory_2(),
	ScopableBindingFinalizer_t6F104B7C464DFDAE8F32C5942F74CB0E096073DA::get_offset_of__singletonSpecificId_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6859 = { sizeof (U3CU3Ec__DisplayClass5_0_t671341B83988426E4EF0E328A156524DD033791E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6859[2] = 
{
	U3CU3Ec__DisplayClass5_0_t671341B83988426E4EF0E328A156524DD033791E::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t671341B83988426E4EF0E328A156524DD033791E::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6860 = { sizeof (U3CU3Ec__DisplayClass6_0_tD33DA19F7BDF68403DB522B40F2E13502CAAF94C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6860[2] = 
{
	U3CU3Ec__DisplayClass6_0_tD33DA19F7BDF68403DB522B40F2E13502CAAF94C::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_tD33DA19F7BDF68403DB522B40F2E13502CAAF94C::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6861 = { sizeof (SingleProviderBindingFinalizer_tDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6861[1] = 
{
	SingleProviderBindingFinalizer_tDE90B0B56B5DD8F7F5B8204BA28369BCE5B4EA68::get_offset_of__providerFactory_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6862 = { sizeof (SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6862[2] = 
{
	SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439::get_offset_of__subIdentifier_1(),
	SubContainerInstallerBindingFinalizer_t5D7B30B362E2AA1240C2B9B8AE29DC0E858DD439::get_offset_of__installerType_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6863 = { sizeof (U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6863[2] = 
{
	U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_tF55F5A9B4503D57334B5B99926A3E010BA6990E1::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6864 = { sizeof (U3CU3Ec__DisplayClass5_1_tD196D1B2197FC063E15A307A2C089B00170012F2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6864[2] = 
{
	U3CU3Ec__DisplayClass5_1_tD196D1B2197FC063E15A307A2C089B00170012F2::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_tD196D1B2197FC063E15A307A2C089B00170012F2::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6865 = { sizeof (U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6865[2] = 
{
	U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t33470911219FA31544BD15AB6DFCCC7EBB9F243E::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6866 = { sizeof (U3CU3Ec__DisplayClass6_1_t6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6866[2] = 
{
	U3CU3Ec__DisplayClass6_1_t6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass6_1_t6E8A4252DB96BEB4567AAF41A865EF623FA2DA4F::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6867 = { sizeof (SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6867[2] = 
{
	SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842::get_offset_of__subIdentifier_1(),
	SubContainerMethodBindingFinalizer_t7A90CF0B9B3D2E1B0EA420CCD057AA17E55ED842::get_offset_of__installMethod_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6868 = { sizeof (U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6868[2] = 
{
	U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass4_0_t8B2AB9B8ABD1901900B76FC107F0B812BDF7D11F::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6869 = { sizeof (U3CU3Ec__DisplayClass4_1_t2E3E392C160FD73D6F795057C1D96407D882289E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6869[2] = 
{
	U3CU3Ec__DisplayClass4_1_t2E3E392C160FD73D6F795057C1D96407D882289E::get_offset_of_creator_0(),
	U3CU3Ec__DisplayClass4_1_t2E3E392C160FD73D6F795057C1D96407D882289E::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6870 = { sizeof (U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6870[2] = 
{
	U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t686763593E6D3FAFE37419486DF47CC614BBB89E::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6871 = { sizeof (U3CU3Ec__DisplayClass5_1_t123ABD1B51B7BB69119488CAEFFBB08B57104B58), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6871[2] = 
{
	U3CU3Ec__DisplayClass5_1_t123ABD1B51B7BB69119488CAEFFBB08B57104B58::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_t123ABD1B51B7BB69119488CAEFFBB08B57104B58::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6872 = { sizeof (SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6872[3] = 
{
	SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C::get_offset_of__prefab_1(),
	SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C::get_offset_of__subIdentifier_2(),
	SubContainerPrefabBindingFinalizer_tCBEFDC5A6CE5514659C82111F46E0C68C8CED45C::get_offset_of__gameObjectBindInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6873 = { sizeof (U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6873[2] = 
{
	U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_t57364071B16667E624DD84D49042B299919EDE16::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6874 = { sizeof (U3CU3Ec__DisplayClass5_1_t2CE375160CE52751FA94BBD3257A3DE9FF91EF84), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6874[2] = 
{
	U3CU3Ec__DisplayClass5_1_t2CE375160CE52751FA94BBD3257A3DE9FF91EF84::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_t2CE375160CE52751FA94BBD3257A3DE9FF91EF84::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6875 = { sizeof (U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6875[2] = 
{
	U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_tE95844800F4E141486AB15308F50728AD59B04BB::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6876 = { sizeof (U3CU3Ec__DisplayClass6_1_t7E4FBAEDA265DB02BE0799B830292E45F88C91C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6876[2] = 
{
	U3CU3Ec__DisplayClass6_1_t7E4FBAEDA265DB02BE0799B830292E45F88C91C1::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass6_1_t7E4FBAEDA265DB02BE0799B830292E45F88C91C1::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6877 = { sizeof (SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6877[3] = 
{
	SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F::get_offset_of__resourcePath_1(),
	SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F::get_offset_of__subIdentifier_2(),
	SubContainerPrefabResourceBindingFinalizer_tF73E5148915E7E8AB5FD55128292037C6A6A2A2F::get_offset_of__gameObjectBindInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6878 = { sizeof (U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6878[2] = 
{
	U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass5_0_tBC022BE40EB4177C07DF3DF7C38830398C3B7CF5::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6879 = { sizeof (U3CU3Ec__DisplayClass5_1_t803DC2A04E0D8F018FA6A38075623FF4D2E44D0A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6879[2] = 
{
	U3CU3Ec__DisplayClass5_1_t803DC2A04E0D8F018FA6A38075623FF4D2E44D0A::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass5_1_t803DC2A04E0D8F018FA6A38075623FF4D2E44D0A::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6880 = { sizeof (U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6880[2] = 
{
	U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567::get_offset_of_container_0(),
	U3CU3Ec__DisplayClass6_0_t3A79532E9A95BE473A4D254E6BA0490E00B7A567::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6881 = { sizeof (U3CU3Ec__DisplayClass6_1_t232CADB2592F1A9746D69FBFD2FACC762311748A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6881[2] = 
{
	U3CU3Ec__DisplayClass6_1_t232CADB2592F1A9746D69FBFD2FACC762311748A::get_offset_of_containerCreator_0(),
	U3CU3Ec__DisplayClass6_1_t232CADB2592F1A9746D69FBFD2FACC762311748A::get_offset_of_CSU24U3CU3E8__locals1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6882 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6883 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6883[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6884 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6885 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6885[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6886 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6887 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6887[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6888 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6889 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6889[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6890 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6891 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6891[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6892 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6893 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6893[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6894 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6894[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6895 = { sizeof (GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6895[2] = 
{
	GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1::get_offset_of__container_0(),
	GameObjectFactory_t69C0DC65BD450BDDD99982D281023992E254FDC1::get_offset_of__prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6896 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6897 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6898 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6899 = { 0, 0, 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
