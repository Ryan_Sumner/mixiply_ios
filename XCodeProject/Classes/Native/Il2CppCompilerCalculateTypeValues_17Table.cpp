﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.DtdParser/UndeclaredNotation>
struct Dictionary_2_t916DC1A8D299E7D1846930151D649D79F4D9D5A2;
// System.Collections.Generic.IComparer`1<System.Object>
struct IComparer_1_tFF77EB203CF12E843446A71A6581145AB929D681;
// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName>
struct List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.TextReader
struct TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A;
// System.IO.TextWriter
struct TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.Decoder
struct Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26;
// System.Text.Encoding
struct Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.AttributePSVIInfo
struct AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94;
// System.Xml.AttributePSVIInfo[]
struct AttributePSVIInfoU5BU5D_tB383C48FDFE60597390398272BA52949B7ADE951;
// System.Xml.BitStack
struct BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7;
// System.Xml.CachingEventHandler
struct CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6;
// System.Xml.DomNameTable
struct DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A;
// System.Xml.EmptyEnumerator
struct EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3;
// System.Xml.IDtdDefaultAttributeInfo
struct IDtdDefaultAttributeInfo_tA4B426A7E92054F6EF626DC0E493CA19C4ECB26A;
// System.Xml.IDtdEntityInfo
struct IDtdEntityInfo_t4477A2221D64D9E3DB7F89E82E963BB4858A38D2;
// System.Xml.IDtdParserAdapter
struct IDtdParserAdapter_t3BDE16E68BD0E75DFF1390906C81D067824E1829;
// System.Xml.IDtdParserAdapterWithValidation
struct IDtdParserAdapterWithValidation_tADA45FC6B3FCB0FCD0F19F72E83968F2242A58F8;
// System.Xml.IXmlLineInfo
struct IXmlLineInfo_tD6D8818DFB22D29FC2397C76BA6BFFF54604284A;
// System.Xml.IXmlNamespaceResolver
struct IXmlNamespaceResolver_t252EBD93E225063727450B6A8B4BE94F5F2E8427;
// System.Xml.NameTable/Entry[]
struct EntryU5BU5D_t8E2877E1FE7833A2136987F940F926D3F1683525;
// System.Xml.ReadContentAsBinaryHelper
struct ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797;
// System.Xml.Schema.BaseValidator
struct BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43;
// System.Xml.Schema.IXmlSchemaInfo
struct IXmlSchemaInfo_t2FE768B806BA73C644AEE436491F2C3E04039CF1;
// System.Xml.Schema.Parser
struct Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5;
// System.Xml.Schema.XmlSchemaInfo
struct XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9;
// System.Xml.Schema.XmlSchemaValidator
struct XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C;
// System.Xml.Schema.XmlValueGetter
struct XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF;
// System.Xml.SecureStringHasher
struct SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3;
// System.Xml.ValidatingReaderNodeData
struct ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F;
// System.Xml.ValidatingReaderNodeData[]
struct ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF;
// System.Xml.WriteState[]
struct WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A;
// System.Xml.XmlAttributeCollection
struct XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlImplementation
struct XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF;
// System.Xml.XmlLinkedNode
struct XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E;
// System.Xml.XmlName
struct XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlName[]
struct XmlNameU5BU5D_tC7C9008163B62E5C79C948665D33D4E4DAA495F4;
// System.Xml.XmlNamedNodeMap
struct XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;
// System.Xml.XmlNodeChangedEventArgs
struct XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA;
// System.Xml.XmlNodeChangedEventHandler
struct XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838;
// System.Xml.XmlNodeReaderNavigator
struct XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B;
// System.Xml.XmlNodeReaderNavigator/VirtualAttribute[]
struct VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D;
// System.Xml.XmlParserContext
struct XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279;
// System.Xml.XmlRawWriter
struct XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2;
// System.Xml.XmlRawWriterBase64Encoder
struct XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678;
// System.Xml.XmlReader
struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlTextEncoder
struct XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475;
// System.Xml.XmlTextReaderImpl
struct XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61;
// System.Xml.XmlTextReaderImpl/NodeData
struct NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF;
// System.Xml.XmlTextWriter/Namespace[]
struct NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829;
// System.Xml.XmlTextWriter/State[]
struct StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2;
// System.Xml.XmlTextWriter/TagInfo[]
struct TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910;
// System.Xml.XmlTextWriterBase64Encoder
struct XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E;
// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling
struct ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1;
// System.Xml.XmlWellFormedWriter
struct XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A;
// System.Xml.XmlWellFormedWriter/AttrName[]
struct AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033;
// System.Xml.XmlWellFormedWriter/AttributeValueCache
struct AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA;
// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item[]
struct ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815;
// System.Xml.XmlWellFormedWriter/ElementScope[]
struct ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80;
// System.Xml.XmlWellFormedWriter/Namespace[]
struct NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C;
// System.Xml.XmlWellFormedWriter/State[]
struct StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4;
// System.Xml.XmlWriter
struct XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869;
// System.Xml.XsdCachingReader
struct XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ATTRIBUTEPSVIINFO_T1991F050207B868048BC97BE0C50F6CC9A430C94_H
#define ATTRIBUTEPSVIINFO_T1991F050207B868048BC97BE0C50F6CC9A430C94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.AttributePSVIInfo
struct  AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94  : public RuntimeObject
{
public:
	// System.String System.Xml.AttributePSVIInfo::localName
	String_t* ___localName_0;
	// System.String System.Xml.AttributePSVIInfo::namespaceUri
	String_t* ___namespaceUri_1;
	// System.Object System.Xml.AttributePSVIInfo::typedAttributeValue
	RuntimeObject * ___typedAttributeValue_2;
	// System.Xml.Schema.XmlSchemaInfo System.Xml.AttributePSVIInfo::attributeSchemaInfo
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 * ___attributeSchemaInfo_3;

public:
	inline static int32_t get_offset_of_localName_0() { return static_cast<int32_t>(offsetof(AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94, ___localName_0)); }
	inline String_t* get_localName_0() const { return ___localName_0; }
	inline String_t** get_address_of_localName_0() { return &___localName_0; }
	inline void set_localName_0(String_t* value)
	{
		___localName_0 = value;
		Il2CppCodeGenWriteBarrier((&___localName_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_typedAttributeValue_2() { return static_cast<int32_t>(offsetof(AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94, ___typedAttributeValue_2)); }
	inline RuntimeObject * get_typedAttributeValue_2() const { return ___typedAttributeValue_2; }
	inline RuntimeObject ** get_address_of_typedAttributeValue_2() { return &___typedAttributeValue_2; }
	inline void set_typedAttributeValue_2(RuntimeObject * value)
	{
		___typedAttributeValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___typedAttributeValue_2), value);
	}

	inline static int32_t get_offset_of_attributeSchemaInfo_3() { return static_cast<int32_t>(offsetof(AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94, ___attributeSchemaInfo_3)); }
	inline XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 * get_attributeSchemaInfo_3() const { return ___attributeSchemaInfo_3; }
	inline XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 ** get_address_of_attributeSchemaInfo_3() { return &___attributeSchemaInfo_3; }
	inline void set_attributeSchemaInfo_3(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 * value)
	{
		___attributeSchemaInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeSchemaInfo_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEPSVIINFO_T1991F050207B868048BC97BE0C50F6CC9A430C94_H
#ifndef DOMNAMETABLE_T09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A_H
#define DOMNAMETABLE_T09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DomNameTable
struct  DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A  : public RuntimeObject
{
public:
	// System.Xml.XmlName[] System.Xml.DomNameTable::entries
	XmlNameU5BU5D_tC7C9008163B62E5C79C948665D33D4E4DAA495F4* ___entries_0;
	// System.Int32 System.Xml.DomNameTable::count
	int32_t ___count_1;
	// System.Int32 System.Xml.DomNameTable::mask
	int32_t ___mask_2;
	// System.Xml.XmlDocument System.Xml.DomNameTable::ownerDocument
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___ownerDocument_3;
	// System.Xml.XmlNameTable System.Xml.DomNameTable::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_4;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A, ___entries_0)); }
	inline XmlNameU5BU5D_tC7C9008163B62E5C79C948665D33D4E4DAA495F4* get_entries_0() const { return ___entries_0; }
	inline XmlNameU5BU5D_tC7C9008163B62E5C79C948665D33D4E4DAA495F4** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(XmlNameU5BU5D_tC7C9008163B62E5C79C948665D33D4E4DAA495F4* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A, ___mask_2)); }
	inline int32_t get_mask_2() const { return ___mask_2; }
	inline int32_t* get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(int32_t value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_ownerDocument_3() { return static_cast<int32_t>(offsetof(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A, ___ownerDocument_3)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_ownerDocument_3() const { return ___ownerDocument_3; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_ownerDocument_3() { return &___ownerDocument_3; }
	inline void set_ownerDocument_3(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___ownerDocument_3 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDocument_3), value);
	}

	inline static int32_t get_offset_of_nameTable_4() { return static_cast<int32_t>(offsetof(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A, ___nameTable_4)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_4() const { return ___nameTable_4; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_4() { return &___nameTable_4; }
	inline void set_nameTable_4(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOMNAMETABLE_T09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A_H
#ifndef EMPTYENUMERATOR_T7702FAD652CDD694DC3D136712CA48C48FFB0DC3_H
#define EMPTYENUMERATOR_T7702FAD652CDD694DC3D136712CA48C48FFB0DC3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.EmptyEnumerator
struct  EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYENUMERATOR_T7702FAD652CDD694DC3D136712CA48C48FFB0DC3_H
#ifndef HWSTACK_T2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE_H
#define HWSTACK_T2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.HWStack
struct  HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE  : public RuntimeObject
{
public:
	// System.Object[] System.Xml.HWStack::stack
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___stack_0;
	// System.Int32 System.Xml.HWStack::growthRate
	int32_t ___growthRate_1;
	// System.Int32 System.Xml.HWStack::used
	int32_t ___used_2;
	// System.Int32 System.Xml.HWStack::size
	int32_t ___size_3;
	// System.Int32 System.Xml.HWStack::limit
	int32_t ___limit_4;

public:
	inline static int32_t get_offset_of_stack_0() { return static_cast<int32_t>(offsetof(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE, ___stack_0)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_stack_0() const { return ___stack_0; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_stack_0() { return &___stack_0; }
	inline void set_stack_0(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___stack_0 = value;
		Il2CppCodeGenWriteBarrier((&___stack_0), value);
	}

	inline static int32_t get_offset_of_growthRate_1() { return static_cast<int32_t>(offsetof(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE, ___growthRate_1)); }
	inline int32_t get_growthRate_1() const { return ___growthRate_1; }
	inline int32_t* get_address_of_growthRate_1() { return &___growthRate_1; }
	inline void set_growthRate_1(int32_t value)
	{
		___growthRate_1 = value;
	}

	inline static int32_t get_offset_of_used_2() { return static_cast<int32_t>(offsetof(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE, ___used_2)); }
	inline int32_t get_used_2() const { return ___used_2; }
	inline int32_t* get_address_of_used_2() { return &___used_2; }
	inline void set_used_2(int32_t value)
	{
		___used_2 = value;
	}

	inline static int32_t get_offset_of_size_3() { return static_cast<int32_t>(offsetof(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE, ___size_3)); }
	inline int32_t get_size_3() const { return ___size_3; }
	inline int32_t* get_address_of_size_3() { return &___size_3; }
	inline void set_size_3(int32_t value)
	{
		___size_3 = value;
	}

	inline static int32_t get_offset_of_limit_4() { return static_cast<int32_t>(offsetof(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE, ___limit_4)); }
	inline int32_t get_limit_4() const { return ___limit_4; }
	inline int32_t* get_address_of_limit_4() { return &___limit_4; }
	inline void set_limit_4(int32_t value)
	{
		___limit_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HWSTACK_T2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE_H
#ifndef ENTRY_TF2506228AAB202078CB7A755668744901B207977_H
#define ENTRY_TF2506228AAB202078CB7A755668744901B207977_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable/Entry
struct  Entry_tF2506228AAB202078CB7A755668744901B207977  : public RuntimeObject
{
public:
	// System.String System.Xml.NameTable/Entry::str
	String_t* ___str_0;
	// System.Int32 System.Xml.NameTable/Entry::hashCode
	int32_t ___hashCode_1;
	// System.Xml.NameTable/Entry System.Xml.NameTable/Entry::next
	Entry_tF2506228AAB202078CB7A755668744901B207977 * ___next_2;

public:
	inline static int32_t get_offset_of_str_0() { return static_cast<int32_t>(offsetof(Entry_tF2506228AAB202078CB7A755668744901B207977, ___str_0)); }
	inline String_t* get_str_0() const { return ___str_0; }
	inline String_t** get_address_of_str_0() { return &___str_0; }
	inline void set_str_0(String_t* value)
	{
		___str_0 = value;
		Il2CppCodeGenWriteBarrier((&___str_0), value);
	}

	inline static int32_t get_offset_of_hashCode_1() { return static_cast<int32_t>(offsetof(Entry_tF2506228AAB202078CB7A755668744901B207977, ___hashCode_1)); }
	inline int32_t get_hashCode_1() const { return ___hashCode_1; }
	inline int32_t* get_address_of_hashCode_1() { return &___hashCode_1; }
	inline void set_hashCode_1(int32_t value)
	{
		___hashCode_1 = value;
	}

	inline static int32_t get_offset_of_next_2() { return static_cast<int32_t>(offsetof(Entry_tF2506228AAB202078CB7A755668744901B207977, ___next_2)); }
	inline Entry_tF2506228AAB202078CB7A755668744901B207977 * get_next_2() const { return ___next_2; }
	inline Entry_tF2506228AAB202078CB7A755668744901B207977 ** get_address_of_next_2() { return &___next_2; }
	inline void set_next_2(Entry_tF2506228AAB202078CB7A755668744901B207977 * value)
	{
		___next_2 = value;
		Il2CppCodeGenWriteBarrier((&___next_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTRY_TF2506228AAB202078CB7A755668744901B207977_H
#ifndef POSITIONINFO_TC35A3FA759E0F23D150BB8B774EC0391EA099684_H
#define POSITIONINFO_TC35A3FA759E0F23D150BB8B774EC0391EA099684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.PositionInfo
struct  PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITIONINFO_TC35A3FA759E0F23D150BB8B774EC0391EA099684_H
#ifndef REF_T58DB2E95F0FC9A292628F954DDC3D643DF1B692E_H
#define REF_T58DB2E95F0FC9A292628F954DDC3D643DF1B692E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Ref
struct  Ref_t58DB2E95F0FC9A292628F954DDC3D643DF1B692E  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REF_T58DB2E95F0FC9A292628F954DDC3D643DF1B692E_H
#ifndef XMLCHILDENUMERATOR_TA5E9216BDDC135630E6C1283B8976192A9847ABA_H
#define XMLCHILDENUMERATOR_TA5E9216BDDC135630E6C1283B8976192A9847ABA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChildEnumerator
struct  XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::container
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___container_0;
	// System.Xml.XmlNode System.Xml.XmlChildEnumerator::child
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___child_1;
	// System.Boolean System.Xml.XmlChildEnumerator::isFirst
	bool ___isFirst_2;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA, ___container_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_container_0() const { return ___container_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}

	inline static int32_t get_offset_of_child_1() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA, ___child_1)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_child_1() const { return ___child_1; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_child_1() { return &___child_1; }
	inline void set_child_1(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___child_1 = value;
		Il2CppCodeGenWriteBarrier((&___child_1), value);
	}

	inline static int32_t get_offset_of_isFirst_2() { return static_cast<int32_t>(offsetof(XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA, ___isFirst_2)); }
	inline bool get_isFirst_2() const { return ___isFirst_2; }
	inline bool* get_address_of_isFirst_2() { return &___isFirst_2; }
	inline void set_isFirst_2(bool value)
	{
		___isFirst_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHILDENUMERATOR_TA5E9216BDDC135630E6C1283B8976192A9847ABA_H
#ifndef XMLIMPLEMENTATION_T7C01D70C3943C7AA929C106D360E90E2D576E1EF_H
#define XMLIMPLEMENTATION_T7C01D70C3943C7AA929C106D360E90E2D576E1EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlImplementation
struct  XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.XmlImplementation::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_0;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF, ___nameTable_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLIMPLEMENTATION_T7C01D70C3943C7AA929C106D360E90E2D576E1EF_H
#ifndef XMLLOADER_T8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D_H
#define XMLLOADER_T8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLoader
struct  XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D  : public RuntimeObject
{
public:
	// System.Xml.XmlDocument System.Xml.XmlLoader::doc
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___doc_0;
	// System.Xml.XmlReader System.Xml.XmlLoader::reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___reader_1;
	// System.Boolean System.Xml.XmlLoader::preserveWhitespace
	bool ___preserveWhitespace_2;

public:
	inline static int32_t get_offset_of_doc_0() { return static_cast<int32_t>(offsetof(XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D, ___doc_0)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_doc_0() const { return ___doc_0; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_doc_0() { return &___doc_0; }
	inline void set_doc_0(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___doc_0 = value;
		Il2CppCodeGenWriteBarrier((&___doc_0), value);
	}

	inline static int32_t get_offset_of_reader_1() { return static_cast<int32_t>(offsetof(XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D, ___reader_1)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_reader_1() const { return ___reader_1; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_reader_1() { return &___reader_1; }
	inline void set_reader_1(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___reader_1 = value;
		Il2CppCodeGenWriteBarrier((&___reader_1), value);
	}

	inline static int32_t get_offset_of_preserveWhitespace_2() { return static_cast<int32_t>(offsetof(XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D, ___preserveWhitespace_2)); }
	inline bool get_preserveWhitespace_2() const { return ___preserveWhitespace_2; }
	inline bool* get_address_of_preserveWhitespace_2() { return &___preserveWhitespace_2; }
	inline void set_preserveWhitespace_2(bool value)
	{
		___preserveWhitespace_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLOADER_T8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D_H
#ifndef XMLNAME_T993BD9C827C8B0B6A0B7C49A03F2D14740AC2682_H
#define XMLNAME_T993BD9C827C8B0B6A0B7C49A03F2D14740AC2682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlName
struct  XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682  : public RuntimeObject
{
public:
	// System.String System.Xml.XmlName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlName::localName
	String_t* ___localName_1;
	// System.String System.Xml.XmlName::ns
	String_t* ___ns_2;
	// System.String System.Xml.XmlName::name
	String_t* ___name_3;
	// System.Int32 System.Xml.XmlName::hashCode
	int32_t ___hashCode_4;
	// System.Xml.XmlDocument System.Xml.XmlName::ownerDoc
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___ownerDoc_5;
	// System.Xml.XmlName System.Xml.XmlName::next
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * ___next_6;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_localName_1() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___localName_1)); }
	inline String_t* get_localName_1() const { return ___localName_1; }
	inline String_t** get_address_of_localName_1() { return &___localName_1; }
	inline void set_localName_1(String_t* value)
	{
		___localName_1 = value;
		Il2CppCodeGenWriteBarrier((&___localName_1), value);
	}

	inline static int32_t get_offset_of_ns_2() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___ns_2)); }
	inline String_t* get_ns_2() const { return ___ns_2; }
	inline String_t** get_address_of_ns_2() { return &___ns_2; }
	inline void set_ns_2(String_t* value)
	{
		___ns_2 = value;
		Il2CppCodeGenWriteBarrier((&___ns_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}

	inline static int32_t get_offset_of_hashCode_4() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___hashCode_4)); }
	inline int32_t get_hashCode_4() const { return ___hashCode_4; }
	inline int32_t* get_address_of_hashCode_4() { return &___hashCode_4; }
	inline void set_hashCode_4(int32_t value)
	{
		___hashCode_4 = value;
	}

	inline static int32_t get_offset_of_ownerDoc_5() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___ownerDoc_5)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_ownerDoc_5() const { return ___ownerDoc_5; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_ownerDoc_5() { return &___ownerDoc_5; }
	inline void set_ownerDoc_5(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___ownerDoc_5 = value;
		Il2CppCodeGenWriteBarrier((&___ownerDoc_5), value);
	}

	inline static int32_t get_offset_of_next_6() { return static_cast<int32_t>(offsetof(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682, ___next_6)); }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * get_next_6() const { return ___next_6; }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 ** get_address_of_next_6() { return &___next_6; }
	inline void set_next_6(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * value)
	{
		___next_6 = value;
		Il2CppCodeGenWriteBarrier((&___next_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAME_T993BD9C827C8B0B6A0B7C49A03F2D14740AC2682_H
#ifndef XMLNAMETABLE_T3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6_H
#define XMLNAMETABLE_T3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameTable
struct  XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMETABLE_T3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6_H
#ifndef SINGLEOBJECTENUMERATOR_TA9C95CFC90A374F3FB891F834486863C55883F7A_H
#define SINGLEOBJECTENUMERATOR_TA9C95CFC90A374F3FB891F834486863C55883F7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap/SmallXmlNodeList/SingleObjectEnumerator
struct  SingleObjectEnumerator_tA9C95CFC90A374F3FB891F834486863C55883F7A  : public RuntimeObject
{
public:
	// System.Object System.Xml.XmlNamedNodeMap/SmallXmlNodeList/SingleObjectEnumerator::loneValue
	RuntimeObject * ___loneValue_0;
	// System.Int32 System.Xml.XmlNamedNodeMap/SmallXmlNodeList/SingleObjectEnumerator::position
	int32_t ___position_1;

public:
	inline static int32_t get_offset_of_loneValue_0() { return static_cast<int32_t>(offsetof(SingleObjectEnumerator_tA9C95CFC90A374F3FB891F834486863C55883F7A, ___loneValue_0)); }
	inline RuntimeObject * get_loneValue_0() const { return ___loneValue_0; }
	inline RuntimeObject ** get_address_of_loneValue_0() { return &___loneValue_0; }
	inline void set_loneValue_0(RuntimeObject * value)
	{
		___loneValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___loneValue_0), value);
	}

	inline static int32_t get_offset_of_position_1() { return static_cast<int32_t>(offsetof(SingleObjectEnumerator_tA9C95CFC90A374F3FB891F834486863C55883F7A, ___position_1)); }
	inline int32_t get_position_1() const { return ___position_1; }
	inline int32_t* get_address_of_position_1() { return &___position_1; }
	inline void set_position_1(int32_t value)
	{
		___position_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLEOBJECTENUMERATOR_TA9C95CFC90A374F3FB891F834486863C55883F7A_H
#ifndef XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#define XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_0), value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___hashTable_4)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashTable_4), value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((&___xml_6), value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlNs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#ifndef XMLNODE_T07D70045D843753E4FE8AFE40FD36244E6B6D7FB_H
#define XMLNODE_T07D70045D843753E4FE8AFE40FD36244E6B6D7FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNode
struct  XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNode::parentNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parentNode_0;

public:
	inline static int32_t get_offset_of_parentNode_0() { return static_cast<int32_t>(offsetof(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB, ___parentNode_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_parentNode_0() const { return ___parentNode_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_parentNode_0() { return &___parentNode_0; }
	inline void set_parentNode_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___parentNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODE_T07D70045D843753E4FE8AFE40FD36244E6B6D7FB_H
#ifndef XMLNODELIST_T6A2162EDB563F1707F00C5156460E1073244C8E7_H
#define XMLNODELIST_T6A2162EDB563F1707F00C5156460E1073244C8E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeList
struct  XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODELIST_T6A2162EDB563F1707F00C5156460E1073244C8E7_H
#ifndef XMLNODEREADERNAVIGATOR_T6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B_H
#define XMLNODEREADERNAVIGATOR_T6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReaderNavigator
struct  XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNodeReaderNavigator::curNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___curNode_0;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderNavigator::elemNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___elemNode_1;
	// System.Xml.XmlNode System.Xml.XmlNodeReaderNavigator::logNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___logNode_2;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::attrIndex
	int32_t ___attrIndex_3;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::logAttrIndex
	int32_t ___logAttrIndex_4;
	// System.Xml.XmlNameTable System.Xml.XmlNodeReaderNavigator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_5;
	// System.Xml.XmlDocument System.Xml.XmlNodeReaderNavigator::doc
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___doc_6;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nAttrInd
	int32_t ___nAttrInd_7;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nDeclarationAttrCount
	int32_t ___nDeclarationAttrCount_8;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nDocTypeAttrCount
	int32_t ___nDocTypeAttrCount_9;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nLogLevel
	int32_t ___nLogLevel_10;
	// System.Int32 System.Xml.XmlNodeReaderNavigator::nLogAttrInd
	int32_t ___nLogAttrInd_11;
	// System.Boolean System.Xml.XmlNodeReaderNavigator::bLogOnAttrVal
	bool ___bLogOnAttrVal_12;
	// System.Boolean System.Xml.XmlNodeReaderNavigator::bCreatedOnAttribute
	bool ___bCreatedOnAttribute_13;
	// System.Xml.XmlNodeReaderNavigator/VirtualAttribute[] System.Xml.XmlNodeReaderNavigator::decNodeAttributes
	VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D* ___decNodeAttributes_14;
	// System.Xml.XmlNodeReaderNavigator/VirtualAttribute[] System.Xml.XmlNodeReaderNavigator::docTypeNodeAttributes
	VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D* ___docTypeNodeAttributes_15;
	// System.Boolean System.Xml.XmlNodeReaderNavigator::bOnAttrVal
	bool ___bOnAttrVal_16;

public:
	inline static int32_t get_offset_of_curNode_0() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___curNode_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_curNode_0() const { return ___curNode_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_curNode_0() { return &___curNode_0; }
	inline void set_curNode_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___curNode_0 = value;
		Il2CppCodeGenWriteBarrier((&___curNode_0), value);
	}

	inline static int32_t get_offset_of_elemNode_1() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___elemNode_1)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_elemNode_1() const { return ___elemNode_1; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_elemNode_1() { return &___elemNode_1; }
	inline void set_elemNode_1(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___elemNode_1 = value;
		Il2CppCodeGenWriteBarrier((&___elemNode_1), value);
	}

	inline static int32_t get_offset_of_logNode_2() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___logNode_2)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_logNode_2() const { return ___logNode_2; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_logNode_2() { return &___logNode_2; }
	inline void set_logNode_2(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___logNode_2 = value;
		Il2CppCodeGenWriteBarrier((&___logNode_2), value);
	}

	inline static int32_t get_offset_of_attrIndex_3() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___attrIndex_3)); }
	inline int32_t get_attrIndex_3() const { return ___attrIndex_3; }
	inline int32_t* get_address_of_attrIndex_3() { return &___attrIndex_3; }
	inline void set_attrIndex_3(int32_t value)
	{
		___attrIndex_3 = value;
	}

	inline static int32_t get_offset_of_logAttrIndex_4() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___logAttrIndex_4)); }
	inline int32_t get_logAttrIndex_4() const { return ___logAttrIndex_4; }
	inline int32_t* get_address_of_logAttrIndex_4() { return &___logAttrIndex_4; }
	inline void set_logAttrIndex_4(int32_t value)
	{
		___logAttrIndex_4 = value;
	}

	inline static int32_t get_offset_of_nameTable_5() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___nameTable_5)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_5() const { return ___nameTable_5; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_5() { return &___nameTable_5; }
	inline void set_nameTable_5(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_5), value);
	}

	inline static int32_t get_offset_of_doc_6() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___doc_6)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_doc_6() const { return ___doc_6; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_doc_6() { return &___doc_6; }
	inline void set_doc_6(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___doc_6 = value;
		Il2CppCodeGenWriteBarrier((&___doc_6), value);
	}

	inline static int32_t get_offset_of_nAttrInd_7() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___nAttrInd_7)); }
	inline int32_t get_nAttrInd_7() const { return ___nAttrInd_7; }
	inline int32_t* get_address_of_nAttrInd_7() { return &___nAttrInd_7; }
	inline void set_nAttrInd_7(int32_t value)
	{
		___nAttrInd_7 = value;
	}

	inline static int32_t get_offset_of_nDeclarationAttrCount_8() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___nDeclarationAttrCount_8)); }
	inline int32_t get_nDeclarationAttrCount_8() const { return ___nDeclarationAttrCount_8; }
	inline int32_t* get_address_of_nDeclarationAttrCount_8() { return &___nDeclarationAttrCount_8; }
	inline void set_nDeclarationAttrCount_8(int32_t value)
	{
		___nDeclarationAttrCount_8 = value;
	}

	inline static int32_t get_offset_of_nDocTypeAttrCount_9() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___nDocTypeAttrCount_9)); }
	inline int32_t get_nDocTypeAttrCount_9() const { return ___nDocTypeAttrCount_9; }
	inline int32_t* get_address_of_nDocTypeAttrCount_9() { return &___nDocTypeAttrCount_9; }
	inline void set_nDocTypeAttrCount_9(int32_t value)
	{
		___nDocTypeAttrCount_9 = value;
	}

	inline static int32_t get_offset_of_nLogLevel_10() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___nLogLevel_10)); }
	inline int32_t get_nLogLevel_10() const { return ___nLogLevel_10; }
	inline int32_t* get_address_of_nLogLevel_10() { return &___nLogLevel_10; }
	inline void set_nLogLevel_10(int32_t value)
	{
		___nLogLevel_10 = value;
	}

	inline static int32_t get_offset_of_nLogAttrInd_11() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___nLogAttrInd_11)); }
	inline int32_t get_nLogAttrInd_11() const { return ___nLogAttrInd_11; }
	inline int32_t* get_address_of_nLogAttrInd_11() { return &___nLogAttrInd_11; }
	inline void set_nLogAttrInd_11(int32_t value)
	{
		___nLogAttrInd_11 = value;
	}

	inline static int32_t get_offset_of_bLogOnAttrVal_12() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___bLogOnAttrVal_12)); }
	inline bool get_bLogOnAttrVal_12() const { return ___bLogOnAttrVal_12; }
	inline bool* get_address_of_bLogOnAttrVal_12() { return &___bLogOnAttrVal_12; }
	inline void set_bLogOnAttrVal_12(bool value)
	{
		___bLogOnAttrVal_12 = value;
	}

	inline static int32_t get_offset_of_bCreatedOnAttribute_13() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___bCreatedOnAttribute_13)); }
	inline bool get_bCreatedOnAttribute_13() const { return ___bCreatedOnAttribute_13; }
	inline bool* get_address_of_bCreatedOnAttribute_13() { return &___bCreatedOnAttribute_13; }
	inline void set_bCreatedOnAttribute_13(bool value)
	{
		___bCreatedOnAttribute_13 = value;
	}

	inline static int32_t get_offset_of_decNodeAttributes_14() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___decNodeAttributes_14)); }
	inline VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D* get_decNodeAttributes_14() const { return ___decNodeAttributes_14; }
	inline VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D** get_address_of_decNodeAttributes_14() { return &___decNodeAttributes_14; }
	inline void set_decNodeAttributes_14(VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D* value)
	{
		___decNodeAttributes_14 = value;
		Il2CppCodeGenWriteBarrier((&___decNodeAttributes_14), value);
	}

	inline static int32_t get_offset_of_docTypeNodeAttributes_15() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___docTypeNodeAttributes_15)); }
	inline VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D* get_docTypeNodeAttributes_15() const { return ___docTypeNodeAttributes_15; }
	inline VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D** get_address_of_docTypeNodeAttributes_15() { return &___docTypeNodeAttributes_15; }
	inline void set_docTypeNodeAttributes_15(VirtualAttributeU5BU5D_tD492912725E8803C8FB0373BB4C873AE60693E8D* value)
	{
		___docTypeNodeAttributes_15 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeNodeAttributes_15), value);
	}

	inline static int32_t get_offset_of_bOnAttrVal_16() { return static_cast<int32_t>(offsetof(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B, ___bOnAttrVal_16)); }
	inline bool get_bOnAttrVal_16() const { return ___bOnAttrVal_16; }
	inline bool* get_address_of_bOnAttrVal_16() { return &___bOnAttrVal_16; }
	inline void set_bOnAttrVal_16(bool value)
	{
		___bOnAttrVal_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEREADERNAVIGATOR_T6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B_H
#ifndef XMLREADER_T13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_H
#define XMLREADER_T13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlReader
struct  XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB  : public RuntimeObject
{
public:

public:
};

struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields
{
public:
	// System.UInt32 System.Xml.XmlReader::IsTextualNodeBitmap
	uint32_t ___IsTextualNodeBitmap_0;
	// System.UInt32 System.Xml.XmlReader::CanReadContentAsBitmap
	uint32_t ___CanReadContentAsBitmap_1;
	// System.UInt32 System.Xml.XmlReader::HasValueBitmap
	uint32_t ___HasValueBitmap_2;

public:
	inline static int32_t get_offset_of_IsTextualNodeBitmap_0() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___IsTextualNodeBitmap_0)); }
	inline uint32_t get_IsTextualNodeBitmap_0() const { return ___IsTextualNodeBitmap_0; }
	inline uint32_t* get_address_of_IsTextualNodeBitmap_0() { return &___IsTextualNodeBitmap_0; }
	inline void set_IsTextualNodeBitmap_0(uint32_t value)
	{
		___IsTextualNodeBitmap_0 = value;
	}

	inline static int32_t get_offset_of_CanReadContentAsBitmap_1() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___CanReadContentAsBitmap_1)); }
	inline uint32_t get_CanReadContentAsBitmap_1() const { return ___CanReadContentAsBitmap_1; }
	inline uint32_t* get_address_of_CanReadContentAsBitmap_1() { return &___CanReadContentAsBitmap_1; }
	inline void set_CanReadContentAsBitmap_1(uint32_t value)
	{
		___CanReadContentAsBitmap_1 = value;
	}

	inline static int32_t get_offset_of_HasValueBitmap_2() { return static_cast<int32_t>(offsetof(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_StaticFields, ___HasValueBitmap_2)); }
	inline uint32_t get_HasValueBitmap_2() const { return ___HasValueBitmap_2; }
	inline uint32_t* get_address_of_HasValueBitmap_2() { return &___HasValueBitmap_2; }
	inline void set_HasValueBitmap_2(uint32_t value)
	{
		___HasValueBitmap_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLREADER_T13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB_H
#ifndef DTDDEFAULTATTRIBUTEINFOTONODEDATACOMPARER_T0B9FEF7A80832980C3E32D47BE835964E8B423B7_H
#define DTDDEFAULTATTRIBUTEINFOTONODEDATACOMPARER_T0B9FEF7A80832980C3E32D47BE835964E8B423B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/DtdDefaultAttributeInfoToNodeDataComparer
struct  DtdDefaultAttributeInfoToNodeDataComparer_t0B9FEF7A80832980C3E32D47BE835964E8B423B7  : public RuntimeObject
{
public:

public:
};

struct DtdDefaultAttributeInfoToNodeDataComparer_t0B9FEF7A80832980C3E32D47BE835964E8B423B7_StaticFields
{
public:
	// System.Collections.Generic.IComparer`1<System.Object> System.Xml.XmlTextReaderImpl/DtdDefaultAttributeInfoToNodeDataComparer::s_instance
	RuntimeObject* ___s_instance_0;

public:
	inline static int32_t get_offset_of_s_instance_0() { return static_cast<int32_t>(offsetof(DtdDefaultAttributeInfoToNodeDataComparer_t0B9FEF7A80832980C3E32D47BE835964E8B423B7_StaticFields, ___s_instance_0)); }
	inline RuntimeObject* get_s_instance_0() const { return ___s_instance_0; }
	inline RuntimeObject** get_address_of_s_instance_0() { return &___s_instance_0; }
	inline void set_s_instance_0(RuntimeObject* value)
	{
		___s_instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDDEFAULTATTRIBUTEINFOTONODEDATACOMPARER_T0B9FEF7A80832980C3E32D47BE835964E8B423B7_H
#ifndef DTDPARSERPROXY_TF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7_H
#define DTDPARSERPROXY_TF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/DtdParserProxy
struct  DtdParserProxy_tF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7  : public RuntimeObject
{
public:
	// System.Xml.XmlTextReaderImpl System.Xml.XmlTextReaderImpl/DtdParserProxy::reader
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * ___reader_0;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(DtdParserProxy_tF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7, ___reader_0)); }
	inline XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * get_reader_0() const { return ___reader_0; }
	inline XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARSERPROXY_TF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7_H
#ifndef VALIDATIONEVENTHANDLING_TB979AC71B85B7B96E85CF917D8F33BEC867F1EC1_H
#define VALIDATIONEVENTHANDLING_TB979AC71B85B7B96E85CF917D8F33BEC867F1EC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling
struct  ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1  : public RuntimeObject
{
public:
	// System.Xml.XmlValidatingReaderImpl System.Xml.XmlValidatingReaderImpl/ValidationEventHandling::reader
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___reader_0;
	// System.Xml.Schema.ValidationEventHandler System.Xml.XmlValidatingReaderImpl/ValidationEventHandling::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_1;

public:
	inline static int32_t get_offset_of_reader_0() { return static_cast<int32_t>(offsetof(ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1, ___reader_0)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_reader_0() const { return ___reader_0; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_reader_0() { return &___reader_0; }
	inline void set_reader_0(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___reader_0 = value;
		Il2CppCodeGenWriteBarrier((&___reader_0), value);
	}

	inline static int32_t get_offset_of_eventHandler_1() { return static_cast<int32_t>(offsetof(ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1, ___eventHandler_1)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_1() const { return ___eventHandler_1; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_1() { return &___eventHandler_1; }
	inline void set_eventHandler_1(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTHANDLING_TB979AC71B85B7B96E85CF917D8F33BEC867F1EC1_H
#ifndef ATTRIBUTEVALUECACHE_T810F1A90B8C60C2C91358AE02E7CEF1D975245FA_H
#define ATTRIBUTEVALUECACHE_T810F1A90B8C60C2C91358AE02E7CEF1D975245FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache
struct  AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA  : public RuntimeObject
{
public:
	// System.Text.StringBuilder System.Xml.XmlWellFormedWriter/AttributeValueCache::stringValue
	StringBuilder_t * ___stringValue_0;
	// System.String System.Xml.XmlWellFormedWriter/AttributeValueCache::singleStringValue
	String_t* ___singleStringValue_1;
	// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item[] System.Xml.XmlWellFormedWriter/AttributeValueCache::items
	ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815* ___items_2;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache::firstItem
	int32_t ___firstItem_3;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache::lastItem
	int32_t ___lastItem_4;

public:
	inline static int32_t get_offset_of_stringValue_0() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___stringValue_0)); }
	inline StringBuilder_t * get_stringValue_0() const { return ___stringValue_0; }
	inline StringBuilder_t ** get_address_of_stringValue_0() { return &___stringValue_0; }
	inline void set_stringValue_0(StringBuilder_t * value)
	{
		___stringValue_0 = value;
		Il2CppCodeGenWriteBarrier((&___stringValue_0), value);
	}

	inline static int32_t get_offset_of_singleStringValue_1() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___singleStringValue_1)); }
	inline String_t* get_singleStringValue_1() const { return ___singleStringValue_1; }
	inline String_t** get_address_of_singleStringValue_1() { return &___singleStringValue_1; }
	inline void set_singleStringValue_1(String_t* value)
	{
		___singleStringValue_1 = value;
		Il2CppCodeGenWriteBarrier((&___singleStringValue_1), value);
	}

	inline static int32_t get_offset_of_items_2() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___items_2)); }
	inline ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815* get_items_2() const { return ___items_2; }
	inline ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815** get_address_of_items_2() { return &___items_2; }
	inline void set_items_2(ItemU5BU5D_t666DD62B00301367DBA22FFEC913EAA74166D815* value)
	{
		___items_2 = value;
		Il2CppCodeGenWriteBarrier((&___items_2), value);
	}

	inline static int32_t get_offset_of_firstItem_3() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___firstItem_3)); }
	inline int32_t get_firstItem_3() const { return ___firstItem_3; }
	inline int32_t* get_address_of_firstItem_3() { return &___firstItem_3; }
	inline void set_firstItem_3(int32_t value)
	{
		___firstItem_3 = value;
	}

	inline static int32_t get_offset_of_lastItem_4() { return static_cast<int32_t>(offsetof(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA, ___lastItem_4)); }
	inline int32_t get_lastItem_4() const { return ___lastItem_4; }
	inline int32_t* get_address_of_lastItem_4() { return &___lastItem_4; }
	inline void set_lastItem_4(int32_t value)
	{
		___lastItem_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEVALUECACHE_T810F1A90B8C60C2C91358AE02E7CEF1D975245FA_H
#ifndef BUFFERCHUNK_TA67064620A574CA90D827C2A5516F6A2986DF4C3_H
#define BUFFERCHUNK_TA67064620A574CA90D827C2A5516F6A2986DF4C3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk
struct  BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3  : public RuntimeObject
{
public:
	// System.Char[] System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk::buffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___buffer_0;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk::index
	int32_t ___index_1;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache/BufferChunk::count
	int32_t ___count_2;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3, ___buffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_buffer_0() const { return ___buffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUFFERCHUNK_TA67064620A574CA90D827C2A5516F6A2986DF4C3_H
#ifndef NAMESPACERESOLVERPROXY_TC483EA09DCE72B15A815CF58FE8DC0FDD237C452_H
#define NAMESPACERESOLVERPROXY_TC483EA09DCE72B15A815CF58FE8DC0FDD237C452_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/NamespaceResolverProxy
struct  NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452  : public RuntimeObject
{
public:
	// System.Xml.XmlWellFormedWriter System.Xml.XmlWellFormedWriter/NamespaceResolverProxy::wfWriter
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A * ___wfWriter_0;

public:
	inline static int32_t get_offset_of_wfWriter_0() { return static_cast<int32_t>(offsetof(NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452, ___wfWriter_0)); }
	inline XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A * get_wfWriter_0() const { return ___wfWriter_0; }
	inline XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A ** get_address_of_wfWriter_0() { return &___wfWriter_0; }
	inline void set_wfWriter_0(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A * value)
	{
		___wfWriter_0 = value;
		Il2CppCodeGenWriteBarrier((&___wfWriter_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACERESOLVERPROXY_TC483EA09DCE72B15A815CF58FE8DC0FDD237C452_H
#ifndef XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#define XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriter
struct  XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869  : public RuntimeObject
{
public:
	// System.Char[] System.Xml.XmlWriter::writeNodeBuffer
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___writeNodeBuffer_0;

public:
	inline static int32_t get_offset_of_writeNodeBuffer_0() { return static_cast<int32_t>(offsetof(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869, ___writeNodeBuffer_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_writeNodeBuffer_0() const { return ___writeNodeBuffer_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_writeNodeBuffer_0() { return &___writeNodeBuffer_0; }
	inline void set_writeNodeBuffer_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___writeNodeBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___writeNodeBuffer_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITER_T4FAF83E5244FC8F339B19D481C348ACA1510E869_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef LINEINFO_T7E3D50496C7BA51B84D485D0A30B9006943544E5_H
#define LINEINFO_T7E3D50496C7BA51B84D485D0A30B9006943544E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.LineInfo
struct  LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 
{
public:
	// System.Int32 System.Xml.LineInfo::lineNo
	int32_t ___lineNo_0;
	// System.Int32 System.Xml.LineInfo::linePos
	int32_t ___linePos_1;

public:
	inline static int32_t get_offset_of_lineNo_0() { return static_cast<int32_t>(offsetof(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5, ___lineNo_0)); }
	inline int32_t get_lineNo_0() const { return ___lineNo_0; }
	inline int32_t* get_address_of_lineNo_0() { return &___lineNo_0; }
	inline void set_lineNo_0(int32_t value)
	{
		___lineNo_0 = value;
	}

	inline static int32_t get_offset_of_linePos_1() { return static_cast<int32_t>(offsetof(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5, ___linePos_1)); }
	inline int32_t get_linePos_1() const { return ___linePos_1; }
	inline int32_t* get_address_of_linePos_1() { return &___linePos_1; }
	inline void set_linePos_1(int32_t value)
	{
		___linePos_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFO_T7E3D50496C7BA51B84D485D0A30B9006943544E5_H
#ifndef NAMETABLE_TB2F4359686539290B81EB39DCB1828BE81B11C8C_H
#define NAMETABLE_TB2F4359686539290B81EB39DCB1828BE81B11C8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NameTable
struct  NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C  : public XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6
{
public:
	// System.Xml.NameTable/Entry[] System.Xml.NameTable::entries
	EntryU5BU5D_t8E2877E1FE7833A2136987F940F926D3F1683525* ___entries_0;
	// System.Int32 System.Xml.NameTable::count
	int32_t ___count_1;
	// System.Int32 System.Xml.NameTable::mask
	int32_t ___mask_2;
	// System.Int32 System.Xml.NameTable::hashCodeRandomizer
	int32_t ___hashCodeRandomizer_3;

public:
	inline static int32_t get_offset_of_entries_0() { return static_cast<int32_t>(offsetof(NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C, ___entries_0)); }
	inline EntryU5BU5D_t8E2877E1FE7833A2136987F940F926D3F1683525* get_entries_0() const { return ___entries_0; }
	inline EntryU5BU5D_t8E2877E1FE7833A2136987F940F926D3F1683525** get_address_of_entries_0() { return &___entries_0; }
	inline void set_entries_0(EntryU5BU5D_t8E2877E1FE7833A2136987F940F926D3F1683525* value)
	{
		___entries_0 = value;
		Il2CppCodeGenWriteBarrier((&___entries_0), value);
	}

	inline static int32_t get_offset_of_count_1() { return static_cast<int32_t>(offsetof(NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C, ___count_1)); }
	inline int32_t get_count_1() const { return ___count_1; }
	inline int32_t* get_address_of_count_1() { return &___count_1; }
	inline void set_count_1(int32_t value)
	{
		___count_1 = value;
	}

	inline static int32_t get_offset_of_mask_2() { return static_cast<int32_t>(offsetof(NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C, ___mask_2)); }
	inline int32_t get_mask_2() const { return ___mask_2; }
	inline int32_t* get_address_of_mask_2() { return &___mask_2; }
	inline void set_mask_2(int32_t value)
	{
		___mask_2 = value;
	}

	inline static int32_t get_offset_of_hashCodeRandomizer_3() { return static_cast<int32_t>(offsetof(NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C, ___hashCodeRandomizer_3)); }
	inline int32_t get_hashCodeRandomizer_3() const { return ___hashCodeRandomizer_3; }
	inline int32_t* get_address_of_hashCodeRandomizer_3() { return &___hashCodeRandomizer_3; }
	inline void set_hashCodeRandomizer_3(int32_t value)
	{
		___hashCodeRandomizer_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMETABLE_TB2F4359686539290B81EB39DCB1828BE81B11C8C_H
#ifndef READERPOSITIONINFO_T835B819542967DE0AA809FE5423E59D9F4283EB9_H
#define READERPOSITIONINFO_T835B819542967DE0AA809FE5423E59D9F4283EB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReaderPositionInfo
struct  ReaderPositionInfo_t835B819542967DE0AA809FE5423E59D9F4283EB9  : public PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684
{
public:
	// System.Xml.IXmlLineInfo System.Xml.ReaderPositionInfo::lineInfo
	RuntimeObject* ___lineInfo_0;

public:
	inline static int32_t get_offset_of_lineInfo_0() { return static_cast<int32_t>(offsetof(ReaderPositionInfo_t835B819542967DE0AA809FE5423E59D9F4283EB9, ___lineInfo_0)); }
	inline RuntimeObject* get_lineInfo_0() const { return ___lineInfo_0; }
	inline RuntimeObject** get_address_of_lineInfo_0() { return &___lineInfo_0; }
	inline void set_lineInfo_0(RuntimeObject* value)
	{
		___lineInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READERPOSITIONINFO_T835B819542967DE0AA809FE5423E59D9F4283EB9_H
#ifndef XMLATTRIBUTE_TEAB5F066D1D6965D6528617BD89826AE7114DEFA_H
#define XMLATTRIBUTE_TEAB5F066D1D6965D6528617BD89826AE7114DEFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttribute
struct  XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlName System.Xml.XmlAttribute::name
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * ___name_1;
	// System.Xml.XmlLinkedNode System.Xml.XmlAttribute::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_2;

public:
	inline static int32_t get_offset_of_name_1() { return static_cast<int32_t>(offsetof(XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA, ___name_1)); }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * get_name_1() const { return ___name_1; }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 ** get_address_of_name_1() { return &___name_1; }
	inline void set_name_1(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * value)
	{
		___name_1 = value;
		Il2CppCodeGenWriteBarrier((&___name_1), value);
	}

	inline static int32_t get_offset_of_lastChild_2() { return static_cast<int32_t>(offsetof(XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA, ___lastChild_2)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_2() const { return ___lastChild_2; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_2() { return &___lastChild_2; }
	inline void set_lastChild_2(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_2 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTE_TEAB5F066D1D6965D6528617BD89826AE7114DEFA_H
#ifndef XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#define XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9, ___charProperties_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifndef XMLCHILDNODES_TEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4_H
#define XMLCHILDNODES_TEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlChildNodes
struct  XmlChildNodes_tEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4  : public XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7
{
public:
	// System.Xml.XmlNode System.Xml.XmlChildNodes::container
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___container_0;

public:
	inline static int32_t get_offset_of_container_0() { return static_cast<int32_t>(offsetof(XmlChildNodes_tEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4, ___container_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_container_0() const { return ___container_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_container_0() { return &___container_0; }
	inline void set_container_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___container_0 = value;
		Il2CppCodeGenWriteBarrier((&___container_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHILDNODES_TEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4_H
#ifndef XMLDOCUMENT_T448325D04430147AF19F2955BD6A5F1551003C97_H
#define XMLDOCUMENT_T448325D04430147AF19F2955BD6A5F1551003C97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocument
struct  XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlImplementation System.Xml.XmlDocument::implementation
	XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF * ___implementation_1;
	// System.Xml.DomNameTable System.Xml.XmlDocument::domNameTable
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A * ___domNameTable_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlDocument::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_3;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocument::entities
	XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * ___entities_4;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIdMap
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___htElementIdMap_5;
	// System.Collections.Hashtable System.Xml.XmlDocument::htElementIDAttrDecl
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___htElementIDAttrDecl_6;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocument::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.Schema.XmlSchemaSet System.Xml.XmlDocument::schemas
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * ___schemas_8;
	// System.Boolean System.Xml.XmlDocument::reportValidity
	bool ___reportValidity_9;
	// System.Boolean System.Xml.XmlDocument::actualLoadingStatus
	bool ___actualLoadingStatus_10;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertingDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeInsertingDelegate_11;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeInsertedDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeInsertedDelegate_12;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovingDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeRemovingDelegate_13;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeRemovedDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeRemovedDelegate_14;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangingDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeChangingDelegate_15;
	// System.Xml.XmlNodeChangedEventHandler System.Xml.XmlDocument::onNodeChangedDelegate
	XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * ___onNodeChangedDelegate_16;
	// System.Boolean System.Xml.XmlDocument::fEntRefNodesPresent
	bool ___fEntRefNodesPresent_17;
	// System.Boolean System.Xml.XmlDocument::fCDataNodesPresent
	bool ___fCDataNodesPresent_18;
	// System.Boolean System.Xml.XmlDocument::preserveWhitespace
	bool ___preserveWhitespace_19;
	// System.Boolean System.Xml.XmlDocument::isLoading
	bool ___isLoading_20;
	// System.String System.Xml.XmlDocument::strDocumentName
	String_t* ___strDocumentName_21;
	// System.String System.Xml.XmlDocument::strDocumentFragmentName
	String_t* ___strDocumentFragmentName_22;
	// System.String System.Xml.XmlDocument::strCommentName
	String_t* ___strCommentName_23;
	// System.String System.Xml.XmlDocument::strTextName
	String_t* ___strTextName_24;
	// System.String System.Xml.XmlDocument::strCDataSectionName
	String_t* ___strCDataSectionName_25;
	// System.String System.Xml.XmlDocument::strEntityName
	String_t* ___strEntityName_26;
	// System.String System.Xml.XmlDocument::strID
	String_t* ___strID_27;
	// System.String System.Xml.XmlDocument::strXmlns
	String_t* ___strXmlns_28;
	// System.String System.Xml.XmlDocument::strXml
	String_t* ___strXml_29;
	// System.String System.Xml.XmlDocument::strSpace
	String_t* ___strSpace_30;
	// System.String System.Xml.XmlDocument::strLang
	String_t* ___strLang_31;
	// System.String System.Xml.XmlDocument::strEmpty
	String_t* ___strEmpty_32;
	// System.String System.Xml.XmlDocument::strNonSignificantWhitespaceName
	String_t* ___strNonSignificantWhitespaceName_33;
	// System.String System.Xml.XmlDocument::strSignificantWhitespaceName
	String_t* ___strSignificantWhitespaceName_34;
	// System.String System.Xml.XmlDocument::strReservedXmlns
	String_t* ___strReservedXmlns_35;
	// System.String System.Xml.XmlDocument::strReservedXml
	String_t* ___strReservedXml_36;
	// System.String System.Xml.XmlDocument::baseURI
	String_t* ___baseURI_37;
	// System.Xml.XmlResolver System.Xml.XmlDocument::resolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___resolver_38;
	// System.Boolean System.Xml.XmlDocument::bSetResolver
	bool ___bSetResolver_39;
	// System.Object System.Xml.XmlDocument::objLock
	RuntimeObject * ___objLock_40;

public:
	inline static int32_t get_offset_of_implementation_1() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___implementation_1)); }
	inline XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF * get_implementation_1() const { return ___implementation_1; }
	inline XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF ** get_address_of_implementation_1() { return &___implementation_1; }
	inline void set_implementation_1(XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF * value)
	{
		___implementation_1 = value;
		Il2CppCodeGenWriteBarrier((&___implementation_1), value);
	}

	inline static int32_t get_offset_of_domNameTable_2() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___domNameTable_2)); }
	inline DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A * get_domNameTable_2() const { return ___domNameTable_2; }
	inline DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A ** get_address_of_domNameTable_2() { return &___domNameTable_2; }
	inline void set_domNameTable_2(DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A * value)
	{
		___domNameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___domNameTable_2), value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___lastChild_3)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_3), value);
	}

	inline static int32_t get_offset_of_entities_4() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___entities_4)); }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * get_entities_4() const { return ___entities_4; }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 ** get_address_of_entities_4() { return &___entities_4; }
	inline void set_entities_4(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * value)
	{
		___entities_4 = value;
		Il2CppCodeGenWriteBarrier((&___entities_4), value);
	}

	inline static int32_t get_offset_of_htElementIdMap_5() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___htElementIdMap_5)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_htElementIdMap_5() const { return ___htElementIdMap_5; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_htElementIdMap_5() { return &___htElementIdMap_5; }
	inline void set_htElementIdMap_5(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___htElementIdMap_5 = value;
		Il2CppCodeGenWriteBarrier((&___htElementIdMap_5), value);
	}

	inline static int32_t get_offset_of_htElementIDAttrDecl_6() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___htElementIDAttrDecl_6)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_htElementIDAttrDecl_6() const { return ___htElementIDAttrDecl_6; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_htElementIDAttrDecl_6() { return &___htElementIDAttrDecl_6; }
	inline void set_htElementIDAttrDecl_6(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___htElementIDAttrDecl_6 = value;
		Il2CppCodeGenWriteBarrier((&___htElementIDAttrDecl_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_schemas_8() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___schemas_8)); }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * get_schemas_8() const { return ___schemas_8; }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F ** get_address_of_schemas_8() { return &___schemas_8; }
	inline void set_schemas_8(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * value)
	{
		___schemas_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemas_8), value);
	}

	inline static int32_t get_offset_of_reportValidity_9() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___reportValidity_9)); }
	inline bool get_reportValidity_9() const { return ___reportValidity_9; }
	inline bool* get_address_of_reportValidity_9() { return &___reportValidity_9; }
	inline void set_reportValidity_9(bool value)
	{
		___reportValidity_9 = value;
	}

	inline static int32_t get_offset_of_actualLoadingStatus_10() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___actualLoadingStatus_10)); }
	inline bool get_actualLoadingStatus_10() const { return ___actualLoadingStatus_10; }
	inline bool* get_address_of_actualLoadingStatus_10() { return &___actualLoadingStatus_10; }
	inline void set_actualLoadingStatus_10(bool value)
	{
		___actualLoadingStatus_10 = value;
	}

	inline static int32_t get_offset_of_onNodeInsertingDelegate_11() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeInsertingDelegate_11)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeInsertingDelegate_11() const { return ___onNodeInsertingDelegate_11; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeInsertingDelegate_11() { return &___onNodeInsertingDelegate_11; }
	inline void set_onNodeInsertingDelegate_11(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeInsertingDelegate_11 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeInsertingDelegate_11), value);
	}

	inline static int32_t get_offset_of_onNodeInsertedDelegate_12() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeInsertedDelegate_12)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeInsertedDelegate_12() const { return ___onNodeInsertedDelegate_12; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeInsertedDelegate_12() { return &___onNodeInsertedDelegate_12; }
	inline void set_onNodeInsertedDelegate_12(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeInsertedDelegate_12 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeInsertedDelegate_12), value);
	}

	inline static int32_t get_offset_of_onNodeRemovingDelegate_13() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeRemovingDelegate_13)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeRemovingDelegate_13() const { return ___onNodeRemovingDelegate_13; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeRemovingDelegate_13() { return &___onNodeRemovingDelegate_13; }
	inline void set_onNodeRemovingDelegate_13(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeRemovingDelegate_13 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeRemovingDelegate_13), value);
	}

	inline static int32_t get_offset_of_onNodeRemovedDelegate_14() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeRemovedDelegate_14)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeRemovedDelegate_14() const { return ___onNodeRemovedDelegate_14; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeRemovedDelegate_14() { return &___onNodeRemovedDelegate_14; }
	inline void set_onNodeRemovedDelegate_14(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeRemovedDelegate_14 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeRemovedDelegate_14), value);
	}

	inline static int32_t get_offset_of_onNodeChangingDelegate_15() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeChangingDelegate_15)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeChangingDelegate_15() const { return ___onNodeChangingDelegate_15; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeChangingDelegate_15() { return &___onNodeChangingDelegate_15; }
	inline void set_onNodeChangingDelegate_15(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeChangingDelegate_15 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeChangingDelegate_15), value);
	}

	inline static int32_t get_offset_of_onNodeChangedDelegate_16() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___onNodeChangedDelegate_16)); }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * get_onNodeChangedDelegate_16() const { return ___onNodeChangedDelegate_16; }
	inline XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 ** get_address_of_onNodeChangedDelegate_16() { return &___onNodeChangedDelegate_16; }
	inline void set_onNodeChangedDelegate_16(XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838 * value)
	{
		___onNodeChangedDelegate_16 = value;
		Il2CppCodeGenWriteBarrier((&___onNodeChangedDelegate_16), value);
	}

	inline static int32_t get_offset_of_fEntRefNodesPresent_17() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___fEntRefNodesPresent_17)); }
	inline bool get_fEntRefNodesPresent_17() const { return ___fEntRefNodesPresent_17; }
	inline bool* get_address_of_fEntRefNodesPresent_17() { return &___fEntRefNodesPresent_17; }
	inline void set_fEntRefNodesPresent_17(bool value)
	{
		___fEntRefNodesPresent_17 = value;
	}

	inline static int32_t get_offset_of_fCDataNodesPresent_18() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___fCDataNodesPresent_18)); }
	inline bool get_fCDataNodesPresent_18() const { return ___fCDataNodesPresent_18; }
	inline bool* get_address_of_fCDataNodesPresent_18() { return &___fCDataNodesPresent_18; }
	inline void set_fCDataNodesPresent_18(bool value)
	{
		___fCDataNodesPresent_18 = value;
	}

	inline static int32_t get_offset_of_preserveWhitespace_19() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___preserveWhitespace_19)); }
	inline bool get_preserveWhitespace_19() const { return ___preserveWhitespace_19; }
	inline bool* get_address_of_preserveWhitespace_19() { return &___preserveWhitespace_19; }
	inline void set_preserveWhitespace_19(bool value)
	{
		___preserveWhitespace_19 = value;
	}

	inline static int32_t get_offset_of_isLoading_20() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___isLoading_20)); }
	inline bool get_isLoading_20() const { return ___isLoading_20; }
	inline bool* get_address_of_isLoading_20() { return &___isLoading_20; }
	inline void set_isLoading_20(bool value)
	{
		___isLoading_20 = value;
	}

	inline static int32_t get_offset_of_strDocumentName_21() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strDocumentName_21)); }
	inline String_t* get_strDocumentName_21() const { return ___strDocumentName_21; }
	inline String_t** get_address_of_strDocumentName_21() { return &___strDocumentName_21; }
	inline void set_strDocumentName_21(String_t* value)
	{
		___strDocumentName_21 = value;
		Il2CppCodeGenWriteBarrier((&___strDocumentName_21), value);
	}

	inline static int32_t get_offset_of_strDocumentFragmentName_22() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strDocumentFragmentName_22)); }
	inline String_t* get_strDocumentFragmentName_22() const { return ___strDocumentFragmentName_22; }
	inline String_t** get_address_of_strDocumentFragmentName_22() { return &___strDocumentFragmentName_22; }
	inline void set_strDocumentFragmentName_22(String_t* value)
	{
		___strDocumentFragmentName_22 = value;
		Il2CppCodeGenWriteBarrier((&___strDocumentFragmentName_22), value);
	}

	inline static int32_t get_offset_of_strCommentName_23() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strCommentName_23)); }
	inline String_t* get_strCommentName_23() const { return ___strCommentName_23; }
	inline String_t** get_address_of_strCommentName_23() { return &___strCommentName_23; }
	inline void set_strCommentName_23(String_t* value)
	{
		___strCommentName_23 = value;
		Il2CppCodeGenWriteBarrier((&___strCommentName_23), value);
	}

	inline static int32_t get_offset_of_strTextName_24() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strTextName_24)); }
	inline String_t* get_strTextName_24() const { return ___strTextName_24; }
	inline String_t** get_address_of_strTextName_24() { return &___strTextName_24; }
	inline void set_strTextName_24(String_t* value)
	{
		___strTextName_24 = value;
		Il2CppCodeGenWriteBarrier((&___strTextName_24), value);
	}

	inline static int32_t get_offset_of_strCDataSectionName_25() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strCDataSectionName_25)); }
	inline String_t* get_strCDataSectionName_25() const { return ___strCDataSectionName_25; }
	inline String_t** get_address_of_strCDataSectionName_25() { return &___strCDataSectionName_25; }
	inline void set_strCDataSectionName_25(String_t* value)
	{
		___strCDataSectionName_25 = value;
		Il2CppCodeGenWriteBarrier((&___strCDataSectionName_25), value);
	}

	inline static int32_t get_offset_of_strEntityName_26() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strEntityName_26)); }
	inline String_t* get_strEntityName_26() const { return ___strEntityName_26; }
	inline String_t** get_address_of_strEntityName_26() { return &___strEntityName_26; }
	inline void set_strEntityName_26(String_t* value)
	{
		___strEntityName_26 = value;
		Il2CppCodeGenWriteBarrier((&___strEntityName_26), value);
	}

	inline static int32_t get_offset_of_strID_27() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strID_27)); }
	inline String_t* get_strID_27() const { return ___strID_27; }
	inline String_t** get_address_of_strID_27() { return &___strID_27; }
	inline void set_strID_27(String_t* value)
	{
		___strID_27 = value;
		Il2CppCodeGenWriteBarrier((&___strID_27), value);
	}

	inline static int32_t get_offset_of_strXmlns_28() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strXmlns_28)); }
	inline String_t* get_strXmlns_28() const { return ___strXmlns_28; }
	inline String_t** get_address_of_strXmlns_28() { return &___strXmlns_28; }
	inline void set_strXmlns_28(String_t* value)
	{
		___strXmlns_28 = value;
		Il2CppCodeGenWriteBarrier((&___strXmlns_28), value);
	}

	inline static int32_t get_offset_of_strXml_29() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strXml_29)); }
	inline String_t* get_strXml_29() const { return ___strXml_29; }
	inline String_t** get_address_of_strXml_29() { return &___strXml_29; }
	inline void set_strXml_29(String_t* value)
	{
		___strXml_29 = value;
		Il2CppCodeGenWriteBarrier((&___strXml_29), value);
	}

	inline static int32_t get_offset_of_strSpace_30() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strSpace_30)); }
	inline String_t* get_strSpace_30() const { return ___strSpace_30; }
	inline String_t** get_address_of_strSpace_30() { return &___strSpace_30; }
	inline void set_strSpace_30(String_t* value)
	{
		___strSpace_30 = value;
		Il2CppCodeGenWriteBarrier((&___strSpace_30), value);
	}

	inline static int32_t get_offset_of_strLang_31() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strLang_31)); }
	inline String_t* get_strLang_31() const { return ___strLang_31; }
	inline String_t** get_address_of_strLang_31() { return &___strLang_31; }
	inline void set_strLang_31(String_t* value)
	{
		___strLang_31 = value;
		Il2CppCodeGenWriteBarrier((&___strLang_31), value);
	}

	inline static int32_t get_offset_of_strEmpty_32() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strEmpty_32)); }
	inline String_t* get_strEmpty_32() const { return ___strEmpty_32; }
	inline String_t** get_address_of_strEmpty_32() { return &___strEmpty_32; }
	inline void set_strEmpty_32(String_t* value)
	{
		___strEmpty_32 = value;
		Il2CppCodeGenWriteBarrier((&___strEmpty_32), value);
	}

	inline static int32_t get_offset_of_strNonSignificantWhitespaceName_33() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strNonSignificantWhitespaceName_33)); }
	inline String_t* get_strNonSignificantWhitespaceName_33() const { return ___strNonSignificantWhitespaceName_33; }
	inline String_t** get_address_of_strNonSignificantWhitespaceName_33() { return &___strNonSignificantWhitespaceName_33; }
	inline void set_strNonSignificantWhitespaceName_33(String_t* value)
	{
		___strNonSignificantWhitespaceName_33 = value;
		Il2CppCodeGenWriteBarrier((&___strNonSignificantWhitespaceName_33), value);
	}

	inline static int32_t get_offset_of_strSignificantWhitespaceName_34() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strSignificantWhitespaceName_34)); }
	inline String_t* get_strSignificantWhitespaceName_34() const { return ___strSignificantWhitespaceName_34; }
	inline String_t** get_address_of_strSignificantWhitespaceName_34() { return &___strSignificantWhitespaceName_34; }
	inline void set_strSignificantWhitespaceName_34(String_t* value)
	{
		___strSignificantWhitespaceName_34 = value;
		Il2CppCodeGenWriteBarrier((&___strSignificantWhitespaceName_34), value);
	}

	inline static int32_t get_offset_of_strReservedXmlns_35() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strReservedXmlns_35)); }
	inline String_t* get_strReservedXmlns_35() const { return ___strReservedXmlns_35; }
	inline String_t** get_address_of_strReservedXmlns_35() { return &___strReservedXmlns_35; }
	inline void set_strReservedXmlns_35(String_t* value)
	{
		___strReservedXmlns_35 = value;
		Il2CppCodeGenWriteBarrier((&___strReservedXmlns_35), value);
	}

	inline static int32_t get_offset_of_strReservedXml_36() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___strReservedXml_36)); }
	inline String_t* get_strReservedXml_36() const { return ___strReservedXml_36; }
	inline String_t** get_address_of_strReservedXml_36() { return &___strReservedXml_36; }
	inline void set_strReservedXml_36(String_t* value)
	{
		___strReservedXml_36 = value;
		Il2CppCodeGenWriteBarrier((&___strReservedXml_36), value);
	}

	inline static int32_t get_offset_of_baseURI_37() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___baseURI_37)); }
	inline String_t* get_baseURI_37() const { return ___baseURI_37; }
	inline String_t** get_address_of_baseURI_37() { return &___baseURI_37; }
	inline void set_baseURI_37(String_t* value)
	{
		___baseURI_37 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_37), value);
	}

	inline static int32_t get_offset_of_resolver_38() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___resolver_38)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_resolver_38() const { return ___resolver_38; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_resolver_38() { return &___resolver_38; }
	inline void set_resolver_38(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___resolver_38 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_38), value);
	}

	inline static int32_t get_offset_of_bSetResolver_39() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___bSetResolver_39)); }
	inline bool get_bSetResolver_39() const { return ___bSetResolver_39; }
	inline bool* get_address_of_bSetResolver_39() { return &___bSetResolver_39; }
	inline void set_bSetResolver_39(bool value)
	{
		___bSetResolver_39 = value;
	}

	inline static int32_t get_offset_of_objLock_40() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97, ___objLock_40)); }
	inline RuntimeObject * get_objLock_40() const { return ___objLock_40; }
	inline RuntimeObject ** get_address_of_objLock_40() { return &___objLock_40; }
	inline void set_objLock_40(RuntimeObject * value)
	{
		___objLock_40 = value;
		Il2CppCodeGenWriteBarrier((&___objLock_40), value);
	}
};

struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields
{
public:
	// System.Xml.EmptyEnumerator System.Xml.XmlDocument::EmptyEnumerator
	EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 * ___EmptyEnumerator_41;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::NotKnownSchemaInfo
	RuntimeObject* ___NotKnownSchemaInfo_42;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::ValidSchemaInfo
	RuntimeObject* ___ValidSchemaInfo_43;
	// System.Xml.Schema.IXmlSchemaInfo System.Xml.XmlDocument::InvalidSchemaInfo
	RuntimeObject* ___InvalidSchemaInfo_44;

public:
	inline static int32_t get_offset_of_EmptyEnumerator_41() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___EmptyEnumerator_41)); }
	inline EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 * get_EmptyEnumerator_41() const { return ___EmptyEnumerator_41; }
	inline EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 ** get_address_of_EmptyEnumerator_41() { return &___EmptyEnumerator_41; }
	inline void set_EmptyEnumerator_41(EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3 * value)
	{
		___EmptyEnumerator_41 = value;
		Il2CppCodeGenWriteBarrier((&___EmptyEnumerator_41), value);
	}

	inline static int32_t get_offset_of_NotKnownSchemaInfo_42() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___NotKnownSchemaInfo_42)); }
	inline RuntimeObject* get_NotKnownSchemaInfo_42() const { return ___NotKnownSchemaInfo_42; }
	inline RuntimeObject** get_address_of_NotKnownSchemaInfo_42() { return &___NotKnownSchemaInfo_42; }
	inline void set_NotKnownSchemaInfo_42(RuntimeObject* value)
	{
		___NotKnownSchemaInfo_42 = value;
		Il2CppCodeGenWriteBarrier((&___NotKnownSchemaInfo_42), value);
	}

	inline static int32_t get_offset_of_ValidSchemaInfo_43() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___ValidSchemaInfo_43)); }
	inline RuntimeObject* get_ValidSchemaInfo_43() const { return ___ValidSchemaInfo_43; }
	inline RuntimeObject** get_address_of_ValidSchemaInfo_43() { return &___ValidSchemaInfo_43; }
	inline void set_ValidSchemaInfo_43(RuntimeObject* value)
	{
		___ValidSchemaInfo_43 = value;
		Il2CppCodeGenWriteBarrier((&___ValidSchemaInfo_43), value);
	}

	inline static int32_t get_offset_of_InvalidSchemaInfo_44() { return static_cast<int32_t>(offsetof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields, ___InvalidSchemaInfo_44)); }
	inline RuntimeObject* get_InvalidSchemaInfo_44() const { return ___InvalidSchemaInfo_44; }
	inline RuntimeObject** get_address_of_InvalidSchemaInfo_44() { return &___InvalidSchemaInfo_44; }
	inline void set_InvalidSchemaInfo_44(RuntimeObject* value)
	{
		___InvalidSchemaInfo_44 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidSchemaInfo_44), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENT_T448325D04430147AF19F2955BD6A5F1551003C97_H
#ifndef XMLDOCUMENTFRAGMENT_TF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D_H
#define XMLDOCUMENTFRAGMENT_TF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentFragment
struct  XmlDocumentFragment_tF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlDocumentFragment::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_1;

public:
	inline static int32_t get_offset_of_lastChild_1() { return static_cast<int32_t>(offsetof(XmlDocumentFragment_tF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D, ___lastChild_1)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_1() const { return ___lastChild_1; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_1() { return &___lastChild_1; }
	inline void set_lastChild_1(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_1 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTFRAGMENT_TF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D_H
#ifndef XMLENTITY_T8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA_H
#define XMLENTITY_T8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntity
struct  XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.String System.Xml.XmlEntity::publicId
	String_t* ___publicId_1;
	// System.String System.Xml.XmlEntity::systemId
	String_t* ___systemId_2;
	// System.String System.Xml.XmlEntity::notationName
	String_t* ___notationName_3;
	// System.String System.Xml.XmlEntity::name
	String_t* ___name_4;
	// System.String System.Xml.XmlEntity::unparsedReplacementStr
	String_t* ___unparsedReplacementStr_5;
	// System.String System.Xml.XmlEntity::baseURI
	String_t* ___baseURI_6;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntity::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_7;
	// System.Boolean System.Xml.XmlEntity::childrenFoliating
	bool ___childrenFoliating_8;

public:
	inline static int32_t get_offset_of_publicId_1() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___publicId_1)); }
	inline String_t* get_publicId_1() const { return ___publicId_1; }
	inline String_t** get_address_of_publicId_1() { return &___publicId_1; }
	inline void set_publicId_1(String_t* value)
	{
		___publicId_1 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_1), value);
	}

	inline static int32_t get_offset_of_systemId_2() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___systemId_2)); }
	inline String_t* get_systemId_2() const { return ___systemId_2; }
	inline String_t** get_address_of_systemId_2() { return &___systemId_2; }
	inline void set_systemId_2(String_t* value)
	{
		___systemId_2 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_2), value);
	}

	inline static int32_t get_offset_of_notationName_3() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___notationName_3)); }
	inline String_t* get_notationName_3() const { return ___notationName_3; }
	inline String_t** get_address_of_notationName_3() { return &___notationName_3; }
	inline void set_notationName_3(String_t* value)
	{
		___notationName_3 = value;
		Il2CppCodeGenWriteBarrier((&___notationName_3), value);
	}

	inline static int32_t get_offset_of_name_4() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___name_4)); }
	inline String_t* get_name_4() const { return ___name_4; }
	inline String_t** get_address_of_name_4() { return &___name_4; }
	inline void set_name_4(String_t* value)
	{
		___name_4 = value;
		Il2CppCodeGenWriteBarrier((&___name_4), value);
	}

	inline static int32_t get_offset_of_unparsedReplacementStr_5() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___unparsedReplacementStr_5)); }
	inline String_t* get_unparsedReplacementStr_5() const { return ___unparsedReplacementStr_5; }
	inline String_t** get_address_of_unparsedReplacementStr_5() { return &___unparsedReplacementStr_5; }
	inline void set_unparsedReplacementStr_5(String_t* value)
	{
		___unparsedReplacementStr_5 = value;
		Il2CppCodeGenWriteBarrier((&___unparsedReplacementStr_5), value);
	}

	inline static int32_t get_offset_of_baseURI_6() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___baseURI_6)); }
	inline String_t* get_baseURI_6() const { return ___baseURI_6; }
	inline String_t** get_address_of_baseURI_6() { return &___baseURI_6; }
	inline void set_baseURI_6(String_t* value)
	{
		___baseURI_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_6), value);
	}

	inline static int32_t get_offset_of_lastChild_7() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___lastChild_7)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_7() const { return ___lastChild_7; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_7() { return &___lastChild_7; }
	inline void set_lastChild_7(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_7 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_7), value);
	}

	inline static int32_t get_offset_of_childrenFoliating_8() { return static_cast<int32_t>(offsetof(XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA, ___childrenFoliating_8)); }
	inline bool get_childrenFoliating_8() const { return ___childrenFoliating_8; }
	inline bool* get_address_of_childrenFoliating_8() { return &___childrenFoliating_8; }
	inline void set_childrenFoliating_8(bool value)
	{
		___childrenFoliating_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITY_T8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA_H
#ifndef XMLLINKEDNODE_T4F76C8580C2E6D2908D88AC84A86060FA9289A0E_H
#define XMLLINKEDNODE_T4F76C8580C2E6D2908D88AC84A86060FA9289A0E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlLinkedNode
struct  XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.Xml.XmlLinkedNode System.Xml.XmlLinkedNode::next
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___next_1;

public:
	inline static int32_t get_offset_of_next_1() { return static_cast<int32_t>(offsetof(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E, ___next_1)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_next_1() const { return ___next_1; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_next_1() { return &___next_1; }
	inline void set_next_1(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___next_1 = value;
		Il2CppCodeGenWriteBarrier((&___next_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLLINKEDNODE_T4F76C8580C2E6D2908D88AC84A86060FA9289A0E_H
#ifndef XMLNAMEEX_T33130AC6936DEC05C2496BAA31DDB0867F4FD8DE_H
#define XMLNAMEEX_T33130AC6936DEC05C2496BAA31DDB0867F4FD8DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNameEx
struct  XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE  : public XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682
{
public:
	// System.Byte System.Xml.XmlNameEx::flags
	uint8_t ___flags_7;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.XmlNameEx::memberType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___memberType_8;
	// System.Xml.Schema.XmlSchemaType System.Xml.XmlNameEx::schemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___schemaType_9;
	// System.Object System.Xml.XmlNameEx::decl
	RuntimeObject * ___decl_10;

public:
	inline static int32_t get_offset_of_flags_7() { return static_cast<int32_t>(offsetof(XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE, ___flags_7)); }
	inline uint8_t get_flags_7() const { return ___flags_7; }
	inline uint8_t* get_address_of_flags_7() { return &___flags_7; }
	inline void set_flags_7(uint8_t value)
	{
		___flags_7 = value;
	}

	inline static int32_t get_offset_of_memberType_8() { return static_cast<int32_t>(offsetof(XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE, ___memberType_8)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_memberType_8() const { return ___memberType_8; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_memberType_8() { return &___memberType_8; }
	inline void set_memberType_8(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___memberType_8 = value;
		Il2CppCodeGenWriteBarrier((&___memberType_8), value);
	}

	inline static int32_t get_offset_of_schemaType_9() { return static_cast<int32_t>(offsetof(XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE, ___schemaType_9)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_schemaType_9() const { return ___schemaType_9; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_schemaType_9() { return &___schemaType_9; }
	inline void set_schemaType_9(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___schemaType_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_9), value);
	}

	inline static int32_t get_offset_of_decl_10() { return static_cast<int32_t>(offsetof(XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE, ___decl_10)); }
	inline RuntimeObject * get_decl_10() const { return ___decl_10; }
	inline RuntimeObject ** get_address_of_decl_10() { return &___decl_10; }
	inline void set_decl_10(RuntimeObject * value)
	{
		___decl_10 = value;
		Il2CppCodeGenWriteBarrier((&___decl_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEEX_T33130AC6936DEC05C2496BAA31DDB0867F4FD8DE_H
#ifndef SMALLXMLNODELIST_T962D7A66CF19950FE6DFA9476903952B76844A1E_H
#define SMALLXMLNODELIST_T962D7A66CF19950FE6DFA9476903952B76844A1E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct  SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E 
{
public:
	// System.Object System.Xml.XmlNamedNodeMap/SmallXmlNodeList::field
	RuntimeObject * ___field_0;

public:
	inline static int32_t get_offset_of_field_0() { return static_cast<int32_t>(offsetof(SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E, ___field_0)); }
	inline RuntimeObject * get_field_0() const { return ___field_0; }
	inline RuntimeObject ** get_address_of_field_0() { return &___field_0; }
	inline void set_field_0(RuntimeObject * value)
	{
		___field_0 = value;
		Il2CppCodeGenWriteBarrier((&___field_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E_marshaled_pinvoke
{
	Il2CppIUnknown* ___field_0;
};
// Native definition for COM marshalling of System.Xml.XmlNamedNodeMap/SmallXmlNodeList
struct SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E_marshaled_com
{
	Il2CppIUnknown* ___field_0;
};
#endif // SMALLXMLNODELIST_T962D7A66CF19950FE6DFA9476903952B76844A1E_H
#ifndef VIRTUALATTRIBUTE_T3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465_H
#define VIRTUALATTRIBUTE_T3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReaderNavigator/VirtualAttribute
struct  VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465 
{
public:
	// System.String System.Xml.XmlNodeReaderNavigator/VirtualAttribute::name
	String_t* ___name_0;
	// System.String System.Xml.XmlNodeReaderNavigator/VirtualAttribute::value
	String_t* ___value_1;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465, ___value_1)); }
	inline String_t* get_value_1() const { return ___value_1; }
	inline String_t** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(String_t* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlNodeReaderNavigator/VirtualAttribute
struct VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465_marshaled_pinvoke
{
	char* ___name_0;
	char* ___value_1;
};
// Native definition for COM marshalling of System.Xml.XmlNodeReaderNavigator/VirtualAttribute
struct VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___value_1;
};
#endif // VIRTUALATTRIBUTE_T3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465_H
#ifndef XMLNOTATION_TA1B86454CE48EBA5498A774B06535CEC10ABBF53_H
#define XMLNOTATION_TA1B86454CE48EBA5498A774B06535CEC10ABBF53_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNotation
struct  XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53  : public XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB
{
public:
	// System.String System.Xml.XmlNotation::publicId
	String_t* ___publicId_1;
	// System.String System.Xml.XmlNotation::systemId
	String_t* ___systemId_2;
	// System.String System.Xml.XmlNotation::name
	String_t* ___name_3;

public:
	inline static int32_t get_offset_of_publicId_1() { return static_cast<int32_t>(offsetof(XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53, ___publicId_1)); }
	inline String_t* get_publicId_1() const { return ___publicId_1; }
	inline String_t** get_address_of_publicId_1() { return &___publicId_1; }
	inline void set_publicId_1(String_t* value)
	{
		___publicId_1 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_1), value);
	}

	inline static int32_t get_offset_of_systemId_2() { return static_cast<int32_t>(offsetof(XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53, ___systemId_2)); }
	inline String_t* get_systemId_2() const { return ___systemId_2; }
	inline String_t** get_address_of_systemId_2() { return &___systemId_2; }
	inline void set_systemId_2(String_t* value)
	{
		___systemId_2 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_2), value);
	}

	inline static int32_t get_offset_of_name_3() { return static_cast<int32_t>(offsetof(XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53, ___name_3)); }
	inline String_t* get_name_3() const { return ___name_3; }
	inline String_t** get_address_of_name_3() { return &___name_3; }
	inline void set_name_3(String_t* value)
	{
		___name_3 = value;
		Il2CppCodeGenWriteBarrier((&___name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNOTATION_TA1B86454CE48EBA5498A774B06535CEC10ABBF53_H
#ifndef XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#define XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlRawWriter
struct  XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlRawWriterBase64Encoder System.Xml.XmlRawWriter::base64Encoder
	XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * ___base64Encoder_1;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlRawWriter::resolver
	RuntimeObject* ___resolver_2;

public:
	inline static int32_t get_offset_of_base64Encoder_1() { return static_cast<int32_t>(offsetof(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2, ___base64Encoder_1)); }
	inline XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * get_base64Encoder_1() const { return ___base64Encoder_1; }
	inline XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 ** get_address_of_base64Encoder_1() { return &___base64Encoder_1; }
	inline void set_base64Encoder_1(XmlRawWriterBase64Encoder_tD74EC123571C47ABE46E0B6CCE61964E30695678 * value)
	{
		___base64Encoder_1 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_1), value);
	}

	inline static int32_t get_offset_of_resolver_2() { return static_cast<int32_t>(offsetof(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2, ___resolver_2)); }
	inline RuntimeObject* get_resolver_2() const { return ___resolver_2; }
	inline RuntimeObject** get_address_of_resolver_2() { return &___resolver_2; }
	inline void set_resolver_2(RuntimeObject* value)
	{
		___resolver_2 = value;
		Il2CppCodeGenWriteBarrier((&___resolver_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLRAWWRITER_T96129C955F861EC1DDA80EEABE73BB794D617FF2_H
#ifndef NONAMESPACEMANAGER_T11014200BFBB3974A5E72A75867B9730C7D7E883_H
#define NONAMESPACEMANAGER_T11014200BFBB3974A5E72A75867B9730C7D7E883_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/NoNamespaceManager
struct  NoNamespaceManager_t11014200BFBB3974A5E72A75867B9730C7D7E883  : public XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NONAMESPACEMANAGER_T11014200BFBB3974A5E72A75867B9730C7D7E883_H
#ifndef PARSINGSTATE_TE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_H
#define PARSINGSTATE_TE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/ParsingState
struct  ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2 
{
public:
	// System.Char[] System.Xml.XmlTextReaderImpl/ParsingState::chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___chars_0;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::charPos
	int32_t ___charPos_1;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::charsUsed
	int32_t ___charsUsed_2;
	// System.Text.Encoding System.Xml.XmlTextReaderImpl/ParsingState::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::appendMode
	bool ___appendMode_4;
	// System.IO.Stream System.Xml.XmlTextReaderImpl/ParsingState::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Text.Decoder System.Xml.XmlTextReaderImpl/ParsingState::decoder
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_6;
	// System.Byte[] System.Xml.XmlTextReaderImpl/ParsingState::bytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes_7;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::bytePos
	int32_t ___bytePos_8;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::bytesUsed
	int32_t ___bytesUsed_9;
	// System.IO.TextReader System.Xml.XmlTextReaderImpl/ParsingState::textReader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___textReader_10;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::lineNo
	int32_t ___lineNo_11;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::lineStartPos
	int32_t ___lineStartPos_12;
	// System.String System.Xml.XmlTextReaderImpl/ParsingState::baseUriStr
	String_t* ___baseUriStr_13;
	// System.Uri System.Xml.XmlTextReaderImpl/ParsingState::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_14;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::isEof
	bool ___isEof_15;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::isStreamEof
	bool ___isStreamEof_16;
	// System.Xml.IDtdEntityInfo System.Xml.XmlTextReaderImpl/ParsingState::entity
	RuntimeObject* ___entity_17;
	// System.Int32 System.Xml.XmlTextReaderImpl/ParsingState::entityId
	int32_t ___entityId_18;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::eolNormalized
	bool ___eolNormalized_19;
	// System.Boolean System.Xml.XmlTextReaderImpl/ParsingState::entityResolvedManually
	bool ___entityResolvedManually_20;

public:
	inline static int32_t get_offset_of_chars_0() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___chars_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_chars_0() const { return ___chars_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_chars_0() { return &___chars_0; }
	inline void set_chars_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___chars_0), value);
	}

	inline static int32_t get_offset_of_charPos_1() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___charPos_1)); }
	inline int32_t get_charPos_1() const { return ___charPos_1; }
	inline int32_t* get_address_of_charPos_1() { return &___charPos_1; }
	inline void set_charPos_1(int32_t value)
	{
		___charPos_1 = value;
	}

	inline static int32_t get_offset_of_charsUsed_2() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___charsUsed_2)); }
	inline int32_t get_charsUsed_2() const { return ___charsUsed_2; }
	inline int32_t* get_address_of_charsUsed_2() { return &___charsUsed_2; }
	inline void set_charsUsed_2(int32_t value)
	{
		___charsUsed_2 = value;
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_appendMode_4() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___appendMode_4)); }
	inline bool get_appendMode_4() const { return ___appendMode_4; }
	inline bool* get_address_of_appendMode_4() { return &___appendMode_4; }
	inline void set_appendMode_4(bool value)
	{
		___appendMode_4 = value;
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_decoder_6() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___decoder_6)); }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * get_decoder_6() const { return ___decoder_6; }
	inline Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 ** get_address_of_decoder_6() { return &___decoder_6; }
	inline void set_decoder_6(Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * value)
	{
		___decoder_6 = value;
		Il2CppCodeGenWriteBarrier((&___decoder_6), value);
	}

	inline static int32_t get_offset_of_bytes_7() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___bytes_7)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bytes_7() const { return ___bytes_7; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bytes_7() { return &___bytes_7; }
	inline void set_bytes_7(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bytes_7 = value;
		Il2CppCodeGenWriteBarrier((&___bytes_7), value);
	}

	inline static int32_t get_offset_of_bytePos_8() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___bytePos_8)); }
	inline int32_t get_bytePos_8() const { return ___bytePos_8; }
	inline int32_t* get_address_of_bytePos_8() { return &___bytePos_8; }
	inline void set_bytePos_8(int32_t value)
	{
		___bytePos_8 = value;
	}

	inline static int32_t get_offset_of_bytesUsed_9() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___bytesUsed_9)); }
	inline int32_t get_bytesUsed_9() const { return ___bytesUsed_9; }
	inline int32_t* get_address_of_bytesUsed_9() { return &___bytesUsed_9; }
	inline void set_bytesUsed_9(int32_t value)
	{
		___bytesUsed_9 = value;
	}

	inline static int32_t get_offset_of_textReader_10() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___textReader_10)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_textReader_10() const { return ___textReader_10; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_textReader_10() { return &___textReader_10; }
	inline void set_textReader_10(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___textReader_10 = value;
		Il2CppCodeGenWriteBarrier((&___textReader_10), value);
	}

	inline static int32_t get_offset_of_lineNo_11() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___lineNo_11)); }
	inline int32_t get_lineNo_11() const { return ___lineNo_11; }
	inline int32_t* get_address_of_lineNo_11() { return &___lineNo_11; }
	inline void set_lineNo_11(int32_t value)
	{
		___lineNo_11 = value;
	}

	inline static int32_t get_offset_of_lineStartPos_12() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___lineStartPos_12)); }
	inline int32_t get_lineStartPos_12() const { return ___lineStartPos_12; }
	inline int32_t* get_address_of_lineStartPos_12() { return &___lineStartPos_12; }
	inline void set_lineStartPos_12(int32_t value)
	{
		___lineStartPos_12 = value;
	}

	inline static int32_t get_offset_of_baseUriStr_13() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___baseUriStr_13)); }
	inline String_t* get_baseUriStr_13() const { return ___baseUriStr_13; }
	inline String_t** get_address_of_baseUriStr_13() { return &___baseUriStr_13; }
	inline void set_baseUriStr_13(String_t* value)
	{
		___baseUriStr_13 = value;
		Il2CppCodeGenWriteBarrier((&___baseUriStr_13), value);
	}

	inline static int32_t get_offset_of_baseUri_14() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___baseUri_14)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_14() const { return ___baseUri_14; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_14() { return &___baseUri_14; }
	inline void set_baseUri_14(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_14 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_14), value);
	}

	inline static int32_t get_offset_of_isEof_15() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___isEof_15)); }
	inline bool get_isEof_15() const { return ___isEof_15; }
	inline bool* get_address_of_isEof_15() { return &___isEof_15; }
	inline void set_isEof_15(bool value)
	{
		___isEof_15 = value;
	}

	inline static int32_t get_offset_of_isStreamEof_16() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___isStreamEof_16)); }
	inline bool get_isStreamEof_16() const { return ___isStreamEof_16; }
	inline bool* get_address_of_isStreamEof_16() { return &___isStreamEof_16; }
	inline void set_isStreamEof_16(bool value)
	{
		___isStreamEof_16 = value;
	}

	inline static int32_t get_offset_of_entity_17() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___entity_17)); }
	inline RuntimeObject* get_entity_17() const { return ___entity_17; }
	inline RuntimeObject** get_address_of_entity_17() { return &___entity_17; }
	inline void set_entity_17(RuntimeObject* value)
	{
		___entity_17 = value;
		Il2CppCodeGenWriteBarrier((&___entity_17), value);
	}

	inline static int32_t get_offset_of_entityId_18() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___entityId_18)); }
	inline int32_t get_entityId_18() const { return ___entityId_18; }
	inline int32_t* get_address_of_entityId_18() { return &___entityId_18; }
	inline void set_entityId_18(int32_t value)
	{
		___entityId_18 = value;
	}

	inline static int32_t get_offset_of_eolNormalized_19() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___eolNormalized_19)); }
	inline bool get_eolNormalized_19() const { return ___eolNormalized_19; }
	inline bool* get_address_of_eolNormalized_19() { return &___eolNormalized_19; }
	inline void set_eolNormalized_19(bool value)
	{
		___eolNormalized_19 = value;
	}

	inline static int32_t get_offset_of_entityResolvedManually_20() { return static_cast<int32_t>(offsetof(ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2, ___entityResolvedManually_20)); }
	inline bool get_entityResolvedManually_20() const { return ___entityResolvedManually_20; }
	inline bool* get_address_of_entityResolvedManually_20() { return &___entityResolvedManually_20; }
	inline void set_entityResolvedManually_20(bool value)
	{
		___entityResolvedManually_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextReaderImpl/ParsingState
struct ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_marshaled_pinvoke
{
	uint8_t* ___chars_0;
	int32_t ___charPos_1;
	int32_t ___charsUsed_2;
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	int32_t ___appendMode_4;
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_6;
	uint8_t* ___bytes_7;
	int32_t ___bytePos_8;
	int32_t ___bytesUsed_9;
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___textReader_10;
	int32_t ___lineNo_11;
	int32_t ___lineStartPos_12;
	char* ___baseUriStr_13;
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_14;
	int32_t ___isEof_15;
	int32_t ___isStreamEof_16;
	RuntimeObject* ___entity_17;
	int32_t ___entityId_18;
	int32_t ___eolNormalized_19;
	int32_t ___entityResolvedManually_20;
};
// Native definition for COM marshalling of System.Xml.XmlTextReaderImpl/ParsingState
struct ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_marshaled_com
{
	uint8_t* ___chars_0;
	int32_t ___charPos_1;
	int32_t ___charsUsed_2;
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	int32_t ___appendMode_4;
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	Decoder_tEEF45EB6F965222036C49E8EC6BA8A0692AA1F26 * ___decoder_6;
	uint8_t* ___bytes_7;
	int32_t ___bytePos_8;
	int32_t ___bytesUsed_9;
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___textReader_10;
	int32_t ___lineNo_11;
	int32_t ___lineStartPos_12;
	Il2CppChar* ___baseUriStr_13;
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_14;
	int32_t ___isEof_15;
	int32_t ___isStreamEof_16;
	RuntimeObject* ___entity_17;
	int32_t ___entityId_18;
	int32_t ___eolNormalized_19;
	int32_t ___entityResolvedManually_20;
};
#endif // PARSINGSTATE_TE4A8E7F14B2068AE43ECF99F81F55B0301A551A2_H
#ifndef NAMESPACE_T092048EEBC7FF22AF20114DDA7633072C44CA26B_H
#define NAMESPACE_T092048EEBC7FF22AF20114DDA7633072C44CA26B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/Namespace
struct  Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B 
{
public:
	// System.String System.Xml.XmlTextWriter/Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlTextWriter/Namespace::ns
	String_t* ___ns_1;
	// System.Boolean System.Xml.XmlTextWriter/Namespace::declared
	bool ___declared_2;
	// System.Int32 System.Xml.XmlTextWriter/Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_ns_1() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___ns_1)); }
	inline String_t* get_ns_1() const { return ___ns_1; }
	inline String_t** get_address_of_ns_1() { return &___ns_1; }
	inline void set_ns_1(String_t* value)
	{
		___ns_1 = value;
		Il2CppCodeGenWriteBarrier((&___ns_1), value);
	}

	inline static int32_t get_offset_of_declared_2() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___declared_2)); }
	inline bool get_declared_2() const { return ___declared_2; }
	inline bool* get_address_of_declared_2() { return &___declared_2; }
	inline void set_declared_2(bool value)
	{
		___declared_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/Namespace
struct Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___ns_1;
	int32_t ___declared_2;
	int32_t ___prevNsIndex_3;
};
#endif // NAMESPACE_T092048EEBC7FF22AF20114DDA7633072C44CA26B_H
#ifndef XMLVALIDATINGREADER_T4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92_H
#define XMLVALIDATINGREADER_T4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReader
struct  XmlValidatingReader_t4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlValidatingReaderImpl System.Xml.XmlValidatingReader::impl
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___impl_3;

public:
	inline static int32_t get_offset_of_impl_3() { return static_cast<int32_t>(offsetof(XmlValidatingReader_t4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92, ___impl_3)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_impl_3() const { return ___impl_3; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_impl_3() { return &___impl_3; }
	inline void set_impl_3(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___impl_3 = value;
		Il2CppCodeGenWriteBarrier((&___impl_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALIDATINGREADER_T4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92_H
#ifndef ATTRNAME_T56333AEE26116ABEF12DF292DB01D863108AD298_H
#define ATTRNAME_T56333AEE26116ABEF12DF292DB01D863108AD298_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttrName
struct  AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298 
{
public:
	// System.String System.Xml.XmlWellFormedWriter/AttrName::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlWellFormedWriter/AttrName::namespaceUri
	String_t* ___namespaceUri_1;
	// System.String System.Xml.XmlWellFormedWriter/AttrName::localName
	String_t* ___localName_2;
	// System.Int32 System.Xml.XmlWellFormedWriter/AttrName::prev
	int32_t ___prev_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_prev_3() { return static_cast<int32_t>(offsetof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298, ___prev_3)); }
	inline int32_t get_prev_3() const { return ___prev_3; }
	inline int32_t* get_address_of_prev_3() { return &___prev_3; }
	inline void set_prev_3(int32_t value)
	{
		___prev_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/AttrName
struct AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___namespaceUri_1;
	char* ___localName_2;
	int32_t ___prev_3;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/AttrName
struct AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___namespaceUri_1;
	Il2CppChar* ___localName_2;
	int32_t ___prev_3;
};
#endif // ATTRNAME_T56333AEE26116ABEF12DF292DB01D863108AD298_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#define CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ConformanceLevel
struct  ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A 
{
public:
	// System.Int32 System.Xml.ConformanceLevel::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConformanceLevel_t42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFORMANCELEVEL_T42F011B2E0FC7FB8324076CCBDEDAD7CC6FFBE1A_H
#ifndef SCANNINGFUNCTION_TF6628905AD8C7F4E5595E4258DFC60F88004B359_H
#define SCANNINGFUNCTION_TF6628905AD8C7F4E5595E4258DFC60F88004B359_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/ScanningFunction
struct  ScanningFunction_tF6628905AD8C7F4E5595E4258DFC60F88004B359 
{
public:
	// System.Int32 System.Xml.DtdParser/ScanningFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ScanningFunction_tF6628905AD8C7F4E5595E4258DFC60F88004B359, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCANNINGFUNCTION_TF6628905AD8C7F4E5595E4258DFC60F88004B359_H
#ifndef TOKEN_T411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6_H
#define TOKEN_T411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser/Token
struct  Token_t411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6 
{
public:
	// System.Int32 System.Xml.DtdParser/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6_H
#ifndef FORMATTING_TA4B29B0BD063518BB29B92C6B165E39D01C553E2_H
#define FORMATTING_TA4B29B0BD063518BB29B92C6B165E39D01C553E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Formatting
struct  Formatting_tA4B29B0BD063518BB29B92C6B165E39D01C553E2 
{
public:
	// System.Int32 System.Xml.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tA4B29B0BD063518BB29B92C6B165E39D01C553E2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TA4B29B0BD063518BB29B92C6B165E39D01C553E2_H
#ifndef NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#define NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NamespaceHandling
struct  NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618 
{
public:
	// System.Int32 System.Xml.NamespaceHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceHandling_tECE89F8A3E0CF300C5A18E41FBC65494C7EB3618, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEHANDLING_TECE89F8A3E0CF300C5A18E41FBC65494C7EB3618_H
#ifndef NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#define NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.NewLineHandling
struct  NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543 
{
public:
	// System.Int32 System.Xml.NewLineHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NewLineHandling_t35189DCBC088E592125C4F2FF4EDD3DB9F594543, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWLINEHANDLING_T35189DCBC088E592125C4F2FF4EDD3DB9F594543_H
#ifndef READSTATE_TAAF15D8DE96B6A22379D2B6FEF22764640D05DD0_H
#define READSTATE_TAAF15D8DE96B6A22379D2B6FEF22764640D05DD0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ReadState
struct  ReadState_tAAF15D8DE96B6A22379D2B6FEF22764640D05DD0 
{
public:
	// System.Int32 System.Xml.ReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadState_tAAF15D8DE96B6A22379D2B6FEF22764640D05DD0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READSTATE_TAAF15D8DE96B6A22379D2B6FEF22764640D05DD0_H
#ifndef TRISTATE_T7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0_H
#define TRISTATE_T7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.TriState
struct  TriState_t7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0 
{
public:
	// System.Int32 System.Xml.TriState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TriState_t7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRISTATE_T7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0_H
#ifndef VALIDATIONTYPE_TFA1BE5AAA395D6B8033755CD823243B8FAC2673E_H
#define VALIDATIONTYPE_TFA1BE5AAA395D6B8033755CD823243B8FAC2673E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.ValidationType
struct  ValidationType_tFA1BE5AAA395D6B8033755CD823243B8FAC2673E 
{
public:
	// System.Int32 System.Xml.ValidationType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidationType_tFA1BE5AAA395D6B8033755CD823243B8FAC2673E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONTYPE_TFA1BE5AAA395D6B8033755CD823243B8FAC2673E_H
#ifndef WRITESTATE_TB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132_H
#define WRITESTATE_TB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.WriteState
struct  WriteState_tB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132 
{
public:
	// System.Int32 System.Xml.WriteState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(WriteState_tB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WRITESTATE_TB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132_H
#ifndef XMLCHARACTERDATA_TA7587D706680E42BD7A094F87CB0859C840A8531_H
#define XMLCHARACTERDATA_TA7587D706680E42BD7A094F87CB0859C840A8531_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharacterData
struct  XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.String System.Xml.XmlCharacterData::data
	String_t* ___data_2;

public:
	inline static int32_t get_offset_of_data_2() { return static_cast<int32_t>(offsetof(XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531, ___data_2)); }
	inline String_t* get_data_2() const { return ___data_2; }
	inline String_t** get_address_of_data_2() { return &___data_2; }
	inline void set_data_2(String_t* value)
	{
		___data_2 = value;
		Il2CppCodeGenWriteBarrier((&___data_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCHARACTERDATA_TA7587D706680E42BD7A094F87CB0859C840A8531_H
#ifndef XMLDECLARATION_TB67A8B98F543947C6EB990C44A212527B9B51200_H
#define XMLDECLARATION_TB67A8B98F543947C6EB990C44A212527B9B51200_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDeclaration
struct  XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.String System.Xml.XmlDeclaration::version
	String_t* ___version_2;
	// System.String System.Xml.XmlDeclaration::encoding
	String_t* ___encoding_3;
	// System.String System.Xml.XmlDeclaration::standalone
	String_t* ___standalone_4;

public:
	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200, ___version_2)); }
	inline String_t* get_version_2() const { return ___version_2; }
	inline String_t** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(String_t* value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200, ___encoding_3)); }
	inline String_t* get_encoding_3() const { return ___encoding_3; }
	inline String_t** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(String_t* value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_standalone_4() { return static_cast<int32_t>(offsetof(XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200, ___standalone_4)); }
	inline String_t* get_standalone_4() const { return ___standalone_4; }
	inline String_t** get_address_of_standalone_4() { return &___standalone_4; }
	inline void set_standalone_4(String_t* value)
	{
		___standalone_4 = value;
		Il2CppCodeGenWriteBarrier((&___standalone_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATION_TB67A8B98F543947C6EB990C44A212527B9B51200_H
#ifndef XMLDOCUMENTTYPE_T9AB8D5C3C27B699B885D941D4B4735A6EE039136_H
#define XMLDOCUMENTTYPE_T9AB8D5C3C27B699B885D941D4B4735A6EE039136_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlDocumentType
struct  XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.String System.Xml.XmlDocumentType::name
	String_t* ___name_2;
	// System.String System.Xml.XmlDocumentType::publicId
	String_t* ___publicId_3;
	// System.String System.Xml.XmlDocumentType::systemId
	String_t* ___systemId_4;
	// System.String System.Xml.XmlDocumentType::internalSubset
	String_t* ___internalSubset_5;
	// System.Boolean System.Xml.XmlDocumentType::namespaces
	bool ___namespaces_6;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::entities
	XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * ___entities_7;
	// System.Xml.XmlNamedNodeMap System.Xml.XmlDocumentType::notations
	XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * ___notations_8;
	// System.Xml.Schema.SchemaInfo System.Xml.XmlDocumentType::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_9;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_publicId_3() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___publicId_3)); }
	inline String_t* get_publicId_3() const { return ___publicId_3; }
	inline String_t** get_address_of_publicId_3() { return &___publicId_3; }
	inline void set_publicId_3(String_t* value)
	{
		___publicId_3 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_3), value);
	}

	inline static int32_t get_offset_of_systemId_4() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___systemId_4)); }
	inline String_t* get_systemId_4() const { return ___systemId_4; }
	inline String_t** get_address_of_systemId_4() { return &___systemId_4; }
	inline void set_systemId_4(String_t* value)
	{
		___systemId_4 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_4), value);
	}

	inline static int32_t get_offset_of_internalSubset_5() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___internalSubset_5)); }
	inline String_t* get_internalSubset_5() const { return ___internalSubset_5; }
	inline String_t** get_address_of_internalSubset_5() { return &___internalSubset_5; }
	inline void set_internalSubset_5(String_t* value)
	{
		___internalSubset_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubset_5), value);
	}

	inline static int32_t get_offset_of_namespaces_6() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___namespaces_6)); }
	inline bool get_namespaces_6() const { return ___namespaces_6; }
	inline bool* get_address_of_namespaces_6() { return &___namespaces_6; }
	inline void set_namespaces_6(bool value)
	{
		___namespaces_6 = value;
	}

	inline static int32_t get_offset_of_entities_7() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___entities_7)); }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * get_entities_7() const { return ___entities_7; }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 ** get_address_of_entities_7() { return &___entities_7; }
	inline void set_entities_7(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * value)
	{
		___entities_7 = value;
		Il2CppCodeGenWriteBarrier((&___entities_7), value);
	}

	inline static int32_t get_offset_of_notations_8() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___notations_8)); }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * get_notations_8() const { return ___notations_8; }
	inline XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 ** get_address_of_notations_8() { return &___notations_8; }
	inline void set_notations_8(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31 * value)
	{
		___notations_8 = value;
		Il2CppCodeGenWriteBarrier((&___notations_8), value);
	}

	inline static int32_t get_offset_of_schemaInfo_9() { return static_cast<int32_t>(offsetof(XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136, ___schemaInfo_9)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_9() const { return ___schemaInfo_9; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_9() { return &___schemaInfo_9; }
	inline void set_schemaInfo_9(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_9 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTTYPE_T9AB8D5C3C27B699B885D941D4B4735A6EE039136_H
#ifndef XMLELEMENT_T05D8C7971DE016A354D86028E7FFD84CD9DDDFDC_H
#define XMLELEMENT_T05D8C7971DE016A354D86028E7FFD84CD9DDDFDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlElement
struct  XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.Xml.XmlName System.Xml.XmlElement::name
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * ___name_2;
	// System.Xml.XmlAttributeCollection System.Xml.XmlElement::attributes
	XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * ___attributes_3;
	// System.Xml.XmlLinkedNode System.Xml.XmlElement::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_4;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC, ___name_2)); }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * get_name_2() const { return ___name_2; }
	inline XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 ** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682 * value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_attributes_3() { return static_cast<int32_t>(offsetof(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC, ___attributes_3)); }
	inline XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * get_attributes_3() const { return ___attributes_3; }
	inline XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E ** get_address_of_attributes_3() { return &___attributes_3; }
	inline void set_attributes_3(XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E * value)
	{
		___attributes_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_3), value);
	}

	inline static int32_t get_offset_of_lastChild_4() { return static_cast<int32_t>(offsetof(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC, ___lastChild_4)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_4() const { return ___lastChild_4; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_4() { return &___lastChild_4; }
	inline void set_lastChild_4(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_4 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENT_T05D8C7971DE016A354D86028E7FFD84CD9DDDFDC_H
#ifndef XMLENTITYREFERENCE_T878158918F3AD20215DE606E9C1725A05372A204_H
#define XMLENTITYREFERENCE_T878158918F3AD20215DE606E9C1725A05372A204_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlEntityReference
struct  XmlEntityReference_t878158918F3AD20215DE606E9C1725A05372A204  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.String System.Xml.XmlEntityReference::name
	String_t* ___name_2;
	// System.Xml.XmlLinkedNode System.Xml.XmlEntityReference::lastChild
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * ___lastChild_3;

public:
	inline static int32_t get_offset_of_name_2() { return static_cast<int32_t>(offsetof(XmlEntityReference_t878158918F3AD20215DE606E9C1725A05372A204, ___name_2)); }
	inline String_t* get_name_2() const { return ___name_2; }
	inline String_t** get_address_of_name_2() { return &___name_2; }
	inline void set_name_2(String_t* value)
	{
		___name_2 = value;
		Il2CppCodeGenWriteBarrier((&___name_2), value);
	}

	inline static int32_t get_offset_of_lastChild_3() { return static_cast<int32_t>(offsetof(XmlEntityReference_t878158918F3AD20215DE606E9C1725A05372A204, ___lastChild_3)); }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * get_lastChild_3() const { return ___lastChild_3; }
	inline XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E ** get_address_of_lastChild_3() { return &___lastChild_3; }
	inline void set_lastChild_3(XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E * value)
	{
		___lastChild_3 = value;
		Il2CppCodeGenWriteBarrier((&___lastChild_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLENTITYREFERENCE_T878158918F3AD20215DE606E9C1725A05372A204_H
#ifndef XMLNAMEDNODEMAP_T260246787BA7B6747AB878378D22EC7693465E31_H
#define XMLNAMEDNODEMAP_T260246787BA7B6747AB878378D22EC7693465E31_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamedNodeMap
struct  XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31  : public RuntimeObject
{
public:
	// System.Xml.XmlNode System.Xml.XmlNamedNodeMap::parent
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parent_0;
	// System.Xml.XmlNamedNodeMap/SmallXmlNodeList System.Xml.XmlNamedNodeMap::nodes
	SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E  ___nodes_1;

public:
	inline static int32_t get_offset_of_parent_0() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31, ___parent_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_parent_0() const { return ___parent_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_parent_0() { return &___parent_0; }
	inline void set_parent_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___parent_0 = value;
		Il2CppCodeGenWriteBarrier((&___parent_0), value);
	}

	inline static int32_t get_offset_of_nodes_1() { return static_cast<int32_t>(offsetof(XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31, ___nodes_1)); }
	inline SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E  get_nodes_1() const { return ___nodes_1; }
	inline SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E * get_address_of_nodes_1() { return &___nodes_1; }
	inline void set_nodes_1(SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E  value)
	{
		___nodes_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMEDNODEMAP_T260246787BA7B6747AB878378D22EC7693465E31_H
#ifndef XMLNODECHANGEDACTION_TD5DC99DE9A26516B08D3AFBE8B31613469009E29_H
#define XMLNODECHANGEDACTION_TD5DC99DE9A26516B08D3AFBE8B31613469009E29_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedAction
struct  XmlNodeChangedAction_tD5DC99DE9A26516B08D3AFBE8B31613469009E29 
{
public:
	// System.Int32 System.Xml.XmlNodeChangedAction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeChangedAction_tD5DC99DE9A26516B08D3AFBE8B31613469009E29, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDACTION_TD5DC99DE9A26516B08D3AFBE8B31613469009E29_H
#ifndef XMLNODETYPE_TEE56AC4F9EC36B979516EA5836C4DA730B0A21E1_H
#define XMLNODETYPE_TEE56AC4F9EC36B979516EA5836C4DA730B0A21E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeType
struct  XmlNodeType_tEE56AC4F9EC36B979516EA5836C4DA730B0A21E1 
{
public:
	// System.Int32 System.Xml.XmlNodeType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlNodeType_tEE56AC4F9EC36B979516EA5836C4DA730B0A21E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODETYPE_TEE56AC4F9EC36B979516EA5836C4DA730B0A21E1_H
#ifndef XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#define XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlOutputMethod
struct  XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4 
{
public:
	// System.Int32 System.Xml.XmlOutputMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLOUTPUTMETHOD_TF47F7732ECF902DFC04BAB9F9B7025F94DA862D4_H
#ifndef XMLPROCESSINGINSTRUCTION_T87662A3B85D8B2F79E74917CA294C8954F71C6D0_H
#define XMLPROCESSINGINSTRUCTION_T87662A3B85D8B2F79E74917CA294C8954F71C6D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlProcessingInstruction
struct  XmlProcessingInstruction_t87662A3B85D8B2F79E74917CA294C8954F71C6D0  : public XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E
{
public:
	// System.String System.Xml.XmlProcessingInstruction::target
	String_t* ___target_2;
	// System.String System.Xml.XmlProcessingInstruction::data
	String_t* ___data_3;

public:
	inline static int32_t get_offset_of_target_2() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t87662A3B85D8B2F79E74917CA294C8954F71C6D0, ___target_2)); }
	inline String_t* get_target_2() const { return ___target_2; }
	inline String_t** get_address_of_target_2() { return &___target_2; }
	inline void set_target_2(String_t* value)
	{
		___target_2 = value;
		Il2CppCodeGenWriteBarrier((&___target_2), value);
	}

	inline static int32_t get_offset_of_data_3() { return static_cast<int32_t>(offsetof(XmlProcessingInstruction_t87662A3B85D8B2F79E74917CA294C8954F71C6D0, ___data_3)); }
	inline String_t* get_data_3() const { return ___data_3; }
	inline String_t** get_address_of_data_3() { return &___data_3; }
	inline void set_data_3(String_t* value)
	{
		___data_3 = value;
		Il2CppCodeGenWriteBarrier((&___data_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLPROCESSINGINSTRUCTION_T87662A3B85D8B2F79E74917CA294C8954F71C6D0_H
#ifndef XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#define XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSpace
struct  XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB 
{
public:
	// System.Int32 System.Xml.XmlSpace::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSpace_tC1A644D65B6BE72CF02BA2687B5AE169713271AB, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSPACE_TC1A644D65B6BE72CF02BA2687B5AE169713271AB_H
#ifndef XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#define XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlStandalone
struct  XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B 
{
public:
	// System.Int32 System.Xml.XmlStandalone::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSTANDALONE_TD77DD6044609EA966459D821754EEE0BED37B39B_H
#ifndef ENTITYEXPANDTYPE_TC9953664AB0CE6F0813792286A9F3F6D6EF4A878_H
#define ENTITYEXPANDTYPE_TC9953664AB0CE6F0813792286A9F3F6D6EF4A878_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/EntityExpandType
struct  EntityExpandType_tC9953664AB0CE6F0813792286A9F3F6D6EF4A878 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/EntityExpandType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityExpandType_tC9953664AB0CE6F0813792286A9F3F6D6EF4A878, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYEXPANDTYPE_TC9953664AB0CE6F0813792286A9F3F6D6EF4A878_H
#ifndef ENTITYTYPE_TD1C5D0F2F43C096B07783E681D885A6F511F0657_H
#define ENTITYTYPE_TD1C5D0F2F43C096B07783E681D885A6F511F0657_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/EntityType
struct  EntityType_tD1C5D0F2F43C096B07783E681D885A6F511F0657 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/EntityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(EntityType_tD1C5D0F2F43C096B07783E681D885A6F511F0657, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYTYPE_TD1C5D0F2F43C096B07783E681D885A6F511F0657_H
#ifndef INCREMENTALREADSTATE_T9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C_H
#define INCREMENTALREADSTATE_T9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/IncrementalReadState
struct  IncrementalReadState_t9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/IncrementalReadState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(IncrementalReadState_t9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INCREMENTALREADSTATE_T9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C_H
#ifndef INITINPUTTYPE_TBF51CCB79B0C9D912C92528B2B8F1DBCC3649175_H
#define INITINPUTTYPE_TBF51CCB79B0C9D912C92528B2B8F1DBCC3649175_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/InitInputType
struct  InitInputType_tBF51CCB79B0C9D912C92528B2B8F1DBCC3649175 
{
public:
	// System.Int32 System.Xml.XmlTextReaderImpl/InitInputType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InitInputType_tBF51CCB79B0C9D912C92528B2B8F1DBCC3649175, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INITINPUTTYPE_TBF51CCB79B0C9D912C92528B2B8F1DBCC3649175_H
#ifndef NAMESPACESTATE_TA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32_H
#define NAMESPACESTATE_TA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/NamespaceState
struct  NamespaceState_tA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/NamespaceState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceState_tA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACESTATE_TA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32_H
#ifndef SPECIALATTR_TC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528_H
#define SPECIALATTR_TC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/SpecialAttr
struct  SpecialAttr_tC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/SpecialAttr::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttr_tC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALATTR_TC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528_H
#ifndef STATE_T8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648_H
#define STATE_T8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/State
struct  State_t8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648_H
#ifndef TOKEN_TBCB56C1082F0646C24614CAED556F59987BAFA32_H
#define TOKEN_TBCB56C1082F0646C24614CAED556F59987BAFA32_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/Token
struct  Token_tBCB56C1082F0646C24614CAED556F59987BAFA32 
{
public:
	// System.Int32 System.Xml.XmlTextWriter/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_tBCB56C1082F0646C24614CAED556F59987BAFA32, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_TBCB56C1082F0646C24614CAED556F59987BAFA32_H
#ifndef XMLUNSPECIFIEDATTRIBUTE_T1C82B285B60096D164B703202A8BD91A2009DE5D_H
#define XMLUNSPECIFIEDATTRIBUTE_T1C82B285B60096D164B703202A8BD91A2009DE5D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUnspecifiedAttribute
struct  XmlUnspecifiedAttribute_t1C82B285B60096D164B703202A8BD91A2009DE5D  : public XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA
{
public:
	// System.Boolean System.Xml.XmlUnspecifiedAttribute::fSpecified
	bool ___fSpecified_3;

public:
	inline static int32_t get_offset_of_fSpecified_3() { return static_cast<int32_t>(offsetof(XmlUnspecifiedAttribute_t1C82B285B60096D164B703202A8BD91A2009DE5D, ___fSpecified_3)); }
	inline bool get_fSpecified_3() const { return ___fSpecified_3; }
	inline bool* get_address_of_fSpecified_3() { return &___fSpecified_3; }
	inline void set_fSpecified_3(bool value)
	{
		___fSpecified_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUNSPECIFIEDATTRIBUTE_T1C82B285B60096D164B703202A8BD91A2009DE5D_H
#ifndef PARSINGFUNCTION_T972AAE136ADB01CEF82FE8710B519BBF44704B92_H
#define PARSINGFUNCTION_T972AAE136ADB01CEF82FE8710B519BBF44704B92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl/ParsingFunction
struct  ParsingFunction_t972AAE136ADB01CEF82FE8710B519BBF44704B92 
{
public:
	// System.Int32 System.Xml.XmlValidatingReaderImpl/ParsingFunction::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParsingFunction_t972AAE136ADB01CEF82FE8710B519BBF44704B92, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSINGFUNCTION_T972AAE136ADB01CEF82FE8710B519BBF44704B92_H
#ifndef ITEMTYPE_T492099D243BA127F275386D81B3E94A40FA53B35_H
#define ITEMTYPE_T492099D243BA127F275386D81B3E94A40FA53B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache/ItemType
struct  ItemType_t492099D243BA127F275386D81B3E94A40FA53B35 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/AttributeValueCache/ItemType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ItemType_t492099D243BA127F275386D81B3E94A40FA53B35, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEMTYPE_T492099D243BA127F275386D81B3E94A40FA53B35_H
#ifndef NAMESPACEKIND_TE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED_H
#define NAMESPACEKIND_TE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/NamespaceKind
struct  NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/NamespaceKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEKIND_TE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED_H
#ifndef SPECIALATTRIBUTE_T5A666AF6BF22A7E35321430E1059E7429866A7B0_H
#define SPECIALATTRIBUTE_T5A666AF6BF22A7E35321430E1059E7429866A7B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/SpecialAttribute
struct  SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/SpecialAttribute::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALATTRIBUTE_T5A666AF6BF22A7E35321430E1059E7429866A7B0_H
#ifndef STATE_T296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D_H
#define STATE_T296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/State
struct  State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D_H
#ifndef TOKEN_T2F590F413BBC0AAE03A6153F994A1ECB8F527F4D_H
#define TOKEN_T2F590F413BBC0AAE03A6153F994A1ECB8F527F4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/Token
struct  Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2F590F413BBC0AAE03A6153F994A1ECB8F527F4D_H
#ifndef CACHINGREADERSTATE_T28388E57781451F791B55B64D533099BA1903229_H
#define CACHINGREADERSTATE_T28388E57781451F791B55B64D533099BA1903229_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XsdCachingReader/CachingReaderState
struct  CachingReaderState_t28388E57781451F791B55B64D533099BA1903229 
{
public:
	// System.Int32 System.Xml.XsdCachingReader/CachingReaderState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CachingReaderState_t28388E57781451F791B55B64D533099BA1903229, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGREADERSTATE_T28388E57781451F791B55B64D533099BA1903229_H
#ifndef VALIDATINGREADERSTATE_TC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE_H
#define VALIDATINGREADERSTATE_TC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XsdValidatingReader/ValidatingReaderState
struct  ValidatingReaderState_tC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE 
{
public:
	// System.Int32 System.Xml.XsdValidatingReader/ValidatingReaderState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ValidatingReaderState_tC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATINGREADERSTATE_TC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef DTDPARSER_T9C63F125C98379A2AD3996648E8B7C234EA56574_H
#define DTDPARSER_T9C63F125C98379A2AD3996648E8B7C234EA56574_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.DtdParser
struct  DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574  : public RuntimeObject
{
public:
	// System.Xml.IDtdParserAdapter System.Xml.DtdParser::readerAdapter
	RuntimeObject* ___readerAdapter_0;
	// System.Xml.IDtdParserAdapterWithValidation System.Xml.DtdParser::readerAdapterWithValidation
	RuntimeObject* ___readerAdapterWithValidation_1;
	// System.Xml.XmlNameTable System.Xml.DtdParser::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Xml.Schema.SchemaInfo System.Xml.DtdParser::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_3;
	// System.Xml.XmlCharType System.Xml.DtdParser::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_4;
	// System.String System.Xml.DtdParser::systemId
	String_t* ___systemId_5;
	// System.String System.Xml.DtdParser::publicId
	String_t* ___publicId_6;
	// System.Boolean System.Xml.DtdParser::normalize
	bool ___normalize_7;
	// System.Boolean System.Xml.DtdParser::validate
	bool ___validate_8;
	// System.Boolean System.Xml.DtdParser::supportNamespaces
	bool ___supportNamespaces_9;
	// System.Boolean System.Xml.DtdParser::v1Compat
	bool ___v1Compat_10;
	// System.Char[] System.Xml.DtdParser::chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___chars_11;
	// System.Int32 System.Xml.DtdParser::charsUsed
	int32_t ___charsUsed_12;
	// System.Int32 System.Xml.DtdParser::curPos
	int32_t ___curPos_13;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::scanningFunction
	int32_t ___scanningFunction_14;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::nextScaningFunction
	int32_t ___nextScaningFunction_15;
	// System.Xml.DtdParser/ScanningFunction System.Xml.DtdParser::savedScanningFunction
	int32_t ___savedScanningFunction_16;
	// System.Boolean System.Xml.DtdParser::whitespaceSeen
	bool ___whitespaceSeen_17;
	// System.Int32 System.Xml.DtdParser::tokenStartPos
	int32_t ___tokenStartPos_18;
	// System.Int32 System.Xml.DtdParser::colonPos
	int32_t ___colonPos_19;
	// System.Text.StringBuilder System.Xml.DtdParser::internalSubsetValueSb
	StringBuilder_t * ___internalSubsetValueSb_20;
	// System.Int32 System.Xml.DtdParser::externalEntitiesDepth
	int32_t ___externalEntitiesDepth_21;
	// System.Int32 System.Xml.DtdParser::currentEntityId
	int32_t ___currentEntityId_22;
	// System.Boolean System.Xml.DtdParser::freeFloatingDtd
	bool ___freeFloatingDtd_23;
	// System.Boolean System.Xml.DtdParser::hasFreeFloatingInternalSubset
	bool ___hasFreeFloatingInternalSubset_24;
	// System.Text.StringBuilder System.Xml.DtdParser::stringBuilder
	StringBuilder_t * ___stringBuilder_25;
	// System.Int32 System.Xml.DtdParser::condSectionDepth
	int32_t ___condSectionDepth_26;
	// System.Xml.LineInfo System.Xml.DtdParser::literalLineInfo
	LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  ___literalLineInfo_27;
	// System.Char System.Xml.DtdParser::literalQuoteChar
	Il2CppChar ___literalQuoteChar_28;
	// System.String System.Xml.DtdParser::documentBaseUri
	String_t* ___documentBaseUri_29;
	// System.String System.Xml.DtdParser::externalDtdBaseUri
	String_t* ___externalDtdBaseUri_30;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.DtdParser/UndeclaredNotation> System.Xml.DtdParser::undeclaredNotations
	Dictionary_2_t916DC1A8D299E7D1846930151D649D79F4D9D5A2 * ___undeclaredNotations_31;
	// System.Int32[] System.Xml.DtdParser::condSectionEntityIds
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___condSectionEntityIds_32;

public:
	inline static int32_t get_offset_of_readerAdapter_0() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___readerAdapter_0)); }
	inline RuntimeObject* get_readerAdapter_0() const { return ___readerAdapter_0; }
	inline RuntimeObject** get_address_of_readerAdapter_0() { return &___readerAdapter_0; }
	inline void set_readerAdapter_0(RuntimeObject* value)
	{
		___readerAdapter_0 = value;
		Il2CppCodeGenWriteBarrier((&___readerAdapter_0), value);
	}

	inline static int32_t get_offset_of_readerAdapterWithValidation_1() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___readerAdapterWithValidation_1)); }
	inline RuntimeObject* get_readerAdapterWithValidation_1() const { return ___readerAdapterWithValidation_1; }
	inline RuntimeObject** get_address_of_readerAdapterWithValidation_1() { return &___readerAdapterWithValidation_1; }
	inline void set_readerAdapterWithValidation_1(RuntimeObject* value)
	{
		___readerAdapterWithValidation_1 = value;
		Il2CppCodeGenWriteBarrier((&___readerAdapterWithValidation_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaInfo_3() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___schemaInfo_3)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_3() const { return ___schemaInfo_3; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_3() { return &___schemaInfo_3; }
	inline void set_schemaInfo_3(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_3), value);
	}

	inline static int32_t get_offset_of_xmlCharType_4() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___xmlCharType_4)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_4() const { return ___xmlCharType_4; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_4() { return &___xmlCharType_4; }
	inline void set_xmlCharType_4(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_4 = value;
	}

	inline static int32_t get_offset_of_systemId_5() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___systemId_5)); }
	inline String_t* get_systemId_5() const { return ___systemId_5; }
	inline String_t** get_address_of_systemId_5() { return &___systemId_5; }
	inline void set_systemId_5(String_t* value)
	{
		___systemId_5 = value;
		Il2CppCodeGenWriteBarrier((&___systemId_5), value);
	}

	inline static int32_t get_offset_of_publicId_6() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___publicId_6)); }
	inline String_t* get_publicId_6() const { return ___publicId_6; }
	inline String_t** get_address_of_publicId_6() { return &___publicId_6; }
	inline void set_publicId_6(String_t* value)
	{
		___publicId_6 = value;
		Il2CppCodeGenWriteBarrier((&___publicId_6), value);
	}

	inline static int32_t get_offset_of_normalize_7() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___normalize_7)); }
	inline bool get_normalize_7() const { return ___normalize_7; }
	inline bool* get_address_of_normalize_7() { return &___normalize_7; }
	inline void set_normalize_7(bool value)
	{
		___normalize_7 = value;
	}

	inline static int32_t get_offset_of_validate_8() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___validate_8)); }
	inline bool get_validate_8() const { return ___validate_8; }
	inline bool* get_address_of_validate_8() { return &___validate_8; }
	inline void set_validate_8(bool value)
	{
		___validate_8 = value;
	}

	inline static int32_t get_offset_of_supportNamespaces_9() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___supportNamespaces_9)); }
	inline bool get_supportNamespaces_9() const { return ___supportNamespaces_9; }
	inline bool* get_address_of_supportNamespaces_9() { return &___supportNamespaces_9; }
	inline void set_supportNamespaces_9(bool value)
	{
		___supportNamespaces_9 = value;
	}

	inline static int32_t get_offset_of_v1Compat_10() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___v1Compat_10)); }
	inline bool get_v1Compat_10() const { return ___v1Compat_10; }
	inline bool* get_address_of_v1Compat_10() { return &___v1Compat_10; }
	inline void set_v1Compat_10(bool value)
	{
		___v1Compat_10 = value;
	}

	inline static int32_t get_offset_of_chars_11() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___chars_11)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_chars_11() const { return ___chars_11; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_chars_11() { return &___chars_11; }
	inline void set_chars_11(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___chars_11 = value;
		Il2CppCodeGenWriteBarrier((&___chars_11), value);
	}

	inline static int32_t get_offset_of_charsUsed_12() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___charsUsed_12)); }
	inline int32_t get_charsUsed_12() const { return ___charsUsed_12; }
	inline int32_t* get_address_of_charsUsed_12() { return &___charsUsed_12; }
	inline void set_charsUsed_12(int32_t value)
	{
		___charsUsed_12 = value;
	}

	inline static int32_t get_offset_of_curPos_13() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___curPos_13)); }
	inline int32_t get_curPos_13() const { return ___curPos_13; }
	inline int32_t* get_address_of_curPos_13() { return &___curPos_13; }
	inline void set_curPos_13(int32_t value)
	{
		___curPos_13 = value;
	}

	inline static int32_t get_offset_of_scanningFunction_14() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___scanningFunction_14)); }
	inline int32_t get_scanningFunction_14() const { return ___scanningFunction_14; }
	inline int32_t* get_address_of_scanningFunction_14() { return &___scanningFunction_14; }
	inline void set_scanningFunction_14(int32_t value)
	{
		___scanningFunction_14 = value;
	}

	inline static int32_t get_offset_of_nextScaningFunction_15() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___nextScaningFunction_15)); }
	inline int32_t get_nextScaningFunction_15() const { return ___nextScaningFunction_15; }
	inline int32_t* get_address_of_nextScaningFunction_15() { return &___nextScaningFunction_15; }
	inline void set_nextScaningFunction_15(int32_t value)
	{
		___nextScaningFunction_15 = value;
	}

	inline static int32_t get_offset_of_savedScanningFunction_16() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___savedScanningFunction_16)); }
	inline int32_t get_savedScanningFunction_16() const { return ___savedScanningFunction_16; }
	inline int32_t* get_address_of_savedScanningFunction_16() { return &___savedScanningFunction_16; }
	inline void set_savedScanningFunction_16(int32_t value)
	{
		___savedScanningFunction_16 = value;
	}

	inline static int32_t get_offset_of_whitespaceSeen_17() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___whitespaceSeen_17)); }
	inline bool get_whitespaceSeen_17() const { return ___whitespaceSeen_17; }
	inline bool* get_address_of_whitespaceSeen_17() { return &___whitespaceSeen_17; }
	inline void set_whitespaceSeen_17(bool value)
	{
		___whitespaceSeen_17 = value;
	}

	inline static int32_t get_offset_of_tokenStartPos_18() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___tokenStartPos_18)); }
	inline int32_t get_tokenStartPos_18() const { return ___tokenStartPos_18; }
	inline int32_t* get_address_of_tokenStartPos_18() { return &___tokenStartPos_18; }
	inline void set_tokenStartPos_18(int32_t value)
	{
		___tokenStartPos_18 = value;
	}

	inline static int32_t get_offset_of_colonPos_19() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___colonPos_19)); }
	inline int32_t get_colonPos_19() const { return ___colonPos_19; }
	inline int32_t* get_address_of_colonPos_19() { return &___colonPos_19; }
	inline void set_colonPos_19(int32_t value)
	{
		___colonPos_19 = value;
	}

	inline static int32_t get_offset_of_internalSubsetValueSb_20() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___internalSubsetValueSb_20)); }
	inline StringBuilder_t * get_internalSubsetValueSb_20() const { return ___internalSubsetValueSb_20; }
	inline StringBuilder_t ** get_address_of_internalSubsetValueSb_20() { return &___internalSubsetValueSb_20; }
	inline void set_internalSubsetValueSb_20(StringBuilder_t * value)
	{
		___internalSubsetValueSb_20 = value;
		Il2CppCodeGenWriteBarrier((&___internalSubsetValueSb_20), value);
	}

	inline static int32_t get_offset_of_externalEntitiesDepth_21() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___externalEntitiesDepth_21)); }
	inline int32_t get_externalEntitiesDepth_21() const { return ___externalEntitiesDepth_21; }
	inline int32_t* get_address_of_externalEntitiesDepth_21() { return &___externalEntitiesDepth_21; }
	inline void set_externalEntitiesDepth_21(int32_t value)
	{
		___externalEntitiesDepth_21 = value;
	}

	inline static int32_t get_offset_of_currentEntityId_22() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___currentEntityId_22)); }
	inline int32_t get_currentEntityId_22() const { return ___currentEntityId_22; }
	inline int32_t* get_address_of_currentEntityId_22() { return &___currentEntityId_22; }
	inline void set_currentEntityId_22(int32_t value)
	{
		___currentEntityId_22 = value;
	}

	inline static int32_t get_offset_of_freeFloatingDtd_23() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___freeFloatingDtd_23)); }
	inline bool get_freeFloatingDtd_23() const { return ___freeFloatingDtd_23; }
	inline bool* get_address_of_freeFloatingDtd_23() { return &___freeFloatingDtd_23; }
	inline void set_freeFloatingDtd_23(bool value)
	{
		___freeFloatingDtd_23 = value;
	}

	inline static int32_t get_offset_of_hasFreeFloatingInternalSubset_24() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___hasFreeFloatingInternalSubset_24)); }
	inline bool get_hasFreeFloatingInternalSubset_24() const { return ___hasFreeFloatingInternalSubset_24; }
	inline bool* get_address_of_hasFreeFloatingInternalSubset_24() { return &___hasFreeFloatingInternalSubset_24; }
	inline void set_hasFreeFloatingInternalSubset_24(bool value)
	{
		___hasFreeFloatingInternalSubset_24 = value;
	}

	inline static int32_t get_offset_of_stringBuilder_25() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___stringBuilder_25)); }
	inline StringBuilder_t * get_stringBuilder_25() const { return ___stringBuilder_25; }
	inline StringBuilder_t ** get_address_of_stringBuilder_25() { return &___stringBuilder_25; }
	inline void set_stringBuilder_25(StringBuilder_t * value)
	{
		___stringBuilder_25 = value;
		Il2CppCodeGenWriteBarrier((&___stringBuilder_25), value);
	}

	inline static int32_t get_offset_of_condSectionDepth_26() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___condSectionDepth_26)); }
	inline int32_t get_condSectionDepth_26() const { return ___condSectionDepth_26; }
	inline int32_t* get_address_of_condSectionDepth_26() { return &___condSectionDepth_26; }
	inline void set_condSectionDepth_26(int32_t value)
	{
		___condSectionDepth_26 = value;
	}

	inline static int32_t get_offset_of_literalLineInfo_27() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___literalLineInfo_27)); }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  get_literalLineInfo_27() const { return ___literalLineInfo_27; }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 * get_address_of_literalLineInfo_27() { return &___literalLineInfo_27; }
	inline void set_literalLineInfo_27(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  value)
	{
		___literalLineInfo_27 = value;
	}

	inline static int32_t get_offset_of_literalQuoteChar_28() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___literalQuoteChar_28)); }
	inline Il2CppChar get_literalQuoteChar_28() const { return ___literalQuoteChar_28; }
	inline Il2CppChar* get_address_of_literalQuoteChar_28() { return &___literalQuoteChar_28; }
	inline void set_literalQuoteChar_28(Il2CppChar value)
	{
		___literalQuoteChar_28 = value;
	}

	inline static int32_t get_offset_of_documentBaseUri_29() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___documentBaseUri_29)); }
	inline String_t* get_documentBaseUri_29() const { return ___documentBaseUri_29; }
	inline String_t** get_address_of_documentBaseUri_29() { return &___documentBaseUri_29; }
	inline void set_documentBaseUri_29(String_t* value)
	{
		___documentBaseUri_29 = value;
		Il2CppCodeGenWriteBarrier((&___documentBaseUri_29), value);
	}

	inline static int32_t get_offset_of_externalDtdBaseUri_30() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___externalDtdBaseUri_30)); }
	inline String_t* get_externalDtdBaseUri_30() const { return ___externalDtdBaseUri_30; }
	inline String_t** get_address_of_externalDtdBaseUri_30() { return &___externalDtdBaseUri_30; }
	inline void set_externalDtdBaseUri_30(String_t* value)
	{
		___externalDtdBaseUri_30 = value;
		Il2CppCodeGenWriteBarrier((&___externalDtdBaseUri_30), value);
	}

	inline static int32_t get_offset_of_undeclaredNotations_31() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___undeclaredNotations_31)); }
	inline Dictionary_2_t916DC1A8D299E7D1846930151D649D79F4D9D5A2 * get_undeclaredNotations_31() const { return ___undeclaredNotations_31; }
	inline Dictionary_2_t916DC1A8D299E7D1846930151D649D79F4D9D5A2 ** get_address_of_undeclaredNotations_31() { return &___undeclaredNotations_31; }
	inline void set_undeclaredNotations_31(Dictionary_2_t916DC1A8D299E7D1846930151D649D79F4D9D5A2 * value)
	{
		___undeclaredNotations_31 = value;
		Il2CppCodeGenWriteBarrier((&___undeclaredNotations_31), value);
	}

	inline static int32_t get_offset_of_condSectionEntityIds_32() { return static_cast<int32_t>(offsetof(DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574, ___condSectionEntityIds_32)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_condSectionEntityIds_32() const { return ___condSectionEntityIds_32; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_condSectionEntityIds_32() { return &___condSectionEntityIds_32; }
	inline void set_condSectionEntityIds_32(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___condSectionEntityIds_32 = value;
		Il2CppCodeGenWriteBarrier((&___condSectionEntityIds_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDPARSER_T9C63F125C98379A2AD3996648E8B7C234EA56574_H
#ifndef XMLATTRIBUTECOLLECTION_TF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E_H
#define XMLATTRIBUTECOLLECTION_TF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlAttributeCollection
struct  XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E  : public XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLATTRIBUTECOLLECTION_TF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E_H
#ifndef XMLCDATASECTION_T4DFDF8D529E42121C9B3A0EE52BD6B5AEC1F9709_H
#define XMLCDATASECTION_T4DFDF8D529E42121C9B3A0EE52BD6B5AEC1F9709_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCDataSection
struct  XmlCDataSection_t4DFDF8D529E42121C9B3A0EE52BD6B5AEC1F9709  : public XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCDATASECTION_T4DFDF8D529E42121C9B3A0EE52BD6B5AEC1F9709_H
#ifndef XMLCOMMENT_TBC2125BFD52EF7590FE6C06B2D92C61FF344860D_H
#define XMLCOMMENT_TBC2125BFD52EF7590FE6C06B2D92C61FF344860D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlComment
struct  XmlComment_tBC2125BFD52EF7590FE6C06B2D92C61FF344860D  : public XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCOMMENT_TBC2125BFD52EF7590FE6C06B2D92C61FF344860D_H
#ifndef XMLNODECHANGEDEVENTARGS_T0376E69AE8B0ED655552CD4B073F3213AE414BDA_H
#define XMLNODECHANGEDEVENTARGS_T0376E69AE8B0ED655552CD4B073F3213AE414BDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventArgs
struct  XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Xml.XmlNodeChangedAction System.Xml.XmlNodeChangedEventArgs::action
	int32_t ___action_1;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::node
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___node_2;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::oldParent
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___oldParent_3;
	// System.Xml.XmlNode System.Xml.XmlNodeChangedEventArgs::newParent
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___newParent_4;
	// System.String System.Xml.XmlNodeChangedEventArgs::oldValue
	String_t* ___oldValue_5;
	// System.String System.Xml.XmlNodeChangedEventArgs::newValue
	String_t* ___newValue_6;

public:
	inline static int32_t get_offset_of_action_1() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA, ___action_1)); }
	inline int32_t get_action_1() const { return ___action_1; }
	inline int32_t* get_address_of_action_1() { return &___action_1; }
	inline void set_action_1(int32_t value)
	{
		___action_1 = value;
	}

	inline static int32_t get_offset_of_node_2() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA, ___node_2)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_node_2() const { return ___node_2; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_node_2() { return &___node_2; }
	inline void set_node_2(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___node_2 = value;
		Il2CppCodeGenWriteBarrier((&___node_2), value);
	}

	inline static int32_t get_offset_of_oldParent_3() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA, ___oldParent_3)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_oldParent_3() const { return ___oldParent_3; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_oldParent_3() { return &___oldParent_3; }
	inline void set_oldParent_3(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___oldParent_3 = value;
		Il2CppCodeGenWriteBarrier((&___oldParent_3), value);
	}

	inline static int32_t get_offset_of_newParent_4() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA, ___newParent_4)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_newParent_4() const { return ___newParent_4; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_newParent_4() { return &___newParent_4; }
	inline void set_newParent_4(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___newParent_4 = value;
		Il2CppCodeGenWriteBarrier((&___newParent_4), value);
	}

	inline static int32_t get_offset_of_oldValue_5() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA, ___oldValue_5)); }
	inline String_t* get_oldValue_5() const { return ___oldValue_5; }
	inline String_t** get_address_of_oldValue_5() { return &___oldValue_5; }
	inline void set_oldValue_5(String_t* value)
	{
		___oldValue_5 = value;
		Il2CppCodeGenWriteBarrier((&___oldValue_5), value);
	}

	inline static int32_t get_offset_of_newValue_6() { return static_cast<int32_t>(offsetof(XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA, ___newValue_6)); }
	inline String_t* get_newValue_6() const { return ___newValue_6; }
	inline String_t** get_address_of_newValue_6() { return &___newValue_6; }
	inline void set_newValue_6(String_t* value)
	{
		___newValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___newValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTARGS_T0376E69AE8B0ED655552CD4B073F3213AE414BDA_H
#ifndef XMLNODEREADER_T6AD91B6CB41F2761AC4C77F0968339AE7310DBE6_H
#define XMLNODEREADER_T6AD91B6CB41F2761AC4C77F0968339AE7310DBE6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeReader
struct  XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlNodeReaderNavigator System.Xml.XmlNodeReader::readerNav
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B * ___readerNav_3;
	// System.Xml.XmlNodeType System.Xml.XmlNodeReader::nodeType
	int32_t ___nodeType_4;
	// System.Int32 System.Xml.XmlNodeReader::curDepth
	int32_t ___curDepth_5;
	// System.Xml.ReadState System.Xml.XmlNodeReader::readState
	int32_t ___readState_6;
	// System.Boolean System.Xml.XmlNodeReader::fEOF
	bool ___fEOF_7;
	// System.Boolean System.Xml.XmlNodeReader::bResolveEntity
	bool ___bResolveEntity_8;
	// System.Boolean System.Xml.XmlNodeReader::bStartFromDocument
	bool ___bStartFromDocument_9;
	// System.Boolean System.Xml.XmlNodeReader::bInReadBinary
	bool ___bInReadBinary_10;
	// System.Xml.ReadContentAsBinaryHelper System.Xml.XmlNodeReader::readBinaryHelper
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * ___readBinaryHelper_11;

public:
	inline static int32_t get_offset_of_readerNav_3() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___readerNav_3)); }
	inline XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B * get_readerNav_3() const { return ___readerNav_3; }
	inline XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B ** get_address_of_readerNav_3() { return &___readerNav_3; }
	inline void set_readerNav_3(XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B * value)
	{
		___readerNav_3 = value;
		Il2CppCodeGenWriteBarrier((&___readerNav_3), value);
	}

	inline static int32_t get_offset_of_nodeType_4() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___nodeType_4)); }
	inline int32_t get_nodeType_4() const { return ___nodeType_4; }
	inline int32_t* get_address_of_nodeType_4() { return &___nodeType_4; }
	inline void set_nodeType_4(int32_t value)
	{
		___nodeType_4 = value;
	}

	inline static int32_t get_offset_of_curDepth_5() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___curDepth_5)); }
	inline int32_t get_curDepth_5() const { return ___curDepth_5; }
	inline int32_t* get_address_of_curDepth_5() { return &___curDepth_5; }
	inline void set_curDepth_5(int32_t value)
	{
		___curDepth_5 = value;
	}

	inline static int32_t get_offset_of_readState_6() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___readState_6)); }
	inline int32_t get_readState_6() const { return ___readState_6; }
	inline int32_t* get_address_of_readState_6() { return &___readState_6; }
	inline void set_readState_6(int32_t value)
	{
		___readState_6 = value;
	}

	inline static int32_t get_offset_of_fEOF_7() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___fEOF_7)); }
	inline bool get_fEOF_7() const { return ___fEOF_7; }
	inline bool* get_address_of_fEOF_7() { return &___fEOF_7; }
	inline void set_fEOF_7(bool value)
	{
		___fEOF_7 = value;
	}

	inline static int32_t get_offset_of_bResolveEntity_8() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___bResolveEntity_8)); }
	inline bool get_bResolveEntity_8() const { return ___bResolveEntity_8; }
	inline bool* get_address_of_bResolveEntity_8() { return &___bResolveEntity_8; }
	inline void set_bResolveEntity_8(bool value)
	{
		___bResolveEntity_8 = value;
	}

	inline static int32_t get_offset_of_bStartFromDocument_9() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___bStartFromDocument_9)); }
	inline bool get_bStartFromDocument_9() const { return ___bStartFromDocument_9; }
	inline bool* get_address_of_bStartFromDocument_9() { return &___bStartFromDocument_9; }
	inline void set_bStartFromDocument_9(bool value)
	{
		___bStartFromDocument_9 = value;
	}

	inline static int32_t get_offset_of_bInReadBinary_10() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___bInReadBinary_10)); }
	inline bool get_bInReadBinary_10() const { return ___bInReadBinary_10; }
	inline bool* get_address_of_bInReadBinary_10() { return &___bInReadBinary_10; }
	inline void set_bInReadBinary_10(bool value)
	{
		___bInReadBinary_10 = value;
	}

	inline static int32_t get_offset_of_readBinaryHelper_11() { return static_cast<int32_t>(offsetof(XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6, ___readBinaryHelper_11)); }
	inline ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * get_readBinaryHelper_11() const { return ___readBinaryHelper_11; }
	inline ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 ** get_address_of_readBinaryHelper_11() { return &___readBinaryHelper_11; }
	inline void set_readBinaryHelper_11(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * value)
	{
		___readBinaryHelper_11 = value;
		Il2CppCodeGenWriteBarrier((&___readBinaryHelper_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEREADER_T6AD91B6CB41F2761AC4C77F0968339AE7310DBE6_H
#ifndef XMLSIGNIFICANTWHITESPACE_TCCDC4754CB2DD2682B9FDAE23DBF09603618F5FB_H
#define XMLSIGNIFICANTWHITESPACE_TCCDC4754CB2DD2682B9FDAE23DBF09603618F5FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlSignificantWhitespace
struct  XmlSignificantWhitespace_tCCDC4754CB2DD2682B9FDAE23DBF09603618F5FB  : public XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSIGNIFICANTWHITESPACE_TCCDC4754CB2DD2682B9FDAE23DBF09603618F5FB_H
#ifndef XMLTEXT_T9C88A0254C370EBF90FC518EA99EF5D1B7D9E649_H
#define XMLTEXT_T9C88A0254C370EBF90FC518EA99EF5D1B7D9E649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlText
struct  XmlText_t9C88A0254C370EBF90FC518EA99EF5D1B7D9E649  : public XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXT_T9C88A0254C370EBF90FC518EA99EF5D1B7D9E649_H
#ifndef LATERINITPARAM_TE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF_H
#define LATERINITPARAM_TE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/LaterInitParam
struct  LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlTextReaderImpl/LaterInitParam::useAsync
	bool ___useAsync_0;
	// System.IO.Stream System.Xml.XmlTextReaderImpl/LaterInitParam::inputStream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___inputStream_1;
	// System.Byte[] System.Xml.XmlTextReaderImpl/LaterInitParam::inputBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___inputBytes_2;
	// System.Int32 System.Xml.XmlTextReaderImpl/LaterInitParam::inputByteCount
	int32_t ___inputByteCount_3;
	// System.Uri System.Xml.XmlTextReaderImpl/LaterInitParam::inputbaseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___inputbaseUri_4;
	// System.String System.Xml.XmlTextReaderImpl/LaterInitParam::inputUriStr
	String_t* ___inputUriStr_5;
	// System.Xml.XmlResolver System.Xml.XmlTextReaderImpl/LaterInitParam::inputUriResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___inputUriResolver_6;
	// System.Xml.XmlParserContext System.Xml.XmlTextReaderImpl/LaterInitParam::inputContext
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * ___inputContext_7;
	// System.IO.TextReader System.Xml.XmlTextReaderImpl/LaterInitParam::inputTextReader
	TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * ___inputTextReader_8;
	// System.Xml.XmlTextReaderImpl/InitInputType System.Xml.XmlTextReaderImpl/LaterInitParam::initType
	int32_t ___initType_9;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_inputStream_1() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputStream_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_inputStream_1() const { return ___inputStream_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_inputStream_1() { return &___inputStream_1; }
	inline void set_inputStream_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___inputStream_1 = value;
		Il2CppCodeGenWriteBarrier((&___inputStream_1), value);
	}

	inline static int32_t get_offset_of_inputBytes_2() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputBytes_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_inputBytes_2() const { return ___inputBytes_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_inputBytes_2() { return &___inputBytes_2; }
	inline void set_inputBytes_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___inputBytes_2 = value;
		Il2CppCodeGenWriteBarrier((&___inputBytes_2), value);
	}

	inline static int32_t get_offset_of_inputByteCount_3() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputByteCount_3)); }
	inline int32_t get_inputByteCount_3() const { return ___inputByteCount_3; }
	inline int32_t* get_address_of_inputByteCount_3() { return &___inputByteCount_3; }
	inline void set_inputByteCount_3(int32_t value)
	{
		___inputByteCount_3 = value;
	}

	inline static int32_t get_offset_of_inputbaseUri_4() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputbaseUri_4)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_inputbaseUri_4() const { return ___inputbaseUri_4; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_inputbaseUri_4() { return &___inputbaseUri_4; }
	inline void set_inputbaseUri_4(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___inputbaseUri_4 = value;
		Il2CppCodeGenWriteBarrier((&___inputbaseUri_4), value);
	}

	inline static int32_t get_offset_of_inputUriStr_5() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputUriStr_5)); }
	inline String_t* get_inputUriStr_5() const { return ___inputUriStr_5; }
	inline String_t** get_address_of_inputUriStr_5() { return &___inputUriStr_5; }
	inline void set_inputUriStr_5(String_t* value)
	{
		___inputUriStr_5 = value;
		Il2CppCodeGenWriteBarrier((&___inputUriStr_5), value);
	}

	inline static int32_t get_offset_of_inputUriResolver_6() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputUriResolver_6)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_inputUriResolver_6() const { return ___inputUriResolver_6; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_inputUriResolver_6() { return &___inputUriResolver_6; }
	inline void set_inputUriResolver_6(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___inputUriResolver_6 = value;
		Il2CppCodeGenWriteBarrier((&___inputUriResolver_6), value);
	}

	inline static int32_t get_offset_of_inputContext_7() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputContext_7)); }
	inline XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * get_inputContext_7() const { return ___inputContext_7; }
	inline XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 ** get_address_of_inputContext_7() { return &___inputContext_7; }
	inline void set_inputContext_7(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * value)
	{
		___inputContext_7 = value;
		Il2CppCodeGenWriteBarrier((&___inputContext_7), value);
	}

	inline static int32_t get_offset_of_inputTextReader_8() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___inputTextReader_8)); }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * get_inputTextReader_8() const { return ___inputTextReader_8; }
	inline TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A ** get_address_of_inputTextReader_8() { return &___inputTextReader_8; }
	inline void set_inputTextReader_8(TextReader_t7DF8314B601D202ECFEDF623093A87BFDAB58D0A * value)
	{
		___inputTextReader_8 = value;
		Il2CppCodeGenWriteBarrier((&___inputTextReader_8), value);
	}

	inline static int32_t get_offset_of_initType_9() { return static_cast<int32_t>(offsetof(LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF, ___initType_9)); }
	inline int32_t get_initType_9() const { return ___initType_9; }
	inline int32_t* get_address_of_initType_9() { return &___initType_9; }
	inline void set_initType_9(int32_t value)
	{
		___initType_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LATERINITPARAM_TE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF_H
#ifndef NODEDATA_TAACF4D5ECADB9124568D01525E72D14495BC5ACF_H
#define NODEDATA_TAACF4D5ECADB9124568D01525E72D14495BC5ACF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/NodeData
struct  NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF  : public RuntimeObject
{
public:
	// System.Xml.XmlNodeType System.Xml.XmlTextReaderImpl/NodeData::type
	int32_t ___type_1;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::localName
	String_t* ___localName_2;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::prefix
	String_t* ___prefix_3;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::ns
	String_t* ___ns_4;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::nameWPrefix
	String_t* ___nameWPrefix_5;
	// System.String System.Xml.XmlTextReaderImpl/NodeData::value
	String_t* ___value_6;
	// System.Char[] System.Xml.XmlTextReaderImpl/NodeData::chars
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___chars_7;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::valueStartPos
	int32_t ___valueStartPos_8;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::valueLength
	int32_t ___valueLength_9;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl/NodeData::lineInfo
	LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  ___lineInfo_10;
	// System.Xml.LineInfo System.Xml.XmlTextReaderImpl/NodeData::lineInfo2
	LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  ___lineInfo2_11;
	// System.Char System.Xml.XmlTextReaderImpl/NodeData::quoteChar
	Il2CppChar ___quoteChar_12;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::depth
	int32_t ___depth_13;
	// System.Boolean System.Xml.XmlTextReaderImpl/NodeData::isEmptyOrDefault
	bool ___isEmptyOrDefault_14;
	// System.Int32 System.Xml.XmlTextReaderImpl/NodeData::entityId
	int32_t ___entityId_15;
	// System.Boolean System.Xml.XmlTextReaderImpl/NodeData::xmlContextPushed
	bool ___xmlContextPushed_16;
	// System.Xml.XmlTextReaderImpl/NodeData System.Xml.XmlTextReaderImpl/NodeData::nextAttrValueChunk
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * ___nextAttrValueChunk_17;
	// System.Object System.Xml.XmlTextReaderImpl/NodeData::schemaType
	RuntimeObject * ___schemaType_18;
	// System.Object System.Xml.XmlTextReaderImpl/NodeData::typedValue
	RuntimeObject * ___typedValue_19;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_prefix_3() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___prefix_3)); }
	inline String_t* get_prefix_3() const { return ___prefix_3; }
	inline String_t** get_address_of_prefix_3() { return &___prefix_3; }
	inline void set_prefix_3(String_t* value)
	{
		___prefix_3 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_3), value);
	}

	inline static int32_t get_offset_of_ns_4() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___ns_4)); }
	inline String_t* get_ns_4() const { return ___ns_4; }
	inline String_t** get_address_of_ns_4() { return &___ns_4; }
	inline void set_ns_4(String_t* value)
	{
		___ns_4 = value;
		Il2CppCodeGenWriteBarrier((&___ns_4), value);
	}

	inline static int32_t get_offset_of_nameWPrefix_5() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___nameWPrefix_5)); }
	inline String_t* get_nameWPrefix_5() const { return ___nameWPrefix_5; }
	inline String_t** get_address_of_nameWPrefix_5() { return &___nameWPrefix_5; }
	inline void set_nameWPrefix_5(String_t* value)
	{
		___nameWPrefix_5 = value;
		Il2CppCodeGenWriteBarrier((&___nameWPrefix_5), value);
	}

	inline static int32_t get_offset_of_value_6() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___value_6)); }
	inline String_t* get_value_6() const { return ___value_6; }
	inline String_t** get_address_of_value_6() { return &___value_6; }
	inline void set_value_6(String_t* value)
	{
		___value_6 = value;
		Il2CppCodeGenWriteBarrier((&___value_6), value);
	}

	inline static int32_t get_offset_of_chars_7() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___chars_7)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_chars_7() const { return ___chars_7; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_chars_7() { return &___chars_7; }
	inline void set_chars_7(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___chars_7 = value;
		Il2CppCodeGenWriteBarrier((&___chars_7), value);
	}

	inline static int32_t get_offset_of_valueStartPos_8() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___valueStartPos_8)); }
	inline int32_t get_valueStartPos_8() const { return ___valueStartPos_8; }
	inline int32_t* get_address_of_valueStartPos_8() { return &___valueStartPos_8; }
	inline void set_valueStartPos_8(int32_t value)
	{
		___valueStartPos_8 = value;
	}

	inline static int32_t get_offset_of_valueLength_9() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___valueLength_9)); }
	inline int32_t get_valueLength_9() const { return ___valueLength_9; }
	inline int32_t* get_address_of_valueLength_9() { return &___valueLength_9; }
	inline void set_valueLength_9(int32_t value)
	{
		___valueLength_9 = value;
	}

	inline static int32_t get_offset_of_lineInfo_10() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___lineInfo_10)); }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  get_lineInfo_10() const { return ___lineInfo_10; }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 * get_address_of_lineInfo_10() { return &___lineInfo_10; }
	inline void set_lineInfo_10(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  value)
	{
		___lineInfo_10 = value;
	}

	inline static int32_t get_offset_of_lineInfo2_11() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___lineInfo2_11)); }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  get_lineInfo2_11() const { return ___lineInfo2_11; }
	inline LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 * get_address_of_lineInfo2_11() { return &___lineInfo2_11; }
	inline void set_lineInfo2_11(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5  value)
	{
		___lineInfo2_11 = value;
	}

	inline static int32_t get_offset_of_quoteChar_12() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___quoteChar_12)); }
	inline Il2CppChar get_quoteChar_12() const { return ___quoteChar_12; }
	inline Il2CppChar* get_address_of_quoteChar_12() { return &___quoteChar_12; }
	inline void set_quoteChar_12(Il2CppChar value)
	{
		___quoteChar_12 = value;
	}

	inline static int32_t get_offset_of_depth_13() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___depth_13)); }
	inline int32_t get_depth_13() const { return ___depth_13; }
	inline int32_t* get_address_of_depth_13() { return &___depth_13; }
	inline void set_depth_13(int32_t value)
	{
		___depth_13 = value;
	}

	inline static int32_t get_offset_of_isEmptyOrDefault_14() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___isEmptyOrDefault_14)); }
	inline bool get_isEmptyOrDefault_14() const { return ___isEmptyOrDefault_14; }
	inline bool* get_address_of_isEmptyOrDefault_14() { return &___isEmptyOrDefault_14; }
	inline void set_isEmptyOrDefault_14(bool value)
	{
		___isEmptyOrDefault_14 = value;
	}

	inline static int32_t get_offset_of_entityId_15() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___entityId_15)); }
	inline int32_t get_entityId_15() const { return ___entityId_15; }
	inline int32_t* get_address_of_entityId_15() { return &___entityId_15; }
	inline void set_entityId_15(int32_t value)
	{
		___entityId_15 = value;
	}

	inline static int32_t get_offset_of_xmlContextPushed_16() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___xmlContextPushed_16)); }
	inline bool get_xmlContextPushed_16() const { return ___xmlContextPushed_16; }
	inline bool* get_address_of_xmlContextPushed_16() { return &___xmlContextPushed_16; }
	inline void set_xmlContextPushed_16(bool value)
	{
		___xmlContextPushed_16 = value;
	}

	inline static int32_t get_offset_of_nextAttrValueChunk_17() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___nextAttrValueChunk_17)); }
	inline NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * get_nextAttrValueChunk_17() const { return ___nextAttrValueChunk_17; }
	inline NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF ** get_address_of_nextAttrValueChunk_17() { return &___nextAttrValueChunk_17; }
	inline void set_nextAttrValueChunk_17(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * value)
	{
		___nextAttrValueChunk_17 = value;
		Il2CppCodeGenWriteBarrier((&___nextAttrValueChunk_17), value);
	}

	inline static int32_t get_offset_of_schemaType_18() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___schemaType_18)); }
	inline RuntimeObject * get_schemaType_18() const { return ___schemaType_18; }
	inline RuntimeObject ** get_address_of_schemaType_18() { return &___schemaType_18; }
	inline void set_schemaType_18(RuntimeObject * value)
	{
		___schemaType_18 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_18), value);
	}

	inline static int32_t get_offset_of_typedValue_19() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF, ___typedValue_19)); }
	inline RuntimeObject * get_typedValue_19() const { return ___typedValue_19; }
	inline RuntimeObject ** get_address_of_typedValue_19() { return &___typedValue_19; }
	inline void set_typedValue_19(RuntimeObject * value)
	{
		___typedValue_19 = value;
		Il2CppCodeGenWriteBarrier((&___typedValue_19), value);
	}
};

struct NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF_StaticFields
{
public:
	// System.Xml.XmlTextReaderImpl/NodeData modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlTextReaderImpl/NodeData::s_None
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * ___s_None_0;

public:
	inline static int32_t get_offset_of_s_None_0() { return static_cast<int32_t>(offsetof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF_StaticFields, ___s_None_0)); }
	inline NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * get_s_None_0() const { return ___s_None_0; }
	inline NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF ** get_address_of_s_None_0() { return &___s_None_0; }
	inline void set_s_None_0(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF * value)
	{
		___s_None_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_None_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NODEDATA_TAACF4D5ECADB9124568D01525E72D14495BC5ACF_H
#ifndef XMLCONTEXT_TC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52_H
#define XMLCONTEXT_TC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/XmlContext
struct  XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52  : public RuntimeObject
{
public:
	// System.Xml.XmlSpace System.Xml.XmlTextReaderImpl/XmlContext::xmlSpace
	int32_t ___xmlSpace_0;
	// System.String System.Xml.XmlTextReaderImpl/XmlContext::xmlLang
	String_t* ___xmlLang_1;
	// System.String System.Xml.XmlTextReaderImpl/XmlContext::defaultNamespace
	String_t* ___defaultNamespace_2;
	// System.Xml.XmlTextReaderImpl/XmlContext System.Xml.XmlTextReaderImpl/XmlContext::previousContext
	XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 * ___previousContext_3;

public:
	inline static int32_t get_offset_of_xmlSpace_0() { return static_cast<int32_t>(offsetof(XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52, ___xmlSpace_0)); }
	inline int32_t get_xmlSpace_0() const { return ___xmlSpace_0; }
	inline int32_t* get_address_of_xmlSpace_0() { return &___xmlSpace_0; }
	inline void set_xmlSpace_0(int32_t value)
	{
		___xmlSpace_0 = value;
	}

	inline static int32_t get_offset_of_xmlLang_1() { return static_cast<int32_t>(offsetof(XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52, ___xmlLang_1)); }
	inline String_t* get_xmlLang_1() const { return ___xmlLang_1; }
	inline String_t** get_address_of_xmlLang_1() { return &___xmlLang_1; }
	inline void set_xmlLang_1(String_t* value)
	{
		___xmlLang_1 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_1), value);
	}

	inline static int32_t get_offset_of_defaultNamespace_2() { return static_cast<int32_t>(offsetof(XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52, ___defaultNamespace_2)); }
	inline String_t* get_defaultNamespace_2() const { return ___defaultNamespace_2; }
	inline String_t** get_address_of_defaultNamespace_2() { return &___defaultNamespace_2; }
	inline void set_defaultNamespace_2(String_t* value)
	{
		___defaultNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNamespace_2), value);
	}

	inline static int32_t get_offset_of_previousContext_3() { return static_cast<int32_t>(offsetof(XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52, ___previousContext_3)); }
	inline XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 * get_previousContext_3() const { return ___previousContext_3; }
	inline XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 ** get_address_of_previousContext_3() { return &___previousContext_3; }
	inline void set_previousContext_3(XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52 * value)
	{
		___previousContext_3 = value;
		Il2CppCodeGenWriteBarrier((&___previousContext_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLCONTEXT_TC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52_H
#ifndef XMLTEXTWRITER_TAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_H
#define XMLTEXTWRITER_TAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter
struct  XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.IO.TextWriter System.Xml.XmlTextWriter::textWriter
	TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * ___textWriter_1;
	// System.Xml.XmlTextEncoder System.Xml.XmlTextWriter::xmlEncoder
	XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * ___xmlEncoder_2;
	// System.Text.Encoding System.Xml.XmlTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_3;
	// System.Xml.Formatting System.Xml.XmlTextWriter::formatting
	int32_t ___formatting_4;
	// System.Boolean System.Xml.XmlTextWriter::indented
	bool ___indented_5;
	// System.Int32 System.Xml.XmlTextWriter::indentation
	int32_t ___indentation_6;
	// System.Char System.Xml.XmlTextWriter::indentChar
	Il2CppChar ___indentChar_7;
	// System.Xml.XmlTextWriter/TagInfo[] System.Xml.XmlTextWriter::stack
	TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* ___stack_8;
	// System.Int32 System.Xml.XmlTextWriter::top
	int32_t ___top_9;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTable
	StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* ___stateTable_10;
	// System.Xml.XmlTextWriter/State System.Xml.XmlTextWriter::currentState
	int32_t ___currentState_11;
	// System.Xml.XmlTextWriter/Token System.Xml.XmlTextWriter::lastToken
	int32_t ___lastToken_12;
	// System.Xml.XmlTextWriterBase64Encoder System.Xml.XmlTextWriter::base64Encoder
	XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * ___base64Encoder_13;
	// System.Char System.Xml.XmlTextWriter::quoteChar
	Il2CppChar ___quoteChar_14;
	// System.Char System.Xml.XmlTextWriter::curQuoteChar
	Il2CppChar ___curQuoteChar_15;
	// System.Boolean System.Xml.XmlTextWriter::namespaces
	bool ___namespaces_16;
	// System.Xml.XmlTextWriter/SpecialAttr System.Xml.XmlTextWriter::specialAttr
	int32_t ___specialAttr_17;
	// System.String System.Xml.XmlTextWriter::prefixForXmlNs
	String_t* ___prefixForXmlNs_18;
	// System.Boolean System.Xml.XmlTextWriter::flush
	bool ___flush_19;
	// System.Xml.XmlTextWriter/Namespace[] System.Xml.XmlTextWriter::nsStack
	NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* ___nsStack_20;
	// System.Int32 System.Xml.XmlTextWriter::nsTop
	int32_t ___nsTop_21;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlTextWriter::nsHashtable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___nsHashtable_22;
	// System.Boolean System.Xml.XmlTextWriter::useNsHashtable
	bool ___useNsHashtable_23;
	// System.Xml.XmlCharType System.Xml.XmlTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_24;

public:
	inline static int32_t get_offset_of_textWriter_1() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___textWriter_1)); }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * get_textWriter_1() const { return ___textWriter_1; }
	inline TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 ** get_address_of_textWriter_1() { return &___textWriter_1; }
	inline void set_textWriter_1(TextWriter_t92451D929322093838C41489883D5B2D7ABAF3F0 * value)
	{
		___textWriter_1 = value;
		Il2CppCodeGenWriteBarrier((&___textWriter_1), value);
	}

	inline static int32_t get_offset_of_xmlEncoder_2() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___xmlEncoder_2)); }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * get_xmlEncoder_2() const { return ___xmlEncoder_2; }
	inline XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 ** get_address_of_xmlEncoder_2() { return &___xmlEncoder_2; }
	inline void set_xmlEncoder_2(XmlTextEncoder_t444AD6ADF242669731301011B84C2A007B08A475 * value)
	{
		___xmlEncoder_2 = value;
		Il2CppCodeGenWriteBarrier((&___xmlEncoder_2), value);
	}

	inline static int32_t get_offset_of_encoding_3() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___encoding_3)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_3() const { return ___encoding_3; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_3() { return &___encoding_3; }
	inline void set_encoding_3(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_3 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_3), value);
	}

	inline static int32_t get_offset_of_formatting_4() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___formatting_4)); }
	inline int32_t get_formatting_4() const { return ___formatting_4; }
	inline int32_t* get_address_of_formatting_4() { return &___formatting_4; }
	inline void set_formatting_4(int32_t value)
	{
		___formatting_4 = value;
	}

	inline static int32_t get_offset_of_indented_5() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___indented_5)); }
	inline bool get_indented_5() const { return ___indented_5; }
	inline bool* get_address_of_indented_5() { return &___indented_5; }
	inline void set_indented_5(bool value)
	{
		___indented_5 = value;
	}

	inline static int32_t get_offset_of_indentation_6() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___indentation_6)); }
	inline int32_t get_indentation_6() const { return ___indentation_6; }
	inline int32_t* get_address_of_indentation_6() { return &___indentation_6; }
	inline void set_indentation_6(int32_t value)
	{
		___indentation_6 = value;
	}

	inline static int32_t get_offset_of_indentChar_7() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___indentChar_7)); }
	inline Il2CppChar get_indentChar_7() const { return ___indentChar_7; }
	inline Il2CppChar* get_address_of_indentChar_7() { return &___indentChar_7; }
	inline void set_indentChar_7(Il2CppChar value)
	{
		___indentChar_7 = value;
	}

	inline static int32_t get_offset_of_stack_8() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___stack_8)); }
	inline TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* get_stack_8() const { return ___stack_8; }
	inline TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910** get_address_of_stack_8() { return &___stack_8; }
	inline void set_stack_8(TagInfoU5BU5D_tD76375D724727EC61E004D83983EF983E350C910* value)
	{
		___stack_8 = value;
		Il2CppCodeGenWriteBarrier((&___stack_8), value);
	}

	inline static int32_t get_offset_of_top_9() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___top_9)); }
	inline int32_t get_top_9() const { return ___top_9; }
	inline int32_t* get_address_of_top_9() { return &___top_9; }
	inline void set_top_9(int32_t value)
	{
		___top_9 = value;
	}

	inline static int32_t get_offset_of_stateTable_10() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___stateTable_10)); }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* get_stateTable_10() const { return ___stateTable_10; }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2** get_address_of_stateTable_10() { return &___stateTable_10; }
	inline void set_stateTable_10(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* value)
	{
		___stateTable_10 = value;
		Il2CppCodeGenWriteBarrier((&___stateTable_10), value);
	}

	inline static int32_t get_offset_of_currentState_11() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___currentState_11)); }
	inline int32_t get_currentState_11() const { return ___currentState_11; }
	inline int32_t* get_address_of_currentState_11() { return &___currentState_11; }
	inline void set_currentState_11(int32_t value)
	{
		___currentState_11 = value;
	}

	inline static int32_t get_offset_of_lastToken_12() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___lastToken_12)); }
	inline int32_t get_lastToken_12() const { return ___lastToken_12; }
	inline int32_t* get_address_of_lastToken_12() { return &___lastToken_12; }
	inline void set_lastToken_12(int32_t value)
	{
		___lastToken_12 = value;
	}

	inline static int32_t get_offset_of_base64Encoder_13() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___base64Encoder_13)); }
	inline XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * get_base64Encoder_13() const { return ___base64Encoder_13; }
	inline XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C ** get_address_of_base64Encoder_13() { return &___base64Encoder_13; }
	inline void set_base64Encoder_13(XmlTextWriterBase64Encoder_t1362FFBE93C9BC675B4A34503DB31AA6D3AAF01C * value)
	{
		___base64Encoder_13 = value;
		Il2CppCodeGenWriteBarrier((&___base64Encoder_13), value);
	}

	inline static int32_t get_offset_of_quoteChar_14() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___quoteChar_14)); }
	inline Il2CppChar get_quoteChar_14() const { return ___quoteChar_14; }
	inline Il2CppChar* get_address_of_quoteChar_14() { return &___quoteChar_14; }
	inline void set_quoteChar_14(Il2CppChar value)
	{
		___quoteChar_14 = value;
	}

	inline static int32_t get_offset_of_curQuoteChar_15() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___curQuoteChar_15)); }
	inline Il2CppChar get_curQuoteChar_15() const { return ___curQuoteChar_15; }
	inline Il2CppChar* get_address_of_curQuoteChar_15() { return &___curQuoteChar_15; }
	inline void set_curQuoteChar_15(Il2CppChar value)
	{
		___curQuoteChar_15 = value;
	}

	inline static int32_t get_offset_of_namespaces_16() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___namespaces_16)); }
	inline bool get_namespaces_16() const { return ___namespaces_16; }
	inline bool* get_address_of_namespaces_16() { return &___namespaces_16; }
	inline void set_namespaces_16(bool value)
	{
		___namespaces_16 = value;
	}

	inline static int32_t get_offset_of_specialAttr_17() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___specialAttr_17)); }
	inline int32_t get_specialAttr_17() const { return ___specialAttr_17; }
	inline int32_t* get_address_of_specialAttr_17() { return &___specialAttr_17; }
	inline void set_specialAttr_17(int32_t value)
	{
		___specialAttr_17 = value;
	}

	inline static int32_t get_offset_of_prefixForXmlNs_18() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___prefixForXmlNs_18)); }
	inline String_t* get_prefixForXmlNs_18() const { return ___prefixForXmlNs_18; }
	inline String_t** get_address_of_prefixForXmlNs_18() { return &___prefixForXmlNs_18; }
	inline void set_prefixForXmlNs_18(String_t* value)
	{
		___prefixForXmlNs_18 = value;
		Il2CppCodeGenWriteBarrier((&___prefixForXmlNs_18), value);
	}

	inline static int32_t get_offset_of_flush_19() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___flush_19)); }
	inline bool get_flush_19() const { return ___flush_19; }
	inline bool* get_address_of_flush_19() { return &___flush_19; }
	inline void set_flush_19(bool value)
	{
		___flush_19 = value;
	}

	inline static int32_t get_offset_of_nsStack_20() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___nsStack_20)); }
	inline NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* get_nsStack_20() const { return ___nsStack_20; }
	inline NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829** get_address_of_nsStack_20() { return &___nsStack_20; }
	inline void set_nsStack_20(NamespaceU5BU5D_t543949D15727D25A9D2882E32191DE4C5DBD9829* value)
	{
		___nsStack_20 = value;
		Il2CppCodeGenWriteBarrier((&___nsStack_20), value);
	}

	inline static int32_t get_offset_of_nsTop_21() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___nsTop_21)); }
	inline int32_t get_nsTop_21() const { return ___nsTop_21; }
	inline int32_t* get_address_of_nsTop_21() { return &___nsTop_21; }
	inline void set_nsTop_21(int32_t value)
	{
		___nsTop_21 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_22() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___nsHashtable_22)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_nsHashtable_22() const { return ___nsHashtable_22; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_nsHashtable_22() { return &___nsHashtable_22; }
	inline void set_nsHashtable_22(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___nsHashtable_22 = value;
		Il2CppCodeGenWriteBarrier((&___nsHashtable_22), value);
	}

	inline static int32_t get_offset_of_useNsHashtable_23() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___useNsHashtable_23)); }
	inline bool get_useNsHashtable_23() const { return ___useNsHashtable_23; }
	inline bool* get_address_of_useNsHashtable_23() { return &___useNsHashtable_23; }
	inline void set_useNsHashtable_23(bool value)
	{
		___useNsHashtable_23 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_24() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6, ___xmlCharType_24)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_24() const { return ___xmlCharType_24; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_24() { return &___xmlCharType_24; }
	inline void set_xmlCharType_24(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_24 = value;
	}
};

struct XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields
{
public:
	// System.String[] System.Xml.XmlTextWriter::stateName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___stateName_25;
	// System.String[] System.Xml.XmlTextWriter::tokenName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tokenName_26;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTableDefault
	StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* ___stateTableDefault_27;
	// System.Xml.XmlTextWriter/State[] System.Xml.XmlTextWriter::stateTableDocument
	StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* ___stateTableDocument_28;

public:
	inline static int32_t get_offset_of_stateName_25() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___stateName_25)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_stateName_25() const { return ___stateName_25; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_stateName_25() { return &___stateName_25; }
	inline void set_stateName_25(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___stateName_25 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_25), value);
	}

	inline static int32_t get_offset_of_tokenName_26() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___tokenName_26)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tokenName_26() const { return ___tokenName_26; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tokenName_26() { return &___tokenName_26; }
	inline void set_tokenName_26(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tokenName_26 = value;
		Il2CppCodeGenWriteBarrier((&___tokenName_26), value);
	}

	inline static int32_t get_offset_of_stateTableDefault_27() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___stateTableDefault_27)); }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* get_stateTableDefault_27() const { return ___stateTableDefault_27; }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2** get_address_of_stateTableDefault_27() { return &___stateTableDefault_27; }
	inline void set_stateTableDefault_27(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* value)
	{
		___stateTableDefault_27 = value;
		Il2CppCodeGenWriteBarrier((&___stateTableDefault_27), value);
	}

	inline static int32_t get_offset_of_stateTableDocument_28() { return static_cast<int32_t>(offsetof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields, ___stateTableDocument_28)); }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* get_stateTableDocument_28() const { return ___stateTableDocument_28; }
	inline StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2** get_address_of_stateTableDocument_28() { return &___stateTableDocument_28; }
	inline void set_stateTableDocument_28(StateU5BU5D_t81C65FBCFDEA7DE192DE625334AE40FB591ED4D2* value)
	{
		___stateTableDocument_28 = value;
		Il2CppCodeGenWriteBarrier((&___stateTableDocument_28), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTEXTWRITER_TAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_H
#ifndef TAGINFO_T9F395B388162AB8B9277E4ABA1ADEF785AE197B5_H
#define TAGINFO_T9F395B388162AB8B9277E4ABA1ADEF785AE197B5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextWriter/TagInfo
struct  TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5 
{
public:
	// System.String System.Xml.XmlTextWriter/TagInfo::name
	String_t* ___name_0;
	// System.String System.Xml.XmlTextWriter/TagInfo::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlTextWriter/TagInfo::defaultNs
	String_t* ___defaultNs_2;
	// System.Xml.XmlTextWriter/NamespaceState System.Xml.XmlTextWriter/TagInfo::defaultNsState
	int32_t ___defaultNsState_3;
	// System.Xml.XmlSpace System.Xml.XmlTextWriter/TagInfo::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlTextWriter/TagInfo::xmlLang
	String_t* ___xmlLang_5;
	// System.Int32 System.Xml.XmlTextWriter/TagInfo::prevNsTop
	int32_t ___prevNsTop_6;
	// System.Int32 System.Xml.XmlTextWriter/TagInfo::prefixCount
	int32_t ___prefixCount_7;
	// System.Boolean System.Xml.XmlTextWriter/TagInfo::mixed
	bool ___mixed_8;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_defaultNs_2() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___defaultNs_2)); }
	inline String_t* get_defaultNs_2() const { return ___defaultNs_2; }
	inline String_t** get_address_of_defaultNs_2() { return &___defaultNs_2; }
	inline void set_defaultNs_2(String_t* value)
	{
		___defaultNs_2 = value;
		Il2CppCodeGenWriteBarrier((&___defaultNs_2), value);
	}

	inline static int32_t get_offset_of_defaultNsState_3() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___defaultNsState_3)); }
	inline int32_t get_defaultNsState_3() const { return ___defaultNsState_3; }
	inline int32_t* get_address_of_defaultNsState_3() { return &___defaultNsState_3; }
	inline void set_defaultNsState_3(int32_t value)
	{
		___defaultNsState_3 = value;
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_5), value);
	}

	inline static int32_t get_offset_of_prevNsTop_6() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___prevNsTop_6)); }
	inline int32_t get_prevNsTop_6() const { return ___prevNsTop_6; }
	inline int32_t* get_address_of_prevNsTop_6() { return &___prevNsTop_6; }
	inline void set_prevNsTop_6(int32_t value)
	{
		___prevNsTop_6 = value;
	}

	inline static int32_t get_offset_of_prefixCount_7() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___prefixCount_7)); }
	inline int32_t get_prefixCount_7() const { return ___prefixCount_7; }
	inline int32_t* get_address_of_prefixCount_7() { return &___prefixCount_7; }
	inline void set_prefixCount_7(int32_t value)
	{
		___prefixCount_7 = value;
	}

	inline static int32_t get_offset_of_mixed_8() { return static_cast<int32_t>(offsetof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5, ___mixed_8)); }
	inline bool get_mixed_8() const { return ___mixed_8; }
	inline bool* get_address_of_mixed_8() { return &___mixed_8; }
	inline void set_mixed_8(bool value)
	{
		___mixed_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_pinvoke
{
	char* ___name_0;
	char* ___prefix_1;
	char* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};
// Native definition for COM marshalling of System.Xml.XmlTextWriter/TagInfo
struct TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_com
{
	Il2CppChar* ___name_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___defaultNs_2;
	int32_t ___defaultNsState_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
	int32_t ___prevNsTop_6;
	int32_t ___prefixCount_7;
	int32_t ___mixed_8;
};
#endif // TAGINFO_T9F395B388162AB8B9277E4ABA1ADEF785AE197B5_H
#ifndef XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#define XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriter
struct  XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E  : public XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2
{
public:
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::useAsync
	bool ___useAsync_3;
	// System.Byte[] System.Xml.XmlUtf8RawTextWriter::bufBytes
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bufBytes_4;
	// System.IO.Stream System.Xml.XmlUtf8RawTextWriter::stream
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___stream_5;
	// System.Text.Encoding System.Xml.XmlUtf8RawTextWriter::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_6;
	// System.Xml.XmlCharType System.Xml.XmlUtf8RawTextWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_7;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufPos
	int32_t ___bufPos_8;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::textPos
	int32_t ___textPos_9;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::contentPos
	int32_t ___contentPos_10;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::cdataPos
	int32_t ___cdataPos_11;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::attrEndPos
	int32_t ___attrEndPos_12;
	// System.Int32 System.Xml.XmlUtf8RawTextWriter::bufLen
	int32_t ___bufLen_13;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::writeToNull
	bool ___writeToNull_14;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::hadDoubleBracket
	bool ___hadDoubleBracket_15;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::inAttributeValue
	bool ___inAttributeValue_16;
	// System.Xml.NewLineHandling System.Xml.XmlUtf8RawTextWriter::newLineHandling
	int32_t ___newLineHandling_17;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::closeOutput
	bool ___closeOutput_18;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::omitXmlDeclaration
	bool ___omitXmlDeclaration_19;
	// System.String System.Xml.XmlUtf8RawTextWriter::newLineChars
	String_t* ___newLineChars_20;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::checkCharacters
	bool ___checkCharacters_21;
	// System.Xml.XmlStandalone System.Xml.XmlUtf8RawTextWriter::standalone
	int32_t ___standalone_22;
	// System.Xml.XmlOutputMethod System.Xml.XmlUtf8RawTextWriter::outputMethod
	int32_t ___outputMethod_23;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::autoXmlDeclaration
	bool ___autoXmlDeclaration_24;
	// System.Boolean System.Xml.XmlUtf8RawTextWriter::mergeCDataSections
	bool ___mergeCDataSections_25;

public:
	inline static int32_t get_offset_of_useAsync_3() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___useAsync_3)); }
	inline bool get_useAsync_3() const { return ___useAsync_3; }
	inline bool* get_address_of_useAsync_3() { return &___useAsync_3; }
	inline void set_useAsync_3(bool value)
	{
		___useAsync_3 = value;
	}

	inline static int32_t get_offset_of_bufBytes_4() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufBytes_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_bufBytes_4() const { return ___bufBytes_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_bufBytes_4() { return &___bufBytes_4; }
	inline void set_bufBytes_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___bufBytes_4 = value;
		Il2CppCodeGenWriteBarrier((&___bufBytes_4), value);
	}

	inline static int32_t get_offset_of_stream_5() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___stream_5)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_stream_5() const { return ___stream_5; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_stream_5() { return &___stream_5; }
	inline void set_stream_5(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___stream_5 = value;
		Il2CppCodeGenWriteBarrier((&___stream_5), value);
	}

	inline static int32_t get_offset_of_encoding_6() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___encoding_6)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_6() const { return ___encoding_6; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_6() { return &___encoding_6; }
	inline void set_encoding_6(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_6 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_6), value);
	}

	inline static int32_t get_offset_of_xmlCharType_7() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___xmlCharType_7)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_7() const { return ___xmlCharType_7; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_7() { return &___xmlCharType_7; }
	inline void set_xmlCharType_7(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_7 = value;
	}

	inline static int32_t get_offset_of_bufPos_8() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufPos_8)); }
	inline int32_t get_bufPos_8() const { return ___bufPos_8; }
	inline int32_t* get_address_of_bufPos_8() { return &___bufPos_8; }
	inline void set_bufPos_8(int32_t value)
	{
		___bufPos_8 = value;
	}

	inline static int32_t get_offset_of_textPos_9() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___textPos_9)); }
	inline int32_t get_textPos_9() const { return ___textPos_9; }
	inline int32_t* get_address_of_textPos_9() { return &___textPos_9; }
	inline void set_textPos_9(int32_t value)
	{
		___textPos_9 = value;
	}

	inline static int32_t get_offset_of_contentPos_10() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___contentPos_10)); }
	inline int32_t get_contentPos_10() const { return ___contentPos_10; }
	inline int32_t* get_address_of_contentPos_10() { return &___contentPos_10; }
	inline void set_contentPos_10(int32_t value)
	{
		___contentPos_10 = value;
	}

	inline static int32_t get_offset_of_cdataPos_11() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___cdataPos_11)); }
	inline int32_t get_cdataPos_11() const { return ___cdataPos_11; }
	inline int32_t* get_address_of_cdataPos_11() { return &___cdataPos_11; }
	inline void set_cdataPos_11(int32_t value)
	{
		___cdataPos_11 = value;
	}

	inline static int32_t get_offset_of_attrEndPos_12() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___attrEndPos_12)); }
	inline int32_t get_attrEndPos_12() const { return ___attrEndPos_12; }
	inline int32_t* get_address_of_attrEndPos_12() { return &___attrEndPos_12; }
	inline void set_attrEndPos_12(int32_t value)
	{
		___attrEndPos_12 = value;
	}

	inline static int32_t get_offset_of_bufLen_13() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___bufLen_13)); }
	inline int32_t get_bufLen_13() const { return ___bufLen_13; }
	inline int32_t* get_address_of_bufLen_13() { return &___bufLen_13; }
	inline void set_bufLen_13(int32_t value)
	{
		___bufLen_13 = value;
	}

	inline static int32_t get_offset_of_writeToNull_14() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___writeToNull_14)); }
	inline bool get_writeToNull_14() const { return ___writeToNull_14; }
	inline bool* get_address_of_writeToNull_14() { return &___writeToNull_14; }
	inline void set_writeToNull_14(bool value)
	{
		___writeToNull_14 = value;
	}

	inline static int32_t get_offset_of_hadDoubleBracket_15() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___hadDoubleBracket_15)); }
	inline bool get_hadDoubleBracket_15() const { return ___hadDoubleBracket_15; }
	inline bool* get_address_of_hadDoubleBracket_15() { return &___hadDoubleBracket_15; }
	inline void set_hadDoubleBracket_15(bool value)
	{
		___hadDoubleBracket_15 = value;
	}

	inline static int32_t get_offset_of_inAttributeValue_16() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___inAttributeValue_16)); }
	inline bool get_inAttributeValue_16() const { return ___inAttributeValue_16; }
	inline bool* get_address_of_inAttributeValue_16() { return &___inAttributeValue_16; }
	inline void set_inAttributeValue_16(bool value)
	{
		___inAttributeValue_16 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_17() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___newLineHandling_17)); }
	inline int32_t get_newLineHandling_17() const { return ___newLineHandling_17; }
	inline int32_t* get_address_of_newLineHandling_17() { return &___newLineHandling_17; }
	inline void set_newLineHandling_17(int32_t value)
	{
		___newLineHandling_17 = value;
	}

	inline static int32_t get_offset_of_closeOutput_18() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___closeOutput_18)); }
	inline bool get_closeOutput_18() const { return ___closeOutput_18; }
	inline bool* get_address_of_closeOutput_18() { return &___closeOutput_18; }
	inline void set_closeOutput_18(bool value)
	{
		___closeOutput_18 = value;
	}

	inline static int32_t get_offset_of_omitXmlDeclaration_19() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___omitXmlDeclaration_19)); }
	inline bool get_omitXmlDeclaration_19() const { return ___omitXmlDeclaration_19; }
	inline bool* get_address_of_omitXmlDeclaration_19() { return &___omitXmlDeclaration_19; }
	inline void set_omitXmlDeclaration_19(bool value)
	{
		___omitXmlDeclaration_19 = value;
	}

	inline static int32_t get_offset_of_newLineChars_20() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___newLineChars_20)); }
	inline String_t* get_newLineChars_20() const { return ___newLineChars_20; }
	inline String_t** get_address_of_newLineChars_20() { return &___newLineChars_20; }
	inline void set_newLineChars_20(String_t* value)
	{
		___newLineChars_20 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_20), value);
	}

	inline static int32_t get_offset_of_checkCharacters_21() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___checkCharacters_21)); }
	inline bool get_checkCharacters_21() const { return ___checkCharacters_21; }
	inline bool* get_address_of_checkCharacters_21() { return &___checkCharacters_21; }
	inline void set_checkCharacters_21(bool value)
	{
		___checkCharacters_21 = value;
	}

	inline static int32_t get_offset_of_standalone_22() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___standalone_22)); }
	inline int32_t get_standalone_22() const { return ___standalone_22; }
	inline int32_t* get_address_of_standalone_22() { return &___standalone_22; }
	inline void set_standalone_22(int32_t value)
	{
		___standalone_22 = value;
	}

	inline static int32_t get_offset_of_outputMethod_23() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___outputMethod_23)); }
	inline int32_t get_outputMethod_23() const { return ___outputMethod_23; }
	inline int32_t* get_address_of_outputMethod_23() { return &___outputMethod_23; }
	inline void set_outputMethod_23(int32_t value)
	{
		___outputMethod_23 = value;
	}

	inline static int32_t get_offset_of_autoXmlDeclaration_24() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___autoXmlDeclaration_24)); }
	inline bool get_autoXmlDeclaration_24() const { return ___autoXmlDeclaration_24; }
	inline bool* get_address_of_autoXmlDeclaration_24() { return &___autoXmlDeclaration_24; }
	inline void set_autoXmlDeclaration_24(bool value)
	{
		___autoXmlDeclaration_24 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_25() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E, ___mergeCDataSections_25)); }
	inline bool get_mergeCDataSections_25() const { return ___mergeCDataSections_25; }
	inline bool* get_address_of_mergeCDataSections_25() { return &___mergeCDataSections_25; }
	inline void set_mergeCDataSections_25(bool value)
	{
		___mergeCDataSections_25 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITER_TD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E_H
#ifndef XMLVALIDATINGREADERIMPL_T4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_H
#define XMLVALIDATINGREADERIMPL_T4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlValidatingReaderImpl
struct  XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlReader System.Xml.XmlValidatingReaderImpl::coreReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___coreReader_3;
	// System.Xml.XmlTextReaderImpl System.Xml.XmlValidatingReaderImpl::coreReaderImpl
	XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * ___coreReaderImpl_4;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlValidatingReaderImpl::coreReaderNSResolver
	RuntimeObject* ___coreReaderNSResolver_5;
	// System.Xml.ValidationType System.Xml.XmlValidatingReaderImpl::validationType
	int32_t ___validationType_6;
	// System.Xml.Schema.BaseValidator System.Xml.XmlValidatingReaderImpl::validator
	BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43 * ___validator_7;
	// System.Xml.Schema.XmlSchemaCollection System.Xml.XmlValidatingReaderImpl::schemaCollection
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * ___schemaCollection_8;
	// System.Boolean System.Xml.XmlValidatingReaderImpl::processIdentityConstraints
	bool ___processIdentityConstraints_9;
	// System.Xml.XmlValidatingReaderImpl/ParsingFunction System.Xml.XmlValidatingReaderImpl::parsingFunction
	int32_t ___parsingFunction_10;
	// System.Xml.XmlValidatingReaderImpl/ValidationEventHandling System.Xml.XmlValidatingReaderImpl::eventHandling
	ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1 * ___eventHandling_11;
	// System.Xml.XmlParserContext System.Xml.XmlValidatingReaderImpl::parserContext
	XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * ___parserContext_12;
	// System.Xml.ReadContentAsBinaryHelper System.Xml.XmlValidatingReaderImpl::readBinaryHelper
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * ___readBinaryHelper_13;
	// System.Xml.XmlReader System.Xml.XmlValidatingReaderImpl::outerReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___outerReader_14;

public:
	inline static int32_t get_offset_of_coreReader_3() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___coreReader_3)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_coreReader_3() const { return ___coreReader_3; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_coreReader_3() { return &___coreReader_3; }
	inline void set_coreReader_3(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___coreReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___coreReader_3), value);
	}

	inline static int32_t get_offset_of_coreReaderImpl_4() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___coreReaderImpl_4)); }
	inline XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * get_coreReaderImpl_4() const { return ___coreReaderImpl_4; }
	inline XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 ** get_address_of_coreReaderImpl_4() { return &___coreReaderImpl_4; }
	inline void set_coreReaderImpl_4(XmlTextReaderImpl_t393737BE3F9168D966F164C2FD840C3494DEDE61 * value)
	{
		___coreReaderImpl_4 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderImpl_4), value);
	}

	inline static int32_t get_offset_of_coreReaderNSResolver_5() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___coreReaderNSResolver_5)); }
	inline RuntimeObject* get_coreReaderNSResolver_5() const { return ___coreReaderNSResolver_5; }
	inline RuntimeObject** get_address_of_coreReaderNSResolver_5() { return &___coreReaderNSResolver_5; }
	inline void set_coreReaderNSResolver_5(RuntimeObject* value)
	{
		___coreReaderNSResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderNSResolver_5), value);
	}

	inline static int32_t get_offset_of_validationType_6() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___validationType_6)); }
	inline int32_t get_validationType_6() const { return ___validationType_6; }
	inline int32_t* get_address_of_validationType_6() { return &___validationType_6; }
	inline void set_validationType_6(int32_t value)
	{
		___validationType_6 = value;
	}

	inline static int32_t get_offset_of_validator_7() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___validator_7)); }
	inline BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43 * get_validator_7() const { return ___validator_7; }
	inline BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43 ** get_address_of_validator_7() { return &___validator_7; }
	inline void set_validator_7(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43 * value)
	{
		___validator_7 = value;
		Il2CppCodeGenWriteBarrier((&___validator_7), value);
	}

	inline static int32_t get_offset_of_schemaCollection_8() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___schemaCollection_8)); }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * get_schemaCollection_8() const { return ___schemaCollection_8; }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 ** get_address_of_schemaCollection_8() { return &___schemaCollection_8; }
	inline void set_schemaCollection_8(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * value)
	{
		___schemaCollection_8 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_8), value);
	}

	inline static int32_t get_offset_of_processIdentityConstraints_9() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___processIdentityConstraints_9)); }
	inline bool get_processIdentityConstraints_9() const { return ___processIdentityConstraints_9; }
	inline bool* get_address_of_processIdentityConstraints_9() { return &___processIdentityConstraints_9; }
	inline void set_processIdentityConstraints_9(bool value)
	{
		___processIdentityConstraints_9 = value;
	}

	inline static int32_t get_offset_of_parsingFunction_10() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___parsingFunction_10)); }
	inline int32_t get_parsingFunction_10() const { return ___parsingFunction_10; }
	inline int32_t* get_address_of_parsingFunction_10() { return &___parsingFunction_10; }
	inline void set_parsingFunction_10(int32_t value)
	{
		___parsingFunction_10 = value;
	}

	inline static int32_t get_offset_of_eventHandling_11() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___eventHandling_11)); }
	inline ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1 * get_eventHandling_11() const { return ___eventHandling_11; }
	inline ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1 ** get_address_of_eventHandling_11() { return &___eventHandling_11; }
	inline void set_eventHandling_11(ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1 * value)
	{
		___eventHandling_11 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_11), value);
	}

	inline static int32_t get_offset_of_parserContext_12() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___parserContext_12)); }
	inline XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * get_parserContext_12() const { return ___parserContext_12; }
	inline XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 ** get_address_of_parserContext_12() { return &___parserContext_12; }
	inline void set_parserContext_12(XmlParserContext_tDC3A95452BC683F0D64EC3C4AF9934D3FC732279 * value)
	{
		___parserContext_12 = value;
		Il2CppCodeGenWriteBarrier((&___parserContext_12), value);
	}

	inline static int32_t get_offset_of_readBinaryHelper_13() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___readBinaryHelper_13)); }
	inline ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * get_readBinaryHelper_13() const { return ___readBinaryHelper_13; }
	inline ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 ** get_address_of_readBinaryHelper_13() { return &___readBinaryHelper_13; }
	inline void set_readBinaryHelper_13(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * value)
	{
		___readBinaryHelper_13 = value;
		Il2CppCodeGenWriteBarrier((&___readBinaryHelper_13), value);
	}

	inline static int32_t get_offset_of_outerReader_14() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E, ___outerReader_14)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_outerReader_14() const { return ___outerReader_14; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_outerReader_14() { return &___outerReader_14; }
	inline void set_outerReader_14(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___outerReader_14 = value;
		Il2CppCodeGenWriteBarrier((&___outerReader_14), value);
	}
};

struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_StaticFields
{
public:
	// System.Xml.XmlResolver System.Xml.XmlValidatingReaderImpl::s_tempResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___s_tempResolver_15;

public:
	inline static int32_t get_offset_of_s_tempResolver_15() { return static_cast<int32_t>(offsetof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_StaticFields, ___s_tempResolver_15)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_s_tempResolver_15() const { return ___s_tempResolver_15; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_s_tempResolver_15() { return &___s_tempResolver_15; }
	inline void set_s_tempResolver_15(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___s_tempResolver_15 = value;
		Il2CppCodeGenWriteBarrier((&___s_tempResolver_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLVALIDATINGREADERIMPL_T4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_H
#ifndef XMLWELLFORMEDWRITER_T8E017277086D1C79023B6E28A84F46FB36E9AF8A_H
#define XMLWELLFORMEDWRITER_T8E017277086D1C79023B6E28A84F46FB36E9AF8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter
struct  XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A  : public XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869
{
public:
	// System.Xml.XmlWriter System.Xml.XmlWellFormedWriter::writer
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * ___writer_1;
	// System.Xml.XmlRawWriter System.Xml.XmlWellFormedWriter::rawWriter
	XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * ___rawWriter_2;
	// System.Xml.IXmlNamespaceResolver System.Xml.XmlWellFormedWriter::predefinedNamespaces
	RuntimeObject* ___predefinedNamespaces_3;
	// System.Xml.XmlWellFormedWriter/Namespace[] System.Xml.XmlWellFormedWriter::nsStack
	NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C* ___nsStack_4;
	// System.Int32 System.Xml.XmlWellFormedWriter::nsTop
	int32_t ___nsTop_5;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlWellFormedWriter::nsHashtable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___nsHashtable_6;
	// System.Boolean System.Xml.XmlWellFormedWriter::useNsHashtable
	bool ___useNsHashtable_7;
	// System.Xml.XmlWellFormedWriter/ElementScope[] System.Xml.XmlWellFormedWriter::elemScopeStack
	ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80* ___elemScopeStack_8;
	// System.Int32 System.Xml.XmlWellFormedWriter::elemTop
	int32_t ___elemTop_9;
	// System.Xml.XmlWellFormedWriter/AttrName[] System.Xml.XmlWellFormedWriter::attrStack
	AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033* ___attrStack_10;
	// System.Int32 System.Xml.XmlWellFormedWriter::attrCount
	int32_t ___attrCount_11;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlWellFormedWriter::attrHashTable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___attrHashTable_12;
	// System.Xml.XmlWellFormedWriter/SpecialAttribute System.Xml.XmlWellFormedWriter::specAttr
	int32_t ___specAttr_13;
	// System.Xml.XmlWellFormedWriter/AttributeValueCache System.Xml.XmlWellFormedWriter::attrValueCache
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA * ___attrValueCache_14;
	// System.String System.Xml.XmlWellFormedWriter::curDeclPrefix
	String_t* ___curDeclPrefix_15;
	// System.Xml.XmlWellFormedWriter/State[] System.Xml.XmlWellFormedWriter::stateTable
	StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* ___stateTable_16;
	// System.Xml.XmlWellFormedWriter/State System.Xml.XmlWellFormedWriter::currentState
	int32_t ___currentState_17;
	// System.Boolean System.Xml.XmlWellFormedWriter::checkCharacters
	bool ___checkCharacters_18;
	// System.Boolean System.Xml.XmlWellFormedWriter::omitDuplNamespaces
	bool ___omitDuplNamespaces_19;
	// System.Boolean System.Xml.XmlWellFormedWriter::writeEndDocumentOnClose
	bool ___writeEndDocumentOnClose_20;
	// System.Xml.ConformanceLevel System.Xml.XmlWellFormedWriter::conformanceLevel
	int32_t ___conformanceLevel_21;
	// System.Boolean System.Xml.XmlWellFormedWriter::dtdWritten
	bool ___dtdWritten_22;
	// System.Boolean System.Xml.XmlWellFormedWriter::xmlDeclFollows
	bool ___xmlDeclFollows_23;
	// System.Xml.XmlCharType System.Xml.XmlWellFormedWriter::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_24;
	// System.Xml.SecureStringHasher System.Xml.XmlWellFormedWriter::hasher
	SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * ___hasher_25;

public:
	inline static int32_t get_offset_of_writer_1() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___writer_1)); }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * get_writer_1() const { return ___writer_1; }
	inline XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 ** get_address_of_writer_1() { return &___writer_1; }
	inline void set_writer_1(XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869 * value)
	{
		___writer_1 = value;
		Il2CppCodeGenWriteBarrier((&___writer_1), value);
	}

	inline static int32_t get_offset_of_rawWriter_2() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___rawWriter_2)); }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * get_rawWriter_2() const { return ___rawWriter_2; }
	inline XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 ** get_address_of_rawWriter_2() { return &___rawWriter_2; }
	inline void set_rawWriter_2(XmlRawWriter_t96129C955F861EC1DDA80EEABE73BB794D617FF2 * value)
	{
		___rawWriter_2 = value;
		Il2CppCodeGenWriteBarrier((&___rawWriter_2), value);
	}

	inline static int32_t get_offset_of_predefinedNamespaces_3() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___predefinedNamespaces_3)); }
	inline RuntimeObject* get_predefinedNamespaces_3() const { return ___predefinedNamespaces_3; }
	inline RuntimeObject** get_address_of_predefinedNamespaces_3() { return &___predefinedNamespaces_3; }
	inline void set_predefinedNamespaces_3(RuntimeObject* value)
	{
		___predefinedNamespaces_3 = value;
		Il2CppCodeGenWriteBarrier((&___predefinedNamespaces_3), value);
	}

	inline static int32_t get_offset_of_nsStack_4() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___nsStack_4)); }
	inline NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C* get_nsStack_4() const { return ___nsStack_4; }
	inline NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C** get_address_of_nsStack_4() { return &___nsStack_4; }
	inline void set_nsStack_4(NamespaceU5BU5D_t01DE66260D2C50A8F3EA1A658D2D43AC0185E79C* value)
	{
		___nsStack_4 = value;
		Il2CppCodeGenWriteBarrier((&___nsStack_4), value);
	}

	inline static int32_t get_offset_of_nsTop_5() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___nsTop_5)); }
	inline int32_t get_nsTop_5() const { return ___nsTop_5; }
	inline int32_t* get_address_of_nsTop_5() { return &___nsTop_5; }
	inline void set_nsTop_5(int32_t value)
	{
		___nsTop_5 = value;
	}

	inline static int32_t get_offset_of_nsHashtable_6() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___nsHashtable_6)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_nsHashtable_6() const { return ___nsHashtable_6; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_nsHashtable_6() { return &___nsHashtable_6; }
	inline void set_nsHashtable_6(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___nsHashtable_6 = value;
		Il2CppCodeGenWriteBarrier((&___nsHashtable_6), value);
	}

	inline static int32_t get_offset_of_useNsHashtable_7() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___useNsHashtable_7)); }
	inline bool get_useNsHashtable_7() const { return ___useNsHashtable_7; }
	inline bool* get_address_of_useNsHashtable_7() { return &___useNsHashtable_7; }
	inline void set_useNsHashtable_7(bool value)
	{
		___useNsHashtable_7 = value;
	}

	inline static int32_t get_offset_of_elemScopeStack_8() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___elemScopeStack_8)); }
	inline ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80* get_elemScopeStack_8() const { return ___elemScopeStack_8; }
	inline ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80** get_address_of_elemScopeStack_8() { return &___elemScopeStack_8; }
	inline void set_elemScopeStack_8(ElementScopeU5BU5D_tF3D49159E31D1555D56CD4A9120D4C27FF160E80* value)
	{
		___elemScopeStack_8 = value;
		Il2CppCodeGenWriteBarrier((&___elemScopeStack_8), value);
	}

	inline static int32_t get_offset_of_elemTop_9() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___elemTop_9)); }
	inline int32_t get_elemTop_9() const { return ___elemTop_9; }
	inline int32_t* get_address_of_elemTop_9() { return &___elemTop_9; }
	inline void set_elemTop_9(int32_t value)
	{
		___elemTop_9 = value;
	}

	inline static int32_t get_offset_of_attrStack_10() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrStack_10)); }
	inline AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033* get_attrStack_10() const { return ___attrStack_10; }
	inline AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033** get_address_of_attrStack_10() { return &___attrStack_10; }
	inline void set_attrStack_10(AttrNameU5BU5D_t3A62DED6782C1A8158652A81629B4BF67EA1A033* value)
	{
		___attrStack_10 = value;
		Il2CppCodeGenWriteBarrier((&___attrStack_10), value);
	}

	inline static int32_t get_offset_of_attrCount_11() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrCount_11)); }
	inline int32_t get_attrCount_11() const { return ___attrCount_11; }
	inline int32_t* get_address_of_attrCount_11() { return &___attrCount_11; }
	inline void set_attrCount_11(int32_t value)
	{
		___attrCount_11 = value;
	}

	inline static int32_t get_offset_of_attrHashTable_12() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrHashTable_12)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_attrHashTable_12() const { return ___attrHashTable_12; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_attrHashTable_12() { return &___attrHashTable_12; }
	inline void set_attrHashTable_12(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___attrHashTable_12 = value;
		Il2CppCodeGenWriteBarrier((&___attrHashTable_12), value);
	}

	inline static int32_t get_offset_of_specAttr_13() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___specAttr_13)); }
	inline int32_t get_specAttr_13() const { return ___specAttr_13; }
	inline int32_t* get_address_of_specAttr_13() { return &___specAttr_13; }
	inline void set_specAttr_13(int32_t value)
	{
		___specAttr_13 = value;
	}

	inline static int32_t get_offset_of_attrValueCache_14() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___attrValueCache_14)); }
	inline AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA * get_attrValueCache_14() const { return ___attrValueCache_14; }
	inline AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA ** get_address_of_attrValueCache_14() { return &___attrValueCache_14; }
	inline void set_attrValueCache_14(AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA * value)
	{
		___attrValueCache_14 = value;
		Il2CppCodeGenWriteBarrier((&___attrValueCache_14), value);
	}

	inline static int32_t get_offset_of_curDeclPrefix_15() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___curDeclPrefix_15)); }
	inline String_t* get_curDeclPrefix_15() const { return ___curDeclPrefix_15; }
	inline String_t** get_address_of_curDeclPrefix_15() { return &___curDeclPrefix_15; }
	inline void set_curDeclPrefix_15(String_t* value)
	{
		___curDeclPrefix_15 = value;
		Il2CppCodeGenWriteBarrier((&___curDeclPrefix_15), value);
	}

	inline static int32_t get_offset_of_stateTable_16() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___stateTable_16)); }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* get_stateTable_16() const { return ___stateTable_16; }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4** get_address_of_stateTable_16() { return &___stateTable_16; }
	inline void set_stateTable_16(StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* value)
	{
		___stateTable_16 = value;
		Il2CppCodeGenWriteBarrier((&___stateTable_16), value);
	}

	inline static int32_t get_offset_of_currentState_17() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___currentState_17)); }
	inline int32_t get_currentState_17() const { return ___currentState_17; }
	inline int32_t* get_address_of_currentState_17() { return &___currentState_17; }
	inline void set_currentState_17(int32_t value)
	{
		___currentState_17 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_18() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___checkCharacters_18)); }
	inline bool get_checkCharacters_18() const { return ___checkCharacters_18; }
	inline bool* get_address_of_checkCharacters_18() { return &___checkCharacters_18; }
	inline void set_checkCharacters_18(bool value)
	{
		___checkCharacters_18 = value;
	}

	inline static int32_t get_offset_of_omitDuplNamespaces_19() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___omitDuplNamespaces_19)); }
	inline bool get_omitDuplNamespaces_19() const { return ___omitDuplNamespaces_19; }
	inline bool* get_address_of_omitDuplNamespaces_19() { return &___omitDuplNamespaces_19; }
	inline void set_omitDuplNamespaces_19(bool value)
	{
		___omitDuplNamespaces_19 = value;
	}

	inline static int32_t get_offset_of_writeEndDocumentOnClose_20() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___writeEndDocumentOnClose_20)); }
	inline bool get_writeEndDocumentOnClose_20() const { return ___writeEndDocumentOnClose_20; }
	inline bool* get_address_of_writeEndDocumentOnClose_20() { return &___writeEndDocumentOnClose_20; }
	inline void set_writeEndDocumentOnClose_20(bool value)
	{
		___writeEndDocumentOnClose_20 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_21() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___conformanceLevel_21)); }
	inline int32_t get_conformanceLevel_21() const { return ___conformanceLevel_21; }
	inline int32_t* get_address_of_conformanceLevel_21() { return &___conformanceLevel_21; }
	inline void set_conformanceLevel_21(int32_t value)
	{
		___conformanceLevel_21 = value;
	}

	inline static int32_t get_offset_of_dtdWritten_22() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___dtdWritten_22)); }
	inline bool get_dtdWritten_22() const { return ___dtdWritten_22; }
	inline bool* get_address_of_dtdWritten_22() { return &___dtdWritten_22; }
	inline void set_dtdWritten_22(bool value)
	{
		___dtdWritten_22 = value;
	}

	inline static int32_t get_offset_of_xmlDeclFollows_23() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___xmlDeclFollows_23)); }
	inline bool get_xmlDeclFollows_23() const { return ___xmlDeclFollows_23; }
	inline bool* get_address_of_xmlDeclFollows_23() { return &___xmlDeclFollows_23; }
	inline void set_xmlDeclFollows_23(bool value)
	{
		___xmlDeclFollows_23 = value;
	}

	inline static int32_t get_offset_of_xmlCharType_24() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___xmlCharType_24)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_24() const { return ___xmlCharType_24; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_24() { return &___xmlCharType_24; }
	inline void set_xmlCharType_24(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_24 = value;
	}

	inline static int32_t get_offset_of_hasher_25() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A, ___hasher_25)); }
	inline SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * get_hasher_25() const { return ___hasher_25; }
	inline SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 ** get_address_of_hasher_25() { return &___hasher_25; }
	inline void set_hasher_25(SecureStringHasher_tC534879569ADBDEDC9C41A2B3503768DE3FEE7F3 * value)
	{
		___hasher_25 = value;
		Il2CppCodeGenWriteBarrier((&___hasher_25), value);
	}
};

struct XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields
{
public:
	// System.String[] System.Xml.XmlWellFormedWriter::stateName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___stateName_26;
	// System.String[] System.Xml.XmlWellFormedWriter::tokenName
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___tokenName_27;
	// System.Xml.WriteState[] System.Xml.XmlWellFormedWriter::state2WriteState
	WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A* ___state2WriteState_28;
	// System.Xml.XmlWellFormedWriter/State[] System.Xml.XmlWellFormedWriter::StateTableDocument
	StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* ___StateTableDocument_29;
	// System.Xml.XmlWellFormedWriter/State[] System.Xml.XmlWellFormedWriter::StateTableAuto
	StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* ___StateTableAuto_30;

public:
	inline static int32_t get_offset_of_stateName_26() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___stateName_26)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_stateName_26() const { return ___stateName_26; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_stateName_26() { return &___stateName_26; }
	inline void set_stateName_26(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___stateName_26 = value;
		Il2CppCodeGenWriteBarrier((&___stateName_26), value);
	}

	inline static int32_t get_offset_of_tokenName_27() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___tokenName_27)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_tokenName_27() const { return ___tokenName_27; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_tokenName_27() { return &___tokenName_27; }
	inline void set_tokenName_27(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___tokenName_27 = value;
		Il2CppCodeGenWriteBarrier((&___tokenName_27), value);
	}

	inline static int32_t get_offset_of_state2WriteState_28() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___state2WriteState_28)); }
	inline WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A* get_state2WriteState_28() const { return ___state2WriteState_28; }
	inline WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A** get_address_of_state2WriteState_28() { return &___state2WriteState_28; }
	inline void set_state2WriteState_28(WriteStateU5BU5D_tF3D704962B3CCE1E6285C564D39C010616FD448A* value)
	{
		___state2WriteState_28 = value;
		Il2CppCodeGenWriteBarrier((&___state2WriteState_28), value);
	}

	inline static int32_t get_offset_of_StateTableDocument_29() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___StateTableDocument_29)); }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* get_StateTableDocument_29() const { return ___StateTableDocument_29; }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4** get_address_of_StateTableDocument_29() { return &___StateTableDocument_29; }
	inline void set_StateTableDocument_29(StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* value)
	{
		___StateTableDocument_29 = value;
		Il2CppCodeGenWriteBarrier((&___StateTableDocument_29), value);
	}

	inline static int32_t get_offset_of_StateTableAuto_30() { return static_cast<int32_t>(offsetof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields, ___StateTableAuto_30)); }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* get_StateTableAuto_30() const { return ___StateTableAuto_30; }
	inline StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4** get_address_of_StateTableAuto_30() { return &___StateTableAuto_30; }
	inline void set_StateTableAuto_30(StateU5BU5D_t9A46AB75358AF7C8FD87B8ACAB654BD24C5741D4* value)
	{
		___StateTableAuto_30 = value;
		Il2CppCodeGenWriteBarrier((&___StateTableAuto_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWELLFORMEDWRITER_T8E017277086D1C79023B6E28A84F46FB36E9AF8A_H
#ifndef ITEM_T5F7D0B65E37A6039818862DBBE1FACBE01D1E74A_H
#define ITEM_T5F7D0B65E37A6039818862DBBE1FACBE01D1E74A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/AttributeValueCache/Item
struct  Item_t5F7D0B65E37A6039818862DBBE1FACBE01D1E74A  : public RuntimeObject
{
public:
	// System.Xml.XmlWellFormedWriter/AttributeValueCache/ItemType System.Xml.XmlWellFormedWriter/AttributeValueCache/Item::type
	int32_t ___type_0;
	// System.Object System.Xml.XmlWellFormedWriter/AttributeValueCache/Item::data
	RuntimeObject * ___data_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(Item_t5F7D0B65E37A6039818862DBBE1FACBE01D1E74A, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_data_1() { return static_cast<int32_t>(offsetof(Item_t5F7D0B65E37A6039818862DBBE1FACBE01D1E74A, ___data_1)); }
	inline RuntimeObject * get_data_1() const { return ___data_1; }
	inline RuntimeObject ** get_address_of_data_1() { return &___data_1; }
	inline void set_data_1(RuntimeObject * value)
	{
		___data_1 = value;
		Il2CppCodeGenWriteBarrier((&___data_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ITEM_T5F7D0B65E37A6039818862DBBE1FACBE01D1E74A_H
#ifndef ELEMENTSCOPE_T8DDAA195533D990229BE354A121DC9F68350F0A7_H
#define ELEMENTSCOPE_T8DDAA195533D990229BE354A121DC9F68350F0A7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/ElementScope
struct  ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7 
{
public:
	// System.Int32 System.Xml.XmlWellFormedWriter/ElementScope::prevNSTop
	int32_t ___prevNSTop_0;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::prefix
	String_t* ___prefix_1;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::localName
	String_t* ___localName_2;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::namespaceUri
	String_t* ___namespaceUri_3;
	// System.Xml.XmlSpace System.Xml.XmlWellFormedWriter/ElementScope::xmlSpace
	int32_t ___xmlSpace_4;
	// System.String System.Xml.XmlWellFormedWriter/ElementScope::xmlLang
	String_t* ___xmlLang_5;

public:
	inline static int32_t get_offset_of_prevNSTop_0() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___prevNSTop_0)); }
	inline int32_t get_prevNSTop_0() const { return ___prevNSTop_0; }
	inline int32_t* get_address_of_prevNSTop_0() { return &___prevNSTop_0; }
	inline void set_prevNSTop_0(int32_t value)
	{
		___prevNSTop_0 = value;
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_localName_2() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___localName_2)); }
	inline String_t* get_localName_2() const { return ___localName_2; }
	inline String_t** get_address_of_localName_2() { return &___localName_2; }
	inline void set_localName_2(String_t* value)
	{
		___localName_2 = value;
		Il2CppCodeGenWriteBarrier((&___localName_2), value);
	}

	inline static int32_t get_offset_of_namespaceUri_3() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___namespaceUri_3)); }
	inline String_t* get_namespaceUri_3() const { return ___namespaceUri_3; }
	inline String_t** get_address_of_namespaceUri_3() { return &___namespaceUri_3; }
	inline void set_namespaceUri_3(String_t* value)
	{
		___namespaceUri_3 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_3), value);
	}

	inline static int32_t get_offset_of_xmlSpace_4() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___xmlSpace_4)); }
	inline int32_t get_xmlSpace_4() const { return ___xmlSpace_4; }
	inline int32_t* get_address_of_xmlSpace_4() { return &___xmlSpace_4; }
	inline void set_xmlSpace_4(int32_t value)
	{
		___xmlSpace_4 = value;
	}

	inline static int32_t get_offset_of_xmlLang_5() { return static_cast<int32_t>(offsetof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7, ___xmlLang_5)); }
	inline String_t* get_xmlLang_5() const { return ___xmlLang_5; }
	inline String_t** get_address_of_xmlLang_5() { return &___xmlLang_5; }
	inline void set_xmlLang_5(String_t* value)
	{
		___xmlLang_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlLang_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/ElementScope
struct ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7_marshaled_pinvoke
{
	int32_t ___prevNSTop_0;
	char* ___prefix_1;
	char* ___localName_2;
	char* ___namespaceUri_3;
	int32_t ___xmlSpace_4;
	char* ___xmlLang_5;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/ElementScope
struct ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7_marshaled_com
{
	int32_t ___prevNSTop_0;
	Il2CppChar* ___prefix_1;
	Il2CppChar* ___localName_2;
	Il2CppChar* ___namespaceUri_3;
	int32_t ___xmlSpace_4;
	Il2CppChar* ___xmlLang_5;
};
#endif // ELEMENTSCOPE_T8DDAA195533D990229BE354A121DC9F68350F0A7_H
#ifndef NAMESPACE_T5D036A8DE620698F09AD5F2444482DD7A6D86438_H
#define NAMESPACE_T5D036A8DE620698F09AD5F2444482DD7A6D86438_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWellFormedWriter/Namespace
struct  Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438 
{
public:
	// System.String System.Xml.XmlWellFormedWriter/Namespace::prefix
	String_t* ___prefix_0;
	// System.String System.Xml.XmlWellFormedWriter/Namespace::namespaceUri
	String_t* ___namespaceUri_1;
	// System.Xml.XmlWellFormedWriter/NamespaceKind System.Xml.XmlWellFormedWriter/Namespace::kind
	int32_t ___kind_2;
	// System.Int32 System.Xml.XmlWellFormedWriter/Namespace::prevNsIndex
	int32_t ___prevNsIndex_3;

public:
	inline static int32_t get_offset_of_prefix_0() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___prefix_0)); }
	inline String_t* get_prefix_0() const { return ___prefix_0; }
	inline String_t** get_address_of_prefix_0() { return &___prefix_0; }
	inline void set_prefix_0(String_t* value)
	{
		___prefix_0 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_0), value);
	}

	inline static int32_t get_offset_of_namespaceUri_1() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___namespaceUri_1)); }
	inline String_t* get_namespaceUri_1() const { return ___namespaceUri_1; }
	inline String_t** get_address_of_namespaceUri_1() { return &___namespaceUri_1; }
	inline void set_namespaceUri_1(String_t* value)
	{
		___namespaceUri_1 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceUri_1), value);
	}

	inline static int32_t get_offset_of_kind_2() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___kind_2)); }
	inline int32_t get_kind_2() const { return ___kind_2; }
	inline int32_t* get_address_of_kind_2() { return &___kind_2; }
	inline void set_kind_2(int32_t value)
	{
		___kind_2 = value;
	}

	inline static int32_t get_offset_of_prevNsIndex_3() { return static_cast<int32_t>(offsetof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438, ___prevNsIndex_3)); }
	inline int32_t get_prevNsIndex_3() const { return ___prevNsIndex_3; }
	inline int32_t* get_address_of_prevNsIndex_3() { return &___prevNsIndex_3; }
	inline void set_prevNsIndex_3(int32_t value)
	{
		___prevNsIndex_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlWellFormedWriter/Namespace
struct Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438_marshaled_pinvoke
{
	char* ___prefix_0;
	char* ___namespaceUri_1;
	int32_t ___kind_2;
	int32_t ___prevNsIndex_3;
};
// Native definition for COM marshalling of System.Xml.XmlWellFormedWriter/Namespace
struct Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438_marshaled_com
{
	Il2CppChar* ___prefix_0;
	Il2CppChar* ___namespaceUri_1;
	int32_t ___kind_2;
	int32_t ___prevNsIndex_3;
};
#endif // NAMESPACE_T5D036A8DE620698F09AD5F2444482DD7A6D86438_H
#ifndef XMLWHITESPACE_TF5EA718743A148EBF7594ADF5A06B3224857AD4D_H
#define XMLWHITESPACE_TF5EA718743A148EBF7594ADF5A06B3224857AD4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWhitespace
struct  XmlWhitespace_tF5EA718743A148EBF7594ADF5A06B3224857AD4D  : public XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWHITESPACE_TF5EA718743A148EBF7594ADF5A06B3224857AD4D_H
#ifndef XMLWRITERSETTINGS_TD14B737CEA4EE4FB8329D33B62F13C6766BD75A3_H
#define XMLWRITERSETTINGS_TD14B737CEA4EE4FB8329D33B62F13C6766BD75A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlWriterSettings
struct  XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.XmlWriterSettings::useAsync
	bool ___useAsync_0;
	// System.Text.Encoding System.Xml.XmlWriterSettings::encoding
	Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * ___encoding_1;
	// System.Boolean System.Xml.XmlWriterSettings::omitXmlDecl
	bool ___omitXmlDecl_2;
	// System.Xml.NewLineHandling System.Xml.XmlWriterSettings::newLineHandling
	int32_t ___newLineHandling_3;
	// System.String System.Xml.XmlWriterSettings::newLineChars
	String_t* ___newLineChars_4;
	// System.Xml.TriState System.Xml.XmlWriterSettings::indent
	int32_t ___indent_5;
	// System.String System.Xml.XmlWriterSettings::indentChars
	String_t* ___indentChars_6;
	// System.Boolean System.Xml.XmlWriterSettings::newLineOnAttributes
	bool ___newLineOnAttributes_7;
	// System.Boolean System.Xml.XmlWriterSettings::closeOutput
	bool ___closeOutput_8;
	// System.Xml.NamespaceHandling System.Xml.XmlWriterSettings::namespaceHandling
	int32_t ___namespaceHandling_9;
	// System.Xml.ConformanceLevel System.Xml.XmlWriterSettings::conformanceLevel
	int32_t ___conformanceLevel_10;
	// System.Boolean System.Xml.XmlWriterSettings::checkCharacters
	bool ___checkCharacters_11;
	// System.Boolean System.Xml.XmlWriterSettings::writeEndDocumentOnClose
	bool ___writeEndDocumentOnClose_12;
	// System.Xml.XmlOutputMethod System.Xml.XmlWriterSettings::outputMethod
	int32_t ___outputMethod_13;
	// System.Collections.Generic.List`1<System.Xml.XmlQualifiedName> System.Xml.XmlWriterSettings::cdataSections
	List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 * ___cdataSections_14;
	// System.Boolean System.Xml.XmlWriterSettings::doNotEscapeUriAttributes
	bool ___doNotEscapeUriAttributes_15;
	// System.Boolean System.Xml.XmlWriterSettings::mergeCDataSections
	bool ___mergeCDataSections_16;
	// System.String System.Xml.XmlWriterSettings::mediaType
	String_t* ___mediaType_17;
	// System.String System.Xml.XmlWriterSettings::docTypeSystem
	String_t* ___docTypeSystem_18;
	// System.String System.Xml.XmlWriterSettings::docTypePublic
	String_t* ___docTypePublic_19;
	// System.Xml.XmlStandalone System.Xml.XmlWriterSettings::standalone
	int32_t ___standalone_20;
	// System.Boolean System.Xml.XmlWriterSettings::autoXmlDecl
	bool ___autoXmlDecl_21;
	// System.Boolean System.Xml.XmlWriterSettings::isReadOnly
	bool ___isReadOnly_22;

public:
	inline static int32_t get_offset_of_useAsync_0() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___useAsync_0)); }
	inline bool get_useAsync_0() const { return ___useAsync_0; }
	inline bool* get_address_of_useAsync_0() { return &___useAsync_0; }
	inline void set_useAsync_0(bool value)
	{
		___useAsync_0 = value;
	}

	inline static int32_t get_offset_of_encoding_1() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___encoding_1)); }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * get_encoding_1() const { return ___encoding_1; }
	inline Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 ** get_address_of_encoding_1() { return &___encoding_1; }
	inline void set_encoding_1(Encoding_t7837A3C0F55EAE0E3959A53C6D6E88B113ED78A4 * value)
	{
		___encoding_1 = value;
		Il2CppCodeGenWriteBarrier((&___encoding_1), value);
	}

	inline static int32_t get_offset_of_omitXmlDecl_2() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___omitXmlDecl_2)); }
	inline bool get_omitXmlDecl_2() const { return ___omitXmlDecl_2; }
	inline bool* get_address_of_omitXmlDecl_2() { return &___omitXmlDecl_2; }
	inline void set_omitXmlDecl_2(bool value)
	{
		___omitXmlDecl_2 = value;
	}

	inline static int32_t get_offset_of_newLineHandling_3() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___newLineHandling_3)); }
	inline int32_t get_newLineHandling_3() const { return ___newLineHandling_3; }
	inline int32_t* get_address_of_newLineHandling_3() { return &___newLineHandling_3; }
	inline void set_newLineHandling_3(int32_t value)
	{
		___newLineHandling_3 = value;
	}

	inline static int32_t get_offset_of_newLineChars_4() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___newLineChars_4)); }
	inline String_t* get_newLineChars_4() const { return ___newLineChars_4; }
	inline String_t** get_address_of_newLineChars_4() { return &___newLineChars_4; }
	inline void set_newLineChars_4(String_t* value)
	{
		___newLineChars_4 = value;
		Il2CppCodeGenWriteBarrier((&___newLineChars_4), value);
	}

	inline static int32_t get_offset_of_indent_5() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___indent_5)); }
	inline int32_t get_indent_5() const { return ___indent_5; }
	inline int32_t* get_address_of_indent_5() { return &___indent_5; }
	inline void set_indent_5(int32_t value)
	{
		___indent_5 = value;
	}

	inline static int32_t get_offset_of_indentChars_6() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___indentChars_6)); }
	inline String_t* get_indentChars_6() const { return ___indentChars_6; }
	inline String_t** get_address_of_indentChars_6() { return &___indentChars_6; }
	inline void set_indentChars_6(String_t* value)
	{
		___indentChars_6 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_6), value);
	}

	inline static int32_t get_offset_of_newLineOnAttributes_7() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___newLineOnAttributes_7)); }
	inline bool get_newLineOnAttributes_7() const { return ___newLineOnAttributes_7; }
	inline bool* get_address_of_newLineOnAttributes_7() { return &___newLineOnAttributes_7; }
	inline void set_newLineOnAttributes_7(bool value)
	{
		___newLineOnAttributes_7 = value;
	}

	inline static int32_t get_offset_of_closeOutput_8() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___closeOutput_8)); }
	inline bool get_closeOutput_8() const { return ___closeOutput_8; }
	inline bool* get_address_of_closeOutput_8() { return &___closeOutput_8; }
	inline void set_closeOutput_8(bool value)
	{
		___closeOutput_8 = value;
	}

	inline static int32_t get_offset_of_namespaceHandling_9() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___namespaceHandling_9)); }
	inline int32_t get_namespaceHandling_9() const { return ___namespaceHandling_9; }
	inline int32_t* get_address_of_namespaceHandling_9() { return &___namespaceHandling_9; }
	inline void set_namespaceHandling_9(int32_t value)
	{
		___namespaceHandling_9 = value;
	}

	inline static int32_t get_offset_of_conformanceLevel_10() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___conformanceLevel_10)); }
	inline int32_t get_conformanceLevel_10() const { return ___conformanceLevel_10; }
	inline int32_t* get_address_of_conformanceLevel_10() { return &___conformanceLevel_10; }
	inline void set_conformanceLevel_10(int32_t value)
	{
		___conformanceLevel_10 = value;
	}

	inline static int32_t get_offset_of_checkCharacters_11() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___checkCharacters_11)); }
	inline bool get_checkCharacters_11() const { return ___checkCharacters_11; }
	inline bool* get_address_of_checkCharacters_11() { return &___checkCharacters_11; }
	inline void set_checkCharacters_11(bool value)
	{
		___checkCharacters_11 = value;
	}

	inline static int32_t get_offset_of_writeEndDocumentOnClose_12() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___writeEndDocumentOnClose_12)); }
	inline bool get_writeEndDocumentOnClose_12() const { return ___writeEndDocumentOnClose_12; }
	inline bool* get_address_of_writeEndDocumentOnClose_12() { return &___writeEndDocumentOnClose_12; }
	inline void set_writeEndDocumentOnClose_12(bool value)
	{
		___writeEndDocumentOnClose_12 = value;
	}

	inline static int32_t get_offset_of_outputMethod_13() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___outputMethod_13)); }
	inline int32_t get_outputMethod_13() const { return ___outputMethod_13; }
	inline int32_t* get_address_of_outputMethod_13() { return &___outputMethod_13; }
	inline void set_outputMethod_13(int32_t value)
	{
		___outputMethod_13 = value;
	}

	inline static int32_t get_offset_of_cdataSections_14() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___cdataSections_14)); }
	inline List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 * get_cdataSections_14() const { return ___cdataSections_14; }
	inline List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 ** get_address_of_cdataSections_14() { return &___cdataSections_14; }
	inline void set_cdataSections_14(List_1_tCAF7C69A9EA5F50BF5587DD8E7657B89C6866626 * value)
	{
		___cdataSections_14 = value;
		Il2CppCodeGenWriteBarrier((&___cdataSections_14), value);
	}

	inline static int32_t get_offset_of_doNotEscapeUriAttributes_15() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___doNotEscapeUriAttributes_15)); }
	inline bool get_doNotEscapeUriAttributes_15() const { return ___doNotEscapeUriAttributes_15; }
	inline bool* get_address_of_doNotEscapeUriAttributes_15() { return &___doNotEscapeUriAttributes_15; }
	inline void set_doNotEscapeUriAttributes_15(bool value)
	{
		___doNotEscapeUriAttributes_15 = value;
	}

	inline static int32_t get_offset_of_mergeCDataSections_16() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___mergeCDataSections_16)); }
	inline bool get_mergeCDataSections_16() const { return ___mergeCDataSections_16; }
	inline bool* get_address_of_mergeCDataSections_16() { return &___mergeCDataSections_16; }
	inline void set_mergeCDataSections_16(bool value)
	{
		___mergeCDataSections_16 = value;
	}

	inline static int32_t get_offset_of_mediaType_17() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___mediaType_17)); }
	inline String_t* get_mediaType_17() const { return ___mediaType_17; }
	inline String_t** get_address_of_mediaType_17() { return &___mediaType_17; }
	inline void set_mediaType_17(String_t* value)
	{
		___mediaType_17 = value;
		Il2CppCodeGenWriteBarrier((&___mediaType_17), value);
	}

	inline static int32_t get_offset_of_docTypeSystem_18() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___docTypeSystem_18)); }
	inline String_t* get_docTypeSystem_18() const { return ___docTypeSystem_18; }
	inline String_t** get_address_of_docTypeSystem_18() { return &___docTypeSystem_18; }
	inline void set_docTypeSystem_18(String_t* value)
	{
		___docTypeSystem_18 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeSystem_18), value);
	}

	inline static int32_t get_offset_of_docTypePublic_19() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___docTypePublic_19)); }
	inline String_t* get_docTypePublic_19() const { return ___docTypePublic_19; }
	inline String_t** get_address_of_docTypePublic_19() { return &___docTypePublic_19; }
	inline void set_docTypePublic_19(String_t* value)
	{
		___docTypePublic_19 = value;
		Il2CppCodeGenWriteBarrier((&___docTypePublic_19), value);
	}

	inline static int32_t get_offset_of_standalone_20() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___standalone_20)); }
	inline int32_t get_standalone_20() const { return ___standalone_20; }
	inline int32_t* get_address_of_standalone_20() { return &___standalone_20; }
	inline void set_standalone_20(int32_t value)
	{
		___standalone_20 = value;
	}

	inline static int32_t get_offset_of_autoXmlDecl_21() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___autoXmlDecl_21)); }
	inline bool get_autoXmlDecl_21() const { return ___autoXmlDecl_21; }
	inline bool* get_address_of_autoXmlDecl_21() { return &___autoXmlDecl_21; }
	inline void set_autoXmlDecl_21(bool value)
	{
		___autoXmlDecl_21 = value;
	}

	inline static int32_t get_offset_of_isReadOnly_22() { return static_cast<int32_t>(offsetof(XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3, ___isReadOnly_22)); }
	inline bool get_isReadOnly_22() const { return ___isReadOnly_22; }
	inline bool* get_address_of_isReadOnly_22() { return &___isReadOnly_22; }
	inline void set_isReadOnly_22(bool value)
	{
		___isReadOnly_22 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLWRITERSETTINGS_TD14B737CEA4EE4FB8329D33B62F13C6766BD75A3_H
#ifndef XSDCACHINGREADER_T8EA10A18D0BF0FC40EC21E5C14F47233231D2B51_H
#define XSDCACHINGREADER_T8EA10A18D0BF0FC40EC21E5C14F47233231D2B51_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XsdCachingReader
struct  XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlReader System.Xml.XsdCachingReader::coreReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___coreReader_3;
	// System.Xml.XmlNameTable System.Xml.XsdCachingReader::coreReaderNameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___coreReaderNameTable_4;
	// System.Xml.ValidatingReaderNodeData[] System.Xml.XsdCachingReader::contentEvents
	ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF* ___contentEvents_5;
	// System.Xml.ValidatingReaderNodeData[] System.Xml.XsdCachingReader::attributeEvents
	ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF* ___attributeEvents_6;
	// System.Xml.ValidatingReaderNodeData System.Xml.XsdCachingReader::cachedNode
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * ___cachedNode_7;
	// System.Xml.XsdCachingReader/CachingReaderState System.Xml.XsdCachingReader::cacheState
	int32_t ___cacheState_8;
	// System.Int32 System.Xml.XsdCachingReader::contentIndex
	int32_t ___contentIndex_9;
	// System.Int32 System.Xml.XsdCachingReader::attributeCount
	int32_t ___attributeCount_10;
	// System.Boolean System.Xml.XsdCachingReader::returnOriginalStringValues
	bool ___returnOriginalStringValues_11;
	// System.Xml.CachingEventHandler System.Xml.XsdCachingReader::cacheHandler
	CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6 * ___cacheHandler_12;
	// System.Int32 System.Xml.XsdCachingReader::currentAttrIndex
	int32_t ___currentAttrIndex_13;
	// System.Int32 System.Xml.XsdCachingReader::currentContentIndex
	int32_t ___currentContentIndex_14;
	// System.Boolean System.Xml.XsdCachingReader::readAhead
	bool ___readAhead_15;
	// System.Xml.IXmlLineInfo System.Xml.XsdCachingReader::lineInfo
	RuntimeObject* ___lineInfo_16;
	// System.Xml.ValidatingReaderNodeData System.Xml.XsdCachingReader::textNode
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * ___textNode_17;

public:
	inline static int32_t get_offset_of_coreReader_3() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___coreReader_3)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_coreReader_3() const { return ___coreReader_3; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_coreReader_3() { return &___coreReader_3; }
	inline void set_coreReader_3(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___coreReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___coreReader_3), value);
	}

	inline static int32_t get_offset_of_coreReaderNameTable_4() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___coreReaderNameTable_4)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_coreReaderNameTable_4() const { return ___coreReaderNameTable_4; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_coreReaderNameTable_4() { return &___coreReaderNameTable_4; }
	inline void set_coreReaderNameTable_4(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___coreReaderNameTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderNameTable_4), value);
	}

	inline static int32_t get_offset_of_contentEvents_5() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___contentEvents_5)); }
	inline ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF* get_contentEvents_5() const { return ___contentEvents_5; }
	inline ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF** get_address_of_contentEvents_5() { return &___contentEvents_5; }
	inline void set_contentEvents_5(ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF* value)
	{
		___contentEvents_5 = value;
		Il2CppCodeGenWriteBarrier((&___contentEvents_5), value);
	}

	inline static int32_t get_offset_of_attributeEvents_6() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___attributeEvents_6)); }
	inline ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF* get_attributeEvents_6() const { return ___attributeEvents_6; }
	inline ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF** get_address_of_attributeEvents_6() { return &___attributeEvents_6; }
	inline void set_attributeEvents_6(ValidatingReaderNodeDataU5BU5D_tFBEB81BF53C53E8F95978989EB44D14E42E30DCF* value)
	{
		___attributeEvents_6 = value;
		Il2CppCodeGenWriteBarrier((&___attributeEvents_6), value);
	}

	inline static int32_t get_offset_of_cachedNode_7() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___cachedNode_7)); }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * get_cachedNode_7() const { return ___cachedNode_7; }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F ** get_address_of_cachedNode_7() { return &___cachedNode_7; }
	inline void set_cachedNode_7(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * value)
	{
		___cachedNode_7 = value;
		Il2CppCodeGenWriteBarrier((&___cachedNode_7), value);
	}

	inline static int32_t get_offset_of_cacheState_8() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___cacheState_8)); }
	inline int32_t get_cacheState_8() const { return ___cacheState_8; }
	inline int32_t* get_address_of_cacheState_8() { return &___cacheState_8; }
	inline void set_cacheState_8(int32_t value)
	{
		___cacheState_8 = value;
	}

	inline static int32_t get_offset_of_contentIndex_9() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___contentIndex_9)); }
	inline int32_t get_contentIndex_9() const { return ___contentIndex_9; }
	inline int32_t* get_address_of_contentIndex_9() { return &___contentIndex_9; }
	inline void set_contentIndex_9(int32_t value)
	{
		___contentIndex_9 = value;
	}

	inline static int32_t get_offset_of_attributeCount_10() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___attributeCount_10)); }
	inline int32_t get_attributeCount_10() const { return ___attributeCount_10; }
	inline int32_t* get_address_of_attributeCount_10() { return &___attributeCount_10; }
	inline void set_attributeCount_10(int32_t value)
	{
		___attributeCount_10 = value;
	}

	inline static int32_t get_offset_of_returnOriginalStringValues_11() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___returnOriginalStringValues_11)); }
	inline bool get_returnOriginalStringValues_11() const { return ___returnOriginalStringValues_11; }
	inline bool* get_address_of_returnOriginalStringValues_11() { return &___returnOriginalStringValues_11; }
	inline void set_returnOriginalStringValues_11(bool value)
	{
		___returnOriginalStringValues_11 = value;
	}

	inline static int32_t get_offset_of_cacheHandler_12() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___cacheHandler_12)); }
	inline CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6 * get_cacheHandler_12() const { return ___cacheHandler_12; }
	inline CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6 ** get_address_of_cacheHandler_12() { return &___cacheHandler_12; }
	inline void set_cacheHandler_12(CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6 * value)
	{
		___cacheHandler_12 = value;
		Il2CppCodeGenWriteBarrier((&___cacheHandler_12), value);
	}

	inline static int32_t get_offset_of_currentAttrIndex_13() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___currentAttrIndex_13)); }
	inline int32_t get_currentAttrIndex_13() const { return ___currentAttrIndex_13; }
	inline int32_t* get_address_of_currentAttrIndex_13() { return &___currentAttrIndex_13; }
	inline void set_currentAttrIndex_13(int32_t value)
	{
		___currentAttrIndex_13 = value;
	}

	inline static int32_t get_offset_of_currentContentIndex_14() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___currentContentIndex_14)); }
	inline int32_t get_currentContentIndex_14() const { return ___currentContentIndex_14; }
	inline int32_t* get_address_of_currentContentIndex_14() { return &___currentContentIndex_14; }
	inline void set_currentContentIndex_14(int32_t value)
	{
		___currentContentIndex_14 = value;
	}

	inline static int32_t get_offset_of_readAhead_15() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___readAhead_15)); }
	inline bool get_readAhead_15() const { return ___readAhead_15; }
	inline bool* get_address_of_readAhead_15() { return &___readAhead_15; }
	inline void set_readAhead_15(bool value)
	{
		___readAhead_15 = value;
	}

	inline static int32_t get_offset_of_lineInfo_16() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___lineInfo_16)); }
	inline RuntimeObject* get_lineInfo_16() const { return ___lineInfo_16; }
	inline RuntimeObject** get_address_of_lineInfo_16() { return &___lineInfo_16; }
	inline void set_lineInfo_16(RuntimeObject* value)
	{
		___lineInfo_16 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_16), value);
	}

	inline static int32_t get_offset_of_textNode_17() { return static_cast<int32_t>(offsetof(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51, ___textNode_17)); }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * get_textNode_17() const { return ___textNode_17; }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F ** get_address_of_textNode_17() { return &___textNode_17; }
	inline void set_textNode_17(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * value)
	{
		___textNode_17 = value;
		Il2CppCodeGenWriteBarrier((&___textNode_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDCACHINGREADER_T8EA10A18D0BF0FC40EC21E5C14F47233231D2B51_H
#ifndef XSDVALIDATINGREADER_T34A7241660D82975EDF0B427CBDF6CC04B1B91E5_H
#define XSDVALIDATINGREADER_T34A7241660D82975EDF0B427CBDF6CC04B1B91E5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XsdValidatingReader
struct  XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5  : public XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB
{
public:
	// System.Xml.XmlReader System.Xml.XsdValidatingReader::coreReader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___coreReader_3;
	// System.Xml.IXmlNamespaceResolver System.Xml.XsdValidatingReader::coreReaderNSResolver
	RuntimeObject* ___coreReaderNSResolver_4;
	// System.Xml.IXmlNamespaceResolver System.Xml.XsdValidatingReader::thisNSResolver
	RuntimeObject* ___thisNSResolver_5;
	// System.Xml.Schema.XmlSchemaValidator System.Xml.XsdValidatingReader::validator
	XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C * ___validator_6;
	// System.Xml.XmlResolver System.Xml.XsdValidatingReader::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_7;
	// System.Xml.Schema.ValidationEventHandler System.Xml.XsdValidatingReader::validationEvent
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___validationEvent_8;
	// System.Xml.XsdValidatingReader/ValidatingReaderState System.Xml.XsdValidatingReader::validationState
	int32_t ___validationState_9;
	// System.Xml.Schema.XmlValueGetter System.Xml.XsdValidatingReader::valueGetter
	XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF * ___valueGetter_10;
	// System.Xml.XmlNamespaceManager System.Xml.XsdValidatingReader::nsManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___nsManager_11;
	// System.Boolean System.Xml.XsdValidatingReader::manageNamespaces
	bool ___manageNamespaces_12;
	// System.Boolean System.Xml.XsdValidatingReader::processInlineSchema
	bool ___processInlineSchema_13;
	// System.Boolean System.Xml.XsdValidatingReader::replayCache
	bool ___replayCache_14;
	// System.Xml.ValidatingReaderNodeData System.Xml.XsdValidatingReader::cachedNode
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * ___cachedNode_15;
	// System.Xml.AttributePSVIInfo System.Xml.XsdValidatingReader::attributePSVI
	AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 * ___attributePSVI_16;
	// System.Int32 System.Xml.XsdValidatingReader::attributeCount
	int32_t ___attributeCount_17;
	// System.Int32 System.Xml.XsdValidatingReader::coreReaderAttributeCount
	int32_t ___coreReaderAttributeCount_18;
	// System.Int32 System.Xml.XsdValidatingReader::currentAttrIndex
	int32_t ___currentAttrIndex_19;
	// System.Xml.AttributePSVIInfo[] System.Xml.XsdValidatingReader::attributePSVINodes
	AttributePSVIInfoU5BU5D_tB383C48FDFE60597390398272BA52949B7ADE951* ___attributePSVINodes_20;
	// System.Collections.ArrayList System.Xml.XsdValidatingReader::defaultAttributes
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___defaultAttributes_21;
	// System.Xml.Schema.Parser System.Xml.XsdValidatingReader::inlineSchemaParser
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * ___inlineSchemaParser_22;
	// System.Object System.Xml.XsdValidatingReader::atomicValue
	RuntimeObject * ___atomicValue_23;
	// System.Xml.Schema.XmlSchemaInfo System.Xml.XsdValidatingReader::xmlSchemaInfo
	XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 * ___xmlSchemaInfo_24;
	// System.String System.Xml.XsdValidatingReader::originalAtomicValueString
	String_t* ___originalAtomicValueString_25;
	// System.Xml.XmlNameTable System.Xml.XsdValidatingReader::coreReaderNameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___coreReaderNameTable_26;
	// System.Xml.XsdCachingReader System.Xml.XsdValidatingReader::cachingReader
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51 * ___cachingReader_27;
	// System.Xml.ValidatingReaderNodeData System.Xml.XsdValidatingReader::textNode
	ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * ___textNode_28;
	// System.String System.Xml.XsdValidatingReader::NsXmlNs
	String_t* ___NsXmlNs_29;
	// System.String System.Xml.XsdValidatingReader::NsXs
	String_t* ___NsXs_30;
	// System.String System.Xml.XsdValidatingReader::NsXsi
	String_t* ___NsXsi_31;
	// System.String System.Xml.XsdValidatingReader::XsiType
	String_t* ___XsiType_32;
	// System.String System.Xml.XsdValidatingReader::XsiNil
	String_t* ___XsiNil_33;
	// System.String System.Xml.XsdValidatingReader::XsdSchema
	String_t* ___XsdSchema_34;
	// System.String System.Xml.XsdValidatingReader::XsiSchemaLocation
	String_t* ___XsiSchemaLocation_35;
	// System.String System.Xml.XsdValidatingReader::XsiNoNamespaceSchemaLocation
	String_t* ___XsiNoNamespaceSchemaLocation_36;
	// System.Xml.XmlCharType System.Xml.XsdValidatingReader::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_37;
	// System.Xml.IXmlLineInfo System.Xml.XsdValidatingReader::lineInfo
	RuntimeObject* ___lineInfo_38;
	// System.Xml.ReadContentAsBinaryHelper System.Xml.XsdValidatingReader::readBinaryHelper
	ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * ___readBinaryHelper_39;
	// System.Xml.XsdValidatingReader/ValidatingReaderState System.Xml.XsdValidatingReader::savedState
	int32_t ___savedState_40;

public:
	inline static int32_t get_offset_of_coreReader_3() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___coreReader_3)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_coreReader_3() const { return ___coreReader_3; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_coreReader_3() { return &___coreReader_3; }
	inline void set_coreReader_3(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___coreReader_3 = value;
		Il2CppCodeGenWriteBarrier((&___coreReader_3), value);
	}

	inline static int32_t get_offset_of_coreReaderNSResolver_4() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___coreReaderNSResolver_4)); }
	inline RuntimeObject* get_coreReaderNSResolver_4() const { return ___coreReaderNSResolver_4; }
	inline RuntimeObject** get_address_of_coreReaderNSResolver_4() { return &___coreReaderNSResolver_4; }
	inline void set_coreReaderNSResolver_4(RuntimeObject* value)
	{
		___coreReaderNSResolver_4 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderNSResolver_4), value);
	}

	inline static int32_t get_offset_of_thisNSResolver_5() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___thisNSResolver_5)); }
	inline RuntimeObject* get_thisNSResolver_5() const { return ___thisNSResolver_5; }
	inline RuntimeObject** get_address_of_thisNSResolver_5() { return &___thisNSResolver_5; }
	inline void set_thisNSResolver_5(RuntimeObject* value)
	{
		___thisNSResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___thisNSResolver_5), value);
	}

	inline static int32_t get_offset_of_validator_6() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___validator_6)); }
	inline XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C * get_validator_6() const { return ___validator_6; }
	inline XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C ** get_address_of_validator_6() { return &___validator_6; }
	inline void set_validator_6(XmlSchemaValidator_tF0EF8E608FF1DD4877C70FC7DC2DE2662A0B072C * value)
	{
		___validator_6 = value;
		Il2CppCodeGenWriteBarrier((&___validator_6), value);
	}

	inline static int32_t get_offset_of_xmlResolver_7() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___xmlResolver_7)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_7() const { return ___xmlResolver_7; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_7() { return &___xmlResolver_7; }
	inline void set_xmlResolver_7(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_7), value);
	}

	inline static int32_t get_offset_of_validationEvent_8() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___validationEvent_8)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_validationEvent_8() const { return ___validationEvent_8; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_validationEvent_8() { return &___validationEvent_8; }
	inline void set_validationEvent_8(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___validationEvent_8 = value;
		Il2CppCodeGenWriteBarrier((&___validationEvent_8), value);
	}

	inline static int32_t get_offset_of_validationState_9() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___validationState_9)); }
	inline int32_t get_validationState_9() const { return ___validationState_9; }
	inline int32_t* get_address_of_validationState_9() { return &___validationState_9; }
	inline void set_validationState_9(int32_t value)
	{
		___validationState_9 = value;
	}

	inline static int32_t get_offset_of_valueGetter_10() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___valueGetter_10)); }
	inline XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF * get_valueGetter_10() const { return ___valueGetter_10; }
	inline XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF ** get_address_of_valueGetter_10() { return &___valueGetter_10; }
	inline void set_valueGetter_10(XmlValueGetter_tE51E83B894C27BA5CD229FD9E8C5FC9C851555DF * value)
	{
		___valueGetter_10 = value;
		Il2CppCodeGenWriteBarrier((&___valueGetter_10), value);
	}

	inline static int32_t get_offset_of_nsManager_11() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___nsManager_11)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_nsManager_11() const { return ___nsManager_11; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_nsManager_11() { return &___nsManager_11; }
	inline void set_nsManager_11(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___nsManager_11 = value;
		Il2CppCodeGenWriteBarrier((&___nsManager_11), value);
	}

	inline static int32_t get_offset_of_manageNamespaces_12() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___manageNamespaces_12)); }
	inline bool get_manageNamespaces_12() const { return ___manageNamespaces_12; }
	inline bool* get_address_of_manageNamespaces_12() { return &___manageNamespaces_12; }
	inline void set_manageNamespaces_12(bool value)
	{
		___manageNamespaces_12 = value;
	}

	inline static int32_t get_offset_of_processInlineSchema_13() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___processInlineSchema_13)); }
	inline bool get_processInlineSchema_13() const { return ___processInlineSchema_13; }
	inline bool* get_address_of_processInlineSchema_13() { return &___processInlineSchema_13; }
	inline void set_processInlineSchema_13(bool value)
	{
		___processInlineSchema_13 = value;
	}

	inline static int32_t get_offset_of_replayCache_14() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___replayCache_14)); }
	inline bool get_replayCache_14() const { return ___replayCache_14; }
	inline bool* get_address_of_replayCache_14() { return &___replayCache_14; }
	inline void set_replayCache_14(bool value)
	{
		___replayCache_14 = value;
	}

	inline static int32_t get_offset_of_cachedNode_15() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___cachedNode_15)); }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * get_cachedNode_15() const { return ___cachedNode_15; }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F ** get_address_of_cachedNode_15() { return &___cachedNode_15; }
	inline void set_cachedNode_15(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * value)
	{
		___cachedNode_15 = value;
		Il2CppCodeGenWriteBarrier((&___cachedNode_15), value);
	}

	inline static int32_t get_offset_of_attributePSVI_16() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___attributePSVI_16)); }
	inline AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 * get_attributePSVI_16() const { return ___attributePSVI_16; }
	inline AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 ** get_address_of_attributePSVI_16() { return &___attributePSVI_16; }
	inline void set_attributePSVI_16(AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94 * value)
	{
		___attributePSVI_16 = value;
		Il2CppCodeGenWriteBarrier((&___attributePSVI_16), value);
	}

	inline static int32_t get_offset_of_attributeCount_17() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___attributeCount_17)); }
	inline int32_t get_attributeCount_17() const { return ___attributeCount_17; }
	inline int32_t* get_address_of_attributeCount_17() { return &___attributeCount_17; }
	inline void set_attributeCount_17(int32_t value)
	{
		___attributeCount_17 = value;
	}

	inline static int32_t get_offset_of_coreReaderAttributeCount_18() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___coreReaderAttributeCount_18)); }
	inline int32_t get_coreReaderAttributeCount_18() const { return ___coreReaderAttributeCount_18; }
	inline int32_t* get_address_of_coreReaderAttributeCount_18() { return &___coreReaderAttributeCount_18; }
	inline void set_coreReaderAttributeCount_18(int32_t value)
	{
		___coreReaderAttributeCount_18 = value;
	}

	inline static int32_t get_offset_of_currentAttrIndex_19() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___currentAttrIndex_19)); }
	inline int32_t get_currentAttrIndex_19() const { return ___currentAttrIndex_19; }
	inline int32_t* get_address_of_currentAttrIndex_19() { return &___currentAttrIndex_19; }
	inline void set_currentAttrIndex_19(int32_t value)
	{
		___currentAttrIndex_19 = value;
	}

	inline static int32_t get_offset_of_attributePSVINodes_20() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___attributePSVINodes_20)); }
	inline AttributePSVIInfoU5BU5D_tB383C48FDFE60597390398272BA52949B7ADE951* get_attributePSVINodes_20() const { return ___attributePSVINodes_20; }
	inline AttributePSVIInfoU5BU5D_tB383C48FDFE60597390398272BA52949B7ADE951** get_address_of_attributePSVINodes_20() { return &___attributePSVINodes_20; }
	inline void set_attributePSVINodes_20(AttributePSVIInfoU5BU5D_tB383C48FDFE60597390398272BA52949B7ADE951* value)
	{
		___attributePSVINodes_20 = value;
		Il2CppCodeGenWriteBarrier((&___attributePSVINodes_20), value);
	}

	inline static int32_t get_offset_of_defaultAttributes_21() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___defaultAttributes_21)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_defaultAttributes_21() const { return ___defaultAttributes_21; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_defaultAttributes_21() { return &___defaultAttributes_21; }
	inline void set_defaultAttributes_21(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___defaultAttributes_21 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttributes_21), value);
	}

	inline static int32_t get_offset_of_inlineSchemaParser_22() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___inlineSchemaParser_22)); }
	inline Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * get_inlineSchemaParser_22() const { return ___inlineSchemaParser_22; }
	inline Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 ** get_address_of_inlineSchemaParser_22() { return &___inlineSchemaParser_22; }
	inline void set_inlineSchemaParser_22(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920 * value)
	{
		___inlineSchemaParser_22 = value;
		Il2CppCodeGenWriteBarrier((&___inlineSchemaParser_22), value);
	}

	inline static int32_t get_offset_of_atomicValue_23() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___atomicValue_23)); }
	inline RuntimeObject * get_atomicValue_23() const { return ___atomicValue_23; }
	inline RuntimeObject ** get_address_of_atomicValue_23() { return &___atomicValue_23; }
	inline void set_atomicValue_23(RuntimeObject * value)
	{
		___atomicValue_23 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValue_23), value);
	}

	inline static int32_t get_offset_of_xmlSchemaInfo_24() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___xmlSchemaInfo_24)); }
	inline XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 * get_xmlSchemaInfo_24() const { return ___xmlSchemaInfo_24; }
	inline XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 ** get_address_of_xmlSchemaInfo_24() { return &___xmlSchemaInfo_24; }
	inline void set_xmlSchemaInfo_24(XmlSchemaInfo_tEC7C8E9E6FC5CF178F83789C9B3336C5A9145035 * value)
	{
		___xmlSchemaInfo_24 = value;
		Il2CppCodeGenWriteBarrier((&___xmlSchemaInfo_24), value);
	}

	inline static int32_t get_offset_of_originalAtomicValueString_25() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___originalAtomicValueString_25)); }
	inline String_t* get_originalAtomicValueString_25() const { return ___originalAtomicValueString_25; }
	inline String_t** get_address_of_originalAtomicValueString_25() { return &___originalAtomicValueString_25; }
	inline void set_originalAtomicValueString_25(String_t* value)
	{
		___originalAtomicValueString_25 = value;
		Il2CppCodeGenWriteBarrier((&___originalAtomicValueString_25), value);
	}

	inline static int32_t get_offset_of_coreReaderNameTable_26() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___coreReaderNameTable_26)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_coreReaderNameTable_26() const { return ___coreReaderNameTable_26; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_coreReaderNameTable_26() { return &___coreReaderNameTable_26; }
	inline void set_coreReaderNameTable_26(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___coreReaderNameTable_26 = value;
		Il2CppCodeGenWriteBarrier((&___coreReaderNameTable_26), value);
	}

	inline static int32_t get_offset_of_cachingReader_27() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___cachingReader_27)); }
	inline XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51 * get_cachingReader_27() const { return ___cachingReader_27; }
	inline XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51 ** get_address_of_cachingReader_27() { return &___cachingReader_27; }
	inline void set_cachingReader_27(XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51 * value)
	{
		___cachingReader_27 = value;
		Il2CppCodeGenWriteBarrier((&___cachingReader_27), value);
	}

	inline static int32_t get_offset_of_textNode_28() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___textNode_28)); }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * get_textNode_28() const { return ___textNode_28; }
	inline ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F ** get_address_of_textNode_28() { return &___textNode_28; }
	inline void set_textNode_28(ValidatingReaderNodeData_tB7D1981AED7CEDF168218AA0F59586DBB3502E3F * value)
	{
		___textNode_28 = value;
		Il2CppCodeGenWriteBarrier((&___textNode_28), value);
	}

	inline static int32_t get_offset_of_NsXmlNs_29() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___NsXmlNs_29)); }
	inline String_t* get_NsXmlNs_29() const { return ___NsXmlNs_29; }
	inline String_t** get_address_of_NsXmlNs_29() { return &___NsXmlNs_29; }
	inline void set_NsXmlNs_29(String_t* value)
	{
		___NsXmlNs_29 = value;
		Il2CppCodeGenWriteBarrier((&___NsXmlNs_29), value);
	}

	inline static int32_t get_offset_of_NsXs_30() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___NsXs_30)); }
	inline String_t* get_NsXs_30() const { return ___NsXs_30; }
	inline String_t** get_address_of_NsXs_30() { return &___NsXs_30; }
	inline void set_NsXs_30(String_t* value)
	{
		___NsXs_30 = value;
		Il2CppCodeGenWriteBarrier((&___NsXs_30), value);
	}

	inline static int32_t get_offset_of_NsXsi_31() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___NsXsi_31)); }
	inline String_t* get_NsXsi_31() const { return ___NsXsi_31; }
	inline String_t** get_address_of_NsXsi_31() { return &___NsXsi_31; }
	inline void set_NsXsi_31(String_t* value)
	{
		___NsXsi_31 = value;
		Il2CppCodeGenWriteBarrier((&___NsXsi_31), value);
	}

	inline static int32_t get_offset_of_XsiType_32() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___XsiType_32)); }
	inline String_t* get_XsiType_32() const { return ___XsiType_32; }
	inline String_t** get_address_of_XsiType_32() { return &___XsiType_32; }
	inline void set_XsiType_32(String_t* value)
	{
		___XsiType_32 = value;
		Il2CppCodeGenWriteBarrier((&___XsiType_32), value);
	}

	inline static int32_t get_offset_of_XsiNil_33() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___XsiNil_33)); }
	inline String_t* get_XsiNil_33() const { return ___XsiNil_33; }
	inline String_t** get_address_of_XsiNil_33() { return &___XsiNil_33; }
	inline void set_XsiNil_33(String_t* value)
	{
		___XsiNil_33 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNil_33), value);
	}

	inline static int32_t get_offset_of_XsdSchema_34() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___XsdSchema_34)); }
	inline String_t* get_XsdSchema_34() const { return ___XsdSchema_34; }
	inline String_t** get_address_of_XsdSchema_34() { return &___XsdSchema_34; }
	inline void set_XsdSchema_34(String_t* value)
	{
		___XsdSchema_34 = value;
		Il2CppCodeGenWriteBarrier((&___XsdSchema_34), value);
	}

	inline static int32_t get_offset_of_XsiSchemaLocation_35() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___XsiSchemaLocation_35)); }
	inline String_t* get_XsiSchemaLocation_35() const { return ___XsiSchemaLocation_35; }
	inline String_t** get_address_of_XsiSchemaLocation_35() { return &___XsiSchemaLocation_35; }
	inline void set_XsiSchemaLocation_35(String_t* value)
	{
		___XsiSchemaLocation_35 = value;
		Il2CppCodeGenWriteBarrier((&___XsiSchemaLocation_35), value);
	}

	inline static int32_t get_offset_of_XsiNoNamespaceSchemaLocation_36() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___XsiNoNamespaceSchemaLocation_36)); }
	inline String_t* get_XsiNoNamespaceSchemaLocation_36() const { return ___XsiNoNamespaceSchemaLocation_36; }
	inline String_t** get_address_of_XsiNoNamespaceSchemaLocation_36() { return &___XsiNoNamespaceSchemaLocation_36; }
	inline void set_XsiNoNamespaceSchemaLocation_36(String_t* value)
	{
		___XsiNoNamespaceSchemaLocation_36 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNoNamespaceSchemaLocation_36), value);
	}

	inline static int32_t get_offset_of_xmlCharType_37() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___xmlCharType_37)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_37() const { return ___xmlCharType_37; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_37() { return &___xmlCharType_37; }
	inline void set_xmlCharType_37(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_37 = value;
	}

	inline static int32_t get_offset_of_lineInfo_38() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___lineInfo_38)); }
	inline RuntimeObject* get_lineInfo_38() const { return ___lineInfo_38; }
	inline RuntimeObject** get_address_of_lineInfo_38() { return &___lineInfo_38; }
	inline void set_lineInfo_38(RuntimeObject* value)
	{
		___lineInfo_38 = value;
		Il2CppCodeGenWriteBarrier((&___lineInfo_38), value);
	}

	inline static int32_t get_offset_of_readBinaryHelper_39() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___readBinaryHelper_39)); }
	inline ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * get_readBinaryHelper_39() const { return ___readBinaryHelper_39; }
	inline ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 ** get_address_of_readBinaryHelper_39() { return &___readBinaryHelper_39; }
	inline void set_readBinaryHelper_39(ReadContentAsBinaryHelper_t3CD837B0BD1C1C1D2E4EEF057C0C22742A766797 * value)
	{
		___readBinaryHelper_39 = value;
		Il2CppCodeGenWriteBarrier((&___readBinaryHelper_39), value);
	}

	inline static int32_t get_offset_of_savedState_40() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5, ___savedState_40)); }
	inline int32_t get_savedState_40() const { return ___savedState_40; }
	inline int32_t* get_address_of_savedState_40() { return &___savedState_40; }
	inline void set_savedState_40(int32_t value)
	{
		___savedState_40 = value;
	}
};

struct XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5_StaticFields
{
public:
	// System.Type modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XsdValidatingReader::TypeOfString
	Type_t * ___TypeOfString_41;

public:
	inline static int32_t get_offset_of_TypeOfString_41() { return static_cast<int32_t>(offsetof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5_StaticFields, ___TypeOfString_41)); }
	inline Type_t * get_TypeOfString_41() const { return ___TypeOfString_41; }
	inline Type_t ** get_address_of_TypeOfString_41() { return &___TypeOfString_41; }
	inline void set_TypeOfString_41(Type_t * value)
	{
		___TypeOfString_41 = value;
		Il2CppCodeGenWriteBarrier((&___TypeOfString_41), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDVALIDATINGREADER_T34A7241660D82975EDF0B427CBDF6CC04B1B91E5_H
#ifndef CACHINGEVENTHANDLER_T44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6_H
#define CACHINGEVENTHANDLER_T44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.CachingEventHandler
struct  CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHINGEVENTHANDLER_T44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6_H
#ifndef XMLNODECHANGEDEVENTHANDLER_T730DECDAE07BB728186F47DDF2A569F50A889838_H
#define XMLNODECHANGEDEVENTHANDLER_T730DECDAE07BB728186F47DDF2A569F50A889838_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNodeChangedEventHandler
struct  XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODECHANGEDEVENTHANDLER_T730DECDAE07BB728186F47DDF2A569F50A889838_H
#ifndef ONDEFAULTATTRIBUTEUSEDELEGATE_TE83A97BD37E08D3C5AA377CEA6388782D99EA34D_H
#define ONDEFAULTATTRIBUTEUSEDELEGATE_TE83A97BD37E08D3C5AA377CEA6388782D99EA34D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlTextReaderImpl/OnDefaultAttributeUseDelegate
struct  OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONDEFAULTATTRIBUTEUSEDELEGATE_TE83A97BD37E08D3C5AA377CEA6388782D99EA34D_H
#ifndef XMLUTF8RAWTEXTWRITERINDENT_T28627A35CC18342CA3A321923AB37F3441BB5840_H
#define XMLUTF8RAWTEXTWRITERINDENT_T28627A35CC18342CA3A321923AB37F3441BB5840_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlUtf8RawTextWriterIndent
struct  XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840  : public XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E
{
public:
	// System.Int32 System.Xml.XmlUtf8RawTextWriterIndent::indentLevel
	int32_t ___indentLevel_26;
	// System.Boolean System.Xml.XmlUtf8RawTextWriterIndent::newLineOnAttributes
	bool ___newLineOnAttributes_27;
	// System.String System.Xml.XmlUtf8RawTextWriterIndent::indentChars
	String_t* ___indentChars_28;
	// System.Boolean System.Xml.XmlUtf8RawTextWriterIndent::mixedContent
	bool ___mixedContent_29;
	// System.Xml.BitStack System.Xml.XmlUtf8RawTextWriterIndent::mixedContentStack
	BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * ___mixedContentStack_30;
	// System.Xml.ConformanceLevel System.Xml.XmlUtf8RawTextWriterIndent::conformanceLevel
	int32_t ___conformanceLevel_31;

public:
	inline static int32_t get_offset_of_indentLevel_26() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___indentLevel_26)); }
	inline int32_t get_indentLevel_26() const { return ___indentLevel_26; }
	inline int32_t* get_address_of_indentLevel_26() { return &___indentLevel_26; }
	inline void set_indentLevel_26(int32_t value)
	{
		___indentLevel_26 = value;
	}

	inline static int32_t get_offset_of_newLineOnAttributes_27() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___newLineOnAttributes_27)); }
	inline bool get_newLineOnAttributes_27() const { return ___newLineOnAttributes_27; }
	inline bool* get_address_of_newLineOnAttributes_27() { return &___newLineOnAttributes_27; }
	inline void set_newLineOnAttributes_27(bool value)
	{
		___newLineOnAttributes_27 = value;
	}

	inline static int32_t get_offset_of_indentChars_28() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___indentChars_28)); }
	inline String_t* get_indentChars_28() const { return ___indentChars_28; }
	inline String_t** get_address_of_indentChars_28() { return &___indentChars_28; }
	inline void set_indentChars_28(String_t* value)
	{
		___indentChars_28 = value;
		Il2CppCodeGenWriteBarrier((&___indentChars_28), value);
	}

	inline static int32_t get_offset_of_mixedContent_29() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___mixedContent_29)); }
	inline bool get_mixedContent_29() const { return ___mixedContent_29; }
	inline bool* get_address_of_mixedContent_29() { return &___mixedContent_29; }
	inline void set_mixedContent_29(bool value)
	{
		___mixedContent_29 = value;
	}

	inline static int32_t get_offset_of_mixedContentStack_30() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___mixedContentStack_30)); }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * get_mixedContentStack_30() const { return ___mixedContentStack_30; }
	inline BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 ** get_address_of_mixedContentStack_30() { return &___mixedContentStack_30; }
	inline void set_mixedContentStack_30(BitStack_tA699E16D899D050049FF82EC15EC8B8F36CFB2E7 * value)
	{
		___mixedContentStack_30 = value;
		Il2CppCodeGenWriteBarrier((&___mixedContentStack_30), value);
	}

	inline static int32_t get_offset_of_conformanceLevel_31() { return static_cast<int32_t>(offsetof(XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840, ___conformanceLevel_31)); }
	inline int32_t get_conformanceLevel_31() const { return ___conformanceLevel_31; }
	inline int32_t* get_address_of_conformanceLevel_31() { return &___conformanceLevel_31; }
	inline void set_conformanceLevel_31(int32_t value)
	{
		___conformanceLevel_31 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLUTF8RAWTEXTWRITERINDENT_T28627A35CC18342CA3A321923AB37F3441BB5840_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1700 = { sizeof (EntityType_tD1C5D0F2F43C096B07783E681D885A6F511F0657)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1700[9] = 
{
	EntityType_tD1C5D0F2F43C096B07783E681D885A6F511F0657::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1701 = { sizeof (EntityExpandType_tC9953664AB0CE6F0813792286A9F3F6D6EF4A878)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1701[4] = 
{
	EntityExpandType_tC9953664AB0CE6F0813792286A9F3F6D6EF4A878::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1702 = { sizeof (IncrementalReadState_t9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1702[16] = 
{
	IncrementalReadState_t9D04D5EB17C9FE284AD3C7D5F3BB74EAA765653C::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1703 = { sizeof (LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1703[10] = 
{
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_useAsync_0(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputStream_1(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputBytes_2(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputByteCount_3(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputbaseUri_4(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputUriStr_5(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputUriResolver_6(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputContext_7(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_inputTextReader_8(),
	LaterInitParam_tE2FFFA55A2C7B183721762B4AC4B0DF93485A1AF::get_offset_of_initType_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1704 = { sizeof (InitInputType_tBF51CCB79B0C9D912C92528B2B8F1DBCC3649175)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1704[5] = 
{
	InitInputType_tBF51CCB79B0C9D912C92528B2B8F1DBCC3649175::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1705 = { sizeof (ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1705[21] = 
{
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_chars_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_charPos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_charsUsed_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_encoding_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_appendMode_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_stream_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_decoder_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_bytes_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_bytePos_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_bytesUsed_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_textReader_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_lineNo_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_lineStartPos_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_baseUriStr_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_baseUri_14() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_isEof_15() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_isStreamEof_16() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_entity_17() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_entityId_18() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_eolNormalized_19() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsingState_tE4A8E7F14B2068AE43ECF99F81F55B0301A551A2::get_offset_of_entityResolvedManually_20() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1706 = { sizeof (XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1706[4] = 
{
	XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52::get_offset_of_xmlSpace_0(),
	XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52::get_offset_of_xmlLang_1(),
	XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52::get_offset_of_defaultNamespace_2(),
	XmlContext_tC94B1A0D023B096ADF338FA72FB55D0E2DBF9B52::get_offset_of_previousContext_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1707 = { sizeof (NoNamespaceManager_t11014200BFBB3974A5E72A75867B9730C7D7E883), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1708 = { sizeof (DtdParserProxy_tF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1708[1] = 
{
	DtdParserProxy_tF94C8FBE3EC41EF7C960BEDC0CD3DD25090265B7::get_offset_of_reader_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1709 = { sizeof (NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF), -1, sizeof(NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1709[20] = 
{
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF_StaticFields::get_offset_of_s_None_0(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_type_1(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_localName_2(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_prefix_3(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_ns_4(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_nameWPrefix_5(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_value_6(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_chars_7(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_valueStartPos_8(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_valueLength_9(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_lineInfo_10(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_lineInfo2_11(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_quoteChar_12(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_depth_13(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_isEmptyOrDefault_14(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_entityId_15(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_xmlContextPushed_16(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_nextAttrValueChunk_17(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_schemaType_18(),
	NodeData_tAACF4D5ECADB9124568D01525E72D14495BC5ACF::get_offset_of_typedValue_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1710 = { sizeof (DtdDefaultAttributeInfoToNodeDataComparer_t0B9FEF7A80832980C3E32D47BE835964E8B423B7), -1, sizeof(DtdDefaultAttributeInfoToNodeDataComparer_t0B9FEF7A80832980C3E32D47BE835964E8B423B7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1710[1] = 
{
	DtdDefaultAttributeInfoToNodeDataComparer_t0B9FEF7A80832980C3E32D47BE835964E8B423B7_StaticFields::get_offset_of_s_instance_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1711 = { sizeof (OnDefaultAttributeUseDelegate_tE83A97BD37E08D3C5AA377CEA6388782D99EA34D), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1712 = { sizeof (Formatting_tA4B29B0BD063518BB29B92C6B165E39D01C553E2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1712[3] = 
{
	Formatting_tA4B29B0BD063518BB29B92C6B165E39D01C553E2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1713 = { sizeof (XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6), -1, sizeof(XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1713[28] = 
{
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_textWriter_1(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_xmlEncoder_2(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_encoding_3(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_formatting_4(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_indented_5(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_indentation_6(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_indentChar_7(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_stack_8(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_top_9(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_stateTable_10(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_currentState_11(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_lastToken_12(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_base64Encoder_13(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_quoteChar_14(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_curQuoteChar_15(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_namespaces_16(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_specialAttr_17(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_prefixForXmlNs_18(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_flush_19(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_nsStack_20(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_nsTop_21(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_nsHashtable_22(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_useNsHashtable_23(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6::get_offset_of_xmlCharType_24(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields::get_offset_of_stateName_25(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields::get_offset_of_tokenName_26(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields::get_offset_of_stateTableDefault_27(),
	XmlTextWriter_tAAB86C3C398B243DFB3EFD3BB4356B4821F1CEB6_StaticFields::get_offset_of_stateTableDocument_28(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1714 = { sizeof (NamespaceState_tA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1714[5] = 
{
	NamespaceState_tA6D09DE55EC80A8EB2C0C5853DC6EDA93857DD32::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1715 = { sizeof (TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5)+ sizeof (RuntimeObject), sizeof(TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1715[9] = 
{
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_prefix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_defaultNs_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_defaultNsState_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_xmlSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_xmlLang_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_prevNsTop_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_prefixCount_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TagInfo_t9F395B388162AB8B9277E4ABA1ADEF785AE197B5::get_offset_of_mixed_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1716 = { sizeof (Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B)+ sizeof (RuntimeObject), sizeof(Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1716[4] = 
{
	Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B::get_offset_of_ns_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B::get_offset_of_declared_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t092048EEBC7FF22AF20114DDA7633072C44CA26B::get_offset_of_prevNsIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1717 = { sizeof (SpecialAttr_tC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1717[5] = 
{
	SpecialAttr_tC6122E1A1EC64E66D03B8B3D17FE6FE0522D6528::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1718 = { sizeof (State_t8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1718[11] = 
{
	State_t8831DFDA8E7A6CD6ACC98BD44F6BEF50B0C0E648::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1719 = { sizeof (Token_tBCB56C1082F0646C24614CAED556F59987BAFA32)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1719[15] = 
{
	Token_tBCB56C1082F0646C24614CAED556F59987BAFA32::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1720 = { sizeof (XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1720[23] = 
{
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_useAsync_3(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_bufBytes_4(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_stream_5(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_encoding_6(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_xmlCharType_7(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_bufPos_8(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_textPos_9(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_contentPos_10(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_cdataPos_11(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_attrEndPos_12(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_bufLen_13(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_writeToNull_14(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_hadDoubleBracket_15(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_inAttributeValue_16(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_newLineHandling_17(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_closeOutput_18(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_omitXmlDeclaration_19(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_newLineChars_20(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_checkCharacters_21(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_standalone_22(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_outputMethod_23(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_autoXmlDeclaration_24(),
	XmlUtf8RawTextWriter_tD2249A207D4AD1A47BDD8509C0496CB3E4F6E92E::get_offset_of_mergeCDataSections_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1721 = { sizeof (XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1721[6] = 
{
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_indentLevel_26(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_newLineOnAttributes_27(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_indentChars_28(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_mixedContent_29(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_mixedContentStack_30(),
	XmlUtf8RawTextWriterIndent_t28627A35CC18342CA3A321923AB37F3441BB5840::get_offset_of_conformanceLevel_31(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1722 = { sizeof (XmlValidatingReader_t4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1722[1] = 
{
	XmlValidatingReader_t4A9D568A29AFF61C6A1CE073BFA9BF17EB9BAE92::get_offset_of_impl_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1723 = { sizeof (XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E), -1, sizeof(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1723[13] = 
{
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_coreReader_3(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_coreReaderImpl_4(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_coreReaderNSResolver_5(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_validationType_6(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_validator_7(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_schemaCollection_8(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_processIdentityConstraints_9(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_parsingFunction_10(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_eventHandling_11(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_parserContext_12(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_readBinaryHelper_13(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E::get_offset_of_outerReader_14(),
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E_StaticFields::get_offset_of_s_tempResolver_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1724 = { sizeof (ParsingFunction_t972AAE136ADB01CEF82FE8710B519BBF44704B92)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1724[9] = 
{
	ParsingFunction_t972AAE136ADB01CEF82FE8710B519BBF44704B92::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1725 = { sizeof (ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1725[2] = 
{
	ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1::get_offset_of_reader_0(),
	ValidationEventHandling_tB979AC71B85B7B96E85CF917D8F33BEC867F1EC1::get_offset_of_eventHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1726 = { sizeof (XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A), -1, sizeof(XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1726[30] = 
{
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_writer_1(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_rawWriter_2(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_predefinedNamespaces_3(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_nsStack_4(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_nsTop_5(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_nsHashtable_6(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_useNsHashtable_7(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_elemScopeStack_8(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_elemTop_9(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrStack_10(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrCount_11(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrHashTable_12(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_specAttr_13(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_attrValueCache_14(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_curDeclPrefix_15(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_stateTable_16(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_currentState_17(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_checkCharacters_18(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_omitDuplNamespaces_19(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_writeEndDocumentOnClose_20(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_conformanceLevel_21(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_dtdWritten_22(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_xmlDeclFollows_23(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_xmlCharType_24(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A::get_offset_of_hasher_25(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_stateName_26(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_tokenName_27(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_state2WriteState_28(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_StateTableDocument_29(),
	XmlWellFormedWriter_t8E017277086D1C79023B6E28A84F46FB36E9AF8A_StaticFields::get_offset_of_StateTableAuto_30(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1727 = { sizeof (State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1727[34] = 
{
	State_t296D8DF2FCC0329CF5A5F961B81FA56ADFC3103D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1728 = { sizeof (Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1728[16] = 
{
	Token_t2F590F413BBC0AAE03A6153F994A1ECB8F527F4D::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1729 = { sizeof (NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1729[1] = 
{
	NamespaceResolverProxy_tC483EA09DCE72B15A815CF58FE8DC0FDD237C452::get_offset_of_wfWriter_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1730 = { sizeof (ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7)+ sizeof (RuntimeObject), sizeof(ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1730[6] = 
{
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_prevNSTop_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_prefix_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_localName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_namespaceUri_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_xmlSpace_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ElementScope_t8DDAA195533D990229BE354A121DC9F68350F0A7::get_offset_of_xmlLang_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1731 = { sizeof (NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1731[5] = 
{
	NamespaceKind_tE3C39939D8B71DF91CD4F53D5FF8520A9361D4ED::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1732 = { sizeof (Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438)+ sizeof (RuntimeObject), sizeof(Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1732[4] = 
{
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_namespaceUri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_kind_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Namespace_t5D036A8DE620698F09AD5F2444482DD7A6D86438::get_offset_of_prevNsIndex_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1733 = { sizeof (AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298)+ sizeof (RuntimeObject), sizeof(AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1733[4] = 
{
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_prefix_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_namespaceUri_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_localName_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	AttrName_t56333AEE26116ABEF12DF292DB01D863108AD298::get_offset_of_prev_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1734 = { sizeof (SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1734[6] = 
{
	SpecialAttribute_t5A666AF6BF22A7E35321430E1059E7429866A7B0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1735 = { sizeof (AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1735[5] = 
{
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_stringValue_0(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_singleStringValue_1(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_items_2(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_firstItem_3(),
	AttributeValueCache_t810F1A90B8C60C2C91358AE02E7CEF1D975245FA::get_offset_of_lastItem_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1736 = { sizeof (ItemType_t492099D243BA127F275386D81B3E94A40FA53B35)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1736[10] = 
{
	ItemType_t492099D243BA127F275386D81B3E94A40FA53B35::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1737 = { sizeof (Item_t5F7D0B65E37A6039818862DBBE1FACBE01D1E74A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1737[2] = 
{
	Item_t5F7D0B65E37A6039818862DBBE1FACBE01D1E74A::get_offset_of_type_0(),
	Item_t5F7D0B65E37A6039818862DBBE1FACBE01D1E74A::get_offset_of_data_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1738 = { sizeof (BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1738[3] = 
{
	BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3::get_offset_of_buffer_0(),
	BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3::get_offset_of_index_1(),
	BufferChunk_tA67064620A574CA90D827C2A5516F6A2986DF4C3::get_offset_of_count_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1739 = { sizeof (WriteState_tB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1739[8] = 
{
	WriteState_tB6E09FBE3E53ABF78DCE76F82D8587ACA70EE132::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1740 = { sizeof (XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1740[1] = 
{
	XmlWriter_t4FAF83E5244FC8F339B19D481C348ACA1510E869::get_offset_of_writeNodeBuffer_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1741 = { sizeof (XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1741[5] = 
{
	XmlOutputMethod_tF47F7732ECF902DFC04BAB9F9B7025F94DA862D4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1742 = { sizeof (TriState_t7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1742[4] = 
{
	TriState_t7F02F6CD4AE210F0DC6B2C03291967B54D0D77B0::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1743 = { sizeof (XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1743[4] = 
{
	XmlStandalone_tD77DD6044609EA966459D821754EEE0BED37B39B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1744 = { sizeof (XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1744[23] = 
{
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_useAsync_0(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_encoding_1(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_omitXmlDecl_2(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_newLineHandling_3(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_newLineChars_4(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_indent_5(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_indentChars_6(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_newLineOnAttributes_7(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_closeOutput_8(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_namespaceHandling_9(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_conformanceLevel_10(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_checkCharacters_11(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_writeEndDocumentOnClose_12(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_outputMethod_13(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_cdataSections_14(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_doNotEscapeUriAttributes_15(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_mergeCDataSections_16(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_mediaType_17(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_docTypeSystem_18(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_docTypePublic_19(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_standalone_20(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_autoXmlDecl_21(),
	XmlWriterSettings_tD14B737CEA4EE4FB8329D33B62F13C6766BD75A3::get_offset_of_isReadOnly_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1745 = { sizeof (XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1745[15] = 
{
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_coreReader_3(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_coreReaderNameTable_4(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_contentEvents_5(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_attributeEvents_6(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_cachedNode_7(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_cacheState_8(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_contentIndex_9(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_attributeCount_10(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_returnOriginalStringValues_11(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_cacheHandler_12(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_currentAttrIndex_13(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_currentContentIndex_14(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_readAhead_15(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_lineInfo_16(),
	XsdCachingReader_t8EA10A18D0BF0FC40EC21E5C14F47233231D2B51::get_offset_of_textNode_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1746 = { sizeof (CachingReaderState_t28388E57781451F791B55B64D533099BA1903229)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1746[7] = 
{
	CachingReaderState_t28388E57781451F791B55B64D533099BA1903229::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1747 = { sizeof (CachingEventHandler_t44C6A67E0AAD2F217D8A9FFAAFF39575AEF642F6), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1748 = { sizeof (AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1748[4] = 
{
	AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94::get_offset_of_localName_0(),
	AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94::get_offset_of_namespaceUri_1(),
	AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94::get_offset_of_typedAttributeValue_2(),
	AttributePSVIInfo_t1991F050207B868048BC97BE0C50F6CC9A430C94::get_offset_of_attributeSchemaInfo_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1749 = { sizeof (XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5), -1, sizeof(XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1749[39] = 
{
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_coreReader_3(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_coreReaderNSResolver_4(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_thisNSResolver_5(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_validator_6(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_xmlResolver_7(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_validationEvent_8(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_validationState_9(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_valueGetter_10(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_nsManager_11(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_manageNamespaces_12(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_processInlineSchema_13(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_replayCache_14(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_cachedNode_15(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_attributePSVI_16(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_attributeCount_17(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_coreReaderAttributeCount_18(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_currentAttrIndex_19(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_attributePSVINodes_20(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_defaultAttributes_21(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_inlineSchemaParser_22(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_atomicValue_23(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_xmlSchemaInfo_24(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_originalAtomicValueString_25(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_coreReaderNameTable_26(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_cachingReader_27(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_textNode_28(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_NsXmlNs_29(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_NsXs_30(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_NsXsi_31(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_XsiType_32(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_XsiNil_33(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_XsdSchema_34(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_XsiSchemaLocation_35(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_XsiNoNamespaceSchemaLocation_36(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_xmlCharType_37(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_lineInfo_38(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_readBinaryHelper_39(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5::get_offset_of_savedState_40(),
	XsdValidatingReader_t34A7241660D82975EDF0B427CBDF6CC04B1B91E5_StaticFields::get_offset_of_TypeOfString_41(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1750 = { sizeof (ValidatingReaderState_tC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1750[14] = 
{
	ValidatingReaderState_tC8CD733F0A228AA8345EEB6CC764A4F08AC0D2CE::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1751 = { sizeof (DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1751[5] = 
{
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A::get_offset_of_entries_0(),
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A::get_offset_of_count_1(),
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A::get_offset_of_mask_2(),
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A::get_offset_of_ownerDocument_3(),
	DomNameTable_t09CF0ACAE0D0EA06D0D6892AE0E7D17C7F931C9A::get_offset_of_nameTable_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1752 = { sizeof (XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1752[2] = 
{
	XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA::get_offset_of_name_1(),
	XmlAttribute_tEAB5F066D1D6965D6528617BD89826AE7114DEFA::get_offset_of_lastChild_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1753 = { sizeof (XmlAttributeCollection_tF927E860FF6289A37D53FEFBB8CECA9CBBC8C49E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1754 = { sizeof (XmlCDataSection_t4DFDF8D529E42121C9B3A0EE52BD6B5AEC1F9709), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1755 = { sizeof (XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1755[1] = 
{
	XmlCharacterData_tA7587D706680E42BD7A094F87CB0859C840A8531::get_offset_of_data_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1756 = { sizeof (XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1756[3] = 
{
	XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA::get_offset_of_container_0(),
	XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA::get_offset_of_child_1(),
	XmlChildEnumerator_tA5E9216BDDC135630E6C1283B8976192A9847ABA::get_offset_of_isFirst_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1757 = { sizeof (XmlChildNodes_tEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1757[1] = 
{
	XmlChildNodes_tEDFFAA22FB673580AA2BE8D818DC8D90319DE7F4::get_offset_of_container_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1758 = { sizeof (XmlComment_tBC2125BFD52EF7590FE6C06B2D92C61FF344860D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1759 = { sizeof (XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1759[3] = 
{
	XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200::get_offset_of_version_2(),
	XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200::get_offset_of_encoding_3(),
	XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200::get_offset_of_standalone_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1760 = { sizeof (XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97), -1, sizeof(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable1760[44] = 
{
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_implementation_1(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_domNameTable_2(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_lastChild_3(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_entities_4(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_htElementIdMap_5(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_htElementIDAttrDecl_6(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_schemaInfo_7(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_schemas_8(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_reportValidity_9(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_actualLoadingStatus_10(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_onNodeInsertingDelegate_11(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_onNodeInsertedDelegate_12(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_onNodeRemovingDelegate_13(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_onNodeRemovedDelegate_14(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_onNodeChangingDelegate_15(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_onNodeChangedDelegate_16(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_fEntRefNodesPresent_17(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_fCDataNodesPresent_18(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_preserveWhitespace_19(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_isLoading_20(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strDocumentName_21(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strDocumentFragmentName_22(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strCommentName_23(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strTextName_24(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strCDataSectionName_25(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strEntityName_26(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strID_27(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strXmlns_28(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strXml_29(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strSpace_30(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strLang_31(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strEmpty_32(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strNonSignificantWhitespaceName_33(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strSignificantWhitespaceName_34(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strReservedXmlns_35(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_strReservedXml_36(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_baseURI_37(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_resolver_38(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_bSetResolver_39(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97::get_offset_of_objLock_40(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields::get_offset_of_EmptyEnumerator_41(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields::get_offset_of_NotKnownSchemaInfo_42(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields::get_offset_of_ValidSchemaInfo_43(),
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97_StaticFields::get_offset_of_InvalidSchemaInfo_44(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1761 = { sizeof (XmlDocumentFragment_tF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1761[1] = 
{
	XmlDocumentFragment_tF5CEFFDD9372DB4C0035CCA961B5540FF94EB93D::get_offset_of_lastChild_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1762 = { sizeof (XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1762[8] = 
{
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_name_2(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_publicId_3(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_systemId_4(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_internalSubset_5(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_namespaces_6(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_entities_7(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_notations_8(),
	XmlDocumentType_t9AB8D5C3C27B699B885D941D4B4735A6EE039136::get_offset_of_schemaInfo_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1763 = { sizeof (XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1763[3] = 
{
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC::get_offset_of_name_2(),
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC::get_offset_of_attributes_3(),
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC::get_offset_of_lastChild_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1764 = { sizeof (XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1764[8] = 
{
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_publicId_1(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_systemId_2(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_notationName_3(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_name_4(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_unparsedReplacementStr_5(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_baseURI_6(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_lastChild_7(),
	XmlEntity_t8353905D145B17CEC6FAAFF0A21B15C2BCBBA1FA::get_offset_of_childrenFoliating_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1765 = { sizeof (XmlEntityReference_t878158918F3AD20215DE606E9C1725A05372A204), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1765[2] = 
{
	XmlEntityReference_t878158918F3AD20215DE606E9C1725A05372A204::get_offset_of_name_2(),
	XmlEntityReference_t878158918F3AD20215DE606E9C1725A05372A204::get_offset_of_lastChild_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1766 = { sizeof (XmlNodeChangedAction_tD5DC99DE9A26516B08D3AFBE8B31613469009E29)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1766[4] = 
{
	XmlNodeChangedAction_tD5DC99DE9A26516B08D3AFBE8B31613469009E29::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1767 = { sizeof (XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1767[1] = 
{
	XmlImplementation_t7C01D70C3943C7AA929C106D360E90E2D576E1EF::get_offset_of_nameTable_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1768 = { sizeof (XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1768[1] = 
{
	XmlLinkedNode_t4F76C8580C2E6D2908D88AC84A86060FA9289A0E::get_offset_of_next_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1769 = { sizeof (XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1769[3] = 
{
	XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D::get_offset_of_doc_0(),
	XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D::get_offset_of_reader_1(),
	XmlLoader_t8CC4B8C953AE50AD48AA6CC1DE637EF22AC6243D::get_offset_of_preserveWhitespace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1770 = { sizeof (XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1770[7] = 
{
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_prefix_0(),
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_localName_1(),
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_ns_2(),
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_name_3(),
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_hashCode_4(),
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_ownerDoc_5(),
	XmlName_t993BD9C827C8B0B6A0B7C49A03F2D14740AC2682::get_offset_of_next_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1771 = { sizeof (XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1771[4] = 
{
	XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE::get_offset_of_flags_7(),
	XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE::get_offset_of_memberType_8(),
	XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE::get_offset_of_schemaType_9(),
	XmlNameEx_t33130AC6936DEC05C2496BAA31DDB0867F4FD8DE::get_offset_of_decl_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1772 = { sizeof (XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1772[2] = 
{
	XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31::get_offset_of_parent_0(),
	XmlNamedNodeMap_t260246787BA7B6747AB878378D22EC7693465E31::get_offset_of_nodes_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1773 = { sizeof (SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E)+ sizeof (RuntimeObject), sizeof(SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1773[1] = 
{
	SmallXmlNodeList_t962D7A66CF19950FE6DFA9476903952B76844A1E::get_offset_of_field_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1774 = { sizeof (SingleObjectEnumerator_tA9C95CFC90A374F3FB891F834486863C55883F7A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1774[2] = 
{
	SingleObjectEnumerator_tA9C95CFC90A374F3FB891F834486863C55883F7A::get_offset_of_loneValue_0(),
	SingleObjectEnumerator_tA9C95CFC90A374F3FB891F834486863C55883F7A::get_offset_of_position_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1775 = { sizeof (XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1775[1] = 
{
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB::get_offset_of_parentNode_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1776 = { sizeof (XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1776[6] = 
{
	XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA::get_offset_of_action_1(),
	XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA::get_offset_of_node_2(),
	XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA::get_offset_of_oldParent_3(),
	XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA::get_offset_of_newParent_4(),
	XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA::get_offset_of_oldValue_5(),
	XmlNodeChangedEventArgs_t0376E69AE8B0ED655552CD4B073F3213AE414BDA::get_offset_of_newValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1777 = { sizeof (XmlNodeChangedEventHandler_t730DECDAE07BB728186F47DDF2A569F50A889838), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1778 = { sizeof (XmlNodeList_t6A2162EDB563F1707F00C5156460E1073244C8E7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1779 = { sizeof (XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1779[17] = 
{
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_curNode_0(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_elemNode_1(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_logNode_2(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_attrIndex_3(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_logAttrIndex_4(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_nameTable_5(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_doc_6(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_nAttrInd_7(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_nDeclarationAttrCount_8(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_nDocTypeAttrCount_9(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_nLogLevel_10(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_nLogAttrInd_11(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_bLogOnAttrVal_12(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_bCreatedOnAttribute_13(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_decNodeAttributes_14(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_docTypeNodeAttributes_15(),
	XmlNodeReaderNavigator_t6D2E7338E1343F01CE80FB6DEB0722D4CF7ABA1B::get_offset_of_bOnAttrVal_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1780 = { sizeof (VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465)+ sizeof (RuntimeObject), sizeof(VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable1780[2] = 
{
	VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465::get_offset_of_name_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	VirtualAttribute_t3CED95AE4D2D98B3E1C79C4CA6C4B076326DC465::get_offset_of_value_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1781 = { sizeof (XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1781[9] = 
{
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_readerNav_3(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_nodeType_4(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_curDepth_5(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_readState_6(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_fEOF_7(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_bResolveEntity_8(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_bStartFromDocument_9(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_bInReadBinary_10(),
	XmlNodeReader_t6AD91B6CB41F2761AC4C77F0968339AE7310DBE6::get_offset_of_readBinaryHelper_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1782 = { sizeof (XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1782[3] = 
{
	XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53::get_offset_of_publicId_1(),
	XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53::get_offset_of_systemId_2(),
	XmlNotation_tA1B86454CE48EBA5498A774B06535CEC10ABBF53::get_offset_of_name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1783 = { sizeof (XmlProcessingInstruction_t87662A3B85D8B2F79E74917CA294C8954F71C6D0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1783[2] = 
{
	XmlProcessingInstruction_t87662A3B85D8B2F79E74917CA294C8954F71C6D0::get_offset_of_target_2(),
	XmlProcessingInstruction_t87662A3B85D8B2F79E74917CA294C8954F71C6D0::get_offset_of_data_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1784 = { sizeof (XmlSignificantWhitespace_tCCDC4754CB2DD2682B9FDAE23DBF09603618F5FB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1785 = { sizeof (XmlText_t9C88A0254C370EBF90FC518EA99EF5D1B7D9E649), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1786 = { sizeof (XmlUnspecifiedAttribute_t1C82B285B60096D164B703202A8BD91A2009DE5D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1786[1] = 
{
	XmlUnspecifiedAttribute_t1C82B285B60096D164B703202A8BD91A2009DE5D::get_offset_of_fSpecified_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1787 = { sizeof (XmlWhitespace_tF5EA718743A148EBF7594ADF5A06B3224857AD4D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1788 = { sizeof (EmptyEnumerator_t7702FAD652CDD694DC3D136712CA48C48FFB0DC3), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1789 = { sizeof (HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1789[5] = 
{
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE::get_offset_of_stack_0(),
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE::get_offset_of_growthRate_1(),
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE::get_offset_of_used_2(),
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE::get_offset_of_size_3(),
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE::get_offset_of_limit_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1790 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1791 = { sizeof (PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1792 = { sizeof (ReaderPositionInfo_t835B819542967DE0AA809FE5423E59D9F4283EB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1792[1] = 
{
	ReaderPositionInfo_t835B819542967DE0AA809FE5423E59D9F4283EB9::get_offset_of_lineInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1793 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1794 = { sizeof (LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5)+ sizeof (RuntimeObject), sizeof(LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5 ), 0, 0 };
extern const int32_t g_FieldOffsetTable1794[2] = 
{
	LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5::get_offset_of_lineNo_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	LineInfo_t7E3D50496C7BA51B84D485D0A30B9006943544E5::get_offset_of_linePos_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1795 = { sizeof (NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1795[4] = 
{
	NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C::get_offset_of_entries_0(),
	NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C::get_offset_of_count_1(),
	NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C::get_offset_of_mask_2(),
	NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C::get_offset_of_hashCodeRandomizer_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1796 = { sizeof (Entry_tF2506228AAB202078CB7A755668744901B207977), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1796[3] = 
{
	Entry_tF2506228AAB202078CB7A755668744901B207977::get_offset_of_str_0(),
	Entry_tF2506228AAB202078CB7A755668744901B207977::get_offset_of_hashCode_1(),
	Entry_tF2506228AAB202078CB7A755668744901B207977::get_offset_of_next_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1797 = { sizeof (Ref_t58DB2E95F0FC9A292628F954DDC3D643DF1B692E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1798 = { sizeof (DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable1798[33] = 
{
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_readerAdapter_0(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_readerAdapterWithValidation_1(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_nameTable_2(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_schemaInfo_3(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_xmlCharType_4(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_systemId_5(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_publicId_6(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_normalize_7(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_validate_8(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_supportNamespaces_9(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_v1Compat_10(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_chars_11(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_charsUsed_12(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_curPos_13(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_scanningFunction_14(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_nextScaningFunction_15(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_savedScanningFunction_16(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_whitespaceSeen_17(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_tokenStartPos_18(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_colonPos_19(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_internalSubsetValueSb_20(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_externalEntitiesDepth_21(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_currentEntityId_22(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_freeFloatingDtd_23(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_hasFreeFloatingInternalSubset_24(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_stringBuilder_25(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_condSectionDepth_26(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_literalLineInfo_27(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_literalQuoteChar_28(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_documentBaseUri_29(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_externalDtdBaseUri_30(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_undeclaredNotations_31(),
	DtdParser_t9C63F125C98379A2AD3996648E8B7C234EA56574::get_offset_of_condSectionEntityIds_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize1799 = { sizeof (Token_t411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable1799[49] = 
{
	Token_t411C6CF24FE0CF019FDFD2EBAC48EC69C0C0D1D6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
