﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Newtonsoft.Json.JsonConverter
struct JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59;
// Newtonsoft.Json.JsonConverterCollection
struct JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF;
// Newtonsoft.Json.JsonConverter[]
struct JsonConverterU5BU5D_t24D9494837F678B153025E744B60968EEC7AC034;
// Newtonsoft.Json.JsonReader
struct JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC;
// Newtonsoft.Json.JsonSerializer
struct JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB;
// Newtonsoft.Json.JsonTextWriter
struct JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225;
// Newtonsoft.Json.JsonWriter
struct JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091;
// Newtonsoft.Json.JsonWriter/State[][]
struct StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84;
// Newtonsoft.Json.Linq.JContainer
struct JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48;
// Newtonsoft.Json.Linq.JObject
struct JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF;
// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5;
// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E;
// Newtonsoft.Json.Linq.JToken
struct JToken_t06FA337591EC253506CE2A342A01C19CA990982B;
// Newtonsoft.Json.Linq.JTokenType[]
struct JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5;
// Newtonsoft.Json.Linq.JValue
struct JValue_t80B940E650348637E29E2241958BC52097B1202F;
// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0
struct U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9;
// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81;
// Newtonsoft.Json.Serialization.ErrorContext
struct ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C;
// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853;
// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0;
// Newtonsoft.Json.Serialization.IAttributeProvider
struct IAttributeProvider_t608C6300D3F4E32A2842A880068A3B6BD6AFA989;
// Newtonsoft.Json.Serialization.IContractResolver
struct IContractResolver_tAA60D6EAFB1C28A616AE27F13A62595380353D8F;
// Newtonsoft.Json.Serialization.IReferenceResolver
struct IReferenceResolver_t2047669577EF6EFE700D5900AEEB4267BEE1861F;
// Newtonsoft.Json.Serialization.ITraceWriter
struct ITraceWriter_tB41E33E7400454A55DD6062387CF209637B08028;
// Newtonsoft.Json.Serialization.IValueProvider
struct IValueProvider_tBE9C0768654B6D3A4D071805EFEFBE6A1FA5FBA4;
// Newtonsoft.Json.Serialization.JsonContract
struct JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3;
// Newtonsoft.Json.Serialization.JsonISerializableContract
struct JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F;
// Newtonsoft.Json.Serialization.JsonProperty
struct JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97;
// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF;
// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA;
// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC;
// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA;
// Newtonsoft.Json.Serialization.NamingStrategy
struct NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19;
// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object>
struct ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8;
// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object>
struct BidirectionalDictionary_2_t2EF6D275E540A8487EA2791B4194212A09364980;
// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object>
struct MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC;
// Newtonsoft.Json.Utilities.PropertyNameTable
struct PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028;
// Newtonsoft.Json.Utilities.ReflectionObject
struct ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type>
struct ThreadSafeStore_2_t90990059095E89671C4EDDA28BFBF2587BA2E1DF;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject>
struct ThreadSafeStore_2_t2B6A0345C1C863C0D37840F03E46B1CBC010F70A;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],System.Object>>
struct ThreadSafeStore_2_t474A8779F8ED1AD7FE30E6F9B52AD2A86C205F86;
// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type>
struct ThreadSafeStore_2_tFD76CF8D918ECADB2E6AB0D19A3391ADDF0A9CA6;
// System.Action`2<System.Object,System.Object>
struct Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Attribute[]
struct AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract>
struct Dictionary_2_t72D4692AD177245078C9435277E31732CEFB637A;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken>
struct Dictionary_2_t58D04B117C44904BD16279EC6BD5F00D800073EE;
// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>
struct Dictionary_2_t605BBA472B4E5A8AEF3D0693F4328264B39AAE03;
// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType>
struct Dictionary_2_tFB7A438A8035214C0B4040917D6B5B4117234CF3;
// System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerable_1_t00C11E28587644E33CB7F154833508D63BFAC5B2;
// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken>
struct IEnumerator_1_t44D637C1BC95A751884B40EEAFB3AE78764AB49A;
// System.Collections.Generic.IEqualityComparer`1<System.String>
struct IEqualityComparer_1_t1F07EAC22CC1D4F279164B144240E4718BD7E7A9;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Linq.JToken>
struct IList_1_t226495C624AD9B4E3442E8FF7B1095E0943C0300;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.JsonProperty>
struct IList_1_tE1F49FAD7E10B24DCAEE5E942523E1293E226422;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct IList_1_t7A73F3AA4F879E183A72D428272454B25A1E22BD;
// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback>
struct IList_1_t6F718C95706D43059BC6EC70022801F365AD0F50;
// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode>
struct List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3;
// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition>
struct List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037;
// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken>
struct List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonProperty>
struct List_1_tBEA2EBC26E996C0F45B0EFBA2C5A1B39F19CDEB8;
// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback>
struct List_1_t21D9BA4C41DEFB88D15B6D260F571539789A245C;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IEqualityComparer
struct IEqualityComparer_t3102D0F5BABD60224F6DFF4815BCA1045831FB7C;
// System.ComponentModel.AttributeCollection
struct AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE;
// System.ComponentModel.ListChangedEventHandler
struct ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88;
// System.ComponentModel.PropertyChangedEventHandler
struct PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82;
// System.ComponentModel.PropertyChangingEventHandler
struct PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1;
// System.ComponentModel.TypeConverter
struct TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs>
struct EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A;
// System.Exception
struct Exception_t;
// System.Func`1<System.Object>
struct Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty>
struct Func_2_t5A6DCDFC261BED0BC7BA489A2D9242068FE16D2D;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct Func_2_tFA9126F6567451D625F0BE1EB1A8A5F299CF4A52;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32>
struct Func_2_tF2972C495FEE95E5A95380ED132AD325F61ED7C6;
// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String>
struct Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B;
// System.Func`2<System.Object,System.Object>
struct Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4;
// System.Func`2<System.Object,System.Type>
struct Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E;
// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean>
struct Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3;
// System.Func`2<System.Reflection.FieldInfo,System.Boolean>
struct Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D;
// System.Func`2<System.Reflection.MemberInfo,System.Boolean>
struct Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422;
// System.Func`2<System.Reflection.MemberInfo,System.String>
struct Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19;
// System.Func`2<System.Reflection.ParameterInfo,System.Type>
struct Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1;
// System.Func`2<System.String,System.String>
struct Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>>
struct Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.StringWriter
struct StringWriter_t194EF1526E072B93984370042AA80926C2EB6139;
// System.Object[]
struct ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A;
// System.Predicate`1<System.Object>
struct Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SerializationBinder
struct SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4;
// System.String
struct String_t;
// System.Type
struct Type_t;
// System.Type[]
struct TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.XmlDeclaration
struct XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlElement
struct XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef XMLNODEWRAPPER_TADE3A83C456747510EF6C4046E6689838DD070F8_H
#define XMLNODEWRAPPER_TADE3A83C456747510EF6C4046E6689838DD070F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlNodeWrapper
struct  XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8  : public RuntimeObject
{
public:
	// System.Xml.XmlNode Newtonsoft.Json.Converters.XmlNodeWrapper::_node
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ____node_0;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_childNodes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ____childNodes_1;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Converters.IXmlNode> Newtonsoft.Json.Converters.XmlNodeWrapper::_attributes
	List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * ____attributes_2;

public:
	inline static int32_t get_offset_of__node_0() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8, ____node_0)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get__node_0() const { return ____node_0; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of__node_0() { return &____node_0; }
	inline void set__node_0(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		____node_0 = value;
		Il2CppCodeGenWriteBarrier((&____node_0), value);
	}

	inline static int32_t get_offset_of__childNodes_1() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8, ____childNodes_1)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get__childNodes_1() const { return ____childNodes_1; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of__childNodes_1() { return &____childNodes_1; }
	inline void set__childNodes_1(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		____childNodes_1 = value;
		Il2CppCodeGenWriteBarrier((&____childNodes_1), value);
	}

	inline static int32_t get_offset_of__attributes_2() { return static_cast<int32_t>(offsetof(XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8, ____attributes_2)); }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * get__attributes_2() const { return ____attributes_2; }
	inline List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 ** get_address_of__attributes_2() { return &____attributes_2; }
	inline void set__attributes_2(List_1_t8958E3A3766FA1A9FDF616BFAF75F68C12870EE3 * value)
	{
		____attributes_2 = value;
		Il2CppCodeGenWriteBarrier((&____attributes_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNODEWRAPPER_TADE3A83C456747510EF6C4046E6689838DD070F8_H
#ifndef JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#define JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonConverter
struct  JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONVERTER_T1CC2EC16B3208095A97A3CAE15286A5E16C3DD59_H
#ifndef JPROPERTYLIST_T7AB9D3A952D43CB35002B72C07797F00C99810C5_H
#define JPROPERTYLIST_T7AB9D3A952D43CB35002B72C07797F00C99810C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty/JPropertyList
struct  JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty/JPropertyList::_token
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____token_0;

public:
	inline static int32_t get_offset_of__token_0() { return static_cast<int32_t>(offsetof(JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5, ____token_0)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__token_0() const { return ____token_0; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__token_0() { return &____token_0; }
	inline void set__token_0(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____token_0 = value;
		Il2CppCodeGenWriteBarrier((&____token_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYLIST_T7AB9D3A952D43CB35002B72C07797F00C99810C5_H
#ifndef U3CGETENUMERATORU3ED__1_TFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834_H
#define U3CGETENUMERATORU3ED__1_TFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1
struct  U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>2__current
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JProperty/JPropertyList Newtonsoft.Json.Linq.JProperty/JPropertyList/<GetEnumerator>d__1::<>4__this
	JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834, ___U3CU3E2__current_1)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834, ___U3CU3E4__this_2)); }
	inline JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__1_TFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834_H
#ifndef JTOKEN_T06FA337591EC253506CE2A342A01C19CA990982B_H
#define JTOKEN_T06FA337591EC253506CE2A342A01C19CA990982B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken
struct  JToken_t06FA337591EC253506CE2A342A01C19CA990982B  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JToken::_parent
	JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * ____parent_0;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_previous
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____previous_1;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JToken::_next
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____next_2;
	// System.Object Newtonsoft.Json.Linq.JToken::_annotations
	RuntimeObject * ____annotations_3;

public:
	inline static int32_t get_offset_of__parent_0() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B, ____parent_0)); }
	inline JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * get__parent_0() const { return ____parent_0; }
	inline JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 ** get_address_of__parent_0() { return &____parent_0; }
	inline void set__parent_0(JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * value)
	{
		____parent_0 = value;
		Il2CppCodeGenWriteBarrier((&____parent_0), value);
	}

	inline static int32_t get_offset_of__previous_1() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B, ____previous_1)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__previous_1() const { return ____previous_1; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__previous_1() { return &____previous_1; }
	inline void set__previous_1(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____previous_1 = value;
		Il2CppCodeGenWriteBarrier((&____previous_1), value);
	}

	inline static int32_t get_offset_of__next_2() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B, ____next_2)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__next_2() const { return ____next_2; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__next_2() { return &____next_2; }
	inline void set__next_2(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____next_2 = value;
		Il2CppCodeGenWriteBarrier((&____next_2), value);
	}

	inline static int32_t get_offset_of__annotations_3() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B, ____annotations_3)); }
	inline RuntimeObject * get__annotations_3() const { return ____annotations_3; }
	inline RuntimeObject ** get_address_of__annotations_3() { return &____annotations_3; }
	inline void set__annotations_3(RuntimeObject * value)
	{
		____annotations_3 = value;
		Il2CppCodeGenWriteBarrier((&____annotations_3), value);
	}
};

struct JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields
{
public:
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BooleanTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___BooleanTypes_4;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::NumberTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___NumberTypes_5;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::StringTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___StringTypes_6;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::GuidTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___GuidTypes_7;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::TimeSpanTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___TimeSpanTypes_8;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::UriTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___UriTypes_9;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::CharTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___CharTypes_10;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::DateTimeTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___DateTimeTypes_11;
	// Newtonsoft.Json.Linq.JTokenType[] Newtonsoft.Json.Linq.JToken::BytesTypes
	JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* ___BytesTypes_12;

public:
	inline static int32_t get_offset_of_BooleanTypes_4() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___BooleanTypes_4)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_BooleanTypes_4() const { return ___BooleanTypes_4; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_BooleanTypes_4() { return &___BooleanTypes_4; }
	inline void set_BooleanTypes_4(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___BooleanTypes_4 = value;
		Il2CppCodeGenWriteBarrier((&___BooleanTypes_4), value);
	}

	inline static int32_t get_offset_of_NumberTypes_5() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___NumberTypes_5)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_NumberTypes_5() const { return ___NumberTypes_5; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_NumberTypes_5() { return &___NumberTypes_5; }
	inline void set_NumberTypes_5(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___NumberTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___NumberTypes_5), value);
	}

	inline static int32_t get_offset_of_StringTypes_6() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___StringTypes_6)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_StringTypes_6() const { return ___StringTypes_6; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_StringTypes_6() { return &___StringTypes_6; }
	inline void set_StringTypes_6(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___StringTypes_6 = value;
		Il2CppCodeGenWriteBarrier((&___StringTypes_6), value);
	}

	inline static int32_t get_offset_of_GuidTypes_7() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___GuidTypes_7)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_GuidTypes_7() const { return ___GuidTypes_7; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_GuidTypes_7() { return &___GuidTypes_7; }
	inline void set_GuidTypes_7(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___GuidTypes_7 = value;
		Il2CppCodeGenWriteBarrier((&___GuidTypes_7), value);
	}

	inline static int32_t get_offset_of_TimeSpanTypes_8() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___TimeSpanTypes_8)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_TimeSpanTypes_8() const { return ___TimeSpanTypes_8; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_TimeSpanTypes_8() { return &___TimeSpanTypes_8; }
	inline void set_TimeSpanTypes_8(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___TimeSpanTypes_8 = value;
		Il2CppCodeGenWriteBarrier((&___TimeSpanTypes_8), value);
	}

	inline static int32_t get_offset_of_UriTypes_9() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___UriTypes_9)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_UriTypes_9() const { return ___UriTypes_9; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_UriTypes_9() { return &___UriTypes_9; }
	inline void set_UriTypes_9(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___UriTypes_9 = value;
		Il2CppCodeGenWriteBarrier((&___UriTypes_9), value);
	}

	inline static int32_t get_offset_of_CharTypes_10() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___CharTypes_10)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_CharTypes_10() const { return ___CharTypes_10; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_CharTypes_10() { return &___CharTypes_10; }
	inline void set_CharTypes_10(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___CharTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___CharTypes_10), value);
	}

	inline static int32_t get_offset_of_DateTimeTypes_11() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___DateTimeTypes_11)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_DateTimeTypes_11() const { return ___DateTimeTypes_11; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_DateTimeTypes_11() { return &___DateTimeTypes_11; }
	inline void set_DateTimeTypes_11(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___DateTimeTypes_11 = value;
		Il2CppCodeGenWriteBarrier((&___DateTimeTypes_11), value);
	}

	inline static int32_t get_offset_of_BytesTypes_12() { return static_cast<int32_t>(offsetof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields, ___BytesTypes_12)); }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* get_BytesTypes_12() const { return ___BytesTypes_12; }
	inline JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5** get_address_of_BytesTypes_12() { return &___BytesTypes_12; }
	inline void set_BytesTypes_12(JTokenTypeU5BU5D_t4F1FE72B8ADFD5B42D435B2D2D28D11C501E91D5* value)
	{
		___BytesTypes_12 = value;
		Il2CppCodeGenWriteBarrier((&___BytesTypes_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKEN_T06FA337591EC253506CE2A342A01C19CA990982B_H
#ifndef LINEINFOANNOTATION_T0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A_H
#define LINEINFOANNOTATION_T0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JToken/LineInfoAnnotation
struct  LineInfoAnnotation_t0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JToken/LineInfoAnnotation::LineNumber
	int32_t ___LineNumber_0;
	// System.Int32 Newtonsoft.Json.Linq.JToken/LineInfoAnnotation::LinePosition
	int32_t ___LinePosition_1;

public:
	inline static int32_t get_offset_of_LineNumber_0() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A, ___LineNumber_0)); }
	inline int32_t get_LineNumber_0() const { return ___LineNumber_0; }
	inline int32_t* get_address_of_LineNumber_0() { return &___LineNumber_0; }
	inline void set_LineNumber_0(int32_t value)
	{
		___LineNumber_0 = value;
	}

	inline static int32_t get_offset_of_LinePosition_1() { return static_cast<int32_t>(offsetof(LineInfoAnnotation_t0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A, ___LinePosition_1)); }
	inline int32_t get_LinePosition_1() const { return ___LinePosition_1; }
	inline int32_t* get_address_of_LinePosition_1() { return &___LinePosition_1; }
	inline void set_LinePosition_1(int32_t value)
	{
		___LinePosition_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOANNOTATION_T0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A_H
#ifndef U3CU3EC_T7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_H
#define U3CU3EC_T7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c
struct  U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__34_0
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__34_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__34_1
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__34_1_2;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MemberInfo>> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__37_0
	Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * ___U3CU3E9__37_0_3;
	// System.Func`2<System.Reflection.MemberInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__37_1
	Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * ___U3CU3E9__37_1_4;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__40_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__40_0_5;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.Int32> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c::<>9__64_0
	Func_2_tF2972C495FEE95E5A95380ED132AD325F61ED7C6 * ___U3CU3E9__64_0_6;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9__34_0_1)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__34_0_1() const { return ___U3CU3E9__34_0_1; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__34_0_1() { return &___U3CU3E9__34_0_1; }
	inline void set_U3CU3E9__34_0_1(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__34_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__34_1_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9__34_1_2)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__34_1_2() const { return ___U3CU3E9__34_1_2; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__34_1_2() { return &___U3CU3E9__34_1_2; }
	inline void set_U3CU3E9__34_1_2(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__34_1_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__34_1_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9__37_0_3)); }
	inline Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * get_U3CU3E9__37_0_3() const { return ___U3CU3E9__37_0_3; }
	inline Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A ** get_address_of_U3CU3E9__37_0_3() { return &___U3CU3E9__37_0_3; }
	inline void set_U3CU3E9__37_0_3(Func_2_t29F202ACCD37C598BCE6BF369E5B1493D173132A * value)
	{
		___U3CU3E9__37_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9__37_1_4)); }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * get_U3CU3E9__37_1_4() const { return ___U3CU3E9__37_1_4; }
	inline Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 ** get_address_of_U3CU3E9__37_1_4() { return &___U3CU3E9__37_1_4; }
	inline void set_U3CU3E9__37_1_4(Func_2_tAAE9C7C218FB19430A4E3DF0E3281ECED849B422 * value)
	{
		___U3CU3E9__37_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_1_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__40_0_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9__40_0_5)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__40_0_5() const { return ___U3CU3E9__40_0_5; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__40_0_5() { return &___U3CU3E9__40_0_5; }
	inline void set_U3CU3E9__40_0_5(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__40_0_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__40_0_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__64_0_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields, ___U3CU3E9__64_0_6)); }
	inline Func_2_tF2972C495FEE95E5A95380ED132AD325F61ED7C6 * get_U3CU3E9__64_0_6() const { return ___U3CU3E9__64_0_6; }
	inline Func_2_tF2972C495FEE95E5A95380ED132AD325F61ED7C6 ** get_address_of_U3CU3E9__64_0_6() { return &___U3CU3E9__64_0_6; }
	inline void set_U3CU3E9__64_0_6(Func_2_tF2972C495FEE95E5A95380ED132AD325F61ED7C6 * value)
	{
		___U3CU3E9__64_0_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__64_0_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T01A9E51479162C1C6A88684134FA0B00A389FAD9_H
#define U3CU3EC__DISPLAYCLASS38_0_T01A9E51479162C1C6A88684134FA0B00A389FAD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0::getExtensionDataDictionary
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___getExtensionDataDictionary_0;
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0::member
	MemberInfo_t * ___member_1;

public:
	inline static int32_t get_offset_of_getExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9, ___getExtensionDataDictionary_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_getExtensionDataDictionary_0() const { return ___getExtensionDataDictionary_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_getExtensionDataDictionary_0() { return &___getExtensionDataDictionary_0; }
	inline void set_getExtensionDataDictionary_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___getExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___getExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_member_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9, ___member_1)); }
	inline MemberInfo_t * get_member_1() const { return ___member_1; }
	inline MemberInfo_t ** get_address_of_member_1() { return &___member_1; }
	inline void set_member_1(MemberInfo_t * value)
	{
		___member_1 = value;
		Il2CppCodeGenWriteBarrier((&___member_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T01A9E51479162C1C6A88684134FA0B00A389FAD9_H
#ifndef U3CU3EC__DISPLAYCLASS38_1_T7DDC3209F5744DF0ADC3FFA92B42016008F3685E_H
#define U3CU3EC__DISPLAYCLASS38_1_T7DDC3209F5744DF0ADC3FFA92B42016008F3685E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1
struct  U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E  : public RuntimeObject
{
public:
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1::setExtensionDataDictionary
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___setExtensionDataDictionary_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1::createExtensionDataDictionary
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___createExtensionDataDictionary_1;
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1::setExtensionDataDictionaryValue
	MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * ___setExtensionDataDictionaryValue_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0 Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_1::CS$<>8__locals1
	U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 * ___CSU24U3CU3E8__locals1_3;

public:
	inline static int32_t get_offset_of_setExtensionDataDictionary_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E, ___setExtensionDataDictionary_0)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_setExtensionDataDictionary_0() const { return ___setExtensionDataDictionary_0; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_setExtensionDataDictionary_0() { return &___setExtensionDataDictionary_0; }
	inline void set_setExtensionDataDictionary_0(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___setExtensionDataDictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionary_0), value);
	}

	inline static int32_t get_offset_of_createExtensionDataDictionary_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E, ___createExtensionDataDictionary_1)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_createExtensionDataDictionary_1() const { return ___createExtensionDataDictionary_1; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_createExtensionDataDictionary_1() { return &___createExtensionDataDictionary_1; }
	inline void set_createExtensionDataDictionary_1(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___createExtensionDataDictionary_1 = value;
		Il2CppCodeGenWriteBarrier((&___createExtensionDataDictionary_1), value);
	}

	inline static int32_t get_offset_of_setExtensionDataDictionaryValue_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E, ___setExtensionDataDictionaryValue_2)); }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * get_setExtensionDataDictionaryValue_2() const { return ___setExtensionDataDictionaryValue_2; }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC ** get_address_of_setExtensionDataDictionaryValue_2() { return &___setExtensionDataDictionaryValue_2; }
	inline void set_setExtensionDataDictionaryValue_2(MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * value)
	{
		___setExtensionDataDictionaryValue_2 = value;
		Il2CppCodeGenWriteBarrier((&___setExtensionDataDictionaryValue_2), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals1_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E, ___CSU24U3CU3E8__locals1_3)); }
	inline U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 * get_CSU24U3CU3E8__locals1_3() const { return ___CSU24U3CU3E8__locals1_3; }
	inline U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 ** get_address_of_CSU24U3CU3E8__locals1_3() { return &___CSU24U3CU3E8__locals1_3; }
	inline void set_CSU24U3CU3E8__locals1_3(U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 * value)
	{
		___CSU24U3CU3E8__locals1_3 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_1_T7DDC3209F5744DF0ADC3FFA92B42016008F3685E_H
#ifndef U3CU3EC__DISPLAYCLASS38_2_T7820E34E56FECDC1033E4553C02296C3D57C9547_H
#define U3CU3EC__DISPLAYCLASS38_2_T7820E34E56FECDC1033E4553C02296C3D57C9547_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_2
struct  U3CU3Ec__DisplayClass38_2_t7820E34E56FECDC1033E4553C02296C3D57C9547  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_2::createEnumerableWrapper
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ___createEnumerableWrapper_0;
	// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_0 Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass38_2::CS$<>8__locals2
	U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 * ___CSU24U3CU3E8__locals2_1;

public:
	inline static int32_t get_offset_of_createEnumerableWrapper_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_2_t7820E34E56FECDC1033E4553C02296C3D57C9547, ___createEnumerableWrapper_0)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get_createEnumerableWrapper_0() const { return ___createEnumerableWrapper_0; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of_createEnumerableWrapper_0() { return &___createEnumerableWrapper_0; }
	inline void set_createEnumerableWrapper_0(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		___createEnumerableWrapper_0 = value;
		Il2CppCodeGenWriteBarrier((&___createEnumerableWrapper_0), value);
	}

	inline static int32_t get_offset_of_CSU24U3CU3E8__locals2_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_2_t7820E34E56FECDC1033E4553C02296C3D57C9547, ___CSU24U3CU3E8__locals2_1)); }
	inline U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 * get_CSU24U3CU3E8__locals2_1() const { return ___CSU24U3CU3E8__locals2_1; }
	inline U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 ** get_address_of_CSU24U3CU3E8__locals2_1() { return &___CSU24U3CU3E8__locals2_1; }
	inline void set_CSU24U3CU3E8__locals2_1(U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9 * value)
	{
		___CSU24U3CU3E8__locals2_1 = value;
		Il2CppCodeGenWriteBarrier((&___CSU24U3CU3E8__locals2_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_2_T7820E34E56FECDC1033E4553C02296C3D57C9547_H
#ifndef U3CU3EC__DISPLAYCLASS52_0_T913E9808AA5ECF6123EF1DB918E477E5E16B2967_H
#define U3CU3EC__DISPLAYCLASS52_0_T913E9808AA5ECF6123EF1DB918E477E5E16B2967_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass52_0
struct  U3CU3Ec__DisplayClass52_0_t913E9808AA5ECF6123EF1DB918E477E5E16B2967  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass52_0::namingStrategy
	NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * ___namingStrategy_0;

public:
	inline static int32_t get_offset_of_namingStrategy_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass52_0_t913E9808AA5ECF6123EF1DB918E477E5E16B2967, ___namingStrategy_0)); }
	inline NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * get_namingStrategy_0() const { return ___namingStrategy_0; }
	inline NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 ** get_address_of_namingStrategy_0() { return &___namingStrategy_0; }
	inline void set_namingStrategy_0(NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * value)
	{
		___namingStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&___namingStrategy_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS52_0_T913E9808AA5ECF6123EF1DB918E477E5E16B2967_H
#ifndef U3CU3EC__DISPLAYCLASS68_0_TB38B93210F3F0A3FC10E6453326887E9CEEFE994_H
#define U3CU3EC__DISPLAYCLASS68_0_TB38B93210F3F0A3FC10E6453326887E9CEEFE994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass68_0
struct  U3CU3Ec__DisplayClass68_0_tB38B93210F3F0A3FC10E6453326887E9CEEFE994  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Utilities.MethodCall`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass68_0::shouldSerializeCall
	MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * ___shouldSerializeCall_0;

public:
	inline static int32_t get_offset_of_shouldSerializeCall_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass68_0_tB38B93210F3F0A3FC10E6453326887E9CEEFE994, ___shouldSerializeCall_0)); }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * get_shouldSerializeCall_0() const { return ___shouldSerializeCall_0; }
	inline MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC ** get_address_of_shouldSerializeCall_0() { return &___shouldSerializeCall_0; }
	inline void set_shouldSerializeCall_0(MethodCall_2_tA55D4E95FDE26527159CF1C0B0E1546AB9FA9BBC * value)
	{
		___shouldSerializeCall_0 = value;
		Il2CppCodeGenWriteBarrier((&___shouldSerializeCall_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS68_0_TB38B93210F3F0A3FC10E6453326887E9CEEFE994_H
#ifndef U3CU3EC__DISPLAYCLASS69_0_T60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30_H
#define U3CU3EC__DISPLAYCLASS69_0_T60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass69_0
struct  U3CU3Ec__DisplayClass69_0_t60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30  : public RuntimeObject
{
public:
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DefaultContractResolver/<>c__DisplayClass69_0::specifiedPropertyGet
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ___specifiedPropertyGet_0;

public:
	inline static int32_t get_offset_of_specifiedPropertyGet_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass69_0_t60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30, ___specifiedPropertyGet_0)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get_specifiedPropertyGet_0() const { return ___specifiedPropertyGet_0; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of_specifiedPropertyGet_0() { return &___specifiedPropertyGet_0; }
	inline void set_specifiedPropertyGet_0(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		___specifiedPropertyGet_0 = value;
		Il2CppCodeGenWriteBarrier((&___specifiedPropertyGet_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS69_0_T60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30_H
#ifndef DEFAULTCONTRACTRESOLVERSTATE_TDCE9D5776838E8C023A19E0828F7D6D470556A81_H
#define DEFAULTCONTRACTRESOLVERSTATE_TDCE9D5776838E8C023A19E0828F7D6D470556A81_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolverState
struct  DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<Newtonsoft.Json.Serialization.ResolverContractKey,Newtonsoft.Json.Serialization.JsonContract> Newtonsoft.Json.Serialization.DefaultContractResolverState::ContractCache
	Dictionary_2_t72D4692AD177245078C9435277E31732CEFB637A * ___ContractCache_0;
	// Newtonsoft.Json.Utilities.PropertyNameTable Newtonsoft.Json.Serialization.DefaultContractResolverState::NameTable
	PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 * ___NameTable_1;

public:
	inline static int32_t get_offset_of_ContractCache_0() { return static_cast<int32_t>(offsetof(DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81, ___ContractCache_0)); }
	inline Dictionary_2_t72D4692AD177245078C9435277E31732CEFB637A * get_ContractCache_0() const { return ___ContractCache_0; }
	inline Dictionary_2_t72D4692AD177245078C9435277E31732CEFB637A ** get_address_of_ContractCache_0() { return &___ContractCache_0; }
	inline void set_ContractCache_0(Dictionary_2_t72D4692AD177245078C9435277E31732CEFB637A * value)
	{
		___ContractCache_0 = value;
		Il2CppCodeGenWriteBarrier((&___ContractCache_0), value);
	}

	inline static int32_t get_offset_of_NameTable_1() { return static_cast<int32_t>(offsetof(DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81, ___NameTable_1)); }
	inline PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 * get_NameTable_1() const { return ___NameTable_1; }
	inline PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 ** get_address_of_NameTable_1() { return &___NameTable_1; }
	inline void set_NameTable_1(PropertyNameTable_tD2DF6B5B8271568C7CFA78F401DC06370B77B028 * value)
	{
		___NameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___NameTable_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVERSTATE_TDCE9D5776838E8C023A19E0828F7D6D470556A81_H
#ifndef DEFAULTREFERENCERESOLVER_TF31AA9DCD47128442345DAD6F515A4175D50CC23_H
#define DEFAULTREFERENCERESOLVER_TF31AA9DCD47128442345DAD6F515A4175D50CC23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultReferenceResolver
struct  DefaultReferenceResolver_tF31AA9DCD47128442345DAD6F515A4175D50CC23  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.DefaultReferenceResolver::_referenceCount
	int32_t ____referenceCount_0;

public:
	inline static int32_t get_offset_of__referenceCount_0() { return static_cast<int32_t>(offsetof(DefaultReferenceResolver_tF31AA9DCD47128442345DAD6F515A4175D50CC23, ____referenceCount_0)); }
	inline int32_t get__referenceCount_0() const { return ____referenceCount_0; }
	inline int32_t* get_address_of__referenceCount_0() { return &____referenceCount_0; }
	inline void set__referenceCount_0(int32_t value)
	{
		____referenceCount_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTREFERENCERESOLVER_TF31AA9DCD47128442345DAD6F515A4175D50CC23_H
#ifndef DYNAMICVALUEPROVIDER_TC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6_H
#define DYNAMICVALUEPROVIDER_TC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DynamicValueProvider
struct  DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.DynamicValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;
	// System.Func`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DynamicValueProvider::_getter
	Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * ____getter_1;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.DynamicValueProvider::_setter
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ____setter_2;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}

	inline static int32_t get_offset_of__getter_1() { return static_cast<int32_t>(offsetof(DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6, ____getter_1)); }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * get__getter_1() const { return ____getter_1; }
	inline Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 ** get_address_of__getter_1() { return &____getter_1; }
	inline void set__getter_1(Func_2_tE9A60F007AC624EA27BF19DEF4242B7DA2F1C2A4 * value)
	{
		____getter_1 = value;
		Il2CppCodeGenWriteBarrier((&____getter_1), value);
	}

	inline static int32_t get_offset_of__setter_2() { return static_cast<int32_t>(offsetof(DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6, ____setter_2)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get__setter_2() const { return ____setter_2; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of__setter_2() { return &____setter_2; }
	inline void set__setter_2(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		____setter_2 = value;
		Il2CppCodeGenWriteBarrier((&____setter_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICVALUEPROVIDER_TC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6_H
#ifndef ERRORCONTEXT_T82EE08F672CC1074063B6C6AA200EFFA96E2BF8C_H
#define ERRORCONTEXT_T82EE08F672CC1074063B6C6AA200EFFA96E2BF8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ErrorContext
struct  ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.ErrorContext::<Traced>k__BackingField
	bool ___U3CTracedU3Ek__BackingField_0;
	// System.Exception Newtonsoft.Json.Serialization.ErrorContext::<Error>k__BackingField
	Exception_t * ___U3CErrorU3Ek__BackingField_1;
	// System.Object Newtonsoft.Json.Serialization.ErrorContext::<OriginalObject>k__BackingField
	RuntimeObject * ___U3COriginalObjectU3Ek__BackingField_2;
	// System.Object Newtonsoft.Json.Serialization.ErrorContext::<Member>k__BackingField
	RuntimeObject * ___U3CMemberU3Ek__BackingField_3;
	// System.String Newtonsoft.Json.Serialization.ErrorContext::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_4;
	// System.Boolean Newtonsoft.Json.Serialization.ErrorContext::<Handled>k__BackingField
	bool ___U3CHandledU3Ek__BackingField_5;

public:
	inline static int32_t get_offset_of_U3CTracedU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C, ___U3CTracedU3Ek__BackingField_0)); }
	inline bool get_U3CTracedU3Ek__BackingField_0() const { return ___U3CTracedU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CTracedU3Ek__BackingField_0() { return &___U3CTracedU3Ek__BackingField_0; }
	inline void set_U3CTracedU3Ek__BackingField_0(bool value)
	{
		___U3CTracedU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CErrorU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C, ___U3CErrorU3Ek__BackingField_1)); }
	inline Exception_t * get_U3CErrorU3Ek__BackingField_1() const { return ___U3CErrorU3Ek__BackingField_1; }
	inline Exception_t ** get_address_of_U3CErrorU3Ek__BackingField_1() { return &___U3CErrorU3Ek__BackingField_1; }
	inline void set_U3CErrorU3Ek__BackingField_1(Exception_t * value)
	{
		___U3CErrorU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3COriginalObjectU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C, ___U3COriginalObjectU3Ek__BackingField_2)); }
	inline RuntimeObject * get_U3COriginalObjectU3Ek__BackingField_2() const { return ___U3COriginalObjectU3Ek__BackingField_2; }
	inline RuntimeObject ** get_address_of_U3COriginalObjectU3Ek__BackingField_2() { return &___U3COriginalObjectU3Ek__BackingField_2; }
	inline void set_U3COriginalObjectU3Ek__BackingField_2(RuntimeObject * value)
	{
		___U3COriginalObjectU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3COriginalObjectU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CMemberU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C, ___U3CMemberU3Ek__BackingField_3)); }
	inline RuntimeObject * get_U3CMemberU3Ek__BackingField_3() const { return ___U3CMemberU3Ek__BackingField_3; }
	inline RuntimeObject ** get_address_of_U3CMemberU3Ek__BackingField_3() { return &___U3CMemberU3Ek__BackingField_3; }
	inline void set_U3CMemberU3Ek__BackingField_3(RuntimeObject * value)
	{
		___U3CMemberU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C, ___U3CPathU3Ek__BackingField_4)); }
	inline String_t* get_U3CPathU3Ek__BackingField_4() const { return ___U3CPathU3Ek__BackingField_4; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_4() { return &___U3CPathU3Ek__BackingField_4; }
	inline void set_U3CPathU3Ek__BackingField_4(String_t* value)
	{
		___U3CPathU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CHandledU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C, ___U3CHandledU3Ek__BackingField_5)); }
	inline bool get_U3CHandledU3Ek__BackingField_5() const { return ___U3CHandledU3Ek__BackingField_5; }
	inline bool* get_address_of_U3CHandledU3Ek__BackingField_5() { return &___U3CHandledU3Ek__BackingField_5; }
	inline void set_U3CHandledU3Ek__BackingField_5(bool value)
	{
		___U3CHandledU3Ek__BackingField_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERRORCONTEXT_T82EE08F672CC1074063B6C6AA200EFFA96E2BF8C_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F_H
#define U3CU3EC__DISPLAYCLASS73_0_T313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass73_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F_H
#ifndef U3CU3EC__DISPLAYCLASS74_0_T6A58FBB8C433D921DF0DEB2DD2F95857C8B21265_H
#define U3CU3EC__DISPLAYCLASS74_0_T6A58FBB8C433D921DF0DEB2DD2F95857C8B21265_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0
struct  U3CU3Ec__DisplayClass74_0_t6A58FBB8C433D921DF0DEB2DD2F95857C8B21265  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo Newtonsoft.Json.Serialization.JsonContract/<>c__DisplayClass74_0::callbackMethodInfo
	MethodInfo_t * ___callbackMethodInfo_0;

public:
	inline static int32_t get_offset_of_callbackMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass74_0_t6A58FBB8C433D921DF0DEB2DD2F95857C8B21265, ___callbackMethodInfo_0)); }
	inline MethodInfo_t * get_callbackMethodInfo_0() const { return ___callbackMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_callbackMethodInfo_0() { return &___callbackMethodInfo_0; }
	inline void set_callbackMethodInfo_0(MethodInfo_t * value)
	{
		___callbackMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___callbackMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS74_0_T6A58FBB8C433D921DF0DEB2DD2F95857C8B21265_H
#ifndef JSONFORMATTERCONVERTER_T7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE_H
#define JSONFORMATTERCONVERTER_T7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonFormatterConverter
struct  JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonFormatterConverter::_reader
	JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA * ____reader_0;
	// Newtonsoft.Json.Serialization.JsonISerializableContract Newtonsoft.Json.Serialization.JsonFormatterConverter::_contract
	JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F * ____contract_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonFormatterConverter::_member
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * ____member_2;

public:
	inline static int32_t get_offset_of__reader_0() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE, ____reader_0)); }
	inline JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA * get__reader_0() const { return ____reader_0; }
	inline JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA ** get_address_of__reader_0() { return &____reader_0; }
	inline void set__reader_0(JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA * value)
	{
		____reader_0 = value;
		Il2CppCodeGenWriteBarrier((&____reader_0), value);
	}

	inline static int32_t get_offset_of__contract_1() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE, ____contract_1)); }
	inline JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F * get__contract_1() const { return ____contract_1; }
	inline JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F ** get_address_of__contract_1() { return &____contract_1; }
	inline void set__contract_1(JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F * value)
	{
		____contract_1 = value;
		Il2CppCodeGenWriteBarrier((&____contract_1), value);
	}

	inline static int32_t get_offset_of__member_2() { return static_cast<int32_t>(offsetof(JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE, ____member_2)); }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * get__member_2() const { return ____member_2; }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 ** get_address_of__member_2() { return &____member_2; }
	inline void set__member_2(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * value)
	{
		____member_2 = value;
		Il2CppCodeGenWriteBarrier((&____member_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONFORMATTERCONVERTER_T7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE_H
#ifndef JSONSERIALIZERINTERNALBASE_T36137479698478C0B377AE13D3CE73961CB3B780_H
#define JSONSERIALIZERINTERNALBASE_T36137479698478C0B377AE13D3CE73961CB3B780_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase
struct  JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_currentErrorContext
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C * ____currentErrorContext_0;
	// Newtonsoft.Json.Utilities.BidirectionalDictionary`2<System.String,System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalBase::_mappings
	BidirectionalDictionary_2_t2EF6D275E540A8487EA2791B4194212A09364980 * ____mappings_1;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerInternalBase::Serializer
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB * ___Serializer_2;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.Serialization.JsonSerializerInternalBase::TraceWriter
	RuntimeObject* ___TraceWriter_3;
	// Newtonsoft.Json.Serialization.JsonSerializerProxy Newtonsoft.Json.Serialization.JsonSerializerInternalBase::InternalSerializer
	JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA * ___InternalSerializer_4;

public:
	inline static int32_t get_offset_of__currentErrorContext_0() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780, ____currentErrorContext_0)); }
	inline ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C * get__currentErrorContext_0() const { return ____currentErrorContext_0; }
	inline ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C ** get_address_of__currentErrorContext_0() { return &____currentErrorContext_0; }
	inline void set__currentErrorContext_0(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C * value)
	{
		____currentErrorContext_0 = value;
		Il2CppCodeGenWriteBarrier((&____currentErrorContext_0), value);
	}

	inline static int32_t get_offset_of__mappings_1() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780, ____mappings_1)); }
	inline BidirectionalDictionary_2_t2EF6D275E540A8487EA2791B4194212A09364980 * get__mappings_1() const { return ____mappings_1; }
	inline BidirectionalDictionary_2_t2EF6D275E540A8487EA2791B4194212A09364980 ** get_address_of__mappings_1() { return &____mappings_1; }
	inline void set__mappings_1(BidirectionalDictionary_2_t2EF6D275E540A8487EA2791B4194212A09364980 * value)
	{
		____mappings_1 = value;
		Il2CppCodeGenWriteBarrier((&____mappings_1), value);
	}

	inline static int32_t get_offset_of_Serializer_2() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780, ___Serializer_2)); }
	inline JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB * get_Serializer_2() const { return ___Serializer_2; }
	inline JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB ** get_address_of_Serializer_2() { return &___Serializer_2; }
	inline void set_Serializer_2(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB * value)
	{
		___Serializer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Serializer_2), value);
	}

	inline static int32_t get_offset_of_TraceWriter_3() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780, ___TraceWriter_3)); }
	inline RuntimeObject* get_TraceWriter_3() const { return ___TraceWriter_3; }
	inline RuntimeObject** get_address_of_TraceWriter_3() { return &___TraceWriter_3; }
	inline void set_TraceWriter_3(RuntimeObject* value)
	{
		___TraceWriter_3 = value;
		Il2CppCodeGenWriteBarrier((&___TraceWriter_3), value);
	}

	inline static int32_t get_offset_of_InternalSerializer_4() { return static_cast<int32_t>(offsetof(JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780, ___InternalSerializer_4)); }
	inline JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA * get_InternalSerializer_4() const { return ___InternalSerializer_4; }
	inline JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA ** get_address_of_InternalSerializer_4() { return &___InternalSerializer_4; }
	inline void set_InternalSerializer_4(JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA * value)
	{
		___InternalSerializer_4 = value;
		Il2CppCodeGenWriteBarrier((&___InternalSerializer_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALBASE_T36137479698478C0B377AE13D3CE73961CB3B780_H
#ifndef REFERENCEEQUALSEQUALITYCOMPARER_T0D48B8904CB037F121AB933D92ACF97CD1427318_H
#define REFERENCEEQUALSEQUALITYCOMPARER_T0D48B8904CB037F121AB933D92ACF97CD1427318_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalBase/ReferenceEqualsEqualityComparer
struct  ReferenceEqualsEqualityComparer_t0D48B8904CB037F121AB933D92ACF97CD1427318  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCEEQUALSEQUALITYCOMPARER_T0D48B8904CB037F121AB933D92ACF97CD1427318_H
#ifndef U3CU3EC_T89EBA95A043507BD466A45BD1891EC3074271877_H
#define U3CU3EC_T89EBA95A043507BD466A45BD1891EC3074271877_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c
struct  U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9
	U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877 * ___U3CU3E9_0;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__36_0
	Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B * ___U3CU3E9__36_0_1;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,System.String> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__36_2
	Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B * ___U3CU3E9__36_2_2;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__41_0
	Func_2_t5A6DCDFC261BED0BC7BA489A2D9242068FE16D2D * ___U3CU3E9__41_0_3;
	// System.Func`2<Newtonsoft.Json.Serialization.JsonProperty,Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c::<>9__41_1
	Func_2_tFA9126F6567451D625F0BE1EB1A8A5F299CF4A52 * ___U3CU3E9__41_1_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields, ___U3CU3E9__36_0_1)); }
	inline Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B * get_U3CU3E9__36_0_1() const { return ___U3CU3E9__36_0_1; }
	inline Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B ** get_address_of_U3CU3E9__36_0_1() { return &___U3CU3E9__36_0_1; }
	inline void set_U3CU3E9__36_0_1(Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B * value)
	{
		___U3CU3E9__36_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__36_2_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields, ___U3CU3E9__36_2_2)); }
	inline Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B * get_U3CU3E9__36_2_2() const { return ___U3CU3E9__36_2_2; }
	inline Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B ** get_address_of_U3CU3E9__36_2_2() { return &___U3CU3E9__36_2_2; }
	inline void set_U3CU3E9__36_2_2(Func_2_tCC35710F9121E8EA6C75E737F54561400EFCE10B * value)
	{
		___U3CU3E9__36_2_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__36_2_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields, ___U3CU3E9__41_0_3)); }
	inline Func_2_t5A6DCDFC261BED0BC7BA489A2D9242068FE16D2D * get_U3CU3E9__41_0_3() const { return ___U3CU3E9__41_0_3; }
	inline Func_2_t5A6DCDFC261BED0BC7BA489A2D9242068FE16D2D ** get_address_of_U3CU3E9__41_0_3() { return &___U3CU3E9__41_0_3; }
	inline void set_U3CU3E9__41_0_3(Func_2_t5A6DCDFC261BED0BC7BA489A2D9242068FE16D2D * value)
	{
		___U3CU3E9__41_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__41_1_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields, ___U3CU3E9__41_1_4)); }
	inline Func_2_tFA9126F6567451D625F0BE1EB1A8A5F299CF4A52 * get_U3CU3E9__41_1_4() const { return ___U3CU3E9__41_1_4; }
	inline Func_2_tFA9126F6567451D625F0BE1EB1A8A5F299CF4A52 ** get_address_of_U3CU3E9__41_1_4() { return &___U3CU3E9__41_1_4; }
	inline void set_U3CU3E9__41_1_4(Func_2_tFA9126F6567451D625F0BE1EB1A8A5F299CF4A52 * value)
	{
		___U3CU3E9__41_1_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__41_1_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T89EBA95A043507BD466A45BD1891EC3074271877_H
#ifndef U3CU3EC__DISPLAYCLASS36_0_T2486B96EBEB5BD6EE795D93545DE89A31AACDA4D_H
#define U3CU3EC__DISPLAYCLASS36_0_T2486B96EBEB5BD6EE795D93545DE89A31AACDA4D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0
struct  U3CU3Ec__DisplayClass36_0_t2486B96EBEB5BD6EE795D93545DE89A31AACDA4D  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/<>c__DisplayClass36_0::property
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * ___property_0;

public:
	inline static int32_t get_offset_of_property_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass36_0_t2486B96EBEB5BD6EE795D93545DE89A31AACDA4D, ___property_0)); }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * get_property_0() const { return ___property_0; }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 ** get_address_of_property_0() { return &___property_0; }
	inline void set_property_0(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * value)
	{
		___property_0 = value;
		Il2CppCodeGenWriteBarrier((&___property_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS36_0_T2486B96EBEB5BD6EE795D93545DE89A31AACDA4D_H
#ifndef U3CU3EC_T4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_H
#define U3CU3EC_T4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c
struct  U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::<>9
	U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5 * ___U3CU3E9_0;
	// System.Func`2<System.Object,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector/<>c::<>9__20_1
	Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * ___U3CU3E9__20_1_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__20_1_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_StaticFields, ___U3CU3E9__20_1_1)); }
	inline Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * get_U3CU3E9__20_1_1() const { return ___U3CU3E9__20_1_1; }
	inline Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E ** get_address_of_U3CU3E9__20_1_1() { return &___U3CU3E9__20_1_1; }
	inline void set_U3CU3E9__20_1_1(Func_2_tC75E5979808C24BA38FF6DFCAEB0CA1CF7C0993E * value)
	{
		___U3CU3E9__20_1_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__20_1_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_H
#ifndef U3CU3EC__DISPLAYCLASS20_0_T7E278D9E65E015D4EDB942083BCD1847E7B6AB19_H
#define U3CU3EC__DISPLAYCLASS20_0_T7E278D9E65E015D4EDB942083BCD1847E7B6AB19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass20_0
struct  U3CU3Ec__DisplayClass20_0_t7E278D9E65E015D4EDB942083BCD1847E7B6AB19  : public RuntimeObject
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass20_0::type
	Type_t * ___type_0;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonTypeReflector/<>c__DisplayClass20_0::defaultConstructor
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___defaultConstructor_1;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t7E278D9E65E015D4EDB942083BCD1847E7B6AB19, ___type_0)); }
	inline Type_t * get_type_0() const { return ___type_0; }
	inline Type_t ** get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(Type_t * value)
	{
		___type_0 = value;
		Il2CppCodeGenWriteBarrier((&___type_0), value);
	}

	inline static int32_t get_offset_of_defaultConstructor_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass20_0_t7E278D9E65E015D4EDB942083BCD1847E7B6AB19, ___defaultConstructor_1)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_defaultConstructor_1() const { return ___defaultConstructor_1; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_defaultConstructor_1() { return &___defaultConstructor_1; }
	inline void set_defaultConstructor_1(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___defaultConstructor_1 = value;
		Il2CppCodeGenWriteBarrier((&___defaultConstructor_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS20_0_T7E278D9E65E015D4EDB942083BCD1847E7B6AB19_H
#ifndef NAMINGSTRATEGY_TECACBFDC2C898D48D76511B4C22E07CB86713F19_H
#define NAMINGSTRATEGY_TECACBFDC2C898D48D76511B4C22E07CB86713F19_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.NamingStrategy
struct  NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::<ProcessDictionaryKeys>k__BackingField
	bool ___U3CProcessDictionaryKeysU3Ek__BackingField_0;
	// System.Boolean Newtonsoft.Json.Serialization.NamingStrategy::<OverrideSpecifiedNames>k__BackingField
	bool ___U3COverrideSpecifiedNamesU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19, ___U3CProcessDictionaryKeysU3Ek__BackingField_0)); }
	inline bool get_U3CProcessDictionaryKeysU3Ek__BackingField_0() const { return ___U3CProcessDictionaryKeysU3Ek__BackingField_0; }
	inline bool* get_address_of_U3CProcessDictionaryKeysU3Ek__BackingField_0() { return &___U3CProcessDictionaryKeysU3Ek__BackingField_0; }
	inline void set_U3CProcessDictionaryKeysU3Ek__BackingField_0(bool value)
	{
		___U3CProcessDictionaryKeysU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19, ___U3COverrideSpecifiedNamesU3Ek__BackingField_1)); }
	inline bool get_U3COverrideSpecifiedNamesU3Ek__BackingField_1() const { return ___U3COverrideSpecifiedNamesU3Ek__BackingField_1; }
	inline bool* get_address_of_U3COverrideSpecifiedNamesU3Ek__BackingField_1() { return &___U3COverrideSpecifiedNamesU3Ek__BackingField_1; }
	inline void set_U3COverrideSpecifiedNamesU3Ek__BackingField_1(bool value)
	{
		___U3COverrideSpecifiedNamesU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMINGSTRATEGY_TECACBFDC2C898D48D76511B4C22E07CB86713F19_H
#ifndef REFLECTIONATTRIBUTEPROVIDER_TC4C4111A99124372494B63DCC0D27B78329575F3_H
#define REFLECTIONATTRIBUTEPROVIDER_TC4C4111A99124372494B63DCC0D27B78329575F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionAttributeProvider
struct  ReflectionAttributeProvider_tC4C4111A99124372494B63DCC0D27B78329575F3  : public RuntimeObject
{
public:
	// System.Object Newtonsoft.Json.Serialization.ReflectionAttributeProvider::_attributeProvider
	RuntimeObject * ____attributeProvider_0;

public:
	inline static int32_t get_offset_of__attributeProvider_0() { return static_cast<int32_t>(offsetof(ReflectionAttributeProvider_tC4C4111A99124372494B63DCC0D27B78329575F3, ____attributeProvider_0)); }
	inline RuntimeObject * get__attributeProvider_0() const { return ____attributeProvider_0; }
	inline RuntimeObject ** get_address_of__attributeProvider_0() { return &____attributeProvider_0; }
	inline void set__attributeProvider_0(RuntimeObject * value)
	{
		____attributeProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____attributeProvider_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONATTRIBUTEPROVIDER_TC4C4111A99124372494B63DCC0D27B78329575F3_H
#ifndef REFLECTIONVALUEPROVIDER_TBA0226A7B27908047CD040913E5EE5EB0D613454_H
#define REFLECTIONVALUEPROVIDER_TBA0226A7B27908047CD040913E5EE5EB0D613454_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ReflectionValueProvider
struct  ReflectionValueProvider_tBA0226A7B27908047CD040913E5EE5EB0D613454  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo Newtonsoft.Json.Serialization.ReflectionValueProvider::_memberInfo
	MemberInfo_t * ____memberInfo_0;

public:
	inline static int32_t get_offset_of__memberInfo_0() { return static_cast<int32_t>(offsetof(ReflectionValueProvider_tBA0226A7B27908047CD040913E5EE5EB0D613454, ____memberInfo_0)); }
	inline MemberInfo_t * get__memberInfo_0() const { return ____memberInfo_0; }
	inline MemberInfo_t ** get_address_of__memberInfo_0() { return &____memberInfo_0; }
	inline void set__memberInfo_0(MemberInfo_t * value)
	{
		____memberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFLECTIONVALUEPROVIDER_TBA0226A7B27908047CD040913E5EE5EB0D613454_H
#ifndef U3CU3EC_T111CB085677BC83B5C93876C251A5017BC0B3E03_H
#define U3CU3EC_T111CB085677BC83B5C93876C251A5017BC0B3E03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils/<>c
struct  U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionUtils/<>c Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<>9
	U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ConstructorInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<>9__10_0
	Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * ___U3CU3E9__10_0_1;
	// System.Func`2<System.Reflection.MemberInfo,System.String> Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<>9__29_0
	Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * ___U3CU3E9__29_0_2;
	// System.Func`2<System.Reflection.ParameterInfo,System.Type> Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<>9__37_0
	Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * ___U3CU3E9__37_0_3;
	// System.Func`2<System.Reflection.FieldInfo,System.Boolean> Newtonsoft.Json.Utilities.ReflectionUtils/<>c::<>9__39_0
	Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * ___U3CU3E9__39_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_tE50BC1D78F3969472F5276AA4812403240D166E3 * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields, ___U3CU3E9__29_0_2)); }
	inline Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * get_U3CU3E9__29_0_2() const { return ___U3CU3E9__29_0_2; }
	inline Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 ** get_address_of_U3CU3E9__29_0_2() { return &___U3CU3E9__29_0_2; }
	inline void set_U3CU3E9__29_0_2(Func_2_t63044DF8D9DC0D80C055ED44A5C897E63EA40F19 * value)
	{
		___U3CU3E9__29_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__37_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields, ___U3CU3E9__37_0_3)); }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * get_U3CU3E9__37_0_3() const { return ___U3CU3E9__37_0_3; }
	inline Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 ** get_address_of_U3CU3E9__37_0_3() { return &___U3CU3E9__37_0_3; }
	inline void set_U3CU3E9__37_0_3(Func_2_t48A5921F25D621759871E768DB35DE5C54D511A1 * value)
	{
		___U3CU3E9__37_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__37_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__39_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields, ___U3CU3E9__39_0_4)); }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * get_U3CU3E9__39_0_4() const { return ___U3CU3E9__39_0_4; }
	inline Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D ** get_address_of_U3CU3E9__39_0_4() { return &___U3CU3E9__39_0_4; }
	inline void set_U3CU3E9__39_0_4(Func_2_t8B737A62DCD6BCDB37818A2DFF357973A9317E8D * value)
	{
		___U3CU3E9__39_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__39_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T111CB085677BC83B5C93876C251A5017BC0B3E03_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_T2FC85827C1F79DAA5523C47AA9ABF809B95B780E_H
#define U3CU3EC__DISPLAYCLASS42_0_T2FC85827C1F79DAA5523C47AA9ABF809B95B780E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_t2FC85827C1F79DAA5523C47AA9ABF809B95B780E  : public RuntimeObject
{
public:
	// System.Reflection.PropertyInfo Newtonsoft.Json.Utilities.ReflectionUtils/<>c__DisplayClass42_0::subTypeProperty
	PropertyInfo_t * ___subTypeProperty_0;

public:
	inline static int32_t get_offset_of_subTypeProperty_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_t2FC85827C1F79DAA5523C47AA9ABF809B95B780E, ___subTypeProperty_0)); }
	inline PropertyInfo_t * get_subTypeProperty_0() const { return ___subTypeProperty_0; }
	inline PropertyInfo_t ** get_address_of_subTypeProperty_0() { return &___subTypeProperty_0; }
	inline void set_subTypeProperty_0(PropertyInfo_t * value)
	{
		___subTypeProperty_0 = value;
		Il2CppCodeGenWriteBarrier((&___subTypeProperty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_T2FC85827C1F79DAA5523C47AA9ABF809B95B780E_H
#ifndef STRINGUTILS_T55F456118274076335BAD29C3915865F4E1A5A79_H
#define STRINGUTILS_T55F456118274076335BAD29C3915865F4E1A5A79_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.StringUtils
struct  StringUtils_t55F456118274076335BAD29C3915865F4E1A5A79  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGUTILS_T55F456118274076335BAD29C3915865F4E1A5A79_H
#ifndef TYPEEXTENSIONS_T18C41DFD462FE5D51523C9DC26F889FE0F8E3429_H
#define TYPEEXTENSIONS_T18C41DFD462FE5D51523C9DC26F889FE0F8E3429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.TypeExtensions
struct  TypeExtensions_t18C41DFD462FE5D51523C9DC26F889FE0F8E3429  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEEXTENSIONS_T18C41DFD462FE5D51523C9DC26F889FE0F8E3429_H
#ifndef VALIDATIONUTILS_T8D1A362D70752CDE5F929271E80723D846FECF10_H
#define VALIDATIONUTILS_T8D1A362D70752CDE5F929271E80723D846FECF10_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.ValidationUtils
struct  ValidationUtils_t8D1A362D70752CDE5F929271E80723D846FECF10  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONUTILS_T8D1A362D70752CDE5F929271E80723D846FECF10_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef COLLECTION_1_T2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE_H
#define COLLECTION_1_T2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Linq.JToken>
struct  Collection_1_t2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE_H
#ifndef COLLECTION_1_T36AB6F03B575A9908BD5B22EF630716ED380A5C8_H
#define COLLECTION_1_T36AB6F03B575A9908BD5B22EF630716ED380A5C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.Collection`1<Newtonsoft.Json.Serialization.JsonProperty>
struct  Collection_1_t36AB6F03B575A9908BD5B22EF630716ED380A5C8  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<T> System.Collections.ObjectModel.Collection`1::items
	RuntimeObject* ___items_0;
	// System.Object System.Collections.ObjectModel.Collection`1::_syncRoot
	RuntimeObject * ____syncRoot_1;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(Collection_1_t36AB6F03B575A9908BD5B22EF630716ED380A5C8, ___items_0)); }
	inline RuntimeObject* get_items_0() const { return ___items_0; }
	inline RuntimeObject** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(RuntimeObject* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}

	inline static int32_t get_offset_of__syncRoot_1() { return static_cast<int32_t>(offsetof(Collection_1_t36AB6F03B575A9908BD5B22EF630716ED380A5C8, ____syncRoot_1)); }
	inline RuntimeObject * get__syncRoot_1() const { return ____syncRoot_1; }
	inline RuntimeObject ** get_address_of__syncRoot_1() { return &____syncRoot_1; }
	inline void set__syncRoot_1(RuntimeObject * value)
	{
		____syncRoot_1 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTION_1_T36AB6F03B575A9908BD5B22EF630716ED380A5C8_H
#ifndef MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#define MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.MemberDescriptor
struct  MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8  : public RuntimeObject
{
public:
	// System.String System.ComponentModel.MemberDescriptor::name
	String_t* ___name_0;
	// System.String System.ComponentModel.MemberDescriptor::displayName
	String_t* ___displayName_1;
	// System.Int32 System.ComponentModel.MemberDescriptor::nameHash
	int32_t ___nameHash_2;
	// System.ComponentModel.AttributeCollection System.ComponentModel.MemberDescriptor::attributeCollection
	AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * ___attributeCollection_3;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::attributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___attributes_4;
	// System.Attribute[] System.ComponentModel.MemberDescriptor::originalAttributes
	AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* ___originalAttributes_5;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFiltered
	bool ___attributesFiltered_6;
	// System.Boolean System.ComponentModel.MemberDescriptor::attributesFilled
	bool ___attributesFilled_7;
	// System.Int32 System.ComponentModel.MemberDescriptor::metadataVersion
	int32_t ___metadataVersion_8;
	// System.String System.ComponentModel.MemberDescriptor::category
	String_t* ___category_9;
	// System.String System.ComponentModel.MemberDescriptor::description
	String_t* ___description_10;
	// System.Object System.ComponentModel.MemberDescriptor::lockCookie
	RuntimeObject * ___lockCookie_11;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___name_0)); }
	inline String_t* get_name_0() const { return ___name_0; }
	inline String_t** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(String_t* value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_displayName_1() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___displayName_1)); }
	inline String_t* get_displayName_1() const { return ___displayName_1; }
	inline String_t** get_address_of_displayName_1() { return &___displayName_1; }
	inline void set_displayName_1(String_t* value)
	{
		___displayName_1 = value;
		Il2CppCodeGenWriteBarrier((&___displayName_1), value);
	}

	inline static int32_t get_offset_of_nameHash_2() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___nameHash_2)); }
	inline int32_t get_nameHash_2() const { return ___nameHash_2; }
	inline int32_t* get_address_of_nameHash_2() { return &___nameHash_2; }
	inline void set_nameHash_2(int32_t value)
	{
		___nameHash_2 = value;
	}

	inline static int32_t get_offset_of_attributeCollection_3() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributeCollection_3)); }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * get_attributeCollection_3() const { return ___attributeCollection_3; }
	inline AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE ** get_address_of_attributeCollection_3() { return &___attributeCollection_3; }
	inline void set_attributeCollection_3(AttributeCollection_tBE6941BB802EDE34B7F986C14A7D7E3A4E135EBE * value)
	{
		___attributeCollection_3 = value;
		Il2CppCodeGenWriteBarrier((&___attributeCollection_3), value);
	}

	inline static int32_t get_offset_of_attributes_4() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributes_4)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_attributes_4() const { return ___attributes_4; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_attributes_4() { return &___attributes_4; }
	inline void set_attributes_4(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___attributes_4 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_4), value);
	}

	inline static int32_t get_offset_of_originalAttributes_5() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___originalAttributes_5)); }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* get_originalAttributes_5() const { return ___originalAttributes_5; }
	inline AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17** get_address_of_originalAttributes_5() { return &___originalAttributes_5; }
	inline void set_originalAttributes_5(AttributeU5BU5D_t777BEFAB7857CFA5F0EE6C3EB1F8F7FF61F00A17* value)
	{
		___originalAttributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___originalAttributes_5), value);
	}

	inline static int32_t get_offset_of_attributesFiltered_6() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFiltered_6)); }
	inline bool get_attributesFiltered_6() const { return ___attributesFiltered_6; }
	inline bool* get_address_of_attributesFiltered_6() { return &___attributesFiltered_6; }
	inline void set_attributesFiltered_6(bool value)
	{
		___attributesFiltered_6 = value;
	}

	inline static int32_t get_offset_of_attributesFilled_7() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___attributesFilled_7)); }
	inline bool get_attributesFilled_7() const { return ___attributesFilled_7; }
	inline bool* get_address_of_attributesFilled_7() { return &___attributesFilled_7; }
	inline void set_attributesFilled_7(bool value)
	{
		___attributesFilled_7 = value;
	}

	inline static int32_t get_offset_of_metadataVersion_8() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___metadataVersion_8)); }
	inline int32_t get_metadataVersion_8() const { return ___metadataVersion_8; }
	inline int32_t* get_address_of_metadataVersion_8() { return &___metadataVersion_8; }
	inline void set_metadataVersion_8(int32_t value)
	{
		___metadataVersion_8 = value;
	}

	inline static int32_t get_offset_of_category_9() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___category_9)); }
	inline String_t* get_category_9() const { return ___category_9; }
	inline String_t** get_address_of_category_9() { return &___category_9; }
	inline void set_category_9(String_t* value)
	{
		___category_9 = value;
		Il2CppCodeGenWriteBarrier((&___category_9), value);
	}

	inline static int32_t get_offset_of_description_10() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___description_10)); }
	inline String_t* get_description_10() const { return ___description_10; }
	inline String_t** get_address_of_description_10() { return &___description_10; }
	inline void set_description_10(String_t* value)
	{
		___description_10 = value;
		Il2CppCodeGenWriteBarrier((&___description_10), value);
	}

	inline static int32_t get_offset_of_lockCookie_11() { return static_cast<int32_t>(offsetof(MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8, ___lockCookie_11)); }
	inline RuntimeObject * get_lockCookie_11() const { return ___lockCookie_11; }
	inline RuntimeObject ** get_address_of_lockCookie_11() { return &___lockCookie_11; }
	inline void set_lockCookie_11(RuntimeObject * value)
	{
		___lockCookie_11 = value;
		Il2CppCodeGenWriteBarrier((&___lockCookie_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERDESCRIPTOR_T9540F11CAE19431295C2582699585264E053B8F8_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#define SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.SerializationBinder
struct  SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONBINDER_TB5EBAF328371FB7CF23E37F5984D8412762CFFA4_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BINARYCONVERTER_T66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14_H
#define BINARYCONVERTER_T66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BinaryConverter
struct  BinaryConverter_t66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.BinaryConverter::_reflectionObject
	ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(BinaryConverter_t66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14, ____reflectionObject_0)); }
	inline ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYCONVERTER_T66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14_H
#ifndef BSONOBJECTIDCONVERTER_TEB36B1ADB76417099EE3AF1AC3B9A4CA528B91FD_H
#define BSONOBJECTIDCONVERTER_TEB36B1ADB76417099EE3AF1AC3B9A4CA528B91FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.BsonObjectIdConverter
struct  BsonObjectIdConverter_tEB36B1ADB76417099EE3AF1AC3B9A4CA528B91FD  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BSONOBJECTIDCONVERTER_TEB36B1ADB76417099EE3AF1AC3B9A4CA528B91FD_H
#ifndef DATASETCONVERTER_TA28DA7A656D39DC52341B4F30194A7309A6F57C0_H
#define DATASETCONVERTER_TA28DA7A656D39DC52341B4F30194A7309A6F57C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.DataSetConverter
struct  DataSetConverter_tA28DA7A656D39DC52341B4F30194A7309A6F57C0  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATASETCONVERTER_TA28DA7A656D39DC52341B4F30194A7309A6F57C0_H
#ifndef DATATABLECONVERTER_TAD924E8CC72D9C291DFE9ED4F563067BF01E1AFF_H
#define DATATABLECONVERTER_TAD924E8CC72D9C291DFE9ED4F563067BF01E1AFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.DataTableConverter
struct  DataTableConverter_tAD924E8CC72D9C291DFE9ED4F563067BF01E1AFF  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATABLECONVERTER_TAD924E8CC72D9C291DFE9ED4F563067BF01E1AFF_H
#ifndef ENTITYKEYMEMBERCONVERTER_TD3A19946087110A3F6D3410645A86D8229D4573E_H
#define ENTITYKEYMEMBERCONVERTER_TD3A19946087110A3F6D3410645A86D8229D4573E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.EntityKeyMemberConverter
struct  EntityKeyMemberConverter_tD3A19946087110A3F6D3410645A86D8229D4573E  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:

public:
};

struct EntityKeyMemberConverter_tD3A19946087110A3F6D3410645A86D8229D4573E_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Converters.EntityKeyMemberConverter::_reflectionObject
	ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * ____reflectionObject_0;

public:
	inline static int32_t get_offset_of__reflectionObject_0() { return static_cast<int32_t>(offsetof(EntityKeyMemberConverter_tD3A19946087110A3F6D3410645A86D8229D4573E_StaticFields, ____reflectionObject_0)); }
	inline ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * get__reflectionObject_0() const { return ____reflectionObject_0; }
	inline ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 ** get_address_of__reflectionObject_0() { return &____reflectionObject_0; }
	inline void set__reflectionObject_0(ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * value)
	{
		____reflectionObject_0 = value;
		Il2CppCodeGenWriteBarrier((&____reflectionObject_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYKEYMEMBERCONVERTER_TD3A19946087110A3F6D3410645A86D8229D4573E_H
#ifndef KEYVALUEPAIRCONVERTER_TCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_H
#define KEYVALUEPAIRCONVERTER_TCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.KeyValuePairConverter
struct  KeyValuePairConverter_tCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:

public:
};

struct KeyValuePairConverter_tCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_StaticFields
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,Newtonsoft.Json.Utilities.ReflectionObject> Newtonsoft.Json.Converters.KeyValuePairConverter::ReflectionObjectPerType
	ThreadSafeStore_2_t2B6A0345C1C863C0D37840F03E46B1CBC010F70A * ___ReflectionObjectPerType_0;

public:
	inline static int32_t get_offset_of_ReflectionObjectPerType_0() { return static_cast<int32_t>(offsetof(KeyValuePairConverter_tCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_StaticFields, ___ReflectionObjectPerType_0)); }
	inline ThreadSafeStore_2_t2B6A0345C1C863C0D37840F03E46B1CBC010F70A * get_ReflectionObjectPerType_0() const { return ___ReflectionObjectPerType_0; }
	inline ThreadSafeStore_2_t2B6A0345C1C863C0D37840F03E46B1CBC010F70A ** get_address_of_ReflectionObjectPerType_0() { return &___ReflectionObjectPerType_0; }
	inline void set_ReflectionObjectPerType_0(ThreadSafeStore_2_t2B6A0345C1C863C0D37840F03E46B1CBC010F70A * value)
	{
		___ReflectionObjectPerType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ReflectionObjectPerType_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIRCONVERTER_TCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_H
#ifndef REGEXCONVERTER_T62129A36894B2F932287215AAE74CADEC3E22144_H
#define REGEXCONVERTER_T62129A36894B2F932287215AAE74CADEC3E22144_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.RegexConverter
struct  RegexConverter_t62129A36894B2F932287215AAE74CADEC3E22144  : public JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXCONVERTER_T62129A36894B2F932287215AAE74CADEC3E22144_H
#ifndef XMLDECLARATIONWRAPPER_TE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35_H
#define XMLDECLARATIONWRAPPER_TE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDeclarationWrapper
struct  XmlDeclarationWrapper_tE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35  : public XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8
{
public:
	// System.Xml.XmlDeclaration Newtonsoft.Json.Converters.XmlDeclarationWrapper::_declaration
	XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * ____declaration_3;

public:
	inline static int32_t get_offset_of__declaration_3() { return static_cast<int32_t>(offsetof(XmlDeclarationWrapper_tE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35, ____declaration_3)); }
	inline XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * get__declaration_3() const { return ____declaration_3; }
	inline XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 ** get_address_of__declaration_3() { return &____declaration_3; }
	inline void set__declaration_3(XmlDeclaration_tB67A8B98F543947C6EB990C44A212527B9B51200 * value)
	{
		____declaration_3 = value;
		Il2CppCodeGenWriteBarrier((&____declaration_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDECLARATIONWRAPPER_TE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35_H
#ifndef XMLDOCUMENTWRAPPER_T0AF9193D2327378170CC705168522CE7785FED2C_H
#define XMLDOCUMENTWRAPPER_T0AF9193D2327378170CC705168522CE7785FED2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlDocumentWrapper
struct  XmlDocumentWrapper_t0AF9193D2327378170CC705168522CE7785FED2C  : public XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8
{
public:
	// System.Xml.XmlDocument Newtonsoft.Json.Converters.XmlDocumentWrapper::_document
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ____document_3;

public:
	inline static int32_t get_offset_of__document_3() { return static_cast<int32_t>(offsetof(XmlDocumentWrapper_t0AF9193D2327378170CC705168522CE7785FED2C, ____document_3)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get__document_3() const { return ____document_3; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of__document_3() { return &____document_3; }
	inline void set__document_3(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		____document_3 = value;
		Il2CppCodeGenWriteBarrier((&____document_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLDOCUMENTWRAPPER_T0AF9193D2327378170CC705168522CE7785FED2C_H
#ifndef XMLELEMENTWRAPPER_T47B237D03964251821968EAF0F71DE66BFA62577_H
#define XMLELEMENTWRAPPER_T47B237D03964251821968EAF0F71DE66BFA62577_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Converters.XmlElementWrapper
struct  XmlElementWrapper_t47B237D03964251821968EAF0F71DE66BFA62577  : public XmlNodeWrapper_tADE3A83C456747510EF6C4046E6689838DD070F8
{
public:
	// System.Xml.XmlElement Newtonsoft.Json.Converters.XmlElementWrapper::_element
	XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * ____element_3;

public:
	inline static int32_t get_offset_of__element_3() { return static_cast<int32_t>(offsetof(XmlElementWrapper_t47B237D03964251821968EAF0F71DE66BFA62577, ____element_3)); }
	inline XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * get__element_3() const { return ____element_3; }
	inline XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC ** get_address_of__element_3() { return &____element_3; }
	inline void set__element_3(XmlElement_t05D8C7971DE016A354D86028E7FFD84CD9DDDFDC * value)
	{
		____element_3 = value;
		Il2CppCodeGenWriteBarrier((&____element_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLELEMENTWRAPPER_T47B237D03964251821968EAF0F71DE66BFA62577_H
#ifndef JCONTAINER_T2C8C23CEFF94834E9445F4970B6AB16831C84E48_H
#define JCONTAINER_T2C8C23CEFF94834E9445F4970B6AB16831C84E48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JContainer
struct  JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48  : public JToken_t06FA337591EC253506CE2A342A01C19CA990982B
{
public:
	// System.ComponentModel.ListChangedEventHandler Newtonsoft.Json.Linq.JContainer::_listChanged
	ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * ____listChanged_13;
	// System.Object Newtonsoft.Json.Linq.JContainer::_syncRoot
	RuntimeObject * ____syncRoot_14;
	// System.Boolean Newtonsoft.Json.Linq.JContainer::_busy
	bool ____busy_15;

public:
	inline static int32_t get_offset_of__listChanged_13() { return static_cast<int32_t>(offsetof(JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48, ____listChanged_13)); }
	inline ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * get__listChanged_13() const { return ____listChanged_13; }
	inline ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 ** get_address_of__listChanged_13() { return &____listChanged_13; }
	inline void set__listChanged_13(ListChangedEventHandler_t8081F1428D22013519901C16884C5ACE86A72A88 * value)
	{
		____listChanged_13 = value;
		Il2CppCodeGenWriteBarrier((&____listChanged_13), value);
	}

	inline static int32_t get_offset_of__syncRoot_14() { return static_cast<int32_t>(offsetof(JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48, ____syncRoot_14)); }
	inline RuntimeObject * get__syncRoot_14() const { return ____syncRoot_14; }
	inline RuntimeObject ** get_address_of__syncRoot_14() { return &____syncRoot_14; }
	inline void set__syncRoot_14(RuntimeObject * value)
	{
		____syncRoot_14 = value;
		Il2CppCodeGenWriteBarrier((&____syncRoot_14), value);
	}

	inline static int32_t get_offset_of__busy_15() { return static_cast<int32_t>(offsetof(JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48, ____busy_15)); }
	inline bool get__busy_15() const { return ____busy_15; }
	inline bool* get_address_of__busy_15() { return &____busy_15; }
	inline void set__busy_15(bool value)
	{
		____busy_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONTAINER_T2C8C23CEFF94834E9445F4970B6AB16831C84E48_H
#ifndef JPROPERTYKEYEDCOLLECTION_TA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_H
#define JPROPERTYKEYEDCOLLECTION_TA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyKeyedCollection
struct  JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E  : public Collection_1_t2C8F6168B8EEA2B5E620631BA0B718F71AE7CFFE
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JPropertyKeyedCollection::_dictionary
	Dictionary_2_t58D04B117C44904BD16279EC6BD5F00D800073EE * ____dictionary_3;

public:
	inline static int32_t get_offset_of__dictionary_3() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E, ____dictionary_3)); }
	inline Dictionary_2_t58D04B117C44904BD16279EC6BD5F00D800073EE * get__dictionary_3() const { return ____dictionary_3; }
	inline Dictionary_2_t58D04B117C44904BD16279EC6BD5F00D800073EE ** get_address_of__dictionary_3() { return &____dictionary_3; }
	inline void set__dictionary_3(Dictionary_2_t58D04B117C44904BD16279EC6BD5F00D800073EE * value)
	{
		____dictionary_3 = value;
		Il2CppCodeGenWriteBarrier((&____dictionary_3), value);
	}
};

struct JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_StaticFields
{
public:
	// System.Collections.Generic.IEqualityComparer`1<System.String> Newtonsoft.Json.Linq.JPropertyKeyedCollection::Comparer
	RuntimeObject* ___Comparer_2;

public:
	inline static int32_t get_offset_of_Comparer_2() { return static_cast<int32_t>(offsetof(JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_StaticFields, ___Comparer_2)); }
	inline RuntimeObject* get_Comparer_2() const { return ___Comparer_2; }
	inline RuntimeObject** get_address_of_Comparer_2() { return &___Comparer_2; }
	inline void set_Comparer_2(RuntimeObject* value)
	{
		___Comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___Comparer_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYKEYEDCOLLECTION_TA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_H
#ifndef DEFAULTSERIALIZATIONBINDER_TC19C5DC7F736EF693DF8424D7EF1A8071A46E956_H
#define DEFAULTSERIALIZATIONBINDER_TC19C5DC7F736EF693DF8424D7EF1A8071A46E956_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder
struct  DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956  : public SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4
{
public:
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey,System.Type> Newtonsoft.Json.Serialization.DefaultSerializationBinder::_typeCache
	ThreadSafeStore_2_t90990059095E89671C4EDDA28BFBF2587BA2E1DF * ____typeCache_1;

public:
	inline static int32_t get_offset_of__typeCache_1() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956, ____typeCache_1)); }
	inline ThreadSafeStore_2_t90990059095E89671C4EDDA28BFBF2587BA2E1DF * get__typeCache_1() const { return ____typeCache_1; }
	inline ThreadSafeStore_2_t90990059095E89671C4EDDA28BFBF2587BA2E1DF ** get_address_of__typeCache_1() { return &____typeCache_1; }
	inline void set__typeCache_1(ThreadSafeStore_2_t90990059095E89671C4EDDA28BFBF2587BA2E1DF * value)
	{
		____typeCache_1 = value;
		Il2CppCodeGenWriteBarrier((&____typeCache_1), value);
	}
};

struct DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.DefaultSerializationBinder Newtonsoft.Json.Serialization.DefaultSerializationBinder::Instance
	DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956 * ___Instance_0;

public:
	inline static int32_t get_offset_of_Instance_0() { return static_cast<int32_t>(offsetof(DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956_StaticFields, ___Instance_0)); }
	inline DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956 * get_Instance_0() const { return ___Instance_0; }
	inline DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956 ** get_address_of_Instance_0() { return &___Instance_0; }
	inline void set_Instance_0(DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956 * value)
	{
		___Instance_0 = value;
		Il2CppCodeGenWriteBarrier((&___Instance_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTSERIALIZATIONBINDER_TC19C5DC7F736EF693DF8424D7EF1A8071A46E956_H
#ifndef TYPENAMEKEY_T7C679BD7C266C37EA2B068F889AC5038DF5093F9_H
#define TYPENAMEKEY_T7C679BD7C266C37EA2B068F889AC5038DF5093F9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct  TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9 
{
public:
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::AssemblyName
	String_t* ___AssemblyName_0;
	// System.String Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey::TypeName
	String_t* ___TypeName_1;

public:
	inline static int32_t get_offset_of_AssemblyName_0() { return static_cast<int32_t>(offsetof(TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9, ___AssemblyName_0)); }
	inline String_t* get_AssemblyName_0() const { return ___AssemblyName_0; }
	inline String_t** get_address_of_AssemblyName_0() { return &___AssemblyName_0; }
	inline void set_AssemblyName_0(String_t* value)
	{
		___AssemblyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___AssemblyName_0), value);
	}

	inline static int32_t get_offset_of_TypeName_1() { return static_cast<int32_t>(offsetof(TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9, ___TypeName_1)); }
	inline String_t* get_TypeName_1() const { return ___TypeName_1; }
	inline String_t** get_address_of_TypeName_1() { return &___TypeName_1; }
	inline void set_TypeName_1(String_t* value)
	{
		___TypeName_1 = value;
		Il2CppCodeGenWriteBarrier((&___TypeName_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9_marshaled_pinvoke
{
	char* ___AssemblyName_0;
	char* ___TypeName_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.DefaultSerializationBinder/TypeNameKey
struct TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9_marshaled_com
{
	Il2CppChar* ___AssemblyName_0;
	Il2CppChar* ___TypeName_1;
};
#endif // TYPENAMEKEY_T7C679BD7C266C37EA2B068F889AC5038DF5093F9_H
#ifndef ERROREVENTARGS_T099CA39B0A9B3D06DE3F9C0599366AD502B61C05_H
#define ERROREVENTARGS_T099CA39B0A9B3D06DE3F9C0599366AD502B61C05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ErrorEventArgs
struct  ErrorEventArgs_t099CA39B0A9B3D06DE3F9C0599366AD502B61C05  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Object Newtonsoft.Json.Serialization.ErrorEventArgs::<CurrentObject>k__BackingField
	RuntimeObject * ___U3CCurrentObjectU3Ek__BackingField_1;
	// Newtonsoft.Json.Serialization.ErrorContext Newtonsoft.Json.Serialization.ErrorEventArgs::<ErrorContext>k__BackingField
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C * ___U3CErrorContextU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CCurrentObjectU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t099CA39B0A9B3D06DE3F9C0599366AD502B61C05, ___U3CCurrentObjectU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CCurrentObjectU3Ek__BackingField_1() const { return ___U3CCurrentObjectU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CCurrentObjectU3Ek__BackingField_1() { return &___U3CCurrentObjectU3Ek__BackingField_1; }
	inline void set_U3CCurrentObjectU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CCurrentObjectU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentObjectU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CErrorContextU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ErrorEventArgs_t099CA39B0A9B3D06DE3F9C0599366AD502B61C05, ___U3CErrorContextU3Ek__BackingField_2)); }
	inline ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C * get_U3CErrorContextU3Ek__BackingField_2() const { return ___U3CErrorContextU3Ek__BackingField_2; }
	inline ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C ** get_address_of_U3CErrorContextU3Ek__BackingField_2() { return &___U3CErrorContextU3Ek__BackingField_2; }
	inline void set_U3CErrorContextU3Ek__BackingField_2(ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C * value)
	{
		___U3CErrorContextU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CErrorContextU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ERROREVENTARGS_T099CA39B0A9B3D06DE3F9C0599366AD502B61C05_H
#ifndef JSONSERIALIZERINTERNALREADER_T6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA_H
#define JSONSERIALIZERINTERNALREADER_T6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader
struct  JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA  : public JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALREADER_T6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA_H
#ifndef JSONSERIALIZERINTERNALWRITER_TF75087E71BAA4AB66068B04C5F62A39E6601DDEC_H
#define JSONSERIALIZERINTERNALWRITER_TF75087E71BAA4AB66068B04C5F62A39E6601DDEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter
struct  JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC  : public JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootType
	Type_t * ____rootType_5;
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_rootLevel
	int32_t ____rootLevel_6;
	// System.Collections.Generic.List`1<System.Object> Newtonsoft.Json.Serialization.JsonSerializerInternalWriter::_serializeStack
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____serializeStack_7;

public:
	inline static int32_t get_offset_of__rootType_5() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC, ____rootType_5)); }
	inline Type_t * get__rootType_5() const { return ____rootType_5; }
	inline Type_t ** get_address_of__rootType_5() { return &____rootType_5; }
	inline void set__rootType_5(Type_t * value)
	{
		____rootType_5 = value;
		Il2CppCodeGenWriteBarrier((&____rootType_5), value);
	}

	inline static int32_t get_offset_of__rootLevel_6() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC, ____rootLevel_6)); }
	inline int32_t get__rootLevel_6() const { return ____rootLevel_6; }
	inline int32_t* get_address_of__rootLevel_6() { return &____rootLevel_6; }
	inline void set__rootLevel_6(int32_t value)
	{
		____rootLevel_6 = value;
	}

	inline static int32_t get_offset_of__serializeStack_7() { return static_cast<int32_t>(offsetof(JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC, ____serializeStack_7)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__serializeStack_7() const { return ____serializeStack_7; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__serializeStack_7() { return &____serializeStack_7; }
	inline void set__serializeStack_7(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____serializeStack_7 = value;
		Il2CppCodeGenWriteBarrier((&____serializeStack_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERINTERNALWRITER_TF75087E71BAA4AB66068B04C5F62A39E6601DDEC_H
#ifndef ONERRORATTRIBUTE_TC65EB9E3FBE80CEF59678F1801F9D6D52E126F8F_H
#define ONERRORATTRIBUTE_TC65EB9E3FBE80CEF59678F1801F9D6D52E126F8F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.OnErrorAttribute
struct  OnErrorAttribute_tC65EB9E3FBE80CEF59678F1801F9D6D52E126F8F  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONERRORATTRIBUTE_TC65EB9E3FBE80CEF59678F1801F9D6D52E126F8F_H
#ifndef RESOLVERCONTRACTKEY_T103816732D945CB015D10DBE1F538DF8CBD4CFAD_H
#define RESOLVERCONTRACTKEY_T103816732D945CB015D10DBE1F538DF8CBD4CFAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ResolverContractKey
struct  ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD 
{
public:
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_resolverType
	Type_t * ____resolverType_0;
	// System.Type Newtonsoft.Json.Serialization.ResolverContractKey::_contractType
	Type_t * ____contractType_1;

public:
	inline static int32_t get_offset_of__resolverType_0() { return static_cast<int32_t>(offsetof(ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD, ____resolverType_0)); }
	inline Type_t * get__resolverType_0() const { return ____resolverType_0; }
	inline Type_t ** get_address_of__resolverType_0() { return &____resolverType_0; }
	inline void set__resolverType_0(Type_t * value)
	{
		____resolverType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resolverType_0), value);
	}

	inline static int32_t get_offset_of__contractType_1() { return static_cast<int32_t>(offsetof(ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD, ____contractType_1)); }
	inline Type_t * get__contractType_1() const { return ____contractType_1; }
	inline Type_t ** get_address_of__contractType_1() { return &____contractType_1; }
	inline void set__contractType_1(Type_t * value)
	{
		____contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD_marshaled_pinvoke
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
// Native definition for COM marshalling of Newtonsoft.Json.Serialization.ResolverContractKey
struct ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD_marshaled_com
{
	Type_t * ____resolverType_0;
	Type_t * ____contractType_1;
};
#endif // RESOLVERCONTRACTKEY_T103816732D945CB015D10DBE1F538DF8CBD4CFAD_H
#ifndef KEYVALUEPAIR_2_T4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB_H
#define KEYVALUEPAIR_2_T4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken>
struct  KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB, ___value_1)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get_value_1() const { return ___value_1; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB_H
#ifndef KEYEDCOLLECTION_2_T666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C_H
#define KEYEDCOLLECTION_2_T666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.ObjectModel.KeyedCollection`2<System.String,Newtonsoft.Json.Serialization.JsonProperty>
struct  KeyedCollection_2_t666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C  : public Collection_1_t36AB6F03B575A9908BD5B22EF630716ED380A5C8
{
public:
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.ObjectModel.KeyedCollection`2::comparer
	RuntimeObject* ___comparer_2;
	// System.Collections.Generic.Dictionary`2<TKey,TItem> System.Collections.ObjectModel.KeyedCollection`2::dict
	Dictionary_2_t605BBA472B4E5A8AEF3D0693F4328264B39AAE03 * ___dict_3;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::keyCount
	int32_t ___keyCount_4;
	// System.Int32 System.Collections.ObjectModel.KeyedCollection`2::threshold
	int32_t ___threshold_5;

public:
	inline static int32_t get_offset_of_comparer_2() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C, ___comparer_2)); }
	inline RuntimeObject* get_comparer_2() const { return ___comparer_2; }
	inline RuntimeObject** get_address_of_comparer_2() { return &___comparer_2; }
	inline void set_comparer_2(RuntimeObject* value)
	{
		___comparer_2 = value;
		Il2CppCodeGenWriteBarrier((&___comparer_2), value);
	}

	inline static int32_t get_offset_of_dict_3() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C, ___dict_3)); }
	inline Dictionary_2_t605BBA472B4E5A8AEF3D0693F4328264B39AAE03 * get_dict_3() const { return ___dict_3; }
	inline Dictionary_2_t605BBA472B4E5A8AEF3D0693F4328264B39AAE03 ** get_address_of_dict_3() { return &___dict_3; }
	inline void set_dict_3(Dictionary_2_t605BBA472B4E5A8AEF3D0693F4328264B39AAE03 * value)
	{
		___dict_3 = value;
		Il2CppCodeGenWriteBarrier((&___dict_3), value);
	}

	inline static int32_t get_offset_of_keyCount_4() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C, ___keyCount_4)); }
	inline int32_t get_keyCount_4() const { return ___keyCount_4; }
	inline int32_t* get_address_of_keyCount_4() { return &___keyCount_4; }
	inline void set_keyCount_4(int32_t value)
	{
		___keyCount_4 = value;
	}

	inline static int32_t get_offset_of_threshold_5() { return static_cast<int32_t>(offsetof(KeyedCollection_2_t666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C, ___threshold_5)); }
	inline int32_t get_threshold_5() const { return ___threshold_5; }
	inline int32_t* get_address_of_threshold_5() { return &___threshold_5; }
	inline void set_threshold_5(int32_t value)
	{
		___threshold_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYEDCOLLECTION_2_T666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C_H
#ifndef PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#define PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ComponentModel.PropertyDescriptor
struct  PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D  : public MemberDescriptor_t9540F11CAE19431295C2582699585264E053B8F8
{
public:
	// System.ComponentModel.TypeConverter System.ComponentModel.PropertyDescriptor::converter
	TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * ___converter_12;
	// System.Collections.Hashtable System.ComponentModel.PropertyDescriptor::valueChangedHandlers
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___valueChangedHandlers_13;
	// System.Object[] System.ComponentModel.PropertyDescriptor::editors
	ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* ___editors_14;
	// System.Type[] System.ComponentModel.PropertyDescriptor::editorTypes
	TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* ___editorTypes_15;
	// System.Int32 System.ComponentModel.PropertyDescriptor::editorCount
	int32_t ___editorCount_16;

public:
	inline static int32_t get_offset_of_converter_12() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___converter_12)); }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * get_converter_12() const { return ___converter_12; }
	inline TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB ** get_address_of_converter_12() { return &___converter_12; }
	inline void set_converter_12(TypeConverter_t8306AE03734853B551DDF089C1F17836A7764DBB * value)
	{
		___converter_12 = value;
		Il2CppCodeGenWriteBarrier((&___converter_12), value);
	}

	inline static int32_t get_offset_of_valueChangedHandlers_13() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___valueChangedHandlers_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_valueChangedHandlers_13() const { return ___valueChangedHandlers_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_valueChangedHandlers_13() { return &___valueChangedHandlers_13; }
	inline void set_valueChangedHandlers_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___valueChangedHandlers_13 = value;
		Il2CppCodeGenWriteBarrier((&___valueChangedHandlers_13), value);
	}

	inline static int32_t get_offset_of_editors_14() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editors_14)); }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* get_editors_14() const { return ___editors_14; }
	inline ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A** get_address_of_editors_14() { return &___editors_14; }
	inline void set_editors_14(ObjectU5BU5D_t3C9242B5C88A48B2A5BD9FDA6CD0024E792AF08A* value)
	{
		___editors_14 = value;
		Il2CppCodeGenWriteBarrier((&___editors_14), value);
	}

	inline static int32_t get_offset_of_editorTypes_15() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorTypes_15)); }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* get_editorTypes_15() const { return ___editorTypes_15; }
	inline TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F** get_address_of_editorTypes_15() { return &___editorTypes_15; }
	inline void set_editorTypes_15(TypeU5BU5D_t7FE623A666B49176DE123306221193E888A12F5F* value)
	{
		___editorTypes_15 = value;
		Il2CppCodeGenWriteBarrier((&___editorTypes_15), value);
	}

	inline static int32_t get_offset_of_editorCount_16() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D, ___editorCount_16)); }
	inline int32_t get_editorCount_16() const { return ___editorCount_16; }
	inline int32_t* get_address_of_editorCount_16() { return &___editorCount_16; }
	inline void set_editorCount_16(int32_t value)
	{
		___editorCount_16 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#define CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ConstructorHandling
struct  ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371 
{
public:
	// System.Int32 Newtonsoft.Json.ConstructorHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ConstructorHandling_t24063773444D2C4382EB581D086B8F078E533371, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONSTRUCTORHANDLING_T24063773444D2C4382EB581D086B8F078E533371_H
#ifndef DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#define DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateFormatHandling
struct  DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424 
{
public:
	// System.Int32 Newtonsoft.Json.DateFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateFormatHandling_t0D54266C0C7CD81612959E7CB8174F6F8AA21424, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEFORMATHANDLING_T0D54266C0C7CD81612959E7CB8174F6F8AA21424_H
#ifndef DATEPARSEHANDLING_T0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C_H
#define DATEPARSEHANDLING_T0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateParseHandling
struct  DateParseHandling_t0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C 
{
public:
	// System.Int32 Newtonsoft.Json.DateParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateParseHandling_t0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATEPARSEHANDLING_T0428B8AFAEE2CBEF5C0E1D3CEE0F5F1123E1E22C_H
#ifndef DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#define DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DateTimeZoneHandling
struct  DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD 
{
public:
	// System.Int32 Newtonsoft.Json.DateTimeZoneHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DateTimeZoneHandling_t4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEZONEHANDLING_T4F5E89ACFB9E6C55DD8866641395AA90D0CA46CD_H
#ifndef DEFAULTVALUEHANDLING_T85DC44F1FBDD9D31309A45A0334A4518B5C08696_H
#define DEFAULTVALUEHANDLING_T85DC44F1FBDD9D31309A45A0334A4518B5C08696_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.DefaultValueHandling
struct  DefaultValueHandling_t85DC44F1FBDD9D31309A45A0334A4518B5C08696 
{
public:
	// System.Int32 Newtonsoft.Json.DefaultValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DefaultValueHandling_t85DC44F1FBDD9D31309A45A0334A4518B5C08696, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTVALUEHANDLING_T85DC44F1FBDD9D31309A45A0334A4518B5C08696_H
#ifndef FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#define FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatFormatHandling
struct  FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289 
{
public:
	// System.Int32 Newtonsoft.Json.FloatFormatHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatFormatHandling_tE78C0C14E4CFE64BDF71370419573A3ED8B68289, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATFORMATHANDLING_TE78C0C14E4CFE64BDF71370419573A3ED8B68289_H
#ifndef FLOATPARSEHANDLING_T4571C78B0F87871B93E071E07321989BEB7633B1_H
#define FLOATPARSEHANDLING_T4571C78B0F87871B93E071E07321989BEB7633B1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.FloatParseHandling
struct  FloatParseHandling_t4571C78B0F87871B93E071E07321989BEB7633B1 
{
public:
	// System.Int32 Newtonsoft.Json.FloatParseHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FloatParseHandling_t4571C78B0F87871B93E071E07321989BEB7633B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FLOATPARSEHANDLING_T4571C78B0F87871B93E071E07321989BEB7633B1_H
#ifndef FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#define FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Formatting
struct  Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1 
{
public:
	// System.Int32 Newtonsoft.Json.Formatting::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Formatting_tE2524DB9105FC27E1B956F5AF5F211981B22E3E1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTING_TE2524DB9105FC27E1B956F5AF5F211981B22E3E1_H
#ifndef JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#define JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonContainerType
struct  JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6 
{
public:
	// System.Int32 Newtonsoft.Json.JsonContainerType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContainerType_t1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERTYPE_T1CACB858DCE9C5E10CDD74ADB8FCB7746D4DC5A6_H
#ifndef STATE_TD13934C0FF1DA5189F8BD9DC33D77391AB371944_H
#define STATE_TD13934C0FF1DA5189F8BD9DC33D77391AB371944_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader/State
struct  State_tD13934C0FF1DA5189F8BD9DC33D77391AB371944 
{
public:
	// System.Int32 Newtonsoft.Json.JsonReader/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_tD13934C0FF1DA5189F8BD9DC33D77391AB371944, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_TD13934C0FF1DA5189F8BD9DC33D77391AB371944_H
#ifndef JSONTOKEN_T6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D_H
#define JSONTOKEN_T6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonToken
struct  JsonToken_t6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D 
{
public:
	// System.Int32 Newtonsoft.Json.JsonToken::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonToken_t6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTOKEN_T6ADDEFDDA044FF7A3AAFC9E7CBD0E568B418410D_H
#ifndef STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#define STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter/State
struct  State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F 
{
public:
	// System.Int32 Newtonsoft.Json.JsonWriter/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t1DE392B52BABD9BF376B03FE41F672F7113AE11F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATE_T1DE392B52BABD9BF376B03FE41F672F7113AE11F_H
#ifndef COMMENTHANDLING_T12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA_H
#define COMMENTHANDLING_T12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.CommentHandling
struct  CommentHandling_t12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.CommentHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(CommentHandling_t12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENTHANDLING_T12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA_H
#ifndef JARRAY_T152541B0C88ED7B2655C5CBCF49F2EF4A7C83148_H
#define JARRAY_T152541B0C88ED7B2655C5CBCF49F2EF4A7C83148_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JArray
struct  JArray_t152541B0C88ED7B2655C5CBCF49F2EF4A7C83148  : public JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JArray::_values
	List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 * ____values_16;

public:
	inline static int32_t get_offset_of__values_16() { return static_cast<int32_t>(offsetof(JArray_t152541B0C88ED7B2655C5CBCF49F2EF4A7C83148, ____values_16)); }
	inline List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 * get__values_16() const { return ____values_16; }
	inline List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 ** get_address_of__values_16() { return &____values_16; }
	inline void set__values_16(List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 * value)
	{
		____values_16 = value;
		Il2CppCodeGenWriteBarrier((&____values_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JARRAY_T152541B0C88ED7B2655C5CBCF49F2EF4A7C83148_H
#ifndef JCONSTRUCTOR_T1B38B11D6CA34745263AAD0466339AFA64E3E73A_H
#define JCONSTRUCTOR_T1B38B11D6CA34745263AAD0466339AFA64E3E73A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JConstructor
struct  JConstructor_t1B38B11D6CA34745263AAD0466339AFA64E3E73A  : public JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48
{
public:
	// System.String Newtonsoft.Json.Linq.JConstructor::_name
	String_t* ____name_16;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JConstructor::_values
	List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 * ____values_17;

public:
	inline static int32_t get_offset_of__name_16() { return static_cast<int32_t>(offsetof(JConstructor_t1B38B11D6CA34745263AAD0466339AFA64E3E73A, ____name_16)); }
	inline String_t* get__name_16() const { return ____name_16; }
	inline String_t** get_address_of__name_16() { return &____name_16; }
	inline void set__name_16(String_t* value)
	{
		____name_16 = value;
		Il2CppCodeGenWriteBarrier((&____name_16), value);
	}

	inline static int32_t get_offset_of__values_17() { return static_cast<int32_t>(offsetof(JConstructor_t1B38B11D6CA34745263AAD0466339AFA64E3E73A, ____values_17)); }
	inline List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 * get__values_17() const { return ____values_17; }
	inline List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 ** get_address_of__values_17() { return &____values_17; }
	inline void set__values_17(List_1_t694BD61559125B8BE4280E5B8FCB41C91F343807 * value)
	{
		____values_17 = value;
		Il2CppCodeGenWriteBarrier((&____values_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JCONSTRUCTOR_T1B38B11D6CA34745263AAD0466339AFA64E3E73A_H
#ifndef JOBJECT_TE2D45BD312B5E70DD799B932CEE02289BFC0ADAF_H
#define JOBJECT_TE2D45BD312B5E70DD799B932CEE02289BFC0ADAF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject
struct  JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF  : public JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48
{
public:
	// Newtonsoft.Json.Linq.JPropertyKeyedCollection Newtonsoft.Json.Linq.JObject::_properties
	JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E * ____properties_16;
	// System.ComponentModel.PropertyChangedEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanged
	PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * ___PropertyChanged_17;
	// System.ComponentModel.PropertyChangingEventHandler Newtonsoft.Json.Linq.JObject::PropertyChanging
	PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 * ___PropertyChanging_18;

public:
	inline static int32_t get_offset_of__properties_16() { return static_cast<int32_t>(offsetof(JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF, ____properties_16)); }
	inline JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E * get__properties_16() const { return ____properties_16; }
	inline JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E ** get_address_of__properties_16() { return &____properties_16; }
	inline void set__properties_16(JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E * value)
	{
		____properties_16 = value;
		Il2CppCodeGenWriteBarrier((&____properties_16), value);
	}

	inline static int32_t get_offset_of_PropertyChanged_17() { return static_cast<int32_t>(offsetof(JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF, ___PropertyChanged_17)); }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * get_PropertyChanged_17() const { return ___PropertyChanged_17; }
	inline PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 ** get_address_of_PropertyChanged_17() { return &___PropertyChanged_17; }
	inline void set_PropertyChanged_17(PropertyChangedEventHandler_t617E98E1876A8EB394D2B329340CE02D21FFFC82 * value)
	{
		___PropertyChanged_17 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanged_17), value);
	}

	inline static int32_t get_offset_of_PropertyChanging_18() { return static_cast<int32_t>(offsetof(JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF, ___PropertyChanging_18)); }
	inline PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 * get_PropertyChanging_18() const { return ___PropertyChanging_18; }
	inline PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 ** get_address_of_PropertyChanging_18() { return &___PropertyChanging_18; }
	inline void set_PropertyChanging_18(PropertyChangingEventHandler_tE2424019EC48E76381C3EB037326A821D4F833C1 * value)
	{
		___PropertyChanging_18 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyChanging_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JOBJECT_TE2D45BD312B5E70DD799B932CEE02289BFC0ADAF_H
#ifndef U3CGETENUMERATORU3ED__58_T5C0A4483C28FF34200A2EC87AB7B9E4524150EB1_H
#define U3CGETENUMERATORU3ED__58_T5C0A4483C28FF34200A2EC87AB7B9E4524150EB1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58
struct  U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1  : public RuntimeObject
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>2__current
	KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB  ___U3CU3E2__current_1;
	// Newtonsoft.Json.Linq.JObject Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>4__this
	JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF * ___U3CU3E4__this_2;
	// System.Collections.Generic.IEnumerator`1<Newtonsoft.Json.Linq.JToken> Newtonsoft.Json.Linq.JObject/<GetEnumerator>d__58::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t4CD6ADE089F5EC258B27F3CE650760AABD6C6CCB  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1, ___U3CU3E4__this_2)); }
	inline JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1, ___U3CU3E7__wrap1_3)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__58_T5C0A4483C28FF34200A2EC87AB7B9E4524150EB1_H
#ifndef JPROPERTY_TBC934D9DB69AF069F9684CDAA0FAC25D3804233B_H
#define JPROPERTY_TBC934D9DB69AF069F9684CDAA0FAC25D3804233B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JProperty
struct  JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B  : public JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48
{
public:
	// Newtonsoft.Json.Linq.JProperty/JPropertyList Newtonsoft.Json.Linq.JProperty::_content
	JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 * ____content_16;
	// System.String Newtonsoft.Json.Linq.JProperty::_name
	String_t* ____name_17;

public:
	inline static int32_t get_offset_of__content_16() { return static_cast<int32_t>(offsetof(JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B, ____content_16)); }
	inline JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 * get__content_16() const { return ____content_16; }
	inline JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 ** get_address_of__content_16() { return &____content_16; }
	inline void set__content_16(JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5 * value)
	{
		____content_16 = value;
		Il2CppCodeGenWriteBarrier((&____content_16), value);
	}

	inline static int32_t get_offset_of__name_17() { return static_cast<int32_t>(offsetof(JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B, ____name_17)); }
	inline String_t* get__name_17() const { return ____name_17; }
	inline String_t** get_address_of__name_17() { return &____name_17; }
	inline void set__name_17(String_t* value)
	{
		____name_17 = value;
		Il2CppCodeGenWriteBarrier((&____name_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTY_TBC934D9DB69AF069F9684CDAA0FAC25D3804233B_H
#ifndef JPROPERTYDESCRIPTOR_TC6034DAF3AD95E066BAB2785173E0CFE703EBA6C_H
#define JPROPERTYDESCRIPTOR_TC6034DAF3AD95E066BAB2785173E0CFE703EBA6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JPropertyDescriptor
struct  JPropertyDescriptor_tC6034DAF3AD95E066BAB2785173E0CFE703EBA6C  : public PropertyDescriptor_tBF646D9949C932A92EEBD17E2EB6AD07D18B7C9D
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JPROPERTYDESCRIPTOR_TC6034DAF3AD95E066BAB2785173E0CFE703EBA6C_H
#ifndef JTOKENTYPE_TAAC6386A425E71C81B95E3583EBB0C699628D42F_H
#define JTOKENTYPE_TAAC6386A425E71C81B95E3583EBB0C699628D42F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenType
struct  JTokenType_tAAC6386A425E71C81B95E3583EBB0C699628D42F 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.JTokenType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JTokenType_tAAC6386A425E71C81B95E3583EBB0C699628D42F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENTYPE_TAAC6386A425E71C81B95E3583EBB0C699628D42F_H
#ifndef LINEINFOHANDLING_T717AD31902D401C6EAD677A21E5C01B4AEA959A2_H
#define LINEINFOHANDLING_T717AD31902D401C6EAD677A21E5C01B4AEA959A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.LineInfoHandling
struct  LineInfoHandling_t717AD31902D401C6EAD677A21E5C01B4AEA959A2 
{
public:
	// System.Int32 Newtonsoft.Json.Linq.LineInfoHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LineInfoHandling_t717AD31902D401C6EAD677A21E5C01B4AEA959A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LINEINFOHANDLING_T717AD31902D401C6EAD677A21E5C01B4AEA959A2_H
#ifndef MEMBERSERIALIZATION_TDB2DD34DBC5EF12D4740B667F601358C7DA90212_H
#define MEMBERSERIALIZATION_TDB2DD34DBC5EF12D4740B667F601358C7DA90212_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MemberSerialization
struct  MemberSerialization_tDB2DD34DBC5EF12D4740B667F601358C7DA90212 
{
public:
	// System.Int32 Newtonsoft.Json.MemberSerialization::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MemberSerialization_tDB2DD34DBC5EF12D4740B667F601358C7DA90212, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERSERIALIZATION_TDB2DD34DBC5EF12D4740B667F601358C7DA90212_H
#ifndef METADATAPROPERTYHANDLING_TD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7_H
#define METADATAPROPERTYHANDLING_TD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MetadataPropertyHandling
struct  MetadataPropertyHandling_tD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7 
{
public:
	// System.Int32 Newtonsoft.Json.MetadataPropertyHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MetadataPropertyHandling_tD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METADATAPROPERTYHANDLING_TD6239BDB5CF9C10FA2CC900DE4BC30C9BD253AF7_H
#ifndef MISSINGMEMBERHANDLING_T1F9543EA590BC359A9E4684D9D12F6E1098E276D_H
#define MISSINGMEMBERHANDLING_T1F9543EA590BC359A9E4684D9D12F6E1098E276D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.MissingMemberHandling
struct  MissingMemberHandling_t1F9543EA590BC359A9E4684D9D12F6E1098E276D 
{
public:
	// System.Int32 Newtonsoft.Json.MissingMemberHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(MissingMemberHandling_t1F9543EA590BC359A9E4684D9D12F6E1098E276D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISSINGMEMBERHANDLING_T1F9543EA590BC359A9E4684D9D12F6E1098E276D_H
#ifndef NULLVALUEHANDLING_TD5E3B6B0BF979D9DA6A17E676FA5394A5493430D_H
#define NULLVALUEHANDLING_TD5E3B6B0BF979D9DA6A17E676FA5394A5493430D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.NullValueHandling
struct  NullValueHandling_tD5E3B6B0BF979D9DA6A17E676FA5394A5493430D 
{
public:
	// System.Int32 Newtonsoft.Json.NullValueHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NullValueHandling_tD5E3B6B0BF979D9DA6A17E676FA5394A5493430D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLVALUEHANDLING_TD5E3B6B0BF979D9DA6A17E676FA5394A5493430D_H
#ifndef OBJECTCREATIONHANDLING_TC02C49234F03D7C4A800245AA414325496355B2C_H
#define OBJECTCREATIONHANDLING_TC02C49234F03D7C4A800245AA414325496355B2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ObjectCreationHandling
struct  ObjectCreationHandling_tC02C49234F03D7C4A800245AA414325496355B2C 
{
public:
	// System.Int32 Newtonsoft.Json.ObjectCreationHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ObjectCreationHandling_tC02C49234F03D7C4A800245AA414325496355B2C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTCREATIONHANDLING_TC02C49234F03D7C4A800245AA414325496355B2C_H
#ifndef PRESERVEREFERENCESHANDLING_TBF8B928632AB094AB854A8C8060F4A0DE246BDE0_H
#define PRESERVEREFERENCESHANDLING_TBF8B928632AB094AB854A8C8060F4A0DE246BDE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.PreserveReferencesHandling
struct  PreserveReferencesHandling_tBF8B928632AB094AB854A8C8060F4A0DE246BDE0 
{
public:
	// System.Int32 Newtonsoft.Json.PreserveReferencesHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PreserveReferencesHandling_tBF8B928632AB094AB854A8C8060F4A0DE246BDE0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRESERVEREFERENCESHANDLING_TBF8B928632AB094AB854A8C8060F4A0DE246BDE0_H
#ifndef READTYPE_T0139C8C2354D6D4A51B10FEE12C9E9549C2C247A_H
#define READTYPE_T0139C8C2354D6D4A51B10FEE12C9E9549C2C247A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReadType
struct  ReadType_t0139C8C2354D6D4A51B10FEE12C9E9549C2C247A 
{
public:
	// System.Int32 Newtonsoft.Json.ReadType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReadType_t0139C8C2354D6D4A51B10FEE12C9E9549C2C247A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // READTYPE_T0139C8C2354D6D4A51B10FEE12C9E9549C2C247A_H
#ifndef REFERENCELOOPHANDLING_T1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672_H
#define REFERENCELOOPHANDLING_T1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.ReferenceLoopHandling
struct  ReferenceLoopHandling_t1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672 
{
public:
	// System.Int32 Newtonsoft.Json.ReferenceLoopHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ReferenceLoopHandling_t1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REFERENCELOOPHANDLING_T1F4E04DDA8FDB18AAA2B32B2E8FAC32ADA9AF672_H
#ifndef REQUIRED_TB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3_H
#define REQUIRED_TB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Required
struct  Required_tB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3 
{
public:
	// System.Int32 Newtonsoft.Json.Required::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Required_tB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REQUIRED_TB3FC6E6E91A682BD5E3D527FCE4979AFE2DD58B3_H
#ifndef JSONCONTRACTTYPE_TC348EADEB87C8F713622F349D659D75FF07D862F_H
#define JSONCONTRACTTYPE_TC348EADEB87C8F713622F349D659D75FF07D862F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContractType
struct  JsonContractType_tC348EADEB87C8F713622F349D659D75FF07D862F 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonContractType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(JsonContractType_tC348EADEB87C8F713622F349D659D75FF07D862F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACTTYPE_TC348EADEB87C8F713622F349D659D75FF07D862F_H
#ifndef JSONPROPERTYCOLLECTION_TC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF_H
#define JSONPROPERTYCOLLECTION_TC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPropertyCollection
struct  JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF  : public KeyedCollection_2_t666BCF70F59E0E1E305B8F7C8E22E8C0D0A8932C
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonPropertyCollection::_type
	Type_t * ____type_6;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.JsonProperty> Newtonsoft.Json.Serialization.JsonPropertyCollection::_list
	List_1_tBEA2EBC26E996C0F45B0EFBA2C5A1B39F19CDEB8 * ____list_7;

public:
	inline static int32_t get_offset_of__type_6() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF, ____type_6)); }
	inline Type_t * get__type_6() const { return ____type_6; }
	inline Type_t ** get_address_of__type_6() { return &____type_6; }
	inline void set__type_6(Type_t * value)
	{
		____type_6 = value;
		Il2CppCodeGenWriteBarrier((&____type_6), value);
	}

	inline static int32_t get_offset_of__list_7() { return static_cast<int32_t>(offsetof(JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF, ____list_7)); }
	inline List_1_tBEA2EBC26E996C0F45B0EFBA2C5A1B39F19CDEB8 * get__list_7() const { return ____list_7; }
	inline List_1_tBEA2EBC26E996C0F45B0EFBA2C5A1B39F19CDEB8 ** get_address_of__list_7() { return &____list_7; }
	inline void set__list_7(List_1_tBEA2EBC26E996C0F45B0EFBA2C5A1B39F19CDEB8 * value)
	{
		____list_7 = value;
		Il2CppCodeGenWriteBarrier((&____list_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTYCOLLECTION_TC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF_H
#ifndef PROPERTYPRESENCE_TE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218_H
#define PROPERTYPRESENCE_TE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence
struct  PropertyPresence_tE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218 
{
public:
	// System.Int32 Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyPresence_tE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYPRESENCE_TE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218_H
#ifndef JSONTYPEREFLECTOR_T04C9307055940940D034DD02E04818C3C7E47C0D_H
#define JSONTYPEREFLECTOR_T04C9307055940940D034DD02E04818C3C7E47C0D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonTypeReflector
struct  JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D  : public RuntimeObject
{
public:

public:
};

struct JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields
{
public:
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_dynamicCodeGeneration
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____dynamicCodeGeneration_0;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonTypeReflector::_fullyTrusted
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____fullyTrusted_1;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Func`2<System.Object[],System.Object>> Newtonsoft.Json.Serialization.JsonTypeReflector::CreatorCache
	ThreadSafeStore_2_t474A8779F8ED1AD7FE30E6F9B52AD2A86C205F86 * ___CreatorCache_2;
	// Newtonsoft.Json.Utilities.ThreadSafeStore`2<System.Type,System.Type> Newtonsoft.Json.Serialization.JsonTypeReflector::AssociatedMetadataTypesCache
	ThreadSafeStore_2_tFD76CF8D918ECADB2E6AB0D19A3391ADDF0A9CA6 * ___AssociatedMetadataTypesCache_3;
	// Newtonsoft.Json.Utilities.ReflectionObject Newtonsoft.Json.Serialization.JsonTypeReflector::_metadataTypeAttributeReflectionObject
	ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * ____metadataTypeAttributeReflectionObject_4;

public:
	inline static int32_t get_offset_of__dynamicCodeGeneration_0() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields, ____dynamicCodeGeneration_0)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__dynamicCodeGeneration_0() const { return ____dynamicCodeGeneration_0; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__dynamicCodeGeneration_0() { return &____dynamicCodeGeneration_0; }
	inline void set__dynamicCodeGeneration_0(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____dynamicCodeGeneration_0 = value;
	}

	inline static int32_t get_offset_of__fullyTrusted_1() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields, ____fullyTrusted_1)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__fullyTrusted_1() const { return ____fullyTrusted_1; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__fullyTrusted_1() { return &____fullyTrusted_1; }
	inline void set__fullyTrusted_1(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____fullyTrusted_1 = value;
	}

	inline static int32_t get_offset_of_CreatorCache_2() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields, ___CreatorCache_2)); }
	inline ThreadSafeStore_2_t474A8779F8ED1AD7FE30E6F9B52AD2A86C205F86 * get_CreatorCache_2() const { return ___CreatorCache_2; }
	inline ThreadSafeStore_2_t474A8779F8ED1AD7FE30E6F9B52AD2A86C205F86 ** get_address_of_CreatorCache_2() { return &___CreatorCache_2; }
	inline void set_CreatorCache_2(ThreadSafeStore_2_t474A8779F8ED1AD7FE30E6F9B52AD2A86C205F86 * value)
	{
		___CreatorCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___CreatorCache_2), value);
	}

	inline static int32_t get_offset_of_AssociatedMetadataTypesCache_3() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields, ___AssociatedMetadataTypesCache_3)); }
	inline ThreadSafeStore_2_tFD76CF8D918ECADB2E6AB0D19A3391ADDF0A9CA6 * get_AssociatedMetadataTypesCache_3() const { return ___AssociatedMetadataTypesCache_3; }
	inline ThreadSafeStore_2_tFD76CF8D918ECADB2E6AB0D19A3391ADDF0A9CA6 ** get_address_of_AssociatedMetadataTypesCache_3() { return &___AssociatedMetadataTypesCache_3; }
	inline void set_AssociatedMetadataTypesCache_3(ThreadSafeStore_2_tFD76CF8D918ECADB2E6AB0D19A3391ADDF0A9CA6 * value)
	{
		___AssociatedMetadataTypesCache_3 = value;
		Il2CppCodeGenWriteBarrier((&___AssociatedMetadataTypesCache_3), value);
	}

	inline static int32_t get_offset_of__metadataTypeAttributeReflectionObject_4() { return static_cast<int32_t>(offsetof(JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields, ____metadataTypeAttributeReflectionObject_4)); }
	inline ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * get__metadataTypeAttributeReflectionObject_4() const { return ____metadataTypeAttributeReflectionObject_4; }
	inline ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 ** get_address_of__metadataTypeAttributeReflectionObject_4() { return &____metadataTypeAttributeReflectionObject_4; }
	inline void set__metadataTypeAttributeReflectionObject_4(ReflectionObject_t986F1380251FCF1B016CA41A331CA95CD4606105 * value)
	{
		____metadataTypeAttributeReflectionObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____metadataTypeAttributeReflectionObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONTYPEREFLECTOR_T04C9307055940940D034DD02E04818C3C7E47C0D_H
#ifndef STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#define STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.StringEscapeHandling
struct  StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A 
{
public:
	// System.Int32 Newtonsoft.Json.StringEscapeHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StringEscapeHandling_t3E67D3B0A780B24A2D57DF8921B9438CC507029A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGESCAPEHANDLING_T3E67D3B0A780B24A2D57DF8921B9438CC507029A_H
#ifndef TYPENAMEHANDLING_T0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9_H
#define TYPENAMEHANDLING_T0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.TypeNameHandling
struct  TypeNameHandling_t0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9 
{
public:
	// System.Int32 Newtonsoft.Json.TypeNameHandling::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TypeNameHandling_t0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPENAMEHANDLING_T0DC2566A7FAFA823002ADAD5175DFF387F7D1ED9_H
#ifndef PRIMITIVETYPECODE_T895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4_H
#define PRIMITIVETYPECODE_T895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Utilities.PrimitiveTypeCode
struct  PrimitiveTypeCode_t895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4 
{
public:
	// System.Int32 Newtonsoft.Json.Utilities.PrimitiveTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PrimitiveTypeCode_t895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRIMITIVETYPECODE_T895AAD3E5F0FFA0FE45CE4B8A0F981F2815567C4_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#define BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Reflection.BindingFlags
struct  BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tE35C91D046E63A1B92BB9AB909FCF9DA84379ED0, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDINGFLAGS_TE35C91D046E63A1B92BB9AB909FCF9DA84379ED0_H
#ifndef FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#define FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle
struct  FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A 
{
public:
	// System.Int32 System.Runtime.Serialization.Formatters.FormatterAssemblyStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FormatterAssemblyStyle_tA1E8A300026362A0AE091830C5DBDEFCBCD5213A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORMATTERASSEMBLYSTYLE_TA1E8A300026362A0AE091830C5DBDEFCBCD5213A_H
#ifndef STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#define STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContextStates
struct  StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F 
{
public:
	// System.Int32 System.Runtime.Serialization.StreamingContextStates::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StreamingContextStates_t6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMINGCONTEXTSTATES_T6D16CD7BC584A66A29B702F5FD59DF62BB1BDD3F_H
#ifndef JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#define JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonPosition
struct  JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD 
{
public:
	// Newtonsoft.Json.JsonContainerType Newtonsoft.Json.JsonPosition::Type
	int32_t ___Type_1;
	// System.Int32 Newtonsoft.Json.JsonPosition::Position
	int32_t ___Position_2;
	// System.String Newtonsoft.Json.JsonPosition::PropertyName
	String_t* ___PropertyName_3;
	// System.Boolean Newtonsoft.Json.JsonPosition::HasIndex
	bool ___HasIndex_4;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Position_2() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___Position_2)); }
	inline int32_t get_Position_2() const { return ___Position_2; }
	inline int32_t* get_address_of_Position_2() { return &___Position_2; }
	inline void set_Position_2(int32_t value)
	{
		___Position_2 = value;
	}

	inline static int32_t get_offset_of_PropertyName_3() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___PropertyName_3)); }
	inline String_t* get_PropertyName_3() const { return ___PropertyName_3; }
	inline String_t** get_address_of_PropertyName_3() { return &___PropertyName_3; }
	inline void set_PropertyName_3(String_t* value)
	{
		___PropertyName_3 = value;
		Il2CppCodeGenWriteBarrier((&___PropertyName_3), value);
	}

	inline static int32_t get_offset_of_HasIndex_4() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD, ___HasIndex_4)); }
	inline bool get_HasIndex_4() const { return ___HasIndex_4; }
	inline bool* get_address_of_HasIndex_4() { return &___HasIndex_4; }
	inline void set_HasIndex_4(bool value)
	{
		___HasIndex_4 = value;
	}
};

struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields
{
public:
	// System.Char[] Newtonsoft.Json.JsonPosition::SpecialCharacters
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___SpecialCharacters_0;

public:
	inline static int32_t get_offset_of_SpecialCharacters_0() { return static_cast<int32_t>(offsetof(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_StaticFields, ___SpecialCharacters_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_SpecialCharacters_0() const { return ___SpecialCharacters_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_SpecialCharacters_0() { return &___SpecialCharacters_0; }
	inline void set_SpecialCharacters_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___SpecialCharacters_0 = value;
		Il2CppCodeGenWriteBarrier((&___SpecialCharacters_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_pinvoke
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	char* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
// Native definition for COM marshalling of Newtonsoft.Json.JsonPosition
struct JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD_marshaled_com
{
	int32_t ___Type_1;
	int32_t ___Position_2;
	Il2CppChar* ___PropertyName_3;
	int32_t ___HasIndex_4;
};
#endif // JSONPOSITION_T7C381FA9E9591B7782B67363333C5CFA331E8EAD_H
#ifndef JVALUE_T80B940E650348637E29E2241958BC52097B1202F_H
#define JVALUE_T80B940E650348637E29E2241958BC52097B1202F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JValue
struct  JValue_t80B940E650348637E29E2241958BC52097B1202F  : public JToken_t06FA337591EC253506CE2A342A01C19CA990982B
{
public:
	// Newtonsoft.Json.Linq.JTokenType Newtonsoft.Json.Linq.JValue::_valueType
	int32_t ____valueType_13;
	// System.Object Newtonsoft.Json.Linq.JValue::_value
	RuntimeObject * ____value_14;

public:
	inline static int32_t get_offset_of__valueType_13() { return static_cast<int32_t>(offsetof(JValue_t80B940E650348637E29E2241958BC52097B1202F, ____valueType_13)); }
	inline int32_t get__valueType_13() const { return ____valueType_13; }
	inline int32_t* get_address_of__valueType_13() { return &____valueType_13; }
	inline void set__valueType_13(int32_t value)
	{
		____valueType_13 = value;
	}

	inline static int32_t get_offset_of__value_14() { return static_cast<int32_t>(offsetof(JValue_t80B940E650348637E29E2241958BC52097B1202F, ____value_14)); }
	inline RuntimeObject * get__value_14() const { return ____value_14; }
	inline RuntimeObject ** get_address_of__value_14() { return &____value_14; }
	inline void set__value_14(RuntimeObject * value)
	{
		____value_14 = value;
		Il2CppCodeGenWriteBarrier((&____value_14), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JVALUE_T80B940E650348637E29E2241958BC52097B1202F_H
#ifndef JSONLOADSETTINGS_TA3C6C90ED0E51EED87B727EDB9AAC5529791BE48_H
#define JSONLOADSETTINGS_TA3C6C90ED0E51EED87B727EDB9AAC5529791BE48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JsonLoadSettings
struct  JsonLoadSettings_tA3C6C90ED0E51EED87B727EDB9AAC5529791BE48  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Linq.CommentHandling Newtonsoft.Json.Linq.JsonLoadSettings::_commentHandling
	int32_t ____commentHandling_0;
	// Newtonsoft.Json.Linq.LineInfoHandling Newtonsoft.Json.Linq.JsonLoadSettings::_lineInfoHandling
	int32_t ____lineInfoHandling_1;

public:
	inline static int32_t get_offset_of__commentHandling_0() { return static_cast<int32_t>(offsetof(JsonLoadSettings_tA3C6C90ED0E51EED87B727EDB9AAC5529791BE48, ____commentHandling_0)); }
	inline int32_t get__commentHandling_0() const { return ____commentHandling_0; }
	inline int32_t* get_address_of__commentHandling_0() { return &____commentHandling_0; }
	inline void set__commentHandling_0(int32_t value)
	{
		____commentHandling_0 = value;
	}

	inline static int32_t get_offset_of__lineInfoHandling_1() { return static_cast<int32_t>(offsetof(JsonLoadSettings_tA3C6C90ED0E51EED87B727EDB9AAC5529791BE48, ____lineInfoHandling_1)); }
	inline int32_t get__lineInfoHandling_1() const { return ____lineInfoHandling_1; }
	inline int32_t* get_address_of__lineInfoHandling_1() { return &____lineInfoHandling_1; }
	inline void set__lineInfoHandling_1(int32_t value)
	{
		____lineInfoHandling_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLOADSETTINGS_TA3C6C90ED0E51EED87B727EDB9AAC5529791BE48_H
#ifndef DEFAULTCONTRACTRESOLVER_TFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_H
#define DEFAULTCONTRACTRESOLVER_TFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.DefaultContractResolver
struct  DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0  : public RuntimeObject
{
public:
	// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::_instanceState
	DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 * ____instanceState_4;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::_sharedCache
	bool ____sharedCache_5;
	// System.Reflection.BindingFlags Newtonsoft.Json.Serialization.DefaultContractResolver::<DefaultMembersSearchFlags>k__BackingField
	int32_t ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<SerializeCompilerGeneratedMembers>k__BackingField
	bool ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableInterface>k__BackingField
	bool ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8;
	// System.Boolean Newtonsoft.Json.Serialization.DefaultContractResolver::<IgnoreSerializableAttribute>k__BackingField
	bool ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9;
	// Newtonsoft.Json.Serialization.NamingStrategy Newtonsoft.Json.Serialization.DefaultContractResolver::<NamingStrategy>k__BackingField
	NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * ___U3CNamingStrategyU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of__instanceState_4() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ____instanceState_4)); }
	inline DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 * get__instanceState_4() const { return ____instanceState_4; }
	inline DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 ** get_address_of__instanceState_4() { return &____instanceState_4; }
	inline void set__instanceState_4(DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 * value)
	{
		____instanceState_4 = value;
		Il2CppCodeGenWriteBarrier((&____instanceState_4), value);
	}

	inline static int32_t get_offset_of__sharedCache_5() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ____sharedCache_5)); }
	inline bool get__sharedCache_5() const { return ____sharedCache_5; }
	inline bool* get_address_of__sharedCache_5() { return &____sharedCache_5; }
	inline void set__sharedCache_5(bool value)
	{
		____sharedCache_5 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6)); }
	inline int32_t get_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() const { return ___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6() { return &___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6; }
	inline void set_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(int32_t value)
	{
		___U3CDefaultMembersSearchFlagsU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7)); }
	inline bool get_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() const { return ___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7() { return &___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7; }
	inline void set_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(bool value)
	{
		___U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8)); }
	inline bool get_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() const { return ___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8() { return &___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8; }
	inline void set_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(bool value)
	{
		___U3CIgnoreSerializableInterfaceU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9)); }
	inline bool get_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() const { return ___U3CIgnoreSerializableAttributeU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9() { return &___U3CIgnoreSerializableAttributeU3Ek__BackingField_9; }
	inline void set_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(bool value)
	{
		___U3CIgnoreSerializableAttributeU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CNamingStrategyU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0, ___U3CNamingStrategyU3Ek__BackingField_10)); }
	inline NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * get_U3CNamingStrategyU3Ek__BackingField_10() const { return ___U3CNamingStrategyU3Ek__BackingField_10; }
	inline NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 ** get_address_of_U3CNamingStrategyU3Ek__BackingField_10() { return &___U3CNamingStrategyU3Ek__BackingField_10; }
	inline void set_U3CNamingStrategyU3Ek__BackingField_10(NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19 * value)
	{
		___U3CNamingStrategyU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamingStrategyU3Ek__BackingField_10), value);
	}
};

struct DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields
{
public:
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.Serialization.DefaultContractResolver::_instance
	RuntimeObject* ____instance_0;
	// Newtonsoft.Json.JsonConverter[] Newtonsoft.Json.Serialization.DefaultContractResolver::BuiltInConverters
	JsonConverterU5BU5D_t24D9494837F678B153025E744B60968EEC7AC034* ___BuiltInConverters_1;
	// System.Object Newtonsoft.Json.Serialization.DefaultContractResolver::TypeContractCacheLock
	RuntimeObject * ___TypeContractCacheLock_2;
	// Newtonsoft.Json.Serialization.DefaultContractResolverState Newtonsoft.Json.Serialization.DefaultContractResolver::_sharedState
	DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 * ____sharedState_3;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields, ____instance_0)); }
	inline RuntimeObject* get__instance_0() const { return ____instance_0; }
	inline RuntimeObject** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject* value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of_BuiltInConverters_1() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields, ___BuiltInConverters_1)); }
	inline JsonConverterU5BU5D_t24D9494837F678B153025E744B60968EEC7AC034* get_BuiltInConverters_1() const { return ___BuiltInConverters_1; }
	inline JsonConverterU5BU5D_t24D9494837F678B153025E744B60968EEC7AC034** get_address_of_BuiltInConverters_1() { return &___BuiltInConverters_1; }
	inline void set_BuiltInConverters_1(JsonConverterU5BU5D_t24D9494837F678B153025E744B60968EEC7AC034* value)
	{
		___BuiltInConverters_1 = value;
		Il2CppCodeGenWriteBarrier((&___BuiltInConverters_1), value);
	}

	inline static int32_t get_offset_of_TypeContractCacheLock_2() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields, ___TypeContractCacheLock_2)); }
	inline RuntimeObject * get_TypeContractCacheLock_2() const { return ___TypeContractCacheLock_2; }
	inline RuntimeObject ** get_address_of_TypeContractCacheLock_2() { return &___TypeContractCacheLock_2; }
	inline void set_TypeContractCacheLock_2(RuntimeObject * value)
	{
		___TypeContractCacheLock_2 = value;
		Il2CppCodeGenWriteBarrier((&___TypeContractCacheLock_2), value);
	}

	inline static int32_t get_offset_of__sharedState_3() { return static_cast<int32_t>(offsetof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields, ____sharedState_3)); }
	inline DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 * get__sharedState_3() const { return ____sharedState_3; }
	inline DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 ** get_address_of__sharedState_3() { return &____sharedState_3; }
	inline void set__sharedState_3(DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81 * value)
	{
		____sharedState_3 = value;
		Il2CppCodeGenWriteBarrier((&____sharedState_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEFAULTCONTRACTRESOLVER_TFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_H
#ifndef JSONCONTRACT_T24F8047299CC6DFDDDB0EBFB116A8A281264BFA3_H
#define JSONCONTRACT_T24F8047299CC6DFDDDB0EBFB116A8A281264BFA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContract
struct  JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3  : public RuntimeObject
{
public:
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsNullable
	bool ___IsNullable_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsConvertable
	bool ___IsConvertable_1;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsEnum
	bool ___IsEnum_2;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::NonNullableUnderlyingType
	Type_t * ___NonNullableUnderlyingType_3;
	// Newtonsoft.Json.ReadType Newtonsoft.Json.Serialization.JsonContract::InternalReadType
	int32_t ___InternalReadType_4;
	// Newtonsoft.Json.Serialization.JsonContractType Newtonsoft.Json.Serialization.JsonContract::ContractType
	int32_t ___ContractType_5;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsReadOnlyOrFixedSize
	bool ___IsReadOnlyOrFixedSize_6;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsSealed
	bool ___IsSealed_7;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::IsInstantiable
	bool ___IsInstantiable_8;
	// System.Collections.Generic.List`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializedCallbacks
	List_1_t21D9BA4C41DEFB88D15B6D260F571539789A245C * ____onDeserializedCallbacks_9;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onDeserializingCallbacks
	RuntimeObject* ____onDeserializingCallbacks_10;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializedCallbacks
	RuntimeObject* ____onSerializedCallbacks_11;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationCallback> Newtonsoft.Json.Serialization.JsonContract::_onSerializingCallbacks
	RuntimeObject* ____onSerializingCallbacks_12;
	// System.Collections.Generic.IList`1<Newtonsoft.Json.Serialization.SerializationErrorCallback> Newtonsoft.Json.Serialization.JsonContract::_onErrorCallbacks
	RuntimeObject* ____onErrorCallbacks_13;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::_createdType
	Type_t * ____createdType_14;
	// System.Type Newtonsoft.Json.Serialization.JsonContract::<UnderlyingType>k__BackingField
	Type_t * ___U3CUnderlyingTypeU3Ek__BackingField_15;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContract::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_16;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<Converter>k__BackingField
	JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * ___U3CConverterU3Ek__BackingField_17;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContract::<InternalConverter>k__BackingField
	JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * ___U3CInternalConverterU3Ek__BackingField_18;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonContract::<DefaultCreator>k__BackingField
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ___U3CDefaultCreatorU3Ek__BackingField_19;
	// System.Boolean Newtonsoft.Json.Serialization.JsonContract::<DefaultCreatorNonPublic>k__BackingField
	bool ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20;

public:
	inline static int32_t get_offset_of_IsNullable_0() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___IsNullable_0)); }
	inline bool get_IsNullable_0() const { return ___IsNullable_0; }
	inline bool* get_address_of_IsNullable_0() { return &___IsNullable_0; }
	inline void set_IsNullable_0(bool value)
	{
		___IsNullable_0 = value;
	}

	inline static int32_t get_offset_of_IsConvertable_1() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___IsConvertable_1)); }
	inline bool get_IsConvertable_1() const { return ___IsConvertable_1; }
	inline bool* get_address_of_IsConvertable_1() { return &___IsConvertable_1; }
	inline void set_IsConvertable_1(bool value)
	{
		___IsConvertable_1 = value;
	}

	inline static int32_t get_offset_of_IsEnum_2() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___IsEnum_2)); }
	inline bool get_IsEnum_2() const { return ___IsEnum_2; }
	inline bool* get_address_of_IsEnum_2() { return &___IsEnum_2; }
	inline void set_IsEnum_2(bool value)
	{
		___IsEnum_2 = value;
	}

	inline static int32_t get_offset_of_NonNullableUnderlyingType_3() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___NonNullableUnderlyingType_3)); }
	inline Type_t * get_NonNullableUnderlyingType_3() const { return ___NonNullableUnderlyingType_3; }
	inline Type_t ** get_address_of_NonNullableUnderlyingType_3() { return &___NonNullableUnderlyingType_3; }
	inline void set_NonNullableUnderlyingType_3(Type_t * value)
	{
		___NonNullableUnderlyingType_3 = value;
		Il2CppCodeGenWriteBarrier((&___NonNullableUnderlyingType_3), value);
	}

	inline static int32_t get_offset_of_InternalReadType_4() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___InternalReadType_4)); }
	inline int32_t get_InternalReadType_4() const { return ___InternalReadType_4; }
	inline int32_t* get_address_of_InternalReadType_4() { return &___InternalReadType_4; }
	inline void set_InternalReadType_4(int32_t value)
	{
		___InternalReadType_4 = value;
	}

	inline static int32_t get_offset_of_ContractType_5() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___ContractType_5)); }
	inline int32_t get_ContractType_5() const { return ___ContractType_5; }
	inline int32_t* get_address_of_ContractType_5() { return &___ContractType_5; }
	inline void set_ContractType_5(int32_t value)
	{
		___ContractType_5 = value;
	}

	inline static int32_t get_offset_of_IsReadOnlyOrFixedSize_6() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___IsReadOnlyOrFixedSize_6)); }
	inline bool get_IsReadOnlyOrFixedSize_6() const { return ___IsReadOnlyOrFixedSize_6; }
	inline bool* get_address_of_IsReadOnlyOrFixedSize_6() { return &___IsReadOnlyOrFixedSize_6; }
	inline void set_IsReadOnlyOrFixedSize_6(bool value)
	{
		___IsReadOnlyOrFixedSize_6 = value;
	}

	inline static int32_t get_offset_of_IsSealed_7() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___IsSealed_7)); }
	inline bool get_IsSealed_7() const { return ___IsSealed_7; }
	inline bool* get_address_of_IsSealed_7() { return &___IsSealed_7; }
	inline void set_IsSealed_7(bool value)
	{
		___IsSealed_7 = value;
	}

	inline static int32_t get_offset_of_IsInstantiable_8() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___IsInstantiable_8)); }
	inline bool get_IsInstantiable_8() const { return ___IsInstantiable_8; }
	inline bool* get_address_of_IsInstantiable_8() { return &___IsInstantiable_8; }
	inline void set_IsInstantiable_8(bool value)
	{
		___IsInstantiable_8 = value;
	}

	inline static int32_t get_offset_of__onDeserializedCallbacks_9() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ____onDeserializedCallbacks_9)); }
	inline List_1_t21D9BA4C41DEFB88D15B6D260F571539789A245C * get__onDeserializedCallbacks_9() const { return ____onDeserializedCallbacks_9; }
	inline List_1_t21D9BA4C41DEFB88D15B6D260F571539789A245C ** get_address_of__onDeserializedCallbacks_9() { return &____onDeserializedCallbacks_9; }
	inline void set__onDeserializedCallbacks_9(List_1_t21D9BA4C41DEFB88D15B6D260F571539789A245C * value)
	{
		____onDeserializedCallbacks_9 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializedCallbacks_9), value);
	}

	inline static int32_t get_offset_of__onDeserializingCallbacks_10() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ____onDeserializingCallbacks_10)); }
	inline RuntimeObject* get__onDeserializingCallbacks_10() const { return ____onDeserializingCallbacks_10; }
	inline RuntimeObject** get_address_of__onDeserializingCallbacks_10() { return &____onDeserializingCallbacks_10; }
	inline void set__onDeserializingCallbacks_10(RuntimeObject* value)
	{
		____onDeserializingCallbacks_10 = value;
		Il2CppCodeGenWriteBarrier((&____onDeserializingCallbacks_10), value);
	}

	inline static int32_t get_offset_of__onSerializedCallbacks_11() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ____onSerializedCallbacks_11)); }
	inline RuntimeObject* get__onSerializedCallbacks_11() const { return ____onSerializedCallbacks_11; }
	inline RuntimeObject** get_address_of__onSerializedCallbacks_11() { return &____onSerializedCallbacks_11; }
	inline void set__onSerializedCallbacks_11(RuntimeObject* value)
	{
		____onSerializedCallbacks_11 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializedCallbacks_11), value);
	}

	inline static int32_t get_offset_of__onSerializingCallbacks_12() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ____onSerializingCallbacks_12)); }
	inline RuntimeObject* get__onSerializingCallbacks_12() const { return ____onSerializingCallbacks_12; }
	inline RuntimeObject** get_address_of__onSerializingCallbacks_12() { return &____onSerializingCallbacks_12; }
	inline void set__onSerializingCallbacks_12(RuntimeObject* value)
	{
		____onSerializingCallbacks_12 = value;
		Il2CppCodeGenWriteBarrier((&____onSerializingCallbacks_12), value);
	}

	inline static int32_t get_offset_of__onErrorCallbacks_13() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ____onErrorCallbacks_13)); }
	inline RuntimeObject* get__onErrorCallbacks_13() const { return ____onErrorCallbacks_13; }
	inline RuntimeObject** get_address_of__onErrorCallbacks_13() { return &____onErrorCallbacks_13; }
	inline void set__onErrorCallbacks_13(RuntimeObject* value)
	{
		____onErrorCallbacks_13 = value;
		Il2CppCodeGenWriteBarrier((&____onErrorCallbacks_13), value);
	}

	inline static int32_t get_offset_of__createdType_14() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ____createdType_14)); }
	inline Type_t * get__createdType_14() const { return ____createdType_14; }
	inline Type_t ** get_address_of__createdType_14() { return &____createdType_14; }
	inline void set__createdType_14(Type_t * value)
	{
		____createdType_14 = value;
		Il2CppCodeGenWriteBarrier((&____createdType_14), value);
	}

	inline static int32_t get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___U3CUnderlyingTypeU3Ek__BackingField_15)); }
	inline Type_t * get_U3CUnderlyingTypeU3Ek__BackingField_15() const { return ___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline Type_t ** get_address_of_U3CUnderlyingTypeU3Ek__BackingField_15() { return &___U3CUnderlyingTypeU3Ek__BackingField_15; }
	inline void set_U3CUnderlyingTypeU3Ek__BackingField_15(Type_t * value)
	{
		___U3CUnderlyingTypeU3Ek__BackingField_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingTypeU3Ek__BackingField_15), value);
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___U3CIsReferenceU3Ek__BackingField_16)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_16() const { return ___U3CIsReferenceU3Ek__BackingField_16; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_16() { return &___U3CIsReferenceU3Ek__BackingField_16; }
	inline void set_U3CIsReferenceU3Ek__BackingField_16(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___U3CConverterU3Ek__BackingField_17)); }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * get_U3CConverterU3Ek__BackingField_17() const { return ___U3CConverterU3Ek__BackingField_17; }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 ** get_address_of_U3CConverterU3Ek__BackingField_17() { return &___U3CConverterU3Ek__BackingField_17; }
	inline void set_U3CConverterU3Ek__BackingField_17(JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * value)
	{
		___U3CConverterU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_17), value);
	}

	inline static int32_t get_offset_of_U3CInternalConverterU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___U3CInternalConverterU3Ek__BackingField_18)); }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * get_U3CInternalConverterU3Ek__BackingField_18() const { return ___U3CInternalConverterU3Ek__BackingField_18; }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 ** get_address_of_U3CInternalConverterU3Ek__BackingField_18() { return &___U3CInternalConverterU3Ek__BackingField_18; }
	inline void set_U3CInternalConverterU3Ek__BackingField_18(JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * value)
	{
		___U3CInternalConverterU3Ek__BackingField_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInternalConverterU3Ek__BackingField_18), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___U3CDefaultCreatorU3Ek__BackingField_19)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get_U3CDefaultCreatorU3Ek__BackingField_19() const { return ___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of_U3CDefaultCreatorU3Ek__BackingField_19() { return &___U3CDefaultCreatorU3Ek__BackingField_19; }
	inline void set_U3CDefaultCreatorU3Ek__BackingField_19(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		___U3CDefaultCreatorU3Ek__BackingField_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultCreatorU3Ek__BackingField_19), value);
	}

	inline static int32_t get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3, ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20)); }
	inline bool get_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() const { return ___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline bool* get_address_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20() { return &___U3CDefaultCreatorNonPublicU3Ek__BackingField_20; }
	inline void set_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(bool value)
	{
		___U3CDefaultCreatorNonPublicU3Ek__BackingField_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTRACT_T24F8047299CC6DFDDDB0EBFB116A8A281264BFA3_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef NULLABLE_1_T23DBCFD9D5A38259E91BB91E0DA2F310925C5660_H
#define NULLABLE_1_T23DBCFD9D5A38259E91BB91E0DA2F310925C5660_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateFormatHandling>
struct  Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T23DBCFD9D5A38259E91BB91E0DA2F310925C5660_H
#ifndef NULLABLE_1_T0043F1937EEC03CE189188BDA0D7755CFAB90BDF_H
#define NULLABLE_1_T0043F1937EEC03CE189188BDA0D7755CFAB90BDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateParseHandling>
struct  Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0043F1937EEC03CE189188BDA0D7755CFAB90BDF_H
#ifndef NULLABLE_1_TCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE_H
#define NULLABLE_1_TCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling>
struct  Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE_H
#ifndef NULLABLE_1_T29A6EA9275C7673344AD9B7F5F44F6126748C0FA_H
#define NULLABLE_1_T29A6EA9275C7673344AD9B7F5F44F6126748C0FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling>
struct  Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T29A6EA9275C7673344AD9B7F5F44F6126748C0FA_H
#ifndef NULLABLE_1_T11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810_H
#define NULLABLE_1_T11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling>
struct  Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810_H
#ifndef NULLABLE_1_T7F088871846AAE8EF21D751D8B97430B761673C6_H
#define NULLABLE_1_T7F088871846AAE8EF21D751D8B97430B761673C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.FloatParseHandling>
struct  Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T7F088871846AAE8EF21D751D8B97430B761673C6_H
#ifndef NULLABLE_1_T941F23108D6567DD26A44A84616CC08E05CD0059_H
#define NULLABLE_1_T941F23108D6567DD26A44A84616CC08E05CD0059_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Formatting>
struct  Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T941F23108D6567DD26A44A84616CC08E05CD0059_H
#ifndef NULLABLE_1_TBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA_H
#define NULLABLE_1_TBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.NullValueHandling>
struct  Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA_H
#ifndef NULLABLE_1_T43CD81743444619B643C0A3912749713399518C5_H
#define NULLABLE_1_T43CD81743444619B643C0A3912749713399518C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling>
struct  Nullable_1_t43CD81743444619B643C0A3912749713399518C5 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t43CD81743444619B643C0A3912749713399518C5, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t43CD81743444619B643C0A3912749713399518C5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T43CD81743444619B643C0A3912749713399518C5_H
#ifndef NULLABLE_1_T916E09184D3B63637815710054D9D54B992F5F33_H
#define NULLABLE_1_T916E09184D3B63637815710054D9D54B992F5F33_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling>
struct  Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T916E09184D3B63637815710054D9D54B992F5F33_H
#ifndef NULLABLE_1_T77B4FDCF98614C924B1310C1941E0897738EA65D_H
#define NULLABLE_1_T77B4FDCF98614C924B1310C1941E0897738EA65D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Required>
struct  Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T77B4FDCF98614C924B1310C1941E0897738EA65D_H
#ifndef NULLABLE_1_T1EAC8106ECCF52137638D92C96E7A2A9566D43A9_H
#define NULLABLE_1_T1EAC8106ECCF52137638D92C96E7A2A9566D43A9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence>
struct  Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T1EAC8106ECCF52137638D92C96E7A2A9566D43A9_H
#ifndef NULLABLE_1_T5FD184676887BA5E49A49440E56E8C7ADFC19C6E_H
#define NULLABLE_1_T5FD184676887BA5E49A49440E56E8C7ADFC19C6E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling>
struct  Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T5FD184676887BA5E49A49440E56E8C7ADFC19C6E_H
#ifndef NULLABLE_1_T86F69995F4B87E51DBFA76271E780A42E928B184_H
#define NULLABLE_1_T86F69995F4B87E51DBFA76271E780A42E928B184_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<Newtonsoft.Json.TypeNameHandling>
struct  Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T86F69995F4B87E51DBFA76271E780A42E928B184_H
#ifndef STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#define STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.Serialization.StreamingContext
struct  StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 
{
public:
	// System.Object System.Runtime.Serialization.StreamingContext::m_additionalContext
	RuntimeObject * ___m_additionalContext_0;
	// System.Runtime.Serialization.StreamingContextStates System.Runtime.Serialization.StreamingContext::m_state
	int32_t ___m_state_1;

public:
	inline static int32_t get_offset_of_m_additionalContext_0() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_additionalContext_0)); }
	inline RuntimeObject * get_m_additionalContext_0() const { return ___m_additionalContext_0; }
	inline RuntimeObject ** get_address_of_m_additionalContext_0() { return &___m_additionalContext_0; }
	inline void set_m_additionalContext_0(RuntimeObject * value)
	{
		___m_additionalContext_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_additionalContext_0), value);
	}

	inline static int32_t get_offset_of_m_state_1() { return static_cast<int32_t>(offsetof(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034, ___m_state_1)); }
	inline int32_t get_m_state_1() const { return ___m_state_1; }
	inline int32_t* get_address_of_m_state_1() { return &___m_state_1; }
	inline void set_m_state_1(int32_t value)
	{
		___m_state_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_pinvoke
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
// Native definition for COM marshalling of System.Runtime.Serialization.StreamingContext
struct StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034_marshaled_com
{
	Il2CppIUnknown* ___m_additionalContext_0;
	int32_t ___m_state_1;
};
#endif // STREAMINGCONTEXT_T2CCDC54E0E8D078AF4A50E3A8B921B828A900034_H
#ifndef JSONREADER_TF29A2AB439002FDEFB9592AA3147F02ADB3841DC_H
#define JSONREADER_TF29A2AB439002FDEFB9592AA3147F02ADB3841DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonReader
struct  JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC  : public RuntimeObject
{
public:
	// Newtonsoft.Json.JsonToken Newtonsoft.Json.JsonReader::_tokenType
	int32_t ____tokenType_0;
	// System.Object Newtonsoft.Json.JsonReader::_value
	RuntimeObject * ____value_1;
	// System.Char Newtonsoft.Json.JsonReader::_quoteChar
	Il2CppChar ____quoteChar_2;
	// Newtonsoft.Json.JsonReader/State Newtonsoft.Json.JsonReader::_currentState
	int32_t ____currentState_3;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonReader::_currentPosition
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  ____currentPosition_4;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonReader::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_5;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonReader::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_6;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonReader::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_7;
	// System.Boolean Newtonsoft.Json.JsonReader::_hasExceededMaxDepth
	bool ____hasExceededMaxDepth_8;
	// Newtonsoft.Json.DateParseHandling Newtonsoft.Json.JsonReader::_dateParseHandling
	int32_t ____dateParseHandling_9;
	// Newtonsoft.Json.FloatParseHandling Newtonsoft.Json.JsonReader::_floatParseHandling
	int32_t ____floatParseHandling_10;
	// System.String Newtonsoft.Json.JsonReader::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonReader::_stack
	List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * ____stack_12;
	// System.Boolean Newtonsoft.Json.JsonReader::<CloseInput>k__BackingField
	bool ___U3CCloseInputU3Ek__BackingField_13;
	// System.Boolean Newtonsoft.Json.JsonReader::<SupportMultipleContent>k__BackingField
	bool ___U3CSupportMultipleContentU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of__tokenType_0() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____tokenType_0)); }
	inline int32_t get__tokenType_0() const { return ____tokenType_0; }
	inline int32_t* get_address_of__tokenType_0() { return &____tokenType_0; }
	inline void set__tokenType_0(int32_t value)
	{
		____tokenType_0 = value;
	}

	inline static int32_t get_offset_of__value_1() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____value_1)); }
	inline RuntimeObject * get__value_1() const { return ____value_1; }
	inline RuntimeObject ** get_address_of__value_1() { return &____value_1; }
	inline void set__value_1(RuntimeObject * value)
	{
		____value_1 = value;
		Il2CppCodeGenWriteBarrier((&____value_1), value);
	}

	inline static int32_t get_offset_of__quoteChar_2() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____quoteChar_2)); }
	inline Il2CppChar get__quoteChar_2() const { return ____quoteChar_2; }
	inline Il2CppChar* get_address_of__quoteChar_2() { return &____quoteChar_2; }
	inline void set__quoteChar_2(Il2CppChar value)
	{
		____quoteChar_2 = value;
	}

	inline static int32_t get_offset_of__currentState_3() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____currentState_3)); }
	inline int32_t get__currentState_3() const { return ____currentState_3; }
	inline int32_t* get_address_of__currentState_3() { return &____currentState_3; }
	inline void set__currentState_3(int32_t value)
	{
		____currentState_3 = value;
	}

	inline static int32_t get_offset_of__currentPosition_4() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____currentPosition_4)); }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  get__currentPosition_4() const { return ____currentPosition_4; }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD * get_address_of__currentPosition_4() { return &____currentPosition_4; }
	inline void set__currentPosition_4(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  value)
	{
		____currentPosition_4 = value;
	}

	inline static int32_t get_offset_of__culture_5() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____culture_5)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_5() const { return ____culture_5; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_5() { return &____culture_5; }
	inline void set__culture_5(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_5 = value;
		Il2CppCodeGenWriteBarrier((&____culture_5), value);
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_6() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____dateTimeZoneHandling_6)); }
	inline int32_t get__dateTimeZoneHandling_6() const { return ____dateTimeZoneHandling_6; }
	inline int32_t* get_address_of__dateTimeZoneHandling_6() { return &____dateTimeZoneHandling_6; }
	inline void set__dateTimeZoneHandling_6(int32_t value)
	{
		____dateTimeZoneHandling_6 = value;
	}

	inline static int32_t get_offset_of__maxDepth_7() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____maxDepth_7)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_7() const { return ____maxDepth_7; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_7() { return &____maxDepth_7; }
	inline void set__maxDepth_7(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_7 = value;
	}

	inline static int32_t get_offset_of__hasExceededMaxDepth_8() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____hasExceededMaxDepth_8)); }
	inline bool get__hasExceededMaxDepth_8() const { return ____hasExceededMaxDepth_8; }
	inline bool* get_address_of__hasExceededMaxDepth_8() { return &____hasExceededMaxDepth_8; }
	inline void set__hasExceededMaxDepth_8(bool value)
	{
		____hasExceededMaxDepth_8 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_9() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____dateParseHandling_9)); }
	inline int32_t get__dateParseHandling_9() const { return ____dateParseHandling_9; }
	inline int32_t* get_address_of__dateParseHandling_9() { return &____dateParseHandling_9; }
	inline void set__dateParseHandling_9(int32_t value)
	{
		____dateParseHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_10() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____floatParseHandling_10)); }
	inline int32_t get__floatParseHandling_10() const { return ____floatParseHandling_10; }
	inline int32_t* get_address_of__floatParseHandling_10() { return &____floatParseHandling_10; }
	inline void set__floatParseHandling_10(int32_t value)
	{
		____floatParseHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__stack_12() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ____stack_12)); }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * get__stack_12() const { return ____stack_12; }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 ** get_address_of__stack_12() { return &____stack_12; }
	inline void set__stack_12(List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * value)
	{
		____stack_12 = value;
		Il2CppCodeGenWriteBarrier((&____stack_12), value);
	}

	inline static int32_t get_offset_of_U3CCloseInputU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ___U3CCloseInputU3Ek__BackingField_13)); }
	inline bool get_U3CCloseInputU3Ek__BackingField_13() const { return ___U3CCloseInputU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CCloseInputU3Ek__BackingField_13() { return &___U3CCloseInputU3Ek__BackingField_13; }
	inline void set_U3CCloseInputU3Ek__BackingField_13(bool value)
	{
		___U3CCloseInputU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC, ___U3CSupportMultipleContentU3Ek__BackingField_14)); }
	inline bool get_U3CSupportMultipleContentU3Ek__BackingField_14() const { return ___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CSupportMultipleContentU3Ek__BackingField_14() { return &___U3CSupportMultipleContentU3Ek__BackingField_14; }
	inline void set_U3CSupportMultipleContentU3Ek__BackingField_14(bool value)
	{
		___U3CSupportMultipleContentU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONREADER_TF29A2AB439002FDEFB9592AA3147F02ADB3841DC_H
#ifndef JSONSERIALIZER_T6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB_H
#define JSONSERIALIZER_T6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonSerializer
struct  JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB  : public RuntimeObject
{
public:
	// Newtonsoft.Json.TypeNameHandling Newtonsoft.Json.JsonSerializer::_typeNameHandling
	int32_t ____typeNameHandling_0;
	// System.Runtime.Serialization.Formatters.FormatterAssemblyStyle Newtonsoft.Json.JsonSerializer::_typeNameAssemblyFormat
	int32_t ____typeNameAssemblyFormat_1;
	// Newtonsoft.Json.PreserveReferencesHandling Newtonsoft.Json.JsonSerializer::_preserveReferencesHandling
	int32_t ____preserveReferencesHandling_2;
	// Newtonsoft.Json.ReferenceLoopHandling Newtonsoft.Json.JsonSerializer::_referenceLoopHandling
	int32_t ____referenceLoopHandling_3;
	// Newtonsoft.Json.MissingMemberHandling Newtonsoft.Json.JsonSerializer::_missingMemberHandling
	int32_t ____missingMemberHandling_4;
	// Newtonsoft.Json.ObjectCreationHandling Newtonsoft.Json.JsonSerializer::_objectCreationHandling
	int32_t ____objectCreationHandling_5;
	// Newtonsoft.Json.NullValueHandling Newtonsoft.Json.JsonSerializer::_nullValueHandling
	int32_t ____nullValueHandling_6;
	// Newtonsoft.Json.DefaultValueHandling Newtonsoft.Json.JsonSerializer::_defaultValueHandling
	int32_t ____defaultValueHandling_7;
	// Newtonsoft.Json.ConstructorHandling Newtonsoft.Json.JsonSerializer::_constructorHandling
	int32_t ____constructorHandling_8;
	// Newtonsoft.Json.MetadataPropertyHandling Newtonsoft.Json.JsonSerializer::_metadataPropertyHandling
	int32_t ____metadataPropertyHandling_9;
	// Newtonsoft.Json.JsonConverterCollection Newtonsoft.Json.JsonSerializer::_converters
	JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF * ____converters_10;
	// Newtonsoft.Json.Serialization.IContractResolver Newtonsoft.Json.JsonSerializer::_contractResolver
	RuntimeObject* ____contractResolver_11;
	// Newtonsoft.Json.Serialization.ITraceWriter Newtonsoft.Json.JsonSerializer::_traceWriter
	RuntimeObject* ____traceWriter_12;
	// System.Collections.IEqualityComparer Newtonsoft.Json.JsonSerializer::_equalityComparer
	RuntimeObject* ____equalityComparer_13;
	// System.Runtime.Serialization.SerializationBinder Newtonsoft.Json.JsonSerializer::_binder
	SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * ____binder_14;
	// System.Runtime.Serialization.StreamingContext Newtonsoft.Json.JsonSerializer::_context
	StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  ____context_15;
	// Newtonsoft.Json.Serialization.IReferenceResolver Newtonsoft.Json.JsonSerializer::_referenceResolver
	RuntimeObject* ____referenceResolver_16;
	// System.Nullable`1<Newtonsoft.Json.Formatting> Newtonsoft.Json.JsonSerializer::_formatting
	Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  ____formatting_17;
	// System.Nullable`1<Newtonsoft.Json.DateFormatHandling> Newtonsoft.Json.JsonSerializer::_dateFormatHandling
	Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  ____dateFormatHandling_18;
	// System.Nullable`1<Newtonsoft.Json.DateTimeZoneHandling> Newtonsoft.Json.JsonSerializer::_dateTimeZoneHandling
	Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  ____dateTimeZoneHandling_19;
	// System.Nullable`1<Newtonsoft.Json.DateParseHandling> Newtonsoft.Json.JsonSerializer::_dateParseHandling
	Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  ____dateParseHandling_20;
	// System.Nullable`1<Newtonsoft.Json.FloatFormatHandling> Newtonsoft.Json.JsonSerializer::_floatFormatHandling
	Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  ____floatFormatHandling_21;
	// System.Nullable`1<Newtonsoft.Json.FloatParseHandling> Newtonsoft.Json.JsonSerializer::_floatParseHandling
	Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  ____floatParseHandling_22;
	// System.Nullable`1<Newtonsoft.Json.StringEscapeHandling> Newtonsoft.Json.JsonSerializer::_stringEscapeHandling
	Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  ____stringEscapeHandling_23;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonSerializer::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_24;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.JsonSerializer::_maxDepth
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ____maxDepth_25;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_maxDepthSet
	bool ____maxDepthSet_26;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.JsonSerializer::_checkAdditionalContent
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____checkAdditionalContent_27;
	// System.String Newtonsoft.Json.JsonSerializer::_dateFormatString
	String_t* ____dateFormatString_28;
	// System.Boolean Newtonsoft.Json.JsonSerializer::_dateFormatStringSet
	bool ____dateFormatStringSet_29;
	// System.EventHandler`1<Newtonsoft.Json.Serialization.ErrorEventArgs> Newtonsoft.Json.JsonSerializer::Error
	EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * ___Error_30;

public:
	inline static int32_t get_offset_of__typeNameHandling_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____typeNameHandling_0)); }
	inline int32_t get__typeNameHandling_0() const { return ____typeNameHandling_0; }
	inline int32_t* get_address_of__typeNameHandling_0() { return &____typeNameHandling_0; }
	inline void set__typeNameHandling_0(int32_t value)
	{
		____typeNameHandling_0 = value;
	}

	inline static int32_t get_offset_of__typeNameAssemblyFormat_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____typeNameAssemblyFormat_1)); }
	inline int32_t get__typeNameAssemblyFormat_1() const { return ____typeNameAssemblyFormat_1; }
	inline int32_t* get_address_of__typeNameAssemblyFormat_1() { return &____typeNameAssemblyFormat_1; }
	inline void set__typeNameAssemblyFormat_1(int32_t value)
	{
		____typeNameAssemblyFormat_1 = value;
	}

	inline static int32_t get_offset_of__preserveReferencesHandling_2() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____preserveReferencesHandling_2)); }
	inline int32_t get__preserveReferencesHandling_2() const { return ____preserveReferencesHandling_2; }
	inline int32_t* get_address_of__preserveReferencesHandling_2() { return &____preserveReferencesHandling_2; }
	inline void set__preserveReferencesHandling_2(int32_t value)
	{
		____preserveReferencesHandling_2 = value;
	}

	inline static int32_t get_offset_of__referenceLoopHandling_3() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____referenceLoopHandling_3)); }
	inline int32_t get__referenceLoopHandling_3() const { return ____referenceLoopHandling_3; }
	inline int32_t* get_address_of__referenceLoopHandling_3() { return &____referenceLoopHandling_3; }
	inline void set__referenceLoopHandling_3(int32_t value)
	{
		____referenceLoopHandling_3 = value;
	}

	inline static int32_t get_offset_of__missingMemberHandling_4() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____missingMemberHandling_4)); }
	inline int32_t get__missingMemberHandling_4() const { return ____missingMemberHandling_4; }
	inline int32_t* get_address_of__missingMemberHandling_4() { return &____missingMemberHandling_4; }
	inline void set__missingMemberHandling_4(int32_t value)
	{
		____missingMemberHandling_4 = value;
	}

	inline static int32_t get_offset_of__objectCreationHandling_5() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____objectCreationHandling_5)); }
	inline int32_t get__objectCreationHandling_5() const { return ____objectCreationHandling_5; }
	inline int32_t* get_address_of__objectCreationHandling_5() { return &____objectCreationHandling_5; }
	inline void set__objectCreationHandling_5(int32_t value)
	{
		____objectCreationHandling_5 = value;
	}

	inline static int32_t get_offset_of__nullValueHandling_6() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____nullValueHandling_6)); }
	inline int32_t get__nullValueHandling_6() const { return ____nullValueHandling_6; }
	inline int32_t* get_address_of__nullValueHandling_6() { return &____nullValueHandling_6; }
	inline void set__nullValueHandling_6(int32_t value)
	{
		____nullValueHandling_6 = value;
	}

	inline static int32_t get_offset_of__defaultValueHandling_7() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____defaultValueHandling_7)); }
	inline int32_t get__defaultValueHandling_7() const { return ____defaultValueHandling_7; }
	inline int32_t* get_address_of__defaultValueHandling_7() { return &____defaultValueHandling_7; }
	inline void set__defaultValueHandling_7(int32_t value)
	{
		____defaultValueHandling_7 = value;
	}

	inline static int32_t get_offset_of__constructorHandling_8() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____constructorHandling_8)); }
	inline int32_t get__constructorHandling_8() const { return ____constructorHandling_8; }
	inline int32_t* get_address_of__constructorHandling_8() { return &____constructorHandling_8; }
	inline void set__constructorHandling_8(int32_t value)
	{
		____constructorHandling_8 = value;
	}

	inline static int32_t get_offset_of__metadataPropertyHandling_9() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____metadataPropertyHandling_9)); }
	inline int32_t get__metadataPropertyHandling_9() const { return ____metadataPropertyHandling_9; }
	inline int32_t* get_address_of__metadataPropertyHandling_9() { return &____metadataPropertyHandling_9; }
	inline void set__metadataPropertyHandling_9(int32_t value)
	{
		____metadataPropertyHandling_9 = value;
	}

	inline static int32_t get_offset_of__converters_10() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____converters_10)); }
	inline JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF * get__converters_10() const { return ____converters_10; }
	inline JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF ** get_address_of__converters_10() { return &____converters_10; }
	inline void set__converters_10(JsonConverterCollection_tE01B17B4897D2254EC50F208B6AA43A938298ABF * value)
	{
		____converters_10 = value;
		Il2CppCodeGenWriteBarrier((&____converters_10), value);
	}

	inline static int32_t get_offset_of__contractResolver_11() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____contractResolver_11)); }
	inline RuntimeObject* get__contractResolver_11() const { return ____contractResolver_11; }
	inline RuntimeObject** get_address_of__contractResolver_11() { return &____contractResolver_11; }
	inline void set__contractResolver_11(RuntimeObject* value)
	{
		____contractResolver_11 = value;
		Il2CppCodeGenWriteBarrier((&____contractResolver_11), value);
	}

	inline static int32_t get_offset_of__traceWriter_12() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____traceWriter_12)); }
	inline RuntimeObject* get__traceWriter_12() const { return ____traceWriter_12; }
	inline RuntimeObject** get_address_of__traceWriter_12() { return &____traceWriter_12; }
	inline void set__traceWriter_12(RuntimeObject* value)
	{
		____traceWriter_12 = value;
		Il2CppCodeGenWriteBarrier((&____traceWriter_12), value);
	}

	inline static int32_t get_offset_of__equalityComparer_13() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____equalityComparer_13)); }
	inline RuntimeObject* get__equalityComparer_13() const { return ____equalityComparer_13; }
	inline RuntimeObject** get_address_of__equalityComparer_13() { return &____equalityComparer_13; }
	inline void set__equalityComparer_13(RuntimeObject* value)
	{
		____equalityComparer_13 = value;
		Il2CppCodeGenWriteBarrier((&____equalityComparer_13), value);
	}

	inline static int32_t get_offset_of__binder_14() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____binder_14)); }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * get__binder_14() const { return ____binder_14; }
	inline SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 ** get_address_of__binder_14() { return &____binder_14; }
	inline void set__binder_14(SerializationBinder_tB5EBAF328371FB7CF23E37F5984D8412762CFFA4 * value)
	{
		____binder_14 = value;
		Il2CppCodeGenWriteBarrier((&____binder_14), value);
	}

	inline static int32_t get_offset_of__context_15() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____context_15)); }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  get__context_15() const { return ____context_15; }
	inline StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034 * get_address_of__context_15() { return &____context_15; }
	inline void set__context_15(StreamingContext_t2CCDC54E0E8D078AF4A50E3A8B921B828A900034  value)
	{
		____context_15 = value;
	}

	inline static int32_t get_offset_of__referenceResolver_16() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____referenceResolver_16)); }
	inline RuntimeObject* get__referenceResolver_16() const { return ____referenceResolver_16; }
	inline RuntimeObject** get_address_of__referenceResolver_16() { return &____referenceResolver_16; }
	inline void set__referenceResolver_16(RuntimeObject* value)
	{
		____referenceResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&____referenceResolver_16), value);
	}

	inline static int32_t get_offset_of__formatting_17() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____formatting_17)); }
	inline Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  get__formatting_17() const { return ____formatting_17; }
	inline Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059 * get_address_of__formatting_17() { return &____formatting_17; }
	inline void set__formatting_17(Nullable_1_t941F23108D6567DD26A44A84616CC08E05CD0059  value)
	{
		____formatting_17 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_18() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateFormatHandling_18)); }
	inline Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  get__dateFormatHandling_18() const { return ____dateFormatHandling_18; }
	inline Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660 * get_address_of__dateFormatHandling_18() { return &____dateFormatHandling_18; }
	inline void set__dateFormatHandling_18(Nullable_1_t23DBCFD9D5A38259E91BB91E0DA2F310925C5660  value)
	{
		____dateFormatHandling_18 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_19() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateTimeZoneHandling_19)); }
	inline Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  get__dateTimeZoneHandling_19() const { return ____dateTimeZoneHandling_19; }
	inline Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE * get_address_of__dateTimeZoneHandling_19() { return &____dateTimeZoneHandling_19; }
	inline void set__dateTimeZoneHandling_19(Nullable_1_tCF0657FE0B30CF5C6985F71CD8CB9F6B30FCFBAE  value)
	{
		____dateTimeZoneHandling_19 = value;
	}

	inline static int32_t get_offset_of__dateParseHandling_20() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateParseHandling_20)); }
	inline Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  get__dateParseHandling_20() const { return ____dateParseHandling_20; }
	inline Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF * get_address_of__dateParseHandling_20() { return &____dateParseHandling_20; }
	inline void set__dateParseHandling_20(Nullable_1_t0043F1937EEC03CE189188BDA0D7755CFAB90BDF  value)
	{
		____dateParseHandling_20 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_21() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____floatFormatHandling_21)); }
	inline Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  get__floatFormatHandling_21() const { return ____floatFormatHandling_21; }
	inline Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810 * get_address_of__floatFormatHandling_21() { return &____floatFormatHandling_21; }
	inline void set__floatFormatHandling_21(Nullable_1_t11EA7D2537CE4EA4D26B215C7D07C7CE7BAC1810  value)
	{
		____floatFormatHandling_21 = value;
	}

	inline static int32_t get_offset_of__floatParseHandling_22() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____floatParseHandling_22)); }
	inline Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  get__floatParseHandling_22() const { return ____floatParseHandling_22; }
	inline Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6 * get_address_of__floatParseHandling_22() { return &____floatParseHandling_22; }
	inline void set__floatParseHandling_22(Nullable_1_t7F088871846AAE8EF21D751D8B97430B761673C6  value)
	{
		____floatParseHandling_22 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_23() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____stringEscapeHandling_23)); }
	inline Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  get__stringEscapeHandling_23() const { return ____stringEscapeHandling_23; }
	inline Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E * get_address_of__stringEscapeHandling_23() { return &____stringEscapeHandling_23; }
	inline void set__stringEscapeHandling_23(Nullable_1_t5FD184676887BA5E49A49440E56E8C7ADFC19C6E  value)
	{
		____stringEscapeHandling_23 = value;
	}

	inline static int32_t get_offset_of__culture_24() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____culture_24)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_24() const { return ____culture_24; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_24() { return &____culture_24; }
	inline void set__culture_24(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_24 = value;
		Il2CppCodeGenWriteBarrier((&____culture_24), value);
	}

	inline static int32_t get_offset_of__maxDepth_25() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____maxDepth_25)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get__maxDepth_25() const { return ____maxDepth_25; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of__maxDepth_25() { return &____maxDepth_25; }
	inline void set__maxDepth_25(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		____maxDepth_25 = value;
	}

	inline static int32_t get_offset_of__maxDepthSet_26() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____maxDepthSet_26)); }
	inline bool get__maxDepthSet_26() const { return ____maxDepthSet_26; }
	inline bool* get_address_of__maxDepthSet_26() { return &____maxDepthSet_26; }
	inline void set__maxDepthSet_26(bool value)
	{
		____maxDepthSet_26 = value;
	}

	inline static int32_t get_offset_of__checkAdditionalContent_27() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____checkAdditionalContent_27)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__checkAdditionalContent_27() const { return ____checkAdditionalContent_27; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__checkAdditionalContent_27() { return &____checkAdditionalContent_27; }
	inline void set__checkAdditionalContent_27(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____checkAdditionalContent_27 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_28() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateFormatString_28)); }
	inline String_t* get__dateFormatString_28() const { return ____dateFormatString_28; }
	inline String_t** get_address_of__dateFormatString_28() { return &____dateFormatString_28; }
	inline void set__dateFormatString_28(String_t* value)
	{
		____dateFormatString_28 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_28), value);
	}

	inline static int32_t get_offset_of__dateFormatStringSet_29() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ____dateFormatStringSet_29)); }
	inline bool get__dateFormatStringSet_29() const { return ____dateFormatStringSet_29; }
	inline bool* get_address_of__dateFormatStringSet_29() { return &____dateFormatStringSet_29; }
	inline void set__dateFormatStringSet_29(bool value)
	{
		____dateFormatStringSet_29 = value;
	}

	inline static int32_t get_offset_of_Error_30() { return static_cast<int32_t>(offsetof(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB, ___Error_30)); }
	inline EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * get_Error_30() const { return ___Error_30; }
	inline EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A ** get_address_of_Error_30() { return &___Error_30; }
	inline void set_Error_30(EventHandler_1_tD0863E67C9A50D35387D2C3E7DF644C8E96F5B9A * value)
	{
		___Error_30 = value;
		Il2CppCodeGenWriteBarrier((&___Error_30), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB_H
#ifndef JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#define JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.JsonWriter
struct  JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<Newtonsoft.Json.JsonPosition> Newtonsoft.Json.JsonWriter::_stack
	List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * ____stack_2;
	// Newtonsoft.Json.JsonPosition Newtonsoft.Json.JsonWriter::_currentPosition
	JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  ____currentPosition_3;
	// Newtonsoft.Json.JsonWriter/State Newtonsoft.Json.JsonWriter::_currentState
	int32_t ____currentState_4;
	// Newtonsoft.Json.Formatting Newtonsoft.Json.JsonWriter::_formatting
	int32_t ____formatting_5;
	// System.Boolean Newtonsoft.Json.JsonWriter::<CloseOutput>k__BackingField
	bool ___U3CCloseOutputU3Ek__BackingField_6;
	// Newtonsoft.Json.DateFormatHandling Newtonsoft.Json.JsonWriter::_dateFormatHandling
	int32_t ____dateFormatHandling_7;
	// Newtonsoft.Json.DateTimeZoneHandling Newtonsoft.Json.JsonWriter::_dateTimeZoneHandling
	int32_t ____dateTimeZoneHandling_8;
	// Newtonsoft.Json.StringEscapeHandling Newtonsoft.Json.JsonWriter::_stringEscapeHandling
	int32_t ____stringEscapeHandling_9;
	// Newtonsoft.Json.FloatFormatHandling Newtonsoft.Json.JsonWriter::_floatFormatHandling
	int32_t ____floatFormatHandling_10;
	// System.String Newtonsoft.Json.JsonWriter::_dateFormatString
	String_t* ____dateFormatString_11;
	// System.Globalization.CultureInfo Newtonsoft.Json.JsonWriter::_culture
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ____culture_12;

public:
	inline static int32_t get_offset_of__stack_2() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____stack_2)); }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * get__stack_2() const { return ____stack_2; }
	inline List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 ** get_address_of__stack_2() { return &____stack_2; }
	inline void set__stack_2(List_1_tBF0C33A77C788F7114B09EDEDE0C44867A1D0037 * value)
	{
		____stack_2 = value;
		Il2CppCodeGenWriteBarrier((&____stack_2), value);
	}

	inline static int32_t get_offset_of__currentPosition_3() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____currentPosition_3)); }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  get__currentPosition_3() const { return ____currentPosition_3; }
	inline JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD * get_address_of__currentPosition_3() { return &____currentPosition_3; }
	inline void set__currentPosition_3(JsonPosition_t7C381FA9E9591B7782B67363333C5CFA331E8EAD  value)
	{
		____currentPosition_3 = value;
	}

	inline static int32_t get_offset_of__currentState_4() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____currentState_4)); }
	inline int32_t get__currentState_4() const { return ____currentState_4; }
	inline int32_t* get_address_of__currentState_4() { return &____currentState_4; }
	inline void set__currentState_4(int32_t value)
	{
		____currentState_4 = value;
	}

	inline static int32_t get_offset_of__formatting_5() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____formatting_5)); }
	inline int32_t get__formatting_5() const { return ____formatting_5; }
	inline int32_t* get_address_of__formatting_5() { return &____formatting_5; }
	inline void set__formatting_5(int32_t value)
	{
		____formatting_5 = value;
	}

	inline static int32_t get_offset_of_U3CCloseOutputU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ___U3CCloseOutputU3Ek__BackingField_6)); }
	inline bool get_U3CCloseOutputU3Ek__BackingField_6() const { return ___U3CCloseOutputU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CCloseOutputU3Ek__BackingField_6() { return &___U3CCloseOutputU3Ek__BackingField_6; }
	inline void set_U3CCloseOutputU3Ek__BackingField_6(bool value)
	{
		___U3CCloseOutputU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of__dateFormatHandling_7() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateFormatHandling_7)); }
	inline int32_t get__dateFormatHandling_7() const { return ____dateFormatHandling_7; }
	inline int32_t* get_address_of__dateFormatHandling_7() { return &____dateFormatHandling_7; }
	inline void set__dateFormatHandling_7(int32_t value)
	{
		____dateFormatHandling_7 = value;
	}

	inline static int32_t get_offset_of__dateTimeZoneHandling_8() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateTimeZoneHandling_8)); }
	inline int32_t get__dateTimeZoneHandling_8() const { return ____dateTimeZoneHandling_8; }
	inline int32_t* get_address_of__dateTimeZoneHandling_8() { return &____dateTimeZoneHandling_8; }
	inline void set__dateTimeZoneHandling_8(int32_t value)
	{
		____dateTimeZoneHandling_8 = value;
	}

	inline static int32_t get_offset_of__stringEscapeHandling_9() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____stringEscapeHandling_9)); }
	inline int32_t get__stringEscapeHandling_9() const { return ____stringEscapeHandling_9; }
	inline int32_t* get_address_of__stringEscapeHandling_9() { return &____stringEscapeHandling_9; }
	inline void set__stringEscapeHandling_9(int32_t value)
	{
		____stringEscapeHandling_9 = value;
	}

	inline static int32_t get_offset_of__floatFormatHandling_10() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____floatFormatHandling_10)); }
	inline int32_t get__floatFormatHandling_10() const { return ____floatFormatHandling_10; }
	inline int32_t* get_address_of__floatFormatHandling_10() { return &____floatFormatHandling_10; }
	inline void set__floatFormatHandling_10(int32_t value)
	{
		____floatFormatHandling_10 = value;
	}

	inline static int32_t get_offset_of__dateFormatString_11() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____dateFormatString_11)); }
	inline String_t* get__dateFormatString_11() const { return ____dateFormatString_11; }
	inline String_t** get_address_of__dateFormatString_11() { return &____dateFormatString_11; }
	inline void set__dateFormatString_11(String_t* value)
	{
		____dateFormatString_11 = value;
		Il2CppCodeGenWriteBarrier((&____dateFormatString_11), value);
	}

	inline static int32_t get_offset_of__culture_12() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091, ____culture_12)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get__culture_12() const { return ____culture_12; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of__culture_12() { return &____culture_12; }
	inline void set__culture_12(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		____culture_12 = value;
		Il2CppCodeGenWriteBarrier((&____culture_12), value);
	}
};

struct JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields
{
public:
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArray
	StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* ___StateArray_0;
	// Newtonsoft.Json.JsonWriter/State[][] Newtonsoft.Json.JsonWriter::StateArrayTempate
	StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* ___StateArrayTempate_1;

public:
	inline static int32_t get_offset_of_StateArray_0() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields, ___StateArray_0)); }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* get_StateArray_0() const { return ___StateArray_0; }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84** get_address_of_StateArray_0() { return &___StateArray_0; }
	inline void set_StateArray_0(StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* value)
	{
		___StateArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___StateArray_0), value);
	}

	inline static int32_t get_offset_of_StateArrayTempate_1() { return static_cast<int32_t>(offsetof(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091_StaticFields, ___StateArrayTempate_1)); }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* get_StateArrayTempate_1() const { return ___StateArrayTempate_1; }
	inline StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84** get_address_of_StateArrayTempate_1() { return &___StateArrayTempate_1; }
	inline void set_StateArrayTempate_1(StateU5BU5DU5BU5D_t93CBF619A66899721F87E82BA122761F964BCD84* value)
	{
		___StateArrayTempate_1 = value;
		Il2CppCodeGenWriteBarrier((&___StateArrayTempate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONWRITER_T4CF8C6EC696955A38B904D71B2AB1C6ED6155091_H
#ifndef JRAW_TDEAC098BFC05E891669DC59AE31064EA708A8A4E_H
#define JRAW_TDEAC098BFC05E891669DC59AE31064EA708A8A4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JRaw
struct  JRaw_tDEAC098BFC05E891669DC59AE31064EA708A8A4E  : public JValue_t80B940E650348637E29E2241958BC52097B1202F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JRAW_TDEAC098BFC05E891669DC59AE31064EA708A8A4E_H
#ifndef EXTENSIONDATAGETTER_T552EB770A47F66A53412918802CCC988B223E853_H
#define EXTENSIONDATAGETTER_T552EB770A47F66A53412918802CCC988B223E853_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ExtensionDataGetter
struct  ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATAGETTER_T552EB770A47F66A53412918802CCC988B223E853_H
#ifndef EXTENSIONDATASETTER_T1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0_H
#define EXTENSIONDATASETTER_T1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.ExtensionDataSetter
struct  ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTENSIONDATASETTER_T1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0_H
#ifndef JSONCONTAINERCONTRACT_T159D64F86F765005FA9031707C63F46AB0ABD127_H
#define JSONCONTAINERCONTRACT_T159D64F86F765005FA9031707C63F46AB0ABD127_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonContainerContract
struct  JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127  : public JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3
{
public:
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_itemContract
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * ____itemContract_21;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonContainerContract::_finalItemContract
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * ____finalItemContract_22;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonContainerContract::<ItemConverter>k__BackingField
	JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * ___U3CItemConverterU3Ek__BackingField_23;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_24;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonContainerContract::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ___U3CItemTypeNameHandlingU3Ek__BackingField_26;

public:
	inline static int32_t get_offset_of__itemContract_21() { return static_cast<int32_t>(offsetof(JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127, ____itemContract_21)); }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * get__itemContract_21() const { return ____itemContract_21; }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 ** get_address_of__itemContract_21() { return &____itemContract_21; }
	inline void set__itemContract_21(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * value)
	{
		____itemContract_21 = value;
		Il2CppCodeGenWriteBarrier((&____itemContract_21), value);
	}

	inline static int32_t get_offset_of__finalItemContract_22() { return static_cast<int32_t>(offsetof(JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127, ____finalItemContract_22)); }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * get__finalItemContract_22() const { return ____finalItemContract_22; }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 ** get_address_of__finalItemContract_22() { return &____finalItemContract_22; }
	inline void set__finalItemContract_22(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * value)
	{
		____finalItemContract_22 = value;
		Il2CppCodeGenWriteBarrier((&____finalItemContract_22), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127, ___U3CItemConverterU3Ek__BackingField_23)); }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * get_U3CItemConverterU3Ek__BackingField_23() const { return ___U3CItemConverterU3Ek__BackingField_23; }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 ** get_address_of_U3CItemConverterU3Ek__BackingField_23() { return &___U3CItemConverterU3Ek__BackingField_23; }
	inline void set_U3CItemConverterU3Ek__BackingField_23(JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * value)
	{
		___U3CItemConverterU3Ek__BackingField_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_23), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127, ___U3CItemIsReferenceU3Ek__BackingField_24)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_24() const { return ___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_24() { return &___U3CItemIsReferenceU3Ek__BackingField_24; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_24(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_25; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_25 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127, ___U3CItemTypeNameHandlingU3Ek__BackingField_26)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get_U3CItemTypeNameHandlingU3Ek__BackingField_26() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_26() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_26; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_26(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_26 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONCONTAINERCONTRACT_T159D64F86F765005FA9031707C63F46AB0ABD127_H
#ifndef JSONLINQCONTRACT_T9FB8F2255FDD47ADFF6C249353BE5713931FC897_H
#define JSONLINQCONTRACT_T9FB8F2255FDD47ADFF6C249353BE5713931FC897_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonLinqContract
struct  JsonLinqContract_t9FB8F2255FDD47ADFF6C249353BE5713931FC897  : public JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONLINQCONTRACT_T9FB8F2255FDD47ADFF6C249353BE5713931FC897_H
#ifndef JSONPRIMITIVECONTRACT_TD7663FB786D31F278ED1E2DF3237CEF558288702_H
#define JSONPRIMITIVECONTRACT_TD7663FB786D31F278ED1E2DF3237CEF558288702_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonPrimitiveContract
struct  JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702  : public JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3
{
public:
	// Newtonsoft.Json.Utilities.PrimitiveTypeCode Newtonsoft.Json.Serialization.JsonPrimitiveContract::<TypeCode>k__BackingField
	int32_t ___U3CTypeCodeU3Ek__BackingField_21;

public:
	inline static int32_t get_offset_of_U3CTypeCodeU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702, ___U3CTypeCodeU3Ek__BackingField_21)); }
	inline int32_t get_U3CTypeCodeU3Ek__BackingField_21() const { return ___U3CTypeCodeU3Ek__BackingField_21; }
	inline int32_t* get_address_of_U3CTypeCodeU3Ek__BackingField_21() { return &___U3CTypeCodeU3Ek__BackingField_21; }
	inline void set_U3CTypeCodeU3Ek__BackingField_21(int32_t value)
	{
		___U3CTypeCodeU3Ek__BackingField_21 = value;
	}
};

struct JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.Type,Newtonsoft.Json.ReadType> Newtonsoft.Json.Serialization.JsonPrimitiveContract::ReadTypeMap
	Dictionary_2_tFB7A438A8035214C0B4040917D6B5B4117234CF3 * ___ReadTypeMap_22;

public:
	inline static int32_t get_offset_of_ReadTypeMap_22() { return static_cast<int32_t>(offsetof(JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702_StaticFields, ___ReadTypeMap_22)); }
	inline Dictionary_2_tFB7A438A8035214C0B4040917D6B5B4117234CF3 * get_ReadTypeMap_22() const { return ___ReadTypeMap_22; }
	inline Dictionary_2_tFB7A438A8035214C0B4040917D6B5B4117234CF3 ** get_address_of_ReadTypeMap_22() { return &___ReadTypeMap_22; }
	inline void set_ReadTypeMap_22(Dictionary_2_tFB7A438A8035214C0B4040917D6B5B4117234CF3 * value)
	{
		___ReadTypeMap_22 = value;
		Il2CppCodeGenWriteBarrier((&___ReadTypeMap_22), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPRIMITIVECONTRACT_TD7663FB786D31F278ED1E2DF3237CEF558288702_H
#ifndef JSONPROPERTY_TAAB286C386C8EDAB84CB6F0966E598677202DA97_H
#define JSONPROPERTY_TAAB286C386C8EDAB84CB6F0966E598677202DA97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonProperty
struct  JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97  : public RuntimeObject
{
public:
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonProperty::_required
	Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  ____required_0;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_hasExplicitDefaultValue
	bool ____hasExplicitDefaultValue_1;
	// System.Object Newtonsoft.Json.Serialization.JsonProperty::_defaultValue
	RuntimeObject * ____defaultValue_2;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_hasGeneratedDefaultValue
	bool ____hasGeneratedDefaultValue_3;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::_propertyName
	String_t* ____propertyName_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::_skipPropertyNameEscape
	bool ____skipPropertyNameEscape_5;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::_propertyType
	Type_t * ____propertyType_6;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonProperty::<PropertyContract>k__BackingField
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * ___U3CPropertyContractU3Ek__BackingField_7;
	// System.Type Newtonsoft.Json.Serialization.JsonProperty::<DeclaringType>k__BackingField
	Type_t * ___U3CDeclaringTypeU3Ek__BackingField_8;
	// System.Nullable`1<System.Int32> Newtonsoft.Json.Serialization.JsonProperty::<Order>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3COrderU3Ek__BackingField_9;
	// System.String Newtonsoft.Json.Serialization.JsonProperty::<UnderlyingName>k__BackingField
	String_t* ___U3CUnderlyingNameU3Ek__BackingField_10;
	// Newtonsoft.Json.Serialization.IValueProvider Newtonsoft.Json.Serialization.JsonProperty::<ValueProvider>k__BackingField
	RuntimeObject* ___U3CValueProviderU3Ek__BackingField_11;
	// Newtonsoft.Json.Serialization.IAttributeProvider Newtonsoft.Json.Serialization.JsonProperty::<AttributeProvider>k__BackingField
	RuntimeObject* ___U3CAttributeProviderU3Ek__BackingField_12;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<Converter>k__BackingField
	JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * ___U3CConverterU3Ek__BackingField_13;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<MemberConverter>k__BackingField
	JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * ___U3CMemberConverterU3Ek__BackingField_14;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Ignored>k__BackingField
	bool ___U3CIgnoredU3Ek__BackingField_15;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Readable>k__BackingField
	bool ___U3CReadableU3Ek__BackingField_16;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<Writable>k__BackingField
	bool ___U3CWritableU3Ek__BackingField_17;
	// System.Boolean Newtonsoft.Json.Serialization.JsonProperty::<HasMemberAttribute>k__BackingField
	bool ___U3CHasMemberAttributeU3Ek__BackingField_18;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<IsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CIsReferenceU3Ek__BackingField_19;
	// System.Nullable`1<Newtonsoft.Json.NullValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<NullValueHandling>k__BackingField
	Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  ___U3CNullValueHandlingU3Ek__BackingField_20;
	// System.Nullable`1<Newtonsoft.Json.DefaultValueHandling> Newtonsoft.Json.Serialization.JsonProperty::<DefaultValueHandling>k__BackingField
	Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  ___U3CDefaultValueHandlingU3Ek__BackingField_21;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ReferenceLoopHandling>k__BackingField
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ___U3CReferenceLoopHandlingU3Ek__BackingField_22;
	// System.Nullable`1<Newtonsoft.Json.ObjectCreationHandling> Newtonsoft.Json.Serialization.JsonProperty::<ObjectCreationHandling>k__BackingField
	Nullable_1_t43CD81743444619B643C0A3912749713399518C5  ___U3CObjectCreationHandlingU3Ek__BackingField_23;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<TypeNameHandling>k__BackingField
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ___U3CTypeNameHandlingU3Ek__BackingField_24;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldSerialize>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CShouldSerializeU3Ek__BackingField_25;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<ShouldDeserialize>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CShouldDeserializeU3Ek__BackingField_26;
	// System.Predicate`1<System.Object> Newtonsoft.Json.Serialization.JsonProperty::<GetIsSpecified>k__BackingField
	Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * ___U3CGetIsSpecifiedU3Ek__BackingField_27;
	// System.Action`2<System.Object,System.Object> Newtonsoft.Json.Serialization.JsonProperty::<SetIsSpecified>k__BackingField
	Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * ___U3CSetIsSpecifiedU3Ek__BackingField_28;
	// Newtonsoft.Json.JsonConverter Newtonsoft.Json.Serialization.JsonProperty::<ItemConverter>k__BackingField
	JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * ___U3CItemConverterU3Ek__BackingField_29;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonProperty::<ItemIsReference>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CItemIsReferenceU3Ek__BackingField_30;
	// System.Nullable`1<Newtonsoft.Json.TypeNameHandling> Newtonsoft.Json.Serialization.JsonProperty::<ItemTypeNameHandling>k__BackingField
	Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  ___U3CItemTypeNameHandlingU3Ek__BackingField_31;
	// System.Nullable`1<Newtonsoft.Json.ReferenceLoopHandling> Newtonsoft.Json.Serialization.JsonProperty::<ItemReferenceLoopHandling>k__BackingField
	Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32;

public:
	inline static int32_t get_offset_of__required_0() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____required_0)); }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  get__required_0() const { return ____required_0; }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D * get_address_of__required_0() { return &____required_0; }
	inline void set__required_0(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  value)
	{
		____required_0 = value;
	}

	inline static int32_t get_offset_of__hasExplicitDefaultValue_1() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____hasExplicitDefaultValue_1)); }
	inline bool get__hasExplicitDefaultValue_1() const { return ____hasExplicitDefaultValue_1; }
	inline bool* get_address_of__hasExplicitDefaultValue_1() { return &____hasExplicitDefaultValue_1; }
	inline void set__hasExplicitDefaultValue_1(bool value)
	{
		____hasExplicitDefaultValue_1 = value;
	}

	inline static int32_t get_offset_of__defaultValue_2() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____defaultValue_2)); }
	inline RuntimeObject * get__defaultValue_2() const { return ____defaultValue_2; }
	inline RuntimeObject ** get_address_of__defaultValue_2() { return &____defaultValue_2; }
	inline void set__defaultValue_2(RuntimeObject * value)
	{
		____defaultValue_2 = value;
		Il2CppCodeGenWriteBarrier((&____defaultValue_2), value);
	}

	inline static int32_t get_offset_of__hasGeneratedDefaultValue_3() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____hasGeneratedDefaultValue_3)); }
	inline bool get__hasGeneratedDefaultValue_3() const { return ____hasGeneratedDefaultValue_3; }
	inline bool* get_address_of__hasGeneratedDefaultValue_3() { return &____hasGeneratedDefaultValue_3; }
	inline void set__hasGeneratedDefaultValue_3(bool value)
	{
		____hasGeneratedDefaultValue_3 = value;
	}

	inline static int32_t get_offset_of__propertyName_4() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____propertyName_4)); }
	inline String_t* get__propertyName_4() const { return ____propertyName_4; }
	inline String_t** get_address_of__propertyName_4() { return &____propertyName_4; }
	inline void set__propertyName_4(String_t* value)
	{
		____propertyName_4 = value;
		Il2CppCodeGenWriteBarrier((&____propertyName_4), value);
	}

	inline static int32_t get_offset_of__skipPropertyNameEscape_5() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____skipPropertyNameEscape_5)); }
	inline bool get__skipPropertyNameEscape_5() const { return ____skipPropertyNameEscape_5; }
	inline bool* get_address_of__skipPropertyNameEscape_5() { return &____skipPropertyNameEscape_5; }
	inline void set__skipPropertyNameEscape_5(bool value)
	{
		____skipPropertyNameEscape_5 = value;
	}

	inline static int32_t get_offset_of__propertyType_6() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ____propertyType_6)); }
	inline Type_t * get__propertyType_6() const { return ____propertyType_6; }
	inline Type_t ** get_address_of__propertyType_6() { return &____propertyType_6; }
	inline void set__propertyType_6(Type_t * value)
	{
		____propertyType_6 = value;
		Il2CppCodeGenWriteBarrier((&____propertyType_6), value);
	}

	inline static int32_t get_offset_of_U3CPropertyContractU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CPropertyContractU3Ek__BackingField_7)); }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * get_U3CPropertyContractU3Ek__BackingField_7() const { return ___U3CPropertyContractU3Ek__BackingField_7; }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 ** get_address_of_U3CPropertyContractU3Ek__BackingField_7() { return &___U3CPropertyContractU3Ek__BackingField_7; }
	inline void set_U3CPropertyContractU3Ek__BackingField_7(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * value)
	{
		___U3CPropertyContractU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertyContractU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CDeclaringTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CDeclaringTypeU3Ek__BackingField_8() const { return ___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CDeclaringTypeU3Ek__BackingField_8() { return &___U3CDeclaringTypeU3Ek__BackingField_8; }
	inline void set_U3CDeclaringTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CDeclaringTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDeclaringTypeU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3COrderU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3COrderU3Ek__BackingField_9)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3COrderU3Ek__BackingField_9() const { return ___U3COrderU3Ek__BackingField_9; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3COrderU3Ek__BackingField_9() { return &___U3COrderU3Ek__BackingField_9; }
	inline void set_U3COrderU3Ek__BackingField_9(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3COrderU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CUnderlyingNameU3Ek__BackingField_10)); }
	inline String_t* get_U3CUnderlyingNameU3Ek__BackingField_10() const { return ___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUnderlyingNameU3Ek__BackingField_10() { return &___U3CUnderlyingNameU3Ek__BackingField_10; }
	inline void set_U3CUnderlyingNameU3Ek__BackingField_10(String_t* value)
	{
		___U3CUnderlyingNameU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUnderlyingNameU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CValueProviderU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CValueProviderU3Ek__BackingField_11)); }
	inline RuntimeObject* get_U3CValueProviderU3Ek__BackingField_11() const { return ___U3CValueProviderU3Ek__BackingField_11; }
	inline RuntimeObject** get_address_of_U3CValueProviderU3Ek__BackingField_11() { return &___U3CValueProviderU3Ek__BackingField_11; }
	inline void set_U3CValueProviderU3Ek__BackingField_11(RuntimeObject* value)
	{
		___U3CValueProviderU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueProviderU3Ek__BackingField_11), value);
	}

	inline static int32_t get_offset_of_U3CAttributeProviderU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CAttributeProviderU3Ek__BackingField_12)); }
	inline RuntimeObject* get_U3CAttributeProviderU3Ek__BackingField_12() const { return ___U3CAttributeProviderU3Ek__BackingField_12; }
	inline RuntimeObject** get_address_of_U3CAttributeProviderU3Ek__BackingField_12() { return &___U3CAttributeProviderU3Ek__BackingField_12; }
	inline void set_U3CAttributeProviderU3Ek__BackingField_12(RuntimeObject* value)
	{
		___U3CAttributeProviderU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAttributeProviderU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CConverterU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CConverterU3Ek__BackingField_13)); }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * get_U3CConverterU3Ek__BackingField_13() const { return ___U3CConverterU3Ek__BackingField_13; }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 ** get_address_of_U3CConverterU3Ek__BackingField_13() { return &___U3CConverterU3Ek__BackingField_13; }
	inline void set_U3CConverterU3Ek__BackingField_13(JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * value)
	{
		___U3CConverterU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConverterU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of_U3CMemberConverterU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CMemberConverterU3Ek__BackingField_14)); }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * get_U3CMemberConverterU3Ek__BackingField_14() const { return ___U3CMemberConverterU3Ek__BackingField_14; }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 ** get_address_of_U3CMemberConverterU3Ek__BackingField_14() { return &___U3CMemberConverterU3Ek__BackingField_14; }
	inline void set_U3CMemberConverterU3Ek__BackingField_14(JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * value)
	{
		___U3CMemberConverterU3Ek__BackingField_14 = value;
		Il2CppCodeGenWriteBarrier((&___U3CMemberConverterU3Ek__BackingField_14), value);
	}

	inline static int32_t get_offset_of_U3CIgnoredU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CIgnoredU3Ek__BackingField_15)); }
	inline bool get_U3CIgnoredU3Ek__BackingField_15() const { return ___U3CIgnoredU3Ek__BackingField_15; }
	inline bool* get_address_of_U3CIgnoredU3Ek__BackingField_15() { return &___U3CIgnoredU3Ek__BackingField_15; }
	inline void set_U3CIgnoredU3Ek__BackingField_15(bool value)
	{
		___U3CIgnoredU3Ek__BackingField_15 = value;
	}

	inline static int32_t get_offset_of_U3CReadableU3Ek__BackingField_16() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CReadableU3Ek__BackingField_16)); }
	inline bool get_U3CReadableU3Ek__BackingField_16() const { return ___U3CReadableU3Ek__BackingField_16; }
	inline bool* get_address_of_U3CReadableU3Ek__BackingField_16() { return &___U3CReadableU3Ek__BackingField_16; }
	inline void set_U3CReadableU3Ek__BackingField_16(bool value)
	{
		___U3CReadableU3Ek__BackingField_16 = value;
	}

	inline static int32_t get_offset_of_U3CWritableU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CWritableU3Ek__BackingField_17)); }
	inline bool get_U3CWritableU3Ek__BackingField_17() const { return ___U3CWritableU3Ek__BackingField_17; }
	inline bool* get_address_of_U3CWritableU3Ek__BackingField_17() { return &___U3CWritableU3Ek__BackingField_17; }
	inline void set_U3CWritableU3Ek__BackingField_17(bool value)
	{
		___U3CWritableU3Ek__BackingField_17 = value;
	}

	inline static int32_t get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CHasMemberAttributeU3Ek__BackingField_18)); }
	inline bool get_U3CHasMemberAttributeU3Ek__BackingField_18() const { return ___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline bool* get_address_of_U3CHasMemberAttributeU3Ek__BackingField_18() { return &___U3CHasMemberAttributeU3Ek__BackingField_18; }
	inline void set_U3CHasMemberAttributeU3Ek__BackingField_18(bool value)
	{
		___U3CHasMemberAttributeU3Ek__BackingField_18 = value;
	}

	inline static int32_t get_offset_of_U3CIsReferenceU3Ek__BackingField_19() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CIsReferenceU3Ek__BackingField_19)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CIsReferenceU3Ek__BackingField_19() const { return ___U3CIsReferenceU3Ek__BackingField_19; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CIsReferenceU3Ek__BackingField_19() { return &___U3CIsReferenceU3Ek__BackingField_19; }
	inline void set_U3CIsReferenceU3Ek__BackingField_19(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CIsReferenceU3Ek__BackingField_19 = value;
	}

	inline static int32_t get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CNullValueHandlingU3Ek__BackingField_20)); }
	inline Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  get_U3CNullValueHandlingU3Ek__BackingField_20() const { return ___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA * get_address_of_U3CNullValueHandlingU3Ek__BackingField_20() { return &___U3CNullValueHandlingU3Ek__BackingField_20; }
	inline void set_U3CNullValueHandlingU3Ek__BackingField_20(Nullable_1_tBA4F03135C1B236E5E52E46C4D7C7A161D6D65AA  value)
	{
		___U3CNullValueHandlingU3Ek__BackingField_20 = value;
	}

	inline static int32_t get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CDefaultValueHandlingU3Ek__BackingField_21)); }
	inline Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  get_U3CDefaultValueHandlingU3Ek__BackingField_21() const { return ___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA * get_address_of_U3CDefaultValueHandlingU3Ek__BackingField_21() { return &___U3CDefaultValueHandlingU3Ek__BackingField_21; }
	inline void set_U3CDefaultValueHandlingU3Ek__BackingField_21(Nullable_1_t29A6EA9275C7673344AD9B7F5F44F6126748C0FA  value)
	{
		___U3CDefaultValueHandlingU3Ek__BackingField_21 = value;
	}

	inline static int32_t get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CReferenceLoopHandlingU3Ek__BackingField_22)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get_U3CReferenceLoopHandlingU3Ek__BackingField_22() const { return ___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of_U3CReferenceLoopHandlingU3Ek__BackingField_22() { return &___U3CReferenceLoopHandlingU3Ek__BackingField_22; }
	inline void set_U3CReferenceLoopHandlingU3Ek__BackingField_22(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		___U3CReferenceLoopHandlingU3Ek__BackingField_22 = value;
	}

	inline static int32_t get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CObjectCreationHandlingU3Ek__BackingField_23)); }
	inline Nullable_1_t43CD81743444619B643C0A3912749713399518C5  get_U3CObjectCreationHandlingU3Ek__BackingField_23() const { return ___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline Nullable_1_t43CD81743444619B643C0A3912749713399518C5 * get_address_of_U3CObjectCreationHandlingU3Ek__BackingField_23() { return &___U3CObjectCreationHandlingU3Ek__BackingField_23; }
	inline void set_U3CObjectCreationHandlingU3Ek__BackingField_23(Nullable_1_t43CD81743444619B643C0A3912749713399518C5  value)
	{
		___U3CObjectCreationHandlingU3Ek__BackingField_23 = value;
	}

	inline static int32_t get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CTypeNameHandlingU3Ek__BackingField_24)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get_U3CTypeNameHandlingU3Ek__BackingField_24() const { return ___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of_U3CTypeNameHandlingU3Ek__BackingField_24() { return &___U3CTypeNameHandlingU3Ek__BackingField_24; }
	inline void set_U3CTypeNameHandlingU3Ek__BackingField_24(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		___U3CTypeNameHandlingU3Ek__BackingField_24 = value;
	}

	inline static int32_t get_offset_of_U3CShouldSerializeU3Ek__BackingField_25() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CShouldSerializeU3Ek__BackingField_25)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CShouldSerializeU3Ek__BackingField_25() const { return ___U3CShouldSerializeU3Ek__BackingField_25; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CShouldSerializeU3Ek__BackingField_25() { return &___U3CShouldSerializeU3Ek__BackingField_25; }
	inline void set_U3CShouldSerializeU3Ek__BackingField_25(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CShouldSerializeU3Ek__BackingField_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldSerializeU3Ek__BackingField_25), value);
	}

	inline static int32_t get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CShouldDeserializeU3Ek__BackingField_26)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CShouldDeserializeU3Ek__BackingField_26() const { return ___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CShouldDeserializeU3Ek__BackingField_26() { return &___U3CShouldDeserializeU3Ek__BackingField_26; }
	inline void set_U3CShouldDeserializeU3Ek__BackingField_26(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CShouldDeserializeU3Ek__BackingField_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CShouldDeserializeU3Ek__BackingField_26), value);
	}

	inline static int32_t get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CGetIsSpecifiedU3Ek__BackingField_27)); }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * get_U3CGetIsSpecifiedU3Ek__BackingField_27() const { return ___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 ** get_address_of_U3CGetIsSpecifiedU3Ek__BackingField_27() { return &___U3CGetIsSpecifiedU3Ek__BackingField_27; }
	inline void set_U3CGetIsSpecifiedU3Ek__BackingField_27(Predicate_1_t4AA10EFD4C5497CA1CD0FE35A6AF5990FF5D0979 * value)
	{
		___U3CGetIsSpecifiedU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetIsSpecifiedU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CSetIsSpecifiedU3Ek__BackingField_28)); }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * get_U3CSetIsSpecifiedU3Ek__BackingField_28() const { return ___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C ** get_address_of_U3CSetIsSpecifiedU3Ek__BackingField_28() { return &___U3CSetIsSpecifiedU3Ek__BackingField_28; }
	inline void set_U3CSetIsSpecifiedU3Ek__BackingField_28(Action_2_t0DB6FD6F515527EAB552B690A291778C6F00D48C * value)
	{
		___U3CSetIsSpecifiedU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetIsSpecifiedU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CItemConverterU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CItemConverterU3Ek__BackingField_29)); }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * get_U3CItemConverterU3Ek__BackingField_29() const { return ___U3CItemConverterU3Ek__BackingField_29; }
	inline JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 ** get_address_of_U3CItemConverterU3Ek__BackingField_29() { return &___U3CItemConverterU3Ek__BackingField_29; }
	inline void set_U3CItemConverterU3Ek__BackingField_29(JsonConverter_t1CC2EC16B3208095A97A3CAE15286A5E16C3DD59 * value)
	{
		___U3CItemConverterU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CItemConverterU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CItemIsReferenceU3Ek__BackingField_30)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CItemIsReferenceU3Ek__BackingField_30() const { return ___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CItemIsReferenceU3Ek__BackingField_30() { return &___U3CItemIsReferenceU3Ek__BackingField_30; }
	inline void set_U3CItemIsReferenceU3Ek__BackingField_30(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CItemIsReferenceU3Ek__BackingField_30 = value;
	}

	inline static int32_t get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CItemTypeNameHandlingU3Ek__BackingField_31)); }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  get_U3CItemTypeNameHandlingU3Ek__BackingField_31() const { return ___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184 * get_address_of_U3CItemTypeNameHandlingU3Ek__BackingField_31() { return &___U3CItemTypeNameHandlingU3Ek__BackingField_31; }
	inline void set_U3CItemTypeNameHandlingU3Ek__BackingField_31(Nullable_1_t86F69995F4B87E51DBFA76271E780A42E928B184  value)
	{
		___U3CItemTypeNameHandlingU3Ek__BackingField_31 = value;
	}

	inline static int32_t get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return static_cast<int32_t>(offsetof(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97, ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32)); }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  get_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() const { return ___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33 * get_address_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32() { return &___U3CItemReferenceLoopHandlingU3Ek__BackingField_32; }
	inline void set_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(Nullable_1_t916E09184D3B63637815710054D9D54B992F5F33  value)
	{
		___U3CItemReferenceLoopHandlingU3Ek__BackingField_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONPROPERTY_TAAB286C386C8EDAB84CB6F0966E598677202DA97_H
#ifndef CREATORPROPERTYCONTEXT_T0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075_H
#define CREATORPROPERTYCONTEXT_T0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext
struct  CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075  : public RuntimeObject
{
public:
	// System.String Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Name
	String_t* ___Name_0;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Property
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * ___Property_1;
	// Newtonsoft.Json.Serialization.JsonProperty Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::ConstructorProperty
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * ___ConstructorProperty_2;
	// System.Nullable`1<Newtonsoft.Json.Serialization.JsonSerializerInternalReader/PropertyPresence> Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Presence
	Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9  ___Presence_3;
	// System.Object Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Value
	RuntimeObject * ___Value_4;
	// System.Boolean Newtonsoft.Json.Serialization.JsonSerializerInternalReader/CreatorPropertyContext::Used
	bool ___Used_5;

public:
	inline static int32_t get_offset_of_Name_0() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075, ___Name_0)); }
	inline String_t* get_Name_0() const { return ___Name_0; }
	inline String_t** get_address_of_Name_0() { return &___Name_0; }
	inline void set_Name_0(String_t* value)
	{
		___Name_0 = value;
		Il2CppCodeGenWriteBarrier((&___Name_0), value);
	}

	inline static int32_t get_offset_of_Property_1() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075, ___Property_1)); }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * get_Property_1() const { return ___Property_1; }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 ** get_address_of_Property_1() { return &___Property_1; }
	inline void set_Property_1(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * value)
	{
		___Property_1 = value;
		Il2CppCodeGenWriteBarrier((&___Property_1), value);
	}

	inline static int32_t get_offset_of_ConstructorProperty_2() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075, ___ConstructorProperty_2)); }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * get_ConstructorProperty_2() const { return ___ConstructorProperty_2; }
	inline JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 ** get_address_of_ConstructorProperty_2() { return &___ConstructorProperty_2; }
	inline void set_ConstructorProperty_2(JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97 * value)
	{
		___ConstructorProperty_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorProperty_2), value);
	}

	inline static int32_t get_offset_of_Presence_3() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075, ___Presence_3)); }
	inline Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9  get_Presence_3() const { return ___Presence_3; }
	inline Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9 * get_address_of_Presence_3() { return &___Presence_3; }
	inline void set_Presence_3(Nullable_1_t1EAC8106ECCF52137638D92C96E7A2A9566D43A9  value)
	{
		___Presence_3 = value;
	}

	inline static int32_t get_offset_of_Value_4() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075, ___Value_4)); }
	inline RuntimeObject * get_Value_4() const { return ___Value_4; }
	inline RuntimeObject ** get_address_of_Value_4() { return &___Value_4; }
	inline void set_Value_4(RuntimeObject * value)
	{
		___Value_4 = value;
		Il2CppCodeGenWriteBarrier((&___Value_4), value);
	}

	inline static int32_t get_offset_of_Used_5() { return static_cast<int32_t>(offsetof(CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075, ___Used_5)); }
	inline bool get_Used_5() const { return ___Used_5; }
	inline bool* get_address_of_Used_5() { return &___Used_5; }
	inline void set_Used_5(bool value)
	{
		___Used_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CREATORPROPERTYCONTEXT_T0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075_H
#ifndef SERIALIZATIONCALLBACK_T7559DC83B02352111F7DF8707658B27E4CBF4F85_H
#define SERIALIZATIONCALLBACK_T7559DC83B02352111F7DF8707658B27E4CBF4F85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.SerializationCallback
struct  SerializationCallback_t7559DC83B02352111F7DF8707658B27E4CBF4F85  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONCALLBACK_T7559DC83B02352111F7DF8707658B27E4CBF4F85_H
#ifndef SERIALIZATIONERRORCALLBACK_T66F9CCCA11F9323880E2D6F4999416B96258F66F_H
#define SERIALIZATIONERRORCALLBACK_T66F9CCCA11F9323880E2D6F4999416B96258F66F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.SerializationErrorCallback
struct  SerializationErrorCallback_t66F9CCCA11F9323880E2D6F4999416B96258F66F  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZATIONERRORCALLBACK_T66F9CCCA11F9323880E2D6F4999416B96258F66F_H
#ifndef JTOKENREADER_T96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11_H
#define JTOKENREADER_T96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenReader
struct  JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11  : public JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC
{
public:
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_root
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____root_15;
	// System.String Newtonsoft.Json.Linq.JTokenReader::_initialPath
	String_t* ____initialPath_16;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_parent
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____parent_17;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenReader::_current
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____current_18;

public:
	inline static int32_t get_offset_of__root_15() { return static_cast<int32_t>(offsetof(JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11, ____root_15)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__root_15() const { return ____root_15; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__root_15() { return &____root_15; }
	inline void set__root_15(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____root_15 = value;
		Il2CppCodeGenWriteBarrier((&____root_15), value);
	}

	inline static int32_t get_offset_of__initialPath_16() { return static_cast<int32_t>(offsetof(JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11, ____initialPath_16)); }
	inline String_t* get__initialPath_16() const { return ____initialPath_16; }
	inline String_t** get_address_of__initialPath_16() { return &____initialPath_16; }
	inline void set__initialPath_16(String_t* value)
	{
		____initialPath_16 = value;
		Il2CppCodeGenWriteBarrier((&____initialPath_16), value);
	}

	inline static int32_t get_offset_of__parent_17() { return static_cast<int32_t>(offsetof(JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11, ____parent_17)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__parent_17() const { return ____parent_17; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__parent_17() { return &____parent_17; }
	inline void set__parent_17(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____parent_17 = value;
		Il2CppCodeGenWriteBarrier((&____parent_17), value);
	}

	inline static int32_t get_offset_of__current_18() { return static_cast<int32_t>(offsetof(JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11, ____current_18)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__current_18() const { return ____current_18; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__current_18() { return &____current_18; }
	inline void set__current_18(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____current_18 = value;
		Il2CppCodeGenWriteBarrier((&____current_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENREADER_T96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11_H
#ifndef JTOKENWRITER_T89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682_H
#define JTOKENWRITER_T89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Linq.JTokenWriter
struct  JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682  : public JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091
{
public:
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_token
	JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * ____token_13;
	// Newtonsoft.Json.Linq.JContainer Newtonsoft.Json.Linq.JTokenWriter::_parent
	JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * ____parent_14;
	// Newtonsoft.Json.Linq.JValue Newtonsoft.Json.Linq.JTokenWriter::_value
	JValue_t80B940E650348637E29E2241958BC52097B1202F * ____value_15;
	// Newtonsoft.Json.Linq.JToken Newtonsoft.Json.Linq.JTokenWriter::_current
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B * ____current_16;

public:
	inline static int32_t get_offset_of__token_13() { return static_cast<int32_t>(offsetof(JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682, ____token_13)); }
	inline JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * get__token_13() const { return ____token_13; }
	inline JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 ** get_address_of__token_13() { return &____token_13; }
	inline void set__token_13(JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * value)
	{
		____token_13 = value;
		Il2CppCodeGenWriteBarrier((&____token_13), value);
	}

	inline static int32_t get_offset_of__parent_14() { return static_cast<int32_t>(offsetof(JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682, ____parent_14)); }
	inline JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * get__parent_14() const { return ____parent_14; }
	inline JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 ** get_address_of__parent_14() { return &____parent_14; }
	inline void set__parent_14(JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48 * value)
	{
		____parent_14 = value;
		Il2CppCodeGenWriteBarrier((&____parent_14), value);
	}

	inline static int32_t get_offset_of__value_15() { return static_cast<int32_t>(offsetof(JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682, ____value_15)); }
	inline JValue_t80B940E650348637E29E2241958BC52097B1202F * get__value_15() const { return ____value_15; }
	inline JValue_t80B940E650348637E29E2241958BC52097B1202F ** get_address_of__value_15() { return &____value_15; }
	inline void set__value_15(JValue_t80B940E650348637E29E2241958BC52097B1202F * value)
	{
		____value_15 = value;
		Il2CppCodeGenWriteBarrier((&____value_15), value);
	}

	inline static int32_t get_offset_of__current_16() { return static_cast<int32_t>(offsetof(JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682, ____current_16)); }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B * get__current_16() const { return ____current_16; }
	inline JToken_t06FA337591EC253506CE2A342A01C19CA990982B ** get_address_of__current_16() { return &____current_16; }
	inline void set__current_16(JToken_t06FA337591EC253506CE2A342A01C19CA990982B * value)
	{
		____current_16 = value;
		Il2CppCodeGenWriteBarrier((&____current_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JTOKENWRITER_T89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682_H
#ifndef JSONARRAYCONTRACT_T6ADAEA1160AD342DBE8C390F26FEA36E911F791B_H
#define JSONARRAYCONTRACT_T6ADAEA1160AD342DBE8C390F26FEA36E911F791B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonArrayContract
struct  JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B  : public JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127
{
public:
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::<CollectionItemType>k__BackingField
	Type_t * ___U3CCollectionItemTypeU3Ek__BackingField_27;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<IsMultidimensionalArray>k__BackingField
	bool ___U3CIsMultidimensionalArrayU3Ek__BackingField_28;
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_29;
	// System.Type Newtonsoft.Json.Serialization.JsonArrayContract::_genericWrapperType
	Type_t * ____genericWrapperType_30;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_genericWrapperCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____genericWrapperCreator_31;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_genericTemporaryCollectionCreator
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ____genericTemporaryCollectionCreator_32;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<IsArray>k__BackingField
	bool ___U3CIsArrayU3Ek__BackingField_33;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_34;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<CanDeserialize>k__BackingField
	bool ___U3CCanDeserializeU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonArrayContract::_parameterizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parameterizedConstructor_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_parameterizedCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____parameterizedCreator_37;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonArrayContract::_overrideCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____overrideCreator_38;
	// System.Boolean Newtonsoft.Json.Serialization.JsonArrayContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ___U3CCollectionItemTypeU3Ek__BackingField_27)); }
	inline Type_t * get_U3CCollectionItemTypeU3Ek__BackingField_27() const { return ___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline Type_t ** get_address_of_U3CCollectionItemTypeU3Ek__BackingField_27() { return &___U3CCollectionItemTypeU3Ek__BackingField_27; }
	inline void set_U3CCollectionItemTypeU3Ek__BackingField_27(Type_t * value)
	{
		___U3CCollectionItemTypeU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCollectionItemTypeU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ___U3CIsMultidimensionalArrayU3Ek__BackingField_28)); }
	inline bool get_U3CIsMultidimensionalArrayU3Ek__BackingField_28() const { return ___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline bool* get_address_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28() { return &___U3CIsMultidimensionalArrayU3Ek__BackingField_28; }
	inline void set_U3CIsMultidimensionalArrayU3Ek__BackingField_28(bool value)
	{
		___U3CIsMultidimensionalArrayU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_29() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____genericCollectionDefinitionType_29)); }
	inline Type_t * get__genericCollectionDefinitionType_29() const { return ____genericCollectionDefinitionType_29; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_29() { return &____genericCollectionDefinitionType_29; }
	inline void set__genericCollectionDefinitionType_29(Type_t * value)
	{
		____genericCollectionDefinitionType_29 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_29), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_30() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____genericWrapperType_30)); }
	inline Type_t * get__genericWrapperType_30() const { return ____genericWrapperType_30; }
	inline Type_t ** get_address_of__genericWrapperType_30() { return &____genericWrapperType_30; }
	inline void set__genericWrapperType_30(Type_t * value)
	{
		____genericWrapperType_30 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_30), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_31() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____genericWrapperCreator_31)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__genericWrapperCreator_31() const { return ____genericWrapperCreator_31; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__genericWrapperCreator_31() { return &____genericWrapperCreator_31; }
	inline void set__genericWrapperCreator_31(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____genericWrapperCreator_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_31), value);
	}

	inline static int32_t get_offset_of__genericTemporaryCollectionCreator_32() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____genericTemporaryCollectionCreator_32)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get__genericTemporaryCollectionCreator_32() const { return ____genericTemporaryCollectionCreator_32; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of__genericTemporaryCollectionCreator_32() { return &____genericTemporaryCollectionCreator_32; }
	inline void set__genericTemporaryCollectionCreator_32(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		____genericTemporaryCollectionCreator_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryCollectionCreator_32), value);
	}

	inline static int32_t get_offset_of_U3CIsArrayU3Ek__BackingField_33() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ___U3CIsArrayU3Ek__BackingField_33)); }
	inline bool get_U3CIsArrayU3Ek__BackingField_33() const { return ___U3CIsArrayU3Ek__BackingField_33; }
	inline bool* get_address_of_U3CIsArrayU3Ek__BackingField_33() { return &___U3CIsArrayU3Ek__BackingField_33; }
	inline void set_U3CIsArrayU3Ek__BackingField_33(bool value)
	{
		___U3CIsArrayU3Ek__BackingField_33 = value;
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ___U3CShouldCreateWrapperU3Ek__BackingField_34)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_34() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_34() { return &___U3CShouldCreateWrapperU3Ek__BackingField_34; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_34(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_34 = value;
	}

	inline static int32_t get_offset_of_U3CCanDeserializeU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ___U3CCanDeserializeU3Ek__BackingField_35)); }
	inline bool get_U3CCanDeserializeU3Ek__BackingField_35() const { return ___U3CCanDeserializeU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CCanDeserializeU3Ek__BackingField_35() { return &___U3CCanDeserializeU3Ek__BackingField_35; }
	inline void set_U3CCanDeserializeU3Ek__BackingField_35(bool value)
	{
		___U3CCanDeserializeU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_37() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____parameterizedCreator_37)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__parameterizedCreator_37() const { return ____parameterizedCreator_37; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__parameterizedCreator_37() { return &____parameterizedCreator_37; }
	inline void set__parameterizedCreator_37(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____parameterizedCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_37), value);
	}

	inline static int32_t get_offset_of__overrideCreator_38() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ____overrideCreator_38)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__overrideCreator_38() const { return ____overrideCreator_38; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__overrideCreator_38() { return &____overrideCreator_38; }
	inline void set__overrideCreator_38(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____overrideCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONARRAYCONTRACT_T6ADAEA1160AD342DBE8C390F26FEA36E911F791B_H
#ifndef JSONDICTIONARYCONTRACT_TC036BB359E283EAF11AFF30E92F8F71A83640EE1_H
#define JSONDICTIONARYCONTRACT_TC036BB359E283EAF11AFF30E92F8F71A83640EE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonDictionaryContract
struct  JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1  : public JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127
{
public:
	// System.Func`2<System.String,System.String> Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryKeyResolver>k__BackingField
	Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * ___U3CDictionaryKeyResolverU3Ek__BackingField_27;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryKeyType>k__BackingField
	Type_t * ___U3CDictionaryKeyTypeU3Ek__BackingField_28;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::<DictionaryValueType>k__BackingField
	Type_t * ___U3CDictionaryValueTypeU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.JsonContract Newtonsoft.Json.Serialization.JsonDictionaryContract::<KeyContract>k__BackingField
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * ___U3CKeyContractU3Ek__BackingField_30;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericCollectionDefinitionType
	Type_t * ____genericCollectionDefinitionType_31;
	// System.Type Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericWrapperType
	Type_t * ____genericWrapperType_32;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericWrapperCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____genericWrapperCreator_33;
	// System.Func`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_genericTemporaryDictionaryCreator
	Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * ____genericTemporaryDictionaryCreator_34;
	// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::<ShouldCreateWrapper>k__BackingField
	bool ___U3CShouldCreateWrapperU3Ek__BackingField_35;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonDictionaryContract::_parameterizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parameterizedConstructor_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_overrideCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____overrideCreator_37;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonDictionaryContract::_parameterizedCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____parameterizedCreator_38;
	// System.Boolean Newtonsoft.Json.Serialization.JsonDictionaryContract::<HasParameterizedCreator>k__BackingField
	bool ___U3CHasParameterizedCreatorU3Ek__BackingField_39;

public:
	inline static int32_t get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ___U3CDictionaryKeyResolverU3Ek__BackingField_27)); }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * get_U3CDictionaryKeyResolverU3Ek__BackingField_27() const { return ___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 ** get_address_of_U3CDictionaryKeyResolverU3Ek__BackingField_27() { return &___U3CDictionaryKeyResolverU3Ek__BackingField_27; }
	inline void set_U3CDictionaryKeyResolverU3Ek__BackingField_27(Func_2_tD9EBF5FAD46B4F561FE5ABE814DC60E6253B8B96 * value)
	{
		___U3CDictionaryKeyResolverU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyResolverU3Ek__BackingField_27), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ___U3CDictionaryKeyTypeU3Ek__BackingField_28)); }
	inline Type_t * get_U3CDictionaryKeyTypeU3Ek__BackingField_28() const { return ___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline Type_t ** get_address_of_U3CDictionaryKeyTypeU3Ek__BackingField_28() { return &___U3CDictionaryKeyTypeU3Ek__BackingField_28; }
	inline void set_U3CDictionaryKeyTypeU3Ek__BackingField_28(Type_t * value)
	{
		___U3CDictionaryKeyTypeU3Ek__BackingField_28 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryKeyTypeU3Ek__BackingField_28), value);
	}

	inline static int32_t get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ___U3CDictionaryValueTypeU3Ek__BackingField_29)); }
	inline Type_t * get_U3CDictionaryValueTypeU3Ek__BackingField_29() const { return ___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline Type_t ** get_address_of_U3CDictionaryValueTypeU3Ek__BackingField_29() { return &___U3CDictionaryValueTypeU3Ek__BackingField_29; }
	inline void set_U3CDictionaryValueTypeU3Ek__BackingField_29(Type_t * value)
	{
		___U3CDictionaryValueTypeU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDictionaryValueTypeU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CKeyContractU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ___U3CKeyContractU3Ek__BackingField_30)); }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * get_U3CKeyContractU3Ek__BackingField_30() const { return ___U3CKeyContractU3Ek__BackingField_30; }
	inline JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 ** get_address_of_U3CKeyContractU3Ek__BackingField_30() { return &___U3CKeyContractU3Ek__BackingField_30; }
	inline void set_U3CKeyContractU3Ek__BackingField_30(JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3 * value)
	{
		___U3CKeyContractU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CKeyContractU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of__genericCollectionDefinitionType_31() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____genericCollectionDefinitionType_31)); }
	inline Type_t * get__genericCollectionDefinitionType_31() const { return ____genericCollectionDefinitionType_31; }
	inline Type_t ** get_address_of__genericCollectionDefinitionType_31() { return &____genericCollectionDefinitionType_31; }
	inline void set__genericCollectionDefinitionType_31(Type_t * value)
	{
		____genericCollectionDefinitionType_31 = value;
		Il2CppCodeGenWriteBarrier((&____genericCollectionDefinitionType_31), value);
	}

	inline static int32_t get_offset_of__genericWrapperType_32() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____genericWrapperType_32)); }
	inline Type_t * get__genericWrapperType_32() const { return ____genericWrapperType_32; }
	inline Type_t ** get_address_of__genericWrapperType_32() { return &____genericWrapperType_32; }
	inline void set__genericWrapperType_32(Type_t * value)
	{
		____genericWrapperType_32 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperType_32), value);
	}

	inline static int32_t get_offset_of__genericWrapperCreator_33() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____genericWrapperCreator_33)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__genericWrapperCreator_33() const { return ____genericWrapperCreator_33; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__genericWrapperCreator_33() { return &____genericWrapperCreator_33; }
	inline void set__genericWrapperCreator_33(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____genericWrapperCreator_33 = value;
		Il2CppCodeGenWriteBarrier((&____genericWrapperCreator_33), value);
	}

	inline static int32_t get_offset_of__genericTemporaryDictionaryCreator_34() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____genericTemporaryDictionaryCreator_34)); }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * get__genericTemporaryDictionaryCreator_34() const { return ____genericTemporaryDictionaryCreator_34; }
	inline Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 ** get_address_of__genericTemporaryDictionaryCreator_34() { return &____genericTemporaryDictionaryCreator_34; }
	inline void set__genericTemporaryDictionaryCreator_34(Func_1_t59BE545225A69AFD7B2056D169D0083051F6D386 * value)
	{
		____genericTemporaryDictionaryCreator_34 = value;
		Il2CppCodeGenWriteBarrier((&____genericTemporaryDictionaryCreator_34), value);
	}

	inline static int32_t get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ___U3CShouldCreateWrapperU3Ek__BackingField_35)); }
	inline bool get_U3CShouldCreateWrapperU3Ek__BackingField_35() const { return ___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline bool* get_address_of_U3CShouldCreateWrapperU3Ek__BackingField_35() { return &___U3CShouldCreateWrapperU3Ek__BackingField_35; }
	inline void set_U3CShouldCreateWrapperU3Ek__BackingField_35(bool value)
	{
		___U3CShouldCreateWrapperU3Ek__BackingField_35 = value;
	}

	inline static int32_t get_offset_of__parameterizedConstructor_36() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____parameterizedConstructor_36)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parameterizedConstructor_36() const { return ____parameterizedConstructor_36; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parameterizedConstructor_36() { return &____parameterizedConstructor_36; }
	inline void set__parameterizedConstructor_36(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parameterizedConstructor_36 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedConstructor_36), value);
	}

	inline static int32_t get_offset_of__overrideCreator_37() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____overrideCreator_37)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__overrideCreator_37() const { return ____overrideCreator_37; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__overrideCreator_37() { return &____overrideCreator_37; }
	inline void set__overrideCreator_37(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____overrideCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_37), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_38() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ____parameterizedCreator_38)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__parameterizedCreator_38() const { return ____parameterizedCreator_38; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__parameterizedCreator_38() { return &____parameterizedCreator_38; }
	inline void set__parameterizedCreator_38(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____parameterizedCreator_38 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_38), value);
	}

	inline static int32_t get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return static_cast<int32_t>(offsetof(JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1, ___U3CHasParameterizedCreatorU3Ek__BackingField_39)); }
	inline bool get_U3CHasParameterizedCreatorU3Ek__BackingField_39() const { return ___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline bool* get_address_of_U3CHasParameterizedCreatorU3Ek__BackingField_39() { return &___U3CHasParameterizedCreatorU3Ek__BackingField_39; }
	inline void set_U3CHasParameterizedCreatorU3Ek__BackingField_39(bool value)
	{
		___U3CHasParameterizedCreatorU3Ek__BackingField_39 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONDICTIONARYCONTRACT_TC036BB359E283EAF11AFF30E92F8F71A83640EE1_H
#ifndef JSONISERIALIZABLECONTRACT_T3D30C47502CCAC10C399BBA90031947AD99CAF7F_H
#define JSONISERIALIZABLECONTRACT_T3D30C47502CCAC10C399BBA90031947AD99CAF7F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonISerializableContract
struct  JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F  : public JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127
{
public:
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonISerializableContract::<ISerializableCreator>k__BackingField
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ___U3CISerializableCreatorU3Ek__BackingField_27;

public:
	inline static int32_t get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F, ___U3CISerializableCreatorU3Ek__BackingField_27)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get_U3CISerializableCreatorU3Ek__BackingField_27() const { return ___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of_U3CISerializableCreatorU3Ek__BackingField_27() { return &___U3CISerializableCreatorU3Ek__BackingField_27; }
	inline void set_U3CISerializableCreatorU3Ek__BackingField_27(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		___U3CISerializableCreatorU3Ek__BackingField_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CISerializableCreatorU3Ek__BackingField_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONISERIALIZABLECONTRACT_T3D30C47502CCAC10C399BBA90031947AD99CAF7F_H
#ifndef JSONOBJECTCONTRACT_T4112253510DAEFD8F4E2E3B5DB6B65040FAFD333_H
#define JSONOBJECTCONTRACT_T4112253510DAEFD8F4E2E3B5DB6B65040FAFD333_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonObjectContract
struct  JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333  : public JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127
{
public:
	// Newtonsoft.Json.MemberSerialization Newtonsoft.Json.Serialization.JsonObjectContract::<MemberSerialization>k__BackingField
	int32_t ___U3CMemberSerializationU3Ek__BackingField_27;
	// System.Nullable`1<Newtonsoft.Json.Required> Newtonsoft.Json.Serialization.JsonObjectContract::<ItemRequired>k__BackingField
	Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  ___U3CItemRequiredU3Ek__BackingField_28;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::<Properties>k__BackingField
	JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF * ___U3CPropertiesU3Ek__BackingField_29;
	// Newtonsoft.Json.Serialization.ExtensionDataSetter Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataSetter>k__BackingField
	ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0 * ___U3CExtensionDataSetterU3Ek__BackingField_30;
	// Newtonsoft.Json.Serialization.ExtensionDataGetter Newtonsoft.Json.Serialization.JsonObjectContract::<ExtensionDataGetter>k__BackingField
	ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853 * ___U3CExtensionDataGetterU3Ek__BackingField_31;
	// System.Boolean Newtonsoft.Json.Serialization.JsonObjectContract::ExtensionDataIsJToken
	bool ___ExtensionDataIsJToken_32;
	// System.Nullable`1<System.Boolean> Newtonsoft.Json.Serialization.JsonObjectContract::_hasRequiredOrDefaultValueProperties
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ____hasRequiredOrDefaultValueProperties_33;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonObjectContract::_parametrizedConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____parametrizedConstructor_34;
	// System.Reflection.ConstructorInfo Newtonsoft.Json.Serialization.JsonObjectContract::_overrideConstructor
	ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * ____overrideConstructor_35;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::_overrideCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____overrideCreator_36;
	// Newtonsoft.Json.Serialization.ObjectConstructor`1<System.Object> Newtonsoft.Json.Serialization.JsonObjectContract::_parameterizedCreator
	ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * ____parameterizedCreator_37;
	// Newtonsoft.Json.Serialization.JsonPropertyCollection Newtonsoft.Json.Serialization.JsonObjectContract::_creatorParameters
	JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF * ____creatorParameters_38;
	// System.Type Newtonsoft.Json.Serialization.JsonObjectContract::_extensionDataValueType
	Type_t * ____extensionDataValueType_39;

public:
	inline static int32_t get_offset_of_U3CMemberSerializationU3Ek__BackingField_27() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ___U3CMemberSerializationU3Ek__BackingField_27)); }
	inline int32_t get_U3CMemberSerializationU3Ek__BackingField_27() const { return ___U3CMemberSerializationU3Ek__BackingField_27; }
	inline int32_t* get_address_of_U3CMemberSerializationU3Ek__BackingField_27() { return &___U3CMemberSerializationU3Ek__BackingField_27; }
	inline void set_U3CMemberSerializationU3Ek__BackingField_27(int32_t value)
	{
		___U3CMemberSerializationU3Ek__BackingField_27 = value;
	}

	inline static int32_t get_offset_of_U3CItemRequiredU3Ek__BackingField_28() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ___U3CItemRequiredU3Ek__BackingField_28)); }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  get_U3CItemRequiredU3Ek__BackingField_28() const { return ___U3CItemRequiredU3Ek__BackingField_28; }
	inline Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D * get_address_of_U3CItemRequiredU3Ek__BackingField_28() { return &___U3CItemRequiredU3Ek__BackingField_28; }
	inline void set_U3CItemRequiredU3Ek__BackingField_28(Nullable_1_t77B4FDCF98614C924B1310C1941E0897738EA65D  value)
	{
		___U3CItemRequiredU3Ek__BackingField_28 = value;
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_29() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ___U3CPropertiesU3Ek__BackingField_29)); }
	inline JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF * get_U3CPropertiesU3Ek__BackingField_29() const { return ___U3CPropertiesU3Ek__BackingField_29; }
	inline JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF ** get_address_of_U3CPropertiesU3Ek__BackingField_29() { return &___U3CPropertiesU3Ek__BackingField_29; }
	inline void set_U3CPropertiesU3Ek__BackingField_29(JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF * value)
	{
		___U3CPropertiesU3Ek__BackingField_29 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_29), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ___U3CExtensionDataSetterU3Ek__BackingField_30)); }
	inline ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0 * get_U3CExtensionDataSetterU3Ek__BackingField_30() const { return ___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0 ** get_address_of_U3CExtensionDataSetterU3Ek__BackingField_30() { return &___U3CExtensionDataSetterU3Ek__BackingField_30; }
	inline void set_U3CExtensionDataSetterU3Ek__BackingField_30(ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0 * value)
	{
		___U3CExtensionDataSetterU3Ek__BackingField_30 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataSetterU3Ek__BackingField_30), value);
	}

	inline static int32_t get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ___U3CExtensionDataGetterU3Ek__BackingField_31)); }
	inline ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853 * get_U3CExtensionDataGetterU3Ek__BackingField_31() const { return ___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853 ** get_address_of_U3CExtensionDataGetterU3Ek__BackingField_31() { return &___U3CExtensionDataGetterU3Ek__BackingField_31; }
	inline void set_U3CExtensionDataGetterU3Ek__BackingField_31(ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853 * value)
	{
		___U3CExtensionDataGetterU3Ek__BackingField_31 = value;
		Il2CppCodeGenWriteBarrier((&___U3CExtensionDataGetterU3Ek__BackingField_31), value);
	}

	inline static int32_t get_offset_of_ExtensionDataIsJToken_32() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ___ExtensionDataIsJToken_32)); }
	inline bool get_ExtensionDataIsJToken_32() const { return ___ExtensionDataIsJToken_32; }
	inline bool* get_address_of_ExtensionDataIsJToken_32() { return &___ExtensionDataIsJToken_32; }
	inline void set_ExtensionDataIsJToken_32(bool value)
	{
		___ExtensionDataIsJToken_32 = value;
	}

	inline static int32_t get_offset_of__hasRequiredOrDefaultValueProperties_33() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____hasRequiredOrDefaultValueProperties_33)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get__hasRequiredOrDefaultValueProperties_33() const { return ____hasRequiredOrDefaultValueProperties_33; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of__hasRequiredOrDefaultValueProperties_33() { return &____hasRequiredOrDefaultValueProperties_33; }
	inline void set__hasRequiredOrDefaultValueProperties_33(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		____hasRequiredOrDefaultValueProperties_33 = value;
	}

	inline static int32_t get_offset_of__parametrizedConstructor_34() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____parametrizedConstructor_34)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__parametrizedConstructor_34() const { return ____parametrizedConstructor_34; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__parametrizedConstructor_34() { return &____parametrizedConstructor_34; }
	inline void set__parametrizedConstructor_34(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____parametrizedConstructor_34 = value;
		Il2CppCodeGenWriteBarrier((&____parametrizedConstructor_34), value);
	}

	inline static int32_t get_offset_of__overrideConstructor_35() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____overrideConstructor_35)); }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * get__overrideConstructor_35() const { return ____overrideConstructor_35; }
	inline ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF ** get_address_of__overrideConstructor_35() { return &____overrideConstructor_35; }
	inline void set__overrideConstructor_35(ConstructorInfo_t9CB51BFC178DF1CBCA5FD16B2D58229618F23EFF * value)
	{
		____overrideConstructor_35 = value;
		Il2CppCodeGenWriteBarrier((&____overrideConstructor_35), value);
	}

	inline static int32_t get_offset_of__overrideCreator_36() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____overrideCreator_36)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__overrideCreator_36() const { return ____overrideCreator_36; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__overrideCreator_36() { return &____overrideCreator_36; }
	inline void set__overrideCreator_36(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____overrideCreator_36 = value;
		Il2CppCodeGenWriteBarrier((&____overrideCreator_36), value);
	}

	inline static int32_t get_offset_of__parameterizedCreator_37() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____parameterizedCreator_37)); }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * get__parameterizedCreator_37() const { return ____parameterizedCreator_37; }
	inline ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 ** get_address_of__parameterizedCreator_37() { return &____parameterizedCreator_37; }
	inline void set__parameterizedCreator_37(ObjectConstructor_1_tAE916A6C4383623EC9EC57B3929946628FE1A0F8 * value)
	{
		____parameterizedCreator_37 = value;
		Il2CppCodeGenWriteBarrier((&____parameterizedCreator_37), value);
	}

	inline static int32_t get_offset_of__creatorParameters_38() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____creatorParameters_38)); }
	inline JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF * get__creatorParameters_38() const { return ____creatorParameters_38; }
	inline JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF ** get_address_of__creatorParameters_38() { return &____creatorParameters_38; }
	inline void set__creatorParameters_38(JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF * value)
	{
		____creatorParameters_38 = value;
		Il2CppCodeGenWriteBarrier((&____creatorParameters_38), value);
	}

	inline static int32_t get_offset_of__extensionDataValueType_39() { return static_cast<int32_t>(offsetof(JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333, ____extensionDataValueType_39)); }
	inline Type_t * get__extensionDataValueType_39() const { return ____extensionDataValueType_39; }
	inline Type_t ** get_address_of__extensionDataValueType_39() { return &____extensionDataValueType_39; }
	inline void set__extensionDataValueType_39(Type_t * value)
	{
		____extensionDataValueType_39 = value;
		Il2CppCodeGenWriteBarrier((&____extensionDataValueType_39), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECTCONTRACT_T4112253510DAEFD8F4E2E3B5DB6B65040FAFD333_H
#ifndef JSONSERIALIZERPROXY_T389025E53712D560A4FA65257413EA8A11D868AA_H
#define JSONSERIALIZERPROXY_T389025E53712D560A4FA65257413EA8A11D868AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonSerializerProxy
struct  JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA  : public JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB
{
public:
	// Newtonsoft.Json.Serialization.JsonSerializerInternalReader Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerReader
	JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA * ____serializerReader_31;
	// Newtonsoft.Json.Serialization.JsonSerializerInternalWriter Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializerWriter
	JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC * ____serializerWriter_32;
	// Newtonsoft.Json.JsonSerializer Newtonsoft.Json.Serialization.JsonSerializerProxy::_serializer
	JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB * ____serializer_33;

public:
	inline static int32_t get_offset_of__serializerReader_31() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA, ____serializerReader_31)); }
	inline JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA * get__serializerReader_31() const { return ____serializerReader_31; }
	inline JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA ** get_address_of__serializerReader_31() { return &____serializerReader_31; }
	inline void set__serializerReader_31(JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA * value)
	{
		____serializerReader_31 = value;
		Il2CppCodeGenWriteBarrier((&____serializerReader_31), value);
	}

	inline static int32_t get_offset_of__serializerWriter_32() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA, ____serializerWriter_32)); }
	inline JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC * get__serializerWriter_32() const { return ____serializerWriter_32; }
	inline JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC ** get_address_of__serializerWriter_32() { return &____serializerWriter_32; }
	inline void set__serializerWriter_32(JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC * value)
	{
		____serializerWriter_32 = value;
		Il2CppCodeGenWriteBarrier((&____serializerWriter_32), value);
	}

	inline static int32_t get_offset_of__serializer_33() { return static_cast<int32_t>(offsetof(JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA, ____serializer_33)); }
	inline JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB * get__serializer_33() const { return ____serializer_33; }
	inline JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB ** get_address_of__serializer_33() { return &____serializer_33; }
	inline void set__serializer_33(JsonSerializer_t6B495C1A06DBBE7EF4C65E08A4BA0D60BE9849BB * value)
	{
		____serializer_33 = value;
		Il2CppCodeGenWriteBarrier((&____serializer_33), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZERPROXY_T389025E53712D560A4FA65257413EA8A11D868AA_H
#ifndef JSONSTRINGCONTRACT_TBD5A8A5530B0C67BAB2C365391405B988AC623C8_H
#define JSONSTRINGCONTRACT_TBD5A8A5530B0C67BAB2C365391405B988AC623C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.JsonStringContract
struct  JsonStringContract_tBD5A8A5530B0C67BAB2C365391405B988AC623C8  : public JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSTRINGCONTRACT_TBD5A8A5530B0C67BAB2C365391405B988AC623C8_H
#ifndef TRACEJSONREADER_TFBF7538BB69CF731AF9D98B02D3985442422D4AA_H
#define TRACEJSONREADER_TFBF7538BB69CF731AF9D98B02D3985442422D4AA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.TraceJsonReader
struct  TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA  : public JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC
{
public:
	// Newtonsoft.Json.JsonReader Newtonsoft.Json.Serialization.TraceJsonReader::_innerReader
	JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * ____innerReader_15;
	// Newtonsoft.Json.JsonTextWriter Newtonsoft.Json.Serialization.TraceJsonReader::_textWriter
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 * ____textWriter_16;
	// System.IO.StringWriter Newtonsoft.Json.Serialization.TraceJsonReader::_sw
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * ____sw_17;

public:
	inline static int32_t get_offset_of__innerReader_15() { return static_cast<int32_t>(offsetof(TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA, ____innerReader_15)); }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * get__innerReader_15() const { return ____innerReader_15; }
	inline JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC ** get_address_of__innerReader_15() { return &____innerReader_15; }
	inline void set__innerReader_15(JsonReader_tF29A2AB439002FDEFB9592AA3147F02ADB3841DC * value)
	{
		____innerReader_15 = value;
		Il2CppCodeGenWriteBarrier((&____innerReader_15), value);
	}

	inline static int32_t get_offset_of__textWriter_16() { return static_cast<int32_t>(offsetof(TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA, ____textWriter_16)); }
	inline JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 * get__textWriter_16() const { return ____textWriter_16; }
	inline JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 ** get_address_of__textWriter_16() { return &____textWriter_16; }
	inline void set__textWriter_16(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 * value)
	{
		____textWriter_16 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_16), value);
	}

	inline static int32_t get_offset_of__sw_17() { return static_cast<int32_t>(offsetof(TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA, ____sw_17)); }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * get__sw_17() const { return ____sw_17; }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 ** get_address_of__sw_17() { return &____sw_17; }
	inline void set__sw_17(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * value)
	{
		____sw_17 = value;
		Il2CppCodeGenWriteBarrier((&____sw_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONREADER_TFBF7538BB69CF731AF9D98B02D3985442422D4AA_H
#ifndef TRACEJSONWRITER_T0C9D421E3FBFA336458B681105F10B57AA81855B_H
#define TRACEJSONWRITER_T0C9D421E3FBFA336458B681105F10B57AA81855B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Newtonsoft.Json.Serialization.TraceJsonWriter
struct  TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B  : public JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091
{
public:
	// Newtonsoft.Json.JsonWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_innerWriter
	JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091 * ____innerWriter_13;
	// Newtonsoft.Json.JsonTextWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_textWriter
	JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 * ____textWriter_14;
	// System.IO.StringWriter Newtonsoft.Json.Serialization.TraceJsonWriter::_sw
	StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * ____sw_15;

public:
	inline static int32_t get_offset_of__innerWriter_13() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B, ____innerWriter_13)); }
	inline JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091 * get__innerWriter_13() const { return ____innerWriter_13; }
	inline JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091 ** get_address_of__innerWriter_13() { return &____innerWriter_13; }
	inline void set__innerWriter_13(JsonWriter_t4CF8C6EC696955A38B904D71B2AB1C6ED6155091 * value)
	{
		____innerWriter_13 = value;
		Il2CppCodeGenWriteBarrier((&____innerWriter_13), value);
	}

	inline static int32_t get_offset_of__textWriter_14() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B, ____textWriter_14)); }
	inline JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 * get__textWriter_14() const { return ____textWriter_14; }
	inline JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 ** get_address_of__textWriter_14() { return &____textWriter_14; }
	inline void set__textWriter_14(JsonTextWriter_t7CF50AEC1B1511909ABCDC7542E0D34DB7922225 * value)
	{
		____textWriter_14 = value;
		Il2CppCodeGenWriteBarrier((&____textWriter_14), value);
	}

	inline static int32_t get_offset_of__sw_15() { return static_cast<int32_t>(offsetof(TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B, ____sw_15)); }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * get__sw_15() const { return ____sw_15; }
	inline StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 ** get_address_of__sw_15() { return &____sw_15; }
	inline void set__sw_15(StringWriter_t194EF1526E072B93984370042AA80926C2EB6139 * value)
	{
		____sw_15 = value;
		Il2CppCodeGenWriteBarrier((&____sw_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRACEJSONWRITER_T0C9D421E3FBFA336458B681105F10B57AA81855B_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5100 = { sizeof (U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03), -1, sizeof(U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5100[5] = 
{
	U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
	U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields::get_offset_of_U3CU3E9__29_0_2(),
	U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
	U3CU3Ec_t111CB085677BC83B5C93876C251A5017BC0B3E03_StaticFields::get_offset_of_U3CU3E9__39_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5101 = { sizeof (U3CU3Ec__DisplayClass42_0_t2FC85827C1F79DAA5523C47AA9ABF809B95B780E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5101[1] = 
{
	U3CU3Ec__DisplayClass42_0_t2FC85827C1F79DAA5523C47AA9ABF809B95B780E::get_offset_of_subTypeProperty_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5102 = { sizeof (StringUtils_t55F456118274076335BAD29C3915865F4E1A5A79), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5103 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5103[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5104 = { sizeof (TypeExtensions_t18C41DFD462FE5D51523C9DC26F889FE0F8E3429), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5105 = { sizeof (ValidationUtils_t8D1A362D70752CDE5F929271E80723D846FECF10), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5106 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5107 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5108 = { sizeof (JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5108[6] = 
{
	JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127::get_offset_of__itemContract_21(),
	JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127::get_offset_of__finalItemContract_22(),
	JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127::get_offset_of_U3CItemConverterU3Ek__BackingField_23(),
	JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_24(),
	JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_25(),
	JsonContainerContract_t159D64F86F765005FA9031707C63F46AB0ABD127::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_26(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5109 = { sizeof (NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5109[2] = 
{
	NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19::get_offset_of_U3CProcessDictionaryKeysU3Ek__BackingField_0(),
	NamingStrategy_tECACBFDC2C898D48D76511B4C22E07CB86713F19::get_offset_of_U3COverrideSpecifiedNamesU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5110 = { sizeof (ReflectionAttributeProvider_tC4C4111A99124372494B63DCC0D27B78329575F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5110[1] = 
{
	ReflectionAttributeProvider_tC4C4111A99124372494B63DCC0D27B78329575F3::get_offset_of__attributeProvider_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5111 = { sizeof (TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5111[3] = 
{
	TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA::get_offset_of__innerReader_15(),
	TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA::get_offset_of__textWriter_16(),
	TraceJsonReader_tFBF7538BB69CF731AF9D98B02D3985442422D4AA::get_offset_of__sw_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5112 = { sizeof (TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5112[3] = 
{
	TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B::get_offset_of__innerWriter_13(),
	TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B::get_offset_of__textWriter_14(),
	TraceJsonWriter_t0C9D421E3FBFA336458B681105F10B57AA81855B::get_offset_of__sw_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5113 = { sizeof (JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5113[3] = 
{
	JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE::get_offset_of__reader_0(),
	JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE::get_offset_of__contract_1(),
	JsonFormatterConverter_t7C1ACD8B03CDAE9E45CCED21322403D7594ACFBE::get_offset_of__member_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5114 = { sizeof (JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5114[1] = 
{
	JsonISerializableContract_t3D30C47502CCAC10C399BBA90031947AD99CAF7F::get_offset_of_U3CISerializableCreatorU3Ek__BackingField_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5115 = { sizeof (JsonLinqContract_t9FB8F2255FDD47ADFF6C249353BE5713931FC897), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5116 = { sizeof (JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702), -1, sizeof(JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5116[2] = 
{
	JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702::get_offset_of_U3CTypeCodeU3Ek__BackingField_21(),
	JsonPrimitiveContract_tD7663FB786D31F278ED1E2DF3237CEF558288702_StaticFields::get_offset_of_ReadTypeMap_22(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5117 = { sizeof (DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5117[3] = 
{
	DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6::get_offset_of__memberInfo_0(),
	DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6::get_offset_of__getter_1(),
	DynamicValueProvider_tC9AF7E7EE4A4F36BB96870CF62D41EF7E1F50FC6::get_offset_of__setter_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5118 = { sizeof (ErrorEventArgs_t099CA39B0A9B3D06DE3F9C0599366AD502B61C05), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5118[2] = 
{
	ErrorEventArgs_t099CA39B0A9B3D06DE3F9C0599366AD502B61C05::get_offset_of_U3CCurrentObjectU3Ek__BackingField_1(),
	ErrorEventArgs_t099CA39B0A9B3D06DE3F9C0599366AD502B61C05::get_offset_of_U3CErrorContextU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5119 = { sizeof (DefaultReferenceResolver_tF31AA9DCD47128442345DAD6F515A4175D50CC23), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5119[1] = 
{
	DefaultReferenceResolver_tF31AA9DCD47128442345DAD6F515A4175D50CC23::get_offset_of__referenceCount_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5120 = { sizeof (ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5120[2] = 
{
	ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD::get_offset_of__resolverType_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ResolverContractKey_t103816732D945CB015D10DBE1F538DF8CBD4CFAD::get_offset_of__contractType_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5121 = { sizeof (DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5121[2] = 
{
	DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81::get_offset_of_ContractCache_0(),
	DefaultContractResolverState_tDCE9D5776838E8C023A19E0828F7D6D470556A81::get_offset_of_NameTable_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5122 = { sizeof (DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0), -1, sizeof(DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5122[11] = 
{
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields::get_offset_of__instance_0(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields::get_offset_of_BuiltInConverters_1(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields::get_offset_of_TypeContractCacheLock_2(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0_StaticFields::get_offset_of__sharedState_3(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of__instanceState_4(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of__sharedCache_5(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of_U3CDefaultMembersSearchFlagsU3Ek__BackingField_6(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of_U3CSerializeCompilerGeneratedMembersU3Ek__BackingField_7(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of_U3CIgnoreSerializableInterfaceU3Ek__BackingField_8(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of_U3CIgnoreSerializableAttributeU3Ek__BackingField_9(),
	DefaultContractResolver_tFC8ABD25FF893486E46EA71B8D5C846F1C4ACBE0::get_offset_of_U3CNamingStrategyU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5123 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5123[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5124 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5124[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5125 = { sizeof (U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A), -1, sizeof(U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5125[7] = 
{
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9__34_0_1(),
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9__34_1_2(),
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9__37_0_3(),
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9__37_1_4(),
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9__40_0_5(),
	U3CU3Ec_t7E2010EF0EE9F512FBFE7D7F2AECF871A7E0139A_StaticFields::get_offset_of_U3CU3E9__64_0_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5126 = { sizeof (U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5126[2] = 
{
	U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9::get_offset_of_getExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass38_0_t01A9E51479162C1C6A88684134FA0B00A389FAD9::get_offset_of_member_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5127 = { sizeof (U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5127[4] = 
{
	U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E::get_offset_of_setExtensionDataDictionary_0(),
	U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E::get_offset_of_createExtensionDataDictionary_1(),
	U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E::get_offset_of_setExtensionDataDictionaryValue_2(),
	U3CU3Ec__DisplayClass38_1_t7DDC3209F5744DF0ADC3FFA92B42016008F3685E::get_offset_of_CSU24U3CU3E8__locals1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5128 = { sizeof (U3CU3Ec__DisplayClass38_2_t7820E34E56FECDC1033E4553C02296C3D57C9547), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5128[2] = 
{
	U3CU3Ec__DisplayClass38_2_t7820E34E56FECDC1033E4553C02296C3D57C9547::get_offset_of_createEnumerableWrapper_0(),
	U3CU3Ec__DisplayClass38_2_t7820E34E56FECDC1033E4553C02296C3D57C9547::get_offset_of_CSU24U3CU3E8__locals2_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5129 = { sizeof (U3CU3Ec__DisplayClass52_0_t913E9808AA5ECF6123EF1DB918E477E5E16B2967), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5129[1] = 
{
	U3CU3Ec__DisplayClass52_0_t913E9808AA5ECF6123EF1DB918E477E5E16B2967::get_offset_of_namingStrategy_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5130 = { sizeof (U3CU3Ec__DisplayClass68_0_tB38B93210F3F0A3FC10E6453326887E9CEEFE994), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5130[1] = 
{
	U3CU3Ec__DisplayClass68_0_tB38B93210F3F0A3FC10E6453326887E9CEEFE994::get_offset_of_shouldSerializeCall_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5131 = { sizeof (U3CU3Ec__DisplayClass69_0_t60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5131[1] = 
{
	U3CU3Ec__DisplayClass69_0_t60C3E19B4FFE6060F8B635FBCD8FBCFA81268A30::get_offset_of_specifiedPropertyGet_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5132 = { sizeof (DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956), -1, sizeof(DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5132[2] = 
{
	DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956_StaticFields::get_offset_of_Instance_0(),
	DefaultSerializationBinder_tC19C5DC7F736EF693DF8424D7EF1A8071A46E956::get_offset_of__typeCache_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5133 = { sizeof (TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9)+ sizeof (RuntimeObject), sizeof(TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable5133[2] = 
{
	TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9::get_offset_of_AssemblyName_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	TypeNameKey_t7C679BD7C266C37EA2B068F889AC5038DF5093F9::get_offset_of_TypeName_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5134 = { sizeof (ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5134[6] = 
{
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C::get_offset_of_U3CTracedU3Ek__BackingField_0(),
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C::get_offset_of_U3CErrorU3Ek__BackingField_1(),
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C::get_offset_of_U3COriginalObjectU3Ek__BackingField_2(),
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C::get_offset_of_U3CMemberU3Ek__BackingField_3(),
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C::get_offset_of_U3CPathU3Ek__BackingField_4(),
	ErrorContext_t82EE08F672CC1074063B6C6AA200EFFA96E2BF8C::get_offset_of_U3CHandledU3Ek__BackingField_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5135 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5136 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5137 = { sizeof (JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5137[13] = 
{
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of_U3CCollectionItemTypeU3Ek__BackingField_27(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of_U3CIsMultidimensionalArrayU3Ek__BackingField_28(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__genericCollectionDefinitionType_29(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__genericWrapperType_30(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__genericWrapperCreator_31(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__genericTemporaryCollectionCreator_32(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of_U3CIsArrayU3Ek__BackingField_33(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_34(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of_U3CCanDeserializeU3Ek__BackingField_35(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__parameterizedConstructor_36(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__parameterizedCreator_37(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of__overrideCreator_38(),
	JsonArrayContract_t6ADAEA1160AD342DBE8C390F26FEA36E911F791B::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5138 = { sizeof (JsonContractType_tC348EADEB87C8F713622F349D659D75FF07D862F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5138[10] = 
{
	JsonContractType_tC348EADEB87C8F713622F349D659D75FF07D862F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5139 = { sizeof (SerializationCallback_t7559DC83B02352111F7DF8707658B27E4CBF4F85), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5140 = { sizeof (SerializationErrorCallback_t66F9CCCA11F9323880E2D6F4999416B96258F66F), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5141 = { sizeof (ExtensionDataSetter_t1C37A4BDDF77C77E733EE128E6A0E9B5ACD631E0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5142 = { sizeof (ExtensionDataGetter_t552EB770A47F66A53412918802CCC988B223E853), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5143 = { sizeof (JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5143[21] = 
{
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_IsNullable_0(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_IsConvertable_1(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_IsEnum_2(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_NonNullableUnderlyingType_3(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_InternalReadType_4(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_ContractType_5(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_IsReadOnlyOrFixedSize_6(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_IsSealed_7(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_IsInstantiable_8(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of__onDeserializedCallbacks_9(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of__onDeserializingCallbacks_10(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of__onSerializedCallbacks_11(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of__onSerializingCallbacks_12(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of__onErrorCallbacks_13(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of__createdType_14(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_U3CUnderlyingTypeU3Ek__BackingField_15(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_U3CIsReferenceU3Ek__BackingField_16(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_U3CConverterU3Ek__BackingField_17(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_U3CInternalConverterU3Ek__BackingField_18(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_U3CDefaultCreatorU3Ek__BackingField_19(),
	JsonContract_t24F8047299CC6DFDDDB0EBFB116A8A281264BFA3::get_offset_of_U3CDefaultCreatorNonPublicU3Ek__BackingField_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5144 = { sizeof (U3CU3Ec__DisplayClass73_0_t313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5144[1] = 
{
	U3CU3Ec__DisplayClass73_0_t313C43C46C08B6886AE9B0CED0A8CDCFD61AEA6F::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5145 = { sizeof (U3CU3Ec__DisplayClass74_0_t6A58FBB8C433D921DF0DEB2DD2F95857C8B21265), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5145[1] = 
{
	U3CU3Ec__DisplayClass74_0_t6A58FBB8C433D921DF0DEB2DD2F95857C8B21265::get_offset_of_callbackMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5146 = { sizeof (JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5146[13] = 
{
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of_U3CDictionaryKeyResolverU3Ek__BackingField_27(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of_U3CDictionaryKeyTypeU3Ek__BackingField_28(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of_U3CDictionaryValueTypeU3Ek__BackingField_29(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of_U3CKeyContractU3Ek__BackingField_30(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__genericCollectionDefinitionType_31(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__genericWrapperType_32(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__genericWrapperCreator_33(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__genericTemporaryDictionaryCreator_34(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of_U3CShouldCreateWrapperU3Ek__BackingField_35(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__parameterizedConstructor_36(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__overrideCreator_37(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of__parameterizedCreator_38(),
	JsonDictionaryContract_tC036BB359E283EAF11AFF30E92F8F71A83640EE1::get_offset_of_U3CHasParameterizedCreatorU3Ek__BackingField_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5147 = { sizeof (JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5147[33] = 
{
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__required_0(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__hasExplicitDefaultValue_1(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__defaultValue_2(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__hasGeneratedDefaultValue_3(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__propertyName_4(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__skipPropertyNameEscape_5(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of__propertyType_6(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CPropertyContractU3Ek__BackingField_7(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CDeclaringTypeU3Ek__BackingField_8(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3COrderU3Ek__BackingField_9(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CUnderlyingNameU3Ek__BackingField_10(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CValueProviderU3Ek__BackingField_11(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CAttributeProviderU3Ek__BackingField_12(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CConverterU3Ek__BackingField_13(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CMemberConverterU3Ek__BackingField_14(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CIgnoredU3Ek__BackingField_15(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CReadableU3Ek__BackingField_16(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CWritableU3Ek__BackingField_17(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CHasMemberAttributeU3Ek__BackingField_18(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CIsReferenceU3Ek__BackingField_19(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CNullValueHandlingU3Ek__BackingField_20(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CDefaultValueHandlingU3Ek__BackingField_21(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CReferenceLoopHandlingU3Ek__BackingField_22(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CObjectCreationHandlingU3Ek__BackingField_23(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CTypeNameHandlingU3Ek__BackingField_24(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CShouldSerializeU3Ek__BackingField_25(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CShouldDeserializeU3Ek__BackingField_26(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CGetIsSpecifiedU3Ek__BackingField_27(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CSetIsSpecifiedU3Ek__BackingField_28(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CItemConverterU3Ek__BackingField_29(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CItemIsReferenceU3Ek__BackingField_30(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CItemTypeNameHandlingU3Ek__BackingField_31(),
	JsonProperty_tAAB286C386C8EDAB84CB6F0966E598677202DA97::get_offset_of_U3CItemReferenceLoopHandlingU3Ek__BackingField_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5148 = { sizeof (JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5148[2] = 
{
	JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF::get_offset_of__type_6(),
	JsonPropertyCollection_tC8702D66F2EE1F7E7DC5B6379FC55006296CEFDF::get_offset_of__list_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5149 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5150 = { sizeof (JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5150[13] = 
{
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of_U3CMemberSerializationU3Ek__BackingField_27(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of_U3CItemRequiredU3Ek__BackingField_28(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of_U3CPropertiesU3Ek__BackingField_29(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of_U3CExtensionDataSetterU3Ek__BackingField_30(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of_U3CExtensionDataGetterU3Ek__BackingField_31(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of_ExtensionDataIsJToken_32(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__hasRequiredOrDefaultValueProperties_33(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__parametrizedConstructor_34(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__overrideConstructor_35(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__overrideCreator_36(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__parameterizedCreator_37(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__creatorParameters_38(),
	JsonObjectContract_t4112253510DAEFD8F4E2E3B5DB6B65040FAFD333::get_offset_of__extensionDataValueType_39(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5151 = { sizeof (JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5151[5] = 
{
	JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780::get_offset_of__currentErrorContext_0(),
	JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780::get_offset_of__mappings_1(),
	JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780::get_offset_of_Serializer_2(),
	JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780::get_offset_of_TraceWriter_3(),
	JsonSerializerInternalBase_t36137479698478C0B377AE13D3CE73961CB3B780::get_offset_of_InternalSerializer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5152 = { sizeof (ReferenceEqualsEqualityComparer_t0D48B8904CB037F121AB933D92ACF97CD1427318), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5153 = { sizeof (JsonSerializerInternalReader_t6A35CE905E9FAEDA1E9A0B0C14E61888E3B0BAFA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5154 = { sizeof (PropertyPresence_tE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5154[4] = 
{
	PropertyPresence_tE2AFA2EC29097E6DBCFEA26B2B4809B61E2B9218::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5155 = { sizeof (CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5155[6] = 
{
	CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075::get_offset_of_Name_0(),
	CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075::get_offset_of_Property_1(),
	CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075::get_offset_of_ConstructorProperty_2(),
	CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075::get_offset_of_Presence_3(),
	CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075::get_offset_of_Value_4(),
	CreatorPropertyContext_t0A79280BA1F52BB5AFCBD2A8FEAFDD815C2A7075::get_offset_of_Used_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5156 = { sizeof (U3CU3Ec__DisplayClass36_0_t2486B96EBEB5BD6EE795D93545DE89A31AACDA4D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5156[1] = 
{
	U3CU3Ec__DisplayClass36_0_t2486B96EBEB5BD6EE795D93545DE89A31AACDA4D::get_offset_of_property_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5157 = { sizeof (U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877), -1, sizeof(U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5157[5] = 
{
	U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields::get_offset_of_U3CU3E9__36_0_1(),
	U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields::get_offset_of_U3CU3E9__36_2_2(),
	U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields::get_offset_of_U3CU3E9__41_0_3(),
	U3CU3Ec_t89EBA95A043507BD466A45BD1891EC3074271877_StaticFields::get_offset_of_U3CU3E9__41_1_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5158 = { sizeof (JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5158[3] = 
{
	JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC::get_offset_of__rootType_5(),
	JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC::get_offset_of__rootLevel_6(),
	JsonSerializerInternalWriter_tF75087E71BAA4AB66068B04C5F62A39E6601DDEC::get_offset_of__serializeStack_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5159 = { sizeof (JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5159[3] = 
{
	JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA::get_offset_of__serializerReader_31(),
	JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA::get_offset_of__serializerWriter_32(),
	JsonSerializerProxy_t389025E53712D560A4FA65257413EA8A11D868AA::get_offset_of__serializer_33(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5160 = { sizeof (JsonStringContract_tBD5A8A5530B0C67BAB2C365391405B988AC623C8), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5161 = { sizeof (JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D), -1, sizeof(JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5161[5] = 
{
	JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields::get_offset_of__dynamicCodeGeneration_0(),
	JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields::get_offset_of__fullyTrusted_1(),
	JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields::get_offset_of_CreatorCache_2(),
	JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields::get_offset_of_AssociatedMetadataTypesCache_3(),
	JsonTypeReflector_t04C9307055940940D034DD02E04818C3C7E47C0D_StaticFields::get_offset_of__metadataTypeAttributeReflectionObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5162 = { sizeof (U3CU3Ec__DisplayClass20_0_t7E278D9E65E015D4EDB942083BCD1847E7B6AB19), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5162[2] = 
{
	U3CU3Ec__DisplayClass20_0_t7E278D9E65E015D4EDB942083BCD1847E7B6AB19::get_offset_of_type_0(),
	U3CU3Ec__DisplayClass20_0_t7E278D9E65E015D4EDB942083BCD1847E7B6AB19::get_offset_of_defaultConstructor_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5163 = { sizeof (U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5), -1, sizeof(U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5163[2] = 
{
	U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t4444F146A1A5AF412E7E53E5D16ABDD36485ABA5_StaticFields::get_offset_of_U3CU3E9__20_1_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5164 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5164[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5165 = { sizeof (ReflectionValueProvider_tBA0226A7B27908047CD040913E5EE5EB0D613454), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5165[1] = 
{
	ReflectionValueProvider_tBA0226A7B27908047CD040913E5EE5EB0D613454::get_offset_of__memberInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5166 = { sizeof (OnErrorAttribute_tC65EB9E3FBE80CEF59678F1801F9D6D52E126F8F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5167 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5168 = { sizeof (CommentHandling_t12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5168[3] = 
{
	CommentHandling_t12C7B844BF60D120BF73F9CA86E31D43CE8B6CFA::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5169 = { sizeof (LineInfoHandling_t717AD31902D401C6EAD677A21E5C01B4AEA959A2)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5169[3] = 
{
	LineInfoHandling_t717AD31902D401C6EAD677A21E5C01B4AEA959A2::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5170 = { sizeof (JPropertyDescriptor_tC6034DAF3AD95E066BAB2785173E0CFE703EBA6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5171 = { sizeof (JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E), -1, sizeof(JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5171[2] = 
{
	JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E_StaticFields::get_offset_of_Comparer_2(),
	JPropertyKeyedCollection_tA5E9085D7B0BB2717C58E4200DE9BCFA26DF778E::get_offset_of__dictionary_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5172 = { sizeof (JsonLoadSettings_tA3C6C90ED0E51EED87B727EDB9AAC5529791BE48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5172[2] = 
{
	JsonLoadSettings_tA3C6C90ED0E51EED87B727EDB9AAC5529791BE48::get_offset_of__commentHandling_0(),
	JsonLoadSettings_tA3C6C90ED0E51EED87B727EDB9AAC5529791BE48::get_offset_of__lineInfoHandling_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5173 = { sizeof (JRaw_tDEAC098BFC05E891669DC59AE31064EA708A8A4E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5174 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5175 = { sizeof (JConstructor_t1B38B11D6CA34745263AAD0466339AFA64E3E73A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5175[2] = 
{
	JConstructor_t1B38B11D6CA34745263AAD0466339AFA64E3E73A::get_offset_of__name_16(),
	JConstructor_t1B38B11D6CA34745263AAD0466339AFA64E3E73A::get_offset_of__values_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5176 = { sizeof (JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5176[3] = 
{
	JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48::get_offset_of__listChanged_13(),
	JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48::get_offset_of__syncRoot_14(),
	JContainer_t2C8C23CEFF94834E9445F4970B6AB16831C84E48::get_offset_of__busy_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5177 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable5177[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5178 = { sizeof (JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5178[3] = 
{
	JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF::get_offset_of__properties_16(),
	JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF::get_offset_of_PropertyChanged_17(),
	JObject_tE2D45BD312B5E70DD799B932CEE02289BFC0ADAF::get_offset_of_PropertyChanging_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5179 = { sizeof (U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5179[4] = 
{
	U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__58_t5C0A4483C28FF34200A2EC87AB7B9E4524150EB1::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5180 = { sizeof (JArray_t152541B0C88ED7B2655C5CBCF49F2EF4A7C83148), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5180[1] = 
{
	JArray_t152541B0C88ED7B2655C5CBCF49F2EF4A7C83148::get_offset_of__values_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5181 = { sizeof (JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5181[4] = 
{
	JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11::get_offset_of__root_15(),
	JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11::get_offset_of__initialPath_16(),
	JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11::get_offset_of__parent_17(),
	JTokenReader_t96FA8A88B1BF47D944BC592C3C8BEBD85F55DC11::get_offset_of__current_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5182 = { sizeof (JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5182[4] = 
{
	JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682::get_offset_of__token_13(),
	JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682::get_offset_of__parent_14(),
	JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682::get_offset_of__value_15(),
	JTokenWriter_t89D4F77CBDCD41F6DDDB5ADEEC51E51B45087682::get_offset_of__current_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5183 = { sizeof (JToken_t06FA337591EC253506CE2A342A01C19CA990982B), -1, sizeof(JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5183[13] = 
{
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B::get_offset_of__parent_0(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B::get_offset_of__previous_1(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B::get_offset_of__next_2(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B::get_offset_of__annotations_3(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_BooleanTypes_4(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_NumberTypes_5(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_StringTypes_6(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_GuidTypes_7(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_TimeSpanTypes_8(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_UriTypes_9(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_CharTypes_10(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_DateTimeTypes_11(),
	JToken_t06FA337591EC253506CE2A342A01C19CA990982B_StaticFields::get_offset_of_BytesTypes_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5184 = { sizeof (LineInfoAnnotation_t0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5184[2] = 
{
	LineInfoAnnotation_t0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A::get_offset_of_LineNumber_0(),
	LineInfoAnnotation_t0A8AC497C7F27A89DDD7E9C9119E4677BE5BA20A::get_offset_of_LinePosition_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5185 = { sizeof (JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5185[2] = 
{
	JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B::get_offset_of__content_16(),
	JProperty_tBC934D9DB69AF069F9684CDAA0FAC25D3804233B::get_offset_of__name_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5186 = { sizeof (JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5186[1] = 
{
	JPropertyList_t7AB9D3A952D43CB35002B72C07797F00C99810C5::get_offset_of__token_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5187 = { sizeof (U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5187[3] = 
{
	U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__1_tFFE3A42F36CAA846615DB99BBF21DAC4FC6B6834::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5188 = { sizeof (JTokenType_tAAC6386A425E71C81B95E3583EBB0C699628D42F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5188[19] = 
{
	JTokenType_tAAC6386A425E71C81B95E3583EBB0C699628D42F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5189 = { sizeof (JValue_t80B940E650348637E29E2241958BC52097B1202F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5189[2] = 
{
	JValue_t80B940E650348637E29E2241958BC52097B1202F::get_offset_of__valueType_13(),
	JValue_t80B940E650348637E29E2241958BC52097B1202F::get_offset_of__value_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5190 = { sizeof (BinaryConverter_t66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5190[1] = 
{
	BinaryConverter_t66F8FA0CD24BB65397AB1D8803E3DDD2564B6D14::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5191 = { sizeof (DataSetConverter_tA28DA7A656D39DC52341B4F30194A7309A6F57C0), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5192 = { sizeof (DataTableConverter_tAD924E8CC72D9C291DFE9ED4F563067BF01E1AFF), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5193 = { sizeof (EntityKeyMemberConverter_tD3A19946087110A3F6D3410645A86D8229D4573E), -1, sizeof(EntityKeyMemberConverter_tD3A19946087110A3F6D3410645A86D8229D4573E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5193[1] = 
{
	EntityKeyMemberConverter_tD3A19946087110A3F6D3410645A86D8229D4573E_StaticFields::get_offset_of__reflectionObject_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5194 = { sizeof (KeyValuePairConverter_tCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E), -1, sizeof(KeyValuePairConverter_tCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5194[1] = 
{
	KeyValuePairConverter_tCCED0AF52B0D583F40FAC90C95EAE7FD7A6DD82E_StaticFields::get_offset_of_ReflectionObjectPerType_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5195 = { sizeof (BsonObjectIdConverter_tEB36B1ADB76417099EE3AF1AC3B9A4CA528B91FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5196 = { sizeof (RegexConverter_t62129A36894B2F932287215AAE74CADEC3E22144), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5197 = { sizeof (XmlDocumentWrapper_t0AF9193D2327378170CC705168522CE7785FED2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5197[1] = 
{
	XmlDocumentWrapper_t0AF9193D2327378170CC705168522CE7785FED2C::get_offset_of__document_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5198 = { sizeof (XmlElementWrapper_t47B237D03964251821968EAF0F71DE66BFA62577), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5198[1] = 
{
	XmlElementWrapper_t47B237D03964251821968EAF0F71DE66BFA62577::get_offset_of__element_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5199 = { sizeof (XmlDeclarationWrapper_tE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5199[1] = 
{
	XmlDeclarationWrapper_tE1A29004869DA3AC0FBB90CA6A5CA9EAFD466B35::get_offset_of__declaration_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
