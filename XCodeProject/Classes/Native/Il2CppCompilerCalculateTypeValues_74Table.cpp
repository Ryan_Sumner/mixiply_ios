﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// AttackBase
struct AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E;
// CanTarget
struct CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0;
// Core.Health.IAlignmentProvider
struct IAlignmentProvider_t2D743EB90EDB9AA7B214CF0DADA5767CC0022D5E;
// Core.Health.SerializableIAlignmentProvider
struct SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61;
// Core.Utilities.Timer
struct Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B;
// Health
struct Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D;
// Launcher
struct Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602;
// LockOnTarget
struct LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283;
// Lose
struct Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA;
// MHLab.PATCH.Install.InstallManager
struct InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990;
// MHLab.PATCH.LauncherManager
struct LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61;
// ParticleProjectile
struct ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4;
// Pathfinding.Nodes.Node
struct Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7;
// PlacementManager
struct PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2;
// ProgressBar
struct ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C;
// Projectile
struct Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D;
// Restart
struct Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E;
// Score
struct Score_t72F7EE757BE7D4C7846803B3072753760AB6427F;
// ScreenFade
struct ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71;
// SimpleHealthBar
struct SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5;
// SpaceInvaderAI
struct SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8;
// SpecialEffect
struct SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<CanTarget>
struct Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4;
// System.Action`1<EventParam>
struct Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0;
// System.Action`1<Health>
struct Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<EventParam>>
struct Dictionary_2_t84B5ABCC68D567AEA18910D7D8084A794F7412B5;
// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2,UnityEngine.GameObject>
struct Dictionary_2_t4BD15E4883130EE3934C68F0D6F5E4EC63344BA9;
// System.Collections.Generic.List`1<CanTarget>
struct List_1_t94FF8EBDACEF9DD31F7052AB0CC280E85F7EAA6F;
// System.Collections.Generic.List`1<Pathfinding.Agents.Agent>
struct List_1_tDF5E7A97BF5CBC75075BAF34E7A7BCA1AE3E1A66;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<UnityEngine.Transform>
struct List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector2,UnityEngine.GameObject>,System.Boolean>
struct Func_2_t7707010E7A7AA7479E3E1D4456363F9D70184E2B;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityDeeplinks
struct UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912;
// UnityEngine.AnimationCurve
struct AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C;
// UnityEngine.Animator
struct Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Collider
struct Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF;
// UnityEngine.GUIText
struct GUIText_t1AAED515CF7E63F24B55C5FC988555DA14DA10F1;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520;
// UnityEngine.Gradient
struct Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A;
// UnityEngine.Light
struct Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C;
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.ParticleSystem
struct ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D;
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.Rigidbody
struct Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.SpringJoint
struct SpringJoint_t2D196194480E339F728B463619720385523B09AF;
// UnityEngine.Transform
struct Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA;
// UnityEngine.Transform[]
struct TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804;
// UnityEngine.UI.Button
struct Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Text
struct Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[]
struct ReplacementDefinitionU5BU5D_t84917E930AAB75021ACF0693FF51880656D40344;
// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1;
// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA;
// UnityStandardAssets.Utility.DragRigidbody
struct DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57;
// UnityStandardAssets.Utility.FOVKick
struct FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2;
// UnityStandardAssets.Utility.LerpControlledBob
struct LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480;
// UnityStandardAssets.Utility.ObjectResetter
struct ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9;
// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03;
// UnityStandardAssets.Utility.TimedObjectActivator/Entries
struct Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E;
// UnityThreading.ActionThread
struct ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB;
// Win
struct Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef COPYCOMPONENT_T6A4912F858C5A927132D3E2E1E813BA8A81D4074_H
#define COPYCOMPONENT_T6A4912F858C5A927132D3E2E1E813BA8A81D4074_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CopyComponent
struct  CopyComponent_t6A4912F858C5A927132D3E2E1E813BA8A81D4074  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COPYCOMPONENT_T6A4912F858C5A927132D3E2E1E813BA8A81D4074_H
#ifndef U3CU3EC__DISPLAYCLASS34_0_TD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF_H
#define U3CU3EC__DISPLAYCLASS34_0_TD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass34_0
struct  U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass34_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.Int32 Launcher/<>c__DisplayClass34_0::max
	int32_t ___max_1;
	// System.Int32 Launcher/<>c__DisplayClass34_0::min
	int32_t ___min_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_min_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF, ___min_2)); }
	inline int32_t get_min_2() const { return ___min_2; }
	inline int32_t* get_address_of_min_2() { return &___min_2; }
	inline void set_min_2(int32_t value)
	{
		___min_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS34_0_TD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF_H
#ifndef U3CU3EC__DISPLAYCLASS35_0_T50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649_H
#define U3CU3EC__DISPLAYCLASS35_0_T50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass35_0
struct  U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass35_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.Int32 Launcher/<>c__DisplayClass35_0::max
	int32_t ___max_1;
	// System.Int32 Launcher/<>c__DisplayClass35_0::min
	int32_t ___min_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649, ___max_1)); }
	inline int32_t get_max_1() const { return ___max_1; }
	inline int32_t* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(int32_t value)
	{
		___max_1 = value;
	}

	inline static int32_t get_offset_of_min_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649, ___min_2)); }
	inline int32_t get_min_2() const { return ___min_2; }
	inline int32_t* get_address_of_min_2() { return &___min_2; }
	inline void set_min_2(int32_t value)
	{
		___min_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS35_0_T50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649_H
#ifndef U3CU3EC__DISPLAYCLASS38_0_T0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25_H
#define U3CU3EC__DISPLAYCLASS38_0_T0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass38_0
struct  U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass38_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.String Launcher/<>c__DisplayClass38_0::main
	String_t* ___main_1;
	// System.String Launcher/<>c__DisplayClass38_0::detail
	String_t* ___detail_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_main_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25, ___main_1)); }
	inline String_t* get_main_1() const { return ___main_1; }
	inline String_t** get_address_of_main_1() { return &___main_1; }
	inline void set_main_1(String_t* value)
	{
		___main_1 = value;
		Il2CppCodeGenWriteBarrier((&___main_1), value);
	}

	inline static int32_t get_offset_of_detail_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25, ___detail_2)); }
	inline String_t* get_detail_2() const { return ___detail_2; }
	inline String_t** get_address_of_detail_2() { return &___detail_2; }
	inline void set_detail_2(String_t* value)
	{
		___detail_2 = value;
		Il2CppCodeGenWriteBarrier((&___detail_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS38_0_T0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25_H
#ifndef U3CU3EC__DISPLAYCLASS39_0_T61E53D9C00D00A6F2CE0FA4D26790939964CADDC_H
#define U3CU3EC__DISPLAYCLASS39_0_T61E53D9C00D00A6F2CE0FA4D26790939964CADDC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass39_0
struct  U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass39_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.String Launcher/<>c__DisplayClass39_0::main
	String_t* ___main_1;
	// System.String Launcher/<>c__DisplayClass39_0::detail
	String_t* ___detail_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_main_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC, ___main_1)); }
	inline String_t* get_main_1() const { return ___main_1; }
	inline String_t** get_address_of_main_1() { return &___main_1; }
	inline void set_main_1(String_t* value)
	{
		___main_1 = value;
		Il2CppCodeGenWriteBarrier((&___main_1), value);
	}

	inline static int32_t get_offset_of_detail_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC, ___detail_2)); }
	inline String_t* get_detail_2() const { return ___detail_2; }
	inline String_t** get_address_of_detail_2() { return &___detail_2; }
	inline void set_detail_2(String_t* value)
	{
		___detail_2 = value;
		Il2CppCodeGenWriteBarrier((&___detail_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS39_0_T61E53D9C00D00A6F2CE0FA4D26790939964CADDC_H
#ifndef U3CU3EC__DISPLAYCLASS40_0_T9C3960F46C0599B1C1471089577DADB400006872_H
#define U3CU3EC__DISPLAYCLASS40_0_T9C3960F46C0599B1C1471089577DADB400006872_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass40_0
struct  U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass40_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.String Launcher/<>c__DisplayClass40_0::main
	String_t* ___main_1;
	// System.String Launcher/<>c__DisplayClass40_0::detail
	String_t* ___detail_2;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_main_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872, ___main_1)); }
	inline String_t* get_main_1() const { return ___main_1; }
	inline String_t** get_address_of_main_1() { return &___main_1; }
	inline void set_main_1(String_t* value)
	{
		___main_1 = value;
		Il2CppCodeGenWriteBarrier((&___main_1), value);
	}

	inline static int32_t get_offset_of_detail_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872, ___detail_2)); }
	inline String_t* get_detail_2() const { return ___detail_2; }
	inline String_t** get_address_of_detail_2() { return &___detail_2; }
	inline void set_detail_2(String_t* value)
	{
		___detail_2 = value;
		Il2CppCodeGenWriteBarrier((&___detail_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS40_0_T9C3960F46C0599B1C1471089577DADB400006872_H
#ifndef U3CU3EC__DISPLAYCLASS41_0_TE1590658328F6B1A5B3997FE6C700B9702F74BAC_H
#define U3CU3EC__DISPLAYCLASS41_0_TE1590658328F6B1A5B3997FE6C700B9702F74BAC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass41_0
struct  U3CU3Ec__DisplayClass41_0_tE1590658328F6B1A5B3997FE6C700B9702F74BAC  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass41_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.String Launcher/<>c__DisplayClass41_0::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_tE1590658328F6B1A5B3997FE6C700B9702F74BAC, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass41_0_tE1590658328F6B1A5B3997FE6C700B9702F74BAC, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS41_0_TE1590658328F6B1A5B3997FE6C700B9702F74BAC_H
#ifndef U3CU3EC__DISPLAYCLASS42_0_T503E84884773FA588880981AF1D54A1FF5FCC3A1_H
#define U3CU3EC__DISPLAYCLASS42_0_T503E84884773FA588880981AF1D54A1FF5FCC3A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass42_0
struct  U3CU3Ec__DisplayClass42_0_t503E84884773FA588880981AF1D54A1FF5FCC3A1  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass42_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.String Launcher/<>c__DisplayClass42_0::message
	String_t* ___message_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_t503E84884773FA588880981AF1D54A1FF5FCC3A1, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_message_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass42_0_t503E84884773FA588880981AF1D54A1FF5FCC3A1, ___message_1)); }
	inline String_t* get_message_1() const { return ___message_1; }
	inline String_t** get_address_of_message_1() { return &___message_1; }
	inline void set_message_1(String_t* value)
	{
		___message_1 = value;
		Il2CppCodeGenWriteBarrier((&___message_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS42_0_T503E84884773FA588880981AF1D54A1FF5FCC3A1_H
#ifndef U3CU3EC__DISPLAYCLASS46_0_T5BC53FED7FC5204240677469CC264CD12001B3D5_H
#define U3CU3EC__DISPLAYCLASS46_0_T5BC53FED7FC5204240677469CC264CD12001B3D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/<>c__DisplayClass46_0
struct  U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5  : public RuntimeObject
{
public:
	// Launcher Launcher/<>c__DisplayClass46_0::<>4__this
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * ___U3CU3E4__this_0;
	// System.Int32 Launcher/<>c__DisplayClass46_0::percentageCompleted
	int32_t ___percentageCompleted_1;
	// System.Int64 Launcher/<>c__DisplayClass46_0::currentFileSize
	int64_t ___currentFileSize_2;
	// System.Int64 Launcher/<>c__DisplayClass46_0::totalFileSize
	int64_t ___totalFileSize_3;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5, ___U3CU3E4__this_0)); }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_percentageCompleted_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5, ___percentageCompleted_1)); }
	inline int32_t get_percentageCompleted_1() const { return ___percentageCompleted_1; }
	inline int32_t* get_address_of_percentageCompleted_1() { return &___percentageCompleted_1; }
	inline void set_percentageCompleted_1(int32_t value)
	{
		___percentageCompleted_1 = value;
	}

	inline static int32_t get_offset_of_currentFileSize_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5, ___currentFileSize_2)); }
	inline int64_t get_currentFileSize_2() const { return ___currentFileSize_2; }
	inline int64_t* get_address_of_currentFileSize_2() { return &___currentFileSize_2; }
	inline void set_currentFileSize_2(int64_t value)
	{
		___currentFileSize_2 = value;
	}

	inline static int32_t get_offset_of_totalFileSize_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5, ___totalFileSize_3)); }
	inline int64_t get_totalFileSize_3() const { return ___totalFileSize_3; }
	inline int64_t* get_address_of_totalFileSize_3() { return &___totalFileSize_3; }
	inline void set_totalFileSize_3(int64_t value)
	{
		___totalFileSize_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS46_0_T5BC53FED7FC5204240677469CC264CD12001B3D5_H
#ifndef U3CU3EC__DISPLAYCLASS25_0_TC8142EFC601BE7AD968547C2E5C193272A353F69_H
#define U3CU3EC__DISPLAYCLASS25_0_TC8142EFC601BE7AD968547C2E5C193272A353F69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlacementManager/<>c__DisplayClass25_0
struct  U3CU3Ec__DisplayClass25_0_tC8142EFC601BE7AD968547C2E5C193272A353F69  : public RuntimeObject
{
public:
	// UnityEngine.GameObject PlacementManager/<>c__DisplayClass25_0::obj
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___obj_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<UnityEngine.Vector2,UnityEngine.GameObject>,System.Boolean> PlacementManager/<>c__DisplayClass25_0::<>9__0
	Func_2_t7707010E7A7AA7479E3E1D4456363F9D70184E2B * ___U3CU3E9__0_1;

public:
	inline static int32_t get_offset_of_obj_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tC8142EFC601BE7AD968547C2E5C193272A353F69, ___obj_0)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_obj_0() const { return ___obj_0; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_obj_0() { return &___obj_0; }
	inline void set_obj_0(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___obj_0 = value;
		Il2CppCodeGenWriteBarrier((&___obj_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass25_0_tC8142EFC601BE7AD968547C2E5C193272A353F69, ___U3CU3E9__0_1)); }
	inline Func_2_t7707010E7A7AA7479E3E1D4456363F9D70184E2B * get_U3CU3E9__0_1() const { return ___U3CU3E9__0_1; }
	inline Func_2_t7707010E7A7AA7479E3E1D4456363F9D70184E2B ** get_address_of_U3CU3E9__0_1() { return &___U3CU3E9__0_1; }
	inline void set_U3CU3E9__0_1(Func_2_t7707010E7A7AA7479E3E1D4456363F9D70184E2B * value)
	{
		___U3CU3E9__0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS25_0_TC8142EFC601BE7AD968547C2E5C193272A353F69_H
#ifndef U3CDURATIONSTOPU3ED__13_T7DA119F17CD7D5C98CFF242F16FE6EF338577F69_H
#define U3CDURATIONSTOPU3ED__13_T7DA119F17CD7D5C98CFF242F16FE6EF338577F69_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Projectile/<DurationStop>d__13
struct  U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69  : public RuntimeObject
{
public:
	// System.Int32 Projectile/<DurationStop>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object Projectile/<DurationStop>d__13::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// Projectile Projectile/<DurationStop>d__13::<>4__this
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69, ___U3CU3E4__this_2)); }
	inline Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDURATIONSTOPU3ED__13_T7DA119F17CD7D5C98CFF242F16FE6EF338577F69_H
#ifndef U3CWAITTOFIREU3ED__17_T23968FBC1439B22031C6D50029436E083DDBD3E7_H
#define U3CWAITTOFIREU3ED__17_T23968FBC1439B22031C6D50029436E083DDBD3E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpaceInvaderAI/<WaitToFire>d__17
struct  U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7  : public RuntimeObject
{
public:
	// System.Int32 SpaceInvaderAI/<WaitToFire>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SpaceInvaderAI/<WaitToFire>d__17::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SpaceInvaderAI/<WaitToFire>d__17::waitTime
	float ___waitTime_2;
	// SpaceInvaderAI SpaceInvaderAI/<WaitToFire>d__17::<>4__this
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7, ___U3CU3E4__this_3)); }
	inline SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITTOFIREU3ED__17_T23968FBC1439B22031C6D50029436E083DDBD3E7_H
#ifndef U3CWAITTOSTEPU3ED__19_T6B5E4BA1EDFA59BF8F263C2F90F27370696D0719_H
#define U3CWAITTOSTEPU3ED__19_T6B5E4BA1EDFA59BF8F263C2F90F27370696D0719_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpaceInvaderAI/<WaitToStep>d__19
struct  U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719  : public RuntimeObject
{
public:
	// System.Int32 SpaceInvaderAI/<WaitToStep>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SpaceInvaderAI/<WaitToStep>d__19::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SpaceInvaderAI/<WaitToStep>d__19::waitTime
	float ___waitTime_2;
	// SpaceInvaderAI SpaceInvaderAI/<WaitToStep>d__19::<>4__this
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719, ___U3CU3E4__this_3)); }
	inline SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITTOSTEPU3ED__19_T6B5E4BA1EDFA59BF8F263C2F90F27370696D0719_H
#ifndef U3CDURATIONSTOPU3ED__3_T6390BE3FCA39C9ABB412C7F135A5D2F48732B78A_H
#define U3CDURATIONSTOPU3ED__3_T6390BE3FCA39C9ABB412C7F135A5D2F48732B78A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpecialEffect/<DurationStop>d__3
struct  U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A  : public RuntimeObject
{
public:
	// System.Int32 SpecialEffect/<DurationStop>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SpecialEffect/<DurationStop>d__3::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// SpecialEffect SpecialEffect/<DurationStop>d__3::<>4__this
	SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A * ___U3CU3E4__this_2;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A, ___U3CU3E4__this_2)); }
	inline SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDURATIONSTOPU3ED__3_T6390BE3FCA39C9ABB412C7F135A5D2F48732B78A_H
#ifndef U3CWAITFORDESTRUCTIONU3ED__4_TA313FCDE5EDB244C740B5265142E98A89D8B5B02_H
#define U3CWAITFORDESTRUCTIONU3ED__4_TA313FCDE5EDB244C740B5265142E98A89D8B5B02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpecialEffect/<WaitForDestruction>d__4
struct  U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02  : public RuntimeObject
{
public:
	// System.Int32 SpecialEffect/<WaitForDestruction>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object SpecialEffect/<WaitForDestruction>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single SpecialEffect/<WaitForDestruction>d__4::waitTime
	float ___waitTime_2;
	// SpecialEffect SpecialEffect/<WaitForDestruction>d__4::<>4__this
	SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_waitTime_2() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02, ___waitTime_2)); }
	inline float get_waitTime_2() const { return ___waitTime_2; }
	inline float* get_address_of_waitTime_2() { return &___waitTime_2; }
	inline void set_waitTime_2(float value)
	{
		___waitTime_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02, ___U3CU3E4__this_3)); }
	inline SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CWAITFORDESTRUCTIONU3ED__4_TA313FCDE5EDB244C740B5265142E98A89D8B5B02_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef REPLACEMENTDEFINITION_TB7B8ADC7CD3FFC08A707B5DF69EA91F335897382_H
#define REPLACEMENTDEFINITION_TB7B8ADC7CD3FFC08A707B5DF69EA91F335897382_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition
struct  ReplacementDefinition_tB7B8ADC7CD3FFC08A707B5DF69EA91F335897382  : public RuntimeObject
{
public:
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::original
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___original_0;
	// UnityEngine.Shader UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition::replacement
	Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___replacement_1;

public:
	inline static int32_t get_offset_of_original_0() { return static_cast<int32_t>(offsetof(ReplacementDefinition_tB7B8ADC7CD3FFC08A707B5DF69EA91F335897382, ___original_0)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_original_0() const { return ___original_0; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_original_0() { return &___original_0; }
	inline void set_original_0(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___original_0 = value;
		Il2CppCodeGenWriteBarrier((&___original_0), value);
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(ReplacementDefinition_tB7B8ADC7CD3FFC08A707B5DF69EA91F335897382, ___replacement_1)); }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * get_replacement_1() const { return ___replacement_1; }
	inline Shader_tE2731FF351B74AB4186897484FB01E000C1160CA ** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTDEFINITION_TB7B8ADC7CD3FFC08A707B5DF69EA91F335897382_H
#ifndef REPLACEMENTLIST_TBC76041BA59383247AE6A08635452D166575F7C1_H
#define REPLACEMENTLIST_TBC76041BA59383247AE6A08635452D166575F7C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList
struct  ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1  : public RuntimeObject
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementDefinition[] UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList::items
	ReplacementDefinitionU5BU5D_t84917E930AAB75021ACF0693FF51880656D40344* ___items_0;

public:
	inline static int32_t get_offset_of_items_0() { return static_cast<int32_t>(offsetof(ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1, ___items_0)); }
	inline ReplacementDefinitionU5BU5D_t84917E930AAB75021ACF0693FF51880656D40344* get_items_0() const { return ___items_0; }
	inline ReplacementDefinitionU5BU5D_t84917E930AAB75021ACF0693FF51880656D40344** get_address_of_items_0() { return &___items_0; }
	inline void set_items_0(ReplacementDefinitionU5BU5D_t84917E930AAB75021ACF0693FF51880656D40344* value)
	{
		___items_0 = value;
		Il2CppCodeGenWriteBarrier((&___items_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REPLACEMENTLIST_TBC76041BA59383247AE6A08635452D166575F7C1_H
#ifndef U3CDRAGOBJECTU3ED__8_T17549ED9CDB53B36A05BA8B556D996F9FA1C6794_H
#define U3CDRAGOBJECTU3ED__8_T17549ED9CDB53B36A05BA8B556D996F9FA1C6794_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8
struct  U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.DragRigidbody UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<>4__this
	DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::distance
	float ___distance_3;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<oldDrag>5__2
	float ___U3ColdDragU3E5__2_4;
	// System.Single UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<oldAngularDrag>5__3
	float ___U3ColdAngularDragU3E5__3_5;
	// UnityEngine.Camera UnityStandardAssets.Utility.DragRigidbody/<DragObject>d__8::<mainCamera>5__4
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___U3CmainCameraU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___U3CU3E4__this_2)); }
	inline DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_distance_3() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___distance_3)); }
	inline float get_distance_3() const { return ___distance_3; }
	inline float* get_address_of_distance_3() { return &___distance_3; }
	inline void set_distance_3(float value)
	{
		___distance_3 = value;
	}

	inline static int32_t get_offset_of_U3ColdDragU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___U3ColdDragU3E5__2_4)); }
	inline float get_U3ColdDragU3E5__2_4() const { return ___U3ColdDragU3E5__2_4; }
	inline float* get_address_of_U3ColdDragU3E5__2_4() { return &___U3ColdDragU3E5__2_4; }
	inline void set_U3ColdDragU3E5__2_4(float value)
	{
		___U3ColdDragU3E5__2_4 = value;
	}

	inline static int32_t get_offset_of_U3ColdAngularDragU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___U3ColdAngularDragU3E5__3_5)); }
	inline float get_U3ColdAngularDragU3E5__3_5() const { return ___U3ColdAngularDragU3E5__3_5; }
	inline float* get_address_of_U3ColdAngularDragU3E5__3_5() { return &___U3ColdAngularDragU3E5__3_5; }
	inline void set_U3ColdAngularDragU3E5__3_5(float value)
	{
		___U3ColdAngularDragU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CmainCameraU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794, ___U3CmainCameraU3E5__4_6)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_U3CmainCameraU3E5__4_6() const { return ___U3CmainCameraU3E5__4_6; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_U3CmainCameraU3E5__4_6() { return &___U3CmainCameraU3E5__4_6; }
	inline void set_U3CmainCameraU3E5__4_6(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___U3CmainCameraU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CmainCameraU3E5__4_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDRAGOBJECTU3ED__8_T17549ED9CDB53B36A05BA8B556D996F9FA1C6794_H
#ifndef FOVKICK_T869A40BE9DDDC819536DBE8AD29768A73F55D2D2_H
#define FOVKICK_T869A40BE9DDDC819536DBE8AD29768A73F55D2D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick
struct  FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.FOVKick::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_0;
	// System.Single UnityStandardAssets.Utility.FOVKick::originalFov
	float ___originalFov_1;
	// System.Single UnityStandardAssets.Utility.FOVKick::FOVIncrease
	float ___FOVIncrease_2;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToIncrease
	float ___TimeToIncrease_3;
	// System.Single UnityStandardAssets.Utility.FOVKick::TimeToDecrease
	float ___TimeToDecrease_4;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.FOVKick::IncreaseCurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___IncreaseCurve_5;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2, ___Camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_originalFov_1() { return static_cast<int32_t>(offsetof(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2, ___originalFov_1)); }
	inline float get_originalFov_1() const { return ___originalFov_1; }
	inline float* get_address_of_originalFov_1() { return &___originalFov_1; }
	inline void set_originalFov_1(float value)
	{
		___originalFov_1 = value;
	}

	inline static int32_t get_offset_of_FOVIncrease_2() { return static_cast<int32_t>(offsetof(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2, ___FOVIncrease_2)); }
	inline float get_FOVIncrease_2() const { return ___FOVIncrease_2; }
	inline float* get_address_of_FOVIncrease_2() { return &___FOVIncrease_2; }
	inline void set_FOVIncrease_2(float value)
	{
		___FOVIncrease_2 = value;
	}

	inline static int32_t get_offset_of_TimeToIncrease_3() { return static_cast<int32_t>(offsetof(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2, ___TimeToIncrease_3)); }
	inline float get_TimeToIncrease_3() const { return ___TimeToIncrease_3; }
	inline float* get_address_of_TimeToIncrease_3() { return &___TimeToIncrease_3; }
	inline void set_TimeToIncrease_3(float value)
	{
		___TimeToIncrease_3 = value;
	}

	inline static int32_t get_offset_of_TimeToDecrease_4() { return static_cast<int32_t>(offsetof(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2, ___TimeToDecrease_4)); }
	inline float get_TimeToDecrease_4() const { return ___TimeToDecrease_4; }
	inline float* get_address_of_TimeToDecrease_4() { return &___TimeToDecrease_4; }
	inline void set_TimeToDecrease_4(float value)
	{
		___TimeToDecrease_4 = value;
	}

	inline static int32_t get_offset_of_IncreaseCurve_5() { return static_cast<int32_t>(offsetof(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2, ___IncreaseCurve_5)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_IncreaseCurve_5() const { return ___IncreaseCurve_5; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_IncreaseCurve_5() { return &___IncreaseCurve_5; }
	inline void set_IncreaseCurve_5(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___IncreaseCurve_5 = value;
		Il2CppCodeGenWriteBarrier((&___IncreaseCurve_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOVKICK_T869A40BE9DDDC819536DBE8AD29768A73F55D2D2_H
#ifndef U3CFOVKICKDOWNU3ED__10_T0C4F8545C484C6353CC526ABA9899D58E90914FA_H
#define U3CFOVKICKDOWNU3ED__10_T0C4F8545C484C6353CC526ABA9899D58E90914FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10
struct  U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<>4__this
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickDown>d__10::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA, ___U3CU3E4__this_2)); }
	inline FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKDOWNU3ED__10_T0C4F8545C484C6353CC526ABA9899D58E90914FA_H
#ifndef U3CFOVKICKUPU3ED__9_T14B549E842A8A92527B9DEAB213F220F748434F3_H
#define U3CFOVKICKUPU3ED__9_T14B549E842A8A92527B9DEAB213F220F748434F3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9
struct  U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.FOVKick UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<>4__this
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.FOVKick/<FOVKickUp>d__9::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3, ___U3CU3E4__this_2)); }
	inline FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CFOVKICKUPU3ED__9_T14B549E842A8A92527B9DEAB213F220F748434F3_H
#ifndef LERPCONTROLLEDBOB_T29D8B1DDB87958C047142D62531E1A4978C3B480_H
#define LERPCONTROLLEDBOB_T29D8B1DDB87958C047142D62531E1A4978C3B480_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob
struct  LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobDuration
	float ___BobDuration_0;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::BobAmount
	float ___BobAmount_1;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob::m_Offset
	float ___m_Offset_2;

public:
	inline static int32_t get_offset_of_BobDuration_0() { return static_cast<int32_t>(offsetof(LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480, ___BobDuration_0)); }
	inline float get_BobDuration_0() const { return ___BobDuration_0; }
	inline float* get_address_of_BobDuration_0() { return &___BobDuration_0; }
	inline void set_BobDuration_0(float value)
	{
		___BobDuration_0 = value;
	}

	inline static int32_t get_offset_of_BobAmount_1() { return static_cast<int32_t>(offsetof(LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480, ___BobAmount_1)); }
	inline float get_BobAmount_1() const { return ___BobAmount_1; }
	inline float* get_address_of_BobAmount_1() { return &___BobAmount_1; }
	inline void set_BobAmount_1(float value)
	{
		___BobAmount_1 = value;
	}

	inline static int32_t get_offset_of_m_Offset_2() { return static_cast<int32_t>(offsetof(LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480, ___m_Offset_2)); }
	inline float get_m_Offset_2() const { return ___m_Offset_2; }
	inline float* get_address_of_m_Offset_2() { return &___m_Offset_2; }
	inline void set_m_Offset_2(float value)
	{
		___m_Offset_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LERPCONTROLLEDBOB_T29D8B1DDB87958C047142D62531E1A4978C3B480_H
#ifndef U3CDOBOBCYCLEU3ED__4_TC7F8DE382FA82646BFD80ECCEF5BACE2954C543E_H
#define U3CDOBOBCYCLEU3ED__4_TC7F8DE382FA82646BFD80ECCEF5BACE2954C543E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4
struct  U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.LerpControlledBob UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<>4__this
	LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480 * ___U3CU3E4__this_2;
	// System.Single UnityStandardAssets.Utility.LerpControlledBob/<DoBobCycle>d__4::<t>5__2
	float ___U3CtU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E, ___U3CU3E4__this_2)); }
	inline LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CtU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E, ___U3CtU3E5__2_3)); }
	inline float get_U3CtU3E5__2_3() const { return ___U3CtU3E5__2_3; }
	inline float* get_address_of_U3CtU3E5__2_3() { return &___U3CtU3E5__2_3; }
	inline void set_U3CtU3E5__2_3(float value)
	{
		___U3CtU3E5__2_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CDOBOBCYCLEU3ED__4_TC7F8DE382FA82646BFD80ECCEF5BACE2954C543E_H
#ifndef U3CRESETCOROUTINEU3ED__6_TAE22AB241D2135B160859D9A0520247B5FAD804C_H
#define U3CRESETCOROUTINEU3ED__6_TAE22AB241D2135B160859D9A0520247B5FAD804C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6
struct  U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// System.Single UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::delay
	float ___delay_2;
	// UnityStandardAssets.Utility.ObjectResetter UnityStandardAssets.Utility.ObjectResetter/<ResetCoroutine>d__6::<>4__this
	ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 * ___U3CU3E4__this_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_delay_2() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C, ___delay_2)); }
	inline float get_delay_2() const { return ___delay_2; }
	inline float* get_address_of_delay_2() { return &___delay_2; }
	inline void set_delay_2(float value)
	{
		___delay_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C, ___U3CU3E4__this_3)); }
	inline ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CRESETCOROUTINEU3ED__6_TAE22AB241D2135B160859D9A0520247B5FAD804C_H
#ifndef U3CSTARTU3ED__4_T15A83C64D0632A9A329968DD8A2A27D5F0F7514C_H
#define U3CSTARTU3ED__4_T15A83C64D0632A9A329968DD8A2A27D5F0F7514C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4
struct  U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C  : public RuntimeObject
{
public:
	// System.Int32 UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// UnityStandardAssets.Utility.ParticleSystemDestroyer UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<>4__this
	ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03 * ___U3CU3E4__this_2;
	// UnityEngine.ParticleSystem[] UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<systems>5__2
	ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* ___U3CsystemsU3E5__2_3;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer/<Start>d__4::<stopTime>5__3
	float ___U3CstopTimeU3E5__3_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C, ___U3CU3E4__this_2)); }
	inline ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CsystemsU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C, ___U3CsystemsU3E5__2_3)); }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* get_U3CsystemsU3E5__2_3() const { return ___U3CsystemsU3E5__2_3; }
	inline ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9** get_address_of_U3CsystemsU3E5__2_3() { return &___U3CsystemsU3E5__2_3; }
	inline void set_U3CsystemsU3E5__2_3(ParticleSystemU5BU5D_t58EE604F685D8CBA8EFC9353456969F5A1B2FBB9* value)
	{
		___U3CsystemsU3E5__2_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CsystemsU3E5__2_3), value);
	}

	inline static int32_t get_offset_of_U3CstopTimeU3E5__3_4() { return static_cast<int32_t>(offsetof(U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C, ___U3CstopTimeU3E5__3_4)); }
	inline float get_U3CstopTimeU3E5__3_4() const { return ___U3CstopTimeU3E5__3_4; }
	inline float* get_address_of_U3CstopTimeU3E5__3_4() { return &___U3CstopTimeU3E5__3_4; }
	inline void set_U3CstopTimeU3E5__3_4(float value)
	{
		___U3CstopTimeU3E5__3_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSTARTU3ED__4_T15A83C64D0632A9A329968DD8A2A27D5F0F7514C_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#define COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Color
struct  Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 
{
public:
	// System.Single UnityEngine.Color::r
	float ___r_0;
	// System.Single UnityEngine.Color::g
	float ___g_1;
	// System.Single UnityEngine.Color::b
	float ___b_2;
	// System.Single UnityEngine.Color::a
	float ___a_3;

public:
	inline static int32_t get_offset_of_r_0() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___r_0)); }
	inline float get_r_0() const { return ___r_0; }
	inline float* get_address_of_r_0() { return &___r_0; }
	inline void set_r_0(float value)
	{
		___r_0 = value;
	}

	inline static int32_t get_offset_of_g_1() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___g_1)); }
	inline float get_g_1() const { return ___g_1; }
	inline float* get_address_of_g_1() { return &___g_1; }
	inline void set_g_1(float value)
	{
		___g_1 = value;
	}

	inline static int32_t get_offset_of_b_2() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___b_2)); }
	inline float get_b_2() const { return ___b_2; }
	inline float* get_address_of_b_2() { return &___b_2; }
	inline void set_b_2(float value)
	{
		___b_2 = value;
	}

	inline static int32_t get_offset_of_a_3() { return static_cast<int32_t>(offsetof(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2, ___a_3)); }
	inline float get_a_3() const { return ___a_3; }
	inline float* get_address_of_a_3() { return &___a_3; }
	inline void set_a_3(float value)
	{
		___a_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLOR_T119BCA590009762C7223FDD3AF9706653AC84ED2_H
#ifndef LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#define LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.LayerMask
struct  LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 
{
public:
	// System.Int32 UnityEngine.LayerMask::m_Mask
	int32_t ___m_Mask_0;

public:
	inline static int32_t get_offset_of_m_Mask_0() { return static_cast<int32_t>(offsetof(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0, ___m_Mask_0)); }
	inline int32_t get_m_Mask_0() const { return ___m_Mask_0; }
	inline int32_t* get_address_of_m_Mask_0() { return &___m_Mask_0; }
	inline void set_m_Mask_0(int32_t value)
	{
		___m_Mask_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAYERMASK_TBB9173D8B6939D476E67E849280AC9F4EC4D93B0_H
#ifndef QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#define QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Quaternion
struct  Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 
{
public:
	// System.Single UnityEngine.Quaternion::x
	float ___x_0;
	// System.Single UnityEngine.Quaternion::y
	float ___y_1;
	// System.Single UnityEngine.Quaternion::z
	float ___z_2;
	// System.Single UnityEngine.Quaternion::w
	float ___w_3;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}

	inline static int32_t get_offset_of_z_2() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___z_2)); }
	inline float get_z_2() const { return ___z_2; }
	inline float* get_address_of_z_2() { return &___z_2; }
	inline void set_z_2(float value)
	{
		___z_2 = value;
	}

	inline static int32_t get_offset_of_w_3() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357, ___w_3)); }
	inline float get_w_3() const { return ___w_3; }
	inline float* get_address_of_w_3() { return &___w_3; }
	inline void set_w_3(float value)
	{
		___w_3 = value;
	}
};

struct Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields
{
public:
	// UnityEngine.Quaternion UnityEngine.Quaternion::identityQuaternion
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___identityQuaternion_4;

public:
	inline static int32_t get_offset_of_identityQuaternion_4() { return static_cast<int32_t>(offsetof(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357_StaticFields, ___identityQuaternion_4)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_identityQuaternion_4() const { return ___identityQuaternion_4; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_identityQuaternion_4() { return &___identityQuaternion_4; }
	inline void set_identityQuaternion_4(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___identityQuaternion_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QUATERNION_T319F3319A7D43FFA5D819AD6C0A98851F0095357_H
#ifndef RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#define RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Rect
struct  Rect_t35B976DE901B5423C11705E156938EA27AB402CE 
{
public:
	// System.Single UnityEngine.Rect::m_XMin
	float ___m_XMin_0;
	// System.Single UnityEngine.Rect::m_YMin
	float ___m_YMin_1;
	// System.Single UnityEngine.Rect::m_Width
	float ___m_Width_2;
	// System.Single UnityEngine.Rect::m_Height
	float ___m_Height_3;

public:
	inline static int32_t get_offset_of_m_XMin_0() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_XMin_0)); }
	inline float get_m_XMin_0() const { return ___m_XMin_0; }
	inline float* get_address_of_m_XMin_0() { return &___m_XMin_0; }
	inline void set_m_XMin_0(float value)
	{
		___m_XMin_0 = value;
	}

	inline static int32_t get_offset_of_m_YMin_1() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_YMin_1)); }
	inline float get_m_YMin_1() const { return ___m_YMin_1; }
	inline float* get_address_of_m_YMin_1() { return &___m_YMin_1; }
	inline void set_m_YMin_1(float value)
	{
		___m_YMin_1 = value;
	}

	inline static int32_t get_offset_of_m_Width_2() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Width_2)); }
	inline float get_m_Width_2() const { return ___m_Width_2; }
	inline float* get_address_of_m_Width_2() { return &___m_Width_2; }
	inline void set_m_Width_2(float value)
	{
		___m_Width_2 = value;
	}

	inline static int32_t get_offset_of_m_Height_3() { return static_cast<int32_t>(offsetof(Rect_t35B976DE901B5423C11705E156938EA27AB402CE, ___m_Height_3)); }
	inline float get_m_Height_3() const { return ___m_Height_3; }
	inline float* get_address_of_m_Height_3() { return &___m_Height_3; }
	inline void set_m_Height_3(float value)
	{
		___m_Height_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECT_T35B976DE901B5423C11705E156938EA27AB402CE_H
#ifndef VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#define VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector2
struct  Vector2_tA85D2DD88578276CA8A8796756458277E72D073D 
{
public:
	// System.Single UnityEngine.Vector2::x
	float ___x_0;
	// System.Single UnityEngine.Vector2::y
	float ___y_1;

public:
	inline static int32_t get_offset_of_x_0() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___x_0)); }
	inline float get_x_0() const { return ___x_0; }
	inline float* get_address_of_x_0() { return &___x_0; }
	inline void set_x_0(float value)
	{
		___x_0 = value;
	}

	inline static int32_t get_offset_of_y_1() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D, ___y_1)); }
	inline float get_y_1() const { return ___y_1; }
	inline float* get_address_of_y_1() { return &___y_1; }
	inline void set_y_1(float value)
	{
		___y_1 = value;
	}
};

struct Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields
{
public:
	// UnityEngine.Vector2 UnityEngine.Vector2::zeroVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___zeroVector_2;
	// UnityEngine.Vector2 UnityEngine.Vector2::oneVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___oneVector_3;
	// UnityEngine.Vector2 UnityEngine.Vector2::upVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___upVector_4;
	// UnityEngine.Vector2 UnityEngine.Vector2::downVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___downVector_5;
	// UnityEngine.Vector2 UnityEngine.Vector2::leftVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___leftVector_6;
	// UnityEngine.Vector2 UnityEngine.Vector2::rightVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rightVector_7;
	// UnityEngine.Vector2 UnityEngine.Vector2::positiveInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___positiveInfinityVector_8;
	// UnityEngine.Vector2 UnityEngine.Vector2::negativeInfinityVector
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___negativeInfinityVector_9;

public:
	inline static int32_t get_offset_of_zeroVector_2() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___zeroVector_2)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_zeroVector_2() const { return ___zeroVector_2; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_zeroVector_2() { return &___zeroVector_2; }
	inline void set_zeroVector_2(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___zeroVector_2 = value;
	}

	inline static int32_t get_offset_of_oneVector_3() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___oneVector_3)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_oneVector_3() const { return ___oneVector_3; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_oneVector_3() { return &___oneVector_3; }
	inline void set_oneVector_3(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___oneVector_3 = value;
	}

	inline static int32_t get_offset_of_upVector_4() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___upVector_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_upVector_4() const { return ___upVector_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_upVector_4() { return &___upVector_4; }
	inline void set_upVector_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___upVector_4 = value;
	}

	inline static int32_t get_offset_of_downVector_5() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___downVector_5)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_downVector_5() const { return ___downVector_5; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_downVector_5() { return &___downVector_5; }
	inline void set_downVector_5(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___downVector_5 = value;
	}

	inline static int32_t get_offset_of_leftVector_6() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___leftVector_6)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_leftVector_6() const { return ___leftVector_6; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_leftVector_6() { return &___leftVector_6; }
	inline void set_leftVector_6(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___leftVector_6 = value;
	}

	inline static int32_t get_offset_of_rightVector_7() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___rightVector_7)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rightVector_7() const { return ___rightVector_7; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rightVector_7() { return &___rightVector_7; }
	inline void set_rightVector_7(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rightVector_7 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_8() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___positiveInfinityVector_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_positiveInfinityVector_8() const { return ___positiveInfinityVector_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_positiveInfinityVector_8() { return &___positiveInfinityVector_8; }
	inline void set_positiveInfinityVector_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___positiveInfinityVector_8 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_9() { return static_cast<int32_t>(offsetof(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D_StaticFields, ___negativeInfinityVector_9)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_negativeInfinityVector_9() const { return ___negativeInfinityVector_9; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_negativeInfinityVector_9() { return &___negativeInfinityVector_9; }
	inline void set_negativeInfinityVector_9(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___negativeInfinityVector_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR2_TA85D2DD88578276CA8A8796756458277E72D073D_H
#ifndef VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#define VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Vector3
struct  Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 
{
public:
	// System.Single UnityEngine.Vector3::x
	float ___x_2;
	// System.Single UnityEngine.Vector3::y
	float ___y_3;
	// System.Single UnityEngine.Vector3::z
	float ___z_4;

public:
	inline static int32_t get_offset_of_x_2() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___x_2)); }
	inline float get_x_2() const { return ___x_2; }
	inline float* get_address_of_x_2() { return &___x_2; }
	inline void set_x_2(float value)
	{
		___x_2 = value;
	}

	inline static int32_t get_offset_of_y_3() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___y_3)); }
	inline float get_y_3() const { return ___y_3; }
	inline float* get_address_of_y_3() { return &___y_3; }
	inline void set_y_3(float value)
	{
		___y_3 = value;
	}

	inline static int32_t get_offset_of_z_4() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720, ___z_4)); }
	inline float get_z_4() const { return ___z_4; }
	inline float* get_address_of_z_4() { return &___z_4; }
	inline void set_z_4(float value)
	{
		___z_4 = value;
	}
};

struct Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields
{
public:
	// UnityEngine.Vector3 UnityEngine.Vector3::zeroVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___zeroVector_5;
	// UnityEngine.Vector3 UnityEngine.Vector3::oneVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___oneVector_6;
	// UnityEngine.Vector3 UnityEngine.Vector3::upVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___upVector_7;
	// UnityEngine.Vector3 UnityEngine.Vector3::downVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___downVector_8;
	// UnityEngine.Vector3 UnityEngine.Vector3::leftVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___leftVector_9;
	// UnityEngine.Vector3 UnityEngine.Vector3::rightVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___rightVector_10;
	// UnityEngine.Vector3 UnityEngine.Vector3::forwardVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___forwardVector_11;
	// UnityEngine.Vector3 UnityEngine.Vector3::backVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___backVector_12;
	// UnityEngine.Vector3 UnityEngine.Vector3::positiveInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___positiveInfinityVector_13;
	// UnityEngine.Vector3 UnityEngine.Vector3::negativeInfinityVector
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___negativeInfinityVector_14;

public:
	inline static int32_t get_offset_of_zeroVector_5() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___zeroVector_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_zeroVector_5() const { return ___zeroVector_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_zeroVector_5() { return &___zeroVector_5; }
	inline void set_zeroVector_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___zeroVector_5 = value;
	}

	inline static int32_t get_offset_of_oneVector_6() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___oneVector_6)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_oneVector_6() const { return ___oneVector_6; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_oneVector_6() { return &___oneVector_6; }
	inline void set_oneVector_6(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___oneVector_6 = value;
	}

	inline static int32_t get_offset_of_upVector_7() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___upVector_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_upVector_7() const { return ___upVector_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_upVector_7() { return &___upVector_7; }
	inline void set_upVector_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___upVector_7 = value;
	}

	inline static int32_t get_offset_of_downVector_8() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___downVector_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_downVector_8() const { return ___downVector_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_downVector_8() { return &___downVector_8; }
	inline void set_downVector_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___downVector_8 = value;
	}

	inline static int32_t get_offset_of_leftVector_9() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___leftVector_9)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_leftVector_9() const { return ___leftVector_9; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_leftVector_9() { return &___leftVector_9; }
	inline void set_leftVector_9(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___leftVector_9 = value;
	}

	inline static int32_t get_offset_of_rightVector_10() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___rightVector_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_rightVector_10() const { return ___rightVector_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_rightVector_10() { return &___rightVector_10; }
	inline void set_rightVector_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___rightVector_10 = value;
	}

	inline static int32_t get_offset_of_forwardVector_11() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___forwardVector_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_forwardVector_11() const { return ___forwardVector_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_forwardVector_11() { return &___forwardVector_11; }
	inline void set_forwardVector_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___forwardVector_11 = value;
	}

	inline static int32_t get_offset_of_backVector_12() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___backVector_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_backVector_12() const { return ___backVector_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_backVector_12() { return &___backVector_12; }
	inline void set_backVector_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___backVector_12 = value;
	}

	inline static int32_t get_offset_of_positiveInfinityVector_13() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___positiveInfinityVector_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_positiveInfinityVector_13() const { return ___positiveInfinityVector_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_positiveInfinityVector_13() { return &___positiveInfinityVector_13; }
	inline void set_positiveInfinityVector_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___positiveInfinityVector_13 = value;
	}

	inline static int32_t get_offset_of_negativeInfinityVector_14() { return static_cast<int32_t>(offsetof(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720_StaticFields, ___negativeInfinityVector_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_negativeInfinityVector_14() const { return ___negativeInfinityVector_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_negativeInfinityVector_14() { return &___negativeInfinityVector_14; }
	inline void set_negativeInfinityVector_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___negativeInfinityVector_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3_TDCF05E21F632FE2BA260C06E0D10CA81513E6720_H
#ifndef EVENTPARAM_TE4C698E268EA31868630820B4529293635F87174_H
#define EVENTPARAM_TE4C698E268EA31868630820B4529293635F87174_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventParam
struct  EventParam_tE4C698E268EA31868630820B4529293635F87174 
{
public:
	// System.String EventParam::String
	String_t* ___String_0;
	// System.Int32 EventParam::Int
	int32_t ___Int_1;
	// System.Single EventParam::Float
	float ___Float_2;
	// System.Boolean EventParam::Bool
	bool ___Bool_3;
	// UnityEngine.Vector3 EventParam::Vector3
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Vector3_4;
	// Core.Health.IAlignmentProvider EventParam::AlignmentProvider
	RuntimeObject* ___AlignmentProvider_5;

public:
	inline static int32_t get_offset_of_String_0() { return static_cast<int32_t>(offsetof(EventParam_tE4C698E268EA31868630820B4529293635F87174, ___String_0)); }
	inline String_t* get_String_0() const { return ___String_0; }
	inline String_t** get_address_of_String_0() { return &___String_0; }
	inline void set_String_0(String_t* value)
	{
		___String_0 = value;
		Il2CppCodeGenWriteBarrier((&___String_0), value);
	}

	inline static int32_t get_offset_of_Int_1() { return static_cast<int32_t>(offsetof(EventParam_tE4C698E268EA31868630820B4529293635F87174, ___Int_1)); }
	inline int32_t get_Int_1() const { return ___Int_1; }
	inline int32_t* get_address_of_Int_1() { return &___Int_1; }
	inline void set_Int_1(int32_t value)
	{
		___Int_1 = value;
	}

	inline static int32_t get_offset_of_Float_2() { return static_cast<int32_t>(offsetof(EventParam_tE4C698E268EA31868630820B4529293635F87174, ___Float_2)); }
	inline float get_Float_2() const { return ___Float_2; }
	inline float* get_address_of_Float_2() { return &___Float_2; }
	inline void set_Float_2(float value)
	{
		___Float_2 = value;
	}

	inline static int32_t get_offset_of_Bool_3() { return static_cast<int32_t>(offsetof(EventParam_tE4C698E268EA31868630820B4529293635F87174, ___Bool_3)); }
	inline bool get_Bool_3() const { return ___Bool_3; }
	inline bool* get_address_of_Bool_3() { return &___Bool_3; }
	inline void set_Bool_3(bool value)
	{
		___Bool_3 = value;
	}

	inline static int32_t get_offset_of_Vector3_4() { return static_cast<int32_t>(offsetof(EventParam_tE4C698E268EA31868630820B4529293635F87174, ___Vector3_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Vector3_4() const { return ___Vector3_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Vector3_4() { return &___Vector3_4; }
	inline void set_Vector3_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Vector3_4 = value;
	}

	inline static int32_t get_offset_of_AlignmentProvider_5() { return static_cast<int32_t>(offsetof(EventParam_tE4C698E268EA31868630820B4529293635F87174, ___AlignmentProvider_5)); }
	inline RuntimeObject* get_AlignmentProvider_5() const { return ___AlignmentProvider_5; }
	inline RuntimeObject** get_address_of_AlignmentProvider_5() { return &___AlignmentProvider_5; }
	inline void set_AlignmentProvider_5(RuntimeObject* value)
	{
		___AlignmentProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___AlignmentProvider_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of EventParam
struct EventParam_tE4C698E268EA31868630820B4529293635F87174_marshaled_pinvoke
{
	char* ___String_0;
	int32_t ___Int_1;
	float ___Float_2;
	int32_t ___Bool_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Vector3_4;
	RuntimeObject* ___AlignmentProvider_5;
};
// Native definition for COM marshalling of EventParam
struct EventParam_tE4C698E268EA31868630820B4529293635F87174_marshaled_com
{
	Il2CppChar* ___String_0;
	int32_t ___Int_1;
	float ___Float_2;
	int32_t ___Bool_3;
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Vector3_4;
	RuntimeObject* ___AlignmentProvider_5;
};
#endif // EVENTPARAM_TE4C698E268EA31868630820B4529293635F87174_H
#ifndef GUISTATE_T6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134_H
#define GUISTATE_T6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GUIState
struct  GUIState_t6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134 
{
public:
	// System.Int32 GUIState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GUIState_t6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GUISTATE_T6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134_H
#ifndef GAMEEVENT_TD296FF101AF6CB90E22B80A20A4261D8BCAB3183_H
#define GAMEEVENT_TD296FF101AF6CB90E22B80A20A4261D8BCAB3183_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEventManager/GameEvent
struct  GameEvent_tD296FF101AF6CB90E22B80A20A4261D8BCAB3183 
{
public:
	// System.Int32 GameEventManager/GameEvent::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(GameEvent_tD296FF101AF6CB90E22B80A20A4261D8BCAB3183, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENT_TD296FF101AF6CB90E22B80A20A4261D8BCAB3183_H
#ifndef LAUNCHERSTATUS_T376D625711B0C059729302BDEA9F2160A84AF41F_H
#define LAUNCHERSTATUS_T376D625711B0C059729302BDEA9F2160A84AF41F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher/LauncherStatus
struct  LauncherStatus_t376D625711B0C059729302BDEA9F2160A84AF41F 
{
public:
	// System.Int32 Launcher/LauncherStatus::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LauncherStatus_t376D625711B0C059729302BDEA9F2160A84AF41F, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHERSTATUS_T376D625711B0C059729302BDEA9F2160A84AF41F_H
#ifndef COLORMODE_TE457C6ADD8CF97DFCE07D3E77B05680CB318212E_H
#define COLORMODE_TE457C6ADD8CF97DFCE07D3E77B05680CB318212E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar/ColorMode
struct  ColorMode_tE457C6ADD8CF97DFCE07D3E77B05680CB318212E 
{
public:
	// System.Int32 SimpleHealthBar/ColorMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ColorMode_tE457C6ADD8CF97DFCE07D3E77B05680CB318212E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLORMODE_TE457C6ADD8CF97DFCE07D3E77B05680CB318212E_H
#ifndef DISPLAYTEXT_T7A4193422DA4378F30BF7A49042056A94F3F8A41_H
#define DISPLAYTEXT_T7A4193422DA4378F30BF7A49042056A94F3F8A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar/DisplayText
struct  DisplayText_t7A4193422DA4378F30BF7A49042056A94F3F8A41 
{
public:
	// System.Int32 SimpleHealthBar/DisplayText::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DisplayText_t7A4193422DA4378F30BF7A49042056A94F3F8A41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DISPLAYTEXT_T7A4193422DA4378F30BF7A49042056A94F3F8A41_H
#ifndef OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#define OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Object
struct  Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};
#endif // OBJECT_TAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_H
#ifndef SPACE_T0F622BF939B7A47E0F9632CE968F7D72FC63AF58_H
#define SPACE_T0F622BF939B7A47E0F9632CE968F7D72FC63AF58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Space
struct  Space_t0F622BF939B7A47E0F9632CE968F7D72FC63AF58 
{
public:
	// System.Int32 UnityEngine.Space::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Space_t0F622BF939B7A47E0F9632CE968F7D72FC63AF58, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACE_T0F622BF939B7A47E0F9632CE968F7D72FC63AF58_H
#ifndef MODE_T936E9A246C14583EB7FEEAD6FE3851D77A219F8A_H
#define MODE_T936E9A246C14583EB7FEEAD6FE3851D77A219F8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger/Mode
struct  Mode_t936E9A246C14583EB7FEEAD6FE3851D77A219F8A 
{
public:
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger/Mode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Mode_t936E9A246C14583EB7FEEAD6FE3851D77A219F8A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MODE_T936E9A246C14583EB7FEEAD6FE3851D77A219F8A_H
#ifndef CAMERAREFOCUS_T2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209_H
#define CAMERAREFOCUS_T2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CameraRefocus
struct  CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209  : public RuntimeObject
{
public:
	// UnityEngine.Camera UnityStandardAssets.Utility.CameraRefocus::Camera
	Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * ___Camera_0;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::Lookatpoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___Lookatpoint_1;
	// UnityEngine.Transform UnityStandardAssets.Utility.CameraRefocus::Parent
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___Parent_2;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CameraRefocus::m_OrigCameraPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_OrigCameraPos_3;
	// System.Boolean UnityStandardAssets.Utility.CameraRefocus::m_Refocus
	bool ___m_Refocus_4;

public:
	inline static int32_t get_offset_of_Camera_0() { return static_cast<int32_t>(offsetof(CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209, ___Camera_0)); }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * get_Camera_0() const { return ___Camera_0; }
	inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 ** get_address_of_Camera_0() { return &___Camera_0; }
	inline void set_Camera_0(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * value)
	{
		___Camera_0 = value;
		Il2CppCodeGenWriteBarrier((&___Camera_0), value);
	}

	inline static int32_t get_offset_of_Lookatpoint_1() { return static_cast<int32_t>(offsetof(CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209, ___Lookatpoint_1)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_Lookatpoint_1() const { return ___Lookatpoint_1; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_Lookatpoint_1() { return &___Lookatpoint_1; }
	inline void set_Lookatpoint_1(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___Lookatpoint_1 = value;
	}

	inline static int32_t get_offset_of_Parent_2() { return static_cast<int32_t>(offsetof(CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209, ___Parent_2)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_Parent_2() const { return ___Parent_2; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_Parent_2() { return &___Parent_2; }
	inline void set_Parent_2(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___Parent_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parent_2), value);
	}

	inline static int32_t get_offset_of_m_OrigCameraPos_3() { return static_cast<int32_t>(offsetof(CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209, ___m_OrigCameraPos_3)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_OrigCameraPos_3() const { return ___m_OrigCameraPos_3; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_OrigCameraPos_3() { return &___m_OrigCameraPos_3; }
	inline void set_m_OrigCameraPos_3(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_OrigCameraPos_3 = value;
	}

	inline static int32_t get_offset_of_m_Refocus_4() { return static_cast<int32_t>(offsetof(CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209, ___m_Refocus_4)); }
	inline bool get_m_Refocus_4() const { return ___m_Refocus_4; }
	inline bool* get_address_of_m_Refocus_4() { return &___m_Refocus_4; }
	inline void set_m_Refocus_4(bool value)
	{
		___m_Refocus_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERAREFOCUS_T2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209_H
#ifndef CURVECONTROLLEDBOB_T603CBC7D528557A646CE216B5389D65F2265CED1_H
#define CURVECONTROLLEDBOB_T603CBC7D528557A646CE216B5389D65F2265CED1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.CurveControlledBob
struct  CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1  : public RuntimeObject
{
public:
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::HorizontalBobRange
	float ___HorizontalBobRange_0;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticalBobRange
	float ___VerticalBobRange_1;
	// UnityEngine.AnimationCurve UnityStandardAssets.Utility.CurveControlledBob::Bobcurve
	AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * ___Bobcurve_2;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::VerticaltoHorizontalRatio
	float ___VerticaltoHorizontalRatio_3;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionX
	float ___m_CyclePositionX_4;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_CyclePositionY
	float ___m_CyclePositionY_5;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_BobBaseInterval
	float ___m_BobBaseInterval_6;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.CurveControlledBob::m_OriginalCameraPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_OriginalCameraPosition_7;
	// System.Single UnityStandardAssets.Utility.CurveControlledBob::m_Time
	float ___m_Time_8;

public:
	inline static int32_t get_offset_of_HorizontalBobRange_0() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___HorizontalBobRange_0)); }
	inline float get_HorizontalBobRange_0() const { return ___HorizontalBobRange_0; }
	inline float* get_address_of_HorizontalBobRange_0() { return &___HorizontalBobRange_0; }
	inline void set_HorizontalBobRange_0(float value)
	{
		___HorizontalBobRange_0 = value;
	}

	inline static int32_t get_offset_of_VerticalBobRange_1() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___VerticalBobRange_1)); }
	inline float get_VerticalBobRange_1() const { return ___VerticalBobRange_1; }
	inline float* get_address_of_VerticalBobRange_1() { return &___VerticalBobRange_1; }
	inline void set_VerticalBobRange_1(float value)
	{
		___VerticalBobRange_1 = value;
	}

	inline static int32_t get_offset_of_Bobcurve_2() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___Bobcurve_2)); }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * get_Bobcurve_2() const { return ___Bobcurve_2; }
	inline AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C ** get_address_of_Bobcurve_2() { return &___Bobcurve_2; }
	inline void set_Bobcurve_2(AnimationCurve_tD2F265379583AAF1BF8D84F1BB8DB12980FA504C * value)
	{
		___Bobcurve_2 = value;
		Il2CppCodeGenWriteBarrier((&___Bobcurve_2), value);
	}

	inline static int32_t get_offset_of_VerticaltoHorizontalRatio_3() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___VerticaltoHorizontalRatio_3)); }
	inline float get_VerticaltoHorizontalRatio_3() const { return ___VerticaltoHorizontalRatio_3; }
	inline float* get_address_of_VerticaltoHorizontalRatio_3() { return &___VerticaltoHorizontalRatio_3; }
	inline void set_VerticaltoHorizontalRatio_3(float value)
	{
		___VerticaltoHorizontalRatio_3 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionX_4() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___m_CyclePositionX_4)); }
	inline float get_m_CyclePositionX_4() const { return ___m_CyclePositionX_4; }
	inline float* get_address_of_m_CyclePositionX_4() { return &___m_CyclePositionX_4; }
	inline void set_m_CyclePositionX_4(float value)
	{
		___m_CyclePositionX_4 = value;
	}

	inline static int32_t get_offset_of_m_CyclePositionY_5() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___m_CyclePositionY_5)); }
	inline float get_m_CyclePositionY_5() const { return ___m_CyclePositionY_5; }
	inline float* get_address_of_m_CyclePositionY_5() { return &___m_CyclePositionY_5; }
	inline void set_m_CyclePositionY_5(float value)
	{
		___m_CyclePositionY_5 = value;
	}

	inline static int32_t get_offset_of_m_BobBaseInterval_6() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___m_BobBaseInterval_6)); }
	inline float get_m_BobBaseInterval_6() const { return ___m_BobBaseInterval_6; }
	inline float* get_address_of_m_BobBaseInterval_6() { return &___m_BobBaseInterval_6; }
	inline void set_m_BobBaseInterval_6(float value)
	{
		___m_BobBaseInterval_6 = value;
	}

	inline static int32_t get_offset_of_m_OriginalCameraPosition_7() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___m_OriginalCameraPosition_7)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_OriginalCameraPosition_7() const { return ___m_OriginalCameraPosition_7; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_OriginalCameraPosition_7() { return &___m_OriginalCameraPosition_7; }
	inline void set_m_OriginalCameraPosition_7(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_OriginalCameraPosition_7 = value;
	}

	inline static int32_t get_offset_of_m_Time_8() { return static_cast<int32_t>(offsetof(CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1, ___m_Time_8)); }
	inline float get_m_Time_8() const { return ___m_Time_8; }
	inline float* get_address_of_m_Time_8() { return &___m_Time_8; }
	inline void set_m_Time_8(float value)
	{
		___m_Time_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CURVECONTROLLEDBOB_T603CBC7D528557A646CE216B5389D65F2265CED1_H
#ifndef BUILDTARGETGROUP_T541998A4F75166E0159D30A8963A2EE8C1CDAB96_H
#define BUILDTARGETGROUP_T541998A4F75166E0159D30A8963A2EE8C1CDAB96_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup
struct  BuildTargetGroup_t541998A4F75166E0159D30A8963A2EE8C1CDAB96 
{
public:
	// System.Int32 UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BuildTargetGroup_t541998A4F75166E0159D30A8963A2EE8C1CDAB96, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BUILDTARGETGROUP_T541998A4F75166E0159D30A8963A2EE8C1CDAB96_H
#ifndef ACTION_T555B81FF41842ECAE14AABF88618DE8D3189FFCD_H
#define ACTION_T555B81FF41842ECAE14AABF88618DE8D3189FFCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator/Action
struct  Action_t555B81FF41842ECAE14AABF88618DE8D3189FFCD 
{
public:
	// System.Int32 UnityStandardAssets.Utility.TimedObjectActivator/Action::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Action_t555B81FF41842ECAE14AABF88618DE8D3189FFCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTION_T555B81FF41842ECAE14AABF88618DE8D3189FFCD_H
#ifndef COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#define COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Component
struct  Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPONENT_T05064EF382ABCAF4B8C94F8A350EA85184C26621_H
#ifndef VECTOR3ANDSPACE_T3E61556293DA41E619A7E7B653625FE6696A5FBA_H
#define VECTOR3ANDSPACE_T3E61556293DA41E619A7E7B653625FE6696A5FBA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace
struct  Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA  : public RuntimeObject
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::value
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___value_0;
	// UnityEngine.Space UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace::space
	int32_t ___space_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA, ___value_0)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_value_0() const { return ___value_0; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_space_1() { return static_cast<int32_t>(offsetof(Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA, ___space_1)); }
	inline int32_t get_space_1() const { return ___space_1; }
	inline int32_t* get_address_of_space_1() { return &___space_1; }
	inline void set_space_1(int32_t value)
	{
		___space_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VECTOR3ANDSPACE_T3E61556293DA41E619A7E7B653625FE6696A5FBA_H
#ifndef BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#define BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.Behaviour
struct  Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BEHAVIOUR_TBDC7E9C3C898AD8348891B82D3E345801D920CA8_H
#ifndef MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#define MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityEngine.MonoBehaviour
struct  MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MONOBEHAVIOUR_T4A60845CF505405AF8BE8C61CC07F75CADEF6429_H
#ifndef AGENTINITIALISER_T7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5_H
#define AGENTINITIALISER_T7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AgentInitialiser
struct  AgentInitialiser_t7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Pathfinding.Nodes.Node AgentInitialiser::startNode
	Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * ___startNode_4;

public:
	inline static int32_t get_offset_of_startNode_4() { return static_cast<int32_t>(offsetof(AgentInitialiser_t7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5, ___startNode_4)); }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * get_startNode_4() const { return ___startNode_4; }
	inline Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 ** get_address_of_startNode_4() { return &___startNode_4; }
	inline void set_startNode_4(Node_tB9FE3EB5FF72CA571D2FA8001110FDAB417D3EC7 * value)
	{
		___startNode_4 = value;
		Il2CppCodeGenWriteBarrier((&___startNode_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AGENTINITIALISER_T7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5_H
#ifndef ALPHABUTTONCLICKMASK_T90B0DF50D542BD644F11259B7CA845036AF5F8F1_H
#define ALPHABUTTONCLICKMASK_T90B0DF50D542BD644F11259B7CA845036AF5F8F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AlphaButtonClickMask
struct  AlphaButtonClickMask_t90B0DF50D542BD644F11259B7CA845036AF5F8F1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image AlphaButtonClickMask::_image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____image_4;

public:
	inline static int32_t get_offset_of__image_4() { return static_cast<int32_t>(offsetof(AlphaButtonClickMask_t90B0DF50D542BD644F11259B7CA845036AF5F8F1, ____image_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__image_4() const { return ____image_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__image_4() { return &____image_4; }
	inline void set__image_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____image_4 = value;
		Il2CppCodeGenWriteBarrier((&____image_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ALPHABUTTONCLICKMASK_T90B0DF50D542BD644F11259B7CA845036AF5F8F1_H
#ifndef ATTACKBASE_TE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E_H
#define ATTACKBASE_TE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AttackBase
struct  AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Health AttackBase::target
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___target_4;
	// UnityEngine.ParticleSystem AttackBase::attackPfx
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___attackPfx_5;
	// UnityEngine.Transform AttackBase::attackPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___attackPoint_6;
	// System.Single AttackBase::damage
	float ___damage_7;
	// System.Single AttackBase::attackChargeTime
	float ___attackChargeTime_8;
	// Core.Utilities.Timer AttackBase::m_HomeBaseAttackTimer
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * ___m_HomeBaseAttackTimer_9;
	// System.Boolean AttackBase::m_IsChargingHomeBaseAttack
	bool ___m_IsChargingHomeBaseAttack_10;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___target_4)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_target_4() const { return ___target_4; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_attackPfx_5() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___attackPfx_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_attackPfx_5() const { return ___attackPfx_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_attackPfx_5() { return &___attackPfx_5; }
	inline void set_attackPfx_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___attackPfx_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackPfx_5), value);
	}

	inline static int32_t get_offset_of_attackPoint_6() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___attackPoint_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_attackPoint_6() const { return ___attackPoint_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_attackPoint_6() { return &___attackPoint_6; }
	inline void set_attackPoint_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___attackPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackPoint_6), value);
	}

	inline static int32_t get_offset_of_damage_7() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___damage_7)); }
	inline float get_damage_7() const { return ___damage_7; }
	inline float* get_address_of_damage_7() { return &___damage_7; }
	inline void set_damage_7(float value)
	{
		___damage_7 = value;
	}

	inline static int32_t get_offset_of_attackChargeTime_8() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___attackChargeTime_8)); }
	inline float get_attackChargeTime_8() const { return ___attackChargeTime_8; }
	inline float* get_address_of_attackChargeTime_8() { return &___attackChargeTime_8; }
	inline void set_attackChargeTime_8(float value)
	{
		___attackChargeTime_8 = value;
	}

	inline static int32_t get_offset_of_m_HomeBaseAttackTimer_9() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___m_HomeBaseAttackTimer_9)); }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * get_m_HomeBaseAttackTimer_9() const { return ___m_HomeBaseAttackTimer_9; }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B ** get_address_of_m_HomeBaseAttackTimer_9() { return &___m_HomeBaseAttackTimer_9; }
	inline void set_m_HomeBaseAttackTimer_9(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * value)
	{
		___m_HomeBaseAttackTimer_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_HomeBaseAttackTimer_9), value);
	}

	inline static int32_t get_offset_of_m_IsChargingHomeBaseAttack_10() { return static_cast<int32_t>(offsetof(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E, ___m_IsChargingHomeBaseAttack_10)); }
	inline bool get_m_IsChargingHomeBaseAttack_10() const { return ___m_IsChargingHomeBaseAttack_10; }
	inline bool* get_address_of_m_IsChargingHomeBaseAttack_10() { return &___m_IsChargingHomeBaseAttack_10; }
	inline void set_m_IsChargingHomeBaseAttack_10(bool value)
	{
		___m_IsChargingHomeBaseAttack_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTACKBASE_TE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E_H
#ifndef BASEATTACKORDER_T664A0042F8A6891E3EEE41F5D2128C59BCC22EDF_H
#define BASEATTACKORDER_T664A0042F8A6891E3EEE41F5D2128C59BCC22EDF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// BaseAttackOrder
struct  BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// AttackBase BaseAttackOrder::attackBase
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E * ___attackBase_4;
	// UnityEngine.ParticleSystem BaseAttackOrder::attackPfx
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___attackPfx_5;
	// UnityEngine.Transform BaseAttackOrder::attackPoint
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___attackPoint_6;
	// UnityEngine.ParticleSystem BaseAttackOrder::chargePfx
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___chargePfx_7;
	// Health BaseAttackOrder::target
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___target_8;
	// System.Collections.Generic.List`1<Pathfinding.Agents.Agent> BaseAttackOrder::m_CurrentAgentsInside
	List_1_tDF5E7A97BF5CBC75075BAF34E7A7BCA1AE3E1A66 * ___m_CurrentAgentsInside_9;

public:
	inline static int32_t get_offset_of_attackBase_4() { return static_cast<int32_t>(offsetof(BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF, ___attackBase_4)); }
	inline AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E * get_attackBase_4() const { return ___attackBase_4; }
	inline AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E ** get_address_of_attackBase_4() { return &___attackBase_4; }
	inline void set_attackBase_4(AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E * value)
	{
		___attackBase_4 = value;
		Il2CppCodeGenWriteBarrier((&___attackBase_4), value);
	}

	inline static int32_t get_offset_of_attackPfx_5() { return static_cast<int32_t>(offsetof(BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF, ___attackPfx_5)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_attackPfx_5() const { return ___attackPfx_5; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_attackPfx_5() { return &___attackPfx_5; }
	inline void set_attackPfx_5(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___attackPfx_5 = value;
		Il2CppCodeGenWriteBarrier((&___attackPfx_5), value);
	}

	inline static int32_t get_offset_of_attackPoint_6() { return static_cast<int32_t>(offsetof(BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF, ___attackPoint_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_attackPoint_6() const { return ___attackPoint_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_attackPoint_6() { return &___attackPoint_6; }
	inline void set_attackPoint_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___attackPoint_6 = value;
		Il2CppCodeGenWriteBarrier((&___attackPoint_6), value);
	}

	inline static int32_t get_offset_of_chargePfx_7() { return static_cast<int32_t>(offsetof(BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF, ___chargePfx_7)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_chargePfx_7() const { return ___chargePfx_7; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_chargePfx_7() { return &___chargePfx_7; }
	inline void set_chargePfx_7(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___chargePfx_7 = value;
		Il2CppCodeGenWriteBarrier((&___chargePfx_7), value);
	}

	inline static int32_t get_offset_of_target_8() { return static_cast<int32_t>(offsetof(BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF, ___target_8)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_target_8() const { return ___target_8; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_target_8() { return &___target_8; }
	inline void set_target_8(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___target_8 = value;
		Il2CppCodeGenWriteBarrier((&___target_8), value);
	}

	inline static int32_t get_offset_of_m_CurrentAgentsInside_9() { return static_cast<int32_t>(offsetof(BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF, ___m_CurrentAgentsInside_9)); }
	inline List_1_tDF5E7A97BF5CBC75075BAF34E7A7BCA1AE3E1A66 * get_m_CurrentAgentsInside_9() const { return ___m_CurrentAgentsInside_9; }
	inline List_1_tDF5E7A97BF5CBC75075BAF34E7A7BCA1AE3E1A66 ** get_address_of_m_CurrentAgentsInside_9() { return &___m_CurrentAgentsInside_9; }
	inline void set_m_CurrentAgentsInside_9(List_1_tDF5E7A97BF5CBC75075BAF34E7A7BCA1AE3E1A66 * value)
	{
		___m_CurrentAgentsInside_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentAgentsInside_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEATTACKORDER_T664A0042F8A6891E3EEE41F5D2128C59BCC22EDF_H
#ifndef CAMERADISABLE_TFC35DE8BA18E0E594FC5ECC6DB57CDA0A9C33601_H
#define CAMERADISABLE_TFC35DE8BA18E0E594FC5ECC6DB57CDA0A9C33601_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraDisable
struct  CameraDisable_tFC35DE8BA18E0E594FC5ECC6DB57CDA0A9C33601  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CAMERADISABLE_TFC35DE8BA18E0E594FC5ECC6DB57CDA0A9C33601_H
#ifndef COMMANDLINECHECKER_T68AF719DB54C6031DC25AF154AF9203F539B725B_H
#define COMMANDLINECHECKER_T68AF719DB54C6031DC25AF154AF9203F539B725B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CommandLineChecker
struct  CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String CommandLineChecker::LaunchArgument
	String_t* ___LaunchArgument_4;
	// System.Int32 CommandLineChecker::LoadLevelId
	int32_t ___LoadLevelId_5;
	// System.Collections.Generic.List`1<System.String> CommandLineChecker::CommandArgs
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___CommandArgs_6;
	// GUIState CommandLineChecker::_guiState
	int32_t ____guiState_7;
	// UnityEngine.Rect CommandLineChecker::windowRect0
	Rect_t35B976DE901B5423C11705E156938EA27AB402CE  ___windowRect0_9;

public:
	inline static int32_t get_offset_of_LaunchArgument_4() { return static_cast<int32_t>(offsetof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B, ___LaunchArgument_4)); }
	inline String_t* get_LaunchArgument_4() const { return ___LaunchArgument_4; }
	inline String_t** get_address_of_LaunchArgument_4() { return &___LaunchArgument_4; }
	inline void set_LaunchArgument_4(String_t* value)
	{
		___LaunchArgument_4 = value;
		Il2CppCodeGenWriteBarrier((&___LaunchArgument_4), value);
	}

	inline static int32_t get_offset_of_LoadLevelId_5() { return static_cast<int32_t>(offsetof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B, ___LoadLevelId_5)); }
	inline int32_t get_LoadLevelId_5() const { return ___LoadLevelId_5; }
	inline int32_t* get_address_of_LoadLevelId_5() { return &___LoadLevelId_5; }
	inline void set_LoadLevelId_5(int32_t value)
	{
		___LoadLevelId_5 = value;
	}

	inline static int32_t get_offset_of_CommandArgs_6() { return static_cast<int32_t>(offsetof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B, ___CommandArgs_6)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_CommandArgs_6() const { return ___CommandArgs_6; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_CommandArgs_6() { return &___CommandArgs_6; }
	inline void set_CommandArgs_6(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___CommandArgs_6 = value;
		Il2CppCodeGenWriteBarrier((&___CommandArgs_6), value);
	}

	inline static int32_t get_offset_of__guiState_7() { return static_cast<int32_t>(offsetof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B, ____guiState_7)); }
	inline int32_t get__guiState_7() const { return ____guiState_7; }
	inline int32_t* get_address_of__guiState_7() { return &____guiState_7; }
	inline void set__guiState_7(int32_t value)
	{
		____guiState_7 = value;
	}

	inline static int32_t get_offset_of_windowRect0_9() { return static_cast<int32_t>(offsetof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B, ___windowRect0_9)); }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE  get_windowRect0_9() const { return ___windowRect0_9; }
	inline Rect_t35B976DE901B5423C11705E156938EA27AB402CE * get_address_of_windowRect0_9() { return &___windowRect0_9; }
	inline void set_windowRect0_9(Rect_t35B976DE901B5423C11705E156938EA27AB402CE  value)
	{
		___windowRect0_9 = value;
	}
};

struct CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B_StaticFields
{
public:
	// UnityEngine.Vector2 CommandLineChecker::windowSize
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___windowSize_8;

public:
	inline static int32_t get_offset_of_windowSize_8() { return static_cast<int32_t>(offsetof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B_StaticFields, ___windowSize_8)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_windowSize_8() const { return ___windowSize_8; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_windowSize_8() { return &___windowSize_8; }
	inline void set_windowSize_8(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___windowSize_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMANDLINECHECKER_T68AF719DB54C6031DC25AF154AF9203F539B725B_H
#ifndef SINGLETON_1_T92032249C7BB318D3055215153531C57A9D8F21B_H
#define SINGLETON_1_T92032249C7BB318D3055215153531C57A9D8F21B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Lose>
struct  Singleton_1_t92032249C7BB318D3055215153531C57A9D8F21B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t92032249C7BB318D3055215153531C57A9D8F21B_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t92032249C7BB318D3055215153531C57A9D8F21B_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T92032249C7BB318D3055215153531C57A9D8F21B_H
#ifndef SINGLETON_1_TD582104BEBDCF0AAD5A8466B04D5F64431BF09D5_H
#define SINGLETON_1_TD582104BEBDCF0AAD5A8466B04D5F64431BF09D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<PlacementManager>
struct  Singleton_1_tD582104BEBDCF0AAD5A8466B04D5F64431BF09D5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tD582104BEBDCF0AAD5A8466B04D5F64431BF09D5_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_tD582104BEBDCF0AAD5A8466B04D5F64431BF09D5_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TD582104BEBDCF0AAD5A8466B04D5F64431BF09D5_H
#ifndef SINGLETON_1_T25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD_H
#define SINGLETON_1_T25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Restart>
struct  Singleton_1_t25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD_H
#ifndef SINGLETON_1_TDFB0300C50E783DC7ADC89FC760E2D37B50C8679_H
#define SINGLETON_1_TDFB0300C50E783DC7ADC89FC760E2D37B50C8679_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Score>
struct  Singleton_1_tDFB0300C50E783DC7ADC89FC760E2D37B50C8679  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_tDFB0300C50E783DC7ADC89FC760E2D37B50C8679_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	Score_t72F7EE757BE7D4C7846803B3072753760AB6427F * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_tDFB0300C50E783DC7ADC89FC760E2D37B50C8679_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Score_t72F7EE757BE7D4C7846803B3072753760AB6427F * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Score_t72F7EE757BE7D4C7846803B3072753760AB6427F ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Score_t72F7EE757BE7D4C7846803B3072753760AB6427F * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_TDFB0300C50E783DC7ADC89FC760E2D37B50C8679_H
#ifndef SINGLETON_1_T20844B3009FAC77AFA3AF5511ACADA1C33792153_H
#define SINGLETON_1_T20844B3009FAC77AFA3AF5511ACADA1C33792153_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<ScreenFade>
struct  Singleton_1_t20844B3009FAC77AFA3AF5511ACADA1C33792153  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t20844B3009FAC77AFA3AF5511ACADA1C33792153_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t20844B3009FAC77AFA3AF5511ACADA1C33792153_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T20844B3009FAC77AFA3AF5511ACADA1C33792153_H
#ifndef SINGLETON_1_T01CF12AA8045F973A4FE8095AE0CA184EBEAEF92_H
#define SINGLETON_1_T01CF12AA8045F973A4FE8095AE0CA184EBEAEF92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<UnityDeeplinks>
struct  Singleton_1_t01CF12AA8045F973A4FE8095AE0CA184EBEAEF92  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t01CF12AA8045F973A4FE8095AE0CA184EBEAEF92_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t01CF12AA8045F973A4FE8095AE0CA184EBEAEF92_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T01CF12AA8045F973A4FE8095AE0CA184EBEAEF92_H
#ifndef SINGLETON_1_T89728DDB3ED716070C09E898F1CB1BA527DF4257_H
#define SINGLETON_1_T89728DDB3ED716070C09E898F1CB1BA527DF4257_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Core.Utilities.Singleton`1<Win>
struct  Singleton_1_t89728DDB3ED716070C09E898F1CB1BA527DF4257  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

struct Singleton_1_t89728DDB3ED716070C09E898F1CB1BA527DF4257_StaticFields
{
public:
	// T Core.Utilities.Singleton`1::<Instance>k__BackingField
	Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24 * ___U3CInstanceU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(Singleton_1_t89728DDB3ED716070C09E898F1CB1BA527DF4257_StaticFields, ___U3CInstanceU3Ek__BackingField_4)); }
	inline Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24 * get_U3CInstanceU3Ek__BackingField_4() const { return ___U3CInstanceU3Ek__BackingField_4; }
	inline Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24 ** get_address_of_U3CInstanceU3Ek__BackingField_4() { return &___U3CInstanceU3Ek__BackingField_4; }
	inline void set_U3CInstanceU3Ek__BackingField_4(Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24 * value)
	{
		___U3CInstanceU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETON_1_T89728DDB3ED716070C09E898F1CB1BA527DF4257_H
#ifndef DEATHEFFECT_TEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050_H
#define DEATHEFFECT_TEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DeathEffect
struct  DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.ParticleSystem DeathEffect::deathParticleSystemPrefab
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___deathParticleSystemPrefab_4;
	// UnityEngine.Vector3 DeathEffect::deathEffectOffset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___deathEffectOffset_5;
	// Health DeathEffect::health
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___health_6;

public:
	inline static int32_t get_offset_of_deathParticleSystemPrefab_4() { return static_cast<int32_t>(offsetof(DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050, ___deathParticleSystemPrefab_4)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_deathParticleSystemPrefab_4() const { return ___deathParticleSystemPrefab_4; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_deathParticleSystemPrefab_4() { return &___deathParticleSystemPrefab_4; }
	inline void set_deathParticleSystemPrefab_4(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___deathParticleSystemPrefab_4 = value;
		Il2CppCodeGenWriteBarrier((&___deathParticleSystemPrefab_4), value);
	}

	inline static int32_t get_offset_of_deathEffectOffset_5() { return static_cast<int32_t>(offsetof(DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050, ___deathEffectOffset_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_deathEffectOffset_5() const { return ___deathEffectOffset_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_deathEffectOffset_5() { return &___deathEffectOffset_5; }
	inline void set_deathEffectOffset_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___deathEffectOffset_5 = value;
	}

	inline static int32_t get_offset_of_health_6() { return static_cast<int32_t>(offsetof(DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050, ___health_6)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_health_6() const { return ___health_6; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_health_6() { return &___health_6; }
	inline void set_health_6(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___health_6 = value;
		Il2CppCodeGenWriteBarrier((&___health_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEATHEFFECT_TEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050_H
#ifndef EDGE_T537CEFC8CC07A43A6D7658B0F4DBE06BCDEC1507_H
#define EDGE_T537CEFC8CC07A43A6D7658B0F4DBE06BCDEC1507_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Edge
struct  Edge_t537CEFC8CC07A43A6D7658B0F4DBE06BCDEC1507  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EDGE_T537CEFC8CC07A43A6D7658B0F4DBE06BCDEC1507_H
#ifndef EVENTSYSTEMCHECKER_T3DE6F6D9672E49F03867024BEBA4169B6EFD3DB6_H
#define EVENTSYSTEMCHECKER_T3DE6F6D9672E49F03867024BEBA4169B6EFD3DB6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// EventSystemChecker
struct  EventSystemChecker_t3DE6F6D9672E49F03867024BEBA4169B6EFD3DB6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTSYSTEMCHECKER_T3DE6F6D9672E49F03867024BEBA4169B6EFD3DB6_H
#ifndef FORCEDRESET_T8D0ECEC1BFEDF13E0142F6F50190FC9C36CA6C05_H
#define FORCEDRESET_T8D0ECEC1BFEDF13E0142F6F50190FC9C36CA6C05_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ForcedReset
struct  ForcedReset_t8D0ECEC1BFEDF13E0142F6F50190FC9C36CA6C05  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORCEDRESET_T8D0ECEC1BFEDF13E0142F6F50190FC9C36CA6C05_H
#ifndef GAMEEVENTHANDLER_TFF35528BFF12FD8289A5380DDB1A755ABCE84917_H
#define GAMEEVENTHANDLER_TFF35528BFF12FD8289A5380DDB1A755ABCE84917_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEventHandler
struct  GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<EventParam> GameEventHandler::playerDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___playerDead_4;
	// System.Action`1<EventParam> GameEventHandler::winScore
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___winScore_5;
	// System.Action`1<EventParam> GameEventHandler::enemyDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___enemyDead_6;
	// System.Single GameEventHandler::<WinScoreValue>k__BackingField
	float ___U3CWinScoreValueU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of_playerDead_4() { return static_cast<int32_t>(offsetof(GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917, ___playerDead_4)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_playerDead_4() const { return ___playerDead_4; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_playerDead_4() { return &___playerDead_4; }
	inline void set_playerDead_4(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___playerDead_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerDead_4), value);
	}

	inline static int32_t get_offset_of_winScore_5() { return static_cast<int32_t>(offsetof(GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917, ___winScore_5)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_winScore_5() const { return ___winScore_5; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_winScore_5() { return &___winScore_5; }
	inline void set_winScore_5(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___winScore_5 = value;
		Il2CppCodeGenWriteBarrier((&___winScore_5), value);
	}

	inline static int32_t get_offset_of_enemyDead_6() { return static_cast<int32_t>(offsetof(GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917, ___enemyDead_6)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_enemyDead_6() const { return ___enemyDead_6; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_enemyDead_6() { return &___enemyDead_6; }
	inline void set_enemyDead_6(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___enemyDead_6 = value;
		Il2CppCodeGenWriteBarrier((&___enemyDead_6), value);
	}

	inline static int32_t get_offset_of_U3CWinScoreValueU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917, ___U3CWinScoreValueU3Ek__BackingField_7)); }
	inline float get_U3CWinScoreValueU3Ek__BackingField_7() const { return ___U3CWinScoreValueU3Ek__BackingField_7; }
	inline float* get_address_of_U3CWinScoreValueU3Ek__BackingField_7() { return &___U3CWinScoreValueU3Ek__BackingField_7; }
	inline void set_U3CWinScoreValueU3Ek__BackingField_7(float value)
	{
		___U3CWinScoreValueU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTHANDLER_TFF35528BFF12FD8289A5380DDB1A755ABCE84917_H
#ifndef GAMEEVENTMANAGER_T11533E03B357685A7E3C1CD74A336EEFAC995C16_H
#define GAMEEVENTMANAGER_T11533E03B357685A7E3C1CD74A336EEFAC995C16_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// GameEventManager
struct  GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Action`1<EventParam>> GameEventManager::eventDictionary
	Dictionary_2_t84B5ABCC68D567AEA18910D7D8084A794F7412B5 * ___eventDictionary_4;

public:
	inline static int32_t get_offset_of_eventDictionary_4() { return static_cast<int32_t>(offsetof(GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16, ___eventDictionary_4)); }
	inline Dictionary_2_t84B5ABCC68D567AEA18910D7D8084A794F7412B5 * get_eventDictionary_4() const { return ___eventDictionary_4; }
	inline Dictionary_2_t84B5ABCC68D567AEA18910D7D8084A794F7412B5 ** get_address_of_eventDictionary_4() { return &___eventDictionary_4; }
	inline void set_eventDictionary_4(Dictionary_2_t84B5ABCC68D567AEA18910D7D8084A794F7412B5 * value)
	{
		___eventDictionary_4 = value;
		Il2CppCodeGenWriteBarrier((&___eventDictionary_4), value);
	}
};

struct GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16_StaticFields
{
public:
	// GameEventManager GameEventManager::gameEventManager
	GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16 * ___gameEventManager_5;

public:
	inline static int32_t get_offset_of_gameEventManager_5() { return static_cast<int32_t>(offsetof(GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16_StaticFields, ___gameEventManager_5)); }
	inline GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16 * get_gameEventManager_5() const { return ___gameEventManager_5; }
	inline GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16 ** get_address_of_gameEventManager_5() { return &___gameEventManager_5; }
	inline void set_gameEventManager_5(GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16 * value)
	{
		___gameEventManager_5 = value;
		Il2CppCodeGenWriteBarrier((&___gameEventManager_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GAMEEVENTMANAGER_T11533E03B357685A7E3C1CD74A336EEFAC995C16_H
#ifndef HEALTH_TBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D_H
#define HEALTH_TBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Health
struct  Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Health.SerializableIAlignmentProvider Health::alignment
	SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * ___alignment_4;
	// Core.Health.IAlignmentProvider Health::alignmentProvider
	RuntimeObject* ___alignmentProvider_5;
	// System.Single Health::MaxHealth
	float ___MaxHealth_6;
	// System.Single Health::CurrentHealth
	float ___CurrentHealth_7;
	// System.Action`1<Health> Health::removed
	Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 * ___removed_8;
	// System.Action Health::healthChanged
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___healthChanged_9;
	// System.Action Health::died
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___died_10;

public:
	inline static int32_t get_offset_of_alignment_4() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___alignment_4)); }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * get_alignment_4() const { return ___alignment_4; }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 ** get_address_of_alignment_4() { return &___alignment_4; }
	inline void set_alignment_4(SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * value)
	{
		___alignment_4 = value;
		Il2CppCodeGenWriteBarrier((&___alignment_4), value);
	}

	inline static int32_t get_offset_of_alignmentProvider_5() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___alignmentProvider_5)); }
	inline RuntimeObject* get_alignmentProvider_5() const { return ___alignmentProvider_5; }
	inline RuntimeObject** get_address_of_alignmentProvider_5() { return &___alignmentProvider_5; }
	inline void set_alignmentProvider_5(RuntimeObject* value)
	{
		___alignmentProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentProvider_5), value);
	}

	inline static int32_t get_offset_of_MaxHealth_6() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___MaxHealth_6)); }
	inline float get_MaxHealth_6() const { return ___MaxHealth_6; }
	inline float* get_address_of_MaxHealth_6() { return &___MaxHealth_6; }
	inline void set_MaxHealth_6(float value)
	{
		___MaxHealth_6 = value;
	}

	inline static int32_t get_offset_of_CurrentHealth_7() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___CurrentHealth_7)); }
	inline float get_CurrentHealth_7() const { return ___CurrentHealth_7; }
	inline float* get_address_of_CurrentHealth_7() { return &___CurrentHealth_7; }
	inline void set_CurrentHealth_7(float value)
	{
		___CurrentHealth_7 = value;
	}

	inline static int32_t get_offset_of_removed_8() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___removed_8)); }
	inline Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 * get_removed_8() const { return ___removed_8; }
	inline Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 ** get_address_of_removed_8() { return &___removed_8; }
	inline void set_removed_8(Action_1_t001094C4FAC15A904C20F73F81BD57C81EBC71C4 * value)
	{
		___removed_8 = value;
		Il2CppCodeGenWriteBarrier((&___removed_8), value);
	}

	inline static int32_t get_offset_of_healthChanged_9() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___healthChanged_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_healthChanged_9() const { return ___healthChanged_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_healthChanged_9() { return &___healthChanged_9; }
	inline void set_healthChanged_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___healthChanged_9 = value;
		Il2CppCodeGenWriteBarrier((&___healthChanged_9), value);
	}

	inline static int32_t get_offset_of_died_10() { return static_cast<int32_t>(offsetof(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D, ___died_10)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_died_10() const { return ___died_10; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_died_10() { return &___died_10; }
	inline void set_died_10(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___died_10 = value;
		Il2CppCodeGenWriteBarrier((&___died_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTH_TBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D_H
#ifndef HEALTHVISUALIZER_T02E5D62DC19403A3940E800D331346BC34CB09A1_H
#define HEALTHVISUALIZER_T02E5D62DC19403A3940E800D331346BC34CB09A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HealthVisualizer
struct  HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Health HealthVisualizer::health
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * ___health_4;
	// UnityEngine.Transform HealthVisualizer::healthBar
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___healthBar_5;
	// UnityEngine.Transform HealthVisualizer::backgroundBar
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___backgroundBar_6;
	// System.Boolean HealthVisualizer::showWhenFull
	bool ___showWhenFull_7;
	// UnityEngine.Transform HealthVisualizer::m_CameraToFace
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___m_CameraToFace_8;

public:
	inline static int32_t get_offset_of_health_4() { return static_cast<int32_t>(offsetof(HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1, ___health_4)); }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * get_health_4() const { return ___health_4; }
	inline Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D ** get_address_of_health_4() { return &___health_4; }
	inline void set_health_4(Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D * value)
	{
		___health_4 = value;
		Il2CppCodeGenWriteBarrier((&___health_4), value);
	}

	inline static int32_t get_offset_of_healthBar_5() { return static_cast<int32_t>(offsetof(HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1, ___healthBar_5)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_healthBar_5() const { return ___healthBar_5; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_healthBar_5() { return &___healthBar_5; }
	inline void set_healthBar_5(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___healthBar_5 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_5), value);
	}

	inline static int32_t get_offset_of_backgroundBar_6() { return static_cast<int32_t>(offsetof(HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1, ___backgroundBar_6)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_backgroundBar_6() const { return ___backgroundBar_6; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_backgroundBar_6() { return &___backgroundBar_6; }
	inline void set_backgroundBar_6(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___backgroundBar_6 = value;
		Il2CppCodeGenWriteBarrier((&___backgroundBar_6), value);
	}

	inline static int32_t get_offset_of_showWhenFull_7() { return static_cast<int32_t>(offsetof(HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1, ___showWhenFull_7)); }
	inline bool get_showWhenFull_7() const { return ___showWhenFull_7; }
	inline bool* get_address_of_showWhenFull_7() { return &___showWhenFull_7; }
	inline void set_showWhenFull_7(bool value)
	{
		___showWhenFull_7 = value;
	}

	inline static int32_t get_offset_of_m_CameraToFace_8() { return static_cast<int32_t>(offsetof(HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1, ___m_CameraToFace_8)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_m_CameraToFace_8() const { return ___m_CameraToFace_8; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_m_CameraToFace_8() { return &___m_CameraToFace_8; }
	inline void set_m_CameraToFace_8(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___m_CameraToFace_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_CameraToFace_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEALTHVISUALIZER_T02E5D62DC19403A3940E800D331346BC34CB09A1_H
#ifndef INVASION_T0D8DEDF915C57F49CA4A0CB15EE75703DC38E054_H
#define INVASION_T0D8DEDF915C57F49CA4A0CB15EE75703DC38E054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Invasion
struct  Invasion_t0D8DEDF915C57F49CA4A0CB15EE75703DC38E054  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INVASION_T0D8DEDF915C57F49CA4A0CB15EE75703DC38E054_H
#ifndef LAUNCHER_T27DF47051ABB500108780CEA7ABCF602164A0602_H
#define LAUNCHER_T27DF47051ABB500108780CEA7ABCF602164A0602_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Launcher
struct  Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// MHLab.PATCH.LauncherManager Launcher::m_launcher
	LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61 * ___m_launcher_4;
	// MHLab.PATCH.Install.InstallManager Launcher::m_installer
	InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990 * ___m_installer_5;
	// System.Boolean Launcher::ActivatePatcher
	bool ___ActivatePatcher_6;
	// System.String Launcher::VersionsFileDownloadURL
	String_t* ___VersionsFileDownloadURL_7;
	// System.String Launcher::PatchesDirectoryURL
	String_t* ___PatchesDirectoryURL_8;
	// System.String Launcher::AppToLaunch
	String_t* ___AppToLaunch_9;
	// System.Boolean Launcher::CloseLauncherOnStart
	bool ___CloseLauncherOnStart_10;
	// System.String Launcher::Argument
	String_t* ___Argument_11;
	// System.Boolean Launcher::UseRawArgument
	bool ___UseRawArgument_12;
	// System.Boolean Launcher::IsIntegrated
	bool ___IsIntegrated_13;
	// System.Int32 Launcher::SceneToLoad
	int32_t ___SceneToLoad_14;
	// System.Boolean Launcher::ActivateInstaller
	bool ___ActivateInstaller_15;
	// System.Boolean Launcher::ActivateRepairer
	bool ___ActivateRepairer_16;
	// System.String Launcher::BuildsDirectoryURL
	String_t* ___BuildsDirectoryURL_17;
	// System.String Launcher::LauncherName
	String_t* ___LauncherName_18;
	// System.Boolean Launcher::InstallInLocalPath
	bool ___InstallInLocalPath_19;
	// System.String Launcher::ProgramFilesDirectoryToInstall
	String_t* ___ProgramFilesDirectoryToInstall_20;
	// System.Boolean Launcher::CreateDesktopShortcut
	bool ___CreateDesktopShortcut_21;
	// System.UInt16 Launcher::DownloadRetryAttempts
	uint16_t ___DownloadRetryAttempts_22;
	// System.Boolean Launcher::EnableCredentials
	bool ___EnableCredentials_23;
	// System.String Launcher::Username
	String_t* ___Username_24;
	// System.String Launcher::Password
	String_t* ___Password_25;
	// ProgressBar Launcher::MainBar
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * ___MainBar_26;
	// ProgressBar Launcher::DetailBar
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * ___DetailBar_27;
	// UnityEngine.UI.Text Launcher::MainLog
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___MainLog_28;
	// UnityEngine.UI.Text Launcher::DetailLog
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___DetailLog_29;
	// UnityEngine.UI.Button Launcher::LaunchButton
	Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * ___LaunchButton_30;
	// UnityEngine.RectTransform Launcher::Overlay
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___Overlay_31;
	// UnityEngine.RectTransform Launcher::MainMenu
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___MainMenu_32;
	// UnityEngine.RectTransform Launcher::RestartMenu
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___RestartMenu_33;
	// UnityEngine.RectTransform Launcher::OptionsMenu
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___OptionsMenu_34;
	// UnityThreading.ActionThread Launcher::m_updateCheckingThread
	ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB * ___m_updateCheckingThread_35;
	// System.DateTime Launcher::_lastTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ____lastTime_36;
	// System.Int64 Launcher::_lastSize
	int64_t ____lastSize_37;
	// System.Int32 Launcher::_downloadSpeed
	int32_t ____downloadSpeed_38;

public:
	inline static int32_t get_offset_of_m_launcher_4() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___m_launcher_4)); }
	inline LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61 * get_m_launcher_4() const { return ___m_launcher_4; }
	inline LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61 ** get_address_of_m_launcher_4() { return &___m_launcher_4; }
	inline void set_m_launcher_4(LauncherManager_tA5EA158E3347DBF833C1A015EFBAE39A10537D61 * value)
	{
		___m_launcher_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_launcher_4), value);
	}

	inline static int32_t get_offset_of_m_installer_5() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___m_installer_5)); }
	inline InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990 * get_m_installer_5() const { return ___m_installer_5; }
	inline InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990 ** get_address_of_m_installer_5() { return &___m_installer_5; }
	inline void set_m_installer_5(InstallManager_t6A16071C2DDC3B23EF861B128B2E2EC152A30990 * value)
	{
		___m_installer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_installer_5), value);
	}

	inline static int32_t get_offset_of_ActivatePatcher_6() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___ActivatePatcher_6)); }
	inline bool get_ActivatePatcher_6() const { return ___ActivatePatcher_6; }
	inline bool* get_address_of_ActivatePatcher_6() { return &___ActivatePatcher_6; }
	inline void set_ActivatePatcher_6(bool value)
	{
		___ActivatePatcher_6 = value;
	}

	inline static int32_t get_offset_of_VersionsFileDownloadURL_7() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___VersionsFileDownloadURL_7)); }
	inline String_t* get_VersionsFileDownloadURL_7() const { return ___VersionsFileDownloadURL_7; }
	inline String_t** get_address_of_VersionsFileDownloadURL_7() { return &___VersionsFileDownloadURL_7; }
	inline void set_VersionsFileDownloadURL_7(String_t* value)
	{
		___VersionsFileDownloadURL_7 = value;
		Il2CppCodeGenWriteBarrier((&___VersionsFileDownloadURL_7), value);
	}

	inline static int32_t get_offset_of_PatchesDirectoryURL_8() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___PatchesDirectoryURL_8)); }
	inline String_t* get_PatchesDirectoryURL_8() const { return ___PatchesDirectoryURL_8; }
	inline String_t** get_address_of_PatchesDirectoryURL_8() { return &___PatchesDirectoryURL_8; }
	inline void set_PatchesDirectoryURL_8(String_t* value)
	{
		___PatchesDirectoryURL_8 = value;
		Il2CppCodeGenWriteBarrier((&___PatchesDirectoryURL_8), value);
	}

	inline static int32_t get_offset_of_AppToLaunch_9() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___AppToLaunch_9)); }
	inline String_t* get_AppToLaunch_9() const { return ___AppToLaunch_9; }
	inline String_t** get_address_of_AppToLaunch_9() { return &___AppToLaunch_9; }
	inline void set_AppToLaunch_9(String_t* value)
	{
		___AppToLaunch_9 = value;
		Il2CppCodeGenWriteBarrier((&___AppToLaunch_9), value);
	}

	inline static int32_t get_offset_of_CloseLauncherOnStart_10() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___CloseLauncherOnStart_10)); }
	inline bool get_CloseLauncherOnStart_10() const { return ___CloseLauncherOnStart_10; }
	inline bool* get_address_of_CloseLauncherOnStart_10() { return &___CloseLauncherOnStart_10; }
	inline void set_CloseLauncherOnStart_10(bool value)
	{
		___CloseLauncherOnStart_10 = value;
	}

	inline static int32_t get_offset_of_Argument_11() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___Argument_11)); }
	inline String_t* get_Argument_11() const { return ___Argument_11; }
	inline String_t** get_address_of_Argument_11() { return &___Argument_11; }
	inline void set_Argument_11(String_t* value)
	{
		___Argument_11 = value;
		Il2CppCodeGenWriteBarrier((&___Argument_11), value);
	}

	inline static int32_t get_offset_of_UseRawArgument_12() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___UseRawArgument_12)); }
	inline bool get_UseRawArgument_12() const { return ___UseRawArgument_12; }
	inline bool* get_address_of_UseRawArgument_12() { return &___UseRawArgument_12; }
	inline void set_UseRawArgument_12(bool value)
	{
		___UseRawArgument_12 = value;
	}

	inline static int32_t get_offset_of_IsIntegrated_13() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___IsIntegrated_13)); }
	inline bool get_IsIntegrated_13() const { return ___IsIntegrated_13; }
	inline bool* get_address_of_IsIntegrated_13() { return &___IsIntegrated_13; }
	inline void set_IsIntegrated_13(bool value)
	{
		___IsIntegrated_13 = value;
	}

	inline static int32_t get_offset_of_SceneToLoad_14() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___SceneToLoad_14)); }
	inline int32_t get_SceneToLoad_14() const { return ___SceneToLoad_14; }
	inline int32_t* get_address_of_SceneToLoad_14() { return &___SceneToLoad_14; }
	inline void set_SceneToLoad_14(int32_t value)
	{
		___SceneToLoad_14 = value;
	}

	inline static int32_t get_offset_of_ActivateInstaller_15() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___ActivateInstaller_15)); }
	inline bool get_ActivateInstaller_15() const { return ___ActivateInstaller_15; }
	inline bool* get_address_of_ActivateInstaller_15() { return &___ActivateInstaller_15; }
	inline void set_ActivateInstaller_15(bool value)
	{
		___ActivateInstaller_15 = value;
	}

	inline static int32_t get_offset_of_ActivateRepairer_16() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___ActivateRepairer_16)); }
	inline bool get_ActivateRepairer_16() const { return ___ActivateRepairer_16; }
	inline bool* get_address_of_ActivateRepairer_16() { return &___ActivateRepairer_16; }
	inline void set_ActivateRepairer_16(bool value)
	{
		___ActivateRepairer_16 = value;
	}

	inline static int32_t get_offset_of_BuildsDirectoryURL_17() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___BuildsDirectoryURL_17)); }
	inline String_t* get_BuildsDirectoryURL_17() const { return ___BuildsDirectoryURL_17; }
	inline String_t** get_address_of_BuildsDirectoryURL_17() { return &___BuildsDirectoryURL_17; }
	inline void set_BuildsDirectoryURL_17(String_t* value)
	{
		___BuildsDirectoryURL_17 = value;
		Il2CppCodeGenWriteBarrier((&___BuildsDirectoryURL_17), value);
	}

	inline static int32_t get_offset_of_LauncherName_18() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___LauncherName_18)); }
	inline String_t* get_LauncherName_18() const { return ___LauncherName_18; }
	inline String_t** get_address_of_LauncherName_18() { return &___LauncherName_18; }
	inline void set_LauncherName_18(String_t* value)
	{
		___LauncherName_18 = value;
		Il2CppCodeGenWriteBarrier((&___LauncherName_18), value);
	}

	inline static int32_t get_offset_of_InstallInLocalPath_19() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___InstallInLocalPath_19)); }
	inline bool get_InstallInLocalPath_19() const { return ___InstallInLocalPath_19; }
	inline bool* get_address_of_InstallInLocalPath_19() { return &___InstallInLocalPath_19; }
	inline void set_InstallInLocalPath_19(bool value)
	{
		___InstallInLocalPath_19 = value;
	}

	inline static int32_t get_offset_of_ProgramFilesDirectoryToInstall_20() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___ProgramFilesDirectoryToInstall_20)); }
	inline String_t* get_ProgramFilesDirectoryToInstall_20() const { return ___ProgramFilesDirectoryToInstall_20; }
	inline String_t** get_address_of_ProgramFilesDirectoryToInstall_20() { return &___ProgramFilesDirectoryToInstall_20; }
	inline void set_ProgramFilesDirectoryToInstall_20(String_t* value)
	{
		___ProgramFilesDirectoryToInstall_20 = value;
		Il2CppCodeGenWriteBarrier((&___ProgramFilesDirectoryToInstall_20), value);
	}

	inline static int32_t get_offset_of_CreateDesktopShortcut_21() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___CreateDesktopShortcut_21)); }
	inline bool get_CreateDesktopShortcut_21() const { return ___CreateDesktopShortcut_21; }
	inline bool* get_address_of_CreateDesktopShortcut_21() { return &___CreateDesktopShortcut_21; }
	inline void set_CreateDesktopShortcut_21(bool value)
	{
		___CreateDesktopShortcut_21 = value;
	}

	inline static int32_t get_offset_of_DownloadRetryAttempts_22() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___DownloadRetryAttempts_22)); }
	inline uint16_t get_DownloadRetryAttempts_22() const { return ___DownloadRetryAttempts_22; }
	inline uint16_t* get_address_of_DownloadRetryAttempts_22() { return &___DownloadRetryAttempts_22; }
	inline void set_DownloadRetryAttempts_22(uint16_t value)
	{
		___DownloadRetryAttempts_22 = value;
	}

	inline static int32_t get_offset_of_EnableCredentials_23() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___EnableCredentials_23)); }
	inline bool get_EnableCredentials_23() const { return ___EnableCredentials_23; }
	inline bool* get_address_of_EnableCredentials_23() { return &___EnableCredentials_23; }
	inline void set_EnableCredentials_23(bool value)
	{
		___EnableCredentials_23 = value;
	}

	inline static int32_t get_offset_of_Username_24() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___Username_24)); }
	inline String_t* get_Username_24() const { return ___Username_24; }
	inline String_t** get_address_of_Username_24() { return &___Username_24; }
	inline void set_Username_24(String_t* value)
	{
		___Username_24 = value;
		Il2CppCodeGenWriteBarrier((&___Username_24), value);
	}

	inline static int32_t get_offset_of_Password_25() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___Password_25)); }
	inline String_t* get_Password_25() const { return ___Password_25; }
	inline String_t** get_address_of_Password_25() { return &___Password_25; }
	inline void set_Password_25(String_t* value)
	{
		___Password_25 = value;
		Il2CppCodeGenWriteBarrier((&___Password_25), value);
	}

	inline static int32_t get_offset_of_MainBar_26() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___MainBar_26)); }
	inline ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * get_MainBar_26() const { return ___MainBar_26; }
	inline ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C ** get_address_of_MainBar_26() { return &___MainBar_26; }
	inline void set_MainBar_26(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * value)
	{
		___MainBar_26 = value;
		Il2CppCodeGenWriteBarrier((&___MainBar_26), value);
	}

	inline static int32_t get_offset_of_DetailBar_27() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___DetailBar_27)); }
	inline ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * get_DetailBar_27() const { return ___DetailBar_27; }
	inline ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C ** get_address_of_DetailBar_27() { return &___DetailBar_27; }
	inline void set_DetailBar_27(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C * value)
	{
		___DetailBar_27 = value;
		Il2CppCodeGenWriteBarrier((&___DetailBar_27), value);
	}

	inline static int32_t get_offset_of_MainLog_28() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___MainLog_28)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_MainLog_28() const { return ___MainLog_28; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_MainLog_28() { return &___MainLog_28; }
	inline void set_MainLog_28(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___MainLog_28 = value;
		Il2CppCodeGenWriteBarrier((&___MainLog_28), value);
	}

	inline static int32_t get_offset_of_DetailLog_29() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___DetailLog_29)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_DetailLog_29() const { return ___DetailLog_29; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_DetailLog_29() { return &___DetailLog_29; }
	inline void set_DetailLog_29(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___DetailLog_29 = value;
		Il2CppCodeGenWriteBarrier((&___DetailLog_29), value);
	}

	inline static int32_t get_offset_of_LaunchButton_30() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___LaunchButton_30)); }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * get_LaunchButton_30() const { return ___LaunchButton_30; }
	inline Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B ** get_address_of_LaunchButton_30() { return &___LaunchButton_30; }
	inline void set_LaunchButton_30(Button_t1203820000D5513FDCCE3D4BFF9C1C9CC755CC2B * value)
	{
		___LaunchButton_30 = value;
		Il2CppCodeGenWriteBarrier((&___LaunchButton_30), value);
	}

	inline static int32_t get_offset_of_Overlay_31() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___Overlay_31)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_Overlay_31() const { return ___Overlay_31; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_Overlay_31() { return &___Overlay_31; }
	inline void set_Overlay_31(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___Overlay_31 = value;
		Il2CppCodeGenWriteBarrier((&___Overlay_31), value);
	}

	inline static int32_t get_offset_of_MainMenu_32() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___MainMenu_32)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_MainMenu_32() const { return ___MainMenu_32; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_MainMenu_32() { return &___MainMenu_32; }
	inline void set_MainMenu_32(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___MainMenu_32 = value;
		Il2CppCodeGenWriteBarrier((&___MainMenu_32), value);
	}

	inline static int32_t get_offset_of_RestartMenu_33() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___RestartMenu_33)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_RestartMenu_33() const { return ___RestartMenu_33; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_RestartMenu_33() { return &___RestartMenu_33; }
	inline void set_RestartMenu_33(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___RestartMenu_33 = value;
		Il2CppCodeGenWriteBarrier((&___RestartMenu_33), value);
	}

	inline static int32_t get_offset_of_OptionsMenu_34() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___OptionsMenu_34)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_OptionsMenu_34() const { return ___OptionsMenu_34; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_OptionsMenu_34() { return &___OptionsMenu_34; }
	inline void set_OptionsMenu_34(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___OptionsMenu_34 = value;
		Il2CppCodeGenWriteBarrier((&___OptionsMenu_34), value);
	}

	inline static int32_t get_offset_of_m_updateCheckingThread_35() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ___m_updateCheckingThread_35)); }
	inline ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB * get_m_updateCheckingThread_35() const { return ___m_updateCheckingThread_35; }
	inline ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB ** get_address_of_m_updateCheckingThread_35() { return &___m_updateCheckingThread_35; }
	inline void set_m_updateCheckingThread_35(ActionThread_t3DD80C24E69D827BB2DE777B7928D4A7385E3EDB * value)
	{
		___m_updateCheckingThread_35 = value;
		Il2CppCodeGenWriteBarrier((&___m_updateCheckingThread_35), value);
	}

	inline static int32_t get_offset_of__lastTime_36() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ____lastTime_36)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get__lastTime_36() const { return ____lastTime_36; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of__lastTime_36() { return &____lastTime_36; }
	inline void set__lastTime_36(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		____lastTime_36 = value;
	}

	inline static int32_t get_offset_of__lastSize_37() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ____lastSize_37)); }
	inline int64_t get__lastSize_37() const { return ____lastSize_37; }
	inline int64_t* get_address_of__lastSize_37() { return &____lastSize_37; }
	inline void set__lastSize_37(int64_t value)
	{
		____lastSize_37 = value;
	}

	inline static int32_t get_offset_of__downloadSpeed_38() { return static_cast<int32_t>(offsetof(Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602, ____downloadSpeed_38)); }
	inline int32_t get__downloadSpeed_38() const { return ____downloadSpeed_38; }
	inline int32_t* get_address_of__downloadSpeed_38() { return &____downloadSpeed_38; }
	inline void set__downloadSpeed_38(int32_t value)
	{
		____downloadSpeed_38 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAUNCHER_T27DF47051ABB500108780CEA7ABCF602164A0602_H
#ifndef LOCKONTARGET_T727DCADAC20A8B4EBFF3C90F8A643D39FE436283_H
#define LOCKONTARGET_T727DCADAC20A8B4EBFF3C90F8A643D39FE436283_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LockOnTarget
struct  LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Health.SerializableIAlignmentProvider LockOnTarget::alignment
	SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * ___alignment_4;
	// Core.Health.IAlignmentProvider LockOnTarget::alignmentProvider
	RuntimeObject* ___alignmentProvider_5;
	// System.Action`1<CanTarget> LockOnTarget::targetEntersRange
	Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * ___targetEntersRange_6;
	// System.Action`1<CanTarget> LockOnTarget::targetExitsRange
	Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * ___targetExitsRange_7;
	// System.Action`1<CanTarget> LockOnTarget::acquiredTarget
	Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * ___acquiredTarget_8;
	// System.Action LockOnTarget::lostTarget
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___lostTarget_9;
	// UnityEngine.Transform LockOnTarget::turret
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___turret_10;
	// UnityEngine.Vector2 LockOnTarget::turretXRotationRange
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___turretXRotationRange_11;
	// System.Boolean LockOnTarget::onlyYTurretRotation
	bool ___onlyYTurretRotation_12;
	// System.Single LockOnTarget::searchRate
	float ___searchRate_13;
	// System.Single LockOnTarget::idleRotationSpeed
	float ___idleRotationSpeed_14;
	// System.Single LockOnTarget::idleCorrectionTime
	float ___idleCorrectionTime_15;
	// UnityEngine.Collider LockOnTarget::attachedCollider
	Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * ___attachedCollider_16;
	// System.Single LockOnTarget::idleWaitTime
	float ___idleWaitTime_17;
	// System.Collections.Generic.List`1<CanTarget> LockOnTarget::m_TargetsInRange
	List_1_t94FF8EBDACEF9DD31F7052AB0CC280E85F7EAA6F * ___m_TargetsInRange_18;
	// System.Single LockOnTarget::m_SearchTimer
	float ___m_SearchTimer_19;
	// System.Single LockOnTarget::m_WaitTimer
	float ___m_WaitTimer_20;
	// CanTarget LockOnTarget::m_CurrentCanTarget
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * ___m_CurrentCanTarget_21;
	// System.Single LockOnTarget::m_XRotationCorrectionTime
	float ___m_XRotationCorrectionTime_22;
	// System.Boolean LockOnTarget::m_HadTarget
	bool ___m_HadTarget_23;
	// System.Single LockOnTarget::m_CurrentRotationSpeed
	float ___m_CurrentRotationSpeed_24;

public:
	inline static int32_t get_offset_of_alignment_4() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___alignment_4)); }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * get_alignment_4() const { return ___alignment_4; }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 ** get_address_of_alignment_4() { return &___alignment_4; }
	inline void set_alignment_4(SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * value)
	{
		___alignment_4 = value;
		Il2CppCodeGenWriteBarrier((&___alignment_4), value);
	}

	inline static int32_t get_offset_of_alignmentProvider_5() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___alignmentProvider_5)); }
	inline RuntimeObject* get_alignmentProvider_5() const { return ___alignmentProvider_5; }
	inline RuntimeObject** get_address_of_alignmentProvider_5() { return &___alignmentProvider_5; }
	inline void set_alignmentProvider_5(RuntimeObject* value)
	{
		___alignmentProvider_5 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentProvider_5), value);
	}

	inline static int32_t get_offset_of_targetEntersRange_6() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___targetEntersRange_6)); }
	inline Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * get_targetEntersRange_6() const { return ___targetEntersRange_6; }
	inline Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 ** get_address_of_targetEntersRange_6() { return &___targetEntersRange_6; }
	inline void set_targetEntersRange_6(Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * value)
	{
		___targetEntersRange_6 = value;
		Il2CppCodeGenWriteBarrier((&___targetEntersRange_6), value);
	}

	inline static int32_t get_offset_of_targetExitsRange_7() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___targetExitsRange_7)); }
	inline Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * get_targetExitsRange_7() const { return ___targetExitsRange_7; }
	inline Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 ** get_address_of_targetExitsRange_7() { return &___targetExitsRange_7; }
	inline void set_targetExitsRange_7(Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * value)
	{
		___targetExitsRange_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetExitsRange_7), value);
	}

	inline static int32_t get_offset_of_acquiredTarget_8() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___acquiredTarget_8)); }
	inline Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * get_acquiredTarget_8() const { return ___acquiredTarget_8; }
	inline Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 ** get_address_of_acquiredTarget_8() { return &___acquiredTarget_8; }
	inline void set_acquiredTarget_8(Action_1_t1596CE486A9C6E9EE8E73C8545A46F1AF05F82E4 * value)
	{
		___acquiredTarget_8 = value;
		Il2CppCodeGenWriteBarrier((&___acquiredTarget_8), value);
	}

	inline static int32_t get_offset_of_lostTarget_9() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___lostTarget_9)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_lostTarget_9() const { return ___lostTarget_9; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_lostTarget_9() { return &___lostTarget_9; }
	inline void set_lostTarget_9(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___lostTarget_9 = value;
		Il2CppCodeGenWriteBarrier((&___lostTarget_9), value);
	}

	inline static int32_t get_offset_of_turret_10() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___turret_10)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_turret_10() const { return ___turret_10; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_turret_10() { return &___turret_10; }
	inline void set_turret_10(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___turret_10 = value;
		Il2CppCodeGenWriteBarrier((&___turret_10), value);
	}

	inline static int32_t get_offset_of_turretXRotationRange_11() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___turretXRotationRange_11)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_turretXRotationRange_11() const { return ___turretXRotationRange_11; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_turretXRotationRange_11() { return &___turretXRotationRange_11; }
	inline void set_turretXRotationRange_11(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___turretXRotationRange_11 = value;
	}

	inline static int32_t get_offset_of_onlyYTurretRotation_12() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___onlyYTurretRotation_12)); }
	inline bool get_onlyYTurretRotation_12() const { return ___onlyYTurretRotation_12; }
	inline bool* get_address_of_onlyYTurretRotation_12() { return &___onlyYTurretRotation_12; }
	inline void set_onlyYTurretRotation_12(bool value)
	{
		___onlyYTurretRotation_12 = value;
	}

	inline static int32_t get_offset_of_searchRate_13() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___searchRate_13)); }
	inline float get_searchRate_13() const { return ___searchRate_13; }
	inline float* get_address_of_searchRate_13() { return &___searchRate_13; }
	inline void set_searchRate_13(float value)
	{
		___searchRate_13 = value;
	}

	inline static int32_t get_offset_of_idleRotationSpeed_14() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___idleRotationSpeed_14)); }
	inline float get_idleRotationSpeed_14() const { return ___idleRotationSpeed_14; }
	inline float* get_address_of_idleRotationSpeed_14() { return &___idleRotationSpeed_14; }
	inline void set_idleRotationSpeed_14(float value)
	{
		___idleRotationSpeed_14 = value;
	}

	inline static int32_t get_offset_of_idleCorrectionTime_15() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___idleCorrectionTime_15)); }
	inline float get_idleCorrectionTime_15() const { return ___idleCorrectionTime_15; }
	inline float* get_address_of_idleCorrectionTime_15() { return &___idleCorrectionTime_15; }
	inline void set_idleCorrectionTime_15(float value)
	{
		___idleCorrectionTime_15 = value;
	}

	inline static int32_t get_offset_of_attachedCollider_16() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___attachedCollider_16)); }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * get_attachedCollider_16() const { return ___attachedCollider_16; }
	inline Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF ** get_address_of_attachedCollider_16() { return &___attachedCollider_16; }
	inline void set_attachedCollider_16(Collider_t0FEEB36760860AD21B3B1F0509C365B393EC4BDF * value)
	{
		___attachedCollider_16 = value;
		Il2CppCodeGenWriteBarrier((&___attachedCollider_16), value);
	}

	inline static int32_t get_offset_of_idleWaitTime_17() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___idleWaitTime_17)); }
	inline float get_idleWaitTime_17() const { return ___idleWaitTime_17; }
	inline float* get_address_of_idleWaitTime_17() { return &___idleWaitTime_17; }
	inline void set_idleWaitTime_17(float value)
	{
		___idleWaitTime_17 = value;
	}

	inline static int32_t get_offset_of_m_TargetsInRange_18() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_TargetsInRange_18)); }
	inline List_1_t94FF8EBDACEF9DD31F7052AB0CC280E85F7EAA6F * get_m_TargetsInRange_18() const { return ___m_TargetsInRange_18; }
	inline List_1_t94FF8EBDACEF9DD31F7052AB0CC280E85F7EAA6F ** get_address_of_m_TargetsInRange_18() { return &___m_TargetsInRange_18; }
	inline void set_m_TargetsInRange_18(List_1_t94FF8EBDACEF9DD31F7052AB0CC280E85F7EAA6F * value)
	{
		___m_TargetsInRange_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TargetsInRange_18), value);
	}

	inline static int32_t get_offset_of_m_SearchTimer_19() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_SearchTimer_19)); }
	inline float get_m_SearchTimer_19() const { return ___m_SearchTimer_19; }
	inline float* get_address_of_m_SearchTimer_19() { return &___m_SearchTimer_19; }
	inline void set_m_SearchTimer_19(float value)
	{
		___m_SearchTimer_19 = value;
	}

	inline static int32_t get_offset_of_m_WaitTimer_20() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_WaitTimer_20)); }
	inline float get_m_WaitTimer_20() const { return ___m_WaitTimer_20; }
	inline float* get_address_of_m_WaitTimer_20() { return &___m_WaitTimer_20; }
	inline void set_m_WaitTimer_20(float value)
	{
		___m_WaitTimer_20 = value;
	}

	inline static int32_t get_offset_of_m_CurrentCanTarget_21() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_CurrentCanTarget_21)); }
	inline CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * get_m_CurrentCanTarget_21() const { return ___m_CurrentCanTarget_21; }
	inline CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 ** get_address_of_m_CurrentCanTarget_21() { return &___m_CurrentCanTarget_21; }
	inline void set_m_CurrentCanTarget_21(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * value)
	{
		___m_CurrentCanTarget_21 = value;
		Il2CppCodeGenWriteBarrier((&___m_CurrentCanTarget_21), value);
	}

	inline static int32_t get_offset_of_m_XRotationCorrectionTime_22() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_XRotationCorrectionTime_22)); }
	inline float get_m_XRotationCorrectionTime_22() const { return ___m_XRotationCorrectionTime_22; }
	inline float* get_address_of_m_XRotationCorrectionTime_22() { return &___m_XRotationCorrectionTime_22; }
	inline void set_m_XRotationCorrectionTime_22(float value)
	{
		___m_XRotationCorrectionTime_22 = value;
	}

	inline static int32_t get_offset_of_m_HadTarget_23() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_HadTarget_23)); }
	inline bool get_m_HadTarget_23() const { return ___m_HadTarget_23; }
	inline bool* get_address_of_m_HadTarget_23() { return &___m_HadTarget_23; }
	inline void set_m_HadTarget_23(bool value)
	{
		___m_HadTarget_23 = value;
	}

	inline static int32_t get_offset_of_m_CurrentRotationSpeed_24() { return static_cast<int32_t>(offsetof(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283, ___m_CurrentRotationSpeed_24)); }
	inline float get_m_CurrentRotationSpeed_24() const { return ___m_CurrentRotationSpeed_24; }
	inline float* get_address_of_m_CurrentRotationSpeed_24() { return &___m_CurrentRotationSpeed_24; }
	inline void set_m_CurrentRotationSpeed_24(float value)
	{
		___m_CurrentRotationSpeed_24 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCKONTARGET_T727DCADAC20A8B4EBFF3C90F8A643D39FE436283_H
#ifndef MENUEVENTHANDLER_T20B78868BE68C2673919DB632F87B209543F98C9_H
#define MENUEVENTHANDLER_T20B78868BE68C2673919DB632F87B209543F98C9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MenuEventHandler
struct  MenuEventHandler_t20B78868BE68C2673919DB632F87B209543F98C9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<EventParam> MenuEventHandler::newGame
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___newGame_4;
	// System.Action`1<EventParam> MenuEventHandler::quit
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___quit_5;

public:
	inline static int32_t get_offset_of_newGame_4() { return static_cast<int32_t>(offsetof(MenuEventHandler_t20B78868BE68C2673919DB632F87B209543F98C9, ___newGame_4)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_newGame_4() const { return ___newGame_4; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_newGame_4() { return &___newGame_4; }
	inline void set_newGame_4(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___newGame_4 = value;
		Il2CppCodeGenWriteBarrier((&___newGame_4), value);
	}

	inline static int32_t get_offset_of_quit_5() { return static_cast<int32_t>(offsetof(MenuEventHandler_t20B78868BE68C2673919DB632F87B209543F98C9, ___quit_5)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_quit_5() const { return ___quit_5; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_quit_5() { return &___quit_5; }
	inline void set_quit_5(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___quit_5 = value;
		Il2CppCodeGenWriteBarrier((&___quit_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MENUEVENTHANDLER_T20B78868BE68C2673919DB632F87B209543F98C9_H
#ifndef MOVETOWARDSTARGET_T086585A65E43D5FFABFBC3D00BE48157070D410E_H
#define MOVETOWARDSTARGET_T086585A65E43D5FFABFBC3D00BE48157070D410E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// MoveTowardsTarget
struct  MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject MoveTowardsTarget::target
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___target_4;
	// System.Single MoveTowardsTarget::movementMultiplier
	float ___movementMultiplier_5;
	// System.Single MoveTowardsTarget::attackSpeedMultiplier
	float ___attackSpeedMultiplier_6;
	// System.Single MoveTowardsTarget::distanceToAttack
	float ___distanceToAttack_7;
	// System.Single MoveTowardsTarget::distanceToMove
	float ___distanceToMove_8;
	// System.Single MoveTowardsTarget::degreesPerSecond
	float ___degreesPerSecond_9;
	// System.Boolean MoveTowardsTarget::beginMoving
	bool ___beginMoving_10;
	// System.Boolean MoveTowardsTarget::beginAttacking
	bool ___beginAttacking_11;
	// UnityEngine.Animator MoveTowardsTarget::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_12;
	// UnityEngine.Quaternion MoveTowardsTarget::startRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___startRotation_13;
	// UnityEngine.Quaternion MoveTowardsTarget::endRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___endRotation_14;
	// System.Single MoveTowardsTarget::delta
	float ___delta_15;
	// System.Single MoveTowardsTarget::attackTickDuration
	float ___attackTickDuration_16;
	// System.Single MoveTowardsTarget::lastAttackTick
	float ___lastAttackTick_17;
	// System.Action MoveTowardsTarget::OnAttackTick
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___OnAttackTick_18;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___target_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_target_4() const { return ___target_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_movementMultiplier_5() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___movementMultiplier_5)); }
	inline float get_movementMultiplier_5() const { return ___movementMultiplier_5; }
	inline float* get_address_of_movementMultiplier_5() { return &___movementMultiplier_5; }
	inline void set_movementMultiplier_5(float value)
	{
		___movementMultiplier_5 = value;
	}

	inline static int32_t get_offset_of_attackSpeedMultiplier_6() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___attackSpeedMultiplier_6)); }
	inline float get_attackSpeedMultiplier_6() const { return ___attackSpeedMultiplier_6; }
	inline float* get_address_of_attackSpeedMultiplier_6() { return &___attackSpeedMultiplier_6; }
	inline void set_attackSpeedMultiplier_6(float value)
	{
		___attackSpeedMultiplier_6 = value;
	}

	inline static int32_t get_offset_of_distanceToAttack_7() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___distanceToAttack_7)); }
	inline float get_distanceToAttack_7() const { return ___distanceToAttack_7; }
	inline float* get_address_of_distanceToAttack_7() { return &___distanceToAttack_7; }
	inline void set_distanceToAttack_7(float value)
	{
		___distanceToAttack_7 = value;
	}

	inline static int32_t get_offset_of_distanceToMove_8() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___distanceToMove_8)); }
	inline float get_distanceToMove_8() const { return ___distanceToMove_8; }
	inline float* get_address_of_distanceToMove_8() { return &___distanceToMove_8; }
	inline void set_distanceToMove_8(float value)
	{
		___distanceToMove_8 = value;
	}

	inline static int32_t get_offset_of_degreesPerSecond_9() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___degreesPerSecond_9)); }
	inline float get_degreesPerSecond_9() const { return ___degreesPerSecond_9; }
	inline float* get_address_of_degreesPerSecond_9() { return &___degreesPerSecond_9; }
	inline void set_degreesPerSecond_9(float value)
	{
		___degreesPerSecond_9 = value;
	}

	inline static int32_t get_offset_of_beginMoving_10() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___beginMoving_10)); }
	inline bool get_beginMoving_10() const { return ___beginMoving_10; }
	inline bool* get_address_of_beginMoving_10() { return &___beginMoving_10; }
	inline void set_beginMoving_10(bool value)
	{
		___beginMoving_10 = value;
	}

	inline static int32_t get_offset_of_beginAttacking_11() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___beginAttacking_11)); }
	inline bool get_beginAttacking_11() const { return ___beginAttacking_11; }
	inline bool* get_address_of_beginAttacking_11() { return &___beginAttacking_11; }
	inline void set_beginAttacking_11(bool value)
	{
		___beginAttacking_11 = value;
	}

	inline static int32_t get_offset_of_animator_12() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___animator_12)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_12() const { return ___animator_12; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_12() { return &___animator_12; }
	inline void set_animator_12(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_12 = value;
		Il2CppCodeGenWriteBarrier((&___animator_12), value);
	}

	inline static int32_t get_offset_of_startRotation_13() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___startRotation_13)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_startRotation_13() const { return ___startRotation_13; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_startRotation_13() { return &___startRotation_13; }
	inline void set_startRotation_13(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___startRotation_13 = value;
	}

	inline static int32_t get_offset_of_endRotation_14() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___endRotation_14)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_endRotation_14() const { return ___endRotation_14; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_endRotation_14() { return &___endRotation_14; }
	inline void set_endRotation_14(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___endRotation_14 = value;
	}

	inline static int32_t get_offset_of_delta_15() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___delta_15)); }
	inline float get_delta_15() const { return ___delta_15; }
	inline float* get_address_of_delta_15() { return &___delta_15; }
	inline void set_delta_15(float value)
	{
		___delta_15 = value;
	}

	inline static int32_t get_offset_of_attackTickDuration_16() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___attackTickDuration_16)); }
	inline float get_attackTickDuration_16() const { return ___attackTickDuration_16; }
	inline float* get_address_of_attackTickDuration_16() { return &___attackTickDuration_16; }
	inline void set_attackTickDuration_16(float value)
	{
		___attackTickDuration_16 = value;
	}

	inline static int32_t get_offset_of_lastAttackTick_17() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___lastAttackTick_17)); }
	inline float get_lastAttackTick_17() const { return ___lastAttackTick_17; }
	inline float* get_address_of_lastAttackTick_17() { return &___lastAttackTick_17; }
	inline void set_lastAttackTick_17(float value)
	{
		___lastAttackTick_17 = value;
	}

	inline static int32_t get_offset_of_OnAttackTick_18() { return static_cast<int32_t>(offsetof(MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E, ___OnAttackTick_18)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_OnAttackTick_18() const { return ___OnAttackTick_18; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_OnAttackTick_18() { return &___OnAttackTick_18; }
	inline void set_OnAttackTick_18(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___OnAttackTick_18 = value;
		Il2CppCodeGenWriteBarrier((&___OnAttackTick_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MOVETOWARDSTARGET_T086585A65E43D5FFABFBC3D00BE48157070D410E_H
#ifndef ONCLICKGAMEEVENTTRIGGER_T9030AEB71286BC0F29BF2E08659240A420F6BFD3_H
#define ONCLICKGAMEEVENTTRIGGER_T9030AEB71286BC0F29BF2E08659240A420F6BFD3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// OnClickGameEventTrigger
struct  OnClickGameEventTrigger_t9030AEB71286BC0F29BF2E08659240A420F6BFD3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String OnClickGameEventTrigger::gameEventToTrigger
	String_t* ___gameEventToTrigger_4;

public:
	inline static int32_t get_offset_of_gameEventToTrigger_4() { return static_cast<int32_t>(offsetof(OnClickGameEventTrigger_t9030AEB71286BC0F29BF2E08659240A420F6BFD3, ___gameEventToTrigger_4)); }
	inline String_t* get_gameEventToTrigger_4() const { return ___gameEventToTrigger_4; }
	inline String_t** get_address_of_gameEventToTrigger_4() { return &___gameEventToTrigger_4; }
	inline void set_gameEventToTrigger_4(String_t* value)
	{
		___gameEventToTrigger_4 = value;
		Il2CppCodeGenWriteBarrier((&___gameEventToTrigger_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ONCLICKGAMEEVENTTRIGGER_T9030AEB71286BC0F29BF2E08659240A420F6BFD3_H
#ifndef PARTICLEPROJECTILE_T29371705B536834A390DE525D11C2F84C8CB5FF4_H
#define PARTICLEPROJECTILE_T29371705B536834A390DE525D11C2F84C8CB5FF4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ParticleProjectile
struct  ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single ParticleProjectile::delay
	float ___delay_4;
	// Core.Utilities.Timer ParticleProjectile::m_Timer
	Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * ___m_Timer_5;
	// System.Single ParticleProjectile::damage
	float ___damage_6;
	// Core.Health.IAlignmentProvider ParticleProjectile::alignmentProvider
	RuntimeObject* ___alignmentProvider_7;
	// CanTarget ParticleProjectile::m_Enemy
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * ___m_Enemy_8;
	// UnityEngine.ParticleSystem ParticleProjectile::fireParticleSystem
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___fireParticleSystem_9;
	// UnityEngine.ParticleSystem ParticleProjectile::collisionParticles
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___collisionParticles_10;
	// System.Boolean ParticleProjectile::m_PauseTimer
	bool ___m_PauseTimer_11;

public:
	inline static int32_t get_offset_of_delay_4() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___delay_4)); }
	inline float get_delay_4() const { return ___delay_4; }
	inline float* get_address_of_delay_4() { return &___delay_4; }
	inline void set_delay_4(float value)
	{
		___delay_4 = value;
	}

	inline static int32_t get_offset_of_m_Timer_5() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___m_Timer_5)); }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * get_m_Timer_5() const { return ___m_Timer_5; }
	inline Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B ** get_address_of_m_Timer_5() { return &___m_Timer_5; }
	inline void set_m_Timer_5(Timer_tAA66D657EFD530564D7F0ABC86482FF91BC89C1B * value)
	{
		___m_Timer_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Timer_5), value);
	}

	inline static int32_t get_offset_of_damage_6() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___damage_6)); }
	inline float get_damage_6() const { return ___damage_6; }
	inline float* get_address_of_damage_6() { return &___damage_6; }
	inline void set_damage_6(float value)
	{
		___damage_6 = value;
	}

	inline static int32_t get_offset_of_alignmentProvider_7() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___alignmentProvider_7)); }
	inline RuntimeObject* get_alignmentProvider_7() const { return ___alignmentProvider_7; }
	inline RuntimeObject** get_address_of_alignmentProvider_7() { return &___alignmentProvider_7; }
	inline void set_alignmentProvider_7(RuntimeObject* value)
	{
		___alignmentProvider_7 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentProvider_7), value);
	}

	inline static int32_t get_offset_of_m_Enemy_8() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___m_Enemy_8)); }
	inline CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * get_m_Enemy_8() const { return ___m_Enemy_8; }
	inline CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 ** get_address_of_m_Enemy_8() { return &___m_Enemy_8; }
	inline void set_m_Enemy_8(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * value)
	{
		___m_Enemy_8 = value;
		Il2CppCodeGenWriteBarrier((&___m_Enemy_8), value);
	}

	inline static int32_t get_offset_of_fireParticleSystem_9() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___fireParticleSystem_9)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_fireParticleSystem_9() const { return ___fireParticleSystem_9; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_fireParticleSystem_9() { return &___fireParticleSystem_9; }
	inline void set_fireParticleSystem_9(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___fireParticleSystem_9 = value;
		Il2CppCodeGenWriteBarrier((&___fireParticleSystem_9), value);
	}

	inline static int32_t get_offset_of_collisionParticles_10() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___collisionParticles_10)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_collisionParticles_10() const { return ___collisionParticles_10; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_collisionParticles_10() { return &___collisionParticles_10; }
	inline void set_collisionParticles_10(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___collisionParticles_10 = value;
		Il2CppCodeGenWriteBarrier((&___collisionParticles_10), value);
	}

	inline static int32_t get_offset_of_m_PauseTimer_11() { return static_cast<int32_t>(offsetof(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4, ___m_PauseTimer_11)); }
	inline bool get_m_PauseTimer_11() const { return ___m_PauseTimer_11; }
	inline bool* get_address_of_m_PauseTimer_11() { return &___m_PauseTimer_11; }
	inline void set_m_PauseTimer_11(bool value)
	{
		___m_PauseTimer_11 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLEPROJECTILE_T29371705B536834A390DE525D11C2F84C8CB5FF4_H
#ifndef PAUSE_TC27C5C290DF55219F3ACD2CD1CD2D6E8254EA5A5_H
#define PAUSE_TC27C5C290DF55219F3ACD2CD1CD2D6E8254EA5A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Pause
struct  Pause_tC27C5C290DF55219F3ACD2CD1CD2D6E8254EA5A5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PAUSE_TC27C5C290DF55219F3ACD2CD1CD2D6E8254EA5A5_H
#ifndef PLACEABLESURFACE_TB529C5A5A7869931E85E639C82EED07271F7B5A3_H
#define PLACEABLESURFACE_TB529C5A5A7869931E85E639C82EED07271F7B5A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlaceableSurface
struct  PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single PlaceableSurface::gridUnit
	float ___gridUnit_4;
	// System.Int32 PlaceableSurface::<xMax>k__BackingField
	int32_t ___U3CxMaxU3Ek__BackingField_5;
	// System.Int32 PlaceableSurface::<zMax>k__BackingField
	int32_t ___U3CzMaxU3Ek__BackingField_6;
	// System.Collections.Generic.Dictionary`2<UnityEngine.Vector2,UnityEngine.GameObject> PlaceableSurface::placedObjects
	Dictionary_2_t4BD15E4883130EE3934C68F0D6F5E4EC63344BA9 * ___placedObjects_7;

public:
	inline static int32_t get_offset_of_gridUnit_4() { return static_cast<int32_t>(offsetof(PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3, ___gridUnit_4)); }
	inline float get_gridUnit_4() const { return ___gridUnit_4; }
	inline float* get_address_of_gridUnit_4() { return &___gridUnit_4; }
	inline void set_gridUnit_4(float value)
	{
		___gridUnit_4 = value;
	}

	inline static int32_t get_offset_of_U3CxMaxU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3, ___U3CxMaxU3Ek__BackingField_5)); }
	inline int32_t get_U3CxMaxU3Ek__BackingField_5() const { return ___U3CxMaxU3Ek__BackingField_5; }
	inline int32_t* get_address_of_U3CxMaxU3Ek__BackingField_5() { return &___U3CxMaxU3Ek__BackingField_5; }
	inline void set_U3CxMaxU3Ek__BackingField_5(int32_t value)
	{
		___U3CxMaxU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CzMaxU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3, ___U3CzMaxU3Ek__BackingField_6)); }
	inline int32_t get_U3CzMaxU3Ek__BackingField_6() const { return ___U3CzMaxU3Ek__BackingField_6; }
	inline int32_t* get_address_of_U3CzMaxU3Ek__BackingField_6() { return &___U3CzMaxU3Ek__BackingField_6; }
	inline void set_U3CzMaxU3Ek__BackingField_6(int32_t value)
	{
		___U3CzMaxU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_placedObjects_7() { return static_cast<int32_t>(offsetof(PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3, ___placedObjects_7)); }
	inline Dictionary_2_t4BD15E4883130EE3934C68F0D6F5E4EC63344BA9 * get_placedObjects_7() const { return ___placedObjects_7; }
	inline Dictionary_2_t4BD15E4883130EE3934C68F0D6F5E4EC63344BA9 ** get_address_of_placedObjects_7() { return &___placedObjects_7; }
	inline void set_placedObjects_7(Dictionary_2_t4BD15E4883130EE3934C68F0D6F5E4EC63344BA9 * value)
	{
		___placedObjects_7 = value;
		Il2CppCodeGenWriteBarrier((&___placedObjects_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEABLESURFACE_TB529C5A5A7869931E85E639C82EED07271F7B5A3_H
#ifndef PROGRESSBAR_TAEBA7E1A59C678A8651AEF196DDD334625C6164C_H
#define PROGRESSBAR_TAEBA7E1A59C678A8651AEF196DDD334625C6164C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ProgressBar
struct  ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RectTransform ProgressBar::_rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____rect_4;
	// UnityEngine.UI.Text ProgressBar::_progressText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ____progressText_5;
	// System.Single ProgressBar::_currentProgress
	float ____currentProgress_6;
	// System.Single ProgressBar::_maximumValue
	float ____maximumValue_7;
	// System.Single ProgressBar::_minimumValue
	float ____minimumValue_8;
	// System.Single ProgressBar::_stepValue
	float ____stepValue_9;
	// UnityEngine.Vector3 ProgressBar::_localScale
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ____localScale_10;

public:
	inline static int32_t get_offset_of__rect_4() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____rect_4)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__rect_4() const { return ____rect_4; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__rect_4() { return &____rect_4; }
	inline void set__rect_4(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____rect_4 = value;
		Il2CppCodeGenWriteBarrier((&____rect_4), value);
	}

	inline static int32_t get_offset_of__progressText_5() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____progressText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get__progressText_5() const { return ____progressText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of__progressText_5() { return &____progressText_5; }
	inline void set__progressText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		____progressText_5 = value;
		Il2CppCodeGenWriteBarrier((&____progressText_5), value);
	}

	inline static int32_t get_offset_of__currentProgress_6() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____currentProgress_6)); }
	inline float get__currentProgress_6() const { return ____currentProgress_6; }
	inline float* get_address_of__currentProgress_6() { return &____currentProgress_6; }
	inline void set__currentProgress_6(float value)
	{
		____currentProgress_6 = value;
	}

	inline static int32_t get_offset_of__maximumValue_7() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____maximumValue_7)); }
	inline float get__maximumValue_7() const { return ____maximumValue_7; }
	inline float* get_address_of__maximumValue_7() { return &____maximumValue_7; }
	inline void set__maximumValue_7(float value)
	{
		____maximumValue_7 = value;
	}

	inline static int32_t get_offset_of__minimumValue_8() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____minimumValue_8)); }
	inline float get__minimumValue_8() const { return ____minimumValue_8; }
	inline float* get_address_of__minimumValue_8() { return &____minimumValue_8; }
	inline void set__minimumValue_8(float value)
	{
		____minimumValue_8 = value;
	}

	inline static int32_t get_offset_of__stepValue_9() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____stepValue_9)); }
	inline float get__stepValue_9() const { return ____stepValue_9; }
	inline float* get_address_of__stepValue_9() { return &____stepValue_9; }
	inline void set__stepValue_9(float value)
	{
		____stepValue_9 = value;
	}

	inline static int32_t get_offset_of__localScale_10() { return static_cast<int32_t>(offsetof(ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C, ____localScale_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get__localScale_10() const { return ____localScale_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of__localScale_10() { return &____localScale_10; }
	inline void set__localScale_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		____localScale_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRESSBAR_TAEBA7E1A59C678A8651AEF196DDD334625C6164C_H
#ifndef PROJECTILE_T84D26E956289868C2E474F1A0C2A5EC645EAFC6D_H
#define PROJECTILE_T84D26E956289868C2E474F1A0C2A5EC645EAFC6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Projectile
struct  Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single Projectile::projectileSpeed
	float ___projectileSpeed_4;
	// System.Single Projectile::damageValue
	float ___damageValue_5;
	// System.Single Projectile::lifeSpan
	float ___lifeSpan_6;
	// UnityEngine.GameObject Projectile::onHitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onHitEffect_7;
	// UnityEngine.Vector3 Projectile::spawnPoint
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___spawnPoint_8;
	// UnityEngine.GameObject Projectile::spawnEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___spawnEffect_9;
	// System.Boolean Projectile::fired
	bool ___fired_10;
	// Core.Health.IAlignmentProvider Projectile::alignmentProvider
	RuntimeObject* ___alignmentProvider_11;

public:
	inline static int32_t get_offset_of_projectileSpeed_4() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___projectileSpeed_4)); }
	inline float get_projectileSpeed_4() const { return ___projectileSpeed_4; }
	inline float* get_address_of_projectileSpeed_4() { return &___projectileSpeed_4; }
	inline void set_projectileSpeed_4(float value)
	{
		___projectileSpeed_4 = value;
	}

	inline static int32_t get_offset_of_damageValue_5() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___damageValue_5)); }
	inline float get_damageValue_5() const { return ___damageValue_5; }
	inline float* get_address_of_damageValue_5() { return &___damageValue_5; }
	inline void set_damageValue_5(float value)
	{
		___damageValue_5 = value;
	}

	inline static int32_t get_offset_of_lifeSpan_6() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___lifeSpan_6)); }
	inline float get_lifeSpan_6() const { return ___lifeSpan_6; }
	inline float* get_address_of_lifeSpan_6() { return &___lifeSpan_6; }
	inline void set_lifeSpan_6(float value)
	{
		___lifeSpan_6 = value;
	}

	inline static int32_t get_offset_of_onHitEffect_7() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___onHitEffect_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_onHitEffect_7() const { return ___onHitEffect_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_onHitEffect_7() { return &___onHitEffect_7; }
	inline void set_onHitEffect_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___onHitEffect_7 = value;
		Il2CppCodeGenWriteBarrier((&___onHitEffect_7), value);
	}

	inline static int32_t get_offset_of_spawnPoint_8() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___spawnPoint_8)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_spawnPoint_8() const { return ___spawnPoint_8; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_spawnPoint_8() { return &___spawnPoint_8; }
	inline void set_spawnPoint_8(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___spawnPoint_8 = value;
	}

	inline static int32_t get_offset_of_spawnEffect_9() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___spawnEffect_9)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_spawnEffect_9() const { return ___spawnEffect_9; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_spawnEffect_9() { return &___spawnEffect_9; }
	inline void set_spawnEffect_9(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___spawnEffect_9 = value;
		Il2CppCodeGenWriteBarrier((&___spawnEffect_9), value);
	}

	inline static int32_t get_offset_of_fired_10() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___fired_10)); }
	inline bool get_fired_10() const { return ___fired_10; }
	inline bool* get_address_of_fired_10() { return &___fired_10; }
	inline void set_fired_10(bool value)
	{
		___fired_10 = value;
	}

	inline static int32_t get_offset_of_alignmentProvider_11() { return static_cast<int32_t>(offsetof(Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D, ___alignmentProvider_11)); }
	inline RuntimeObject* get_alignmentProvider_11() const { return ___alignmentProvider_11; }
	inline RuntimeObject** get_address_of_alignmentProvider_11() { return &___alignmentProvider_11; }
	inline void set_alignmentProvider_11(RuntimeObject* value)
	{
		___alignmentProvider_11 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentProvider_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROJECTILE_T84D26E956289868C2E474F1A0C2A5EC645EAFC6D_H
#ifndef RECTLOOKATTARGET_T79F7A65C8D237262B735E4E715E09A94740392DC_H
#define RECTLOOKATTARGET_T79F7A65C8D237262B735E4E715E09A94740392DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RectLookAtTarget
struct  RectLookAtTarget_t79F7A65C8D237262B735E4E715E09A94740392DC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject RectLookAtTarget::target
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___target_4;
	// UnityEngine.RectTransform RectLookAtTarget::rect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ___rect_5;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(RectLookAtTarget_t79F7A65C8D237262B735E4E715E09A94740392DC, ___target_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_target_4() const { return ___target_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_rect_5() { return static_cast<int32_t>(offsetof(RectLookAtTarget_t79F7A65C8D237262B735E4E715E09A94740392DC, ___rect_5)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get_rect_5() const { return ___rect_5; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of_rect_5() { return &___rect_5; }
	inline void set_rect_5(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		___rect_5 = value;
		Il2CppCodeGenWriteBarrier((&___rect_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RECTLOOKATTARGET_T79F7A65C8D237262B735E4E715E09A94740392DC_H
#ifndef RESTARTMENU_TA4A63306081E7D79860F1DA13F90B839FA3EF7FC_H
#define RESTARTMENU_TA4A63306081E7D79860F1DA13F90B839FA3EF7FC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestartMenu
struct  RestartMenu_tA4A63306081E7D79860F1DA13F90B839FA3EF7FC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<EventParam> RestartMenu::restartMenu
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___restartMenu_4;
	// UnityEngine.GameObject RestartMenu::restartUI
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___restartUI_5;

public:
	inline static int32_t get_offset_of_restartMenu_4() { return static_cast<int32_t>(offsetof(RestartMenu_tA4A63306081E7D79860F1DA13F90B839FA3EF7FC, ___restartMenu_4)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_restartMenu_4() const { return ___restartMenu_4; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_restartMenu_4() { return &___restartMenu_4; }
	inline void set_restartMenu_4(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___restartMenu_4 = value;
		Il2CppCodeGenWriteBarrier((&___restartMenu_4), value);
	}

	inline static int32_t get_offset_of_restartUI_5() { return static_cast<int32_t>(offsetof(RestartMenu_tA4A63306081E7D79860F1DA13F90B839FA3EF7FC, ___restartUI_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_restartUI_5() const { return ___restartUI_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_restartUI_5() { return &___restartUI_5; }
	inline void set_restartUI_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___restartUI_5 = value;
		Il2CppCodeGenWriteBarrier((&___restartUI_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTARTMENU_TA4A63306081E7D79860F1DA13F90B839FA3EF7FC_H
#ifndef SHOOTER_T66D7662B87125C892730A0E48F2442B684535B8D_H
#define SHOOTER_T66D7662B87125C892730A0E48F2442B684535B8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Shooter
struct  Shooter_t66D7662B87125C892730A0E48F2442B684535B8D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject Shooter::SpawnEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___SpawnEffect_4;
	// UnityEngine.GameObject Shooter::projectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectile_5;
	// UnityEngine.GameObject Shooter::HitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___HitEffect_6;

public:
	inline static int32_t get_offset_of_SpawnEffect_4() { return static_cast<int32_t>(offsetof(Shooter_t66D7662B87125C892730A0E48F2442B684535B8D, ___SpawnEffect_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_SpawnEffect_4() const { return ___SpawnEffect_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_SpawnEffect_4() { return &___SpawnEffect_4; }
	inline void set_SpawnEffect_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___SpawnEffect_4 = value;
		Il2CppCodeGenWriteBarrier((&___SpawnEffect_4), value);
	}

	inline static int32_t get_offset_of_projectile_5() { return static_cast<int32_t>(offsetof(Shooter_t66D7662B87125C892730A0E48F2442B684535B8D, ___projectile_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectile_5() const { return ___projectile_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectile_5() { return &___projectile_5; }
	inline void set_projectile_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectile_5 = value;
		Il2CppCodeGenWriteBarrier((&___projectile_5), value);
	}

	inline static int32_t get_offset_of_HitEffect_6() { return static_cast<int32_t>(offsetof(Shooter_t66D7662B87125C892730A0E48F2442B684535B8D, ___HitEffect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_HitEffect_6() const { return ___HitEffect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_HitEffect_6() { return &___HitEffect_6; }
	inline void set_HitEffect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___HitEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___HitEffect_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SHOOTER_T66D7662B87125C892730A0E48F2442B684535B8D_H
#ifndef SIMPLEHEALTHBAR_T5AA224414BB1D8B5417DC0FD241372CC2DB23CC5_H
#define SIMPLEHEALTHBAR_T5AA224414BB1D8B5417DC0FD241372CC2DB23CC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SimpleHealthBar
struct  SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.UI.Image SimpleHealthBar::barImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___barImage_4;
	// SimpleHealthBar/ColorMode SimpleHealthBar::colorMode
	int32_t ___colorMode_5;
	// UnityEngine.Color SimpleHealthBar::barColor
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___barColor_6;
	// UnityEngine.Gradient SimpleHealthBar::barGradient
	Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * ___barGradient_7;
	// SimpleHealthBar/DisplayText SimpleHealthBar::displayText
	int32_t ___displayText_8;
	// UnityEngine.UI.Text SimpleHealthBar::barText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___barText_9;
	// System.String SimpleHealthBar::additionalText
	String_t* ___additionalText_10;
	// System.Single SimpleHealthBar::_currentFraction
	float ____currentFraction_11;
	// System.Single SimpleHealthBar::_maxValue
	float ____maxValue_12;
	// System.Single SimpleHealthBar::targetFill
	float ___targetFill_13;

public:
	inline static int32_t get_offset_of_barImage_4() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___barImage_4)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_barImage_4() const { return ___barImage_4; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_barImage_4() { return &___barImage_4; }
	inline void set_barImage_4(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___barImage_4 = value;
		Il2CppCodeGenWriteBarrier((&___barImage_4), value);
	}

	inline static int32_t get_offset_of_colorMode_5() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___colorMode_5)); }
	inline int32_t get_colorMode_5() const { return ___colorMode_5; }
	inline int32_t* get_address_of_colorMode_5() { return &___colorMode_5; }
	inline void set_colorMode_5(int32_t value)
	{
		___colorMode_5 = value;
	}

	inline static int32_t get_offset_of_barColor_6() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___barColor_6)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_barColor_6() const { return ___barColor_6; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_barColor_6() { return &___barColor_6; }
	inline void set_barColor_6(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___barColor_6 = value;
	}

	inline static int32_t get_offset_of_barGradient_7() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___barGradient_7)); }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * get_barGradient_7() const { return ___barGradient_7; }
	inline Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A ** get_address_of_barGradient_7() { return &___barGradient_7; }
	inline void set_barGradient_7(Gradient_t35A694DDA1066524440E325E582B01E33DE66A3A * value)
	{
		___barGradient_7 = value;
		Il2CppCodeGenWriteBarrier((&___barGradient_7), value);
	}

	inline static int32_t get_offset_of_displayText_8() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___displayText_8)); }
	inline int32_t get_displayText_8() const { return ___displayText_8; }
	inline int32_t* get_address_of_displayText_8() { return &___displayText_8; }
	inline void set_displayText_8(int32_t value)
	{
		___displayText_8 = value;
	}

	inline static int32_t get_offset_of_barText_9() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___barText_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_barText_9() const { return ___barText_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_barText_9() { return &___barText_9; }
	inline void set_barText_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___barText_9 = value;
		Il2CppCodeGenWriteBarrier((&___barText_9), value);
	}

	inline static int32_t get_offset_of_additionalText_10() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___additionalText_10)); }
	inline String_t* get_additionalText_10() const { return ___additionalText_10; }
	inline String_t** get_address_of_additionalText_10() { return &___additionalText_10; }
	inline void set_additionalText_10(String_t* value)
	{
		___additionalText_10 = value;
		Il2CppCodeGenWriteBarrier((&___additionalText_10), value);
	}

	inline static int32_t get_offset_of__currentFraction_11() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ____currentFraction_11)); }
	inline float get__currentFraction_11() const { return ____currentFraction_11; }
	inline float* get_address_of__currentFraction_11() { return &____currentFraction_11; }
	inline void set__currentFraction_11(float value)
	{
		____currentFraction_11 = value;
	}

	inline static int32_t get_offset_of__maxValue_12() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ____maxValue_12)); }
	inline float get__maxValue_12() const { return ____maxValue_12; }
	inline float* get_address_of__maxValue_12() { return &____maxValue_12; }
	inline void set__maxValue_12(float value)
	{
		____maxValue_12 = value;
	}

	inline static int32_t get_offset_of_targetFill_13() { return static_cast<int32_t>(offsetof(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5, ___targetFill_13)); }
	inline float get_targetFill_13() const { return ___targetFill_13; }
	inline float* get_address_of_targetFill_13() { return &___targetFill_13; }
	inline void set_targetFill_13(float value)
	{
		___targetFill_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEHEALTHBAR_T5AA224414BB1D8B5417DC0FD241372CC2DB23CC5_H
#ifndef SPACEINVADERAI_TC408B223F3570D03E1EEC2878CB27A2492DA37A8_H
#define SPACEINVADERAI_TC408B223F3570D03E1EEC2878CB27A2492DA37A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpaceInvaderAI
struct  SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GameObject SpaceInvaderAI::spawnEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___spawnEffect_4;
	// UnityEngine.GameObject SpaceInvaderAI::projectile
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___projectile_5;
	// UnityEngine.GameObject SpaceInvaderAI::hitEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___hitEffect_6;
	// System.Single SpaceInvaderAI::minFireInterval
	float ___minFireInterval_7;
	// System.Single SpaceInvaderAI::maxFireInterval
	float ___maxFireInterval_8;
	// System.Single SpaceInvaderAI::stepInterval
	float ___stepInterval_9;
	// System.Single SpaceInvaderAI::forwardStep
	float ___forwardStep_10;
	// UnityEngine.Vector3 SpaceInvaderAI::moveDirection
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___moveDirection_11;
	// System.Single SpaceInvaderAI::movementModifier
	float ___movementModifier_12;
	// System.Single SpaceInvaderAI::stepDistance
	float ___stepDistance_13;
	// System.Boolean SpaceInvaderAI::moveFoward
	bool ___moveFoward_14;
	// System.Single SpaceInvaderAI::damage
	float ___damage_15;
	// System.Boolean SpaceInvaderAI::locked
	bool ___locked_16;
	// System.Action`1<EventParam> SpaceInvaderAI::edgeReached
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___edgeReached_17;

public:
	inline static int32_t get_offset_of_spawnEffect_4() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___spawnEffect_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_spawnEffect_4() const { return ___spawnEffect_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_spawnEffect_4() { return &___spawnEffect_4; }
	inline void set_spawnEffect_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___spawnEffect_4 = value;
		Il2CppCodeGenWriteBarrier((&___spawnEffect_4), value);
	}

	inline static int32_t get_offset_of_projectile_5() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___projectile_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_projectile_5() const { return ___projectile_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_projectile_5() { return &___projectile_5; }
	inline void set_projectile_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___projectile_5 = value;
		Il2CppCodeGenWriteBarrier((&___projectile_5), value);
	}

	inline static int32_t get_offset_of_hitEffect_6() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___hitEffect_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_hitEffect_6() const { return ___hitEffect_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_hitEffect_6() { return &___hitEffect_6; }
	inline void set_hitEffect_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___hitEffect_6 = value;
		Il2CppCodeGenWriteBarrier((&___hitEffect_6), value);
	}

	inline static int32_t get_offset_of_minFireInterval_7() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___minFireInterval_7)); }
	inline float get_minFireInterval_7() const { return ___minFireInterval_7; }
	inline float* get_address_of_minFireInterval_7() { return &___minFireInterval_7; }
	inline void set_minFireInterval_7(float value)
	{
		___minFireInterval_7 = value;
	}

	inline static int32_t get_offset_of_maxFireInterval_8() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___maxFireInterval_8)); }
	inline float get_maxFireInterval_8() const { return ___maxFireInterval_8; }
	inline float* get_address_of_maxFireInterval_8() { return &___maxFireInterval_8; }
	inline void set_maxFireInterval_8(float value)
	{
		___maxFireInterval_8 = value;
	}

	inline static int32_t get_offset_of_stepInterval_9() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___stepInterval_9)); }
	inline float get_stepInterval_9() const { return ___stepInterval_9; }
	inline float* get_address_of_stepInterval_9() { return &___stepInterval_9; }
	inline void set_stepInterval_9(float value)
	{
		___stepInterval_9 = value;
	}

	inline static int32_t get_offset_of_forwardStep_10() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___forwardStep_10)); }
	inline float get_forwardStep_10() const { return ___forwardStep_10; }
	inline float* get_address_of_forwardStep_10() { return &___forwardStep_10; }
	inline void set_forwardStep_10(float value)
	{
		___forwardStep_10 = value;
	}

	inline static int32_t get_offset_of_moveDirection_11() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___moveDirection_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_moveDirection_11() const { return ___moveDirection_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_moveDirection_11() { return &___moveDirection_11; }
	inline void set_moveDirection_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___moveDirection_11 = value;
	}

	inline static int32_t get_offset_of_movementModifier_12() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___movementModifier_12)); }
	inline float get_movementModifier_12() const { return ___movementModifier_12; }
	inline float* get_address_of_movementModifier_12() { return &___movementModifier_12; }
	inline void set_movementModifier_12(float value)
	{
		___movementModifier_12 = value;
	}

	inline static int32_t get_offset_of_stepDistance_13() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___stepDistance_13)); }
	inline float get_stepDistance_13() const { return ___stepDistance_13; }
	inline float* get_address_of_stepDistance_13() { return &___stepDistance_13; }
	inline void set_stepDistance_13(float value)
	{
		___stepDistance_13 = value;
	}

	inline static int32_t get_offset_of_moveFoward_14() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___moveFoward_14)); }
	inline bool get_moveFoward_14() const { return ___moveFoward_14; }
	inline bool* get_address_of_moveFoward_14() { return &___moveFoward_14; }
	inline void set_moveFoward_14(bool value)
	{
		___moveFoward_14 = value;
	}

	inline static int32_t get_offset_of_damage_15() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___damage_15)); }
	inline float get_damage_15() const { return ___damage_15; }
	inline float* get_address_of_damage_15() { return &___damage_15; }
	inline void set_damage_15(float value)
	{
		___damage_15 = value;
	}

	inline static int32_t get_offset_of_locked_16() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___locked_16)); }
	inline bool get_locked_16() const { return ___locked_16; }
	inline bool* get_address_of_locked_16() { return &___locked_16; }
	inline void set_locked_16(bool value)
	{
		___locked_16 = value;
	}

	inline static int32_t get_offset_of_edgeReached_17() { return static_cast<int32_t>(offsetof(SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8, ___edgeReached_17)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_edgeReached_17() const { return ___edgeReached_17; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_edgeReached_17() { return &___edgeReached_17; }
	inline void set_edgeReached_17(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___edgeReached_17 = value;
		Il2CppCodeGenWriteBarrier((&___edgeReached_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEINVADERAI_TC408B223F3570D03E1EEC2878CB27A2492DA37A8_H
#ifndef SPACEINVADEREVENTS_T4CBF0345E214F41F72813A9D060AA676A3D9C4EF_H
#define SPACEINVADEREVENTS_T4CBF0345E214F41F72813A9D060AA676A3D9C4EF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpaceInvaderEvents
struct  SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<EventParam> SpaceInvaderEvents::playerDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___playerDead_4;
	// System.Action`1<EventParam> SpaceInvaderEvents::invasion
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___invasion_5;
	// System.Action`1<EventParam> SpaceInvaderEvents::enemyDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___enemyDead_6;
	// System.Action`1<EventParam> SpaceInvaderEvents::allEnemyDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___allEnemyDead_7;
	// System.Action`1<EventParam> SpaceInvaderEvents::restart
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___restart_8;
	// System.Int32 SpaceInvaderEvents::enemyCount
	int32_t ___enemyCount_9;

public:
	inline static int32_t get_offset_of_playerDead_4() { return static_cast<int32_t>(offsetof(SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF, ___playerDead_4)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_playerDead_4() const { return ___playerDead_4; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_playerDead_4() { return &___playerDead_4; }
	inline void set_playerDead_4(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___playerDead_4 = value;
		Il2CppCodeGenWriteBarrier((&___playerDead_4), value);
	}

	inline static int32_t get_offset_of_invasion_5() { return static_cast<int32_t>(offsetof(SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF, ___invasion_5)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_invasion_5() const { return ___invasion_5; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_invasion_5() { return &___invasion_5; }
	inline void set_invasion_5(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___invasion_5 = value;
		Il2CppCodeGenWriteBarrier((&___invasion_5), value);
	}

	inline static int32_t get_offset_of_enemyDead_6() { return static_cast<int32_t>(offsetof(SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF, ___enemyDead_6)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_enemyDead_6() const { return ___enemyDead_6; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_enemyDead_6() { return &___enemyDead_6; }
	inline void set_enemyDead_6(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___enemyDead_6 = value;
		Il2CppCodeGenWriteBarrier((&___enemyDead_6), value);
	}

	inline static int32_t get_offset_of_allEnemyDead_7() { return static_cast<int32_t>(offsetof(SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF, ___allEnemyDead_7)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_allEnemyDead_7() const { return ___allEnemyDead_7; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_allEnemyDead_7() { return &___allEnemyDead_7; }
	inline void set_allEnemyDead_7(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___allEnemyDead_7 = value;
		Il2CppCodeGenWriteBarrier((&___allEnemyDead_7), value);
	}

	inline static int32_t get_offset_of_restart_8() { return static_cast<int32_t>(offsetof(SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF, ___restart_8)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_restart_8() const { return ___restart_8; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_restart_8() { return &___restart_8; }
	inline void set_restart_8(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___restart_8 = value;
		Il2CppCodeGenWriteBarrier((&___restart_8), value);
	}

	inline static int32_t get_offset_of_enemyCount_9() { return static_cast<int32_t>(offsetof(SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF, ___enemyCount_9)); }
	inline int32_t get_enemyCount_9() const { return ___enemyCount_9; }
	inline int32_t* get_address_of_enemyCount_9() { return &___enemyCount_9; }
	inline void set_enemyCount_9(int32_t value)
	{
		___enemyCount_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPACEINVADEREVENTS_T4CBF0345E214F41F72813A9D060AA676A3D9C4EF_H
#ifndef SPECIALEFFECT_TE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A_H
#define SPECIALEFFECT_TE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// SpecialEffect
struct  SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single SpecialEffect::duration
	float ___duration_4;
	// System.Single SpecialEffect::cleanUpTime
	float ___cleanUpTime_5;

public:
	inline static int32_t get_offset_of_duration_4() { return static_cast<int32_t>(offsetof(SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A, ___duration_4)); }
	inline float get_duration_4() const { return ___duration_4; }
	inline float* get_address_of_duration_4() { return &___duration_4; }
	inline void set_duration_4(float value)
	{
		___duration_4 = value;
	}

	inline static int32_t get_offset_of_cleanUpTime_5() { return static_cast<int32_t>(offsetof(SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A, ___cleanUpTime_5)); }
	inline float get_cleanUpTime_5() const { return ___cleanUpTime_5; }
	inline float* get_address_of_cleanUpTime_5() { return &___cleanUpTime_5; }
	inline void set_cleanUpTime_5(float value)
	{
		___cleanUpTime_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPECIALEFFECT_TE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A_H
#ifndef TAKEDAMAGE_TA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B_H
#define TAKEDAMAGE_TA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TakeDamage
struct  TakeDamage_tA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single TakeDamage::damageValue
	float ___damageValue_4;

public:
	inline static int32_t get_offset_of_damageValue_4() { return static_cast<int32_t>(offsetof(TakeDamage_tA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B, ___damageValue_4)); }
	inline float get_damageValue_4() const { return ___damageValue_4; }
	inline float* get_address_of_damageValue_4() { return &___damageValue_4; }
	inline void set_damageValue_4(float value)
	{
		___damageValue_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TAKEDAMAGE_TA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B_H
#ifndef TOWERDEFENSEGAMEEVENTHANDLER_T6E627A6336C8FB19DC1C469D1699BBD46D56B7E7_H
#define TOWERDEFENSEGAMEEVENTHANDLER_T6E627A6336C8FB19DC1C469D1699BBD46D56B7E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// TowerDefenseGameEventHandler
struct  TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Action`1<EventParam> TowerDefenseGameEventHandler::baseDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___baseDead_4;
	// System.Action`1<EventParam> TowerDefenseGameEventHandler::enemySpawned
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___enemySpawned_5;
	// System.Action`1<EventParam> TowerDefenseGameEventHandler::enemyDead
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___enemyDead_6;
	// System.Action`1<EventParam> TowerDefenseGameEventHandler::allEnemiesDefeated
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___allEnemiesDefeated_7;
	// System.Action`1<EventParam> TowerDefenseGameEventHandler::restart
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___restart_8;
	// System.Action`1<EventParam> TowerDefenseGameEventHandler::agentDeath
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___agentDeath_9;
	// System.Int32 TowerDefenseGameEventHandler::enemyCount
	int32_t ___enemyCount_10;
	// Core.Health.SerializableIAlignmentProvider TowerDefenseGameEventHandler::enemy
	SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * ___enemy_11;
	// Core.Health.IAlignmentProvider TowerDefenseGameEventHandler::enemyAlignment
	RuntimeObject* ___enemyAlignment_12;

public:
	inline static int32_t get_offset_of_baseDead_4() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___baseDead_4)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_baseDead_4() const { return ___baseDead_4; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_baseDead_4() { return &___baseDead_4; }
	inline void set_baseDead_4(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___baseDead_4 = value;
		Il2CppCodeGenWriteBarrier((&___baseDead_4), value);
	}

	inline static int32_t get_offset_of_enemySpawned_5() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___enemySpawned_5)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_enemySpawned_5() const { return ___enemySpawned_5; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_enemySpawned_5() { return &___enemySpawned_5; }
	inline void set_enemySpawned_5(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___enemySpawned_5 = value;
		Il2CppCodeGenWriteBarrier((&___enemySpawned_5), value);
	}

	inline static int32_t get_offset_of_enemyDead_6() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___enemyDead_6)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_enemyDead_6() const { return ___enemyDead_6; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_enemyDead_6() { return &___enemyDead_6; }
	inline void set_enemyDead_6(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___enemyDead_6 = value;
		Il2CppCodeGenWriteBarrier((&___enemyDead_6), value);
	}

	inline static int32_t get_offset_of_allEnemiesDefeated_7() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___allEnemiesDefeated_7)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_allEnemiesDefeated_7() const { return ___allEnemiesDefeated_7; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_allEnemiesDefeated_7() { return &___allEnemiesDefeated_7; }
	inline void set_allEnemiesDefeated_7(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___allEnemiesDefeated_7 = value;
		Il2CppCodeGenWriteBarrier((&___allEnemiesDefeated_7), value);
	}

	inline static int32_t get_offset_of_restart_8() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___restart_8)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_restart_8() const { return ___restart_8; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_restart_8() { return &___restart_8; }
	inline void set_restart_8(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___restart_8 = value;
		Il2CppCodeGenWriteBarrier((&___restart_8), value);
	}

	inline static int32_t get_offset_of_agentDeath_9() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___agentDeath_9)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_agentDeath_9() const { return ___agentDeath_9; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_agentDeath_9() { return &___agentDeath_9; }
	inline void set_agentDeath_9(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___agentDeath_9 = value;
		Il2CppCodeGenWriteBarrier((&___agentDeath_9), value);
	}

	inline static int32_t get_offset_of_enemyCount_10() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___enemyCount_10)); }
	inline int32_t get_enemyCount_10() const { return ___enemyCount_10; }
	inline int32_t* get_address_of_enemyCount_10() { return &___enemyCount_10; }
	inline void set_enemyCount_10(int32_t value)
	{
		___enemyCount_10 = value;
	}

	inline static int32_t get_offset_of_enemy_11() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___enemy_11)); }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * get_enemy_11() const { return ___enemy_11; }
	inline SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 ** get_address_of_enemy_11() { return &___enemy_11; }
	inline void set_enemy_11(SerializableIAlignmentProvider_tD55C30A32481580774C690CC7BFD52BA27CE6C61 * value)
	{
		___enemy_11 = value;
		Il2CppCodeGenWriteBarrier((&___enemy_11), value);
	}

	inline static int32_t get_offset_of_enemyAlignment_12() { return static_cast<int32_t>(offsetof(TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7, ___enemyAlignment_12)); }
	inline RuntimeObject* get_enemyAlignment_12() const { return ___enemyAlignment_12; }
	inline RuntimeObject** get_address_of_enemyAlignment_12() { return &___enemyAlignment_12; }
	inline void set_enemyAlignment_12(RuntimeObject* value)
	{
		___enemyAlignment_12 = value;
		Il2CppCodeGenWriteBarrier((&___enemyAlignment_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOWERDEFENSEGAMEEVENTHANDLER_T6E627A6336C8FB19DC1C469D1699BBD46D56B7E7_H
#ifndef TURRET_T5BCEE2BDBDB2299F076303C640ED29C5ABAD354E_H
#define TURRET_T5BCEE2BDBDB2299F076303C640ED29C5ABAD354E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Turret
struct  Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// Core.Health.IAlignmentProvider Turret::alignmentProvider
	RuntimeObject* ___alignmentProvider_4;
	// UnityEngine.LayerMask Turret::<enemyMask>k__BackingField
	LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  ___U3CenemyMaskU3Ek__BackingField_5;
	// ParticleProjectile Turret::projectilePrefab
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 * ___projectilePrefab_6;
	// ParticleProjectile Turret::projectile
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 * ___projectile_7;
	// UnityEngine.ParticleSystem Turret::hitEffect
	ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * ___hitEffect_8;
	// UnityEngine.Transform[] Turret::projectilePoints
	TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* ___projectilePoints_9;
	// System.Int32 Turret::projectilePointIndex
	int32_t ___projectilePointIndex_10;
	// UnityEngine.Transform Turret::epicenter
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___epicenter_11;
	// System.Boolean Turret::isMultiAttack
	bool ___isMultiAttack_12;
	// System.Single Turret::fireRate
	float ___fireRate_13;
	// System.Single Turret::damage
	float ___damage_14;
	// LockOnTarget Turret::targetLockOn
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283 * ___targetLockOn_15;
	// UnityEngine.Color Turret::radiusEffectColour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___radiusEffectColour_16;
	// System.Single Turret::m_FireTimer
	float ___m_FireTimer_17;
	// CanTarget Turret::m_TrackingEnemy
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * ___m_TrackingEnemy_18;

public:
	inline static int32_t get_offset_of_alignmentProvider_4() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___alignmentProvider_4)); }
	inline RuntimeObject* get_alignmentProvider_4() const { return ___alignmentProvider_4; }
	inline RuntimeObject** get_address_of_alignmentProvider_4() { return &___alignmentProvider_4; }
	inline void set_alignmentProvider_4(RuntimeObject* value)
	{
		___alignmentProvider_4 = value;
		Il2CppCodeGenWriteBarrier((&___alignmentProvider_4), value);
	}

	inline static int32_t get_offset_of_U3CenemyMaskU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___U3CenemyMaskU3Ek__BackingField_5)); }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  get_U3CenemyMaskU3Ek__BackingField_5() const { return ___U3CenemyMaskU3Ek__BackingField_5; }
	inline LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0 * get_address_of_U3CenemyMaskU3Ek__BackingField_5() { return &___U3CenemyMaskU3Ek__BackingField_5; }
	inline void set_U3CenemyMaskU3Ek__BackingField_5(LayerMask_tBB9173D8B6939D476E67E849280AC9F4EC4D93B0  value)
	{
		___U3CenemyMaskU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_projectilePrefab_6() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___projectilePrefab_6)); }
	inline ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 * get_projectilePrefab_6() const { return ___projectilePrefab_6; }
	inline ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 ** get_address_of_projectilePrefab_6() { return &___projectilePrefab_6; }
	inline void set_projectilePrefab_6(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 * value)
	{
		___projectilePrefab_6 = value;
		Il2CppCodeGenWriteBarrier((&___projectilePrefab_6), value);
	}

	inline static int32_t get_offset_of_projectile_7() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___projectile_7)); }
	inline ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 * get_projectile_7() const { return ___projectile_7; }
	inline ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 ** get_address_of_projectile_7() { return &___projectile_7; }
	inline void set_projectile_7(ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4 * value)
	{
		___projectile_7 = value;
		Il2CppCodeGenWriteBarrier((&___projectile_7), value);
	}

	inline static int32_t get_offset_of_hitEffect_8() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___hitEffect_8)); }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * get_hitEffect_8() const { return ___hitEffect_8; }
	inline ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D ** get_address_of_hitEffect_8() { return &___hitEffect_8; }
	inline void set_hitEffect_8(ParticleSystem_t45DA87A3E83E738DA3FDAA5A48A133F1A1247C3D * value)
	{
		___hitEffect_8 = value;
		Il2CppCodeGenWriteBarrier((&___hitEffect_8), value);
	}

	inline static int32_t get_offset_of_projectilePoints_9() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___projectilePoints_9)); }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* get_projectilePoints_9() const { return ___projectilePoints_9; }
	inline TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804** get_address_of_projectilePoints_9() { return &___projectilePoints_9; }
	inline void set_projectilePoints_9(TransformU5BU5D_t4F5A1132877D8BA66ABC0A9A7FADA4E0237A7804* value)
	{
		___projectilePoints_9 = value;
		Il2CppCodeGenWriteBarrier((&___projectilePoints_9), value);
	}

	inline static int32_t get_offset_of_projectilePointIndex_10() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___projectilePointIndex_10)); }
	inline int32_t get_projectilePointIndex_10() const { return ___projectilePointIndex_10; }
	inline int32_t* get_address_of_projectilePointIndex_10() { return &___projectilePointIndex_10; }
	inline void set_projectilePointIndex_10(int32_t value)
	{
		___projectilePointIndex_10 = value;
	}

	inline static int32_t get_offset_of_epicenter_11() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___epicenter_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_epicenter_11() const { return ___epicenter_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_epicenter_11() { return &___epicenter_11; }
	inline void set_epicenter_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___epicenter_11 = value;
		Il2CppCodeGenWriteBarrier((&___epicenter_11), value);
	}

	inline static int32_t get_offset_of_isMultiAttack_12() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___isMultiAttack_12)); }
	inline bool get_isMultiAttack_12() const { return ___isMultiAttack_12; }
	inline bool* get_address_of_isMultiAttack_12() { return &___isMultiAttack_12; }
	inline void set_isMultiAttack_12(bool value)
	{
		___isMultiAttack_12 = value;
	}

	inline static int32_t get_offset_of_fireRate_13() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___fireRate_13)); }
	inline float get_fireRate_13() const { return ___fireRate_13; }
	inline float* get_address_of_fireRate_13() { return &___fireRate_13; }
	inline void set_fireRate_13(float value)
	{
		___fireRate_13 = value;
	}

	inline static int32_t get_offset_of_damage_14() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___damage_14)); }
	inline float get_damage_14() const { return ___damage_14; }
	inline float* get_address_of_damage_14() { return &___damage_14; }
	inline void set_damage_14(float value)
	{
		___damage_14 = value;
	}

	inline static int32_t get_offset_of_targetLockOn_15() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___targetLockOn_15)); }
	inline LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283 * get_targetLockOn_15() const { return ___targetLockOn_15; }
	inline LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283 ** get_address_of_targetLockOn_15() { return &___targetLockOn_15; }
	inline void set_targetLockOn_15(LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283 * value)
	{
		___targetLockOn_15 = value;
		Il2CppCodeGenWriteBarrier((&___targetLockOn_15), value);
	}

	inline static int32_t get_offset_of_radiusEffectColour_16() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___radiusEffectColour_16)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_radiusEffectColour_16() const { return ___radiusEffectColour_16; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_radiusEffectColour_16() { return &___radiusEffectColour_16; }
	inline void set_radiusEffectColour_16(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___radiusEffectColour_16 = value;
	}

	inline static int32_t get_offset_of_m_FireTimer_17() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___m_FireTimer_17)); }
	inline float get_m_FireTimer_17() const { return ___m_FireTimer_17; }
	inline float* get_address_of_m_FireTimer_17() { return &___m_FireTimer_17; }
	inline void set_m_FireTimer_17(float value)
	{
		___m_FireTimer_17 = value;
	}

	inline static int32_t get_offset_of_m_TrackingEnemy_18() { return static_cast<int32_t>(offsetof(Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E, ___m_TrackingEnemy_18)); }
	inline CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * get_m_TrackingEnemy_18() const { return ___m_TrackingEnemy_18; }
	inline CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 ** get_address_of_m_TrackingEnemy_18() { return &___m_TrackingEnemy_18; }
	inline void set_m_TrackingEnemy_18(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0 * value)
	{
		___m_TrackingEnemy_18 = value;
		Il2CppCodeGenWriteBarrier((&___m_TrackingEnemy_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TURRET_T5BCEE2BDBDB2299F076303C640ED29C5ABAD354E_H
#ifndef ACTIVATETRIGGER_TB45971BB1DB93436BA51A0F3BFE948876EE04B97_H
#define ACTIVATETRIGGER_TB45971BB1DB93436BA51A0F3BFE948876EE04B97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ActivateTrigger
struct  ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.ActivateTrigger/Mode UnityStandardAssets.Utility.ActivateTrigger::action
	int32_t ___action_4;
	// UnityEngine.Object UnityStandardAssets.Utility.ActivateTrigger::target
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___target_5;
	// UnityEngine.GameObject UnityStandardAssets.Utility.ActivateTrigger::source
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___source_6;
	// System.Int32 UnityStandardAssets.Utility.ActivateTrigger::triggerCount
	int32_t ___triggerCount_7;
	// System.Boolean UnityStandardAssets.Utility.ActivateTrigger::repeatTrigger
	bool ___repeatTrigger_8;

public:
	inline static int32_t get_offset_of_action_4() { return static_cast<int32_t>(offsetof(ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97, ___action_4)); }
	inline int32_t get_action_4() const { return ___action_4; }
	inline int32_t* get_address_of_action_4() { return &___action_4; }
	inline void set_action_4(int32_t value)
	{
		___action_4 = value;
	}

	inline static int32_t get_offset_of_target_5() { return static_cast<int32_t>(offsetof(ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97, ___target_5)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_target_5() const { return ___target_5; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_target_5() { return &___target_5; }
	inline void set_target_5(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___target_5 = value;
		Il2CppCodeGenWriteBarrier((&___target_5), value);
	}

	inline static int32_t get_offset_of_source_6() { return static_cast<int32_t>(offsetof(ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97, ___source_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_source_6() const { return ___source_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_source_6() { return &___source_6; }
	inline void set_source_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___source_6 = value;
		Il2CppCodeGenWriteBarrier((&___source_6), value);
	}

	inline static int32_t get_offset_of_triggerCount_7() { return static_cast<int32_t>(offsetof(ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97, ___triggerCount_7)); }
	inline int32_t get_triggerCount_7() const { return ___triggerCount_7; }
	inline int32_t* get_address_of_triggerCount_7() { return &___triggerCount_7; }
	inline void set_triggerCount_7(int32_t value)
	{
		___triggerCount_7 = value;
	}

	inline static int32_t get_offset_of_repeatTrigger_8() { return static_cast<int32_t>(offsetof(ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97, ___repeatTrigger_8)); }
	inline bool get_repeatTrigger_8() const { return ___repeatTrigger_8; }
	inline bool* get_address_of_repeatTrigger_8() { return &___repeatTrigger_8; }
	inline void set_repeatTrigger_8(bool value)
	{
		___repeatTrigger_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ACTIVATETRIGGER_TB45971BB1DB93436BA51A0F3BFE948876EE04B97_H
#ifndef AUTOMOBILESHADERSWITCH_TA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6_H
#define AUTOMOBILESHADERSWITCH_TA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMobileShaderSwitch
struct  AutoMobileShaderSwitch_tA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.AutoMobileShaderSwitch/ReplacementList UnityStandardAssets.Utility.AutoMobileShaderSwitch::m_ReplacementList
	ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1 * ___m_ReplacementList_4;

public:
	inline static int32_t get_offset_of_m_ReplacementList_4() { return static_cast<int32_t>(offsetof(AutoMobileShaderSwitch_tA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6, ___m_ReplacementList_4)); }
	inline ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1 * get_m_ReplacementList_4() const { return ___m_ReplacementList_4; }
	inline ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1 ** get_address_of_m_ReplacementList_4() { return &___m_ReplacementList_4; }
	inline void set_m_ReplacementList_4(ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1 * value)
	{
		___m_ReplacementList_4 = value;
		Il2CppCodeGenWriteBarrier((&___m_ReplacementList_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOBILESHADERSWITCH_TA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6_H
#ifndef AUTOMOVEANDROTATE_T92043194FF41B06B9441FB187206098DF3848E27_H
#define AUTOMOVEANDROTATE_T92043194FF41B06B9441FB187206098DF3848E27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.AutoMoveAndRotate
struct  AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::moveUnitsPerSecond
	Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA * ___moveUnitsPerSecond_4;
	// UnityStandardAssets.Utility.AutoMoveAndRotate/Vector3andSpace UnityStandardAssets.Utility.AutoMoveAndRotate::rotateDegreesPerSecond
	Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA * ___rotateDegreesPerSecond_5;
	// System.Boolean UnityStandardAssets.Utility.AutoMoveAndRotate::ignoreTimescale
	bool ___ignoreTimescale_6;
	// System.Single UnityStandardAssets.Utility.AutoMoveAndRotate::m_LastRealTime
	float ___m_LastRealTime_7;

public:
	inline static int32_t get_offset_of_moveUnitsPerSecond_4() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27, ___moveUnitsPerSecond_4)); }
	inline Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA * get_moveUnitsPerSecond_4() const { return ___moveUnitsPerSecond_4; }
	inline Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA ** get_address_of_moveUnitsPerSecond_4() { return &___moveUnitsPerSecond_4; }
	inline void set_moveUnitsPerSecond_4(Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA * value)
	{
		___moveUnitsPerSecond_4 = value;
		Il2CppCodeGenWriteBarrier((&___moveUnitsPerSecond_4), value);
	}

	inline static int32_t get_offset_of_rotateDegreesPerSecond_5() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27, ___rotateDegreesPerSecond_5)); }
	inline Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA * get_rotateDegreesPerSecond_5() const { return ___rotateDegreesPerSecond_5; }
	inline Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA ** get_address_of_rotateDegreesPerSecond_5() { return &___rotateDegreesPerSecond_5; }
	inline void set_rotateDegreesPerSecond_5(Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA * value)
	{
		___rotateDegreesPerSecond_5 = value;
		Il2CppCodeGenWriteBarrier((&___rotateDegreesPerSecond_5), value);
	}

	inline static int32_t get_offset_of_ignoreTimescale_6() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27, ___ignoreTimescale_6)); }
	inline bool get_ignoreTimescale_6() const { return ___ignoreTimescale_6; }
	inline bool* get_address_of_ignoreTimescale_6() { return &___ignoreTimescale_6; }
	inline void set_ignoreTimescale_6(bool value)
	{
		___ignoreTimescale_6 = value;
	}

	inline static int32_t get_offset_of_m_LastRealTime_7() { return static_cast<int32_t>(offsetof(AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27, ___m_LastRealTime_7)); }
	inline float get_m_LastRealTime_7() const { return ___m_LastRealTime_7; }
	inline float* get_address_of_m_LastRealTime_7() { return &___m_LastRealTime_7; }
	inline void set_m_LastRealTime_7(float value)
	{
		___m_LastRealTime_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTOMOVEANDROTATE_T92043194FF41B06B9441FB187206098DF3848E27_H
#ifndef DRAGRIGIDBODY_TD01458E8F172F9B508F26B5FF63E1F041AB4AD57_H
#define DRAGRIGIDBODY_TD01458E8F172F9B508F26B5FF63E1F041AB4AD57_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DragRigidbody
struct  DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.SpringJoint UnityStandardAssets.Utility.DragRigidbody::m_SpringJoint
	SpringJoint_t2D196194480E339F728B463619720385523B09AF * ___m_SpringJoint_10;

public:
	inline static int32_t get_offset_of_m_SpringJoint_10() { return static_cast<int32_t>(offsetof(DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57, ___m_SpringJoint_10)); }
	inline SpringJoint_t2D196194480E339F728B463619720385523B09AF * get_m_SpringJoint_10() const { return ___m_SpringJoint_10; }
	inline SpringJoint_t2D196194480E339F728B463619720385523B09AF ** get_address_of_m_SpringJoint_10() { return &___m_SpringJoint_10; }
	inline void set_m_SpringJoint_10(SpringJoint_t2D196194480E339F728B463619720385523B09AF * value)
	{
		___m_SpringJoint_10 = value;
		Il2CppCodeGenWriteBarrier((&___m_SpringJoint_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DRAGRIGIDBODY_TD01458E8F172F9B508F26B5FF63E1F041AB4AD57_H
#ifndef DYNAMICSHADOWSETTINGS_T2ABA716F8D04B9938814F6C1788B5CC0E89E5653_H
#define DYNAMICSHADOWSETTINGS_T2ABA716F8D04B9938814F6C1788B5CC0E89E5653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.DynamicShadowSettings
struct  DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Light UnityStandardAssets.Utility.DynamicShadowSettings::sunLight
	Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * ___sunLight_4;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minHeight
	float ___minHeight_5;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowDistance
	float ___minShadowDistance_6;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::minShadowBias
	float ___minShadowBias_7;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxHeight
	float ___maxHeight_8;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowDistance
	float ___maxShadowDistance_9;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::maxShadowBias
	float ___maxShadowBias_10;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::adaptTime
	float ___adaptTime_11;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_SmoothHeight
	float ___m_SmoothHeight_12;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_ChangeSpeed
	float ___m_ChangeSpeed_13;
	// System.Single UnityStandardAssets.Utility.DynamicShadowSettings::m_OriginalStrength
	float ___m_OriginalStrength_14;

public:
	inline static int32_t get_offset_of_sunLight_4() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___sunLight_4)); }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * get_sunLight_4() const { return ___sunLight_4; }
	inline Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C ** get_address_of_sunLight_4() { return &___sunLight_4; }
	inline void set_sunLight_4(Light_tFDE490EADBC7E080F74CA804929513AF07C31A6C * value)
	{
		___sunLight_4 = value;
		Il2CppCodeGenWriteBarrier((&___sunLight_4), value);
	}

	inline static int32_t get_offset_of_minHeight_5() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___minHeight_5)); }
	inline float get_minHeight_5() const { return ___minHeight_5; }
	inline float* get_address_of_minHeight_5() { return &___minHeight_5; }
	inline void set_minHeight_5(float value)
	{
		___minHeight_5 = value;
	}

	inline static int32_t get_offset_of_minShadowDistance_6() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___minShadowDistance_6)); }
	inline float get_minShadowDistance_6() const { return ___minShadowDistance_6; }
	inline float* get_address_of_minShadowDistance_6() { return &___minShadowDistance_6; }
	inline void set_minShadowDistance_6(float value)
	{
		___minShadowDistance_6 = value;
	}

	inline static int32_t get_offset_of_minShadowBias_7() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___minShadowBias_7)); }
	inline float get_minShadowBias_7() const { return ___minShadowBias_7; }
	inline float* get_address_of_minShadowBias_7() { return &___minShadowBias_7; }
	inline void set_minShadowBias_7(float value)
	{
		___minShadowBias_7 = value;
	}

	inline static int32_t get_offset_of_maxHeight_8() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___maxHeight_8)); }
	inline float get_maxHeight_8() const { return ___maxHeight_8; }
	inline float* get_address_of_maxHeight_8() { return &___maxHeight_8; }
	inline void set_maxHeight_8(float value)
	{
		___maxHeight_8 = value;
	}

	inline static int32_t get_offset_of_maxShadowDistance_9() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___maxShadowDistance_9)); }
	inline float get_maxShadowDistance_9() const { return ___maxShadowDistance_9; }
	inline float* get_address_of_maxShadowDistance_9() { return &___maxShadowDistance_9; }
	inline void set_maxShadowDistance_9(float value)
	{
		___maxShadowDistance_9 = value;
	}

	inline static int32_t get_offset_of_maxShadowBias_10() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___maxShadowBias_10)); }
	inline float get_maxShadowBias_10() const { return ___maxShadowBias_10; }
	inline float* get_address_of_maxShadowBias_10() { return &___maxShadowBias_10; }
	inline void set_maxShadowBias_10(float value)
	{
		___maxShadowBias_10 = value;
	}

	inline static int32_t get_offset_of_adaptTime_11() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___adaptTime_11)); }
	inline float get_adaptTime_11() const { return ___adaptTime_11; }
	inline float* get_address_of_adaptTime_11() { return &___adaptTime_11; }
	inline void set_adaptTime_11(float value)
	{
		___adaptTime_11 = value;
	}

	inline static int32_t get_offset_of_m_SmoothHeight_12() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___m_SmoothHeight_12)); }
	inline float get_m_SmoothHeight_12() const { return ___m_SmoothHeight_12; }
	inline float* get_address_of_m_SmoothHeight_12() { return &___m_SmoothHeight_12; }
	inline void set_m_SmoothHeight_12(float value)
	{
		___m_SmoothHeight_12 = value;
	}

	inline static int32_t get_offset_of_m_ChangeSpeed_13() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___m_ChangeSpeed_13)); }
	inline float get_m_ChangeSpeed_13() const { return ___m_ChangeSpeed_13; }
	inline float* get_address_of_m_ChangeSpeed_13() { return &___m_ChangeSpeed_13; }
	inline void set_m_ChangeSpeed_13(float value)
	{
		___m_ChangeSpeed_13 = value;
	}

	inline static int32_t get_offset_of_m_OriginalStrength_14() { return static_cast<int32_t>(offsetof(DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653, ___m_OriginalStrength_14)); }
	inline float get_m_OriginalStrength_14() const { return ___m_OriginalStrength_14; }
	inline float* get_address_of_m_OriginalStrength_14() { return &___m_OriginalStrength_14; }
	inline void set_m_OriginalStrength_14(float value)
	{
		___m_OriginalStrength_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DYNAMICSHADOWSETTINGS_T2ABA716F8D04B9938814F6C1788B5CC0E89E5653_H
#ifndef FPSCOUNTER_T8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C_H
#define FPSCOUNTER_T8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FPSCounter
struct  FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_FpsAccumulator
	int32_t ___m_FpsAccumulator_5;
	// System.Single UnityStandardAssets.Utility.FPSCounter::m_FpsNextPeriod
	float ___m_FpsNextPeriod_6;
	// System.Int32 UnityStandardAssets.Utility.FPSCounter::m_CurrentFps
	int32_t ___m_CurrentFps_7;
	// UnityEngine.UI.Text UnityStandardAssets.Utility.FPSCounter::m_Text
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___m_Text_9;

public:
	inline static int32_t get_offset_of_m_FpsAccumulator_5() { return static_cast<int32_t>(offsetof(FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C, ___m_FpsAccumulator_5)); }
	inline int32_t get_m_FpsAccumulator_5() const { return ___m_FpsAccumulator_5; }
	inline int32_t* get_address_of_m_FpsAccumulator_5() { return &___m_FpsAccumulator_5; }
	inline void set_m_FpsAccumulator_5(int32_t value)
	{
		___m_FpsAccumulator_5 = value;
	}

	inline static int32_t get_offset_of_m_FpsNextPeriod_6() { return static_cast<int32_t>(offsetof(FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C, ___m_FpsNextPeriod_6)); }
	inline float get_m_FpsNextPeriod_6() const { return ___m_FpsNextPeriod_6; }
	inline float* get_address_of_m_FpsNextPeriod_6() { return &___m_FpsNextPeriod_6; }
	inline void set_m_FpsNextPeriod_6(float value)
	{
		___m_FpsNextPeriod_6 = value;
	}

	inline static int32_t get_offset_of_m_CurrentFps_7() { return static_cast<int32_t>(offsetof(FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C, ___m_CurrentFps_7)); }
	inline int32_t get_m_CurrentFps_7() const { return ___m_CurrentFps_7; }
	inline int32_t* get_address_of_m_CurrentFps_7() { return &___m_CurrentFps_7; }
	inline void set_m_CurrentFps_7(int32_t value)
	{
		___m_CurrentFps_7 = value;
	}

	inline static int32_t get_offset_of_m_Text_9() { return static_cast<int32_t>(offsetof(FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C, ___m_Text_9)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_m_Text_9() const { return ___m_Text_9; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_m_Text_9() { return &___m_Text_9; }
	inline void set_m_Text_9(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___m_Text_9 = value;
		Il2CppCodeGenWriteBarrier((&___m_Text_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FPSCOUNTER_T8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C_H
#ifndef FOLLOWTARGET_TCD44FEA739F1617B46941AF638C3E12AA4B87BEC_H
#define FOLLOWTARGET_TCD44FEA739F1617B46941AF638C3E12AA4B87BEC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.FollowTarget
struct  FollowTarget_tCD44FEA739F1617B46941AF638C3E12AA4B87BEC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.FollowTarget::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.FollowTarget::offset
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___offset_5;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(FollowTarget_tCD44FEA739F1617B46941AF638C3E12AA4B87BEC, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_offset_5() { return static_cast<int32_t>(offsetof(FollowTarget_tCD44FEA739F1617B46941AF638C3E12AA4B87BEC, ___offset_5)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_offset_5() const { return ___offset_5; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_offset_5() { return &___offset_5; }
	inline void set_offset_5(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___offset_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FOLLOWTARGET_TCD44FEA739F1617B46941AF638C3E12AA4B87BEC_H
#ifndef OBJECTRESETTER_TF48720C42F9CC7C8D3EBCAE91B158396383C9EB9_H
#define OBJECTRESETTER_TF48720C42F9CC7C8D3EBCAE91B158396383C9EB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ObjectResetter
struct  ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector3 UnityStandardAssets.Utility.ObjectResetter::originalPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___originalPosition_4;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.ObjectResetter::originalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___originalRotation_5;
	// System.Collections.Generic.List`1<UnityEngine.Transform> UnityStandardAssets.Utility.ObjectResetter::originalStructure
	List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * ___originalStructure_6;
	// UnityEngine.Rigidbody UnityStandardAssets.Utility.ObjectResetter::Rigidbody
	Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * ___Rigidbody_7;

public:
	inline static int32_t get_offset_of_originalPosition_4() { return static_cast<int32_t>(offsetof(ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9, ___originalPosition_4)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_originalPosition_4() const { return ___originalPosition_4; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_originalPosition_4() { return &___originalPosition_4; }
	inline void set_originalPosition_4(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___originalPosition_4 = value;
	}

	inline static int32_t get_offset_of_originalRotation_5() { return static_cast<int32_t>(offsetof(ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9, ___originalRotation_5)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_originalRotation_5() const { return ___originalRotation_5; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_originalRotation_5() { return &___originalRotation_5; }
	inline void set_originalRotation_5(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___originalRotation_5 = value;
	}

	inline static int32_t get_offset_of_originalStructure_6() { return static_cast<int32_t>(offsetof(ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9, ___originalStructure_6)); }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * get_originalStructure_6() const { return ___originalStructure_6; }
	inline List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D ** get_address_of_originalStructure_6() { return &___originalStructure_6; }
	inline void set_originalStructure_6(List_1_t1863EF4EE1FDEED14D460C85AF61BE0850892F6D * value)
	{
		___originalStructure_6 = value;
		Il2CppCodeGenWriteBarrier((&___originalStructure_6), value);
	}

	inline static int32_t get_offset_of_Rigidbody_7() { return static_cast<int32_t>(offsetof(ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9, ___Rigidbody_7)); }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * get_Rigidbody_7() const { return ___Rigidbody_7; }
	inline Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 ** get_address_of_Rigidbody_7() { return &___Rigidbody_7; }
	inline void set_Rigidbody_7(Rigidbody_tE0A58EE5A1F7DC908EFFB4F0D795AC9552A750A5 * value)
	{
		___Rigidbody_7 = value;
		Il2CppCodeGenWriteBarrier((&___Rigidbody_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTRESETTER_TF48720C42F9CC7C8D3EBCAE91B158396383C9EB9_H
#ifndef PARTICLESYSTEMDESTROYER_T65924F95D50242BD6F3F266CCB0EE436255B1C03_H
#define PARTICLESYSTEMDESTROYER_T65924F95D50242BD6F3F266CCB0EE436255B1C03_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.ParticleSystemDestroyer
struct  ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::minDuration
	float ___minDuration_4;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::maxDuration
	float ___maxDuration_5;
	// System.Single UnityStandardAssets.Utility.ParticleSystemDestroyer::m_MaxLifetime
	float ___m_MaxLifetime_6;
	// System.Boolean UnityStandardAssets.Utility.ParticleSystemDestroyer::m_EarlyStop
	bool ___m_EarlyStop_7;

public:
	inline static int32_t get_offset_of_minDuration_4() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03, ___minDuration_4)); }
	inline float get_minDuration_4() const { return ___minDuration_4; }
	inline float* get_address_of_minDuration_4() { return &___minDuration_4; }
	inline void set_minDuration_4(float value)
	{
		___minDuration_4 = value;
	}

	inline static int32_t get_offset_of_maxDuration_5() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03, ___maxDuration_5)); }
	inline float get_maxDuration_5() const { return ___maxDuration_5; }
	inline float* get_address_of_maxDuration_5() { return &___maxDuration_5; }
	inline void set_maxDuration_5(float value)
	{
		___maxDuration_5 = value;
	}

	inline static int32_t get_offset_of_m_MaxLifetime_6() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03, ___m_MaxLifetime_6)); }
	inline float get_m_MaxLifetime_6() const { return ___m_MaxLifetime_6; }
	inline float* get_address_of_m_MaxLifetime_6() { return &___m_MaxLifetime_6; }
	inline void set_m_MaxLifetime_6(float value)
	{
		___m_MaxLifetime_6 = value;
	}

	inline static int32_t get_offset_of_m_EarlyStop_7() { return static_cast<int32_t>(offsetof(ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03, ___m_EarlyStop_7)); }
	inline bool get_m_EarlyStop_7() const { return ___m_EarlyStop_7; }
	inline bool* get_address_of_m_EarlyStop_7() { return &___m_EarlyStop_7; }
	inline void set_m_EarlyStop_7(bool value)
	{
		___m_EarlyStop_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARTICLESYSTEMDESTROYER_T65924F95D50242BD6F3F266CCB0EE436255B1C03_H
#ifndef PLATFORMSPECIFICCONTENT_T9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8_H
#define PLATFORMSPECIFICCONTENT_T9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.PlatformSpecificContent
struct  PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.PlatformSpecificContent/BuildTargetGroup UnityStandardAssets.Utility.PlatformSpecificContent::m_BuildTargetGroup
	int32_t ___m_BuildTargetGroup_4;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.PlatformSpecificContent::m_Content
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___m_Content_5;
	// UnityEngine.MonoBehaviour[] UnityStandardAssets.Utility.PlatformSpecificContent::m_MonoBehaviours
	MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* ___m_MonoBehaviours_6;
	// System.Boolean UnityStandardAssets.Utility.PlatformSpecificContent::m_ChildrenOfThisObject
	bool ___m_ChildrenOfThisObject_7;

public:
	inline static int32_t get_offset_of_m_BuildTargetGroup_4() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8, ___m_BuildTargetGroup_4)); }
	inline int32_t get_m_BuildTargetGroup_4() const { return ___m_BuildTargetGroup_4; }
	inline int32_t* get_address_of_m_BuildTargetGroup_4() { return &___m_BuildTargetGroup_4; }
	inline void set_m_BuildTargetGroup_4(int32_t value)
	{
		___m_BuildTargetGroup_4 = value;
	}

	inline static int32_t get_offset_of_m_Content_5() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8, ___m_Content_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_m_Content_5() const { return ___m_Content_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_m_Content_5() { return &___m_Content_5; }
	inline void set_m_Content_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___m_Content_5 = value;
		Il2CppCodeGenWriteBarrier((&___m_Content_5), value);
	}

	inline static int32_t get_offset_of_m_MonoBehaviours_6() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8, ___m_MonoBehaviours_6)); }
	inline MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* get_m_MonoBehaviours_6() const { return ___m_MonoBehaviours_6; }
	inline MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A** get_address_of_m_MonoBehaviours_6() { return &___m_MonoBehaviours_6; }
	inline void set_m_MonoBehaviours_6(MonoBehaviourU5BU5D_tEC81C7491112CB97F70976A67ABB8C33168F5F0A* value)
	{
		___m_MonoBehaviours_6 = value;
		Il2CppCodeGenWriteBarrier((&___m_MonoBehaviours_6), value);
	}

	inline static int32_t get_offset_of_m_ChildrenOfThisObject_7() { return static_cast<int32_t>(offsetof(PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8, ___m_ChildrenOfThisObject_7)); }
	inline bool get_m_ChildrenOfThisObject_7() const { return ___m_ChildrenOfThisObject_7; }
	inline bool* get_address_of_m_ChildrenOfThisObject_7() { return &___m_ChildrenOfThisObject_7; }
	inline void set_m_ChildrenOfThisObject_7(bool value)
	{
		___m_ChildrenOfThisObject_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLATFORMSPECIFICCONTENT_T9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8_H
#ifndef SIMPLEACTIVATORMENU_T11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43_H
#define SIMPLEACTIVATORMENU_T11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleActivatorMenu
struct  SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.GUIText UnityStandardAssets.Utility.SimpleActivatorMenu::camSwitchButton
	GUIText_t1AAED515CF7E63F24B55C5FC988555DA14DA10F1 * ___camSwitchButton_4;
	// UnityEngine.GameObject[] UnityStandardAssets.Utility.SimpleActivatorMenu::objects
	GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* ___objects_5;
	// System.Int32 UnityStandardAssets.Utility.SimpleActivatorMenu::m_CurrentActiveObject
	int32_t ___m_CurrentActiveObject_6;

public:
	inline static int32_t get_offset_of_camSwitchButton_4() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43, ___camSwitchButton_4)); }
	inline GUIText_t1AAED515CF7E63F24B55C5FC988555DA14DA10F1 * get_camSwitchButton_4() const { return ___camSwitchButton_4; }
	inline GUIText_t1AAED515CF7E63F24B55C5FC988555DA14DA10F1 ** get_address_of_camSwitchButton_4() { return &___camSwitchButton_4; }
	inline void set_camSwitchButton_4(GUIText_t1AAED515CF7E63F24B55C5FC988555DA14DA10F1 * value)
	{
		___camSwitchButton_4 = value;
		Il2CppCodeGenWriteBarrier((&___camSwitchButton_4), value);
	}

	inline static int32_t get_offset_of_objects_5() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43, ___objects_5)); }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* get_objects_5() const { return ___objects_5; }
	inline GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520** get_address_of_objects_5() { return &___objects_5; }
	inline void set_objects_5(GameObjectU5BU5D_tBF9D474747511CF34A040A1697E34C74C19BB520* value)
	{
		___objects_5 = value;
		Il2CppCodeGenWriteBarrier((&___objects_5), value);
	}

	inline static int32_t get_offset_of_m_CurrentActiveObject_6() { return static_cast<int32_t>(offsetof(SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43, ___m_CurrentActiveObject_6)); }
	inline int32_t get_m_CurrentActiveObject_6() const { return ___m_CurrentActiveObject_6; }
	inline int32_t* get_address_of_m_CurrentActiveObject_6() { return &___m_CurrentActiveObject_6; }
	inline void set_m_CurrentActiveObject_6(int32_t value)
	{
		___m_CurrentActiveObject_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEACTIVATORMENU_T11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43_H
#ifndef SIMPLEMOUSEROTATOR_T8655C5C968A2F70D71C707DDC4327BF3F34EB32C_H
#define SIMPLEMOUSEROTATOR_T8655C5C968A2F70D71C707DDC4327BF3F34EB32C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SimpleMouseRotator
struct  SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Vector2 UnityStandardAssets.Utility.SimpleMouseRotator::rotationRange
	Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  ___rotationRange_4;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::rotationSpeed
	float ___rotationSpeed_5;
	// System.Single UnityStandardAssets.Utility.SimpleMouseRotator::dampingTime
	float ___dampingTime_6;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroVerticalOnMobile
	bool ___autoZeroVerticalOnMobile_7;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::autoZeroHorizontalOnMobile
	bool ___autoZeroHorizontalOnMobile_8;
	// System.Boolean UnityStandardAssets.Utility.SimpleMouseRotator::relative
	bool ___relative_9;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_TargetAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_TargetAngles_10;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowAngles
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_FollowAngles_11;
	// UnityEngine.Vector3 UnityStandardAssets.Utility.SimpleMouseRotator::m_FollowVelocity
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_FollowVelocity_12;
	// UnityEngine.Quaternion UnityStandardAssets.Utility.SimpleMouseRotator::m_OriginalRotation
	Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  ___m_OriginalRotation_13;

public:
	inline static int32_t get_offset_of_rotationRange_4() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___rotationRange_4)); }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  get_rotationRange_4() const { return ___rotationRange_4; }
	inline Vector2_tA85D2DD88578276CA8A8796756458277E72D073D * get_address_of_rotationRange_4() { return &___rotationRange_4; }
	inline void set_rotationRange_4(Vector2_tA85D2DD88578276CA8A8796756458277E72D073D  value)
	{
		___rotationRange_4 = value;
	}

	inline static int32_t get_offset_of_rotationSpeed_5() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___rotationSpeed_5)); }
	inline float get_rotationSpeed_5() const { return ___rotationSpeed_5; }
	inline float* get_address_of_rotationSpeed_5() { return &___rotationSpeed_5; }
	inline void set_rotationSpeed_5(float value)
	{
		___rotationSpeed_5 = value;
	}

	inline static int32_t get_offset_of_dampingTime_6() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___dampingTime_6)); }
	inline float get_dampingTime_6() const { return ___dampingTime_6; }
	inline float* get_address_of_dampingTime_6() { return &___dampingTime_6; }
	inline void set_dampingTime_6(float value)
	{
		___dampingTime_6 = value;
	}

	inline static int32_t get_offset_of_autoZeroVerticalOnMobile_7() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___autoZeroVerticalOnMobile_7)); }
	inline bool get_autoZeroVerticalOnMobile_7() const { return ___autoZeroVerticalOnMobile_7; }
	inline bool* get_address_of_autoZeroVerticalOnMobile_7() { return &___autoZeroVerticalOnMobile_7; }
	inline void set_autoZeroVerticalOnMobile_7(bool value)
	{
		___autoZeroVerticalOnMobile_7 = value;
	}

	inline static int32_t get_offset_of_autoZeroHorizontalOnMobile_8() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___autoZeroHorizontalOnMobile_8)); }
	inline bool get_autoZeroHorizontalOnMobile_8() const { return ___autoZeroHorizontalOnMobile_8; }
	inline bool* get_address_of_autoZeroHorizontalOnMobile_8() { return &___autoZeroHorizontalOnMobile_8; }
	inline void set_autoZeroHorizontalOnMobile_8(bool value)
	{
		___autoZeroHorizontalOnMobile_8 = value;
	}

	inline static int32_t get_offset_of_relative_9() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___relative_9)); }
	inline bool get_relative_9() const { return ___relative_9; }
	inline bool* get_address_of_relative_9() { return &___relative_9; }
	inline void set_relative_9(bool value)
	{
		___relative_9 = value;
	}

	inline static int32_t get_offset_of_m_TargetAngles_10() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___m_TargetAngles_10)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_TargetAngles_10() const { return ___m_TargetAngles_10; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_TargetAngles_10() { return &___m_TargetAngles_10; }
	inline void set_m_TargetAngles_10(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_TargetAngles_10 = value;
	}

	inline static int32_t get_offset_of_m_FollowAngles_11() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___m_FollowAngles_11)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_FollowAngles_11() const { return ___m_FollowAngles_11; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_FollowAngles_11() { return &___m_FollowAngles_11; }
	inline void set_m_FollowAngles_11(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_FollowAngles_11 = value;
	}

	inline static int32_t get_offset_of_m_FollowVelocity_12() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___m_FollowVelocity_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_FollowVelocity_12() const { return ___m_FollowVelocity_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_FollowVelocity_12() { return &___m_FollowVelocity_12; }
	inline void set_m_FollowVelocity_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_FollowVelocity_12 = value;
	}

	inline static int32_t get_offset_of_m_OriginalRotation_13() { return static_cast<int32_t>(offsetof(SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C, ___m_OriginalRotation_13)); }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  get_m_OriginalRotation_13() const { return ___m_OriginalRotation_13; }
	inline Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357 * get_address_of_m_OriginalRotation_13() { return &___m_OriginalRotation_13; }
	inline void set_m_OriginalRotation_13(Quaternion_t319F3319A7D43FFA5D819AD6C0A98851F0095357  value)
	{
		___m_OriginalRotation_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEMOUSEROTATOR_T8655C5C968A2F70D71C707DDC4327BF3F34EB32C_H
#ifndef SMOOTHFOLLOW_T6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825_H
#define SMOOTHFOLLOW_T6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.SmoothFollow
struct  SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Transform UnityStandardAssets.Utility.SmoothFollow::target
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___target_4;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::distance
	float ___distance_5;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::height
	float ___height_6;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::rotationDamping
	float ___rotationDamping_7;
	// System.Single UnityStandardAssets.Utility.SmoothFollow::heightDamping
	float ___heightDamping_8;

public:
	inline static int32_t get_offset_of_target_4() { return static_cast<int32_t>(offsetof(SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825, ___target_4)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_target_4() const { return ___target_4; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_target_4() { return &___target_4; }
	inline void set_target_4(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___target_4 = value;
		Il2CppCodeGenWriteBarrier((&___target_4), value);
	}

	inline static int32_t get_offset_of_distance_5() { return static_cast<int32_t>(offsetof(SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825, ___distance_5)); }
	inline float get_distance_5() const { return ___distance_5; }
	inline float* get_address_of_distance_5() { return &___distance_5; }
	inline void set_distance_5(float value)
	{
		___distance_5 = value;
	}

	inline static int32_t get_offset_of_height_6() { return static_cast<int32_t>(offsetof(SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825, ___height_6)); }
	inline float get_height_6() const { return ___height_6; }
	inline float* get_address_of_height_6() { return &___height_6; }
	inline void set_height_6(float value)
	{
		___height_6 = value;
	}

	inline static int32_t get_offset_of_rotationDamping_7() { return static_cast<int32_t>(offsetof(SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825, ___rotationDamping_7)); }
	inline float get_rotationDamping_7() const { return ___rotationDamping_7; }
	inline float* get_address_of_rotationDamping_7() { return &___rotationDamping_7; }
	inline void set_rotationDamping_7(float value)
	{
		___rotationDamping_7 = value;
	}

	inline static int32_t get_offset_of_heightDamping_8() { return static_cast<int32_t>(offsetof(SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825, ___heightDamping_8)); }
	inline float get_heightDamping_8() const { return ___heightDamping_8; }
	inline float* get_address_of_heightDamping_8() { return &___heightDamping_8; }
	inline void set_heightDamping_8(float value)
	{
		___heightDamping_8 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SMOOTHFOLLOW_T6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825_H
#ifndef TIMEDOBJECTACTIVATOR_T775092214A68E514052566C1D7F1089733BADC7B_H
#define TIMEDOBJECTACTIVATOR_T775092214A68E514052566C1D7F1089733BADC7B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityStandardAssets.Utility.TimedObjectActivator
struct  TimedObjectActivator_t775092214A68E514052566C1D7F1089733BADC7B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityStandardAssets.Utility.TimedObjectActivator/Entries UnityStandardAssets.Utility.TimedObjectActivator::entries
	Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E * ___entries_4;

public:
	inline static int32_t get_offset_of_entries_4() { return static_cast<int32_t>(offsetof(TimedObjectActivator_t775092214A68E514052566C1D7F1089733BADC7B, ___entries_4)); }
	inline Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E * get_entries_4() const { return ___entries_4; }
	inline Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E ** get_address_of_entries_4() { return &___entries_4; }
	inline void set_entries_4(Entries_t0337CB356ADCAF3563885A2117B6C5DA4DCB9E3E * value)
	{
		___entries_4 = value;
		Il2CppCodeGenWriteBarrier((&___entries_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMEDOBJECTACTIVATOR_T775092214A68E514052566C1D7F1089733BADC7B_H
#ifndef ZOMBIETEST_TC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE_H
#define ZOMBIETEST_TC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ZombieTest
struct  ZombieTest_tC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.Animator ZombieTest::animator
	Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * ___animator_4;

public:
	inline static int32_t get_offset_of_animator_4() { return static_cast<int32_t>(offsetof(ZombieTest_tC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE, ___animator_4)); }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * get_animator_4() const { return ___animator_4; }
	inline Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A ** get_address_of_animator_4() { return &___animator_4; }
	inline void set_animator_4(Animator_tF1A88E66B3B731DDA75A066DBAE9C55837660F5A * value)
	{
		___animator_4 = value;
		Il2CppCodeGenWriteBarrier((&___animator_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ZOMBIETEST_TC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE_H
#ifndef BOLT_T2683227E98F712FB721C4E3F8DE853EFBE9249FA_H
#define BOLT_T2683227E98F712FB721C4E3F8DE853EFBE9249FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Bolt
struct  Bolt_t2683227E98F712FB721C4E3F8DE853EFBE9249FA  : public Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D
{
public:
	// System.Single Bolt::defaultDistance
	float ___defaultDistance_12;

public:
	inline static int32_t get_offset_of_defaultDistance_12() { return static_cast<int32_t>(offsetof(Bolt_t2683227E98F712FB721C4E3F8DE853EFBE9249FA, ___defaultDistance_12)); }
	inline float get_defaultDistance_12() const { return ___defaultDistance_12; }
	inline float* get_address_of_defaultDistance_12() { return &___defaultDistance_12; }
	inline void set_defaultDistance_12(float value)
	{
		___defaultDistance_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOLT_T2683227E98F712FB721C4E3F8DE853EFBE9249FA_H
#ifndef CANTARGET_T2CFF066CBA8508F76446938BE3B126AF6DDA8AF0_H
#define CANTARGET_T2CFF066CBA8508F76446938BE3B126AF6DDA8AF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CanTarget
struct  CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0  : public Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D
{
public:
	// UnityEngine.Transform CanTarget::targetTransform
	Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * ___targetTransform_11;
	// UnityEngine.Vector3 CanTarget::m_CurrentPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_CurrentPosition_12;
	// UnityEngine.Vector3 CanTarget::m_PreviousPosition
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___m_PreviousPosition_13;
	// UnityEngine.Vector3 CanTarget::<velocity>k__BackingField
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___U3CvelocityU3Ek__BackingField_14;

public:
	inline static int32_t get_offset_of_targetTransform_11() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___targetTransform_11)); }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * get_targetTransform_11() const { return ___targetTransform_11; }
	inline Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA ** get_address_of_targetTransform_11() { return &___targetTransform_11; }
	inline void set_targetTransform_11(Transform_tBB9E78A2766C3C83599A8F66EDE7D1FCAFC66EDA * value)
	{
		___targetTransform_11 = value;
		Il2CppCodeGenWriteBarrier((&___targetTransform_11), value);
	}

	inline static int32_t get_offset_of_m_CurrentPosition_12() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___m_CurrentPosition_12)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_CurrentPosition_12() const { return ___m_CurrentPosition_12; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_CurrentPosition_12() { return &___m_CurrentPosition_12; }
	inline void set_m_CurrentPosition_12(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_CurrentPosition_12 = value;
	}

	inline static int32_t get_offset_of_m_PreviousPosition_13() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___m_PreviousPosition_13)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_m_PreviousPosition_13() const { return ___m_PreviousPosition_13; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_m_PreviousPosition_13() { return &___m_PreviousPosition_13; }
	inline void set_m_PreviousPosition_13(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___m_PreviousPosition_13 = value;
	}

	inline static int32_t get_offset_of_U3CvelocityU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0, ___U3CvelocityU3Ek__BackingField_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_U3CvelocityU3Ek__BackingField_14() const { return ___U3CvelocityU3Ek__BackingField_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_U3CvelocityU3Ek__BackingField_14() { return &___U3CvelocityU3Ek__BackingField_14; }
	inline void set_U3CvelocityU3Ek__BackingField_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___U3CvelocityU3Ek__BackingField_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CANTARGET_T2CFF066CBA8508F76446938BE3B126AF6DDA8AF0_H
#ifndef DESTRUCTABLEOBJECT_T4A489A9A6758215505C0BA0C0727111A97A89D63_H
#define DESTRUCTABLEOBJECT_T4A489A9A6758215505C0BA0C0727111A97A89D63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// DestructableObject
struct  DestructableObject_t4A489A9A6758215505C0BA0C0727111A97A89D63  : public Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D
{
public:
	// UnityEngine.GameObject DestructableObject::onDestroyEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onDestroyEffect_11;

public:
	inline static int32_t get_offset_of_onDestroyEffect_11() { return static_cast<int32_t>(offsetof(DestructableObject_t4A489A9A6758215505C0BA0C0727111A97A89D63, ___onDestroyEffect_11)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_onDestroyEffect_11() const { return ___onDestroyEffect_11; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_onDestroyEffect_11() { return &___onDestroyEffect_11; }
	inline void set_onDestroyEffect_11(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___onDestroyEffect_11 = value;
		Il2CppCodeGenWriteBarrier((&___onDestroyEffect_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DESTRUCTABLEOBJECT_T4A489A9A6758215505C0BA0C0727111A97A89D63_H
#ifndef ENEMY_TFD4DFBD9B55680685776C01FAC4E6877F5935DF6_H
#define ENEMY_TFD4DFBD9B55680685776C01FAC4E6877F5935DF6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Enemy
struct  Enemy_tFD4DFBD9B55680685776C01FAC4E6877F5935DF6  : public Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D
{
public:
	// SimpleHealthBar Enemy::healthBar
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * ___healthBar_11;
	// UnityEngine.GameObject Enemy::onDeathEffect
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___onDeathEffect_12;

public:
	inline static int32_t get_offset_of_healthBar_11() { return static_cast<int32_t>(offsetof(Enemy_tFD4DFBD9B55680685776C01FAC4E6877F5935DF6, ___healthBar_11)); }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * get_healthBar_11() const { return ___healthBar_11; }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 ** get_address_of_healthBar_11() { return &___healthBar_11; }
	inline void set_healthBar_11(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * value)
	{
		___healthBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_11), value);
	}

	inline static int32_t get_offset_of_onDeathEffect_12() { return static_cast<int32_t>(offsetof(Enemy_tFD4DFBD9B55680685776C01FAC4E6877F5935DF6, ___onDeathEffect_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_onDeathEffect_12() const { return ___onDeathEffect_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_onDeathEffect_12() { return &___onDeathEffect_12; }
	inline void set_onDeathEffect_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___onDeathEffect_12 = value;
		Il2CppCodeGenWriteBarrier((&___onDeathEffect_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENEMY_TFD4DFBD9B55680685776C01FAC4E6877F5935DF6_H
#ifndef HOMEBASE_TDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_H
#define HOMEBASE_TDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HomeBase
struct  HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8  : public Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D
{
public:

public:
};

struct HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_StaticFields
{
public:
	// HomeBase HomeBase::<Instance>k__BackingField
	HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8 * ___U3CInstanceU3Ek__BackingField_11;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_StaticFields, ___U3CInstanceU3Ek__BackingField_11)); }
	inline HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8 * get_U3CInstanceU3Ek__BackingField_11() const { return ___U3CInstanceU3Ek__BackingField_11; }
	inline HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8 ** get_address_of_U3CInstanceU3Ek__BackingField_11() { return &___U3CInstanceU3Ek__BackingField_11; }
	inline void set_U3CInstanceU3Ek__BackingField_11(HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8 * value)
	{
		___U3CInstanceU3Ek__BackingField_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOMEBASE_TDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_H
#ifndef LOSE_T1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA_H
#define LOSE_T1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Lose
struct  Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA  : public Singleton_1_t92032249C7BB318D3055215153531C57A9D8F21B
{
public:
	// System.Action`1<EventParam> Lose::lost
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___lost_5;

public:
	inline static int32_t get_offset_of_lost_5() { return static_cast<int32_t>(offsetof(Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA, ___lost_5)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_lost_5() const { return ___lost_5; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_lost_5() { return &___lost_5; }
	inline void set_lost_5(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___lost_5 = value;
		Il2CppCodeGenWriteBarrier((&___lost_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOSE_T1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA_H
#ifndef PLACEMENTMANAGER_TD6D76A0D4E35EF52316D2987336D0E924C035AB2_H
#define PLACEMENTMANAGER_TD6D76A0D4E35EF52316D2987336D0E924C035AB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PlacementManager
struct  PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2  : public Singleton_1_tD582104BEBDCF0AAD5A8466B04D5F64431BF09D5
{
public:
	// UnityEngine.GameObject PlacementManager::prefabPlacementObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefabPlacementObject_5;
	// UnityEngine.GameObject PlacementManager::prefabOK
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefabOK_6;
	// UnityEngine.GameObject PlacementManager::prefabFail
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___prefabFail_7;
	// System.Single PlacementManager::gridUnit
	float ___gridUnit_8;
	// System.Int32 PlacementManager::sizeX
	int32_t ___sizeX_9;
	// System.Single PlacementManager::sizeY
	float ___sizeY_10;
	// System.Int32 PlacementManager::sizeZ
	int32_t ___sizeZ_11;
	// UnityEngine.GameObject PlacementManager::placementObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___placementObject_12;
	// UnityEngine.GameObject PlacementManager::areaObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___areaObject_13;
	// UnityEngine.Vector3 PlacementManager::lastPos
	Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  ___lastPos_14;

public:
	inline static int32_t get_offset_of_prefabPlacementObject_5() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___prefabPlacementObject_5)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefabPlacementObject_5() const { return ___prefabPlacementObject_5; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefabPlacementObject_5() { return &___prefabPlacementObject_5; }
	inline void set_prefabPlacementObject_5(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefabPlacementObject_5 = value;
		Il2CppCodeGenWriteBarrier((&___prefabPlacementObject_5), value);
	}

	inline static int32_t get_offset_of_prefabOK_6() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___prefabOK_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefabOK_6() const { return ___prefabOK_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefabOK_6() { return &___prefabOK_6; }
	inline void set_prefabOK_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefabOK_6 = value;
		Il2CppCodeGenWriteBarrier((&___prefabOK_6), value);
	}

	inline static int32_t get_offset_of_prefabFail_7() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___prefabFail_7)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_prefabFail_7() const { return ___prefabFail_7; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_prefabFail_7() { return &___prefabFail_7; }
	inline void set_prefabFail_7(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___prefabFail_7 = value;
		Il2CppCodeGenWriteBarrier((&___prefabFail_7), value);
	}

	inline static int32_t get_offset_of_gridUnit_8() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___gridUnit_8)); }
	inline float get_gridUnit_8() const { return ___gridUnit_8; }
	inline float* get_address_of_gridUnit_8() { return &___gridUnit_8; }
	inline void set_gridUnit_8(float value)
	{
		___gridUnit_8 = value;
	}

	inline static int32_t get_offset_of_sizeX_9() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___sizeX_9)); }
	inline int32_t get_sizeX_9() const { return ___sizeX_9; }
	inline int32_t* get_address_of_sizeX_9() { return &___sizeX_9; }
	inline void set_sizeX_9(int32_t value)
	{
		___sizeX_9 = value;
	}

	inline static int32_t get_offset_of_sizeY_10() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___sizeY_10)); }
	inline float get_sizeY_10() const { return ___sizeY_10; }
	inline float* get_address_of_sizeY_10() { return &___sizeY_10; }
	inline void set_sizeY_10(float value)
	{
		___sizeY_10 = value;
	}

	inline static int32_t get_offset_of_sizeZ_11() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___sizeZ_11)); }
	inline int32_t get_sizeZ_11() const { return ___sizeZ_11; }
	inline int32_t* get_address_of_sizeZ_11() { return &___sizeZ_11; }
	inline void set_sizeZ_11(int32_t value)
	{
		___sizeZ_11 = value;
	}

	inline static int32_t get_offset_of_placementObject_12() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___placementObject_12)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_placementObject_12() const { return ___placementObject_12; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_placementObject_12() { return &___placementObject_12; }
	inline void set_placementObject_12(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___placementObject_12 = value;
		Il2CppCodeGenWriteBarrier((&___placementObject_12), value);
	}

	inline static int32_t get_offset_of_areaObject_13() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___areaObject_13)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_areaObject_13() const { return ___areaObject_13; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_areaObject_13() { return &___areaObject_13; }
	inline void set_areaObject_13(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___areaObject_13 = value;
		Il2CppCodeGenWriteBarrier((&___areaObject_13), value);
	}

	inline static int32_t get_offset_of_lastPos_14() { return static_cast<int32_t>(offsetof(PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2, ___lastPos_14)); }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  get_lastPos_14() const { return ___lastPos_14; }
	inline Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720 * get_address_of_lastPos_14() { return &___lastPos_14; }
	inline void set_lastPos_14(Vector3_tDCF05E21F632FE2BA260C06E0D10CA81513E6720  value)
	{
		___lastPos_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLACEMENTMANAGER_TD6D76A0D4E35EF52316D2987336D0E924C035AB2_H
#ifndef PLAYER_T8321F4671F549F5A7793BB8BA33D32CCCD538873_H
#define PLAYER_T8321F4671F549F5A7793BB8BA33D32CCCD538873_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Player
struct  Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873  : public Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D
{
public:
	// SimpleHealthBar Player::healthBar
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * ___healthBar_11;
	// System.Single Player::lowHealthPercentage
	float ___lowHealthPercentage_12;
	// System.Single Player::highHealthPercentage
	float ___highHealthPercentage_13;
	// UnityEngine.Color Player::lowHealthColour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___lowHealthColour_14;
	// UnityEngine.Color Player::medHealthColour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___medHealthColour_15;
	// UnityEngine.Color Player::highHealthColour
	Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  ___highHealthColour_16;

public:
	inline static int32_t get_offset_of_healthBar_11() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___healthBar_11)); }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * get_healthBar_11() const { return ___healthBar_11; }
	inline SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 ** get_address_of_healthBar_11() { return &___healthBar_11; }
	inline void set_healthBar_11(SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5 * value)
	{
		___healthBar_11 = value;
		Il2CppCodeGenWriteBarrier((&___healthBar_11), value);
	}

	inline static int32_t get_offset_of_lowHealthPercentage_12() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___lowHealthPercentage_12)); }
	inline float get_lowHealthPercentage_12() const { return ___lowHealthPercentage_12; }
	inline float* get_address_of_lowHealthPercentage_12() { return &___lowHealthPercentage_12; }
	inline void set_lowHealthPercentage_12(float value)
	{
		___lowHealthPercentage_12 = value;
	}

	inline static int32_t get_offset_of_highHealthPercentage_13() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___highHealthPercentage_13)); }
	inline float get_highHealthPercentage_13() const { return ___highHealthPercentage_13; }
	inline float* get_address_of_highHealthPercentage_13() { return &___highHealthPercentage_13; }
	inline void set_highHealthPercentage_13(float value)
	{
		___highHealthPercentage_13 = value;
	}

	inline static int32_t get_offset_of_lowHealthColour_14() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___lowHealthColour_14)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_lowHealthColour_14() const { return ___lowHealthColour_14; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_lowHealthColour_14() { return &___lowHealthColour_14; }
	inline void set_lowHealthColour_14(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___lowHealthColour_14 = value;
	}

	inline static int32_t get_offset_of_medHealthColour_15() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___medHealthColour_15)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_medHealthColour_15() const { return ___medHealthColour_15; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_medHealthColour_15() { return &___medHealthColour_15; }
	inline void set_medHealthColour_15(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___medHealthColour_15 = value;
	}

	inline static int32_t get_offset_of_highHealthColour_16() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873, ___highHealthColour_16)); }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  get_highHealthColour_16() const { return ___highHealthColour_16; }
	inline Color_t119BCA590009762C7223FDD3AF9706653AC84ED2 * get_address_of_highHealthColour_16() { return &___highHealthColour_16; }
	inline void set_highHealthColour_16(Color_t119BCA590009762C7223FDD3AF9706653AC84ED2  value)
	{
		___highHealthColour_16 = value;
	}
};

struct Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields
{
public:
	// Player Player::<Instance>k__BackingField
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * ___U3CInstanceU3Ek__BackingField_17;

public:
	inline static int32_t get_offset_of_U3CInstanceU3Ek__BackingField_17() { return static_cast<int32_t>(offsetof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields, ___U3CInstanceU3Ek__BackingField_17)); }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * get_U3CInstanceU3Ek__BackingField_17() const { return ___U3CInstanceU3Ek__BackingField_17; }
	inline Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 ** get_address_of_U3CInstanceU3Ek__BackingField_17() { return &___U3CInstanceU3Ek__BackingField_17; }
	inline void set_U3CInstanceU3Ek__BackingField_17(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873 * value)
	{
		___U3CInstanceU3Ek__BackingField_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CInstanceU3Ek__BackingField_17), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PLAYER_T8321F4671F549F5A7793BB8BA33D32CCCD538873_H
#ifndef RESTART_TC07A2A8733632EE248BE4ACE644A26A95546032E_H
#define RESTART_TC07A2A8733632EE248BE4ACE644A26A95546032E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Restart
struct  Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E  : public Singleton_1_t25D5D59A3B4D4F9772DDDE55C9A5DFFF72A671FD
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTART_TC07A2A8733632EE248BE4ACE644A26A95546032E_H
#ifndef SCORE_T72F7EE757BE7D4C7846803B3072753760AB6427F_H
#define SCORE_T72F7EE757BE7D4C7846803B3072753760AB6427F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Score
struct  Score_t72F7EE757BE7D4C7846803B3072753760AB6427F  : public Singleton_1_tDFB0300C50E783DC7ADC89FC760E2D37B50C8679
{
public:
	// UnityEngine.UI.Text Score::scoreText
	Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * ___scoreText_5;
	// System.Single Score::score
	float ___score_6;

public:
	inline static int32_t get_offset_of_scoreText_5() { return static_cast<int32_t>(offsetof(Score_t72F7EE757BE7D4C7846803B3072753760AB6427F, ___scoreText_5)); }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * get_scoreText_5() const { return ___scoreText_5; }
	inline Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 ** get_address_of_scoreText_5() { return &___scoreText_5; }
	inline void set_scoreText_5(Text_tE9317B57477F4B50AA4C16F460DE6F82DAD6D030 * value)
	{
		___scoreText_5 = value;
		Il2CppCodeGenWriteBarrier((&___scoreText_5), value);
	}

	inline static int32_t get_offset_of_score_6() { return static_cast<int32_t>(offsetof(Score_t72F7EE757BE7D4C7846803B3072753760AB6427F, ___score_6)); }
	inline float get_score_6() const { return ___score_6; }
	inline float* get_address_of_score_6() { return &___score_6; }
	inline void set_score_6(float value)
	{
		___score_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCORE_T72F7EE757BE7D4C7846803B3072753760AB6427F_H
#ifndef SCREENFADE_T62F6AE98359C462F3A05487360AAAC4E7EB65E71_H
#define SCREENFADE_T62F6AE98359C462F3A05487360AAAC4E7EB65E71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScreenFade
struct  ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71  : public Singleton_1_t20844B3009FAC77AFA3AF5511ACADA1C33792153
{
public:
	// System.Single ScreenFade::flashNum
	float ___flashNum_5;
	// System.Int32 ScreenFade::flashCheck
	int32_t ___flashCheck_6;
	// UnityEngine.UI.Image ScreenFade::image
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ___image_7;
	// System.Single ScreenFade::start
	float ___start_8;
	// System.Single ScreenFade::end
	float ___end_9;
	// System.Single ScreenFade::length
	float ___length_10;

public:
	inline static int32_t get_offset_of_flashNum_5() { return static_cast<int32_t>(offsetof(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71, ___flashNum_5)); }
	inline float get_flashNum_5() const { return ___flashNum_5; }
	inline float* get_address_of_flashNum_5() { return &___flashNum_5; }
	inline void set_flashNum_5(float value)
	{
		___flashNum_5 = value;
	}

	inline static int32_t get_offset_of_flashCheck_6() { return static_cast<int32_t>(offsetof(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71, ___flashCheck_6)); }
	inline int32_t get_flashCheck_6() const { return ___flashCheck_6; }
	inline int32_t* get_address_of_flashCheck_6() { return &___flashCheck_6; }
	inline void set_flashCheck_6(int32_t value)
	{
		___flashCheck_6 = value;
	}

	inline static int32_t get_offset_of_image_7() { return static_cast<int32_t>(offsetof(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71, ___image_7)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get_image_7() const { return ___image_7; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of_image_7() { return &___image_7; }
	inline void set_image_7(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		___image_7 = value;
		Il2CppCodeGenWriteBarrier((&___image_7), value);
	}

	inline static int32_t get_offset_of_start_8() { return static_cast<int32_t>(offsetof(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71, ___start_8)); }
	inline float get_start_8() const { return ___start_8; }
	inline float* get_address_of_start_8() { return &___start_8; }
	inline void set_start_8(float value)
	{
		___start_8 = value;
	}

	inline static int32_t get_offset_of_end_9() { return static_cast<int32_t>(offsetof(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71, ___end_9)); }
	inline float get_end_9() const { return ___end_9; }
	inline float* get_address_of_end_9() { return &___end_9; }
	inline void set_end_9(float value)
	{
		___end_9 = value;
	}

	inline static int32_t get_offset_of_length_10() { return static_cast<int32_t>(offsetof(ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71, ___length_10)); }
	inline float get_length_10() const { return ___length_10; }
	inline float* get_address_of_length_10() { return &___length_10; }
	inline void set_length_10(float value)
	{
		___length_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCREENFADE_T62F6AE98359C462F3A05487360AAAC4E7EB65E71_H
#ifndef UNITYDEEPLINKS_T6797A73F40586A62CEB942309A0F8876516BF912_H
#define UNITYDEEPLINKS_T6797A73F40586A62CEB942309A0F8876516BF912_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// UnityDeeplinks
struct  UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912  : public Singleton_1_t01CF12AA8045F973A4FE8095AE0CA184EBEAEF92
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNITYDEEPLINKS_T6797A73F40586A62CEB942309A0F8876516BF912_H
#ifndef WIN_T42187ECF531C2CEDC56152667B071E3E7FAEBC24_H
#define WIN_T42187ECF531C2CEDC56152667B071E3E7FAEBC24_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Win
struct  Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24  : public Singleton_1_t89728DDB3ED716070C09E898F1CB1BA527DF4257
{
public:
	// System.Action`1<EventParam> Win::won
	Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * ___won_5;

public:
	inline static int32_t get_offset_of_won_5() { return static_cast<int32_t>(offsetof(Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24, ___won_5)); }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * get_won_5() const { return ___won_5; }
	inline Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 ** get_address_of_won_5() { return &___won_5; }
	inline void set_won_5(Action_1_t0DA587DE27AC78C44FBD6A38F39B9C6AA0633CB0 * value)
	{
		___won_5 = value;
		Il2CppCodeGenWriteBarrier((&___won_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WIN_T42187ECF531C2CEDC56152667B071E3E7FAEBC24_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7400 = { sizeof (CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B), -1, sizeof(CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7400[6] = 
{
	CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B::get_offset_of_LaunchArgument_4(),
	CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B::get_offset_of_LoadLevelId_5(),
	CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B::get_offset_of_CommandArgs_6(),
	CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B::get_offset_of__guiState_7(),
	CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B_StaticFields::get_offset_of_windowSize_8(),
	CommandLineChecker_t68AF719DB54C6031DC25AF154AF9203F539B725B::get_offset_of_windowRect0_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7401 = { sizeof (ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7401[7] = 
{
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__rect_4(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__progressText_5(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__currentProgress_6(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__maximumValue_7(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__minimumValue_8(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__stepValue_9(),
	ProgressBar_tAEBA7E1A59C678A8651AEF196DDD334625C6164C::get_offset_of__localScale_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7402 = { sizeof (GUIState_t6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7402[3] = 
{
	GUIState_t6ED87AD5CC5F8A3CACE6C6DDF791265DC4F25134::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7403 = { sizeof (Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7403[35] = 
{
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_m_launcher_4(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_m_installer_5(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_ActivatePatcher_6(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_VersionsFileDownloadURL_7(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_PatchesDirectoryURL_8(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_AppToLaunch_9(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_CloseLauncherOnStart_10(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_Argument_11(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_UseRawArgument_12(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_IsIntegrated_13(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_SceneToLoad_14(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_ActivateInstaller_15(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_ActivateRepairer_16(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_BuildsDirectoryURL_17(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_LauncherName_18(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_InstallInLocalPath_19(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_ProgramFilesDirectoryToInstall_20(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_CreateDesktopShortcut_21(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_DownloadRetryAttempts_22(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_EnableCredentials_23(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_Username_24(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_Password_25(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_MainBar_26(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_DetailBar_27(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_MainLog_28(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_DetailLog_29(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_LaunchButton_30(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_Overlay_31(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_MainMenu_32(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_RestartMenu_33(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_OptionsMenu_34(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of_m_updateCheckingThread_35(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of__lastTime_36(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of__lastSize_37(),
	Launcher_t27DF47051ABB500108780CEA7ABCF602164A0602::get_offset_of__downloadSpeed_38(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7404 = { sizeof (LauncherStatus_t376D625711B0C059729302BDEA9F2160A84AF41F)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7404[3] = 
{
	LauncherStatus_t376D625711B0C059729302BDEA9F2160A84AF41F::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7405 = { sizeof (U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7405[3] = 
{
	U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF::get_offset_of_max_1(),
	U3CU3Ec__DisplayClass34_0_tD9953DC8A43FFD0BB48790E11E2E5435C61FE7FF::get_offset_of_min_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7406 = { sizeof (U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7406[3] = 
{
	U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649::get_offset_of_max_1(),
	U3CU3Ec__DisplayClass35_0_t50AEE1AC7515EF4ED2355AA811EAEA8F97DD4649::get_offset_of_min_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7407 = { sizeof (U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7407[3] = 
{
	U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25::get_offset_of_main_1(),
	U3CU3Ec__DisplayClass38_0_t0EC3BEC0CDD9C4AF316429FFF7B0CC9A253DBA25::get_offset_of_detail_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7408 = { sizeof (U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7408[3] = 
{
	U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC::get_offset_of_main_1(),
	U3CU3Ec__DisplayClass39_0_t61E53D9C00D00A6F2CE0FA4D26790939964CADDC::get_offset_of_detail_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7409 = { sizeof (U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7409[3] = 
{
	U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872::get_offset_of_main_1(),
	U3CU3Ec__DisplayClass40_0_t9C3960F46C0599B1C1471089577DADB400006872::get_offset_of_detail_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7410 = { sizeof (U3CU3Ec__DisplayClass41_0_tE1590658328F6B1A5B3997FE6C700B9702F74BAC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7410[2] = 
{
	U3CU3Ec__DisplayClass41_0_tE1590658328F6B1A5B3997FE6C700B9702F74BAC::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass41_0_tE1590658328F6B1A5B3997FE6C700B9702F74BAC::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7411 = { sizeof (U3CU3Ec__DisplayClass42_0_t503E84884773FA588880981AF1D54A1FF5FCC3A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7411[2] = 
{
	U3CU3Ec__DisplayClass42_0_t503E84884773FA588880981AF1D54A1FF5FCC3A1::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass42_0_t503E84884773FA588880981AF1D54A1FF5FCC3A1::get_offset_of_message_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7412 = { sizeof (U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7412[4] = 
{
	U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5::get_offset_of_percentageCompleted_1(),
	U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5::get_offset_of_currentFileSize_2(),
	U3CU3Ec__DisplayClass46_0_t5BC53FED7FC5204240677469CC264CD12001B3D5::get_offset_of_totalFileSize_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7413 = { sizeof (SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7413[10] = 
{
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_barImage_4(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_colorMode_5(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_barColor_6(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_barGradient_7(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_displayText_8(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_barText_9(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_additionalText_10(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of__currentFraction_11(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of__maxValue_12(),
	SimpleHealthBar_t5AA224414BB1D8B5417DC0FD241372CC2DB23CC5::get_offset_of_targetFill_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7414 = { sizeof (ColorMode_tE457C6ADD8CF97DFCE07D3E77B05680CB318212E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7414[3] = 
{
	ColorMode_tE457C6ADD8CF97DFCE07D3E77B05680CB318212E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7415 = { sizeof (DisplayText_t7A4193422DA4378F30BF7A49042056A94F3F8A41)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7415[5] = 
{
	DisplayText_t7A4193422DA4378F30BF7A49042056A94F3F8A41::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7416 = { sizeof (GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7416[4] = 
{
	GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917::get_offset_of_playerDead_4(),
	GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917::get_offset_of_winScore_5(),
	GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917::get_offset_of_enemyDead_6(),
	GameEventHandler_tFF35528BFF12FD8289A5380DDB1A755ABCE84917::get_offset_of_U3CWinScoreValueU3Ek__BackingField_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7417 = { sizeof (GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16), -1, sizeof(GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7417[2] = 
{
	GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16::get_offset_of_eventDictionary_4(),
	GameEventManager_t11533E03B357685A7E3C1CD74A336EEFAC995C16_StaticFields::get_offset_of_gameEventManager_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7418 = { sizeof (GameEvent_tD296FF101AF6CB90E22B80A20A4261D8BCAB3183)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7418[24] = 
{
	GameEvent_tD296FF101AF6CB90E22B80A20A4261D8BCAB3183::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7419 = { sizeof (EventParam_tE4C698E268EA31868630820B4529293635F87174)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7419[6] = 
{
	EventParam_tE4C698E268EA31868630820B4529293635F87174::get_offset_of_String_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventParam_tE4C698E268EA31868630820B4529293635F87174::get_offset_of_Int_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventParam_tE4C698E268EA31868630820B4529293635F87174::get_offset_of_Float_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventParam_tE4C698E268EA31868630820B4529293635F87174::get_offset_of_Bool_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventParam_tE4C698E268EA31868630820B4529293635F87174::get_offset_of_Vector3_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	EventParam_tE4C698E268EA31868630820B4529293635F87174::get_offset_of_AlignmentProvider_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7420 = { sizeof (DestructableObject_t4A489A9A6758215505C0BA0C0727111A97A89D63), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7420[1] = 
{
	DestructableObject_t4A489A9A6758215505C0BA0C0727111A97A89D63::get_offset_of_onDestroyEffect_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7421 = { sizeof (Enemy_tFD4DFBD9B55680685776C01FAC4E6877F5935DF6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7421[2] = 
{
	Enemy_tFD4DFBD9B55680685776C01FAC4E6877F5935DF6::get_offset_of_healthBar_11(),
	Enemy_tFD4DFBD9B55680685776C01FAC4E6877F5935DF6::get_offset_of_onDeathEffect_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7422 = { sizeof (Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7422[7] = 
{
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_alignment_4(),
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_alignmentProvider_5(),
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_MaxHealth_6(),
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_CurrentHealth_7(),
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_removed_8(),
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_healthChanged_9(),
	Health_tBF57F4E44FB957705C47BECA5A106DDDB2C1FE7D::get_offset_of_died_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7423 = { sizeof (Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873), -1, sizeof(Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7423[7] = 
{
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_healthBar_11(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_lowHealthPercentage_12(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_highHealthPercentage_13(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_lowHealthColour_14(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_medHealthColour_15(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873::get_offset_of_highHealthColour_16(),
	Player_t8321F4671F549F5A7793BB8BA33D32CCCD538873_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7424 = { sizeof (RectLookAtTarget_t79F7A65C8D237262B735E4E715E09A94740392DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7424[2] = 
{
	RectLookAtTarget_t79F7A65C8D237262B735E4E715E09A94740392DC::get_offset_of_target_4(),
	RectLookAtTarget_t79F7A65C8D237262B735E4E715E09A94740392DC::get_offset_of_rect_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7425 = { sizeof (ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7425[6] = 
{
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71::get_offset_of_flashNum_5(),
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71::get_offset_of_flashCheck_6(),
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71::get_offset_of_image_7(),
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71::get_offset_of_start_8(),
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71::get_offset_of_end_9(),
	ScreenFade_t62F6AE98359C462F3A05487360AAAC4E7EB65E71::get_offset_of_length_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7426 = { sizeof (MenuEventHandler_t20B78868BE68C2673919DB632F87B209543F98C9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7426[2] = 
{
	MenuEventHandler_t20B78868BE68C2673919DB632F87B209543F98C9::get_offset_of_newGame_4(),
	MenuEventHandler_t20B78868BE68C2673919DB632F87B209543F98C9::get_offset_of_quit_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7427 = { sizeof (OnClickGameEventTrigger_t9030AEB71286BC0F29BF2E08659240A420F6BFD3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7427[1] = 
{
	OnClickGameEventTrigger_t9030AEB71286BC0F29BF2E08659240A420F6BFD3::get_offset_of_gameEventToTrigger_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7428 = { sizeof (Pause_tC27C5C290DF55219F3ACD2CD1CD2D6E8254EA5A5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7429 = { sizeof (Restart_tC07A2A8733632EE248BE4ACE644A26A95546032E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7430 = { sizeof (RestartMenu_tA4A63306081E7D79860F1DA13F90B839FA3EF7FC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7430[2] = 
{
	RestartMenu_tA4A63306081E7D79860F1DA13F90B839FA3EF7FC::get_offset_of_restartMenu_4(),
	RestartMenu_tA4A63306081E7D79860F1DA13F90B839FA3EF7FC::get_offset_of_restartUI_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7431 = { sizeof (PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7431[4] = 
{
	PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3::get_offset_of_gridUnit_4(),
	PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3::get_offset_of_U3CxMaxU3Ek__BackingField_5(),
	PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3::get_offset_of_U3CzMaxU3Ek__BackingField_6(),
	PlaceableSurface_tB529C5A5A7869931E85E639C82EED07271F7B5A3::get_offset_of_placedObjects_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7432 = { sizeof (PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7432[10] = 
{
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_prefabPlacementObject_5(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_prefabOK_6(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_prefabFail_7(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_gridUnit_8(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_sizeX_9(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_sizeY_10(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_sizeZ_11(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_placementObject_12(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_areaObject_13(),
	PlacementManager_tD6D76A0D4E35EF52316D2987336D0E924C035AB2::get_offset_of_lastPos_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7433 = { sizeof (U3CU3Ec__DisplayClass25_0_tC8142EFC601BE7AD968547C2E5C193272A353F69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7433[2] = 
{
	U3CU3Ec__DisplayClass25_0_tC8142EFC601BE7AD968547C2E5C193272A353F69::get_offset_of_obj_0(),
	U3CU3Ec__DisplayClass25_0_tC8142EFC601BE7AD968547C2E5C193272A353F69::get_offset_of_U3CU3E9__0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7434 = { sizeof (Bolt_t2683227E98F712FB721C4E3F8DE853EFBE9249FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7434[1] = 
{
	Bolt_t2683227E98F712FB721C4E3F8DE853EFBE9249FA::get_offset_of_defaultDistance_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7435 = { sizeof (ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7435[8] = 
{
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_delay_4(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_m_Timer_5(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_damage_6(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_alignmentProvider_7(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_m_Enemy_8(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_fireParticleSystem_9(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_collisionParticles_10(),
	ParticleProjectile_t29371705B536834A390DE525D11C2F84C8CB5FF4::get_offset_of_m_PauseTimer_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7436 = { sizeof (Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7436[8] = 
{
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_projectileSpeed_4(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_damageValue_5(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_lifeSpan_6(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_onHitEffect_7(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_spawnPoint_8(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_spawnEffect_9(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_fired_10(),
	Projectile_t84D26E956289868C2E474F1A0C2A5EC645EAFC6D::get_offset_of_alignmentProvider_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7437 = { sizeof (U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7437[3] = 
{
	U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69::get_offset_of_U3CU3E1__state_0(),
	U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69::get_offset_of_U3CU3E2__current_1(),
	U3CDurationStopU3Ed__13_t7DA119F17CD7D5C98CFF242F16FE6EF338577F69::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7438 = { sizeof (SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7438[2] = 
{
	SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A::get_offset_of_duration_4(),
	SpecialEffect_tE5D8C3CC5BB0354E84E76A954C3BAE5692A8AE6A::get_offset_of_cleanUpTime_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7439 = { sizeof (U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7439[3] = 
{
	U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A::get_offset_of_U3CU3E1__state_0(),
	U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A::get_offset_of_U3CU3E2__current_1(),
	U3CDurationStopU3Ed__3_t6390BE3FCA39C9ABB412C7F135A5D2F48732B78A::get_offset_of_U3CU3E4__this_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7440 = { sizeof (U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7440[4] = 
{
	U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02::get_offset_of_U3CU3E1__state_0(),
	U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02::get_offset_of_U3CU3E2__current_1(),
	U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02::get_offset_of_waitTime_2(),
	U3CWaitForDestructionU3Ed__4_tA313FCDE5EDB244C740B5265142E98A89D8B5B02::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7441 = { sizeof (Score_t72F7EE757BE7D4C7846803B3072753760AB6427F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7441[2] = 
{
	Score_t72F7EE757BE7D4C7846803B3072753760AB6427F::get_offset_of_scoreText_5(),
	Score_t72F7EE757BE7D4C7846803B3072753760AB6427F::get_offset_of_score_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7442 = { sizeof (CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7442[4] = 
{
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0::get_offset_of_targetTransform_11(),
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0::get_offset_of_m_CurrentPosition_12(),
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0::get_offset_of_m_PreviousPosition_13(),
	CanTarget_t2CFF066CBA8508F76446938BE3B126AF6DDA8AF0::get_offset_of_U3CvelocityU3Ek__BackingField_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7443 = { sizeof (LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7443[21] = 
{
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_alignment_4(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_alignmentProvider_5(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_targetEntersRange_6(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_targetExitsRange_7(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_acquiredTarget_8(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_lostTarget_9(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_turret_10(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_turretXRotationRange_11(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_onlyYTurretRotation_12(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_searchRate_13(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_idleRotationSpeed_14(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_idleCorrectionTime_15(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_attachedCollider_16(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_idleWaitTime_17(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_TargetsInRange_18(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_SearchTimer_19(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_WaitTimer_20(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_CurrentCanTarget_21(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_XRotationCorrectionTime_22(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_HadTarget_23(),
	LockOnTarget_t727DCADAC20A8B4EBFF3C90F8A643D39FE436283::get_offset_of_m_CurrentRotationSpeed_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7444 = { sizeof (CopyComponent_t6A4912F858C5A927132D3E2E1E813BA8A81D4074), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7445 = { sizeof (Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7445[1] = 
{
	Lose_t1A8954FB3AB03671FF9CA1CDB83AD32C5CF5A5CA::get_offset_of_lost_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7446 = { sizeof (Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7446[1] = 
{
	Win_t42187ECF531C2CEDC56152667B071E3E7FAEBC24::get_offset_of_won_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7447 = { sizeof (DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7447[3] = 
{
	DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050::get_offset_of_deathParticleSystemPrefab_4(),
	DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050::get_offset_of_deathEffectOffset_5(),
	DeathEffect_tEEBBDB4D9052B4533E5C9F97C1F72B412E2B3050::get_offset_of_health_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7448 = { sizeof (HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7448[5] = 
{
	HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1::get_offset_of_health_4(),
	HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1::get_offset_of_healthBar_5(),
	HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1::get_offset_of_backgroundBar_6(),
	HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1::get_offset_of_showWhenFull_7(),
	HealthVisualizer_t02E5D62DC19403A3940E800D331346BC34CB09A1::get_offset_of_m_CameraToFace_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7449 = { sizeof (Edge_t537CEFC8CC07A43A6D7658B0F4DBE06BCDEC1507), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7450 = { sizeof (Invasion_t0D8DEDF915C57F49CA4A0CB15EE75703DC38E054), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7451 = { sizeof (SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7451[14] = 
{
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_spawnEffect_4(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_projectile_5(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_hitEffect_6(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_minFireInterval_7(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_maxFireInterval_8(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_stepInterval_9(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_forwardStep_10(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_moveDirection_11(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_movementModifier_12(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_stepDistance_13(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_moveFoward_14(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_damage_15(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_locked_16(),
	SpaceInvaderAI_tC408B223F3570D03E1EEC2878CB27A2492DA37A8::get_offset_of_edgeReached_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7452 = { sizeof (U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7452[4] = 
{
	U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7::get_offset_of_U3CU3E1__state_0(),
	U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7::get_offset_of_U3CU3E2__current_1(),
	U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7::get_offset_of_waitTime_2(),
	U3CWaitToFireU3Ed__17_t23968FBC1439B22031C6D50029436E083DDBD3E7::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7453 = { sizeof (U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7453[4] = 
{
	U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719::get_offset_of_U3CU3E1__state_0(),
	U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719::get_offset_of_U3CU3E2__current_1(),
	U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719::get_offset_of_waitTime_2(),
	U3CWaitToStepU3Ed__19_t6B5E4BA1EDFA59BF8F263C2F90F27370696D0719::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7454 = { sizeof (SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7454[6] = 
{
	SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF::get_offset_of_playerDead_4(),
	SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF::get_offset_of_invasion_5(),
	SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF::get_offset_of_enemyDead_6(),
	SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF::get_offset_of_allEnemyDead_7(),
	SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF::get_offset_of_restart_8(),
	SpaceInvaderEvents_t4CBF0345E214F41F72813A9D060AA676A3D9C4EF::get_offset_of_enemyCount_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7455 = { sizeof (AgentInitialiser_t7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7455[1] = 
{
	AgentInitialiser_t7414B077BF7EE0DE3FCEB960B66C9D9B6BA2B8F5::get_offset_of_startNode_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7456 = { sizeof (CameraDisable_tFC35DE8BA18E0E594FC5ECC6DB57CDA0A9C33601), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7457 = { sizeof (Shooter_t66D7662B87125C892730A0E48F2442B684535B8D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7457[3] = 
{
	Shooter_t66D7662B87125C892730A0E48F2442B684535B8D::get_offset_of_SpawnEffect_4(),
	Shooter_t66D7662B87125C892730A0E48F2442B684535B8D::get_offset_of_projectile_5(),
	Shooter_t66D7662B87125C892730A0E48F2442B684535B8D::get_offset_of_HitEffect_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7458 = { sizeof (TakeDamage_tA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7458[1] = 
{
	TakeDamage_tA9DB2BC883BEA79F0134F0E4CD4D5D2062CD304B::get_offset_of_damageValue_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7459 = { sizeof (AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7459[7] = 
{
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_target_4(),
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_attackPfx_5(),
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_attackPoint_6(),
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_damage_7(),
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_attackChargeTime_8(),
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_m_HomeBaseAttackTimer_9(),
	AttackBase_tE6B3C14A63CED270A64B1F39A33DA5CAA0CDB84E::get_offset_of_m_IsChargingHomeBaseAttack_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7460 = { sizeof (BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7460[6] = 
{
	BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF::get_offset_of_attackBase_4(),
	BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF::get_offset_of_attackPfx_5(),
	BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF::get_offset_of_attackPoint_6(),
	BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF::get_offset_of_chargePfx_7(),
	BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF::get_offset_of_target_8(),
	BaseAttackOrder_t664A0042F8A6891E3EEE41F5D2128C59BCC22EDF::get_offset_of_m_CurrentAgentsInside_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7461 = { sizeof (HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8), -1, sizeof(HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7461[1] = 
{
	HomeBase_tDE2A78AE29B63F0C7DE1AD232A95C1DD209AADE8_StaticFields::get_offset_of_U3CInstanceU3Ek__BackingField_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7462 = { sizeof (Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7462[15] = 
{
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_alignmentProvider_4(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_U3CenemyMaskU3Ek__BackingField_5(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_projectilePrefab_6(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_projectile_7(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_hitEffect_8(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_projectilePoints_9(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_projectilePointIndex_10(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_epicenter_11(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_isMultiAttack_12(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_fireRate_13(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_damage_14(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_targetLockOn_15(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_radiusEffectColour_16(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_m_FireTimer_17(),
	Turret_t5BCEE2BDBDB2299F076303C640ED29C5ABAD354E::get_offset_of_m_TrackingEnemy_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7463 = { sizeof (TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7463[9] = 
{
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_baseDead_4(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_enemySpawned_5(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_enemyDead_6(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_allEnemiesDefeated_7(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_restart_8(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_agentDeath_9(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_enemyCount_10(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_enemy_11(),
	TowerDefenseGameEventHandler_t6E627A6336C8FB19DC1C469D1699BBD46D56B7E7::get_offset_of_enemyAlignment_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7464 = { sizeof (MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7464[15] = 
{
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_target_4(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_movementMultiplier_5(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_attackSpeedMultiplier_6(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_distanceToAttack_7(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_distanceToMove_8(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_degreesPerSecond_9(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_beginMoving_10(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_beginAttacking_11(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_animator_12(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_startRotation_13(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_endRotation_14(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_delta_15(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_attackTickDuration_16(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_lastAttackTick_17(),
	MoveTowardsTarget_t086585A65E43D5FFABFBC3D00BE48157070D410E::get_offset_of_OnAttackTick_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7465 = { sizeof (ZombieTest_tC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7465[1] = 
{
	ZombieTest_tC6FB6BAF4A44EE6DEBF064A9B0A542DCF853DBAE::get_offset_of_animator_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7466 = { sizeof (AlphaButtonClickMask_t90B0DF50D542BD644F11259B7CA845036AF5F8F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7466[1] = 
{
	AlphaButtonClickMask_t90B0DF50D542BD644F11259B7CA845036AF5F8F1::get_offset_of__image_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7467 = { sizeof (EventSystemChecker_t3DE6F6D9672E49F03867024BEBA4169B6EFD3DB6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7468 = { sizeof (ForcedReset_t8D0ECEC1BFEDF13E0142F6F50190FC9C36CA6C05), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7469 = { sizeof (UnityDeeplinks_t6797A73F40586A62CEB942309A0F8876516BF912), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7470 = { sizeof (ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7470[5] = 
{
	ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97::get_offset_of_action_4(),
	ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97::get_offset_of_target_5(),
	ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97::get_offset_of_source_6(),
	ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97::get_offset_of_triggerCount_7(),
	ActivateTrigger_tB45971BB1DB93436BA51A0F3BFE948876EE04B97::get_offset_of_repeatTrigger_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7471 = { sizeof (Mode_t936E9A246C14583EB7FEEAD6FE3851D77A219F8A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7471[7] = 
{
	Mode_t936E9A246C14583EB7FEEAD6FE3851D77A219F8A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7472 = { sizeof (AutoMobileShaderSwitch_tA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7472[1] = 
{
	AutoMobileShaderSwitch_tA07C3E544D1D73C145D51F7A6DD6AD83B6BFF0B6::get_offset_of_m_ReplacementList_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7473 = { sizeof (ReplacementDefinition_tB7B8ADC7CD3FFC08A707B5DF69EA91F335897382), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7473[2] = 
{
	ReplacementDefinition_tB7B8ADC7CD3FFC08A707B5DF69EA91F335897382::get_offset_of_original_0(),
	ReplacementDefinition_tB7B8ADC7CD3FFC08A707B5DF69EA91F335897382::get_offset_of_replacement_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7474 = { sizeof (ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7474[1] = 
{
	ReplacementList_tBC76041BA59383247AE6A08635452D166575F7C1::get_offset_of_items_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7475 = { sizeof (AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7475[4] = 
{
	AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27::get_offset_of_moveUnitsPerSecond_4(),
	AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27::get_offset_of_rotateDegreesPerSecond_5(),
	AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27::get_offset_of_ignoreTimescale_6(),
	AutoMoveAndRotate_t92043194FF41B06B9441FB187206098DF3848E27::get_offset_of_m_LastRealTime_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7476 = { sizeof (Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7476[2] = 
{
	Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA::get_offset_of_value_0(),
	Vector3andSpace_t3E61556293DA41E619A7E7B653625FE6696A5FBA::get_offset_of_space_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7477 = { sizeof (CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7477[5] = 
{
	CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209::get_offset_of_Camera_0(),
	CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209::get_offset_of_Lookatpoint_1(),
	CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209::get_offset_of_Parent_2(),
	CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209::get_offset_of_m_OrigCameraPos_3(),
	CameraRefocus_t2B8D4CC6BA8F382E2ECA63EADE3C12D05BD0D209::get_offset_of_m_Refocus_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7478 = { sizeof (CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7478[9] = 
{
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_HorizontalBobRange_0(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_VerticalBobRange_1(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_Bobcurve_2(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_VerticaltoHorizontalRatio_3(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_m_CyclePositionX_4(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_m_CyclePositionY_5(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_m_BobBaseInterval_6(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_m_OriginalCameraPosition_7(),
	CurveControlledBob_t603CBC7D528557A646CE216B5389D65F2265CED1::get_offset_of_m_Time_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7479 = { sizeof (DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7479[7] = 
{
	0,
	0,
	0,
	0,
	0,
	0,
	DragRigidbody_tD01458E8F172F9B508F26B5FF63E1F041AB4AD57::get_offset_of_m_SpringJoint_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7480 = { sizeof (U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7480[7] = 
{
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_U3CU3E1__state_0(),
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_U3CU3E2__current_1(),
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_U3CU3E4__this_2(),
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_distance_3(),
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_U3ColdDragU3E5__2_4(),
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_U3ColdAngularDragU3E5__3_5(),
	U3CDragObjectU3Ed__8_t17549ED9CDB53B36A05BA8B556D996F9FA1C6794::get_offset_of_U3CmainCameraU3E5__4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7481 = { sizeof (DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7481[11] = 
{
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_sunLight_4(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_minHeight_5(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_minShadowDistance_6(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_minShadowBias_7(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_maxHeight_8(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_maxShadowDistance_9(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_maxShadowBias_10(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_adaptTime_11(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_m_SmoothHeight_12(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_m_ChangeSpeed_13(),
	DynamicShadowSettings_t2ABA716F8D04B9938814F6C1788B5CC0E89E5653::get_offset_of_m_OriginalStrength_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7482 = { sizeof (FollowTarget_tCD44FEA739F1617B46941AF638C3E12AA4B87BEC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7482[2] = 
{
	FollowTarget_tCD44FEA739F1617B46941AF638C3E12AA4B87BEC::get_offset_of_target_4(),
	FollowTarget_tCD44FEA739F1617B46941AF638C3E12AA4B87BEC::get_offset_of_offset_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7483 = { sizeof (FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7483[6] = 
{
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2::get_offset_of_Camera_0(),
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2::get_offset_of_originalFov_1(),
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2::get_offset_of_FOVIncrease_2(),
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2::get_offset_of_TimeToIncrease_3(),
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2::get_offset_of_TimeToDecrease_4(),
	FOVKick_t869A40BE9DDDC819536DBE8AD29768A73F55D2D2::get_offset_of_IncreaseCurve_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7484 = { sizeof (U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7484[4] = 
{
	U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3::get_offset_of_U3CU3E1__state_0(),
	U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3::get_offset_of_U3CU3E2__current_1(),
	U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3::get_offset_of_U3CU3E4__this_2(),
	U3CFOVKickUpU3Ed__9_t14B549E842A8A92527B9DEAB213F220F748434F3::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7485 = { sizeof (U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7485[4] = 
{
	U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA::get_offset_of_U3CU3E1__state_0(),
	U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA::get_offset_of_U3CU3E2__current_1(),
	U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA::get_offset_of_U3CU3E4__this_2(),
	U3CFOVKickDownU3Ed__10_t0C4F8545C484C6353CC526ABA9899D58E90914FA::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7486 = { sizeof (FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7486[6] = 
{
	0,
	FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C::get_offset_of_m_FpsAccumulator_5(),
	FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C::get_offset_of_m_FpsNextPeriod_6(),
	FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C::get_offset_of_m_CurrentFps_7(),
	0,
	FPSCounter_t8F5AA66F5FC36D2D1ED6D547485E345DFF602A0C::get_offset_of_m_Text_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7487 = { sizeof (LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7487[3] = 
{
	LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480::get_offset_of_BobDuration_0(),
	LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480::get_offset_of_BobAmount_1(),
	LerpControlledBob_t29D8B1DDB87958C047142D62531E1A4978C3B480::get_offset_of_m_Offset_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7488 = { sizeof (U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7488[4] = 
{
	U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E::get_offset_of_U3CU3E1__state_0(),
	U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E::get_offset_of_U3CU3E2__current_1(),
	U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E::get_offset_of_U3CU3E4__this_2(),
	U3CDoBobCycleU3Ed__4_tC7F8DE382FA82646BFD80ECCEF5BACE2954C543E::get_offset_of_U3CtU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7489 = { sizeof (ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7489[4] = 
{
	ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9::get_offset_of_originalPosition_4(),
	ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9::get_offset_of_originalRotation_5(),
	ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9::get_offset_of_originalStructure_6(),
	ObjectResetter_tF48720C42F9CC7C8D3EBCAE91B158396383C9EB9::get_offset_of_Rigidbody_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7490 = { sizeof (U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7490[4] = 
{
	U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C::get_offset_of_U3CU3E1__state_0(),
	U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C::get_offset_of_U3CU3E2__current_1(),
	U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C::get_offset_of_delay_2(),
	U3CResetCoroutineU3Ed__6_tAE22AB241D2135B160859D9A0520247B5FAD804C::get_offset_of_U3CU3E4__this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7491 = { sizeof (ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7491[4] = 
{
	ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03::get_offset_of_minDuration_4(),
	ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03::get_offset_of_maxDuration_5(),
	ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03::get_offset_of_m_MaxLifetime_6(),
	ParticleSystemDestroyer_t65924F95D50242BD6F3F266CCB0EE436255B1C03::get_offset_of_m_EarlyStop_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7492 = { sizeof (U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7492[5] = 
{
	U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C::get_offset_of_U3CU3E1__state_0(),
	U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C::get_offset_of_U3CU3E2__current_1(),
	U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C::get_offset_of_U3CU3E4__this_2(),
	U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C::get_offset_of_U3CsystemsU3E5__2_3(),
	U3CStartU3Ed__4_t15A83C64D0632A9A329968DD8A2A27D5F0F7514C::get_offset_of_U3CstopTimeU3E5__3_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7493 = { sizeof (PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7493[4] = 
{
	PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8::get_offset_of_m_BuildTargetGroup_4(),
	PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8::get_offset_of_m_Content_5(),
	PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8::get_offset_of_m_MonoBehaviours_6(),
	PlatformSpecificContent_t9346FCE3779FF57A0149C0EDAAD47F6F8DE346E8::get_offset_of_m_ChildrenOfThisObject_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7494 = { sizeof (BuildTargetGroup_t541998A4F75166E0159D30A8963A2EE8C1CDAB96)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7494[3] = 
{
	BuildTargetGroup_t541998A4F75166E0159D30A8963A2EE8C1CDAB96::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7495 = { sizeof (SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7495[3] = 
{
	SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43::get_offset_of_camSwitchButton_4(),
	SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43::get_offset_of_objects_5(),
	SimpleActivatorMenu_t11E88CECDE3EF2CA0A8ECA0DA0FA4293C1E1BC43::get_offset_of_m_CurrentActiveObject_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7496 = { sizeof (SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7496[10] = 
{
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_rotationRange_4(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_rotationSpeed_5(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_dampingTime_6(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_autoZeroVerticalOnMobile_7(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_autoZeroHorizontalOnMobile_8(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_relative_9(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_m_TargetAngles_10(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_m_FollowAngles_11(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_m_FollowVelocity_12(),
	SimpleMouseRotator_t8655C5C968A2F70D71C707DDC4327BF3F34EB32C::get_offset_of_m_OriginalRotation_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7497 = { sizeof (SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7497[5] = 
{
	SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825::get_offset_of_target_4(),
	SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825::get_offset_of_distance_5(),
	SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825::get_offset_of_height_6(),
	SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825::get_offset_of_rotationDamping_7(),
	SmoothFollow_t6CBF60ABA1A66F80E25A9D08A832AEBA9D5DC825::get_offset_of_heightDamping_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7498 = { sizeof (TimedObjectActivator_t775092214A68E514052566C1D7F1089733BADC7B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7498[1] = 
{
	TimedObjectActivator_t775092214A68E514052566C1D7F1089733BADC7B::get_offset_of_entries_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7499 = { sizeof (Action_t555B81FF41842ECAE14AABF88618DE8D3189FFCD)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable7499[6] = 
{
	Action_t555B81FF41842ECAE14AABF88618DE8D3189FFCD::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
