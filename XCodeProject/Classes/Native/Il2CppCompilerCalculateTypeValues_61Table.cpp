﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// RestSharp.HttpResponse
struct HttpResponse_t1C9278F260E9E21515663F30615ADE9CD8B10B4F;
// RestSharp.IAuthenticator
struct IAuthenticator_tEBE9ECFD586CA4D62D1FF630874BAF363A09A4B1;
// RestSharp.IHttpFactory
struct IHttpFactory_t8930B220F3ECE84AC7F5F63DDE06E63A50D4E214;
// RestSharp.IJsonSerializerStrategy
struct IJsonSerializerStrategy_tB46B2F5BCF7A15308077E93B73B49A17657AFF20;
// RestSharp.IRestRequest
struct IRestRequest_tD7503D5F53E7BD19192E528432B70D0095798DEB;
// RestSharp.Parameter
struct Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD;
// RestSharp.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F;
// RestSharp.Reflection.CacheResolver
struct CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21;
// RestSharp.Reflection.GetHandler
struct GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0;
// RestSharp.Reflection.MemberMapLoader
struct MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91;
// RestSharp.Reflection.SafeDictionary`2<System.String,RestSharp.Reflection.CacheResolver/MemberMap>
struct SafeDictionary_2_t3726B0D78DA66F96D92F7C1EA3D8CC95C05F41F2;
// RestSharp.Reflection.SafeDictionary`2<System.Type,RestSharp.Reflection.CacheResolver/CtorDelegate>
struct SafeDictionary_2_tEB3B2DEE429D1D88E37138AD54C8E6ABC42F7ED1;
// RestSharp.Reflection.SafeDictionary`2<System.Type,RestSharp.Reflection.SafeDictionary`2<System.String,RestSharp.Reflection.CacheResolver/MemberMap>>
struct SafeDictionary_2_t67C9F6325E6ACA0BA1808837AC4B1E93BD8074B4;
// RestSharp.Reflection.SetHandler
struct SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8;
// RestSharp.RestClient
struct RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1;
// RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2
struct U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066;
// RestSharp.RestRequestAsyncHandle
struct RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1;
// RestSharp.Serializers.ISerializer
struct ISerializer_tC99AA76717EBAC8C80867F9C57FDE34DB49462E6;
// System.Action
struct Action_t591D2A86165F896B4B800BB5C25CE18672A55579;
// System.Action`1<RestSharp.HttpResponse>
struct Action_1_tD9F6048171550C1555433094A383AF7F8D982157;
// System.Action`1<RestSharp.IRestResponse>
struct Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472;
// System.Action`1<System.IO.Stream>
struct Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621;
// System.Action`1<System.Object>
struct Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0;
// System.Action`2<RestSharp.IRestResponse,RestSharp.RestRequestAsyncHandle>
struct Action_2_t053E5ADD9DC9F2FDA0F021D52561C27B5441D4DC;
// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Boolean[]
struct BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HeaderInfo>
struct Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565;
// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HttpHeaders/HeaderBucket>
struct Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3;
// System.Collections.Generic.Dictionary`2<System.String,System.Object>
struct Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA;
// System.Collections.Generic.IDictionary`2<System.String,RestSharp.Deserializers.IDeserializer>
struct IDictionary_2_t112886A2B2CBDE5CFCFCCFCB1411CDE9882FFD55;
// System.Collections.Generic.IEnumerable`1<System.String>
struct IEnumerable_1_t31EF1520A3A805598500BB6033C14ABDA7116D5E;
// System.Collections.Generic.IList`1<RestSharp.Parameter>
struct IList_1_t8AA549A706E59C70F08B452717CAECCF428161EA;
// System.Collections.Generic.IList`1<System.String>
struct IList_1_tFFB5515FC97391D04D5034ED7F334357FED1FAE6;
// System.Collections.Generic.List`1<RestSharp.FileParameter>
struct List_1_tE557E95A7B0D2C0E2BC087AD8443DB2CD0F13828;
// System.Collections.Generic.List`1<RestSharp.Parameter>
struct List_1_tF576173DE7731AAF41175774D3BC4674C52DEB35;
// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue>
struct List_1_t4C4438F9E20C453876568717EEB3C007B6273D94;
// System.Collections.Generic.List`1<System.Net.Http.Headers.RangeItemHeaderValue>
struct List_1_tBC994597F79973AB8B0C84504B8869AD94636845;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>,System.Boolean>
struct Func_2_t9E72CFF76FE762A39E75734D11FDD5F4BD75DC85;
// System.Func`2<<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>,System.Int32>
struct Func_2_tC8AE27C30FC09CA259587A965D9DE7EC8D8FA835;
// System.Func`2<<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>,System.Reflection.PropertyInfo>
struct Func_2_t57DD463E4FAD3808A6D29BD84041C56CD49888D7;
// System.Func`2<RestSharp.Parameter,RestSharp.HttpCookie>
struct Func_2_tB4FF97CB142B16128A9141A9657DFAFD5FF80C05;
// System.Func`2<RestSharp.Parameter,RestSharp.HttpHeader>
struct Func_2_tE3E70122BCD5867515B9E55A3AE0B39D8029BF8A;
// System.Func`2<RestSharp.Parameter,RestSharp.HttpParameter>
struct Func_2_tBE56C3ADEEA7CFE6F702ACFED4B05D7983C8A47C;
// System.Func`2<RestSharp.Parameter,System.Boolean>
struct Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19;
// System.Func`2<System.Object,System.String>
struct Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF;
// System.Func`2<System.Reflection.PropertyInfo,<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>>
struct Func_2_tE2C5A64B0FEB067AB5C94472646CB768DB7F647F;
// System.Func`2<System.String,System.Boolean>
struct Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0;
// System.Func`4<RestSharp.IHttp,System.Action`1<RestSharp.HttpResponse>,System.String,System.Net.HttpWebRequest>
struct Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22;
// System.Globalization.CultureInfo
struct CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.IO.Stream
struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7;
// System.IO.Stream/ReadWriteTask
struct ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Net.CookieContainer
struct CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73;
// System.Net.Http.Headers.EntityTagHeaderValue
struct EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE;
// System.Net.Http.Headers.HttpContentHeaders
struct HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0;
// System.Net.Http.Headers.HttpHeaders
struct HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1;
// System.Net.Http.Headers.HttpHeaders/HeaderBucket
struct HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F;
// System.Net.Http.Headers.HttpRequestHeaders
struct HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4;
// System.Net.Http.Headers.HttpResponseHeaders
struct HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59;
// System.Net.Http.Headers.ProductHeaderValue
struct ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5;
// System.Net.Http.HttpClient
struct HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7;
// System.Net.Http.HttpClientHandler
struct HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049;
// System.Net.Http.HttpContent
struct HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D;
// System.Net.Http.HttpContent/FixedMemoryStream
struct FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C;
// System.Net.Http.HttpMessageHandler
struct HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894;
// System.Net.Http.HttpMethod
struct HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220;
// System.Net.Http.HttpRequestMessage
struct HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427;
// System.Net.Http.HttpResponseMessage
struct HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904;
// System.Net.HttpWebRequest
struct HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0;
// System.Net.HttpWebResponse
struct HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951;
// System.Net.ICredentials
struct ICredentials_t1A41F1096B037CAB53AE01434DF0747881455344;
// System.Net.IWebProxy
struct IWebProxy_tA24C0862A1ACA35D20FD079E2672CA5786C1A67E;
// System.Predicate`1<System.Net.Http.Headers.TransferCodingHeaderValue>
struct Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9;
// System.Predicate`1<System.String>
struct Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MemberInfo
struct MemberInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.CompilerServices.IAsyncStateMachine
struct IAsyncStateMachine_tEFDFBE18E061A6065AB2FF735F1425FB59F919BC;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.Security.Cryptography.X509Certificates.X509CertificateCollection
struct X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Threading.CancellationCallbackInfo
struct CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36;
// System.Threading.CancellationTokenSource
struct CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE;
// System.Threading.SemaphoreSlim
struct SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048;
// System.Threading.SparselyPopulatedArrayFragment`1<System.Threading.CancellationCallbackInfo>
struct SparselyPopulatedArrayFragment_1_tA54224D01E2FDC03AC2867CDDC8C53E17FA933D7;
// System.Threading.SynchronizationContext
struct SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7;
// System.Threading.Tasks.Task
struct Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2;
// System.Threading.Tasks.Task`1<System.IO.Stream>
struct Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01;
// System.Threading.Tasks.Task`1<System.Int32>
struct Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87;
// System.Threading.Tasks.Task`1<System.Net.Http.HttpResponseMessage>
struct Task_1_t519CB3A303B53176BD616AC273201D66B84FF270;
// System.Threading.Tasks.Task`1<System.Net.WebResponse>
struct Task_1_t59405D5FAC70EF0481DA7783410224C07E3C9C74;
// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult>
struct Task_1_t1359D75350E9D976BFA28AD96E417450DE277673;
// System.Type
struct Type_t;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Version
struct Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef U3CMODULEU3E_T62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6_H
#define U3CMODULEU3E_T62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct  U3CModuleU3E_t62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6 
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CMODULEU3E_T62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6_H
#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef JSONOBJECT_T0589456884908E3386A5EF7E930AFCE86C13EE6C_H
#define JSONOBJECT_T0589456884908E3386A5EF7E930AFCE86C13EE6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.JsonObject
struct  JsonObject_t0589456884908E3386A5EF7E930AFCE86C13EE6C  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Object> RestSharp.JsonObject::_members
	Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * ____members_0;

public:
	inline static int32_t get_offset_of__members_0() { return static_cast<int32_t>(offsetof(JsonObject_t0589456884908E3386A5EF7E930AFCE86C13EE6C, ____members_0)); }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * get__members_0() const { return ____members_0; }
	inline Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA ** get_address_of__members_0() { return &____members_0; }
	inline void set__members_0(Dictionary_2_t9140A71329927AE4FD0F3CF4D4D66668EBE151EA * value)
	{
		____members_0 = value;
		Il2CppCodeGenWriteBarrier((&____members_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONOBJECT_T0589456884908E3386A5EF7E930AFCE86C13EE6C_H
#ifndef POCOJSONSERIALIZERSTRATEGY_TC7A471C0000802F19DE9EF6B444591B71E8B598F_H
#define POCOJSONSERIALIZERSTRATEGY_TC7A471C0000802F19DE9EF6B444591B71E8B598F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.PocoJsonSerializerStrategy
struct  PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F  : public RuntimeObject
{
public:
	// RestSharp.Reflection.CacheResolver RestSharp.PocoJsonSerializerStrategy::CacheResolver
	CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21 * ___CacheResolver_0;

public:
	inline static int32_t get_offset_of_CacheResolver_0() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F, ___CacheResolver_0)); }
	inline CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21 * get_CacheResolver_0() const { return ___CacheResolver_0; }
	inline CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21 ** get_address_of_CacheResolver_0() { return &___CacheResolver_0; }
	inline void set_CacheResolver_0(CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21 * value)
	{
		___CacheResolver_0 = value;
		Il2CppCodeGenWriteBarrier((&___CacheResolver_0), value);
	}
};

struct PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F_StaticFields
{
public:
	// System.String[] RestSharp.PocoJsonSerializerStrategy::Iso8601Format
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___Iso8601Format_1;

public:
	inline static int32_t get_offset_of_Iso8601Format_1() { return static_cast<int32_t>(offsetof(PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F_StaticFields, ___Iso8601Format_1)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_Iso8601Format_1() const { return ___Iso8601Format_1; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_Iso8601Format_1() { return &___Iso8601Format_1; }
	inline void set_Iso8601Format_1(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___Iso8601Format_1 = value;
		Il2CppCodeGenWriteBarrier((&___Iso8601Format_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POCOJSONSERIALIZERSTRATEGY_TC7A471C0000802F19DE9EF6B444591B71E8B598F_H
#ifndef CACHERESOLVER_T755F33B018423E7099C08A0C88F7C43CE1457E21_H
#define CACHERESOLVER_T755F33B018423E7099C08A0C88F7C43CE1457E21_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver
struct  CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21  : public RuntimeObject
{
public:
	// RestSharp.Reflection.MemberMapLoader RestSharp.Reflection.CacheResolver::_memberMapLoader
	MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91 * ____memberMapLoader_0;
	// RestSharp.Reflection.SafeDictionary`2<System.Type,RestSharp.Reflection.SafeDictionary`2<System.String,RestSharp.Reflection.CacheResolver/MemberMap>> RestSharp.Reflection.CacheResolver::_memberMapsCache
	SafeDictionary_2_t67C9F6325E6ACA0BA1808837AC4B1E93BD8074B4 * ____memberMapsCache_1;

public:
	inline static int32_t get_offset_of__memberMapLoader_0() { return static_cast<int32_t>(offsetof(CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21, ____memberMapLoader_0)); }
	inline MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91 * get__memberMapLoader_0() const { return ____memberMapLoader_0; }
	inline MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91 ** get_address_of__memberMapLoader_0() { return &____memberMapLoader_0; }
	inline void set__memberMapLoader_0(MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91 * value)
	{
		____memberMapLoader_0 = value;
		Il2CppCodeGenWriteBarrier((&____memberMapLoader_0), value);
	}

	inline static int32_t get_offset_of__memberMapsCache_1() { return static_cast<int32_t>(offsetof(CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21, ____memberMapsCache_1)); }
	inline SafeDictionary_2_t67C9F6325E6ACA0BA1808837AC4B1E93BD8074B4 * get__memberMapsCache_1() const { return ____memberMapsCache_1; }
	inline SafeDictionary_2_t67C9F6325E6ACA0BA1808837AC4B1E93BD8074B4 ** get_address_of__memberMapsCache_1() { return &____memberMapsCache_1; }
	inline void set__memberMapsCache_1(SafeDictionary_2_t67C9F6325E6ACA0BA1808837AC4B1E93BD8074B4 * value)
	{
		____memberMapsCache_1 = value;
		Il2CppCodeGenWriteBarrier((&____memberMapsCache_1), value);
	}
};

struct CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21_StaticFields
{
public:
	// RestSharp.Reflection.SafeDictionary`2<System.Type,RestSharp.Reflection.CacheResolver/CtorDelegate> RestSharp.Reflection.CacheResolver::ConstructorCache
	SafeDictionary_2_tEB3B2DEE429D1D88E37138AD54C8E6ABC42F7ED1 * ___ConstructorCache_2;

public:
	inline static int32_t get_offset_of_ConstructorCache_2() { return static_cast<int32_t>(offsetof(CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21_StaticFields, ___ConstructorCache_2)); }
	inline SafeDictionary_2_tEB3B2DEE429D1D88E37138AD54C8E6ABC42F7ED1 * get_ConstructorCache_2() const { return ___ConstructorCache_2; }
	inline SafeDictionary_2_tEB3B2DEE429D1D88E37138AD54C8E6ABC42F7ED1 ** get_address_of_ConstructorCache_2() { return &___ConstructorCache_2; }
	inline void set_ConstructorCache_2(SafeDictionary_2_tEB3B2DEE429D1D88E37138AD54C8E6ABC42F7ED1 * value)
	{
		___ConstructorCache_2 = value;
		Il2CppCodeGenWriteBarrier((&___ConstructorCache_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHERESOLVER_T755F33B018423E7099C08A0C88F7C43CE1457E21_H
#ifndef U3CCREATEGETHANDLERU3EC__ANONSTOREY1_TE9CC470A6BAC1499033EC744F320F1C38B0D8F87_H
#define U3CCREATEGETHANDLERU3EC__ANONSTOREY1_TE9CC470A6BAC1499033EC744F320F1C38B0D8F87_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver/<CreateGetHandler>c__AnonStorey1
struct  U3CCreateGetHandlerU3Ec__AnonStorey1_tE9CC470A6BAC1499033EC744F320F1C38B0D8F87  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo RestSharp.Reflection.CacheResolver/<CreateGetHandler>c__AnonStorey1::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CCreateGetHandlerU3Ec__AnonStorey1_tE9CC470A6BAC1499033EC744F320F1C38B0D8F87, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGETHANDLERU3EC__ANONSTOREY1_TE9CC470A6BAC1499033EC744F320F1C38B0D8F87_H
#ifndef U3CCREATEGETHANDLERU3EC__ANONSTOREY3_T060E5A44382EF6E752BA67CBF143EEACCF6E16E0_H
#define U3CCREATEGETHANDLERU3EC__ANONSTOREY3_T060E5A44382EF6E752BA67CBF143EEACCF6E16E0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver/<CreateGetHandler>c__AnonStorey3
struct  U3CCreateGetHandlerU3Ec__AnonStorey3_t060E5A44382EF6E752BA67CBF143EEACCF6E16E0  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo RestSharp.Reflection.CacheResolver/<CreateGetHandler>c__AnonStorey3::getMethodInfo
	MethodInfo_t * ___getMethodInfo_0;

public:
	inline static int32_t get_offset_of_getMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CCreateGetHandlerU3Ec__AnonStorey3_t060E5A44382EF6E752BA67CBF143EEACCF6E16E0, ___getMethodInfo_0)); }
	inline MethodInfo_t * get_getMethodInfo_0() const { return ___getMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_getMethodInfo_0() { return &___getMethodInfo_0; }
	inline void set_getMethodInfo_0(MethodInfo_t * value)
	{
		___getMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___getMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATEGETHANDLERU3EC__ANONSTOREY3_T060E5A44382EF6E752BA67CBF143EEACCF6E16E0_H
#ifndef U3CCREATESETHANDLERU3EC__ANONSTOREY2_T560CD52CBB219DBA1BC7A048F89883B91FD6E10F_H
#define U3CCREATESETHANDLERU3EC__ANONSTOREY2_T560CD52CBB219DBA1BC7A048F89883B91FD6E10F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver/<CreateSetHandler>c__AnonStorey2
struct  U3CCreateSetHandlerU3Ec__AnonStorey2_t560CD52CBB219DBA1BC7A048F89883B91FD6E10F  : public RuntimeObject
{
public:
	// System.Reflection.FieldInfo RestSharp.Reflection.CacheResolver/<CreateSetHandler>c__AnonStorey2::fieldInfo
	FieldInfo_t * ___fieldInfo_0;

public:
	inline static int32_t get_offset_of_fieldInfo_0() { return static_cast<int32_t>(offsetof(U3CCreateSetHandlerU3Ec__AnonStorey2_t560CD52CBB219DBA1BC7A048F89883B91FD6E10F, ___fieldInfo_0)); }
	inline FieldInfo_t * get_fieldInfo_0() const { return ___fieldInfo_0; }
	inline FieldInfo_t ** get_address_of_fieldInfo_0() { return &___fieldInfo_0; }
	inline void set_fieldInfo_0(FieldInfo_t * value)
	{
		___fieldInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___fieldInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATESETHANDLERU3EC__ANONSTOREY2_T560CD52CBB219DBA1BC7A048F89883B91FD6E10F_H
#ifndef U3CCREATESETHANDLERU3EC__ANONSTOREY4_T7F9F4F5A9E16563FA111643F344C7692D8F5D8CA_H
#define U3CCREATESETHANDLERU3EC__ANONSTOREY4_T7F9F4F5A9E16563FA111643F344C7692D8F5D8CA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver/<CreateSetHandler>c__AnonStorey4
struct  U3CCreateSetHandlerU3Ec__AnonStorey4_t7F9F4F5A9E16563FA111643F344C7692D8F5D8CA  : public RuntimeObject
{
public:
	// System.Reflection.MethodInfo RestSharp.Reflection.CacheResolver/<CreateSetHandler>c__AnonStorey4::setMethodInfo
	MethodInfo_t * ___setMethodInfo_0;

public:
	inline static int32_t get_offset_of_setMethodInfo_0() { return static_cast<int32_t>(offsetof(U3CCreateSetHandlerU3Ec__AnonStorey4_t7F9F4F5A9E16563FA111643F344C7692D8F5D8CA, ___setMethodInfo_0)); }
	inline MethodInfo_t * get_setMethodInfo_0() const { return ___setMethodInfo_0; }
	inline MethodInfo_t ** get_address_of_setMethodInfo_0() { return &___setMethodInfo_0; }
	inline void set_setMethodInfo_0(MethodInfo_t * value)
	{
		___setMethodInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___setMethodInfo_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCREATESETHANDLERU3EC__ANONSTOREY4_T7F9F4F5A9E16563FA111643F344C7692D8F5D8CA_H
#ifndef MEMBERMAP_TFBE96498C6D20B6DF316D3933CD05F540C1DAE18_H
#define MEMBERMAP_TFBE96498C6D20B6DF316D3933CD05F540C1DAE18_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver/MemberMap
struct  MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18  : public RuntimeObject
{
public:
	// System.Reflection.MemberInfo RestSharp.Reflection.CacheResolver/MemberMap::MemberInfo
	MemberInfo_t * ___MemberInfo_0;
	// System.Type RestSharp.Reflection.CacheResolver/MemberMap::Type
	Type_t * ___Type_1;
	// RestSharp.Reflection.GetHandler RestSharp.Reflection.CacheResolver/MemberMap::Getter
	GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0 * ___Getter_2;
	// RestSharp.Reflection.SetHandler RestSharp.Reflection.CacheResolver/MemberMap::Setter
	SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8 * ___Setter_3;

public:
	inline static int32_t get_offset_of_MemberInfo_0() { return static_cast<int32_t>(offsetof(MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18, ___MemberInfo_0)); }
	inline MemberInfo_t * get_MemberInfo_0() const { return ___MemberInfo_0; }
	inline MemberInfo_t ** get_address_of_MemberInfo_0() { return &___MemberInfo_0; }
	inline void set_MemberInfo_0(MemberInfo_t * value)
	{
		___MemberInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___MemberInfo_0), value);
	}

	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18, ___Type_1)); }
	inline Type_t * get_Type_1() const { return ___Type_1; }
	inline Type_t ** get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(Type_t * value)
	{
		___Type_1 = value;
		Il2CppCodeGenWriteBarrier((&___Type_1), value);
	}

	inline static int32_t get_offset_of_Getter_2() { return static_cast<int32_t>(offsetof(MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18, ___Getter_2)); }
	inline GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0 * get_Getter_2() const { return ___Getter_2; }
	inline GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0 ** get_address_of_Getter_2() { return &___Getter_2; }
	inline void set_Getter_2(GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0 * value)
	{
		___Getter_2 = value;
		Il2CppCodeGenWriteBarrier((&___Getter_2), value);
	}

	inline static int32_t get_offset_of_Setter_3() { return static_cast<int32_t>(offsetof(MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18, ___Setter_3)); }
	inline SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8 * get_Setter_3() const { return ___Setter_3; }
	inline SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8 ** get_address_of_Setter_3() { return &___Setter_3; }
	inline void set_Setter_3(SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8 * value)
	{
		___Setter_3 = value;
		Il2CppCodeGenWriteBarrier((&___Setter_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERMAP_TFBE96498C6D20B6DF316D3933CD05F540C1DAE18_H
#ifndef U3CCONFIGUREHTTPU3EC__ANONSTOREY0_T4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27_H
#define U3CCONFIGUREHTTPU3EC__ANONSTOREY0_T4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestClient/<ConfigureHttp>c__AnonStorey0
struct  U3CConfigureHttpU3Ec__AnonStorey0_t4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27  : public RuntimeObject
{
public:
	// RestSharp.Parameter RestSharp.RestClient/<ConfigureHttp>c__AnonStorey0::p
	Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD * ___p_0;

public:
	inline static int32_t get_offset_of_p_0() { return static_cast<int32_t>(offsetof(U3CConfigureHttpU3Ec__AnonStorey0_t4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27, ___p_0)); }
	inline Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD * get_p_0() const { return ___p_0; }
	inline Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD ** get_address_of_p_0() { return &___p_0; }
	inline void set_p_0(Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD * value)
	{
		___p_0 = value;
		Il2CppCodeGenWriteBarrier((&___p_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CCONFIGUREHTTPU3EC__ANONSTOREY0_T4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27_H
#ifndef U3CEXECUTEASYNCU3EC__ANONSTOREY1_T478D7C99B107EFE165B42EDF9163DD211FFC01DE_H
#define U3CEXECUTEASYNCU3EC__ANONSTOREY1_T478D7C99B107EFE165B42EDF9163DD211FFC01DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestClient/<ExecuteAsync>c__AnonStorey1
struct  U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE  : public RuntimeObject
{
public:
	// RestSharp.IRestRequest RestSharp.RestClient/<ExecuteAsync>c__AnonStorey1::request
	RuntimeObject* ___request_0;
	// RestSharp.RestRequestAsyncHandle RestSharp.RestClient/<ExecuteAsync>c__AnonStorey1::asyncHandle
	RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1 * ___asyncHandle_1;
	// System.Action`2<RestSharp.IRestResponse,RestSharp.RestRequestAsyncHandle> RestSharp.RestClient/<ExecuteAsync>c__AnonStorey1::callback
	Action_2_t053E5ADD9DC9F2FDA0F021D52561C27B5441D4DC * ___callback_2;
	// RestSharp.RestClient RestSharp.RestClient/<ExecuteAsync>c__AnonStorey1::$this
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1 * ___U24this_3;

public:
	inline static int32_t get_offset_of_request_0() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE, ___request_0)); }
	inline RuntimeObject* get_request_0() const { return ___request_0; }
	inline RuntimeObject** get_address_of_request_0() { return &___request_0; }
	inline void set_request_0(RuntimeObject* value)
	{
		___request_0 = value;
		Il2CppCodeGenWriteBarrier((&___request_0), value);
	}

	inline static int32_t get_offset_of_asyncHandle_1() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE, ___asyncHandle_1)); }
	inline RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1 * get_asyncHandle_1() const { return ___asyncHandle_1; }
	inline RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1 ** get_address_of_asyncHandle_1() { return &___asyncHandle_1; }
	inline void set_asyncHandle_1(RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1 * value)
	{
		___asyncHandle_1 = value;
		Il2CppCodeGenWriteBarrier((&___asyncHandle_1), value);
	}

	inline static int32_t get_offset_of_callback_2() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE, ___callback_2)); }
	inline Action_2_t053E5ADD9DC9F2FDA0F021D52561C27B5441D4DC * get_callback_2() const { return ___callback_2; }
	inline Action_2_t053E5ADD9DC9F2FDA0F021D52561C27B5441D4DC ** get_address_of_callback_2() { return &___callback_2; }
	inline void set_callback_2(Action_2_t053E5ADD9DC9F2FDA0F021D52561C27B5441D4DC * value)
	{
		___callback_2 = value;
		Il2CppCodeGenWriteBarrier((&___callback_2), value);
	}

	inline static int32_t get_offset_of_U24this_3() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE, ___U24this_3)); }
	inline RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1 * get_U24this_3() const { return ___U24this_3; }
	inline RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1 ** get_address_of_U24this_3() { return &___U24this_3; }
	inline void set_U24this_3(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1 * value)
	{
		___U24this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U24this_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEASYNCU3EC__ANONSTOREY1_T478D7C99B107EFE165B42EDF9163DD211FFC01DE_H
#ifndef U3CEXECUTEASYNCU3EC__ANONSTOREY2_T66B943A70AF37AAAF83BD3128510031DC1BE7066_H
#define U3CEXECUTEASYNCU3EC__ANONSTOREY2_T66B943A70AF37AAAF83BD3128510031DC1BE7066_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2
struct  U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066  : public RuntimeObject
{
public:
	// System.Threading.SynchronizationContext RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2::ctx
	SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * ___ctx_0;
	// System.Action`1<RestSharp.HttpResponse> RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2::cb
	Action_1_tD9F6048171550C1555433094A383AF7F8D982157 * ___cb_1;

public:
	inline static int32_t get_offset_of_ctx_0() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066, ___ctx_0)); }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * get_ctx_0() const { return ___ctx_0; }
	inline SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 ** get_address_of_ctx_0() { return &___ctx_0; }
	inline void set_ctx_0(SynchronizationContext_t06AEFE2C7CFCFC242D0A5729A74464AF18CF84E7 * value)
	{
		___ctx_0 = value;
		Il2CppCodeGenWriteBarrier((&___ctx_0), value);
	}

	inline static int32_t get_offset_of_cb_1() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066, ___cb_1)); }
	inline Action_1_tD9F6048171550C1555433094A383AF7F8D982157 * get_cb_1() const { return ___cb_1; }
	inline Action_1_tD9F6048171550C1555433094A383AF7F8D982157 ** get_address_of_cb_1() { return &___cb_1; }
	inline void set_cb_1(Action_1_tD9F6048171550C1555433094A383AF7F8D982157 * value)
	{
		___cb_1 = value;
		Il2CppCodeGenWriteBarrier((&___cb_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEASYNCU3EC__ANONSTOREY2_T66B943A70AF37AAAF83BD3128510031DC1BE7066_H
#ifndef U3CEXECUTEASYNCU3EC__ANONSTOREY3_T6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94_H
#define U3CEXECUTEASYNCU3EC__ANONSTOREY3_T6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2/<ExecuteAsync>c__AnonStorey3
struct  U3CExecuteAsyncU3Ec__AnonStorey3_t6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94  : public RuntimeObject
{
public:
	// RestSharp.HttpResponse RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2/<ExecuteAsync>c__AnonStorey3::resp
	HttpResponse_t1C9278F260E9E21515663F30615ADE9CD8B10B4F * ___resp_0;
	// RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2 RestSharp.RestClient/<ExecuteAsync>c__AnonStorey2/<ExecuteAsync>c__AnonStorey3::<>f__ref$2
	U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066 * ___U3CU3Ef__refU242_1;

public:
	inline static int32_t get_offset_of_resp_0() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey3_t6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94, ___resp_0)); }
	inline HttpResponse_t1C9278F260E9E21515663F30615ADE9CD8B10B4F * get_resp_0() const { return ___resp_0; }
	inline HttpResponse_t1C9278F260E9E21515663F30615ADE9CD8B10B4F ** get_address_of_resp_0() { return &___resp_0; }
	inline void set_resp_0(HttpResponse_t1C9278F260E9E21515663F30615ADE9CD8B10B4F * value)
	{
		___resp_0 = value;
		Il2CppCodeGenWriteBarrier((&___resp_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__refU242_1() { return static_cast<int32_t>(offsetof(U3CExecuteAsyncU3Ec__AnonStorey3_t6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94, ___U3CU3Ef__refU242_1)); }
	inline U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066 * get_U3CU3Ef__refU242_1() const { return ___U3CU3Ef__refU242_1; }
	inline U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066 ** get_address_of_U3CU3Ef__refU242_1() { return &___U3CU3Ef__refU242_1; }
	inline void set_U3CU3Ef__refU242_1(U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066 * value)
	{
		___U3CU3Ef__refU242_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__refU242_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CEXECUTEASYNCU3EC__ANONSTOREY3_T6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94_H
#ifndef RESTREQUESTASYNCHANDLE_T502D2598B36C99705E6214E833A4E1C60A0A24E1_H
#define RESTREQUESTASYNCHANDLE_T502D2598B36C99705E6214E833A4E1C60A0A24E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestRequestAsyncHandle
struct  RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1  : public RuntimeObject
{
public:
	// System.Net.HttpWebRequest RestSharp.RestRequestAsyncHandle::WebRequest
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___WebRequest_0;

public:
	inline static int32_t get_offset_of_WebRequest_0() { return static_cast<int32_t>(offsetof(RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1, ___WebRequest_0)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_WebRequest_0() const { return ___WebRequest_0; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_WebRequest_0() { return &___WebRequest_0; }
	inline void set_WebRequest_0(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___WebRequest_0 = value;
		Il2CppCodeGenWriteBarrier((&___WebRequest_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTREQUESTASYNCHANDLE_T502D2598B36C99705E6214E833A4E1C60A0A24E1_H
#ifndef JSONSERIALIZER_T4D0574E68181735C10A438200A18C404BEF321B9_H
#define JSONSERIALIZER_T4D0574E68181735C10A438200A18C404BEF321B9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Serializers.JsonSerializer
struct  JsonSerializer_t4D0574E68181735C10A438200A18C404BEF321B9  : public RuntimeObject
{
public:
	// System.String RestSharp.Serializers.JsonSerializer::<Namespace>k__BackingField
	String_t* ___U3CNamespaceU3Ek__BackingField_0;
	// System.String RestSharp.Serializers.JsonSerializer::<ContentType>k__BackingField
	String_t* ___U3CContentTypeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(JsonSerializer_t4D0574E68181735C10A438200A18C404BEF321B9, ___U3CNamespaceU3Ek__BackingField_0)); }
	inline String_t* get_U3CNamespaceU3Ek__BackingField_0() const { return ___U3CNamespaceU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNamespaceU3Ek__BackingField_0() { return &___U3CNamespaceU3Ek__BackingField_0; }
	inline void set_U3CNamespaceU3Ek__BackingField_0(String_t* value)
	{
		___U3CNamespaceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamespaceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CContentTypeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(JsonSerializer_t4D0574E68181735C10A438200A18C404BEF321B9, ___U3CContentTypeU3Ek__BackingField_1)); }
	inline String_t* get_U3CContentTypeU3Ek__BackingField_1() const { return ___U3CContentTypeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CContentTypeU3Ek__BackingField_1() { return &___U3CContentTypeU3Ek__BackingField_1; }
	inline void set_U3CContentTypeU3Ek__BackingField_1(String_t* value)
	{
		___U3CContentTypeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentTypeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JSONSERIALIZER_T4D0574E68181735C10A438200A18C404BEF321B9_H
#ifndef XMLSERIALIZER_TB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_H
#define XMLSERIALIZER_TB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Serializers.XmlSerializer
struct  XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC  : public RuntimeObject
{
public:
	// System.String RestSharp.Serializers.XmlSerializer::<RootElement>k__BackingField
	String_t* ___U3CRootElementU3Ek__BackingField_0;
	// System.String RestSharp.Serializers.XmlSerializer::<Namespace>k__BackingField
	String_t* ___U3CNamespaceU3Ek__BackingField_1;
	// System.String RestSharp.Serializers.XmlSerializer::<DateFormat>k__BackingField
	String_t* ___U3CDateFormatU3Ek__BackingField_2;
	// System.String RestSharp.Serializers.XmlSerializer::<ContentType>k__BackingField
	String_t* ___U3CContentTypeU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CRootElementU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC, ___U3CRootElementU3Ek__BackingField_0)); }
	inline String_t* get_U3CRootElementU3Ek__BackingField_0() const { return ___U3CRootElementU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CRootElementU3Ek__BackingField_0() { return &___U3CRootElementU3Ek__BackingField_0; }
	inline void set_U3CRootElementU3Ek__BackingField_0(String_t* value)
	{
		___U3CRootElementU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRootElementU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CNamespaceU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC, ___U3CNamespaceU3Ek__BackingField_1)); }
	inline String_t* get_U3CNamespaceU3Ek__BackingField_1() const { return ___U3CNamespaceU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNamespaceU3Ek__BackingField_1() { return &___U3CNamespaceU3Ek__BackingField_1; }
	inline void set_U3CNamespaceU3Ek__BackingField_1(String_t* value)
	{
		___U3CNamespaceU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNamespaceU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDateFormatU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC, ___U3CDateFormatU3Ek__BackingField_2)); }
	inline String_t* get_U3CDateFormatU3Ek__BackingField_2() const { return ___U3CDateFormatU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CDateFormatU3Ek__BackingField_2() { return &___U3CDateFormatU3Ek__BackingField_2; }
	inline void set_U3CDateFormatU3Ek__BackingField_2(String_t* value)
	{
		___U3CDateFormatU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDateFormatU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CContentTypeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC, ___U3CContentTypeU3Ek__BackingField_3)); }
	inline String_t* get_U3CContentTypeU3Ek__BackingField_3() const { return ___U3CContentTypeU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CContentTypeU3Ek__BackingField_3() { return &___U3CContentTypeU3Ek__BackingField_3; }
	inline void set_U3CContentTypeU3Ek__BackingField_3(String_t* value)
	{
		___U3CContentTypeU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentTypeU3Ek__BackingField_3), value);
	}
};

struct XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields
{
public:
	// System.Func`2<System.Reflection.PropertyInfo,<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>> RestSharp.Serializers.XmlSerializer::<>f__am$cache0
	Func_2_tE2C5A64B0FEB067AB5C94472646CB768DB7F647F * ___U3CU3Ef__amU24cache0_4;
	// System.Func`2<<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>,System.Boolean> RestSharp.Serializers.XmlSerializer::<>f__am$cache1
	Func_2_t9E72CFF76FE762A39E75734D11FDD5F4BD75DC85 * ___U3CU3Ef__amU24cache1_5;
	// System.Func`2<<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>,System.Int32> RestSharp.Serializers.XmlSerializer::<>f__am$cache2
	Func_2_tC8AE27C30FC09CA259587A965D9DE7EC8D8FA835 * ___U3CU3Ef__amU24cache2_6;
	// System.Func`2<<>__AnonType0`2<System.Reflection.PropertyInfo,RestSharp.Serializers.SerializeAsAttribute>,System.Reflection.PropertyInfo> RestSharp.Serializers.XmlSerializer::<>f__am$cache3
	Func_2_t57DD463E4FAD3808A6D29BD84041C56CD49888D7 * ___U3CU3Ef__amU24cache3_7;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_4() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields, ___U3CU3Ef__amU24cache0_4)); }
	inline Func_2_tE2C5A64B0FEB067AB5C94472646CB768DB7F647F * get_U3CU3Ef__amU24cache0_4() const { return ___U3CU3Ef__amU24cache0_4; }
	inline Func_2_tE2C5A64B0FEB067AB5C94472646CB768DB7F647F ** get_address_of_U3CU3Ef__amU24cache0_4() { return &___U3CU3Ef__amU24cache0_4; }
	inline void set_U3CU3Ef__amU24cache0_4(Func_2_tE2C5A64B0FEB067AB5C94472646CB768DB7F647F * value)
	{
		___U3CU3Ef__amU24cache0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_4), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_5() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields, ___U3CU3Ef__amU24cache1_5)); }
	inline Func_2_t9E72CFF76FE762A39E75734D11FDD5F4BD75DC85 * get_U3CU3Ef__amU24cache1_5() const { return ___U3CU3Ef__amU24cache1_5; }
	inline Func_2_t9E72CFF76FE762A39E75734D11FDD5F4BD75DC85 ** get_address_of_U3CU3Ef__amU24cache1_5() { return &___U3CU3Ef__amU24cache1_5; }
	inline void set_U3CU3Ef__amU24cache1_5(Func_2_t9E72CFF76FE762A39E75734D11FDD5F4BD75DC85 * value)
	{
		___U3CU3Ef__amU24cache1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_5), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_6() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields, ___U3CU3Ef__amU24cache2_6)); }
	inline Func_2_tC8AE27C30FC09CA259587A965D9DE7EC8D8FA835 * get_U3CU3Ef__amU24cache2_6() const { return ___U3CU3Ef__amU24cache2_6; }
	inline Func_2_tC8AE27C30FC09CA259587A965D9DE7EC8D8FA835 ** get_address_of_U3CU3Ef__amU24cache2_6() { return &___U3CU3Ef__amU24cache2_6; }
	inline void set_U3CU3Ef__amU24cache2_6(Func_2_tC8AE27C30FC09CA259587A965D9DE7EC8D8FA835 * value)
	{
		___U3CU3Ef__amU24cache2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_6), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_7() { return static_cast<int32_t>(offsetof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields, ___U3CU3Ef__amU24cache3_7)); }
	inline Func_2_t57DD463E4FAD3808A6D29BD84041C56CD49888D7 * get_U3CU3Ef__amU24cache3_7() const { return ___U3CU3Ef__amU24cache3_7; }
	inline Func_2_t57DD463E4FAD3808A6D29BD84041C56CD49888D7 ** get_address_of_U3CU3Ef__amU24cache3_7() { return &___U3CU3Ef__amU24cache3_7; }
	inline void set_U3CU3Ef__amU24cache3_7(Func_2_t57DD463E4FAD3808A6D29BD84041C56CD49888D7 * value)
	{
		___U3CU3Ef__amU24cache3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSERIALIZER_TB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_H
#ifndef SIMPLEJSON_TC36B96B6997F45DC2BF553850B958630143C62C0_H
#define SIMPLEJSON_TC36B96B6997F45DC2BF553850B958630143C62C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.SimpleJson
struct  SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0  : public RuntimeObject
{
public:

public:
};

struct SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0_StaticFields
{
public:
	// RestSharp.IJsonSerializerStrategy RestSharp.SimpleJson::currentJsonSerializerStrategy
	RuntimeObject* ___currentJsonSerializerStrategy_0;
	// RestSharp.PocoJsonSerializerStrategy RestSharp.SimpleJson::pocoJsonSerializerStrategy
	PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F * ___pocoJsonSerializerStrategy_1;

public:
	inline static int32_t get_offset_of_currentJsonSerializerStrategy_0() { return static_cast<int32_t>(offsetof(SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0_StaticFields, ___currentJsonSerializerStrategy_0)); }
	inline RuntimeObject* get_currentJsonSerializerStrategy_0() const { return ___currentJsonSerializerStrategy_0; }
	inline RuntimeObject** get_address_of_currentJsonSerializerStrategy_0() { return &___currentJsonSerializerStrategy_0; }
	inline void set_currentJsonSerializerStrategy_0(RuntimeObject* value)
	{
		___currentJsonSerializerStrategy_0 = value;
		Il2CppCodeGenWriteBarrier((&___currentJsonSerializerStrategy_0), value);
	}

	inline static int32_t get_offset_of_pocoJsonSerializerStrategy_1() { return static_cast<int32_t>(offsetof(SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0_StaticFields, ___pocoJsonSerializerStrategy_1)); }
	inline PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F * get_pocoJsonSerializerStrategy_1() const { return ___pocoJsonSerializerStrategy_1; }
	inline PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F ** get_address_of_pocoJsonSerializerStrategy_1() { return &___pocoJsonSerializerStrategy_1; }
	inline void set_pocoJsonSerializerStrategy_1(PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F * value)
	{
		___pocoJsonSerializerStrategy_1 = value;
		Il2CppCodeGenWriteBarrier((&___pocoJsonSerializerStrategy_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SIMPLEJSON_TC36B96B6997F45DC2BF553850B958630143C62C0_H
#ifndef ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#define ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Attribute
struct  Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTE_TF048C13FB3C8CFCC53F82290E4A3F621089F9A74_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#define MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MarshalByRefObject
struct  MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF  : public RuntimeObject
{
public:
	// System.Object System.MarshalByRefObject::_identity
	RuntimeObject * ____identity_0;

public:
	inline static int32_t get_offset_of__identity_0() { return static_cast<int32_t>(offsetof(MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF, ____identity_0)); }
	inline RuntimeObject * get__identity_0() const { return ____identity_0; }
	inline RuntimeObject ** get_address_of__identity_0() { return &____identity_0; }
	inline void set__identity_0(RuntimeObject * value)
	{
		____identity_0 = value;
		Il2CppCodeGenWriteBarrier((&____identity_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_pinvoke
{
	Il2CppIUnknown* ____identity_0;
};
// Native definition for COM marshalling of System.MarshalByRefObject
struct MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF_marshaled_com
{
	Il2CppIUnknown* ____identity_0;
};
#endif // MARSHALBYREFOBJECT_TC4577953D0A44D0AB8597CFA868E01C858B1C9AF_H
#ifndef AUTHENTICATIONHEADERVALUE_TC285A16C25538CA86FA59B88CC25E6999BB5F1A6_H
#define AUTHENTICATIONHEADERVALUE_TC285A16C25538CA86FA59B88CC25E6999BB5F1A6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.AuthenticationHeaderValue
struct  AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.AuthenticationHeaderValue::<Parameter>k__BackingField
	String_t* ___U3CParameterU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.AuthenticationHeaderValue::<Scheme>k__BackingField
	String_t* ___U3CSchemeU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CParameterU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6, ___U3CParameterU3Ek__BackingField_0)); }
	inline String_t* get_U3CParameterU3Ek__BackingField_0() const { return ___U3CParameterU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CParameterU3Ek__BackingField_0() { return &___U3CParameterU3Ek__BackingField_0; }
	inline void set_U3CParameterU3Ek__BackingField_0(String_t* value)
	{
		___U3CParameterU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParameterU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CSchemeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6, ___U3CSchemeU3Ek__BackingField_1)); }
	inline String_t* get_U3CSchemeU3Ek__BackingField_1() const { return ___U3CSchemeU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CSchemeU3Ek__BackingField_1() { return &___U3CSchemeU3Ek__BackingField_1; }
	inline void set_U3CSchemeU3Ek__BackingField_1(String_t* value)
	{
		___U3CSchemeU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSchemeU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // AUTHENTICATIONHEADERVALUE_TC285A16C25538CA86FA59B88CC25E6999BB5F1A6_H
#ifndef COLLECTIONEXTENSIONS_TF55A4677D40CF6208C37CAB46E039D5DF6DC790B_H
#define COLLECTIONEXTENSIONS_TF55A4677D40CF6208C37CAB46E039D5DF6DC790B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.CollectionExtensions
struct  CollectionExtensions_tF55A4677D40CF6208C37CAB46E039D5DF6DC790B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONEXTENSIONS_TF55A4677D40CF6208C37CAB46E039D5DF6DC790B_H
#ifndef COLLECTIONPARSER_T1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD_H
#define COLLECTIONPARSER_T1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.CollectionParser
struct  CollectionParser_t1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COLLECTIONPARSER_T1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD_H
#ifndef CONTENTDISPOSITIONHEADERVALUE_T424BAD39E99F23CE48CF86744B638923E2B539B0_H
#define CONTENTDISPOSITIONHEADERVALUE_T424BAD39E99F23CE48CF86744B638923E2B539B0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ContentDispositionHeaderValue
struct  ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ContentDispositionHeaderValue::dispositionType
	String_t* ___dispositionType_0;
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.ContentDispositionHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_1;

public:
	inline static int32_t get_offset_of_dispositionType_0() { return static_cast<int32_t>(offsetof(ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0, ___dispositionType_0)); }
	inline String_t* get_dispositionType_0() const { return ___dispositionType_0; }
	inline String_t** get_address_of_dispositionType_0() { return &___dispositionType_0; }
	inline void set_dispositionType_0(String_t* value)
	{
		___dispositionType_0 = value;
		Il2CppCodeGenWriteBarrier((&___dispositionType_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0, ___parameters_1)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_1() const { return ___parameters_1; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTDISPOSITIONHEADERVALUE_T424BAD39E99F23CE48CF86744B638923E2B539B0_H
#ifndef ENTITYTAGHEADERVALUE_TF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_H
#define ENTITYTAGHEADERVALUE_TF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.EntityTagHeaderValue
struct  EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Http.Headers.EntityTagHeaderValue::<IsWeak>k__BackingField
	bool ___U3CIsWeakU3Ek__BackingField_1;
	// System.String System.Net.Http.Headers.EntityTagHeaderValue::<Tag>k__BackingField
	String_t* ___U3CTagU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CIsWeakU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE, ___U3CIsWeakU3Ek__BackingField_1)); }
	inline bool get_U3CIsWeakU3Ek__BackingField_1() const { return ___U3CIsWeakU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CIsWeakU3Ek__BackingField_1() { return &___U3CIsWeakU3Ek__BackingField_1; }
	inline void set_U3CIsWeakU3Ek__BackingField_1(bool value)
	{
		___U3CIsWeakU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CTagU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE, ___U3CTagU3Ek__BackingField_2)); }
	inline String_t* get_U3CTagU3Ek__BackingField_2() const { return ___U3CTagU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CTagU3Ek__BackingField_2() { return &___U3CTagU3Ek__BackingField_2; }
	inline void set_U3CTagU3Ek__BackingField_2(String_t* value)
	{
		___U3CTagU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTagU3Ek__BackingField_2), value);
	}
};

struct EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields
{
public:
	// System.Net.Http.Headers.EntityTagHeaderValue System.Net.Http.Headers.EntityTagHeaderValue::any
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * ___any_0;

public:
	inline static int32_t get_offset_of_any_0() { return static_cast<int32_t>(offsetof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields, ___any_0)); }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * get_any_0() const { return ___any_0; }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE ** get_address_of_any_0() { return &___any_0; }
	inline void set_any_0(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * value)
	{
		___any_0 = value;
		Il2CppCodeGenWriteBarrier((&___any_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENTITYTAGHEADERVALUE_TF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_H
#ifndef HASHCODECALCULATOR_T0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994_H
#define HASHCODECALCULATOR_T0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HashCodeCalculator
struct  HashCodeCalculator_t0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HASHCODECALCULATOR_T0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994_H
#ifndef HEADERBUCKET_TB4D5217F305487EEBBB21B36236E85CFD40E036F_H
#define HEADERBUCKET_TB4D5217F305487EEBBB21B36236E85CFD40E036F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaders/HeaderBucket
struct  HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F  : public RuntimeObject
{
public:
	// System.Object System.Net.Http.Headers.HttpHeaders/HeaderBucket::Parsed
	RuntimeObject * ___Parsed_0;
	// System.Collections.Generic.List`1<System.String> System.Net.Http.Headers.HttpHeaders/HeaderBucket::values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___values_1;
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.HttpHeaders/HeaderBucket::CustomToString
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___CustomToString_2;

public:
	inline static int32_t get_offset_of_Parsed_0() { return static_cast<int32_t>(offsetof(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F, ___Parsed_0)); }
	inline RuntimeObject * get_Parsed_0() const { return ___Parsed_0; }
	inline RuntimeObject ** get_address_of_Parsed_0() { return &___Parsed_0; }
	inline void set_Parsed_0(RuntimeObject * value)
	{
		___Parsed_0 = value;
		Il2CppCodeGenWriteBarrier((&___Parsed_0), value);
	}

	inline static int32_t get_offset_of_values_1() { return static_cast<int32_t>(offsetof(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F, ___values_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_values_1() const { return ___values_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_values_1() { return &___values_1; }
	inline void set_values_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___values_1 = value;
		Il2CppCodeGenWriteBarrier((&___values_1), value);
	}

	inline static int32_t get_offset_of_CustomToString_2() { return static_cast<int32_t>(offsetof(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F, ___CustomToString_2)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_CustomToString_2() const { return ___CustomToString_2; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_CustomToString_2() { return &___CustomToString_2; }
	inline void set_CustomToString_2(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___CustomToString_2 = value;
		Il2CppCodeGenWriteBarrier((&___CustomToString_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERBUCKET_TB4D5217F305487EEBBB21B36236E85CFD40E036F_H
#ifndef U3CU3EC_T54A86B32672AD46F6B14A5F8CF0DED85254C5107_H
#define U3CU3EC_T54A86B32672AD46F6B14A5F8CF0DED85254C5107_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpRequestHeaders/<>c
struct  U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields
{
public:
	// System.Net.Http.Headers.HttpRequestHeaders/<>c System.Net.Http.Headers.HttpRequestHeaders/<>c::<>9
	U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107 * ___U3CU3E9_0;
	// System.Predicate`1<System.String> System.Net.Http.Headers.HttpRequestHeaders/<>c::<>9__19_0
	Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF * ___U3CU3E9__19_0_1;
	// System.Predicate`1<System.String> System.Net.Http.Headers.HttpRequestHeaders/<>c::<>9__22_0
	Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF * ___U3CU3E9__22_0_2;
	// System.Predicate`1<System.Net.Http.Headers.TransferCodingHeaderValue> System.Net.Http.Headers.HttpRequestHeaders/<>c::<>9__29_0
	Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 * ___U3CU3E9__29_0_3;
	// System.Predicate`1<System.Net.Http.Headers.TransferCodingHeaderValue> System.Net.Http.Headers.HttpRequestHeaders/<>c::<>9__71_0
	Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 * ___U3CU3E9__71_0_4;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__19_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields, ___U3CU3E9__19_0_1)); }
	inline Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF * get_U3CU3E9__19_0_1() const { return ___U3CU3E9__19_0_1; }
	inline Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF ** get_address_of_U3CU3E9__19_0_1() { return &___U3CU3E9__19_0_1; }
	inline void set_U3CU3E9__19_0_1(Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF * value)
	{
		___U3CU3E9__19_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__19_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__22_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields, ___U3CU3E9__22_0_2)); }
	inline Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF * get_U3CU3E9__22_0_2() const { return ___U3CU3E9__22_0_2; }
	inline Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF ** get_address_of_U3CU3E9__22_0_2() { return &___U3CU3E9__22_0_2; }
	inline void set_U3CU3E9__22_0_2(Predicate_1_tC575D8C60CE0F295940109F9016CFA53A7E7B3CF * value)
	{
		___U3CU3E9__22_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__22_0_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__29_0_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields, ___U3CU3E9__29_0_3)); }
	inline Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 * get_U3CU3E9__29_0_3() const { return ___U3CU3E9__29_0_3; }
	inline Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 ** get_address_of_U3CU3E9__29_0_3() { return &___U3CU3E9__29_0_3; }
	inline void set_U3CU3E9__29_0_3(Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 * value)
	{
		___U3CU3E9__29_0_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__29_0_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__71_0_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields, ___U3CU3E9__71_0_4)); }
	inline Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 * get_U3CU3E9__71_0_4() const { return ___U3CU3E9__71_0_4; }
	inline Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 ** get_address_of_U3CU3E9__71_0_4() { return &___U3CU3E9__71_0_4; }
	inline void set_U3CU3E9__71_0_4(Predicate_1_t40C8DCCDCA5C2D5EEEA14E41A3194A9DB29472B9 * value)
	{
		___U3CU3E9__71_0_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__71_0_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T54A86B32672AD46F6B14A5F8CF0DED85254C5107_H
#ifndef LEXER_T1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_H
#define LEXER_T1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Lexer
struct  Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.Lexer::s
	String_t* ___s_3;
	// System.Int32 System.Net.Http.Headers.Lexer::pos
	int32_t ___pos_4;

public:
	inline static int32_t get_offset_of_s_3() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3, ___s_3)); }
	inline String_t* get_s_3() const { return ___s_3; }
	inline String_t** get_address_of_s_3() { return &___s_3; }
	inline void set_s_3(String_t* value)
	{
		___s_3 = value;
		Il2CppCodeGenWriteBarrier((&___s_3), value);
	}

	inline static int32_t get_offset_of_pos_4() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3, ___pos_4)); }
	inline int32_t get_pos_4() const { return ___pos_4; }
	inline int32_t* get_address_of_pos_4() { return &___pos_4; }
	inline void set_pos_4(int32_t value)
	{
		___pos_4 = value;
	}
};

struct Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields
{
public:
	// System.Boolean[] System.Net.Http.Headers.Lexer::token_chars
	BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* ___token_chars_0;
	// System.Int32 System.Net.Http.Headers.Lexer::last_token_char
	int32_t ___last_token_char_1;
	// System.String[] System.Net.Http.Headers.Lexer::dt_formats
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___dt_formats_2;

public:
	inline static int32_t get_offset_of_token_chars_0() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields, ___token_chars_0)); }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* get_token_chars_0() const { return ___token_chars_0; }
	inline BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040** get_address_of_token_chars_0() { return &___token_chars_0; }
	inline void set_token_chars_0(BooleanU5BU5D_t192C7579715690E25BD5EFED47F3E0FC9DCB2040* value)
	{
		___token_chars_0 = value;
		Il2CppCodeGenWriteBarrier((&___token_chars_0), value);
	}

	inline static int32_t get_offset_of_last_token_char_1() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields, ___last_token_char_1)); }
	inline int32_t get_last_token_char_1() const { return ___last_token_char_1; }
	inline int32_t* get_address_of_last_token_char_1() { return &___last_token_char_1; }
	inline void set_last_token_char_1(int32_t value)
	{
		___last_token_char_1 = value;
	}

	inline static int32_t get_offset_of_dt_formats_2() { return static_cast<int32_t>(offsetof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields, ___dt_formats_2)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_dt_formats_2() const { return ___dt_formats_2; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_dt_formats_2() { return &___dt_formats_2; }
	inline void set_dt_formats_2(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___dt_formats_2 = value;
		Il2CppCodeGenWriteBarrier((&___dt_formats_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXER_T1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_H
#ifndef MEDIATYPEHEADERVALUE_TB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1_H
#define MEDIATYPEHEADERVALUE_TB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.MediaTypeHeaderValue
struct  MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.MediaTypeHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_0;
	// System.String System.Net.Http.Headers.MediaTypeHeaderValue::media_type
	String_t* ___media_type_1;

public:
	inline static int32_t get_offset_of_parameters_0() { return static_cast<int32_t>(offsetof(MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1, ___parameters_0)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_0() const { return ___parameters_0; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_0() { return &___parameters_0; }
	inline void set_parameters_0(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_0 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_0), value);
	}

	inline static int32_t get_offset_of_media_type_1() { return static_cast<int32_t>(offsetof(MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1, ___media_type_1)); }
	inline String_t* get_media_type_1() const { return ___media_type_1; }
	inline String_t** get_address_of_media_type_1() { return &___media_type_1; }
	inline void set_media_type_1(String_t* value)
	{
		___media_type_1 = value;
		Il2CppCodeGenWriteBarrier((&___media_type_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPEHEADERVALUE_TB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1_H
#ifndef NAMEVALUEHEADERVALUE_TD6B9C6286E270D5EDCDA199D85D786492D15CD1F_H
#define NAMEVALUEHEADERVALUE_TD6B9C6286E270D5EDCDA199D85D786492D15CD1F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.NameValueHeaderValue
struct  NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.NameValueHeaderValue::value
	String_t* ___value_0;
	// System.String System.Net.Http.Headers.NameValueHeaderValue::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F, ___U3CNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CNameU3Ek__BackingField_1() const { return ___U3CNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_1() { return &___U3CNameU3Ek__BackingField_1; }
	inline void set_U3CNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUEHEADERVALUE_TD6B9C6286E270D5EDCDA199D85D786492D15CD1F_H
#ifndef PARSER_TC9CE5F5FFDD4927D039C3189B5945DA5EF958025_H
#define PARSER_TC9CE5F5FFDD4927D039C3189B5945DA5EF958025_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser
struct  Parser_tC9CE5F5FFDD4927D039C3189B5945DA5EF958025  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_TC9CE5F5FFDD4927D039C3189B5945DA5EF958025_H
#ifndef DATETIME_T0F9E1FF7B5D00351111593AEA0FA9034CB63108F_H
#define DATETIME_T0F9E1FF7B5D00351111593AEA0FA9034CB63108F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/DateTime
struct  DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F  : public RuntimeObject
{
public:

public:
};

struct DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.Parser/DateTime::ToString
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___ToString_0;

public:
	inline static int32_t get_offset_of_ToString_0() { return static_cast<int32_t>(offsetof(DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields, ___ToString_0)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_ToString_0() const { return ___ToString_0; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_ToString_0() { return &___ToString_0; }
	inline void set_ToString_0(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___ToString_0 = value;
		Il2CppCodeGenWriteBarrier((&___ToString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T0F9E1FF7B5D00351111593AEA0FA9034CB63108F_H
#ifndef U3CU3EC_T06D259B007D5CD69873ECF647B2C5E37CD9262AE_H
#define U3CU3EC_T06D259B007D5CD69873ECF647B2C5E37CD9262AE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/DateTime/<>c
struct  U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields
{
public:
	// System.Net.Http.Headers.Parser/DateTime/<>c System.Net.Http.Headers.Parser/DateTime/<>c::<>9
	U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T06D259B007D5CD69873ECF647B2C5E37CD9262AE_H
#ifndef EMAILADDRESS_T34037A92039853D504922D84E1512E6BE4FBDE1B_H
#define EMAILADDRESS_T34037A92039853D504922D84E1512E6BE4FBDE1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/EmailAddress
struct  EmailAddress_t34037A92039853D504922D84E1512E6BE4FBDE1B  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMAILADDRESS_T34037A92039853D504922D84E1512E6BE4FBDE1B_H
#ifndef HOST_T225394EBFA7427D924DC794F6FEB33C7B19E6583_H
#define HOST_T225394EBFA7427D924DC794F6FEB33C7B19E6583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/Host
struct  Host_t225394EBFA7427D924DC794F6FEB33C7B19E6583  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HOST_T225394EBFA7427D924DC794F6FEB33C7B19E6583_H
#ifndef INT_T9123F050289CA0271A077EFF47440CBD5C366316_H
#define INT_T9123F050289CA0271A077EFF47440CBD5C366316_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/Int
struct  Int_t9123F050289CA0271A077EFF47440CBD5C366316  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INT_T9123F050289CA0271A077EFF47440CBD5C366316_H
#ifndef LONG_T28918BF6DBAB433B07F2F91D220DF42F896C5C63_H
#define LONG_T28918BF6DBAB433B07F2F91D220DF42F896C5C63_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/Long
struct  Long_t28918BF6DBAB433B07F2F91D220DF42F896C5C63  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LONG_T28918BF6DBAB433B07F2F91D220DF42F896C5C63_H
#ifndef MD5_TBF240EC56773202152DBB5C53796D718895EC4EC_H
#define MD5_TBF240EC56773202152DBB5C53796D718895EC4EC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/MD5
struct  MD5_tBF240EC56773202152DBB5C53796D718895EC4EC  : public RuntimeObject
{
public:

public:
};

struct MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields
{
public:
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.Parser/MD5::ToString
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___ToString_0;

public:
	inline static int32_t get_offset_of_ToString_0() { return static_cast<int32_t>(offsetof(MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields, ___ToString_0)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_ToString_0() const { return ___ToString_0; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_ToString_0() { return &___ToString_0; }
	inline void set_ToString_0(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___ToString_0 = value;
		Il2CppCodeGenWriteBarrier((&___ToString_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MD5_TBF240EC56773202152DBB5C53796D718895EC4EC_H
#ifndef U3CU3EC_T2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_H
#define U3CU3EC_T2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/MD5/<>c
struct  U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields
{
public:
	// System.Net.Http.Headers.Parser/MD5/<>c System.Net.Http.Headers.Parser/MD5/<>c::<>9
	U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F * ___U3CU3E9_0;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_H
#ifndef TIMESPANSECONDS_T147412A8A309180783B8369B158301EFA80A9DA5_H
#define TIMESPANSECONDS_T147412A8A309180783B8369B158301EFA80A9DA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/TimeSpanSeconds
struct  TimeSpanSeconds_t147412A8A309180783B8369B158301EFA80A9DA5  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPANSECONDS_T147412A8A309180783B8369B158301EFA80A9DA5_H
#ifndef TOKEN_T63D07F66DA54B86A2FC571E8F88F92327CD51F13_H
#define TOKEN_T63D07F66DA54B86A2FC571E8F88F92327CD51F13_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/Token
struct  Token_t63D07F66DA54B86A2FC571E8F88F92327CD51F13  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T63D07F66DA54B86A2FC571E8F88F92327CD51F13_H
#ifndef URI_TAA37A4AC2B4CCC173820F682599007F88D08407D_H
#define URI_TAA37A4AC2B4CCC173820F682599007F88D08407D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Parser/Uri
struct  Uri_tAA37A4AC2B4CCC173820F682599007F88D08407D  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // URI_TAA37A4AC2B4CCC173820F682599007F88D08407D_H
#ifndef PRODUCTHEADERVALUE_T3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5_H
#define PRODUCTHEADERVALUE_T3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ProductHeaderValue
struct  ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ProductHeaderValue::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.ProductHeaderValue::<Version>k__BackingField
	String_t* ___U3CVersionU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5, ___U3CVersionU3Ek__BackingField_1)); }
	inline String_t* get_U3CVersionU3Ek__BackingField_1() const { return ___U3CVersionU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CVersionU3Ek__BackingField_1() { return &___U3CVersionU3Ek__BackingField_1; }
	inline void set_U3CVersionU3Ek__BackingField_1(String_t* value)
	{
		___U3CVersionU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVersionU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTHEADERVALUE_T3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5_H
#ifndef PRODUCTINFOHEADERVALUE_T2E8EC6871340B4ABC59FE4CA932EABD0E41C2246_H
#define PRODUCTINFOHEADERVALUE_T2E8EC6871340B4ABC59FE4CA932EABD0E41C2246_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ProductInfoHeaderValue
struct  ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ProductInfoHeaderValue::<Comment>k__BackingField
	String_t* ___U3CCommentU3Ek__BackingField_0;
	// System.Net.Http.Headers.ProductHeaderValue System.Net.Http.Headers.ProductInfoHeaderValue::<Product>k__BackingField
	ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 * ___U3CProductU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246, ___U3CCommentU3Ek__BackingField_0)); }
	inline String_t* get_U3CCommentU3Ek__BackingField_0() const { return ___U3CCommentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCommentU3Ek__BackingField_0() { return &___U3CCommentU3Ek__BackingField_0; }
	inline void set_U3CCommentU3Ek__BackingField_0(String_t* value)
	{
		___U3CCommentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProductU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246, ___U3CProductU3Ek__BackingField_1)); }
	inline ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 * get_U3CProductU3Ek__BackingField_1() const { return ___U3CProductU3Ek__BackingField_1; }
	inline ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 ** get_address_of_U3CProductU3Ek__BackingField_1() { return &___U3CProductU3Ek__BackingField_1; }
	inline void set_U3CProductU3Ek__BackingField_1(ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5 * value)
	{
		___U3CProductU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProductU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PRODUCTINFOHEADERVALUE_T2E8EC6871340B4ABC59FE4CA932EABD0E41C2246_H
#ifndef RANGEHEADERVALUE_T2763CB54E86AD644BD547989483DB862B443D723_H
#define RANGEHEADERVALUE_T2763CB54E86AD644BD547989483DB862B443D723_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RangeHeaderValue
struct  RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.RangeItemHeaderValue> System.Net.Http.Headers.RangeHeaderValue::ranges
	List_1_tBC994597F79973AB8B0C84504B8869AD94636845 * ___ranges_0;
	// System.String System.Net.Http.Headers.RangeHeaderValue::unit
	String_t* ___unit_1;

public:
	inline static int32_t get_offset_of_ranges_0() { return static_cast<int32_t>(offsetof(RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723, ___ranges_0)); }
	inline List_1_tBC994597F79973AB8B0C84504B8869AD94636845 * get_ranges_0() const { return ___ranges_0; }
	inline List_1_tBC994597F79973AB8B0C84504B8869AD94636845 ** get_address_of_ranges_0() { return &___ranges_0; }
	inline void set_ranges_0(List_1_tBC994597F79973AB8B0C84504B8869AD94636845 * value)
	{
		___ranges_0 = value;
		Il2CppCodeGenWriteBarrier((&___ranges_0), value);
	}

	inline static int32_t get_offset_of_unit_1() { return static_cast<int32_t>(offsetof(RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723, ___unit_1)); }
	inline String_t* get_unit_1() const { return ___unit_1; }
	inline String_t** get_address_of_unit_1() { return &___unit_1; }
	inline void set_unit_1(String_t* value)
	{
		___unit_1 = value;
		Il2CppCodeGenWriteBarrier((&___unit_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEHEADERVALUE_T2763CB54E86AD644BD547989483DB862B443D723_H
#ifndef TRANSFERCODINGHEADERVALUE_T26DE304410101552CB12F49B948B041AD63E0E6C_H
#define TRANSFERCODINGHEADERVALUE_T26DE304410101552CB12F49B948B041AD63E0E6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.TransferCodingHeaderValue
struct  TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.TransferCodingHeaderValue::value
	String_t* ___value_0;
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.TransferCodingHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C, ___value_0)); }
	inline String_t* get_value_0() const { return ___value_0; }
	inline String_t** get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(String_t* value)
	{
		___value_0 = value;
		Il2CppCodeGenWriteBarrier((&___value_0), value);
	}

	inline static int32_t get_offset_of_parameters_1() { return static_cast<int32_t>(offsetof(TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C, ___parameters_1)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_1() const { return ___parameters_1; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_1() { return &___parameters_1; }
	inline void set_parameters_1(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_1 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFERCODINGHEADERVALUE_T26DE304410101552CB12F49B948B041AD63E0E6C_H
#ifndef VIAHEADERVALUE_TA4BE22B235705360625337A3278F31BD5FEEFB6B_H
#define VIAHEADERVALUE_TA4BE22B235705360625337A3278F31BD5FEEFB6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ViaHeaderValue
struct  ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ViaHeaderValue::<Comment>k__BackingField
	String_t* ___U3CCommentU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.ViaHeaderValue::<ProtocolName>k__BackingField
	String_t* ___U3CProtocolNameU3Ek__BackingField_1;
	// System.String System.Net.Http.Headers.ViaHeaderValue::<ProtocolVersion>k__BackingField
	String_t* ___U3CProtocolVersionU3Ek__BackingField_2;
	// System.String System.Net.Http.Headers.ViaHeaderValue::<ReceivedBy>k__BackingField
	String_t* ___U3CReceivedByU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CCommentU3Ek__BackingField_0)); }
	inline String_t* get_U3CCommentU3Ek__BackingField_0() const { return ___U3CCommentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCommentU3Ek__BackingField_0() { return &___U3CCommentU3Ek__BackingField_0; }
	inline void set_U3CCommentU3Ek__BackingField_0(String_t* value)
	{
		___U3CCommentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CProtocolNameU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CProtocolNameU3Ek__BackingField_1)); }
	inline String_t* get_U3CProtocolNameU3Ek__BackingField_1() const { return ___U3CProtocolNameU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CProtocolNameU3Ek__BackingField_1() { return &___U3CProtocolNameU3Ek__BackingField_1; }
	inline void set_U3CProtocolNameU3Ek__BackingField_1(String_t* value)
	{
		___U3CProtocolNameU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolNameU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CProtocolVersionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CProtocolVersionU3Ek__BackingField_2)); }
	inline String_t* get_U3CProtocolVersionU3Ek__BackingField_2() const { return ___U3CProtocolVersionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CProtocolVersionU3Ek__BackingField_2() { return &___U3CProtocolVersionU3Ek__BackingField_2; }
	inline void set_U3CProtocolVersionU3Ek__BackingField_2(String_t* value)
	{
		___U3CProtocolVersionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProtocolVersionU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CReceivedByU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B, ___U3CReceivedByU3Ek__BackingField_3)); }
	inline String_t* get_U3CReceivedByU3Ek__BackingField_3() const { return ___U3CReceivedByU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CReceivedByU3Ek__BackingField_3() { return &___U3CReceivedByU3Ek__BackingField_3; }
	inline void set_U3CReceivedByU3Ek__BackingField_3(String_t* value)
	{
		___U3CReceivedByU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CReceivedByU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VIAHEADERVALUE_TA4BE22B235705360625337A3278F31BD5FEEFB6B_H
#ifndef U3CU3EC_T1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_H
#define U3CU3EC_T1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClientHandler/<>c
struct  U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields
{
public:
	// System.Net.Http.HttpClientHandler/<>c System.Net.Http.HttpClientHandler/<>c::<>9
	U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD * ___U3CU3E9_0;
	// System.Func`2<System.String,System.Boolean> System.Net.Http.HttpClientHandler/<>c::<>9__61_0
	Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * ___U3CU3E9__61_0_1;
	// System.Action`1<System.Object> System.Net.Http.HttpClientHandler/<>c::<>9__64_0
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___U3CU3E9__64_0_2;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__61_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields, ___U3CU3E9__61_0_1)); }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * get_U3CU3E9__61_0_1() const { return ___U3CU3E9__61_0_1; }
	inline Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 ** get_address_of_U3CU3E9__61_0_1() { return &___U3CU3E9__61_0_1; }
	inline void set_U3CU3E9__61_0_1(Func_2_t47A4F23E51D5B6A30A39E5329C1AAE9513FD8EE0 * value)
	{
		___U3CU3E9__61_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__61_0_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__64_0_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields, ___U3CU3E9__64_0_2)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_U3CU3E9__64_0_2() const { return ___U3CU3E9__64_0_2; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_U3CU3E9__64_0_2() { return &___U3CU3E9__64_0_2; }
	inline void set_U3CU3E9__64_0_2(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___U3CU3E9__64_0_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__64_0_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_H
#ifndef HTTPCONTENT_T678722F60CA151DD19AFCA917657BE24F111A41D_H
#define HTTPCONTENT_T678722F60CA151DD19AFCA917657BE24F111A41D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpContent
struct  HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D  : public RuntimeObject
{
public:
	// System.Net.Http.HttpContent/FixedMemoryStream System.Net.Http.HttpContent::buffer
	FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C * ___buffer_0;
	// System.Boolean System.Net.Http.HttpContent::disposed
	bool ___disposed_1;
	// System.Net.Http.Headers.HttpContentHeaders System.Net.Http.HttpContent::headers
	HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0 * ___headers_2;

public:
	inline static int32_t get_offset_of_buffer_0() { return static_cast<int32_t>(offsetof(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D, ___buffer_0)); }
	inline FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C * get_buffer_0() const { return ___buffer_0; }
	inline FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C ** get_address_of_buffer_0() { return &___buffer_0; }
	inline void set_buffer_0(FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C * value)
	{
		___buffer_0 = value;
		Il2CppCodeGenWriteBarrier((&___buffer_0), value);
	}

	inline static int32_t get_offset_of_disposed_1() { return static_cast<int32_t>(offsetof(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D, ___disposed_1)); }
	inline bool get_disposed_1() const { return ___disposed_1; }
	inline bool* get_address_of_disposed_1() { return &___disposed_1; }
	inline void set_disposed_1(bool value)
	{
		___disposed_1 = value;
	}

	inline static int32_t get_offset_of_headers_2() { return static_cast<int32_t>(offsetof(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D, ___headers_2)); }
	inline HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0 * get_headers_2() const { return ___headers_2; }
	inline HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0 ** get_address_of_headers_2() { return &___headers_2; }
	inline void set_headers_2(HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0 * value)
	{
		___headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___headers_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONTENT_T678722F60CA151DD19AFCA917657BE24F111A41D_H
#ifndef HTTPMESSAGEHANDLER_T0094EF2850CF6420CBFC3952ED337AD381644894_H
#define HTTPMESSAGEHANDLER_T0094EF2850CF6420CBFC3952ED337AD381644894_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpMessageHandler
struct  HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMESSAGEHANDLER_T0094EF2850CF6420CBFC3952ED337AD381644894_H
#ifndef HTTPMESSAGEINVOKER_T7270E2BED3201CE430D6C4BECF923454AA526A72_H
#define HTTPMESSAGEINVOKER_T7270E2BED3201CE430D6C4BECF923454AA526A72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpMessageInvoker
struct  HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72  : public RuntimeObject
{
public:
	// System.Net.Http.HttpMessageHandler System.Net.Http.HttpMessageInvoker::handler
	HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 * ___handler_0;
	// System.Boolean System.Net.Http.HttpMessageInvoker::disposeHandler
	bool ___disposeHandler_1;

public:
	inline static int32_t get_offset_of_handler_0() { return static_cast<int32_t>(offsetof(HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72, ___handler_0)); }
	inline HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 * get_handler_0() const { return ___handler_0; }
	inline HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 ** get_address_of_handler_0() { return &___handler_0; }
	inline void set_handler_0(HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894 * value)
	{
		___handler_0 = value;
		Il2CppCodeGenWriteBarrier((&___handler_0), value);
	}

	inline static int32_t get_offset_of_disposeHandler_1() { return static_cast<int32_t>(offsetof(HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72, ___disposeHandler_1)); }
	inline bool get_disposeHandler_1() const { return ___disposeHandler_1; }
	inline bool* get_address_of_disposeHandler_1() { return &___disposeHandler_1; }
	inline void set_disposeHandler_1(bool value)
	{
		___disposeHandler_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMESSAGEINVOKER_T7270E2BED3201CE430D6C4BECF923454AA526A72_H
#ifndef HTTPMETHOD_TC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_H
#define HTTPMETHOD_TC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpMethod
struct  HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220  : public RuntimeObject
{
public:
	// System.String System.Net.Http.HttpMethod::method
	String_t* ___method_7;

public:
	inline static int32_t get_offset_of_method_7() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220, ___method_7)); }
	inline String_t* get_method_7() const { return ___method_7; }
	inline String_t** get_address_of_method_7() { return &___method_7; }
	inline void set_method_7(String_t* value)
	{
		___method_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_7), value);
	}
};

struct HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields
{
public:
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::delete_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___delete_method_0;
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::get_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___get_method_1;
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::head_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___head_method_2;
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::options_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___options_method_3;
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::post_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___post_method_4;
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::put_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___put_method_5;
	// System.Net.Http.HttpMethod System.Net.Http.HttpMethod::trace_method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___trace_method_6;

public:
	inline static int32_t get_offset_of_delete_method_0() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___delete_method_0)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_delete_method_0() const { return ___delete_method_0; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_delete_method_0() { return &___delete_method_0; }
	inline void set_delete_method_0(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___delete_method_0 = value;
		Il2CppCodeGenWriteBarrier((&___delete_method_0), value);
	}

	inline static int32_t get_offset_of_get_method_1() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___get_method_1)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_get_method_1() const { return ___get_method_1; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_get_method_1() { return &___get_method_1; }
	inline void set_get_method_1(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___get_method_1 = value;
		Il2CppCodeGenWriteBarrier((&___get_method_1), value);
	}

	inline static int32_t get_offset_of_head_method_2() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___head_method_2)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_head_method_2() const { return ___head_method_2; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_head_method_2() { return &___head_method_2; }
	inline void set_head_method_2(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___head_method_2 = value;
		Il2CppCodeGenWriteBarrier((&___head_method_2), value);
	}

	inline static int32_t get_offset_of_options_method_3() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___options_method_3)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_options_method_3() const { return ___options_method_3; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_options_method_3() { return &___options_method_3; }
	inline void set_options_method_3(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___options_method_3 = value;
		Il2CppCodeGenWriteBarrier((&___options_method_3), value);
	}

	inline static int32_t get_offset_of_post_method_4() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___post_method_4)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_post_method_4() const { return ___post_method_4; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_post_method_4() { return &___post_method_4; }
	inline void set_post_method_4(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___post_method_4 = value;
		Il2CppCodeGenWriteBarrier((&___post_method_4), value);
	}

	inline static int32_t get_offset_of_put_method_5() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___put_method_5)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_put_method_5() const { return ___put_method_5; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_put_method_5() { return &___put_method_5; }
	inline void set_put_method_5(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___put_method_5 = value;
		Il2CppCodeGenWriteBarrier((&___put_method_5), value);
	}

	inline static int32_t get_offset_of_trace_method_6() { return static_cast<int32_t>(offsetof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields, ___trace_method_6)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_trace_method_6() const { return ___trace_method_6; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_trace_method_6() { return &___trace_method_6; }
	inline void set_trace_method_6(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___trace_method_6 = value;
		Il2CppCodeGenWriteBarrier((&___trace_method_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPMETHOD_TC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_H
#ifndef HTTPREQUESTMESSAGE_TBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427_H
#define HTTPREQUESTMESSAGE_TBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpRequestMessage
struct  HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427  : public RuntimeObject
{
public:
	// System.Net.Http.Headers.HttpRequestHeaders System.Net.Http.HttpRequestMessage::headers
	HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * ___headers_0;
	// System.Net.Http.HttpMethod System.Net.Http.HttpRequestMessage::method
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * ___method_1;
	// System.Version System.Net.Http.HttpRequestMessage::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_2;
	// System.Uri System.Net.Http.HttpRequestMessage::uri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___uri_3;
	// System.Boolean System.Net.Http.HttpRequestMessage::is_used
	bool ___is_used_4;
	// System.Boolean System.Net.Http.HttpRequestMessage::disposed
	bool ___disposed_5;
	// System.Net.Http.HttpContent System.Net.Http.HttpRequestMessage::<Content>k__BackingField
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * ___U3CContentU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___headers_0)); }
	inline HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * get_headers_0() const { return ___headers_0; }
	inline HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier((&___headers_0), value);
	}

	inline static int32_t get_offset_of_method_1() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___method_1)); }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * get_method_1() const { return ___method_1; }
	inline HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 ** get_address_of_method_1() { return &___method_1; }
	inline void set_method_1(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220 * value)
	{
		___method_1 = value;
		Il2CppCodeGenWriteBarrier((&___method_1), value);
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___version_2)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_2() const { return ___version_2; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_2 = value;
		Il2CppCodeGenWriteBarrier((&___version_2), value);
	}

	inline static int32_t get_offset_of_uri_3() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___uri_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_uri_3() const { return ___uri_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_uri_3() { return &___uri_3; }
	inline void set_uri_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___uri_3 = value;
		Il2CppCodeGenWriteBarrier((&___uri_3), value);
	}

	inline static int32_t get_offset_of_is_used_4() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___is_used_4)); }
	inline bool get_is_used_4() const { return ___is_used_4; }
	inline bool* get_address_of_is_used_4() { return &___is_used_4; }
	inline void set_is_used_4(bool value)
	{
		___is_used_4 = value;
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}

	inline static int32_t get_offset_of_U3CContentU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427, ___U3CContentU3Ek__BackingField_6)); }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * get_U3CContentU3Ek__BackingField_6() const { return ___U3CContentU3Ek__BackingField_6; }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D ** get_address_of_U3CContentU3Ek__BackingField_6() { return &___U3CContentU3Ek__BackingField_6; }
	inline void set_U3CContentU3Ek__BackingField_6(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * value)
	{
		___U3CContentU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTMESSAGE_TBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#define BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Boolean
struct  Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((&___TrueString_5), value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((&___FalseString_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BOOLEAN_TB53F6830F670160873277339AA58F15CAED4399C_H
#ifndef KEYVALUEPAIR_2_T96231301FF22709182494F359E6D4D77EE2F9684_H
#define KEYVALUEPAIR_2_T96231301FF22709182494F359E6D4D77EE2F9684_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.IEnumerable`1<System.String>>
struct  KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	RuntimeObject* ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684, ___value_1)); }
	inline RuntimeObject* get_value_1() const { return ___value_1; }
	inline RuntimeObject** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(RuntimeObject* value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_T96231301FF22709182494F359E6D4D77EE2F9684_H
#ifndef KEYVALUEPAIR_2_TBBB4C8437E21CB55FCB1A1B93B5440D235FB3768_H
#define KEYVALUEPAIR_2_TBBB4C8437E21CB55FCB1A1B93B5440D235FB3768_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.KeyValuePair`2<System.String,System.Net.Http.Headers.HttpHeaders/HeaderBucket>
struct  KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768 
{
public:
	// TKey System.Collections.Generic.KeyValuePair`2::key
	String_t* ___key_0;
	// TValue System.Collections.Generic.KeyValuePair`2::value
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F * ___value_1;

public:
	inline static int32_t get_offset_of_key_0() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768, ___key_0)); }
	inline String_t* get_key_0() const { return ___key_0; }
	inline String_t** get_address_of_key_0() { return &___key_0; }
	inline void set_key_0(String_t* value)
	{
		___key_0 = value;
		Il2CppCodeGenWriteBarrier((&___key_0), value);
	}

	inline static int32_t get_offset_of_value_1() { return static_cast<int32_t>(offsetof(KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768, ___value_1)); }
	inline HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F * get_value_1() const { return ___value_1; }
	inline HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F ** get_address_of_value_1() { return &___value_1; }
	inline void set_value_1(HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F * value)
	{
		___value_1 = value;
		Il2CppCodeGenWriteBarrier((&___value_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // KEYVALUEPAIR_2_TBBB4C8437E21CB55FCB1A1B93B5440D235FB3768_H
#ifndef DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#define DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTime
struct  DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 
{
public:
	// System.UInt64 System.DateTime::dateData
	uint64_t ___dateData_44;

public:
	inline static int32_t get_offset_of_dateData_44() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132, ___dateData_44)); }
	inline uint64_t get_dateData_44() const { return ___dateData_44; }
	inline uint64_t* get_address_of_dateData_44() { return &___dateData_44; }
	inline void set_dateData_44(uint64_t value)
	{
		___dateData_44 = value;
	}
};

struct DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields
{
public:
	// System.Int32[] System.DateTime::DaysToMonth365
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth365_29;
	// System.Int32[] System.DateTime::DaysToMonth366
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___DaysToMonth366_30;
	// System.DateTime System.DateTime::MinValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MinValue_31;
	// System.DateTime System.DateTime::MaxValue
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___MaxValue_32;

public:
	inline static int32_t get_offset_of_DaysToMonth365_29() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth365_29)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth365_29() const { return ___DaysToMonth365_29; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth365_29() { return &___DaysToMonth365_29; }
	inline void set_DaysToMonth365_29(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth365_29 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth365_29), value);
	}

	inline static int32_t get_offset_of_DaysToMonth366_30() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___DaysToMonth366_30)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_DaysToMonth366_30() const { return ___DaysToMonth366_30; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_DaysToMonth366_30() { return &___DaysToMonth366_30; }
	inline void set_DaysToMonth366_30(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___DaysToMonth366_30 = value;
		Il2CppCodeGenWriteBarrier((&___DaysToMonth366_30), value);
	}

	inline static int32_t get_offset_of_MinValue_31() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MinValue_31)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MinValue_31() const { return ___MinValue_31; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MinValue_31() { return &___MinValue_31; }
	inline void set_MinValue_31(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MinValue_31 = value;
	}

	inline static int32_t get_offset_of_MaxValue_32() { return static_cast<int32_t>(offsetof(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132_StaticFields, ___MaxValue_32)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_MaxValue_32() const { return ___MaxValue_32; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_MaxValue_32() { return &___MaxValue_32; }
	inline void set_MaxValue_32(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___MaxValue_32 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIME_T349B7449FBAAFF4192636E2B7A07694DA9236132_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#define STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.Stream
struct  Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7  : public MarshalByRefObject_tC4577953D0A44D0AB8597CFA868E01C858B1C9AF
{
public:
	// System.IO.Stream/ReadWriteTask System.IO.Stream::_activeReadWriteTask
	ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * ____activeReadWriteTask_2;
	// System.Threading.SemaphoreSlim System.IO.Stream::_asyncActiveSemaphore
	SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * ____asyncActiveSemaphore_3;

public:
	inline static int32_t get_offset_of__activeReadWriteTask_2() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____activeReadWriteTask_2)); }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * get__activeReadWriteTask_2() const { return ____activeReadWriteTask_2; }
	inline ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 ** get_address_of__activeReadWriteTask_2() { return &____activeReadWriteTask_2; }
	inline void set__activeReadWriteTask_2(ReadWriteTask_tFA17EEE8BC5C4C83EAEFCC3662A30DE351ABAA80 * value)
	{
		____activeReadWriteTask_2 = value;
		Il2CppCodeGenWriteBarrier((&____activeReadWriteTask_2), value);
	}

	inline static int32_t get_offset_of__asyncActiveSemaphore_3() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7, ____asyncActiveSemaphore_3)); }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * get__asyncActiveSemaphore_3() const { return ____asyncActiveSemaphore_3; }
	inline SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 ** get_address_of__asyncActiveSemaphore_3() { return &____asyncActiveSemaphore_3; }
	inline void set__asyncActiveSemaphore_3(SemaphoreSlim_t2E2888D1C0C8FAB80823C76F1602E4434B8FA048 * value)
	{
		____asyncActiveSemaphore_3 = value;
		Il2CppCodeGenWriteBarrier((&____asyncActiveSemaphore_3), value);
	}
};

struct Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields
{
public:
	// System.IO.Stream System.IO.Stream::Null
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___Null_1;

public:
	inline static int32_t get_offset_of_Null_1() { return static_cast<int32_t>(offsetof(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7_StaticFields, ___Null_1)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_Null_1() const { return ___Null_1; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_Null_1() { return &___Null_1; }
	inline void set_Null_1(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___Null_1 = value;
		Il2CppCodeGenWriteBarrier((&___Null_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAM_TFC50657DD5AAB87770987F9179D934A51D99D5E7_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef MEDIATYPEWITHQUALITYHEADERVALUE_TA90D440F6C0876845E895A0A3C61FA36CA2B120E_H
#define MEDIATYPEWITHQUALITYHEADERVALUE_TA90D440F6C0876845E895A0A3C61FA36CA2B120E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.MediaTypeWithQualityHeaderValue
struct  MediaTypeWithQualityHeaderValue_tA90D440F6C0876845E895A0A3C61FA36CA2B120E  : public MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEDIATYPEWITHQUALITYHEADERVALUE_TA90D440F6C0876845E895A0A3C61FA36CA2B120E_H
#ifndef NAMEVALUEWITHPARAMETERSHEADERVALUE_T9E5A12E229B5C94317A9544AA090F35F3CBFBDBF_H
#define NAMEVALUEWITHPARAMETERSHEADERVALUE_T9E5A12E229B5C94317A9544AA090F35F3CBFBDBF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.NameValueWithParametersHeaderValue
struct  NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF  : public NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.NameValueWithParametersHeaderValue::parameters
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___parameters_2;

public:
	inline static int32_t get_offset_of_parameters_2() { return static_cast<int32_t>(offsetof(NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF, ___parameters_2)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_parameters_2() const { return ___parameters_2; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_parameters_2() { return &___parameters_2; }
	inline void set_parameters_2(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___parameters_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMEVALUEWITHPARAMETERSHEADERVALUE_T9E5A12E229B5C94317A9544AA090F35F3CBFBDBF_H
#ifndef TRANSFERCODINGWITHQUALITYHEADERVALUE_TCCBC3E7752056FE94AF475137682B15E84FA626C_H
#define TRANSFERCODINGWITHQUALITYHEADERVALUE_TCCBC3E7752056FE94AF475137682B15E84FA626C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.TransferCodingWithQualityHeaderValue
struct  TransferCodingWithQualityHeaderValue_tCCBC3E7752056FE94AF475137682B15E84FA626C  : public TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRANSFERCODINGWITHQUALITYHEADERVALUE_TCCBC3E7752056FE94AF475137682B15E84FA626C_H
#ifndef HTTPREQUESTEXCEPTION_TD4D14C20B90F362C83781589E7508A05E7060349_H
#define HTTPREQUESTEXCEPTION_TD4D14C20B90F362C83781589E7508A05E7060349_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpRequestException
struct  HttpRequestException_tD4D14C20B90F362C83781589E7508A05E7060349  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTEXCEPTION_TD4D14C20B90F362C83781589E7508A05E7060349_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_TBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF_H
#define NULLABLE_1_TBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Double>
struct  Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF 
{
public:
	// T System.Nullable`1::value
	double ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF, ___value_0)); }
	inline double get_value_0() const { return ___value_0; }
	inline double* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(double value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#define NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int64>
struct  Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 
{
public:
	// T System.Nullable`1::value
	int64_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___value_0)); }
	inline int64_t get_value_0() const { return ___value_0; }
	inline int64_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int64_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5_H
#ifndef ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#define ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct  AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 
{
public:
	// System.Runtime.CompilerServices.IAsyncStateMachine System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_stateMachine
	RuntimeObject* ___m_stateMachine_0;
	// System.Action System.Runtime.CompilerServices.AsyncMethodBuilderCore::m_defaultContextAction
	Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * ___m_defaultContextAction_1;

public:
	inline static int32_t get_offset_of_m_stateMachine_0() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_stateMachine_0)); }
	inline RuntimeObject* get_m_stateMachine_0() const { return ___m_stateMachine_0; }
	inline RuntimeObject** get_address_of_m_stateMachine_0() { return &___m_stateMachine_0; }
	inline void set_m_stateMachine_0(RuntimeObject* value)
	{
		___m_stateMachine_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_stateMachine_0), value);
	}

	inline static int32_t get_offset_of_m_defaultContextAction_1() { return static_cast<int32_t>(offsetof(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01, ___m_defaultContextAction_1)); }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * get_m_defaultContextAction_1() const { return ___m_defaultContextAction_1; }
	inline Action_t591D2A86165F896B4B800BB5C25CE18672A55579 ** get_address_of_m_defaultContextAction_1() { return &___m_defaultContextAction_1; }
	inline void set_m_defaultContextAction_1(Action_t591D2A86165F896B4B800BB5C25CE18672A55579 * value)
	{
		___m_defaultContextAction_1 = value;
		Il2CppCodeGenWriteBarrier((&___m_defaultContextAction_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_pinvoke
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncMethodBuilderCore
struct AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_marshaled_com
{
	RuntimeObject* ___m_stateMachine_0;
	Il2CppMethodPointer ___m_defaultContextAction_1;
};
#endif // ASYNCMETHODBUILDERCORE_T4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01_H
#ifndef CONFIGUREDTASKAWAITER_TF1AAA16B8A1250CA037E32157A3424CD2BA47874_H
#define CONFIGUREDTASKAWAITER_TF1AAA16B8A1250CA037E32157A3424CD2BA47874_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct  ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874 
{
public:
	// System.Threading.Tasks.Task System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter::m_task
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874, ___m_task_0)); }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * get_m_task_0() const { return ___m_task_0; }
	inline Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874_marshaled_pinvoke
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
	int32_t ___m_continueOnCapturedContext_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter
struct ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874_marshaled_com
{
	Task_t1F48C203E163126EBC69ACCA679D1A462DEE9EB2 * ___m_task_0;
	int32_t ___m_continueOnCapturedContext_1;
};
#endif // CONFIGUREDTASKAWAITER_TF1AAA16B8A1250CA037E32157A3424CD2BA47874_H
#ifndef CONFIGUREDTASKAWAITER_T27ECAA4B74502BD3AC7E68F47088D46DAA13D133_H
#define CONFIGUREDTASKAWAITER_T27ECAA4B74502BD3AC7E68F47088D46DAA13D133_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.IO.Stream>
struct  ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133, ___m_task_0)); }
	inline Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_tAA7557E5A6A8B2C89E44907CB5B0502A618E9D01 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T27ECAA4B74502BD3AC7E68F47088D46DAA13D133_H
#ifndef CONFIGUREDTASKAWAITER_T88324A1949AEB8DD9108467C8C9B3D027A4C570D_H
#define CONFIGUREDTASKAWAITER_T88324A1949AEB8DD9108467C8C9B3D027A4C570D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.Http.HttpResponseMessage>
struct  ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D, ___m_task_0)); }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_T88324A1949AEB8DD9108467C8C9B3D027A4C570D_H
#ifndef CONFIGUREDTASKAWAITER_TC95EBA3D9B77BA07739749A814CFE48A5E66FAEE_H
#define CONFIGUREDTASKAWAITER_TC95EBA3D9B77BA07739749A814CFE48A5E66FAEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.WebResponse>
struct  ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_task
	Task_1_t59405D5FAC70EF0481DA7783410224C07E3C9C74 * ___m_task_0;
	// System.Boolean System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter::m_continueOnCapturedContext
	bool ___m_continueOnCapturedContext_1;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE, ___m_task_0)); }
	inline Task_1_t59405D5FAC70EF0481DA7783410224C07E3C9C74 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t59405D5FAC70EF0481DA7783410224C07E3C9C74 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t59405D5FAC70EF0481DA7783410224C07E3C9C74 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}

	inline static int32_t get_offset_of_m_continueOnCapturedContext_1() { return static_cast<int32_t>(offsetof(ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE, ___m_continueOnCapturedContext_1)); }
	inline bool get_m_continueOnCapturedContext_1() const { return ___m_continueOnCapturedContext_1; }
	inline bool* get_address_of_m_continueOnCapturedContext_1() { return &___m_continueOnCapturedContext_1; }
	inline void set_m_continueOnCapturedContext_1(bool value)
	{
		___m_continueOnCapturedContext_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONFIGUREDTASKAWAITER_TC95EBA3D9B77BA07739749A814CFE48A5E66FAEE_H
#ifndef TASKAWAITER_1_T0CD71BC02837B0D53252196D19DABDA73A615B86_H
#define TASKAWAITER_1_T0CD71BC02837B0D53252196D19DABDA73A615B86_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.TaskAwaiter`1<System.Net.Http.HttpResponseMessage>
struct  TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86 
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.TaskAwaiter`1::m_task
	Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * ___m_task_0;

public:
	inline static int32_t get_offset_of_m_task_0() { return static_cast<int32_t>(offsetof(TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86, ___m_task_0)); }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * get_m_task_0() const { return ___m_task_0; }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 ** get_address_of_m_task_0() { return &___m_task_0; }
	inline void set_m_task_0(Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * value)
	{
		___m_task_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TASKAWAITER_1_T0CD71BC02837B0D53252196D19DABDA73A615B86_H
#ifndef CANCELLATIONTOKEN_T9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_H
#define CANCELLATIONTOKEN_T9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationToken
struct  CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB 
{
public:
	// System.Threading.CancellationTokenSource System.Threading.CancellationToken::m_source
	CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * ___m_source_0;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB, ___m_source_0)); }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * get_m_source_0() const { return ___m_source_0; }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_source_0), value);
	}
};

struct CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_StaticFields
{
public:
	// System.Action`1<System.Object> System.Threading.CancellationToken::s_ActionToActionObjShunt
	Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * ___s_ActionToActionObjShunt_1;

public:
	inline static int32_t get_offset_of_s_ActionToActionObjShunt_1() { return static_cast<int32_t>(offsetof(CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_StaticFields, ___s_ActionToActionObjShunt_1)); }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * get_s_ActionToActionObjShunt_1() const { return ___s_ActionToActionObjShunt_1; }
	inline Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 ** get_address_of_s_ActionToActionObjShunt_1() { return &___s_ActionToActionObjShunt_1; }
	inline void set_s_ActionToActionObjShunt_1(Action_1_t551A279CEADCF6EEAE8FA2B1E1E757D0D15290D0 * value)
	{
		___s_ActionToActionObjShunt_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_ActionToActionObjShunt_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationToken
struct CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_marshaled_pinvoke
{
	CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * ___m_source_0;
};
// Native definition for COM marshalling of System.Threading.CancellationToken
struct CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_marshaled_com
{
	CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * ___m_source_0;
};
#endif // CANCELLATIONTOKEN_T9E956952F7F20908F2AE72EDF36D97E6C7DB63AB_H
#ifndef SPARSELYPOPULATEDARRAYADDINFO_1_T0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE_H
#define SPARSELYPOPULATEDARRAYADDINFO_1_T0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.SparselyPopulatedArrayAddInfo`1<System.Threading.CancellationCallbackInfo>
struct  SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE 
{
public:
	// System.Threading.SparselyPopulatedArrayFragment`1<T> System.Threading.SparselyPopulatedArrayAddInfo`1::m_source
	SparselyPopulatedArrayFragment_1_tA54224D01E2FDC03AC2867CDDC8C53E17FA933D7 * ___m_source_0;
	// System.Int32 System.Threading.SparselyPopulatedArrayAddInfo`1::m_index
	int32_t ___m_index_1;

public:
	inline static int32_t get_offset_of_m_source_0() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE, ___m_source_0)); }
	inline SparselyPopulatedArrayFragment_1_tA54224D01E2FDC03AC2867CDDC8C53E17FA933D7 * get_m_source_0() const { return ___m_source_0; }
	inline SparselyPopulatedArrayFragment_1_tA54224D01E2FDC03AC2867CDDC8C53E17FA933D7 ** get_address_of_m_source_0() { return &___m_source_0; }
	inline void set_m_source_0(SparselyPopulatedArrayFragment_1_tA54224D01E2FDC03AC2867CDDC8C53E17FA933D7 * value)
	{
		___m_source_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_source_0), value);
	}

	inline static int32_t get_offset_of_m_index_1() { return static_cast<int32_t>(offsetof(SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE, ___m_index_1)); }
	inline int32_t get_m_index_1() const { return ___m_index_1; }
	inline int32_t* get_address_of_m_index_1() { return &___m_index_1; }
	inline void set_m_index_1(int32_t value)
	{
		___m_index_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SPARSELYPOPULATEDARRAYADDINFO_1_T0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef DATAFORMAT_T9F7B74DBD5238AF575612618966530BC2AFA1474_H
#define DATAFORMAT_T9F7B74DBD5238AF575612618966530BC2AFA1474_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.DataFormat
struct  DataFormat_t9F7B74DBD5238AF575612618966530BC2AFA1474 
{
public:
	// System.Int32 RestSharp.DataFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DataFormat_t9F7B74DBD5238AF575612618966530BC2AFA1474, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATAFORMAT_T9F7B74DBD5238AF575612618966530BC2AFA1474_H
#ifndef METHOD_T10215807550AF57F7078A1243049F302E442D274_H
#define METHOD_T10215807550AF57F7078A1243049F302E442D274_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Method
struct  Method_t10215807550AF57F7078A1243049F302E442D274 
{
public:
	// System.Int32 RestSharp.Method::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Method_t10215807550AF57F7078A1243049F302E442D274, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHOD_T10215807550AF57F7078A1243049F302E442D274_H
#ifndef PARAMETERTYPE_TD98517EAF42C7B16FFEDCA23A3C5B110E3CBD66E_H
#define PARAMETERTYPE_TD98517EAF42C7B16FFEDCA23A3C5B110E3CBD66E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.ParameterType
struct  ParameterType_tD98517EAF42C7B16FFEDCA23A3C5B110E3CBD66E 
{
public:
	// System.Int32 RestSharp.ParameterType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ParameterType_tD98517EAF42C7B16FFEDCA23A3C5B110E3CBD66E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETERTYPE_TD98517EAF42C7B16FFEDCA23A3C5B110E3CBD66E_H
#ifndef RESTCLIENT_TCA386028DE94D9A12431EF5FCE26A3961C8323F1_H
#define RESTCLIENT_TCA386028DE94D9A12431EF5FCE26A3961C8323F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestClient
struct  RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1  : public RuntimeObject
{
public:
	// RestSharp.IHttpFactory RestSharp.RestClient::HttpFactory
	RuntimeObject* ___HttpFactory_1;
	// System.Collections.Generic.IDictionary`2<System.String,RestSharp.Deserializers.IDeserializer> RestSharp.RestClient::<ContentHandlers>k__BackingField
	RuntimeObject* ___U3CContentHandlersU3Ek__BackingField_2;
	// System.Collections.Generic.IList`1<System.String> RestSharp.RestClient::<AcceptTypes>k__BackingField
	RuntimeObject* ___U3CAcceptTypesU3Ek__BackingField_3;
	// System.Collections.Generic.IList`1<RestSharp.Parameter> RestSharp.RestClient::<DefaultParameters>k__BackingField
	RuntimeObject* ___U3CDefaultParametersU3Ek__BackingField_4;
	// System.Nullable`1<System.Int32> RestSharp.RestClient::<MaxRedirects>k__BackingField
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___U3CMaxRedirectsU3Ek__BackingField_5;
	// System.Security.Cryptography.X509Certificates.X509CertificateCollection RestSharp.RestClient::<ClientCertificates>k__BackingField
	X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * ___U3CClientCertificatesU3Ek__BackingField_6;
	// System.Net.IWebProxy RestSharp.RestClient::<Proxy>k__BackingField
	RuntimeObject* ___U3CProxyU3Ek__BackingField_7;
	// System.Boolean RestSharp.RestClient::<FollowRedirects>k__BackingField
	bool ___U3CFollowRedirectsU3Ek__BackingField_8;
	// System.Net.CookieContainer RestSharp.RestClient::<CookieContainer>k__BackingField
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * ___U3CCookieContainerU3Ek__BackingField_9;
	// System.String RestSharp.RestClient::<UserAgent>k__BackingField
	String_t* ___U3CUserAgentU3Ek__BackingField_10;
	// System.Int32 RestSharp.RestClient::<Timeout>k__BackingField
	int32_t ___U3CTimeoutU3Ek__BackingField_11;
	// System.Boolean RestSharp.RestClient::<UseSynchronizationContext>k__BackingField
	bool ___U3CUseSynchronizationContextU3Ek__BackingField_12;
	// RestSharp.IAuthenticator RestSharp.RestClient::<Authenticator>k__BackingField
	RuntimeObject* ___U3CAuthenticatorU3Ek__BackingField_13;
	// System.String RestSharp.RestClient::_baseUrl
	String_t* ____baseUrl_14;

public:
	inline static int32_t get_offset_of_HttpFactory_1() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___HttpFactory_1)); }
	inline RuntimeObject* get_HttpFactory_1() const { return ___HttpFactory_1; }
	inline RuntimeObject** get_address_of_HttpFactory_1() { return &___HttpFactory_1; }
	inline void set_HttpFactory_1(RuntimeObject* value)
	{
		___HttpFactory_1 = value;
		Il2CppCodeGenWriteBarrier((&___HttpFactory_1), value);
	}

	inline static int32_t get_offset_of_U3CContentHandlersU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CContentHandlersU3Ek__BackingField_2)); }
	inline RuntimeObject* get_U3CContentHandlersU3Ek__BackingField_2() const { return ___U3CContentHandlersU3Ek__BackingField_2; }
	inline RuntimeObject** get_address_of_U3CContentHandlersU3Ek__BackingField_2() { return &___U3CContentHandlersU3Ek__BackingField_2; }
	inline void set_U3CContentHandlersU3Ek__BackingField_2(RuntimeObject* value)
	{
		___U3CContentHandlersU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentHandlersU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CAcceptTypesU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CAcceptTypesU3Ek__BackingField_3)); }
	inline RuntimeObject* get_U3CAcceptTypesU3Ek__BackingField_3() const { return ___U3CAcceptTypesU3Ek__BackingField_3; }
	inline RuntimeObject** get_address_of_U3CAcceptTypesU3Ek__BackingField_3() { return &___U3CAcceptTypesU3Ek__BackingField_3; }
	inline void set_U3CAcceptTypesU3Ek__BackingField_3(RuntimeObject* value)
	{
		___U3CAcceptTypesU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAcceptTypesU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CDefaultParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CDefaultParametersU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CDefaultParametersU3Ek__BackingField_4() const { return ___U3CDefaultParametersU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CDefaultParametersU3Ek__BackingField_4() { return &___U3CDefaultParametersU3Ek__BackingField_4; }
	inline void set_U3CDefaultParametersU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CDefaultParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDefaultParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CMaxRedirectsU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CMaxRedirectsU3Ek__BackingField_5)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_U3CMaxRedirectsU3Ek__BackingField_5() const { return ___U3CMaxRedirectsU3Ek__BackingField_5; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_U3CMaxRedirectsU3Ek__BackingField_5() { return &___U3CMaxRedirectsU3Ek__BackingField_5; }
	inline void set_U3CMaxRedirectsU3Ek__BackingField_5(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___U3CMaxRedirectsU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CClientCertificatesU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CClientCertificatesU3Ek__BackingField_6)); }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * get_U3CClientCertificatesU3Ek__BackingField_6() const { return ___U3CClientCertificatesU3Ek__BackingField_6; }
	inline X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 ** get_address_of_U3CClientCertificatesU3Ek__BackingField_6() { return &___U3CClientCertificatesU3Ek__BackingField_6; }
	inline void set_U3CClientCertificatesU3Ek__BackingField_6(X509CertificateCollection_t824A6C58D0D1B4A7CAE30F26CE8EE4B23A8A1833 * value)
	{
		___U3CClientCertificatesU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CClientCertificatesU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CProxyU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CProxyU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CProxyU3Ek__BackingField_7() const { return ___U3CProxyU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CProxyU3Ek__BackingField_7() { return &___U3CProxyU3Ek__BackingField_7; }
	inline void set_U3CProxyU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CProxyU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CProxyU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CFollowRedirectsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CFollowRedirectsU3Ek__BackingField_8)); }
	inline bool get_U3CFollowRedirectsU3Ek__BackingField_8() const { return ___U3CFollowRedirectsU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CFollowRedirectsU3Ek__BackingField_8() { return &___U3CFollowRedirectsU3Ek__BackingField_8; }
	inline void set_U3CFollowRedirectsU3Ek__BackingField_8(bool value)
	{
		___U3CFollowRedirectsU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CCookieContainerU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CCookieContainerU3Ek__BackingField_9)); }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * get_U3CCookieContainerU3Ek__BackingField_9() const { return ___U3CCookieContainerU3Ek__BackingField_9; }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 ** get_address_of_U3CCookieContainerU3Ek__BackingField_9() { return &___U3CCookieContainerU3Ek__BackingField_9; }
	inline void set_U3CCookieContainerU3Ek__BackingField_9(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * value)
	{
		___U3CCookieContainerU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCookieContainerU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CUserAgentU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CUserAgentU3Ek__BackingField_10)); }
	inline String_t* get_U3CUserAgentU3Ek__BackingField_10() const { return ___U3CUserAgentU3Ek__BackingField_10; }
	inline String_t** get_address_of_U3CUserAgentU3Ek__BackingField_10() { return &___U3CUserAgentU3Ek__BackingField_10; }
	inline void set_U3CUserAgentU3Ek__BackingField_10(String_t* value)
	{
		___U3CUserAgentU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CUserAgentU3Ek__BackingField_10), value);
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CTimeoutU3Ek__BackingField_11)); }
	inline int32_t get_U3CTimeoutU3Ek__BackingField_11() const { return ___U3CTimeoutU3Ek__BackingField_11; }
	inline int32_t* get_address_of_U3CTimeoutU3Ek__BackingField_11() { return &___U3CTimeoutU3Ek__BackingField_11; }
	inline void set_U3CTimeoutU3Ek__BackingField_11(int32_t value)
	{
		___U3CTimeoutU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CUseSynchronizationContextU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CUseSynchronizationContextU3Ek__BackingField_12)); }
	inline bool get_U3CUseSynchronizationContextU3Ek__BackingField_12() const { return ___U3CUseSynchronizationContextU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CUseSynchronizationContextU3Ek__BackingField_12() { return &___U3CUseSynchronizationContextU3Ek__BackingField_12; }
	inline void set_U3CUseSynchronizationContextU3Ek__BackingField_12(bool value)
	{
		___U3CUseSynchronizationContextU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CAuthenticatorU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ___U3CAuthenticatorU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CAuthenticatorU3Ek__BackingField_13() const { return ___U3CAuthenticatorU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CAuthenticatorU3Ek__BackingField_13() { return &___U3CAuthenticatorU3Ek__BackingField_13; }
	inline void set_U3CAuthenticatorU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CAuthenticatorU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAuthenticatorU3Ek__BackingField_13), value);
	}

	inline static int32_t get_offset_of__baseUrl_14() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1, ____baseUrl_14)); }
	inline String_t* get__baseUrl_14() const { return ____baseUrl_14; }
	inline String_t** get_address_of__baseUrl_14() { return &____baseUrl_14; }
	inline void set__baseUrl_14(String_t* value)
	{
		____baseUrl_14 = value;
		Il2CppCodeGenWriteBarrier((&____baseUrl_14), value);
	}
};

struct RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields
{
public:
	// System.Version RestSharp.RestClient::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_0;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache0
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache0_15;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache1
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache1_16;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache2
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache2_17;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache3
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache3_18;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache4
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache4_19;
	// System.Func`2<RestSharp.Parameter,RestSharp.HttpHeader> RestSharp.RestClient::<>f__am$cache5
	Func_2_tE3E70122BCD5867515B9E55A3AE0B39D8029BF8A * ___U3CU3Ef__amU24cache5_20;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache6
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache6_21;
	// System.Func`2<RestSharp.Parameter,RestSharp.HttpCookie> RestSharp.RestClient::<>f__am$cache7
	Func_2_tB4FF97CB142B16128A9141A9657DFAFD5FF80C05 * ___U3CU3Ef__amU24cache7_22;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cache8
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cache8_23;
	// System.Func`2<RestSharp.Parameter,RestSharp.HttpParameter> RestSharp.RestClient::<>f__am$cache9
	Func_2_tBE56C3ADEEA7CFE6F702ACFED4B05D7983C8A47C * ___U3CU3Ef__amU24cache9_24;
	// System.Func`2<RestSharp.Parameter,System.Boolean> RestSharp.RestClient::<>f__am$cacheA
	Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * ___U3CU3Ef__amU24cacheA_25;
	// System.Func`4<RestSharp.IHttp,System.Action`1<RestSharp.HttpResponse>,System.String,System.Net.HttpWebRequest> RestSharp.RestClient::<>f__mg$cache0
	Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 * ___U3CU3Ef__mgU24cache0_26;
	// System.Func`4<RestSharp.IHttp,System.Action`1<RestSharp.HttpResponse>,System.String,System.Net.HttpWebRequest> RestSharp.RestClient::<>f__mg$cache1
	Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 * ___U3CU3Ef__mgU24cache1_27;

public:
	inline static int32_t get_offset_of_version_0() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___version_0)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_0() const { return ___version_0; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_0() { return &___version_0; }
	inline void set_version_0(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_0 = value;
		Il2CppCodeGenWriteBarrier((&___version_0), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_15() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache0_15)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache0_15() const { return ___U3CU3Ef__amU24cache0_15; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache0_15() { return &___U3CU3Ef__amU24cache0_15; }
	inline void set_U3CU3Ef__amU24cache0_15(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache0_15 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_15), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache1_16() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache1_16)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache1_16() const { return ___U3CU3Ef__amU24cache1_16; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache1_16() { return &___U3CU3Ef__amU24cache1_16; }
	inline void set_U3CU3Ef__amU24cache1_16(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache1_16 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache1_16), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache2_17() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache2_17)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache2_17() const { return ___U3CU3Ef__amU24cache2_17; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache2_17() { return &___U3CU3Ef__amU24cache2_17; }
	inline void set_U3CU3Ef__amU24cache2_17(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache2_17 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache2_17), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache3_18() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache3_18)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache3_18() const { return ___U3CU3Ef__amU24cache3_18; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache3_18() { return &___U3CU3Ef__amU24cache3_18; }
	inline void set_U3CU3Ef__amU24cache3_18(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache3_18 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache3_18), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache4_19() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache4_19)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache4_19() const { return ___U3CU3Ef__amU24cache4_19; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache4_19() { return &___U3CU3Ef__amU24cache4_19; }
	inline void set_U3CU3Ef__amU24cache4_19(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache4_19 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache4_19), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache5_20() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache5_20)); }
	inline Func_2_tE3E70122BCD5867515B9E55A3AE0B39D8029BF8A * get_U3CU3Ef__amU24cache5_20() const { return ___U3CU3Ef__amU24cache5_20; }
	inline Func_2_tE3E70122BCD5867515B9E55A3AE0B39D8029BF8A ** get_address_of_U3CU3Ef__amU24cache5_20() { return &___U3CU3Ef__amU24cache5_20; }
	inline void set_U3CU3Ef__amU24cache5_20(Func_2_tE3E70122BCD5867515B9E55A3AE0B39D8029BF8A * value)
	{
		___U3CU3Ef__amU24cache5_20 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache5_20), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache6_21() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache6_21)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache6_21() const { return ___U3CU3Ef__amU24cache6_21; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache6_21() { return &___U3CU3Ef__amU24cache6_21; }
	inline void set_U3CU3Ef__amU24cache6_21(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache6_21 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache6_21), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache7_22() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache7_22)); }
	inline Func_2_tB4FF97CB142B16128A9141A9657DFAFD5FF80C05 * get_U3CU3Ef__amU24cache7_22() const { return ___U3CU3Ef__amU24cache7_22; }
	inline Func_2_tB4FF97CB142B16128A9141A9657DFAFD5FF80C05 ** get_address_of_U3CU3Ef__amU24cache7_22() { return &___U3CU3Ef__amU24cache7_22; }
	inline void set_U3CU3Ef__amU24cache7_22(Func_2_tB4FF97CB142B16128A9141A9657DFAFD5FF80C05 * value)
	{
		___U3CU3Ef__amU24cache7_22 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache7_22), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache8_23() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache8_23)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cache8_23() const { return ___U3CU3Ef__amU24cache8_23; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cache8_23() { return &___U3CU3Ef__amU24cache8_23; }
	inline void set_U3CU3Ef__amU24cache8_23(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cache8_23 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache8_23), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cache9_24() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cache9_24)); }
	inline Func_2_tBE56C3ADEEA7CFE6F702ACFED4B05D7983C8A47C * get_U3CU3Ef__amU24cache9_24() const { return ___U3CU3Ef__amU24cache9_24; }
	inline Func_2_tBE56C3ADEEA7CFE6F702ACFED4B05D7983C8A47C ** get_address_of_U3CU3Ef__amU24cache9_24() { return &___U3CU3Ef__amU24cache9_24; }
	inline void set_U3CU3Ef__amU24cache9_24(Func_2_tBE56C3ADEEA7CFE6F702ACFED4B05D7983C8A47C * value)
	{
		___U3CU3Ef__amU24cache9_24 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache9_24), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__amU24cacheA_25() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__amU24cacheA_25)); }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * get_U3CU3Ef__amU24cacheA_25() const { return ___U3CU3Ef__amU24cacheA_25; }
	inline Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 ** get_address_of_U3CU3Ef__amU24cacheA_25() { return &___U3CU3Ef__amU24cacheA_25; }
	inline void set_U3CU3Ef__amU24cacheA_25(Func_2_t626B1D1F3C495A55E1177C8DFC962C476D706A19 * value)
	{
		___U3CU3Ef__amU24cacheA_25 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cacheA_25), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache0_26() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__mgU24cache0_26)); }
	inline Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 * get_U3CU3Ef__mgU24cache0_26() const { return ___U3CU3Ef__mgU24cache0_26; }
	inline Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 ** get_address_of_U3CU3Ef__mgU24cache0_26() { return &___U3CU3Ef__mgU24cache0_26; }
	inline void set_U3CU3Ef__mgU24cache0_26(Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 * value)
	{
		___U3CU3Ef__mgU24cache0_26 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache0_26), value);
	}

	inline static int32_t get_offset_of_U3CU3Ef__mgU24cache1_27() { return static_cast<int32_t>(offsetof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields, ___U3CU3Ef__mgU24cache1_27)); }
	inline Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 * get_U3CU3Ef__mgU24cache1_27() const { return ___U3CU3Ef__mgU24cache1_27; }
	inline Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 ** get_address_of_U3CU3Ef__mgU24cache1_27() { return &___U3CU3Ef__mgU24cache1_27; }
	inline void set_U3CU3Ef__mgU24cache1_27(Func_4_t7F9956F2A48040B5008E4EF3E4B9DA4D74C02C22 * value)
	{
		___U3CU3Ef__mgU24cache1_27 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__mgU24cache1_27), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTCLIENT_TCA386028DE94D9A12431EF5FCE26A3961C8323F1_H
#ifndef RESTRESPONSECOOKIE_T24C3016B845BC02B5D718305542C1B3D18799ED7_H
#define RESTRESPONSECOOKIE_T24C3016B845BC02B5D718305542C1B3D18799ED7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestResponseCookie
struct  RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7  : public RuntimeObject
{
public:
	// System.String RestSharp.RestResponseCookie::<Comment>k__BackingField
	String_t* ___U3CCommentU3Ek__BackingField_0;
	// System.Uri RestSharp.RestResponseCookie::<CommentUri>k__BackingField
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___U3CCommentUriU3Ek__BackingField_1;
	// System.Boolean RestSharp.RestResponseCookie::<Discard>k__BackingField
	bool ___U3CDiscardU3Ek__BackingField_2;
	// System.String RestSharp.RestResponseCookie::<Domain>k__BackingField
	String_t* ___U3CDomainU3Ek__BackingField_3;
	// System.Boolean RestSharp.RestResponseCookie::<Expired>k__BackingField
	bool ___U3CExpiredU3Ek__BackingField_4;
	// System.DateTime RestSharp.RestResponseCookie::<Expires>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CExpiresU3Ek__BackingField_5;
	// System.Boolean RestSharp.RestResponseCookie::<HttpOnly>k__BackingField
	bool ___U3CHttpOnlyU3Ek__BackingField_6;
	// System.String RestSharp.RestResponseCookie::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_7;
	// System.String RestSharp.RestResponseCookie::<Path>k__BackingField
	String_t* ___U3CPathU3Ek__BackingField_8;
	// System.String RestSharp.RestResponseCookie::<Port>k__BackingField
	String_t* ___U3CPortU3Ek__BackingField_9;
	// System.Boolean RestSharp.RestResponseCookie::<Secure>k__BackingField
	bool ___U3CSecureU3Ek__BackingField_10;
	// System.DateTime RestSharp.RestResponseCookie::<TimeStamp>k__BackingField
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___U3CTimeStampU3Ek__BackingField_11;
	// System.String RestSharp.RestResponseCookie::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_12;
	// System.Int32 RestSharp.RestResponseCookie::<Version>k__BackingField
	int32_t ___U3CVersionU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CCommentU3Ek__BackingField_0)); }
	inline String_t* get_U3CCommentU3Ek__BackingField_0() const { return ___U3CCommentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CCommentU3Ek__BackingField_0() { return &___U3CCommentU3Ek__BackingField_0; }
	inline void set_U3CCommentU3Ek__BackingField_0(String_t* value)
	{
		___U3CCommentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCommentUriU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CCommentUriU3Ek__BackingField_1)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_U3CCommentUriU3Ek__BackingField_1() const { return ___U3CCommentUriU3Ek__BackingField_1; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_U3CCommentUriU3Ek__BackingField_1() { return &___U3CCommentUriU3Ek__BackingField_1; }
	inline void set_U3CCommentUriU3Ek__BackingField_1(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___U3CCommentUriU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCommentUriU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CDiscardU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CDiscardU3Ek__BackingField_2)); }
	inline bool get_U3CDiscardU3Ek__BackingField_2() const { return ___U3CDiscardU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CDiscardU3Ek__BackingField_2() { return &___U3CDiscardU3Ek__BackingField_2; }
	inline void set_U3CDiscardU3Ek__BackingField_2(bool value)
	{
		___U3CDiscardU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CDomainU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CDomainU3Ek__BackingField_3)); }
	inline String_t* get_U3CDomainU3Ek__BackingField_3() const { return ___U3CDomainU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CDomainU3Ek__BackingField_3() { return &___U3CDomainU3Ek__BackingField_3; }
	inline void set_U3CDomainU3Ek__BackingField_3(String_t* value)
	{
		___U3CDomainU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CDomainU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CExpiredU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CExpiredU3Ek__BackingField_4)); }
	inline bool get_U3CExpiredU3Ek__BackingField_4() const { return ___U3CExpiredU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CExpiredU3Ek__BackingField_4() { return &___U3CExpiredU3Ek__BackingField_4; }
	inline void set_U3CExpiredU3Ek__BackingField_4(bool value)
	{
		___U3CExpiredU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CExpiresU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CExpiresU3Ek__BackingField_5)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CExpiresU3Ek__BackingField_5() const { return ___U3CExpiresU3Ek__BackingField_5; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CExpiresU3Ek__BackingField_5() { return &___U3CExpiresU3Ek__BackingField_5; }
	inline void set_U3CExpiresU3Ek__BackingField_5(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CExpiresU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CHttpOnlyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CHttpOnlyU3Ek__BackingField_6)); }
	inline bool get_U3CHttpOnlyU3Ek__BackingField_6() const { return ___U3CHttpOnlyU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CHttpOnlyU3Ek__BackingField_6() { return &___U3CHttpOnlyU3Ek__BackingField_6; }
	inline void set_U3CHttpOnlyU3Ek__BackingField_6(bool value)
	{
		___U3CHttpOnlyU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CNameU3Ek__BackingField_7)); }
	inline String_t* get_U3CNameU3Ek__BackingField_7() const { return ___U3CNameU3Ek__BackingField_7; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_7() { return &___U3CNameU3Ek__BackingField_7; }
	inline void set_U3CNameU3Ek__BackingField_7(String_t* value)
	{
		___U3CNameU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CPathU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CPathU3Ek__BackingField_8)); }
	inline String_t* get_U3CPathU3Ek__BackingField_8() const { return ___U3CPathU3Ek__BackingField_8; }
	inline String_t** get_address_of_U3CPathU3Ek__BackingField_8() { return &___U3CPathU3Ek__BackingField_8; }
	inline void set_U3CPathU3Ek__BackingField_8(String_t* value)
	{
		___U3CPathU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPathU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CPortU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CPortU3Ek__BackingField_9)); }
	inline String_t* get_U3CPortU3Ek__BackingField_9() const { return ___U3CPortU3Ek__BackingField_9; }
	inline String_t** get_address_of_U3CPortU3Ek__BackingField_9() { return &___U3CPortU3Ek__BackingField_9; }
	inline void set_U3CPortU3Ek__BackingField_9(String_t* value)
	{
		___U3CPortU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPortU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CSecureU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CSecureU3Ek__BackingField_10)); }
	inline bool get_U3CSecureU3Ek__BackingField_10() const { return ___U3CSecureU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CSecureU3Ek__BackingField_10() { return &___U3CSecureU3Ek__BackingField_10; }
	inline void set_U3CSecureU3Ek__BackingField_10(bool value)
	{
		___U3CSecureU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3CTimeStampU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CTimeStampU3Ek__BackingField_11)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_U3CTimeStampU3Ek__BackingField_11() const { return ___U3CTimeStampU3Ek__BackingField_11; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_U3CTimeStampU3Ek__BackingField_11() { return &___U3CTimeStampU3Ek__BackingField_11; }
	inline void set_U3CTimeStampU3Ek__BackingField_11(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___U3CTimeStampU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CValueU3Ek__BackingField_12)); }
	inline String_t* get_U3CValueU3Ek__BackingField_12() const { return ___U3CValueU3Ek__BackingField_12; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_12() { return &___U3CValueU3Ek__BackingField_12; }
	inline void set_U3CValueU3Ek__BackingField_12(String_t* value)
	{
		___U3CValueU3Ek__BackingField_12 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_12), value);
	}

	inline static int32_t get_offset_of_U3CVersionU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7, ___U3CVersionU3Ek__BackingField_13)); }
	inline int32_t get_U3CVersionU3Ek__BackingField_13() const { return ___U3CVersionU3Ek__BackingField_13; }
	inline int32_t* get_address_of_U3CVersionU3Ek__BackingField_13() { return &___U3CVersionU3Ek__BackingField_13; }
	inline void set_U3CVersionU3Ek__BackingField_13(int32_t value)
	{
		___U3CVersionU3Ek__BackingField_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRESPONSECOOKIE_T24C3016B845BC02B5D718305542C1B3D18799ED7_H
#ifndef NAMESTYLE_TAE11853F499C718E4E4C5B55C92D56F84F28747E_H
#define NAMESTYLE_TAE11853F499C718E4E4C5B55C92D56F84F28747E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Serializers.NameStyle
struct  NameStyle_tAE11853F499C718E4E4C5B55C92D56F84F28747E 
{
public:
	// System.Int32 RestSharp.Serializers.NameStyle::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(NameStyle_tAE11853F499C718E4E4C5B55C92D56F84F28747E, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESTYLE_TAE11853F499C718E4E4C5B55C92D56F84F28747E_H
#ifndef ENUMERATOR_T9F6648C781C37CAB33B771059769176CAE5AB6FE_H
#define ENUMERATOR_T9F6648C781C37CAB33B771059769176CAE5AB6FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Net.Http.Headers.HttpHeaders/HeaderBucket>
struct  Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE 
{
public:
	// System.Collections.Generic.Dictionary`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::dictionary
	Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * ___dictionary_0;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::version
	int32_t ___version_1;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::index
	int32_t ___index_2;
	// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator::current
	KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768  ___current_3;
	// System.Int32 System.Collections.Generic.Dictionary`2/Enumerator::getEnumeratorRetType
	int32_t ___getEnumeratorRetType_4;

public:
	inline static int32_t get_offset_of_dictionary_0() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___dictionary_0)); }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * get_dictionary_0() const { return ___dictionary_0; }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 ** get_address_of_dictionary_0() { return &___dictionary_0; }
	inline void set_dictionary_0(Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * value)
	{
		___dictionary_0 = value;
		Il2CppCodeGenWriteBarrier((&___dictionary_0), value);
	}

	inline static int32_t get_offset_of_version_1() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___version_1)); }
	inline int32_t get_version_1() const { return ___version_1; }
	inline int32_t* get_address_of_version_1() { return &___version_1; }
	inline void set_version_1(int32_t value)
	{
		___version_1 = value;
	}

	inline static int32_t get_offset_of_index_2() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___index_2)); }
	inline int32_t get_index_2() const { return ___index_2; }
	inline int32_t* get_address_of_index_2() { return &___index_2; }
	inline void set_index_2(int32_t value)
	{
		___index_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___current_3)); }
	inline KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768  get_current_3() const { return ___current_3; }
	inline KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768 * get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(KeyValuePair_2_tBBB4C8437E21CB55FCB1A1B93B5440D235FB3768  value)
	{
		___current_3 = value;
	}

	inline static int32_t get_offset_of_getEnumeratorRetType_4() { return static_cast<int32_t>(offsetof(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE, ___getEnumeratorRetType_4)); }
	inline int32_t get_getEnumeratorRetType_4() const { return ___getEnumeratorRetType_4; }
	inline int32_t* get_address_of_getEnumeratorRetType_4() { return &___getEnumeratorRetType_4; }
	inline void set_getEnumeratorRetType_4(int32_t value)
	{
		___getEnumeratorRetType_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_T9F6648C781C37CAB33B771059769176CAE5AB6FE_H
#ifndef DATETIMEOFFSET_T6C333873402CAD576160B4F8E159EB6834F06B85_H
#define DATETIMEOFFSET_T6C333873402CAD576160B4F8E159EB6834F06B85_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.DateTimeOffset
struct  DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 
{
public:
	// System.DateTime System.DateTimeOffset::m_dateTime
	DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  ___m_dateTime_2;
	// System.Int16 System.DateTimeOffset::m_offsetMinutes
	int16_t ___m_offsetMinutes_3;

public:
	inline static int32_t get_offset_of_m_dateTime_2() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85, ___m_dateTime_2)); }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  get_m_dateTime_2() const { return ___m_dateTime_2; }
	inline DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132 * get_address_of_m_dateTime_2() { return &___m_dateTime_2; }
	inline void set_m_dateTime_2(DateTime_t349B7449FBAAFF4192636E2B7A07694DA9236132  value)
	{
		___m_dateTime_2 = value;
	}

	inline static int32_t get_offset_of_m_offsetMinutes_3() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85, ___m_offsetMinutes_3)); }
	inline int16_t get_m_offsetMinutes_3() const { return ___m_offsetMinutes_3; }
	inline int16_t* get_address_of_m_offsetMinutes_3() { return &___m_offsetMinutes_3; }
	inline void set_m_offsetMinutes_3(int16_t value)
	{
		___m_offsetMinutes_3 = value;
	}
};

struct DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85_StaticFields
{
public:
	// System.DateTimeOffset System.DateTimeOffset::MinValue
	DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  ___MinValue_0;
	// System.DateTimeOffset System.DateTimeOffset::MaxValue
	DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  ___MaxValue_1;

public:
	inline static int32_t get_offset_of_MinValue_0() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85_StaticFields, ___MinValue_0)); }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  get_MinValue_0() const { return ___MinValue_0; }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 * get_address_of_MinValue_0() { return &___MinValue_0; }
	inline void set_MinValue_0(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  value)
	{
		___MinValue_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85_StaticFields, ___MaxValue_1)); }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  get_MaxValue_1() const { return ___MaxValue_1; }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  value)
	{
		___MaxValue_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEOFFSET_T6C333873402CAD576160B4F8E159EB6834F06B85_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#define MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IO.MemoryStream
struct  MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C  : public Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7
{
public:
	// System.Byte[] System.IO.MemoryStream::_buffer
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____buffer_4;
	// System.Int32 System.IO.MemoryStream::_origin
	int32_t ____origin_5;
	// System.Int32 System.IO.MemoryStream::_position
	int32_t ____position_6;
	// System.Int32 System.IO.MemoryStream::_length
	int32_t ____length_7;
	// System.Int32 System.IO.MemoryStream::_capacity
	int32_t ____capacity_8;
	// System.Boolean System.IO.MemoryStream::_expandable
	bool ____expandable_9;
	// System.Boolean System.IO.MemoryStream::_writable
	bool ____writable_10;
	// System.Boolean System.IO.MemoryStream::_exposable
	bool ____exposable_11;
	// System.Boolean System.IO.MemoryStream::_isOpen
	bool ____isOpen_12;
	// System.Threading.Tasks.Task`1<System.Int32> System.IO.MemoryStream::_lastReadTask
	Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * ____lastReadTask_13;

public:
	inline static int32_t get_offset_of__buffer_4() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____buffer_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get__buffer_4() const { return ____buffer_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of__buffer_4() { return &____buffer_4; }
	inline void set__buffer_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		____buffer_4 = value;
		Il2CppCodeGenWriteBarrier((&____buffer_4), value);
	}

	inline static int32_t get_offset_of__origin_5() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____origin_5)); }
	inline int32_t get__origin_5() const { return ____origin_5; }
	inline int32_t* get_address_of__origin_5() { return &____origin_5; }
	inline void set__origin_5(int32_t value)
	{
		____origin_5 = value;
	}

	inline static int32_t get_offset_of__position_6() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____position_6)); }
	inline int32_t get__position_6() const { return ____position_6; }
	inline int32_t* get_address_of__position_6() { return &____position_6; }
	inline void set__position_6(int32_t value)
	{
		____position_6 = value;
	}

	inline static int32_t get_offset_of__length_7() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____length_7)); }
	inline int32_t get__length_7() const { return ____length_7; }
	inline int32_t* get_address_of__length_7() { return &____length_7; }
	inline void set__length_7(int32_t value)
	{
		____length_7 = value;
	}

	inline static int32_t get_offset_of__capacity_8() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____capacity_8)); }
	inline int32_t get__capacity_8() const { return ____capacity_8; }
	inline int32_t* get_address_of__capacity_8() { return &____capacity_8; }
	inline void set__capacity_8(int32_t value)
	{
		____capacity_8 = value;
	}

	inline static int32_t get_offset_of__expandable_9() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____expandable_9)); }
	inline bool get__expandable_9() const { return ____expandable_9; }
	inline bool* get_address_of__expandable_9() { return &____expandable_9; }
	inline void set__expandable_9(bool value)
	{
		____expandable_9 = value;
	}

	inline static int32_t get_offset_of__writable_10() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____writable_10)); }
	inline bool get__writable_10() const { return ____writable_10; }
	inline bool* get_address_of__writable_10() { return &____writable_10; }
	inline void set__writable_10(bool value)
	{
		____writable_10 = value;
	}

	inline static int32_t get_offset_of__exposable_11() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____exposable_11)); }
	inline bool get__exposable_11() const { return ____exposable_11; }
	inline bool* get_address_of__exposable_11() { return &____exposable_11; }
	inline void set__exposable_11(bool value)
	{
		____exposable_11 = value;
	}

	inline static int32_t get_offset_of__isOpen_12() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____isOpen_12)); }
	inline bool get__isOpen_12() const { return ____isOpen_12; }
	inline bool* get_address_of__isOpen_12() { return &____isOpen_12; }
	inline void set__isOpen_12(bool value)
	{
		____isOpen_12 = value;
	}

	inline static int32_t get_offset_of__lastReadTask_13() { return static_cast<int32_t>(offsetof(MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C, ____lastReadTask_13)); }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * get__lastReadTask_13() const { return ____lastReadTask_13; }
	inline Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 ** get_address_of__lastReadTask_13() { return &____lastReadTask_13; }
	inline void set__lastReadTask_13(Task_1_t640F0CBB720BB9CD14B90B7B81624471A9F56D87 * value)
	{
		____lastReadTask_13 = value;
		Il2CppCodeGenWriteBarrier((&____lastReadTask_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMORYSTREAM_T495F44B85E6B4DDE2BB7E17DE963256A74E2298C_H
#ifndef DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#define DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.DecompressionMethods
struct  DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D 
{
public:
	// System.Int32 System.Net.DecompressionMethods::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DecompressionMethods_t828950DA24A3D2B4A635E51125685CDB629ED51D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECOMPRESSIONMETHODS_T828950DA24A3D2B4A635E51125685CDB629ED51D_H
#ifndef CONTENTRANGEHEADERVALUE_T7910E521DFA28C03F4E0D9C89D87232B6882D11E_H
#define CONTENTRANGEHEADERVALUE_T7910E521DFA28C03F4E0D9C89D87232B6882D11E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.ContentRangeHeaderValue
struct  ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.ContentRangeHeaderValue::unit
	String_t* ___unit_0;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.ContentRangeHeaderValue::<From>k__BackingField
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___U3CFromU3Ek__BackingField_1;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.ContentRangeHeaderValue::<Length>k__BackingField
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___U3CLengthU3Ek__BackingField_2;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.ContentRangeHeaderValue::<To>k__BackingField
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___U3CToU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_unit_0() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___unit_0)); }
	inline String_t* get_unit_0() const { return ___unit_0; }
	inline String_t** get_address_of_unit_0() { return &___unit_0; }
	inline void set_unit_0(String_t* value)
	{
		___unit_0 = value;
		Il2CppCodeGenWriteBarrier((&___unit_0), value);
	}

	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___U3CFromU3Ek__BackingField_1)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_U3CFromU3Ek__BackingField_1() const { return ___U3CFromU3Ek__BackingField_1; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_U3CFromU3Ek__BackingField_1() { return &___U3CFromU3Ek__BackingField_1; }
	inline void set_U3CFromU3Ek__BackingField_1(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___U3CFromU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CLengthU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___U3CLengthU3Ek__BackingField_2)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_U3CLengthU3Ek__BackingField_2() const { return ___U3CLengthU3Ek__BackingField_2; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_U3CLengthU3Ek__BackingField_2() { return &___U3CLengthU3Ek__BackingField_2; }
	inline void set_U3CLengthU3Ek__BackingField_2(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___U3CLengthU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E, ___U3CToU3Ek__BackingField_3)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_U3CToU3Ek__BackingField_3() const { return ___U3CToU3Ek__BackingField_3; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_U3CToU3Ek__BackingField_3() { return &___U3CToU3Ek__BackingField_3; }
	inline void set_U3CToU3Ek__BackingField_3(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___U3CToU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTENTRANGEHEADERVALUE_T7910E521DFA28C03F4E0D9C89D87232B6882D11E_H
#ifndef HTTPHEADERKIND_TB1DD187D27BCE76C6EC20628350D06F1802F9F97_H
#define HTTPHEADERKIND_TB1DD187D27BCE76C6EC20628350D06F1802F9F97_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaderKind
struct  HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97 
{
public:
	// System.Int32 System.Net.Http.Headers.HttpHeaderKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERKIND_TB1DD187D27BCE76C6EC20628350D06F1802F9F97_H
#ifndef RANGEITEMHEADERVALUE_T24C458BCBACB895AC9E6403E55B458559124D664_H
#define RANGEITEMHEADERVALUE_T24C458BCBACB895AC9E6403E55B458559124D664_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RangeItemHeaderValue
struct  RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.RangeItemHeaderValue::<From>k__BackingField
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___U3CFromU3Ek__BackingField_0;
	// System.Nullable`1<System.Int64> System.Net.Http.Headers.RangeItemHeaderValue::<To>k__BackingField
	Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  ___U3CToU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFromU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664, ___U3CFromU3Ek__BackingField_0)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_U3CFromU3Ek__BackingField_0() const { return ___U3CFromU3Ek__BackingField_0; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_U3CFromU3Ek__BackingField_0() { return &___U3CFromU3Ek__BackingField_0; }
	inline void set_U3CFromU3Ek__BackingField_0(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___U3CFromU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CToU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664, ___U3CToU3Ek__BackingField_1)); }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  get_U3CToU3Ek__BackingField_1() const { return ___U3CToU3Ek__BackingField_1; }
	inline Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5 * get_address_of_U3CToU3Ek__BackingField_1() { return &___U3CToU3Ek__BackingField_1; }
	inline void set_U3CToU3Ek__BackingField_1(Nullable_1_t8A84BC5F3B3D55B8E96E9B812BDCCCB962EB2AB5  value)
	{
		___U3CToU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGEITEMHEADERVALUE_T24C458BCBACB895AC9E6403E55B458559124D664_H
#ifndef STRINGWITHQUALITYHEADERVALUE_T1F4318F29FD5B450365AFED6632DFC05B15F7AD8_H
#define STRINGWITHQUALITYHEADERVALUE_T1F4318F29FD5B450365AFED6632DFC05B15F7AD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.StringWithQualityHeaderValue
struct  StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Double> System.Net.Http.Headers.StringWithQualityHeaderValue::<Quality>k__BackingField
	Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  ___U3CQualityU3Ek__BackingField_0;
	// System.String System.Net.Http.Headers.StringWithQualityHeaderValue::<Value>k__BackingField
	String_t* ___U3CValueU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CQualityU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8, ___U3CQualityU3Ek__BackingField_0)); }
	inline Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  get_U3CQualityU3Ek__BackingField_0() const { return ___U3CQualityU3Ek__BackingField_0; }
	inline Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF * get_address_of_U3CQualityU3Ek__BackingField_0() { return &___U3CQualityU3Ek__BackingField_0; }
	inline void set_U3CQualityU3Ek__BackingField_0(Nullable_1_tBFE8022F4FD60FAE6E29634776F92F0C7DDF0ECF  value)
	{
		___U3CQualityU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8, ___U3CValueU3Ek__BackingField_1)); }
	inline String_t* get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline String_t** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(String_t* value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGWITHQUALITYHEADERVALUE_T1F4318F29FD5B450365AFED6632DFC05B15F7AD8_H
#ifndef TYPE_TEBD57B8CE42E0A8AA52CACE5469267D06FB52D72_H
#define TYPE_TEBD57B8CE42E0A8AA52CACE5469267D06FB52D72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Token/Type
struct  Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72 
{
public:
	// System.Int32 System.Net.Http.Headers.Token/Type::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPE_TEBD57B8CE42E0A8AA52CACE5469267D06FB52D72_H
#ifndef HTTPCOMPLETIONOPTION_T58CEC23E366EE12A978C957DE797B907F82ACE0B_H
#define HTTPCOMPLETIONOPTION_T58CEC23E366EE12A978C957DE797B907F82ACE0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpCompletionOption
struct  HttpCompletionOption_t58CEC23E366EE12A978C957DE797B907F82ACE0B 
{
public:
	// System.Int32 System.Net.Http.HttpCompletionOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpCompletionOption_t58CEC23E366EE12A978C957DE797B907F82ACE0B, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCOMPLETIONOPTION_T58CEC23E366EE12A978C957DE797B907F82ACE0B_H
#ifndef STREAMCONTENT_T0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF_H
#define STREAMCONTENT_T0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.StreamContent
struct  StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF  : public HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D
{
public:
	// System.IO.Stream System.Net.Http.StreamContent::content
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___content_3;
	// System.Int32 System.Net.Http.StreamContent::bufferSize
	int32_t ___bufferSize_4;
	// System.Threading.CancellationToken System.Net.Http.StreamContent::cancellationToken
	CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  ___cancellationToken_5;
	// System.Int64 System.Net.Http.StreamContent::startPosition
	int64_t ___startPosition_6;
	// System.Boolean System.Net.Http.StreamContent::contentCopied
	bool ___contentCopied_7;

public:
	inline static int32_t get_offset_of_content_3() { return static_cast<int32_t>(offsetof(StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF, ___content_3)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_content_3() const { return ___content_3; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_content_3() { return &___content_3; }
	inline void set_content_3(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___content_3 = value;
		Il2CppCodeGenWriteBarrier((&___content_3), value);
	}

	inline static int32_t get_offset_of_bufferSize_4() { return static_cast<int32_t>(offsetof(StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF, ___bufferSize_4)); }
	inline int32_t get_bufferSize_4() const { return ___bufferSize_4; }
	inline int32_t* get_address_of_bufferSize_4() { return &___bufferSize_4; }
	inline void set_bufferSize_4(int32_t value)
	{
		___bufferSize_4 = value;
	}

	inline static int32_t get_offset_of_cancellationToken_5() { return static_cast<int32_t>(offsetof(StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF, ___cancellationToken_5)); }
	inline CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  get_cancellationToken_5() const { return ___cancellationToken_5; }
	inline CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB * get_address_of_cancellationToken_5() { return &___cancellationToken_5; }
	inline void set_cancellationToken_5(CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  value)
	{
		___cancellationToken_5 = value;
	}

	inline static int32_t get_offset_of_startPosition_6() { return static_cast<int32_t>(offsetof(StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF, ___startPosition_6)); }
	inline int64_t get_startPosition_6() const { return ___startPosition_6; }
	inline int64_t* get_address_of_startPosition_6() { return &___startPosition_6; }
	inline void set_startPosition_6(int64_t value)
	{
		___startPosition_6 = value;
	}

	inline static int32_t get_offset_of_contentCopied_7() { return static_cast<int32_t>(offsetof(StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF, ___contentCopied_7)); }
	inline bool get_contentCopied_7() const { return ___contentCopied_7; }
	inline bool* get_address_of_contentCopied_7() { return &___contentCopied_7; }
	inline void set_contentCopied_7(bool value)
	{
		___contentCopied_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STREAMCONTENT_T0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF_H
#ifndef HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#define HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.HttpStatusCode
struct  HttpStatusCode_tEEC31491D56EE5BDB252F07906878274FD22AC0C 
{
public:
	// System.Int32 System.Net.HttpStatusCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HttpStatusCode_tEEC31491D56EE5BDB252F07906878274FD22AC0C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPSTATUSCODE_TEEC31491D56EE5BDB252F07906878274FD22AC0C_H
#ifndef ASYNCTASKMETHODBUILDER_1_T0E64AA6FA425130501B776CD7A27C0B3FD630DF0_H
#define ASYNCTASKMETHODBUILDER_1_T0E64AA6FA425130501B776CD7A27C0B3FD630DF0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Net.Http.HttpResponseMessage>
struct  AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0, ___m_task_2)); }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t519CB3A303B53176BD616AC273201D66B84FF270 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T0E64AA6FA425130501B776CD7A27C0B3FD630DF0_H
#ifndef ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#define ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult>
struct  AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 
{
public:
	// System.Runtime.CompilerServices.AsyncMethodBuilderCore System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_coreState
	AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  ___m_coreState_1;
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::m_task
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___m_task_2;

public:
	inline static int32_t get_offset_of_m_coreState_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_coreState_1)); }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  get_m_coreState_1() const { return ___m_coreState_1; }
	inline AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01 * get_address_of_m_coreState_1() { return &___m_coreState_1; }
	inline void set_m_coreState_1(AsyncMethodBuilderCore_t4CE6C1E4B0621A6EC45CF6E0E8F1F633FFF9FF01  value)
	{
		___m_coreState_1 = value;
	}

	inline static int32_t get_offset_of_m_task_2() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9, ___m_task_2)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_m_task_2() const { return ___m_task_2; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_m_task_2() { return &___m_task_2; }
	inline void set_m_task_2(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___m_task_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_task_2), value);
	}
};

struct AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<TResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1::s_defaultResultTask
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_defaultResultTask_0;

public:
	inline static int32_t get_offset_of_s_defaultResultTask_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9_StaticFields, ___s_defaultResultTask_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_defaultResultTask_0() const { return ___s_defaultResultTask_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_defaultResultTask_0() { return &___s_defaultResultTask_0; }
	inline void set_s_defaultResultTask_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_defaultResultTask_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_defaultResultTask_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASYNCTASKMETHODBUILDER_1_T66ED1808B26B8081A2804D6A750D13386E360BD9_H
#ifndef CANCELLATIONTOKENREGISTRATION_TCDB9825D1854DD0D7FF737C82B099FC468107BB2_H
#define CANCELLATIONTOKENREGISTRATION_TCDB9825D1854DD0D7FF737C82B099FC468107BB2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Threading.CancellationTokenRegistration
struct  CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2 
{
public:
	// System.Threading.CancellationCallbackInfo System.Threading.CancellationTokenRegistration::m_callbackInfo
	CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36 * ___m_callbackInfo_0;
	// System.Threading.SparselyPopulatedArrayAddInfo`1<System.Threading.CancellationCallbackInfo> System.Threading.CancellationTokenRegistration::m_registrationInfo
	SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE  ___m_registrationInfo_1;

public:
	inline static int32_t get_offset_of_m_callbackInfo_0() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2, ___m_callbackInfo_0)); }
	inline CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36 * get_m_callbackInfo_0() const { return ___m_callbackInfo_0; }
	inline CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36 ** get_address_of_m_callbackInfo_0() { return &___m_callbackInfo_0; }
	inline void set_m_callbackInfo_0(CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36 * value)
	{
		___m_callbackInfo_0 = value;
		Il2CppCodeGenWriteBarrier((&___m_callbackInfo_0), value);
	}

	inline static int32_t get_offset_of_m_registrationInfo_1() { return static_cast<int32_t>(offsetof(CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2, ___m_registrationInfo_1)); }
	inline SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE  get_m_registrationInfo_1() const { return ___m_registrationInfo_1; }
	inline SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE * get_address_of_m_registrationInfo_1() { return &___m_registrationInfo_1; }
	inline void set_m_registrationInfo_1(SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE  value)
	{
		___m_registrationInfo_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2_marshaled_pinvoke
{
	CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36 * ___m_callbackInfo_0;
	SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE  ___m_registrationInfo_1;
};
// Native definition for COM marshalling of System.Threading.CancellationTokenRegistration
struct CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2_marshaled_com
{
	CancellationCallbackInfo_t8CDEA0AA9C9D1A2321FE2F88878F4B5E0901CF36 * ___m_callbackInfo_0;
	SparselyPopulatedArrayAddInfo_1_t0A76BDD84EBF76BEF894419FC221D25BB3D4FBEE  ___m_registrationInfo_1;
};
#endif // CANCELLATIONTOKENREGISTRATION_TCDB9825D1854DD0D7FF737C82B099FC468107BB2_H
#ifndef TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#define TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.TimeSpan
struct  TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 
{
public:
	// System.Int64 System.TimeSpan::_ticks
	int64_t ____ticks_3;

public:
	inline static int32_t get_offset_of__ticks_3() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4, ____ticks_3)); }
	inline int64_t get__ticks_3() const { return ____ticks_3; }
	inline int64_t* get_address_of__ticks_3() { return &____ticks_3; }
	inline void set__ticks_3(int64_t value)
	{
		____ticks_3 = value;
	}
};

struct TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields
{
public:
	// System.TimeSpan System.TimeSpan::Zero
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___Zero_0;
	// System.TimeSpan System.TimeSpan::MaxValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MaxValue_1;
	// System.TimeSpan System.TimeSpan::MinValue
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___MinValue_2;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyConfigChecked
	bool ____legacyConfigChecked_4;
	// System.Boolean modreq(System.Runtime.CompilerServices.IsVolatile) System.TimeSpan::_legacyMode
	bool ____legacyMode_5;

public:
	inline static int32_t get_offset_of_Zero_0() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___Zero_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_Zero_0() const { return ___Zero_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_Zero_0() { return &___Zero_0; }
	inline void set_Zero_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___Zero_0 = value;
	}

	inline static int32_t get_offset_of_MaxValue_1() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MaxValue_1)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MaxValue_1() const { return ___MaxValue_1; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MaxValue_1() { return &___MaxValue_1; }
	inline void set_MaxValue_1(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MaxValue_1 = value;
	}

	inline static int32_t get_offset_of_MinValue_2() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ___MinValue_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_MinValue_2() const { return ___MinValue_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_MinValue_2() { return &___MinValue_2; }
	inline void set_MinValue_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___MinValue_2 = value;
	}

	inline static int32_t get_offset_of__legacyConfigChecked_4() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyConfigChecked_4)); }
	inline bool get__legacyConfigChecked_4() const { return ____legacyConfigChecked_4; }
	inline bool* get_address_of__legacyConfigChecked_4() { return &____legacyConfigChecked_4; }
	inline void set__legacyConfigChecked_4(bool value)
	{
		____legacyConfigChecked_4 = value;
	}

	inline static int32_t get_offset_of__legacyMode_5() { return static_cast<int32_t>(offsetof(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4_StaticFields, ____legacyMode_5)); }
	inline bool get__legacyMode_5() const { return ____legacyMode_5; }
	inline bool* get_address_of__legacyMode_5() { return &____legacyMode_5; }
	inline void set__legacyMode_5(bool value)
	{
		____legacyMode_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TIMESPAN_TA8069278ACE8A74D6DF7D514A9CD4432433F64C4_H
#ifndef PARAMETER_T9771CF59A5BAB5504895474F08214C79A68679AD_H
#define PARAMETER_T9771CF59A5BAB5504895474F08214C79A68679AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Parameter
struct  Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD  : public RuntimeObject
{
public:
	// System.String RestSharp.Parameter::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Object RestSharp.Parameter::<Value>k__BackingField
	RuntimeObject * ___U3CValueU3Ek__BackingField_1;
	// RestSharp.ParameterType RestSharp.Parameter::<Type>k__BackingField
	int32_t ___U3CTypeU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD, ___U3CValueU3Ek__BackingField_1)); }
	inline RuntimeObject * get_U3CValueU3Ek__BackingField_1() const { return ___U3CValueU3Ek__BackingField_1; }
	inline RuntimeObject ** get_address_of_U3CValueU3Ek__BackingField_1() { return &___U3CValueU3Ek__BackingField_1; }
	inline void set_U3CValueU3Ek__BackingField_1(RuntimeObject * value)
	{
		___U3CValueU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD, ___U3CTypeU3Ek__BackingField_2)); }
	inline int32_t get_U3CTypeU3Ek__BackingField_2() const { return ___U3CTypeU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CTypeU3Ek__BackingField_2() { return &___U3CTypeU3Ek__BackingField_2; }
	inline void set_U3CTypeU3Ek__BackingField_2(int32_t value)
	{
		___U3CTypeU3Ek__BackingField_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARAMETER_T9771CF59A5BAB5504895474F08214C79A68679AD_H
#ifndef RESTREQUEST_TB40F117F5D3AF6C850E10C7B45B179A045C94D8E_H
#define RESTREQUEST_TB40F117F5D3AF6C850E10C7B45B179A045C94D8E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.RestRequest
struct  RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E  : public RuntimeObject
{
public:
	// RestSharp.Serializers.ISerializer RestSharp.RestRequest::<JsonSerializer>k__BackingField
	RuntimeObject* ___U3CJsonSerializerU3Ek__BackingField_0;
	// RestSharp.Serializers.ISerializer RestSharp.RestRequest::<XmlSerializer>k__BackingField
	RuntimeObject* ___U3CXmlSerializerU3Ek__BackingField_1;
	// System.Action`1<System.IO.Stream> RestSharp.RestRequest::<ResponseWriter>k__BackingField
	Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * ___U3CResponseWriterU3Ek__BackingField_2;
	// System.Collections.Generic.List`1<RestSharp.Parameter> RestSharp.RestRequest::<Parameters>k__BackingField
	List_1_tF576173DE7731AAF41175774D3BC4674C52DEB35 * ___U3CParametersU3Ek__BackingField_3;
	// System.Collections.Generic.List`1<RestSharp.FileParameter> RestSharp.RestRequest::<Files>k__BackingField
	List_1_tE557E95A7B0D2C0E2BC087AD8443DB2CD0F13828 * ___U3CFilesU3Ek__BackingField_4;
	// RestSharp.Method RestSharp.RestRequest::_method
	int32_t ____method_5;
	// System.String RestSharp.RestRequest::<Resource>k__BackingField
	String_t* ___U3CResourceU3Ek__BackingField_6;
	// RestSharp.DataFormat RestSharp.RestRequest::_requestFormat
	int32_t ____requestFormat_7;
	// System.Action`1<RestSharp.IRestResponse> RestSharp.RestRequest::<OnBeforeDeserialization>k__BackingField
	Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 * ___U3COnBeforeDeserializationU3Ek__BackingField_8;
	// System.Net.ICredentials RestSharp.RestRequest::<Credentials>k__BackingField
	RuntimeObject* ___U3CCredentialsU3Ek__BackingField_9;
	// System.Int32 RestSharp.RestRequest::<Timeout>k__BackingField
	int32_t ___U3CTimeoutU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_U3CJsonSerializerU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CJsonSerializerU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CJsonSerializerU3Ek__BackingField_0() const { return ___U3CJsonSerializerU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CJsonSerializerU3Ek__BackingField_0() { return &___U3CJsonSerializerU3Ek__BackingField_0; }
	inline void set_U3CJsonSerializerU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CJsonSerializerU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CJsonSerializerU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CXmlSerializerU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CXmlSerializerU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CXmlSerializerU3Ek__BackingField_1() const { return ___U3CXmlSerializerU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CXmlSerializerU3Ek__BackingField_1() { return &___U3CXmlSerializerU3Ek__BackingField_1; }
	inline void set_U3CXmlSerializerU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CXmlSerializerU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CXmlSerializerU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CResponseWriterU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CResponseWriterU3Ek__BackingField_2)); }
	inline Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * get_U3CResponseWriterU3Ek__BackingField_2() const { return ___U3CResponseWriterU3Ek__BackingField_2; }
	inline Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 ** get_address_of_U3CResponseWriterU3Ek__BackingField_2() { return &___U3CResponseWriterU3Ek__BackingField_2; }
	inline void set_U3CResponseWriterU3Ek__BackingField_2(Action_1_tC8BAB6C7B8E5508F10B3A5EF475B0FFAE7688621 * value)
	{
		___U3CResponseWriterU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResponseWriterU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CParametersU3Ek__BackingField_3)); }
	inline List_1_tF576173DE7731AAF41175774D3BC4674C52DEB35 * get_U3CParametersU3Ek__BackingField_3() const { return ___U3CParametersU3Ek__BackingField_3; }
	inline List_1_tF576173DE7731AAF41175774D3BC4674C52DEB35 ** get_address_of_U3CParametersU3Ek__BackingField_3() { return &___U3CParametersU3Ek__BackingField_3; }
	inline void set_U3CParametersU3Ek__BackingField_3(List_1_tF576173DE7731AAF41175774D3BC4674C52DEB35 * value)
	{
		___U3CParametersU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CFilesU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CFilesU3Ek__BackingField_4)); }
	inline List_1_tE557E95A7B0D2C0E2BC087AD8443DB2CD0F13828 * get_U3CFilesU3Ek__BackingField_4() const { return ___U3CFilesU3Ek__BackingField_4; }
	inline List_1_tE557E95A7B0D2C0E2BC087AD8443DB2CD0F13828 ** get_address_of_U3CFilesU3Ek__BackingField_4() { return &___U3CFilesU3Ek__BackingField_4; }
	inline void set_U3CFilesU3Ek__BackingField_4(List_1_tE557E95A7B0D2C0E2BC087AD8443DB2CD0F13828 * value)
	{
		___U3CFilesU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFilesU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of__method_5() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ____method_5)); }
	inline int32_t get__method_5() const { return ____method_5; }
	inline int32_t* get_address_of__method_5() { return &____method_5; }
	inline void set__method_5(int32_t value)
	{
		____method_5 = value;
	}

	inline static int32_t get_offset_of_U3CResourceU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CResourceU3Ek__BackingField_6)); }
	inline String_t* get_U3CResourceU3Ek__BackingField_6() const { return ___U3CResourceU3Ek__BackingField_6; }
	inline String_t** get_address_of_U3CResourceU3Ek__BackingField_6() { return &___U3CResourceU3Ek__BackingField_6; }
	inline void set_U3CResourceU3Ek__BackingField_6(String_t* value)
	{
		___U3CResourceU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CResourceU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of__requestFormat_7() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ____requestFormat_7)); }
	inline int32_t get__requestFormat_7() const { return ____requestFormat_7; }
	inline int32_t* get_address_of__requestFormat_7() { return &____requestFormat_7; }
	inline void set__requestFormat_7(int32_t value)
	{
		____requestFormat_7 = value;
	}

	inline static int32_t get_offset_of_U3COnBeforeDeserializationU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3COnBeforeDeserializationU3Ek__BackingField_8)); }
	inline Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 * get_U3COnBeforeDeserializationU3Ek__BackingField_8() const { return ___U3COnBeforeDeserializationU3Ek__BackingField_8; }
	inline Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 ** get_address_of_U3COnBeforeDeserializationU3Ek__BackingField_8() { return &___U3COnBeforeDeserializationU3Ek__BackingField_8; }
	inline void set_U3COnBeforeDeserializationU3Ek__BackingField_8(Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 * value)
	{
		___U3COnBeforeDeserializationU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3COnBeforeDeserializationU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_U3CCredentialsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CCredentialsU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CCredentialsU3Ek__BackingField_9() const { return ___U3CCredentialsU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CCredentialsU3Ek__BackingField_9() { return &___U3CCredentialsU3Ek__BackingField_9; }
	inline void set_U3CCredentialsU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CCredentialsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCredentialsU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CTimeoutU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E, ___U3CTimeoutU3Ek__BackingField_10)); }
	inline int32_t get_U3CTimeoutU3Ek__BackingField_10() const { return ___U3CTimeoutU3Ek__BackingField_10; }
	inline int32_t* get_address_of_U3CTimeoutU3Ek__BackingField_10() { return &___U3CTimeoutU3Ek__BackingField_10; }
	inline void set_U3CTimeoutU3Ek__BackingField_10(int32_t value)
	{
		___U3CTimeoutU3Ek__BackingField_10 = value;
	}
};

struct RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E_StaticFields
{
public:
	// System.Action`1<RestSharp.IRestResponse> RestSharp.RestRequest::<>f__am$cache0
	Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 * ___U3CU3Ef__amU24cache0_11;

public:
	inline static int32_t get_offset_of_U3CU3Ef__amU24cache0_11() { return static_cast<int32_t>(offsetof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E_StaticFields, ___U3CU3Ef__amU24cache0_11)); }
	inline Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 * get_U3CU3Ef__amU24cache0_11() const { return ___U3CU3Ef__amU24cache0_11; }
	inline Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 ** get_address_of_U3CU3Ef__amU24cache0_11() { return &___U3CU3Ef__amU24cache0_11; }
	inline void set_U3CU3Ef__amU24cache0_11(Action_1_t89A60F10611ED5D73C7316112C72DC61888EF472 * value)
	{
		___U3CU3Ef__amU24cache0_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3Ef__amU24cache0_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTREQUEST_TB40F117F5D3AF6C850E10C7B45B179A045C94D8E_H
#ifndef SERIALIZEASATTRIBUTE_T068C27F705FD2F2725572072BE7962F2DD0829E1_H
#define SERIALIZEASATTRIBUTE_T068C27F705FD2F2725572072BE7962F2DD0829E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Serializers.SerializeAsAttribute
struct  SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1  : public Attribute_tF048C13FB3C8CFCC53F82290E4A3F621089F9A74
{
public:
	// System.String RestSharp.Serializers.SerializeAsAttribute::<Name>k__BackingField
	String_t* ___U3CNameU3Ek__BackingField_0;
	// System.Boolean RestSharp.Serializers.SerializeAsAttribute::<Attribute>k__BackingField
	bool ___U3CAttributeU3Ek__BackingField_1;
	// System.Globalization.CultureInfo RestSharp.Serializers.SerializeAsAttribute::<Culture>k__BackingField
	CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * ___U3CCultureU3Ek__BackingField_2;
	// RestSharp.Serializers.NameStyle RestSharp.Serializers.SerializeAsAttribute::<NameStyle>k__BackingField
	int32_t ___U3CNameStyleU3Ek__BackingField_3;
	// System.Int32 RestSharp.Serializers.SerializeAsAttribute::<Index>k__BackingField
	int32_t ___U3CIndexU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CNameU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1, ___U3CNameU3Ek__BackingField_0)); }
	inline String_t* get_U3CNameU3Ek__BackingField_0() const { return ___U3CNameU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CNameU3Ek__BackingField_0() { return &___U3CNameU3Ek__BackingField_0; }
	inline void set_U3CNameU3Ek__BackingField_0(String_t* value)
	{
		___U3CNameU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CNameU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CAttributeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1, ___U3CAttributeU3Ek__BackingField_1)); }
	inline bool get_U3CAttributeU3Ek__BackingField_1() const { return ___U3CAttributeU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CAttributeU3Ek__BackingField_1() { return &___U3CAttributeU3Ek__BackingField_1; }
	inline void set_U3CAttributeU3Ek__BackingField_1(bool value)
	{
		___U3CAttributeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCultureU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1, ___U3CCultureU3Ek__BackingField_2)); }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * get_U3CCultureU3Ek__BackingField_2() const { return ___U3CCultureU3Ek__BackingField_2; }
	inline CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F ** get_address_of_U3CCultureU3Ek__BackingField_2() { return &___U3CCultureU3Ek__BackingField_2; }
	inline void set_U3CCultureU3Ek__BackingField_2(CultureInfo_t345AC6924134F039ED9A11F3E03F8E91B6A3225F * value)
	{
		___U3CCultureU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCultureU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CNameStyleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1, ___U3CNameStyleU3Ek__BackingField_3)); }
	inline int32_t get_U3CNameStyleU3Ek__BackingField_3() const { return ___U3CNameStyleU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CNameStyleU3Ek__BackingField_3() { return &___U3CNameStyleU3Ek__BackingField_3; }
	inline void set_U3CNameStyleU3Ek__BackingField_3(int32_t value)
	{
		___U3CNameStyleU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CIndexU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1, ___U3CIndexU3Ek__BackingField_4)); }
	inline int32_t get_U3CIndexU3Ek__BackingField_4() const { return ___U3CIndexU3Ek__BackingField_4; }
	inline int32_t* get_address_of_U3CIndexU3Ek__BackingField_4() { return &___U3CIndexU3Ek__BackingField_4; }
	inline void set_U3CIndexU3Ek__BackingField_4(int32_t value)
	{
		___U3CIndexU3Ek__BackingField_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SERIALIZEASATTRIBUTE_T068C27F705FD2F2725572072BE7962F2DD0829E1_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef HEADERINFO_T067D19E53F90FB324CA05965F2429843D223C61C_H
#define HEADERINFO_T067D19E53F90FB324CA05965F2429843D223C61C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HeaderInfo
struct  HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C  : public RuntimeObject
{
public:
	// System.Boolean System.Net.Http.Headers.HeaderInfo::AllowsMany
	bool ___AllowsMany_0;
	// System.Net.Http.Headers.HttpHeaderKind System.Net.Http.Headers.HeaderInfo::HeaderKind
	int32_t ___HeaderKind_1;
	// System.String System.Net.Http.Headers.HeaderInfo::Name
	String_t* ___Name_2;
	// System.Func`2<System.Object,System.String> System.Net.Http.Headers.HeaderInfo::<CustomToString>k__BackingField
	Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * ___U3CCustomToStringU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_AllowsMany_0() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___AllowsMany_0)); }
	inline bool get_AllowsMany_0() const { return ___AllowsMany_0; }
	inline bool* get_address_of_AllowsMany_0() { return &___AllowsMany_0; }
	inline void set_AllowsMany_0(bool value)
	{
		___AllowsMany_0 = value;
	}

	inline static int32_t get_offset_of_HeaderKind_1() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___HeaderKind_1)); }
	inline int32_t get_HeaderKind_1() const { return ___HeaderKind_1; }
	inline int32_t* get_address_of_HeaderKind_1() { return &___HeaderKind_1; }
	inline void set_HeaderKind_1(int32_t value)
	{
		___HeaderKind_1 = value;
	}

	inline static int32_t get_offset_of_Name_2() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___Name_2)); }
	inline String_t* get_Name_2() const { return ___Name_2; }
	inline String_t** get_address_of_Name_2() { return &___Name_2; }
	inline void set_Name_2(String_t* value)
	{
		___Name_2 = value;
		Il2CppCodeGenWriteBarrier((&___Name_2), value);
	}

	inline static int32_t get_offset_of_U3CCustomToStringU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C, ___U3CCustomToStringU3Ek__BackingField_3)); }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * get_U3CCustomToStringU3Ek__BackingField_3() const { return ___U3CCustomToStringU3Ek__BackingField_3; }
	inline Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF ** get_address_of_U3CCustomToStringU3Ek__BackingField_3() { return &___U3CCustomToStringU3Ek__BackingField_3; }
	inline void set_U3CCustomToStringU3Ek__BackingField_3(Func_2_t44B347E67E515867D995E8BD5EFD67FA88CE53CF * value)
	{
		___U3CCustomToStringU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCustomToStringU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HEADERINFO_T067D19E53F90FB324CA05965F2429843D223C61C_H
#ifndef HTTPHEADERS_TF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_H
#define HTTPHEADERS_TF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaders
struct  HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HttpHeaders/HeaderBucket> System.Net.Http.Headers.HttpHeaders::headers
	Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * ___headers_1;
	// System.Net.Http.Headers.HttpHeaderKind System.Net.Http.Headers.HttpHeaders::HeaderKind
	int32_t ___HeaderKind_2;
	// System.Nullable`1<System.Boolean> System.Net.Http.Headers.HttpHeaders::connectionclose
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___connectionclose_3;
	// System.Nullable`1<System.Boolean> System.Net.Http.Headers.HttpHeaders::transferEncodingChunked
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___transferEncodingChunked_4;

public:
	inline static int32_t get_offset_of_headers_1() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___headers_1)); }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * get_headers_1() const { return ___headers_1; }
	inline Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 ** get_address_of_headers_1() { return &___headers_1; }
	inline void set_headers_1(Dictionary_2_t68C9612F2AAB4592A72574ECE587789329FFC1B3 * value)
	{
		___headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___headers_1), value);
	}

	inline static int32_t get_offset_of_HeaderKind_2() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___HeaderKind_2)); }
	inline int32_t get_HeaderKind_2() const { return ___HeaderKind_2; }
	inline int32_t* get_address_of_HeaderKind_2() { return &___HeaderKind_2; }
	inline void set_HeaderKind_2(int32_t value)
	{
		___HeaderKind_2 = value;
	}

	inline static int32_t get_offset_of_connectionclose_3() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___connectionclose_3)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_connectionclose_3() const { return ___connectionclose_3; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_connectionclose_3() { return &___connectionclose_3; }
	inline void set_connectionclose_3(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___connectionclose_3 = value;
	}

	inline static int32_t get_offset_of_transferEncodingChunked_4() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1, ___transferEncodingChunked_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_transferEncodingChunked_4() const { return ___transferEncodingChunked_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_transferEncodingChunked_4() { return &___transferEncodingChunked_4; }
	inline void set_transferEncodingChunked_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___transferEncodingChunked_4 = value;
	}
};

struct HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<System.String,System.Net.Http.Headers.HeaderInfo> System.Net.Http.Headers.HttpHeaders::known_headers
	Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 * ___known_headers_0;

public:
	inline static int32_t get_offset_of_known_headers_0() { return static_cast<int32_t>(offsetof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields, ___known_headers_0)); }
	inline Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 * get_known_headers_0() const { return ___known_headers_0; }
	inline Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 ** get_address_of_known_headers_0() { return &___known_headers_0; }
	inline void set_known_headers_0(Dictionary_2_tEBE1ABD140AD7E9C7019A4D27C5C036F79BA1565 * value)
	{
		___known_headers_0 = value;
		Il2CppCodeGenWriteBarrier((&___known_headers_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPHEADERS_TF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_H
#ifndef U3CGETENUMERATORU3ED__19_TA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A_H
#define U3CGETENUMERATORU3ED__19_TA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpHeaders/<GetEnumerator>d__19
struct  U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A  : public RuntimeObject
{
public:
	// System.Int32 System.Net.Http.Headers.HttpHeaders/<GetEnumerator>d__19::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.IEnumerable`1<System.String>> System.Net.Http.Headers.HttpHeaders/<GetEnumerator>d__19::<>2__current
	KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684  ___U3CU3E2__current_1;
	// System.Net.Http.Headers.HttpHeaders System.Net.Http.Headers.HttpHeaders/<GetEnumerator>d__19::<>4__this
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 * ___U3CU3E4__this_2;
	// System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Net.Http.Headers.HttpHeaders/HeaderBucket> System.Net.Http.Headers.HttpHeaders/<GetEnumerator>d__19::<>7__wrap1
	Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE  ___U3CU3E7__wrap1_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E2__current_1)); }
	inline KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684  get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684 * get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(KeyValuePair_2_t96231301FF22709182494F359E6D4D77EE2F9684  value)
	{
		___U3CU3E2__current_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E4__this_2)); }
	inline HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_3() { return static_cast<int32_t>(offsetof(U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A, ___U3CU3E7__wrap1_3)); }
	inline Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE  get_U3CU3E7__wrap1_3() const { return ___U3CU3E7__wrap1_3; }
	inline Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE * get_address_of_U3CU3E7__wrap1_3() { return &___U3CU3E7__wrap1_3; }
	inline void set_U3CU3E7__wrap1_3(Enumerator_t9F6648C781C37CAB33B771059769176CAE5AB6FE  value)
	{
		___U3CU3E7__wrap1_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETENUMERATORU3ED__19_TA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A_H
#ifndef TOKEN_T5CC7CC2006CB67D86AED528ECA839915D10D4846_H
#define TOKEN_T5CC7CC2006CB67D86AED528ECA839915D10D4846_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.Token
struct  Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846 
{
public:
	// System.Net.Http.Headers.Token/Type System.Net.Http.Headers.Token::type
	int32_t ___type_1;
	// System.Int32 System.Net.Http.Headers.Token::<StartPosition>k__BackingField
	int32_t ___U3CStartPositionU3Ek__BackingField_2;
	// System.Int32 System.Net.Http.Headers.Token::<EndPosition>k__BackingField
	int32_t ___U3CEndPositionU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_type_1() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846, ___type_1)); }
	inline int32_t get_type_1() const { return ___type_1; }
	inline int32_t* get_address_of_type_1() { return &___type_1; }
	inline void set_type_1(int32_t value)
	{
		___type_1 = value;
	}

	inline static int32_t get_offset_of_U3CStartPositionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846, ___U3CStartPositionU3Ek__BackingField_2)); }
	inline int32_t get_U3CStartPositionU3Ek__BackingField_2() const { return ___U3CStartPositionU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CStartPositionU3Ek__BackingField_2() { return &___U3CStartPositionU3Ek__BackingField_2; }
	inline void set_U3CStartPositionU3Ek__BackingField_2(int32_t value)
	{
		___U3CStartPositionU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CEndPositionU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846, ___U3CEndPositionU3Ek__BackingField_3)); }
	inline int32_t get_U3CEndPositionU3Ek__BackingField_3() const { return ___U3CEndPositionU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CEndPositionU3Ek__BackingField_3() { return &___U3CEndPositionU3Ek__BackingField_3; }
	inline void set_U3CEndPositionU3Ek__BackingField_3(int32_t value)
	{
		___U3CEndPositionU3Ek__BackingField_3 = value;
	}
};

struct Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields
{
public:
	// System.Net.Http.Headers.Token System.Net.Http.Headers.Token::Empty
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846  ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields, ___Empty_0)); }
	inline Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846  get_Empty_0() const { return ___Empty_0; }
	inline Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846 * get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846  value)
	{
		___Empty_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T5CC7CC2006CB67D86AED528ECA839915D10D4846_H
#ifndef HTTPCLIENT_TC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_H
#define HTTPCLIENT_TC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClient
struct  HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7  : public HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72
{
public:
	// System.Uri System.Net.Http.HttpClient::base_address
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___base_address_3;
	// System.Threading.CancellationTokenSource System.Net.Http.HttpClient::cts
	CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * ___cts_4;
	// System.Boolean System.Net.Http.HttpClient::disposed
	bool ___disposed_5;
	// System.Net.Http.Headers.HttpRequestHeaders System.Net.Http.HttpClient::headers
	HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * ___headers_6;
	// System.Int64 System.Net.Http.HttpClient::buffer_size
	int64_t ___buffer_size_7;
	// System.TimeSpan System.Net.Http.HttpClient::timeout
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___timeout_8;

public:
	inline static int32_t get_offset_of_base_address_3() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___base_address_3)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_base_address_3() const { return ___base_address_3; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_base_address_3() { return &___base_address_3; }
	inline void set_base_address_3(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___base_address_3 = value;
		Il2CppCodeGenWriteBarrier((&___base_address_3), value);
	}

	inline static int32_t get_offset_of_cts_4() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___cts_4)); }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * get_cts_4() const { return ___cts_4; }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE ** get_address_of_cts_4() { return &___cts_4; }
	inline void set_cts_4(CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * value)
	{
		___cts_4 = value;
		Il2CppCodeGenWriteBarrier((&___cts_4), value);
	}

	inline static int32_t get_offset_of_disposed_5() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___disposed_5)); }
	inline bool get_disposed_5() const { return ___disposed_5; }
	inline bool* get_address_of_disposed_5() { return &___disposed_5; }
	inline void set_disposed_5(bool value)
	{
		___disposed_5 = value;
	}

	inline static int32_t get_offset_of_headers_6() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___headers_6)); }
	inline HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * get_headers_6() const { return ___headers_6; }
	inline HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 ** get_address_of_headers_6() { return &___headers_6; }
	inline void set_headers_6(HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4 * value)
	{
		___headers_6 = value;
		Il2CppCodeGenWriteBarrier((&___headers_6), value);
	}

	inline static int32_t get_offset_of_buffer_size_7() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___buffer_size_7)); }
	inline int64_t get_buffer_size_7() const { return ___buffer_size_7; }
	inline int64_t* get_address_of_buffer_size_7() { return &___buffer_size_7; }
	inline void set_buffer_size_7(int64_t value)
	{
		___buffer_size_7 = value;
	}

	inline static int32_t get_offset_of_timeout_8() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7, ___timeout_8)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_timeout_8() const { return ___timeout_8; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_timeout_8() { return &___timeout_8; }
	inline void set_timeout_8(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___timeout_8 = value;
	}
};

struct HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields
{
public:
	// System.TimeSpan System.Net.Http.HttpClient::TimeoutDefault
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___TimeoutDefault_2;

public:
	inline static int32_t get_offset_of_TimeoutDefault_2() { return static_cast<int32_t>(offsetof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields, ___TimeoutDefault_2)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_TimeoutDefault_2() const { return ___TimeoutDefault_2; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_TimeoutDefault_2() { return &___TimeoutDefault_2; }
	inline void set_TimeoutDefault_2(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___TimeoutDefault_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENT_TC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_H
#ifndef U3CSENDASYNCWORKERU3ED__47_T47F0FB04707E1418B40449E9FC6FE1F2FEAB0941_H
#define U3CSENDASYNCWORKERU3ED__47_T47F0FB04707E1418B40449E9FC6FE1F2FEAB0941_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClient/<SendAsyncWorker>d__47
struct  U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941 
{
public:
	// System.Int32 System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Net.Http.HttpResponseMessage> System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<>t__builder
	AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0  ___U3CU3Et__builder_1;
	// System.Net.Http.HttpClient System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<>4__this
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * ___U3CU3E4__this_2;
	// System.Threading.CancellationToken System.Net.Http.HttpClient/<SendAsyncWorker>d__47::cancellationToken
	CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  ___cancellationToken_3;
	// System.Net.Http.HttpRequestMessage System.Net.Http.HttpClient/<SendAsyncWorker>d__47::request
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * ___request_4;
	// System.Net.Http.HttpCompletionOption System.Net.Http.HttpClient/<SendAsyncWorker>d__47::completionOption
	int32_t ___completionOption_5;
	// System.Net.Http.HttpResponseMessage System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<response>5__1
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 * ___U3CresponseU3E5__1_6;
	// System.Threading.CancellationTokenSource System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<lcts>5__2
	CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * ___U3ClctsU3E5__2_7;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.Http.HttpResponseMessage> System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<>u__1
	ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D  ___U3CU3Eu__1_8;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter System.Net.Http.HttpClient/<SendAsyncWorker>d__47::<>u__2
	ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  ___U3CU3Eu__2_9;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3CU3E4__this_2)); }
	inline HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_cancellationToken_3() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___cancellationToken_3)); }
	inline CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  get_cancellationToken_3() const { return ___cancellationToken_3; }
	inline CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB * get_address_of_cancellationToken_3() { return &___cancellationToken_3; }
	inline void set_cancellationToken_3(CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  value)
	{
		___cancellationToken_3 = value;
	}

	inline static int32_t get_offset_of_request_4() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___request_4)); }
	inline HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * get_request_4() const { return ___request_4; }
	inline HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 ** get_address_of_request_4() { return &___request_4; }
	inline void set_request_4(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * value)
	{
		___request_4 = value;
		Il2CppCodeGenWriteBarrier((&___request_4), value);
	}

	inline static int32_t get_offset_of_completionOption_5() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___completionOption_5)); }
	inline int32_t get_completionOption_5() const { return ___completionOption_5; }
	inline int32_t* get_address_of_completionOption_5() { return &___completionOption_5; }
	inline void set_completionOption_5(int32_t value)
	{
		___completionOption_5 = value;
	}

	inline static int32_t get_offset_of_U3CresponseU3E5__1_6() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3CresponseU3E5__1_6)); }
	inline HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 * get_U3CresponseU3E5__1_6() const { return ___U3CresponseU3E5__1_6; }
	inline HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 ** get_address_of_U3CresponseU3E5__1_6() { return &___U3CresponseU3E5__1_6; }
	inline void set_U3CresponseU3E5__1_6(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904 * value)
	{
		___U3CresponseU3E5__1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CresponseU3E5__1_6), value);
	}

	inline static int32_t get_offset_of_U3ClctsU3E5__2_7() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3ClctsU3E5__2_7)); }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * get_U3ClctsU3E5__2_7() const { return ___U3ClctsU3E5__2_7; }
	inline CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE ** get_address_of_U3ClctsU3E5__2_7() { return &___U3ClctsU3E5__2_7; }
	inline void set_U3ClctsU3E5__2_7(CancellationTokenSource_tF480B7E74A032667AFBD31F0530D619FB43AD3FE * value)
	{
		___U3ClctsU3E5__2_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3ClctsU3E5__2_7), value);
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_8() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3CU3Eu__1_8)); }
	inline ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D  get_U3CU3Eu__1_8() const { return ___U3CU3Eu__1_8; }
	inline ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D * get_address_of_U3CU3Eu__1_8() { return &___U3CU3Eu__1_8; }
	inline void set_U3CU3Eu__1_8(ConfiguredTaskAwaiter_t88324A1949AEB8DD9108467C8C9B3D027A4C570D  value)
	{
		___U3CU3Eu__1_8 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_9() { return static_cast<int32_t>(offsetof(U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941, ___U3CU3Eu__2_9)); }
	inline ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  get_U3CU3Eu__2_9() const { return ___U3CU3Eu__2_9; }
	inline ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874 * get_address_of_U3CU3Eu__2_9() { return &___U3CU3Eu__2_9; }
	inline void set_U3CU3Eu__2_9(ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  value)
	{
		___U3CU3Eu__2_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCWORKERU3ED__47_T47F0FB04707E1418B40449E9FC6FE1F2FEAB0941_H
#ifndef HTTPCLIENTHANDLER_T029F50F2D24A50A3C90A8F8192B8E82CD0076049_H
#define HTTPCLIENTHANDLER_T029F50F2D24A50A3C90A8F8192B8E82CD0076049_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClientHandler
struct  HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049  : public HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894
{
public:
	// System.Boolean System.Net.Http.HttpClientHandler::allowAutoRedirect
	bool ___allowAutoRedirect_1;
	// System.Net.DecompressionMethods System.Net.Http.HttpClientHandler::automaticDecompression
	int32_t ___automaticDecompression_2;
	// System.Net.CookieContainer System.Net.Http.HttpClientHandler::cookieContainer
	CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * ___cookieContainer_3;
	// System.Net.ICredentials System.Net.Http.HttpClientHandler::credentials
	RuntimeObject* ___credentials_4;
	// System.Int32 System.Net.Http.HttpClientHandler::maxAutomaticRedirections
	int32_t ___maxAutomaticRedirections_5;
	// System.Int64 System.Net.Http.HttpClientHandler::maxRequestContentBufferSize
	int64_t ___maxRequestContentBufferSize_6;
	// System.Boolean System.Net.Http.HttpClientHandler::preAuthenticate
	bool ___preAuthenticate_7;
	// System.Net.IWebProxy System.Net.Http.HttpClientHandler::proxy
	RuntimeObject* ___proxy_8;
	// System.Boolean System.Net.Http.HttpClientHandler::useCookies
	bool ___useCookies_9;
	// System.Boolean System.Net.Http.HttpClientHandler::useDefaultCredentials
	bool ___useDefaultCredentials_10;
	// System.Boolean System.Net.Http.HttpClientHandler::useProxy
	bool ___useProxy_11;
	// System.Boolean System.Net.Http.HttpClientHandler::sentRequest
	bool ___sentRequest_12;
	// System.String System.Net.Http.HttpClientHandler::connectionGroupName
	String_t* ___connectionGroupName_13;
	// System.Boolean System.Net.Http.HttpClientHandler::disposed
	bool ___disposed_14;

public:
	inline static int32_t get_offset_of_allowAutoRedirect_1() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___allowAutoRedirect_1)); }
	inline bool get_allowAutoRedirect_1() const { return ___allowAutoRedirect_1; }
	inline bool* get_address_of_allowAutoRedirect_1() { return &___allowAutoRedirect_1; }
	inline void set_allowAutoRedirect_1(bool value)
	{
		___allowAutoRedirect_1 = value;
	}

	inline static int32_t get_offset_of_automaticDecompression_2() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___automaticDecompression_2)); }
	inline int32_t get_automaticDecompression_2() const { return ___automaticDecompression_2; }
	inline int32_t* get_address_of_automaticDecompression_2() { return &___automaticDecompression_2; }
	inline void set_automaticDecompression_2(int32_t value)
	{
		___automaticDecompression_2 = value;
	}

	inline static int32_t get_offset_of_cookieContainer_3() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___cookieContainer_3)); }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * get_cookieContainer_3() const { return ___cookieContainer_3; }
	inline CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 ** get_address_of_cookieContainer_3() { return &___cookieContainer_3; }
	inline void set_cookieContainer_3(CookieContainer_t7E062D04BAED9F3B30DDEC14B09660BB506A2A73 * value)
	{
		___cookieContainer_3 = value;
		Il2CppCodeGenWriteBarrier((&___cookieContainer_3), value);
	}

	inline static int32_t get_offset_of_credentials_4() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___credentials_4)); }
	inline RuntimeObject* get_credentials_4() const { return ___credentials_4; }
	inline RuntimeObject** get_address_of_credentials_4() { return &___credentials_4; }
	inline void set_credentials_4(RuntimeObject* value)
	{
		___credentials_4 = value;
		Il2CppCodeGenWriteBarrier((&___credentials_4), value);
	}

	inline static int32_t get_offset_of_maxAutomaticRedirections_5() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___maxAutomaticRedirections_5)); }
	inline int32_t get_maxAutomaticRedirections_5() const { return ___maxAutomaticRedirections_5; }
	inline int32_t* get_address_of_maxAutomaticRedirections_5() { return &___maxAutomaticRedirections_5; }
	inline void set_maxAutomaticRedirections_5(int32_t value)
	{
		___maxAutomaticRedirections_5 = value;
	}

	inline static int32_t get_offset_of_maxRequestContentBufferSize_6() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___maxRequestContentBufferSize_6)); }
	inline int64_t get_maxRequestContentBufferSize_6() const { return ___maxRequestContentBufferSize_6; }
	inline int64_t* get_address_of_maxRequestContentBufferSize_6() { return &___maxRequestContentBufferSize_6; }
	inline void set_maxRequestContentBufferSize_6(int64_t value)
	{
		___maxRequestContentBufferSize_6 = value;
	}

	inline static int32_t get_offset_of_preAuthenticate_7() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___preAuthenticate_7)); }
	inline bool get_preAuthenticate_7() const { return ___preAuthenticate_7; }
	inline bool* get_address_of_preAuthenticate_7() { return &___preAuthenticate_7; }
	inline void set_preAuthenticate_7(bool value)
	{
		___preAuthenticate_7 = value;
	}

	inline static int32_t get_offset_of_proxy_8() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___proxy_8)); }
	inline RuntimeObject* get_proxy_8() const { return ___proxy_8; }
	inline RuntimeObject** get_address_of_proxy_8() { return &___proxy_8; }
	inline void set_proxy_8(RuntimeObject* value)
	{
		___proxy_8 = value;
		Il2CppCodeGenWriteBarrier((&___proxy_8), value);
	}

	inline static int32_t get_offset_of_useCookies_9() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___useCookies_9)); }
	inline bool get_useCookies_9() const { return ___useCookies_9; }
	inline bool* get_address_of_useCookies_9() { return &___useCookies_9; }
	inline void set_useCookies_9(bool value)
	{
		___useCookies_9 = value;
	}

	inline static int32_t get_offset_of_useDefaultCredentials_10() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___useDefaultCredentials_10)); }
	inline bool get_useDefaultCredentials_10() const { return ___useDefaultCredentials_10; }
	inline bool* get_address_of_useDefaultCredentials_10() { return &___useDefaultCredentials_10; }
	inline void set_useDefaultCredentials_10(bool value)
	{
		___useDefaultCredentials_10 = value;
	}

	inline static int32_t get_offset_of_useProxy_11() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___useProxy_11)); }
	inline bool get_useProxy_11() const { return ___useProxy_11; }
	inline bool* get_address_of_useProxy_11() { return &___useProxy_11; }
	inline void set_useProxy_11(bool value)
	{
		___useProxy_11 = value;
	}

	inline static int32_t get_offset_of_sentRequest_12() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___sentRequest_12)); }
	inline bool get_sentRequest_12() const { return ___sentRequest_12; }
	inline bool* get_address_of_sentRequest_12() { return &___sentRequest_12; }
	inline void set_sentRequest_12(bool value)
	{
		___sentRequest_12 = value;
	}

	inline static int32_t get_offset_of_connectionGroupName_13() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___connectionGroupName_13)); }
	inline String_t* get_connectionGroupName_13() const { return ___connectionGroupName_13; }
	inline String_t** get_address_of_connectionGroupName_13() { return &___connectionGroupName_13; }
	inline void set_connectionGroupName_13(String_t* value)
	{
		___connectionGroupName_13 = value;
		Il2CppCodeGenWriteBarrier((&___connectionGroupName_13), value);
	}

	inline static int32_t get_offset_of_disposed_14() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049, ___disposed_14)); }
	inline bool get_disposed_14() const { return ___disposed_14; }
	inline bool* get_address_of_disposed_14() { return &___disposed_14; }
	inline void set_disposed_14(bool value)
	{
		___disposed_14 = value;
	}
};

struct HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields
{
public:
	// System.Int64 System.Net.Http.HttpClientHandler::groupCounter
	int64_t ___groupCounter_0;

public:
	inline static int32_t get_offset_of_groupCounter_0() { return static_cast<int32_t>(offsetof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields, ___groupCounter_0)); }
	inline int64_t get_groupCounter_0() const { return ___groupCounter_0; }
	inline int64_t* get_address_of_groupCounter_0() { return &___groupCounter_0; }
	inline void set_groupCounter_0(int64_t value)
	{
		___groupCounter_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCLIENTHANDLER_T029F50F2D24A50A3C90A8F8192B8E82CD0076049_H
#ifndef U3CSENDASYNCU3ED__64_T27F5FC426FC9AF2DEA087F38EB6F330957AC16F0_H
#define U3CSENDASYNCU3ED__64_T27F5FC426FC9AF2DEA087F38EB6F330957AC16F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpClientHandler/<SendAsync>d__64
struct  U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0 
{
public:
	// System.Int32 System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Net.Http.HttpResponseMessage> System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>t__builder
	AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0  ___U3CU3Et__builder_1;
	// System.Net.Http.HttpClientHandler System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>4__this
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049 * ___U3CU3E4__this_2;
	// System.Net.Http.HttpRequestMessage System.Net.Http.HttpClientHandler/<SendAsync>d__64::request
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * ___request_3;
	// System.Threading.CancellationToken System.Net.Http.HttpClientHandler/<SendAsync>d__64::cancellationToken
	CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  ___cancellationToken_4;
	// System.Net.HttpWebRequest System.Net.Http.HttpClientHandler/<SendAsync>d__64::<wrequest>5__1
	HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * ___U3CwrequestU3E5__1_5;
	// System.Net.Http.HttpContent System.Net.Http.HttpClientHandler/<SendAsync>d__64::<content>5__2
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * ___U3CcontentU3E5__2_6;
	// System.IO.Stream System.Net.Http.HttpClientHandler/<SendAsync>d__64::<stream>5__3
	Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * ___U3CstreamU3E5__3_7;
	// System.Net.HttpWebResponse System.Net.Http.HttpClientHandler/<SendAsync>d__64::<wresponse>5__4
	HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * ___U3CwresponseU3E5__4_8;
	// System.Threading.CancellationTokenRegistration System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>7__wrap1
	CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2  ___U3CU3E7__wrap1_9;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>u__1
	ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  ___U3CU3Eu__1_10;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.IO.Stream> System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>u__2
	ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133  ___U3CU3Eu__2_11;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable`1/ConfiguredTaskAwaiter<System.Net.WebResponse> System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>u__3
	ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE  ___U3CU3Eu__3_12;
	// System.Runtime.CompilerServices.TaskAwaiter`1<System.Net.Http.HttpResponseMessage> System.Net.Http.HttpClientHandler/<SendAsync>d__64::<>u__4
	TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86  ___U3CU3Eu__4_13;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_1_t0E64AA6FA425130501B776CD7A27C0B3FD630DF0  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3E4__this_2)); }
	inline HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_request_3() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___request_3)); }
	inline HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * get_request_3() const { return ___request_3; }
	inline HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 ** get_address_of_request_3() { return &___request_3; }
	inline void set_request_3(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * value)
	{
		___request_3 = value;
		Il2CppCodeGenWriteBarrier((&___request_3), value);
	}

	inline static int32_t get_offset_of_cancellationToken_4() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___cancellationToken_4)); }
	inline CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  get_cancellationToken_4() const { return ___cancellationToken_4; }
	inline CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB * get_address_of_cancellationToken_4() { return &___cancellationToken_4; }
	inline void set_cancellationToken_4(CancellationToken_t9E956952F7F20908F2AE72EDF36D97E6C7DB63AB  value)
	{
		___cancellationToken_4 = value;
	}

	inline static int32_t get_offset_of_U3CwrequestU3E5__1_5() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CwrequestU3E5__1_5)); }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * get_U3CwrequestU3E5__1_5() const { return ___U3CwrequestU3E5__1_5; }
	inline HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 ** get_address_of_U3CwrequestU3E5__1_5() { return &___U3CwrequestU3E5__1_5; }
	inline void set_U3CwrequestU3E5__1_5(HttpWebRequest_t5B5BFA163B5AD6134620F315940CE3631D7FAAE0 * value)
	{
		___U3CwrequestU3E5__1_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwrequestU3E5__1_5), value);
	}

	inline static int32_t get_offset_of_U3CcontentU3E5__2_6() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CcontentU3E5__2_6)); }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * get_U3CcontentU3E5__2_6() const { return ___U3CcontentU3E5__2_6; }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D ** get_address_of_U3CcontentU3E5__2_6() { return &___U3CcontentU3E5__2_6; }
	inline void set_U3CcontentU3E5__2_6(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * value)
	{
		___U3CcontentU3E5__2_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontentU3E5__2_6), value);
	}

	inline static int32_t get_offset_of_U3CstreamU3E5__3_7() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CstreamU3E5__3_7)); }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * get_U3CstreamU3E5__3_7() const { return ___U3CstreamU3E5__3_7; }
	inline Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 ** get_address_of_U3CstreamU3E5__3_7() { return &___U3CstreamU3E5__3_7; }
	inline void set_U3CstreamU3E5__3_7(Stream_tFC50657DD5AAB87770987F9179D934A51D99D5E7 * value)
	{
		___U3CstreamU3E5__3_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CstreamU3E5__3_7), value);
	}

	inline static int32_t get_offset_of_U3CwresponseU3E5__4_8() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CwresponseU3E5__4_8)); }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * get_U3CwresponseU3E5__4_8() const { return ___U3CwresponseU3E5__4_8; }
	inline HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 ** get_address_of_U3CwresponseU3E5__4_8() { return &___U3CwresponseU3E5__4_8; }
	inline void set_U3CwresponseU3E5__4_8(HttpWebResponse_t34CF6A40A4748A0F8694FEFEA3723D9AE3EF3951 * value)
	{
		___U3CwresponseU3E5__4_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CwresponseU3E5__4_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_9() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3E7__wrap1_9)); }
	inline CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2  get_U3CU3E7__wrap1_9() const { return ___U3CU3E7__wrap1_9; }
	inline CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2 * get_address_of_U3CU3E7__wrap1_9() { return &___U3CU3E7__wrap1_9; }
	inline void set_U3CU3E7__wrap1_9(CancellationTokenRegistration_tCDB9825D1854DD0D7FF737C82B099FC468107BB2  value)
	{
		___U3CU3E7__wrap1_9 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_10() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3Eu__1_10)); }
	inline ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  get_U3CU3Eu__1_10() const { return ___U3CU3Eu__1_10; }
	inline ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874 * get_address_of_U3CU3Eu__1_10() { return &___U3CU3Eu__1_10; }
	inline void set_U3CU3Eu__1_10(ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  value)
	{
		___U3CU3Eu__1_10 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__2_11() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3Eu__2_11)); }
	inline ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133  get_U3CU3Eu__2_11() const { return ___U3CU3Eu__2_11; }
	inline ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133 * get_address_of_U3CU3Eu__2_11() { return &___U3CU3Eu__2_11; }
	inline void set_U3CU3Eu__2_11(ConfiguredTaskAwaiter_t27ECAA4B74502BD3AC7E68F47088D46DAA13D133  value)
	{
		___U3CU3Eu__2_11 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__3_12() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3Eu__3_12)); }
	inline ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE  get_U3CU3Eu__3_12() const { return ___U3CU3Eu__3_12; }
	inline ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE * get_address_of_U3CU3Eu__3_12() { return &___U3CU3Eu__3_12; }
	inline void set_U3CU3Eu__3_12(ConfiguredTaskAwaiter_tC95EBA3D9B77BA07739749A814CFE48A5E66FAEE  value)
	{
		___U3CU3Eu__3_12 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__4_13() { return static_cast<int32_t>(offsetof(U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0, ___U3CU3Eu__4_13)); }
	inline TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86  get_U3CU3Eu__4_13() const { return ___U3CU3Eu__4_13; }
	inline TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86 * get_address_of_U3CU3Eu__4_13() { return &___U3CU3Eu__4_13; }
	inline void set_U3CU3Eu__4_13(TaskAwaiter_1_t0CD71BC02837B0D53252196D19DABDA73A615B86  value)
	{
		___U3CU3Eu__4_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CSENDASYNCU3ED__64_T27F5FC426FC9AF2DEA087F38EB6F330957AC16F0_H
#ifndef FIXEDMEMORYSTREAM_T181662654A3A2002A1F5D33E7747A9E12DAD139C_H
#define FIXEDMEMORYSTREAM_T181662654A3A2002A1F5D33E7747A9E12DAD139C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpContent/FixedMemoryStream
struct  FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C  : public MemoryStream_t495F44B85E6B4DDE2BB7E17DE963256A74E2298C
{
public:
	// System.Int64 System.Net.Http.HttpContent/FixedMemoryStream::maxSize
	int64_t ___maxSize_14;

public:
	inline static int32_t get_offset_of_maxSize_14() { return static_cast<int32_t>(offsetof(FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C, ___maxSize_14)); }
	inline int64_t get_maxSize_14() const { return ___maxSize_14; }
	inline int64_t* get_address_of_maxSize_14() { return &___maxSize_14; }
	inline void set_maxSize_14(int64_t value)
	{
		___maxSize_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIXEDMEMORYSTREAM_T181662654A3A2002A1F5D33E7747A9E12DAD139C_H
#ifndef HTTPRESPONSEMESSAGE_T619DB9FDC8E63CDF2D0A314B61A4C6609A43F904_H
#define HTTPRESPONSEMESSAGE_T619DB9FDC8E63CDF2D0A314B61A4C6609A43F904_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpResponseMessage
struct  HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904  : public RuntimeObject
{
public:
	// System.Net.Http.Headers.HttpResponseHeaders System.Net.Http.HttpResponseMessage::headers
	HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59 * ___headers_0;
	// System.String System.Net.Http.HttpResponseMessage::reasonPhrase
	String_t* ___reasonPhrase_1;
	// System.Net.HttpStatusCode System.Net.Http.HttpResponseMessage::statusCode
	int32_t ___statusCode_2;
	// System.Version System.Net.Http.HttpResponseMessage::version
	Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * ___version_3;
	// System.Boolean System.Net.Http.HttpResponseMessage::disposed
	bool ___disposed_4;
	// System.Net.Http.HttpContent System.Net.Http.HttpResponseMessage::<Content>k__BackingField
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * ___U3CContentU3Ek__BackingField_5;
	// System.Net.Http.HttpRequestMessage System.Net.Http.HttpResponseMessage::<RequestMessage>k__BackingField
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * ___U3CRequestMessageU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_headers_0() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___headers_0)); }
	inline HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59 * get_headers_0() const { return ___headers_0; }
	inline HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59 ** get_address_of_headers_0() { return &___headers_0; }
	inline void set_headers_0(HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59 * value)
	{
		___headers_0 = value;
		Il2CppCodeGenWriteBarrier((&___headers_0), value);
	}

	inline static int32_t get_offset_of_reasonPhrase_1() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___reasonPhrase_1)); }
	inline String_t* get_reasonPhrase_1() const { return ___reasonPhrase_1; }
	inline String_t** get_address_of_reasonPhrase_1() { return &___reasonPhrase_1; }
	inline void set_reasonPhrase_1(String_t* value)
	{
		___reasonPhrase_1 = value;
		Il2CppCodeGenWriteBarrier((&___reasonPhrase_1), value);
	}

	inline static int32_t get_offset_of_statusCode_2() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___statusCode_2)); }
	inline int32_t get_statusCode_2() const { return ___statusCode_2; }
	inline int32_t* get_address_of_statusCode_2() { return &___statusCode_2; }
	inline void set_statusCode_2(int32_t value)
	{
		___statusCode_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___version_3)); }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * get_version_3() const { return ___version_3; }
	inline Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD ** get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(Version_tDBE6876C59B6F56D4F8CAA03851177ABC6FE0DFD * value)
	{
		___version_3 = value;
		Il2CppCodeGenWriteBarrier((&___version_3), value);
	}

	inline static int32_t get_offset_of_disposed_4() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___disposed_4)); }
	inline bool get_disposed_4() const { return ___disposed_4; }
	inline bool* get_address_of_disposed_4() { return &___disposed_4; }
	inline void set_disposed_4(bool value)
	{
		___disposed_4 = value;
	}

	inline static int32_t get_offset_of_U3CContentU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___U3CContentU3Ek__BackingField_5)); }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * get_U3CContentU3Ek__BackingField_5() const { return ___U3CContentU3Ek__BackingField_5; }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D ** get_address_of_U3CContentU3Ek__BackingField_5() { return &___U3CContentU3Ek__BackingField_5; }
	inline void set_U3CContentU3Ek__BackingField_5(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * value)
	{
		___U3CContentU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CContentU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CRequestMessageU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904, ___U3CRequestMessageU3Ek__BackingField_6)); }
	inline HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * get_U3CRequestMessageU3Ek__BackingField_6() const { return ___U3CRequestMessageU3Ek__BackingField_6; }
	inline HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 ** get_address_of_U3CRequestMessageU3Ek__BackingField_6() { return &___U3CRequestMessageU3Ek__BackingField_6; }
	inline void set_U3CRequestMessageU3Ek__BackingField_6(HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427 * value)
	{
		___U3CRequestMessageU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CRequestMessageU3Ek__BackingField_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSEMESSAGE_T619DB9FDC8E63CDF2D0A314B61A4C6609A43F904_H
#ifndef NULLABLE_1_TD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67_H
#define NULLABLE_1_TD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.DateTimeOffset>
struct  Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67 
{
public:
	// T System.Nullable`1::value
	DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67, ___value_0)); }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  get_value_0() const { return ___value_0; }
	inline DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(DateTimeOffset_t6C333873402CAD576160B4F8E159EB6834F06B85  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67_H
#ifndef NULLABLE_1_TA5F97AD8281B6EDEE6731D95047BA50C9996309E_H
#define NULLABLE_1_TA5F97AD8281B6EDEE6731D95047BA50C9996309E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.TimeSpan>
struct  Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E 
{
public:
	// T System.Nullable`1::value
	TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E, ___value_0)); }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  get_value_0() const { return ___value_0; }
	inline TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4 * get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(TimeSpan_tA8069278ACE8A74D6DF7D514A9CD4432433F64C4  value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_TA5F97AD8281B6EDEE6731D95047BA50C9996309E_H
#ifndef ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#define ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct  AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 
{
public:
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::m_builder
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;

public:
	inline static int32_t get_offset_of_m_builder_1() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487, ___m_builder_1)); }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  get_m_builder_1() const { return ___m_builder_1; }
	inline AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9 * get_address_of_m_builder_1() { return &___m_builder_1; }
	inline void set_m_builder_1(AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  value)
	{
		___m_builder_1 = value;
	}
};

struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields
{
public:
	// System.Threading.Tasks.Task`1<System.Threading.Tasks.VoidTaskResult> System.Runtime.CompilerServices.AsyncTaskMethodBuilder::s_cachedCompleted
	Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * ___s_cachedCompleted_0;

public:
	inline static int32_t get_offset_of_s_cachedCompleted_0() { return static_cast<int32_t>(offsetof(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_StaticFields, ___s_cachedCompleted_0)); }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * get_s_cachedCompleted_0() const { return ___s_cachedCompleted_0; }
	inline Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 ** get_address_of_s_cachedCompleted_0() { return &___s_cachedCompleted_0; }
	inline void set_s_cachedCompleted_0(Task_1_t1359D75350E9D976BFA28AD96E417450DE277673 * value)
	{
		___s_cachedCompleted_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_cachedCompleted_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_pinvoke
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
// Native definition for COM marshalling of System.Runtime.CompilerServices.AsyncTaskMethodBuilder
struct AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487_marshaled_com
{
	AsyncTaskMethodBuilder_1_t66ED1808B26B8081A2804D6A750D13386E360BD9  ___m_builder_1;
};
#endif // ASYNCTASKMETHODBUILDER_T0CD1893D670405BED201BE8CA6F2E811F2C0F487_H
#ifndef CTORDELEGATE_T3D608896A17C00ACF115E1896331E79F987144D7_H
#define CTORDELEGATE_T3D608896A17C00ACF115E1896331E79F987144D7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.CacheResolver/CtorDelegate
struct  CtorDelegate_t3D608896A17C00ACF115E1896331E79F987144D7  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CTORDELEGATE_T3D608896A17C00ACF115E1896331E79F987144D7_H
#ifndef GETHANDLER_TBDF089BE73CB25C967659A15E79BC8D364AB5EE0_H
#define GETHANDLER_TBDF089BE73CB25C967659A15E79BC8D364AB5EE0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.GetHandler
struct  GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETHANDLER_TBDF089BE73CB25C967659A15E79BC8D364AB5EE0_H
#ifndef MEMBERMAPLOADER_T18793B8C3AD9903BB61ED00F86C9AFE04A769B91_H
#define MEMBERMAPLOADER_T18793B8C3AD9903BB61ED00F86C9AFE04A769B91_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.MemberMapLoader
struct  MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBERMAPLOADER_T18793B8C3AD9903BB61ED00F86C9AFE04A769B91_H
#ifndef SETHANDLER_T68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8_H
#define SETHANDLER_T68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RestSharp.Reflection.SetHandler
struct  SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETHANDLER_T68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8_H
#ifndef CACHECONTROLHEADERVALUE_T198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB_H
#define CACHECONTROLHEADERVALUE_T198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.CacheControlHeaderValue
struct  CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<System.Net.Http.Headers.NameValueHeaderValue> System.Net.Http.Headers.CacheControlHeaderValue::extensions
	List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * ___extensions_0;
	// System.Collections.Generic.List`1<System.String> System.Net.Http.Headers.CacheControlHeaderValue::no_cache_headers
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___no_cache_headers_1;
	// System.Collections.Generic.List`1<System.String> System.Net.Http.Headers.CacheControlHeaderValue::private_headers
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___private_headers_2;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<MaxAge>k__BackingField
	Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  ___U3CMaxAgeU3Ek__BackingField_3;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<MaxStale>k__BackingField
	bool ___U3CMaxStaleU3Ek__BackingField_4;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<MaxStaleLimit>k__BackingField
	Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  ___U3CMaxStaleLimitU3Ek__BackingField_5;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<MinFresh>k__BackingField
	Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  ___U3CMinFreshU3Ek__BackingField_6;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<MustRevalidate>k__BackingField
	bool ___U3CMustRevalidateU3Ek__BackingField_7;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<NoCache>k__BackingField
	bool ___U3CNoCacheU3Ek__BackingField_8;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<NoStore>k__BackingField
	bool ___U3CNoStoreU3Ek__BackingField_9;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<NoTransform>k__BackingField
	bool ___U3CNoTransformU3Ek__BackingField_10;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<OnlyIfCached>k__BackingField
	bool ___U3COnlyIfCachedU3Ek__BackingField_11;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<Private>k__BackingField
	bool ___U3CPrivateU3Ek__BackingField_12;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<ProxyRevalidate>k__BackingField
	bool ___U3CProxyRevalidateU3Ek__BackingField_13;
	// System.Boolean System.Net.Http.Headers.CacheControlHeaderValue::<Public>k__BackingField
	bool ___U3CPublicU3Ek__BackingField_14;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.CacheControlHeaderValue::<SharedMaxAge>k__BackingField
	Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  ___U3CSharedMaxAgeU3Ek__BackingField_15;

public:
	inline static int32_t get_offset_of_extensions_0() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___extensions_0)); }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * get_extensions_0() const { return ___extensions_0; }
	inline List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 ** get_address_of_extensions_0() { return &___extensions_0; }
	inline void set_extensions_0(List_1_t4C4438F9E20C453876568717EEB3C007B6273D94 * value)
	{
		___extensions_0 = value;
		Il2CppCodeGenWriteBarrier((&___extensions_0), value);
	}

	inline static int32_t get_offset_of_no_cache_headers_1() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___no_cache_headers_1)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_no_cache_headers_1() const { return ___no_cache_headers_1; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_no_cache_headers_1() { return &___no_cache_headers_1; }
	inline void set_no_cache_headers_1(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___no_cache_headers_1 = value;
		Il2CppCodeGenWriteBarrier((&___no_cache_headers_1), value);
	}

	inline static int32_t get_offset_of_private_headers_2() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___private_headers_2)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_private_headers_2() const { return ___private_headers_2; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_private_headers_2() { return &___private_headers_2; }
	inline void set_private_headers_2(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___private_headers_2 = value;
		Il2CppCodeGenWriteBarrier((&___private_headers_2), value);
	}

	inline static int32_t get_offset_of_U3CMaxAgeU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMaxAgeU3Ek__BackingField_3)); }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  get_U3CMaxAgeU3Ek__BackingField_3() const { return ___U3CMaxAgeU3Ek__BackingField_3; }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E * get_address_of_U3CMaxAgeU3Ek__BackingField_3() { return &___U3CMaxAgeU3Ek__BackingField_3; }
	inline void set_U3CMaxAgeU3Ek__BackingField_3(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  value)
	{
		___U3CMaxAgeU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CMaxStaleU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMaxStaleU3Ek__BackingField_4)); }
	inline bool get_U3CMaxStaleU3Ek__BackingField_4() const { return ___U3CMaxStaleU3Ek__BackingField_4; }
	inline bool* get_address_of_U3CMaxStaleU3Ek__BackingField_4() { return &___U3CMaxStaleU3Ek__BackingField_4; }
	inline void set_U3CMaxStaleU3Ek__BackingField_4(bool value)
	{
		___U3CMaxStaleU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CMaxStaleLimitU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMaxStaleLimitU3Ek__BackingField_5)); }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  get_U3CMaxStaleLimitU3Ek__BackingField_5() const { return ___U3CMaxStaleLimitU3Ek__BackingField_5; }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E * get_address_of_U3CMaxStaleLimitU3Ek__BackingField_5() { return &___U3CMaxStaleLimitU3Ek__BackingField_5; }
	inline void set_U3CMaxStaleLimitU3Ek__BackingField_5(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  value)
	{
		___U3CMaxStaleLimitU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CMinFreshU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMinFreshU3Ek__BackingField_6)); }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  get_U3CMinFreshU3Ek__BackingField_6() const { return ___U3CMinFreshU3Ek__BackingField_6; }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E * get_address_of_U3CMinFreshU3Ek__BackingField_6() { return &___U3CMinFreshU3Ek__BackingField_6; }
	inline void set_U3CMinFreshU3Ek__BackingField_6(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  value)
	{
		___U3CMinFreshU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CMustRevalidateU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CMustRevalidateU3Ek__BackingField_7)); }
	inline bool get_U3CMustRevalidateU3Ek__BackingField_7() const { return ___U3CMustRevalidateU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CMustRevalidateU3Ek__BackingField_7() { return &___U3CMustRevalidateU3Ek__BackingField_7; }
	inline void set_U3CMustRevalidateU3Ek__BackingField_7(bool value)
	{
		___U3CMustRevalidateU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CNoCacheU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CNoCacheU3Ek__BackingField_8)); }
	inline bool get_U3CNoCacheU3Ek__BackingField_8() const { return ___U3CNoCacheU3Ek__BackingField_8; }
	inline bool* get_address_of_U3CNoCacheU3Ek__BackingField_8() { return &___U3CNoCacheU3Ek__BackingField_8; }
	inline void set_U3CNoCacheU3Ek__BackingField_8(bool value)
	{
		___U3CNoCacheU3Ek__BackingField_8 = value;
	}

	inline static int32_t get_offset_of_U3CNoStoreU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CNoStoreU3Ek__BackingField_9)); }
	inline bool get_U3CNoStoreU3Ek__BackingField_9() const { return ___U3CNoStoreU3Ek__BackingField_9; }
	inline bool* get_address_of_U3CNoStoreU3Ek__BackingField_9() { return &___U3CNoStoreU3Ek__BackingField_9; }
	inline void set_U3CNoStoreU3Ek__BackingField_9(bool value)
	{
		___U3CNoStoreU3Ek__BackingField_9 = value;
	}

	inline static int32_t get_offset_of_U3CNoTransformU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CNoTransformU3Ek__BackingField_10)); }
	inline bool get_U3CNoTransformU3Ek__BackingField_10() const { return ___U3CNoTransformU3Ek__BackingField_10; }
	inline bool* get_address_of_U3CNoTransformU3Ek__BackingField_10() { return &___U3CNoTransformU3Ek__BackingField_10; }
	inline void set_U3CNoTransformU3Ek__BackingField_10(bool value)
	{
		___U3CNoTransformU3Ek__BackingField_10 = value;
	}

	inline static int32_t get_offset_of_U3COnlyIfCachedU3Ek__BackingField_11() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3COnlyIfCachedU3Ek__BackingField_11)); }
	inline bool get_U3COnlyIfCachedU3Ek__BackingField_11() const { return ___U3COnlyIfCachedU3Ek__BackingField_11; }
	inline bool* get_address_of_U3COnlyIfCachedU3Ek__BackingField_11() { return &___U3COnlyIfCachedU3Ek__BackingField_11; }
	inline void set_U3COnlyIfCachedU3Ek__BackingField_11(bool value)
	{
		___U3COnlyIfCachedU3Ek__BackingField_11 = value;
	}

	inline static int32_t get_offset_of_U3CPrivateU3Ek__BackingField_12() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CPrivateU3Ek__BackingField_12)); }
	inline bool get_U3CPrivateU3Ek__BackingField_12() const { return ___U3CPrivateU3Ek__BackingField_12; }
	inline bool* get_address_of_U3CPrivateU3Ek__BackingField_12() { return &___U3CPrivateU3Ek__BackingField_12; }
	inline void set_U3CPrivateU3Ek__BackingField_12(bool value)
	{
		___U3CPrivateU3Ek__BackingField_12 = value;
	}

	inline static int32_t get_offset_of_U3CProxyRevalidateU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CProxyRevalidateU3Ek__BackingField_13)); }
	inline bool get_U3CProxyRevalidateU3Ek__BackingField_13() const { return ___U3CProxyRevalidateU3Ek__BackingField_13; }
	inline bool* get_address_of_U3CProxyRevalidateU3Ek__BackingField_13() { return &___U3CProxyRevalidateU3Ek__BackingField_13; }
	inline void set_U3CProxyRevalidateU3Ek__BackingField_13(bool value)
	{
		___U3CProxyRevalidateU3Ek__BackingField_13 = value;
	}

	inline static int32_t get_offset_of_U3CPublicU3Ek__BackingField_14() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CPublicU3Ek__BackingField_14)); }
	inline bool get_U3CPublicU3Ek__BackingField_14() const { return ___U3CPublicU3Ek__BackingField_14; }
	inline bool* get_address_of_U3CPublicU3Ek__BackingField_14() { return &___U3CPublicU3Ek__BackingField_14; }
	inline void set_U3CPublicU3Ek__BackingField_14(bool value)
	{
		___U3CPublicU3Ek__BackingField_14 = value;
	}

	inline static int32_t get_offset_of_U3CSharedMaxAgeU3Ek__BackingField_15() { return static_cast<int32_t>(offsetof(CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB, ___U3CSharedMaxAgeU3Ek__BackingField_15)); }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  get_U3CSharedMaxAgeU3Ek__BackingField_15() const { return ___U3CSharedMaxAgeU3Ek__BackingField_15; }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E * get_address_of_U3CSharedMaxAgeU3Ek__BackingField_15() { return &___U3CSharedMaxAgeU3Ek__BackingField_15; }
	inline void set_U3CSharedMaxAgeU3Ek__BackingField_15(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  value)
	{
		___U3CSharedMaxAgeU3Ek__BackingField_15 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHECONTROLHEADERVALUE_T198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB_H
#ifndef HTTPCONTENTHEADERS_T98D8657573ADACD807B6C941F46E6D69EDFD13A0_H
#define HTTPCONTENTHEADERS_T98D8657573ADACD807B6C941F46E6D69EDFD13A0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpContentHeaders
struct  HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0  : public HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1
{
public:
	// System.Net.Http.HttpContent System.Net.Http.Headers.HttpContentHeaders::content
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * ___content_5;

public:
	inline static int32_t get_offset_of_content_5() { return static_cast<int32_t>(offsetof(HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0, ___content_5)); }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * get_content_5() const { return ___content_5; }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D ** get_address_of_content_5() { return &___content_5; }
	inline void set_content_5(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * value)
	{
		___content_5 = value;
		Il2CppCodeGenWriteBarrier((&___content_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPCONTENTHEADERS_T98D8657573ADACD807B6C941F46E6D69EDFD13A0_H
#ifndef HTTPREQUESTHEADERS_TD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4_H
#define HTTPREQUESTHEADERS_TD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpRequestHeaders
struct  HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4  : public HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1
{
public:
	// System.Nullable`1<System.Boolean> System.Net.Http.Headers.HttpRequestHeaders::expectContinue
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___expectContinue_5;

public:
	inline static int32_t get_offset_of_expectContinue_5() { return static_cast<int32_t>(offsetof(HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4, ___expectContinue_5)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_expectContinue_5() const { return ___expectContinue_5; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_expectContinue_5() { return &___expectContinue_5; }
	inline void set_expectContinue_5(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___expectContinue_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPREQUESTHEADERS_TD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4_H
#ifndef HTTPRESPONSEHEADERS_T51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59_H
#define HTTPRESPONSEHEADERS_T51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.HttpResponseHeaders
struct  HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59  : public HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // HTTPRESPONSEHEADERS_T51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59_H
#ifndef RANGECONDITIONHEADERVALUE_T02095108055FCC5C7AC5D76A3C32BBDE75173D92_H
#define RANGECONDITIONHEADERVALUE_T02095108055FCC5C7AC5D76A3C32BBDE75173D92_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RangeConditionHeaderValue
struct  RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92  : public RuntimeObject
{
public:
	// System.Nullable`1<System.DateTimeOffset> System.Net.Http.Headers.RangeConditionHeaderValue::<Date>k__BackingField
	Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  ___U3CDateU3Ek__BackingField_0;
	// System.Net.Http.Headers.EntityTagHeaderValue System.Net.Http.Headers.RangeConditionHeaderValue::<EntityTag>k__BackingField
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * ___U3CEntityTagU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92, ___U3CDateU3Ek__BackingField_0)); }
	inline Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  get_U3CDateU3Ek__BackingField_0() const { return ___U3CDateU3Ek__BackingField_0; }
	inline Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67 * get_address_of_U3CDateU3Ek__BackingField_0() { return &___U3CDateU3Ek__BackingField_0; }
	inline void set_U3CDateU3Ek__BackingField_0(Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  value)
	{
		___U3CDateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CEntityTagU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92, ___U3CEntityTagU3Ek__BackingField_1)); }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * get_U3CEntityTagU3Ek__BackingField_1() const { return ___U3CEntityTagU3Ek__BackingField_1; }
	inline EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE ** get_address_of_U3CEntityTagU3Ek__BackingField_1() { return &___U3CEntityTagU3Ek__BackingField_1; }
	inline void set_U3CEntityTagU3Ek__BackingField_1(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE * value)
	{
		___U3CEntityTagU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEntityTagU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RANGECONDITIONHEADERVALUE_T02095108055FCC5C7AC5D76A3C32BBDE75173D92_H
#ifndef RETRYCONDITIONHEADERVALUE_TA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6_H
#define RETRYCONDITIONHEADERVALUE_TA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.RetryConditionHeaderValue
struct  RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6  : public RuntimeObject
{
public:
	// System.Nullable`1<System.DateTimeOffset> System.Net.Http.Headers.RetryConditionHeaderValue::<Date>k__BackingField
	Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  ___U3CDateU3Ek__BackingField_0;
	// System.Nullable`1<System.TimeSpan> System.Net.Http.Headers.RetryConditionHeaderValue::<Delta>k__BackingField
	Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  ___U3CDeltaU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6, ___U3CDateU3Ek__BackingField_0)); }
	inline Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  get_U3CDateU3Ek__BackingField_0() const { return ___U3CDateU3Ek__BackingField_0; }
	inline Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67 * get_address_of_U3CDateU3Ek__BackingField_0() { return &___U3CDateU3Ek__BackingField_0; }
	inline void set_U3CDateU3Ek__BackingField_0(Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  value)
	{
		___U3CDateU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CDeltaU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6, ___U3CDeltaU3Ek__BackingField_1)); }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  get_U3CDeltaU3Ek__BackingField_1() const { return ___U3CDeltaU3Ek__BackingField_1; }
	inline Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E * get_address_of_U3CDeltaU3Ek__BackingField_1() { return &___U3CDeltaU3Ek__BackingField_1; }
	inline void set_U3CDeltaU3Ek__BackingField_1(Nullable_1_tA5F97AD8281B6EDEE6731D95047BA50C9996309E  value)
	{
		___U3CDeltaU3Ek__BackingField_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETRYCONDITIONHEADERVALUE_TA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6_H
#ifndef WARNINGHEADERVALUE_T1D84F90682667AD771F203A83680632B0B7E8600_H
#define WARNINGHEADERVALUE_T1D84F90682667AD771F203A83680632B0B7E8600_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.Headers.WarningHeaderValue
struct  WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600  : public RuntimeObject
{
public:
	// System.String System.Net.Http.Headers.WarningHeaderValue::<Agent>k__BackingField
	String_t* ___U3CAgentU3Ek__BackingField_0;
	// System.Int32 System.Net.Http.Headers.WarningHeaderValue::<Code>k__BackingField
	int32_t ___U3CCodeU3Ek__BackingField_1;
	// System.Nullable`1<System.DateTimeOffset> System.Net.Http.Headers.WarningHeaderValue::<Date>k__BackingField
	Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  ___U3CDateU3Ek__BackingField_2;
	// System.String System.Net.Http.Headers.WarningHeaderValue::<Text>k__BackingField
	String_t* ___U3CTextU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CAgentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CAgentU3Ek__BackingField_0)); }
	inline String_t* get_U3CAgentU3Ek__BackingField_0() const { return ___U3CAgentU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CAgentU3Ek__BackingField_0() { return &___U3CAgentU3Ek__BackingField_0; }
	inline void set_U3CAgentU3Ek__BackingField_0(String_t* value)
	{
		___U3CAgentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CAgentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CCodeU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CCodeU3Ek__BackingField_1)); }
	inline int32_t get_U3CCodeU3Ek__BackingField_1() const { return ___U3CCodeU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCodeU3Ek__BackingField_1() { return &___U3CCodeU3Ek__BackingField_1; }
	inline void set_U3CCodeU3Ek__BackingField_1(int32_t value)
	{
		___U3CCodeU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CDateU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CDateU3Ek__BackingField_2)); }
	inline Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  get_U3CDateU3Ek__BackingField_2() const { return ___U3CDateU3Ek__BackingField_2; }
	inline Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67 * get_address_of_U3CDateU3Ek__BackingField_2() { return &___U3CDateU3Ek__BackingField_2; }
	inline void set_U3CDateU3Ek__BackingField_2(Nullable_1_tD63596AAA40EDF5AE4981F1C2B4F3A8A24E1DD67  value)
	{
		___U3CDateU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTextU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600, ___U3CTextU3Ek__BackingField_3)); }
	inline String_t* get_U3CTextU3Ek__BackingField_3() const { return ___U3CTextU3Ek__BackingField_3; }
	inline String_t** get_address_of_U3CTextU3Ek__BackingField_3() { return &___U3CTextU3Ek__BackingField_3; }
	inline void set_U3CTextU3Ek__BackingField_3(String_t* value)
	{
		___U3CTextU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTextU3Ek__BackingField_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // WARNINGHEADERVALUE_T1D84F90682667AD771F203A83680632B0B7E8600_H
#ifndef U3CLOADINTOBUFFERASYNCU3ED__17_T985B7339641D6265E8323E166836949D18BF7ABE_H
#define U3CLOADINTOBUFFERASYNCU3ED__17_T985B7339641D6265E8323E166836949D18BF7ABE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Net.Http.HttpContent/<LoadIntoBufferAsync>d__17
struct  U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE 
{
public:
	// System.Int32 System.Net.Http.HttpContent/<LoadIntoBufferAsync>d__17::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Runtime.CompilerServices.AsyncTaskMethodBuilder System.Net.Http.HttpContent/<LoadIntoBufferAsync>d__17::<>t__builder
	AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  ___U3CU3Et__builder_1;
	// System.Net.Http.HttpContent System.Net.Http.HttpContent/<LoadIntoBufferAsync>d__17::<>4__this
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * ___U3CU3E4__this_2;
	// System.Int64 System.Net.Http.HttpContent/<LoadIntoBufferAsync>d__17::maxBufferSize
	int64_t ___maxBufferSize_3;
	// System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter System.Net.Http.HttpContent/<LoadIntoBufferAsync>d__17::<>u__1
	ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  ___U3CU3Eu__1_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3Et__builder_1() { return static_cast<int32_t>(offsetof(U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE, ___U3CU3Et__builder_1)); }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  get_U3CU3Et__builder_1() const { return ___U3CU3Et__builder_1; }
	inline AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487 * get_address_of_U3CU3Et__builder_1() { return &___U3CU3Et__builder_1; }
	inline void set_U3CU3Et__builder_1(AsyncTaskMethodBuilder_t0CD1893D670405BED201BE8CA6F2E811F2C0F487  value)
	{
		___U3CU3Et__builder_1 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE, ___U3CU3E4__this_2)); }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_maxBufferSize_3() { return static_cast<int32_t>(offsetof(U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE, ___maxBufferSize_3)); }
	inline int64_t get_maxBufferSize_3() const { return ___maxBufferSize_3; }
	inline int64_t* get_address_of_maxBufferSize_3() { return &___maxBufferSize_3; }
	inline void set_maxBufferSize_3(int64_t value)
	{
		___maxBufferSize_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3Eu__1_4() { return static_cast<int32_t>(offsetof(U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE, ___U3CU3Eu__1_4)); }
	inline ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  get_U3CU3Eu__1_4() const { return ___U3CU3Eu__1_4; }
	inline ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874 * get_address_of_U3CU3Eu__1_4() { return &___U3CU3Eu__1_4; }
	inline void set_U3CU3Eu__1_4(ConfiguredTaskAwaiter_tF1AAA16B8A1250CA037E32157A3424CD2BA47874  value)
	{
		___U3CU3Eu__1_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CLOADINTOBUFFERASYNCU3ED__17_T985B7339641D6265E8323E166836949D18BF7ABE_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6100 = { sizeof (Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6100[3] = 
{
	Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD::get_offset_of_U3CNameU3Ek__BackingField_0(),
	Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD::get_offset_of_U3CValueU3Ek__BackingField_1(),
	Parameter_t9771CF59A5BAB5504895474F08214C79A68679AD::get_offset_of_U3CTypeU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6101 = { sizeof (RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1), -1, sizeof(RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6101[28] = 
{
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_version_0(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_HttpFactory_1(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CContentHandlersU3Ek__BackingField_2(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CAcceptTypesU3Ek__BackingField_3(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CDefaultParametersU3Ek__BackingField_4(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CMaxRedirectsU3Ek__BackingField_5(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CClientCertificatesU3Ek__BackingField_6(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CProxyU3Ek__BackingField_7(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CFollowRedirectsU3Ek__BackingField_8(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CCookieContainerU3Ek__BackingField_9(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CUserAgentU3Ek__BackingField_10(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CTimeoutU3Ek__BackingField_11(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CUseSynchronizationContextU3Ek__BackingField_12(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of_U3CAuthenticatorU3Ek__BackingField_13(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1::get_offset_of__baseUrl_14(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_15(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_16(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_17(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_18(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache4_19(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache5_20(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache6_21(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache7_22(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache8_23(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cache9_24(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__amU24cacheA_25(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__mgU24cache0_26(),
	RestClient_tCA386028DE94D9A12431EF5FCE26A3961C8323F1_StaticFields::get_offset_of_U3CU3Ef__mgU24cache1_27(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6102 = { sizeof (U3CConfigureHttpU3Ec__AnonStorey0_t4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6102[1] = 
{
	U3CConfigureHttpU3Ec__AnonStorey0_t4D9F6674F2F7DD0E2CBAD13B5207148CF65DBF27::get_offset_of_p_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6103 = { sizeof (U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6103[4] = 
{
	U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE::get_offset_of_request_0(),
	U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE::get_offset_of_asyncHandle_1(),
	U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE::get_offset_of_callback_2(),
	U3CExecuteAsyncU3Ec__AnonStorey1_t478D7C99B107EFE165B42EDF9163DD211FFC01DE::get_offset_of_U24this_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6104 = { sizeof (U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6104[2] = 
{
	U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066::get_offset_of_ctx_0(),
	U3CExecuteAsyncU3Ec__AnonStorey2_t66B943A70AF37AAAF83BD3128510031DC1BE7066::get_offset_of_cb_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6105 = { sizeof (U3CExecuteAsyncU3Ec__AnonStorey3_t6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6105[2] = 
{
	U3CExecuteAsyncU3Ec__AnonStorey3_t6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94::get_offset_of_resp_0(),
	U3CExecuteAsyncU3Ec__AnonStorey3_t6546FFA0BC3499C79E1F8F0FBB4BD54E61A20B94::get_offset_of_U3CU3Ef__refU242_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6106 = { sizeof (RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E), -1, sizeof(RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6106[12] = 
{
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CJsonSerializerU3Ek__BackingField_0(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CXmlSerializerU3Ek__BackingField_1(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CResponseWriterU3Ek__BackingField_2(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CParametersU3Ek__BackingField_3(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CFilesU3Ek__BackingField_4(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of__method_5(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CResourceU3Ek__BackingField_6(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of__requestFormat_7(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3COnBeforeDeserializationU3Ek__BackingField_8(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CCredentialsU3Ek__BackingField_9(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E::get_offset_of_U3CTimeoutU3Ek__BackingField_10(),
	RestRequest_tB40F117F5D3AF6C850E10C7B45B179A045C94D8E_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6107 = { sizeof (RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6107[14] = 
{
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CCommentU3Ek__BackingField_0(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CCommentUriU3Ek__BackingField_1(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CDiscardU3Ek__BackingField_2(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CDomainU3Ek__BackingField_3(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CExpiredU3Ek__BackingField_4(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CExpiresU3Ek__BackingField_5(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CHttpOnlyU3Ek__BackingField_6(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CNameU3Ek__BackingField_7(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CPathU3Ek__BackingField_8(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CPortU3Ek__BackingField_9(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CSecureU3Ek__BackingField_10(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CTimeStampU3Ek__BackingField_11(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CValueU3Ek__BackingField_12(),
	RestResponseCookie_t24C3016B845BC02B5D718305542C1B3D18799ED7::get_offset_of_U3CVersionU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6108 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6109 = { sizeof (JsonSerializer_t4D0574E68181735C10A438200A18C404BEF321B9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6109[2] = 
{
	JsonSerializer_t4D0574E68181735C10A438200A18C404BEF321B9::get_offset_of_U3CNamespaceU3Ek__BackingField_0(),
	JsonSerializer_t4D0574E68181735C10A438200A18C404BEF321B9::get_offset_of_U3CContentTypeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6110 = { sizeof (SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6110[5] = 
{
	SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1::get_offset_of_U3CNameU3Ek__BackingField_0(),
	SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1::get_offset_of_U3CAttributeU3Ek__BackingField_1(),
	SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1::get_offset_of_U3CCultureU3Ek__BackingField_2(),
	SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1::get_offset_of_U3CNameStyleU3Ek__BackingField_3(),
	SerializeAsAttribute_t068C27F705FD2F2725572072BE7962F2DD0829E1::get_offset_of_U3CIndexU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6111 = { sizeof (NameStyle_tAE11853F499C718E4E4C5B55C92D56F84F28747E)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6111[5] = 
{
	NameStyle_tAE11853F499C718E4E4C5B55C92D56F84F28747E::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6112 = { sizeof (XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC), -1, sizeof(XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6112[8] = 
{
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC::get_offset_of_U3CRootElementU3Ek__BackingField_0(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC::get_offset_of_U3CNamespaceU3Ek__BackingField_1(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC::get_offset_of_U3CDateFormatU3Ek__BackingField_2(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC::get_offset_of_U3CContentTypeU3Ek__BackingField_3(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields::get_offset_of_U3CU3Ef__amU24cache0_4(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields::get_offset_of_U3CU3Ef__amU24cache1_5(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields::get_offset_of_U3CU3Ef__amU24cache2_6(),
	XmlSerializer_tB2935B957EC47B53A72EDB4DAD7A2F2385C1CFBC_StaticFields::get_offset_of_U3CU3Ef__amU24cache3_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6113 = { sizeof (JsonObject_t0589456884908E3386A5EF7E930AFCE86C13EE6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6113[1] = 
{
	JsonObject_t0589456884908E3386A5EF7E930AFCE86C13EE6C::get_offset_of__members_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6114 = { sizeof (SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0), -1, sizeof(SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6114[2] = 
{
	SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0_StaticFields::get_offset_of_currentJsonSerializerStrategy_0(),
	SimpleJson_tC36B96B6997F45DC2BF553850B958630143C62C0_StaticFields::get_offset_of_pocoJsonSerializerStrategy_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6115 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6116 = { sizeof (PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F), -1, sizeof(PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6116[2] = 
{
	PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F::get_offset_of_CacheResolver_0(),
	PocoJsonSerializerStrategy_tC7A471C0000802F19DE9EF6B444591B71E8B598F_StaticFields::get_offset_of_Iso8601Format_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6117 = { sizeof (GetHandler_tBDF089BE73CB25C967659A15E79BC8D364AB5EE0), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6118 = { sizeof (SetHandler_t68EE20C5C88F8ABD427D46C3EF1FB51BC5C48BA8), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6119 = { sizeof (MemberMapLoader_t18793B8C3AD9903BB61ED00F86C9AFE04A769B91), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6120 = { sizeof (CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21), -1, sizeof(CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6120[3] = 
{
	CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21::get_offset_of__memberMapLoader_0(),
	CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21::get_offset_of__memberMapsCache_1(),
	CacheResolver_t755F33B018423E7099C08A0C88F7C43CE1457E21_StaticFields::get_offset_of_ConstructorCache_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6121 = { sizeof (CtorDelegate_t3D608896A17C00ACF115E1896331E79F987144D7), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6122 = { sizeof (MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6122[4] = 
{
	MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18::get_offset_of_MemberInfo_0(),
	MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18::get_offset_of_Type_1(),
	MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18::get_offset_of_Getter_2(),
	MemberMap_tFBE96498C6D20B6DF316D3933CD05F540C1DAE18::get_offset_of_Setter_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6123 = { sizeof (U3CCreateGetHandlerU3Ec__AnonStorey1_tE9CC470A6BAC1499033EC744F320F1C38B0D8F87), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6123[1] = 
{
	U3CCreateGetHandlerU3Ec__AnonStorey1_tE9CC470A6BAC1499033EC744F320F1C38B0D8F87::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6124 = { sizeof (U3CCreateSetHandlerU3Ec__AnonStorey2_t560CD52CBB219DBA1BC7A048F89883B91FD6E10F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6124[1] = 
{
	U3CCreateSetHandlerU3Ec__AnonStorey2_t560CD52CBB219DBA1BC7A048F89883B91FD6E10F::get_offset_of_fieldInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6125 = { sizeof (U3CCreateGetHandlerU3Ec__AnonStorey3_t060E5A44382EF6E752BA67CBF143EEACCF6E16E0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6125[1] = 
{
	U3CCreateGetHandlerU3Ec__AnonStorey3_t060E5A44382EF6E752BA67CBF143EEACCF6E16E0::get_offset_of_getMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6126 = { sizeof (U3CCreateSetHandlerU3Ec__AnonStorey4_t7F9F4F5A9E16563FA111643F344C7692D8F5D8CA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6126[1] = 
{
	U3CCreateSetHandlerU3Ec__AnonStorey4_t7F9F4F5A9E16563FA111643F344C7692D8F5D8CA::get_offset_of_setMethodInfo_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6127 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6127[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6128 = { sizeof (RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6128[1] = 
{
	RestRequestAsyncHandle_t502D2598B36C99705E6214E833A4E1C60A0A24E1::get_offset_of_WebRequest_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6129 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6129[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6130 = { sizeof (U3CModuleU3E_t62127DAF1A487C524F4DFBD6F8A0A1F2619F78B6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6131 = { sizeof (HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7), -1, sizeof(HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6131[7] = 
{
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7_StaticFields::get_offset_of_TimeoutDefault_2(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_base_address_3(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_cts_4(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_disposed_5(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_headers_6(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_buffer_size_7(),
	HttpClient_tC7477E4B30DD5CFE1B41DD9CD2EA1F83DB4DE9E7::get_offset_of_timeout_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6132 = { sizeof (U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6132[10] = 
{
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_cancellationToken_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_request_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_completionOption_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3CresponseU3E5__1_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3ClctsU3E5__2_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3CU3Eu__1_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncWorkerU3Ed__47_t47F0FB04707E1418B40449E9FC6FE1F2FEAB0941::get_offset_of_U3CU3Eu__2_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6133 = { sizeof (HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049), -1, sizeof(HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6133[15] = 
{
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049_StaticFields::get_offset_of_groupCounter_0(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_allowAutoRedirect_1(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_automaticDecompression_2(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_cookieContainer_3(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_credentials_4(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_maxAutomaticRedirections_5(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_maxRequestContentBufferSize_6(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_preAuthenticate_7(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_proxy_8(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_useCookies_9(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_useDefaultCredentials_10(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_useProxy_11(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_sentRequest_12(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_connectionGroupName_13(),
	HttpClientHandler_t029F50F2D24A50A3C90A8F8192B8E82CD0076049::get_offset_of_disposed_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6134 = { sizeof (U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD), -1, sizeof(U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6134[3] = 
{
	U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields::get_offset_of_U3CU3E9__61_0_1(),
	U3CU3Ec_t1C81A6E07AAEB8BA056DC9E28917A5FBD6689BBD_StaticFields::get_offset_of_U3CU3E9__64_0_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6135 = { sizeof (U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6135[14] = 
{
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_request_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_cancellationToken_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CwrequestU3E5__1_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CcontentU3E5__2_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CstreamU3E5__3_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CwresponseU3E5__4_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3E7__wrap1_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3Eu__1_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3Eu__2_11() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3Eu__3_12() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CSendAsyncU3Ed__64_t27F5FC426FC9AF2DEA087F38EB6F330957AC16F0::get_offset_of_U3CU3Eu__4_13() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6136 = { sizeof (HttpCompletionOption_t58CEC23E366EE12A978C957DE797B907F82ACE0B)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6136[3] = 
{
	HttpCompletionOption_t58CEC23E366EE12A978C957DE797B907F82ACE0B::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6137 = { sizeof (HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6137[3] = 
{
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D::get_offset_of_buffer_0(),
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D::get_offset_of_disposed_1(),
	HttpContent_t678722F60CA151DD19AFCA917657BE24F111A41D::get_offset_of_headers_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6138 = { sizeof (FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6138[1] = 
{
	FixedMemoryStream_t181662654A3A2002A1F5D33E7747A9E12DAD139C::get_offset_of_maxSize_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6139 = { sizeof (U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6139[5] = 
{
	U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE::get_offset_of_U3CU3E1__state_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE::get_offset_of_U3CU3Et__builder_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE::get_offset_of_U3CU3E4__this_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE::get_offset_of_maxBufferSize_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	U3CLoadIntoBufferAsyncU3Ed__17_t985B7339641D6265E8323E166836949D18BF7ABE::get_offset_of_U3CU3Eu__1_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6140 = { sizeof (HttpMessageHandler_t0094EF2850CF6420CBFC3952ED337AD381644894), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6141 = { sizeof (HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6141[2] = 
{
	HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72::get_offset_of_handler_0(),
	HttpMessageInvoker_t7270E2BED3201CE430D6C4BECF923454AA526A72::get_offset_of_disposeHandler_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6142 = { sizeof (HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220), -1, sizeof(HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6142[8] = 
{
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_delete_method_0(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_get_method_1(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_head_method_2(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_options_method_3(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_post_method_4(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_put_method_5(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220_StaticFields::get_offset_of_trace_method_6(),
	HttpMethod_tC762DCC43B5B2C08E8744B8C1C866D8E55CF6220::get_offset_of_method_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6143 = { sizeof (HttpRequestException_tD4D14C20B90F362C83781589E7508A05E7060349), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6144 = { sizeof (HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6144[7] = 
{
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_headers_0(),
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_method_1(),
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_version_2(),
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_uri_3(),
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_is_used_4(),
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_disposed_5(),
	HttpRequestMessage_tBBC9EBC5D6C1B7E30F9927AABBDB65792FFB5427::get_offset_of_U3CContentU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6145 = { sizeof (HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6145[7] = 
{
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_headers_0(),
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_reasonPhrase_1(),
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_statusCode_2(),
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_version_3(),
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_disposed_4(),
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_U3CContentU3Ek__BackingField_5(),
	HttpResponseMessage_t619DB9FDC8E63CDF2D0A314B61A4C6609A43F904::get_offset_of_U3CRequestMessageU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6146 = { sizeof (StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6146[5] = 
{
	StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF::get_offset_of_content_3(),
	StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF::get_offset_of_bufferSize_4(),
	StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF::get_offset_of_cancellationToken_5(),
	StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF::get_offset_of_startPosition_6(),
	StreamContent_t0E4C08F0720024BFEBB667CCC40DA9E947EFF3FF::get_offset_of_contentCopied_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6147 = { sizeof (AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6147[2] = 
{
	AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6::get_offset_of_U3CParameterU3Ek__BackingField_0(),
	AuthenticationHeaderValue_tC285A16C25538CA86FA59B88CC25E6999BB5F1A6::get_offset_of_U3CSchemeU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6148 = { sizeof (CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6148[16] = 
{
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_extensions_0(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_no_cache_headers_1(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_private_headers_2(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMaxAgeU3Ek__BackingField_3(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMaxStaleU3Ek__BackingField_4(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMaxStaleLimitU3Ek__BackingField_5(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMinFreshU3Ek__BackingField_6(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CMustRevalidateU3Ek__BackingField_7(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CNoCacheU3Ek__BackingField_8(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CNoStoreU3Ek__BackingField_9(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CNoTransformU3Ek__BackingField_10(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3COnlyIfCachedU3Ek__BackingField_11(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CPrivateU3Ek__BackingField_12(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CProxyRevalidateU3Ek__BackingField_13(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CPublicU3Ek__BackingField_14(),
	CacheControlHeaderValue_t198E32C9C8BDE34E39F3463BAD0AFEDB536C64FB::get_offset_of_U3CSharedMaxAgeU3Ek__BackingField_15(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6149 = { sizeof (CollectionExtensions_tF55A4677D40CF6208C37CAB46E039D5DF6DC790B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6150 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6151 = { sizeof (CollectionParser_t1AC0B37276003DB1C0C45E25DFF466DDDBFD24FD), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6152 = { sizeof (ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6152[2] = 
{
	ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0::get_offset_of_dispositionType_0(),
	ContentDispositionHeaderValue_t424BAD39E99F23CE48CF86744B638923E2B539B0::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6153 = { sizeof (ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6153[4] = 
{
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_unit_0(),
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_U3CFromU3Ek__BackingField_1(),
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_U3CLengthU3Ek__BackingField_2(),
	ContentRangeHeaderValue_t7910E521DFA28C03F4E0D9C89D87232B6882D11E::get_offset_of_U3CToU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6154 = { sizeof (EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE), -1, sizeof(EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6154[3] = 
{
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE_StaticFields::get_offset_of_any_0(),
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE::get_offset_of_U3CIsWeakU3Ek__BackingField_1(),
	EntityTagHeaderValue_tF1552D0AB7C248D3D5DB70E95AA5C691C4AB8DCE::get_offset_of_U3CTagU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6155 = { sizeof (HashCodeCalculator_t0AB58AEF6A3BC2B0CBDD73B6BD0087888D401994), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6156 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6157 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6158 = { sizeof (HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6158[4] = 
{
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_AllowsMany_0(),
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_HeaderKind_1(),
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_Name_2(),
	HeaderInfo_t067D19E53F90FB324CA05965F2429843D223C61C::get_offset_of_U3CCustomToStringU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6159 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6159[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6160 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6160[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6161 = { sizeof (HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6161[1] = 
{
	HttpContentHeaders_t98D8657573ADACD807B6C941F46E6D69EDFD13A0::get_offset_of_content_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6162 = { sizeof (HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6162[5] = 
{
	HttpHeaderKind_tB1DD187D27BCE76C6EC20628350D06F1802F9F97::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6163 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable6163[4] = 
{
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6164 = { sizeof (HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1), -1, sizeof(HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6164[5] = 
{
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1_StaticFields::get_offset_of_known_headers_0(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_headers_1(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_HeaderKind_2(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_connectionclose_3(),
	HttpHeaders_tF0E0E01200BD0D8141489FB370D60A9F4DBF42A1::get_offset_of_transferEncodingChunked_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6165 = { sizeof (HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6165[3] = 
{
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F::get_offset_of_Parsed_0(),
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F::get_offset_of_values_1(),
	HeaderBucket_tB4D5217F305487EEBBB21B36236E85CFD40E036F::get_offset_of_CustomToString_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6166 = { sizeof (U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6166[4] = 
{
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E1__state_0(),
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E2__current_1(),
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E4__this_2(),
	U3CGetEnumeratorU3Ed__19_tA84EA98FFD73FFAB0621F7C20E8D8C0D560E6B4A::get_offset_of_U3CU3E7__wrap1_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6167 = { sizeof (HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6167[1] = 
{
	HttpRequestHeaders_tD29E9D795891A7A0D4FE8C8AA7BF0E756FCA40D4::get_offset_of_expectContinue_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6168 = { sizeof (U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107), -1, sizeof(U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6168[5] = 
{
	U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields::get_offset_of_U3CU3E9__19_0_1(),
	U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields::get_offset_of_U3CU3E9__22_0_2(),
	U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields::get_offset_of_U3CU3E9__29_0_3(),
	U3CU3Ec_t54A86B32672AD46F6B14A5F8CF0DED85254C5107_StaticFields::get_offset_of_U3CU3E9__71_0_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6169 = { sizeof (HttpResponseHeaders_t51F7C46C9CE47E3B424A6137CE8F5BCA4E2B1D59), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6170 = { sizeof (Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846)+ sizeof (RuntimeObject), sizeof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846 ), sizeof(Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6170[4] = 
{
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846_StaticFields::get_offset_of_Empty_0(),
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846::get_offset_of_type_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846::get_offset_of_U3CStartPositionU3Ek__BackingField_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Token_t5CC7CC2006CB67D86AED528ECA839915D10D4846::get_offset_of_U3CEndPositionU3Ek__BackingField_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6171 = { sizeof (Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable6171[11] = 
{
	Type_tEBD57B8CE42E0A8AA52CACE5469267D06FB52D72::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6172 = { sizeof (Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3), -1, sizeof(Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6172[5] = 
{
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields::get_offset_of_token_chars_0(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields::get_offset_of_last_token_char_1(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3_StaticFields::get_offset_of_dt_formats_2(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3::get_offset_of_s_3(),
	Lexer_t1A8C702FA3384E0F6E737F7E4AE9B1AC68C8F1B3::get_offset_of_pos_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6173 = { sizeof (MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6173[2] = 
{
	MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1::get_offset_of_parameters_0(),
	MediaTypeHeaderValue_tB4CFA361861F6A00B6F13F7BB5B4E7425415FDE1::get_offset_of_media_type_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6174 = { sizeof (MediaTypeWithQualityHeaderValue_tA90D440F6C0876845E895A0A3C61FA36CA2B120E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6175 = { sizeof (NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6175[2] = 
{
	NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F::get_offset_of_value_0(),
	NameValueHeaderValue_tD6B9C6286E270D5EDCDA199D85D786492D15CD1F::get_offset_of_U3CNameU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6176 = { sizeof (NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6176[1] = 
{
	NameValueWithParametersHeaderValue_t9E5A12E229B5C94317A9544AA090F35F3CBFBDBF::get_offset_of_parameters_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6177 = { sizeof (Parser_tC9CE5F5FFDD4927D039C3189B5945DA5EF958025), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6178 = { sizeof (Token_t63D07F66DA54B86A2FC571E8F88F92327CD51F13), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6179 = { sizeof (DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F), -1, sizeof(DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6179[1] = 
{
	DateTime_t0F9E1FF7B5D00351111593AEA0FA9034CB63108F_StaticFields::get_offset_of_ToString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6180 = { sizeof (U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE), -1, sizeof(U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6180[1] = 
{
	U3CU3Ec_t06D259B007D5CD69873ECF647B2C5E37CD9262AE_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6181 = { sizeof (EmailAddress_t34037A92039853D504922D84E1512E6BE4FBDE1B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6182 = { sizeof (Host_t225394EBFA7427D924DC794F6FEB33C7B19E6583), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6183 = { sizeof (Int_t9123F050289CA0271A077EFF47440CBD5C366316), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6184 = { sizeof (Long_t28918BF6DBAB433B07F2F91D220DF42F896C5C63), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6185 = { sizeof (MD5_tBF240EC56773202152DBB5C53796D718895EC4EC), -1, sizeof(MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6185[1] = 
{
	MD5_tBF240EC56773202152DBB5C53796D718895EC4EC_StaticFields::get_offset_of_ToString_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6186 = { sizeof (U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F), -1, sizeof(U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable6186[1] = 
{
	U3CU3Ec_t2AA8F739A8D4A0762D1A7E2F72AB86B512D78F0F_StaticFields::get_offset_of_U3CU3E9_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6187 = { sizeof (TimeSpanSeconds_t147412A8A309180783B8369B158301EFA80A9DA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6188 = { sizeof (Uri_tAA37A4AC2B4CCC173820F682599007F88D08407D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6189 = { sizeof (ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6189[2] = 
{
	ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5::get_offset_of_U3CNameU3Ek__BackingField_0(),
	ProductHeaderValue_t3D9BB218AE50BBB9DA4CBA98D21BEDEF713EBDF5::get_offset_of_U3CVersionU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6190 = { sizeof (ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6190[2] = 
{
	ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246::get_offset_of_U3CCommentU3Ek__BackingField_0(),
	ProductInfoHeaderValue_t2E8EC6871340B4ABC59FE4CA932EABD0E41C2246::get_offset_of_U3CProductU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6191 = { sizeof (RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6191[2] = 
{
	RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92::get_offset_of_U3CDateU3Ek__BackingField_0(),
	RangeConditionHeaderValue_t02095108055FCC5C7AC5D76A3C32BBDE75173D92::get_offset_of_U3CEntityTagU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6192 = { sizeof (RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6192[2] = 
{
	RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723::get_offset_of_ranges_0(),
	RangeHeaderValue_t2763CB54E86AD644BD547989483DB862B443D723::get_offset_of_unit_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6193 = { sizeof (RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6193[2] = 
{
	RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664::get_offset_of_U3CFromU3Ek__BackingField_0(),
	RangeItemHeaderValue_t24C458BCBACB895AC9E6403E55B458559124D664::get_offset_of_U3CToU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6194 = { sizeof (RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6194[2] = 
{
	RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6::get_offset_of_U3CDateU3Ek__BackingField_0(),
	RetryConditionHeaderValue_tA7EB5D0221D7DAB02971AC357026A5BDA6CEB9D6::get_offset_of_U3CDeltaU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6195 = { sizeof (StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6195[2] = 
{
	StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8::get_offset_of_U3CQualityU3Ek__BackingField_0(),
	StringWithQualityHeaderValue_t1F4318F29FD5B450365AFED6632DFC05B15F7AD8::get_offset_of_U3CValueU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6196 = { sizeof (TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6196[2] = 
{
	TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C::get_offset_of_value_0(),
	TransferCodingHeaderValue_t26DE304410101552CB12F49B948B041AD63E0E6C::get_offset_of_parameters_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6197 = { sizeof (TransferCodingWithQualityHeaderValue_tCCBC3E7752056FE94AF475137682B15E84FA626C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6198 = { sizeof (ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6198[4] = 
{
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CCommentU3Ek__BackingField_0(),
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CProtocolNameU3Ek__BackingField_1(),
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CProtocolVersionU3Ek__BackingField_2(),
	ViaHeaderValue_tA4BE22B235705360625337A3278F31BD5FEEFB6B::get_offset_of_U3CReceivedByU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize6199 = { sizeof (WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable6199[4] = 
{
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CAgentU3Ek__BackingField_0(),
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CCodeU3Ek__BackingField_1(),
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CDateU3Ek__BackingField_2(),
	WarningHeaderValue_t1D84F90682667AD771F203A83680632B0B7E8600::get_offset_of_U3CTextU3Ek__BackingField_3(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
