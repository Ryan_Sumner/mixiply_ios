﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// Jint.Engine
struct Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D;
// Jint.Native.JsValue
struct JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19;
// Jint.Native.JsValue[]
struct JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88;
// Jint.Native.Object.ObjectInstance
struct ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900;
// Jint.Parser.Ast.BlockStatement
struct BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E;
// Jint.Parser.Ast.Expression
struct Expression_tAC599936AD6D021EA6A3A695C803975140919ADB;
// Jint.Parser.Ast.IPropertyKeyExpression
struct IPropertyKeyExpression_t9FE67481EF84DF157EDBBA88D5447939D099635B;
// Jint.Parser.Ast.Identifier
struct Identifier_t50FB8837E02860B417C98D78294E352C617900EB;
// Jint.Parser.Ast.Statement
struct Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0;
// Jint.Parser.Ast.SyntaxNode
struct SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B;
// Jint.Parser.JavaScriptParser/Extra
struct Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5;
// Jint.Parser.Location
struct Location_t9639547EECD85868A9B1627343BF1C80DE46B783;
// Jint.Parser.Token
struct Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37;
// Jint.Runtime.Debugger.DebugHandler
struct DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296;
// Jint.Runtime.Environments.EnvironmentRecord
struct EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B;
// Jint.Runtime.Environments.LexicalEnvironment
struct LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026;
// Jint.Runtime.Interop.ObjectWrapper
struct ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5;
// System.Action`2<Jint.Native.JsValue,Jint.Native.JsValue>
struct Action_2_t06F7602F0814DDCDF44485CB5E571029718A58BC;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Jint.Runtime.CallStackElement,System.Int32>
struct Dictionary_2_t10E2334BD922D6E4791209EC9F1821C67F397912;
// System.Collections.Generic.Dictionary`2<System.String,Jint.Native.JsValue>
struct Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B;
// System.Collections.Generic.HashSet`1<System.String>
struct HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A;
// System.Collections.Generic.ICollection`1<Jint.Parser.Ast.Statement>
struct ICollection_1_tD7981998581C7AA8A8C3C2AFBC5F2737C7BB9804;
// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>
struct IDictionary_2_tA1DF5A6B6E4E121455B7694170DC28D1CBB47AED;
// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Environments.Binding>
struct IDictionary_2_t05E4D01489E124C0D44044B1540E400B303BF9E0;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.CatchClause>
struct IEnumerable_1_t0A875B5A97DD9A7D0D4B0F9E2B1DD30BAF65DF67;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Expression>
struct IEnumerable_1_tFA10332CDD52A35EAD70D326B43B302663CB4211;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Identifier>
struct IEnumerable_1_t67C57F8B6BBDD426AC0EBF8562D184218D10F0B7;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Property>
struct IEnumerable_1_tC78E73C3F52B21F29EB53396517A9354F9613F91;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Statement>
struct IEnumerable_1_t30C46E46B9D96440CBF85D687834EC79AB9625CA;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.SwitchCase>
struct IEnumerable_1_t65A1A16F36B8486C7E4A94E75E43752A76A3ED6E;
// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.VariableDeclarator>
struct IEnumerable_1_t627651FF52767B01FE73D1A35B91915CCBA5A651;
// System.Collections.Generic.IList`1<Jint.Parser.Ast.Expression>
struct IList_1_t3C8C78E3011FA0DCC9FDB477336D14B2623EAF00;
// System.Collections.Generic.IList`1<Jint.Parser.Ast.FunctionDeclaration>
struct IList_1_t2860162AA87FEFE8AB6C9FF1B62D65B05FFB85D6;
// System.Collections.Generic.IList`1<Jint.Parser.Ast.VariableDeclaration>
struct IList_1_tBE92539C803707E18C017C60A81C3045ED962A34;
// System.Collections.Generic.List`1<Jint.Parser.Comment>
struct List_1_t5568459CBFCEB189F889690E2846CB4783B8C909;
// System.Collections.Generic.List`1<Jint.Parser.ParserException>
struct List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1;
// System.Collections.Generic.List`1<Jint.Parser.Token>
struct List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905;
// System.Collections.Generic.Stack`1<Jint.Parser.IFunctionScope>
struct Stack_1_t4C250BB2D2BCCCC50AFECE824126AAB97836ECC4;
// System.Collections.Generic.Stack`1<Jint.Parser.IVariableScope>
struct Stack_1_tC53B481C4C5615750A5D4269B4C01695C4326640;
// System.Collections.Generic.Stack`1<Jint.Runtime.CallStackElement>
struct Stack_1_tD2EAC4775133AB949BFC40D737A35CBC6CDED11B;
// System.Collections.Generic.Stack`1<System.Int32>
struct Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A;
// System.Collections.Generic.Stack`1<System.String>
struct Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Func`2<<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>,System.Boolean>
struct Func_2_tC8337B4C10DC835F5F9A6BE8E4C9BB00DE98DE2A;
// System.Func`2<<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>,System.Reflection.PropertyInfo>
struct Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC;
// System.Func`2<<>f__AnonymousType1`2<System.Type,System.Reflection.MethodInfo>,System.Reflection.MethodInfo>
struct Func_2_tBCEAEF263E293ADF6DAB97525CFB89FD930716B1;
// System.Func`2<Jint.Runtime.CallStackElement,System.String>
struct Func_2_tBD3331CD03481BE2DB45C07A63C2185894EAEE6E;
// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.String>
struct Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759;
// System.Func`2<System.Reflection.ParameterInfo,System.Boolean>
struct Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A;
// System.Func`2<System.Reflection.PropertyInfo,System.Boolean>
struct Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>>
struct Func_2_tE1A6E27E679D795C2ED310502F7ABD12212F4568;
// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>>
struct Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43;
// System.Func`3<System.Type,System.Reflection.MethodInfo,<>f__AnonymousType1`2<System.Type,System.Reflection.MethodInfo>>
struct Func_3_t4E3965D24D0CCD653477F41A012FD12131BFEF7F;
// System.Func`3<System.Type,System.Reflection.PropertyInfo,<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>>
struct Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.MethodInfo[]
struct MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Type
struct Type_t;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#define OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Object.ObjectInstance
struct  ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900  : public RuntimeObject
{
public:
	// Jint.Engine Jint.Native.Object.ObjectInstance::<Engine>k__BackingField
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ___U3CEngineU3Ek__BackingField_0;
	// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor> Jint.Native.Object.ObjectInstance::<Properties>k__BackingField
	RuntimeObject* ___U3CPropertiesU3Ek__BackingField_1;
	// Jint.Native.Object.ObjectInstance Jint.Native.Object.ObjectInstance::<Prototype>k__BackingField
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * ___U3CPrototypeU3Ek__BackingField_2;
	// System.Boolean Jint.Native.Object.ObjectInstance::<Extensible>k__BackingField
	bool ___U3CExtensibleU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CEngineU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CEngineU3Ek__BackingField_0)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get_U3CEngineU3Ek__BackingField_0() const { return ___U3CEngineU3Ek__BackingField_0; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of_U3CEngineU3Ek__BackingField_0() { return &___U3CEngineU3Ek__BackingField_0; }
	inline void set_U3CEngineU3Ek__BackingField_0(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		___U3CEngineU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CEngineU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CPropertiesU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CPropertiesU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CPropertiesU3Ek__BackingField_1() const { return ___U3CPropertiesU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CPropertiesU3Ek__BackingField_1() { return &___U3CPropertiesU3Ek__BackingField_1; }
	inline void set_U3CPropertiesU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CPropertiesU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPropertiesU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CPrototypeU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CPrototypeU3Ek__BackingField_2)); }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * get_U3CPrototypeU3Ek__BackingField_2() const { return ___U3CPrototypeU3Ek__BackingField_2; }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 ** get_address_of_U3CPrototypeU3Ek__BackingField_2() { return &___U3CPrototypeU3Ek__BackingField_2; }
	inline void set_U3CPrototypeU3Ek__BackingField_2(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * value)
	{
		___U3CPrototypeU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CPrototypeU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CExtensibleU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900, ___U3CExtensibleU3Ek__BackingField_3)); }
	inline bool get_U3CExtensibleU3Ek__BackingField_3() const { return ___U3CExtensibleU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CExtensibleU3Ek__BackingField_3() { return &___U3CExtensibleU3Ek__BackingField_3; }
	inline void set_U3CExtensibleU3Ek__BackingField_3(bool value)
	{
		___U3CExtensibleU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTINSTANCE_T9A33413FF3D113AEBA2D280D5498C04222E32900_H
#ifndef COMMENT_T3B095647E0F42D26F18855F536600AB51687CAC1_H
#define COMMENT_T3B095647E0F42D26F18855F536600AB51687CAC1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Comment
struct  Comment_t3B095647E0F42D26F18855F536600AB51687CAC1  : public RuntimeObject
{
public:
	// Jint.Parser.Location Jint.Parser.Comment::Location
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ___Location_0;
	// System.Int32[] Jint.Parser.Comment::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_1;
	// System.String Jint.Parser.Comment::Type
	String_t* ___Type_2;
	// System.String Jint.Parser.Comment::Value
	String_t* ___Value_3;

public:
	inline static int32_t get_offset_of_Location_0() { return static_cast<int32_t>(offsetof(Comment_t3B095647E0F42D26F18855F536600AB51687CAC1, ___Location_0)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get_Location_0() const { return ___Location_0; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of_Location_0() { return &___Location_0; }
	inline void set_Location_0(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		___Location_0 = value;
		Il2CppCodeGenWriteBarrier((&___Location_0), value);
	}

	inline static int32_t get_offset_of_Range_1() { return static_cast<int32_t>(offsetof(Comment_t3B095647E0F42D26F18855F536600AB51687CAC1, ___Range_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_1() const { return ___Range_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_1() { return &___Range_1; }
	inline void set_Range_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_1 = value;
		Il2CppCodeGenWriteBarrier((&___Range_1), value);
	}

	inline static int32_t get_offset_of_Type_2() { return static_cast<int32_t>(offsetof(Comment_t3B095647E0F42D26F18855F536600AB51687CAC1, ___Type_2)); }
	inline String_t* get_Type_2() const { return ___Type_2; }
	inline String_t** get_address_of_Type_2() { return &___Type_2; }
	inline void set_Type_2(String_t* value)
	{
		___Type_2 = value;
		Il2CppCodeGenWriteBarrier((&___Type_2), value);
	}

	inline static int32_t get_offset_of_Value_3() { return static_cast<int32_t>(offsetof(Comment_t3B095647E0F42D26F18855F536600AB51687CAC1, ___Value_3)); }
	inline String_t* get_Value_3() const { return ___Value_3; }
	inline String_t** get_address_of_Value_3() { return &___Value_3; }
	inline void set_Value_3(String_t* value)
	{
		___Value_3 = value;
		Il2CppCodeGenWriteBarrier((&___Value_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMMENT_T3B095647E0F42D26F18855F536600AB51687CAC1_H
#ifndef FUNCTIONSCOPE_T18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434_H
#define FUNCTIONSCOPE_T18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.FunctionScope
struct  FunctionScope_t18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.FunctionDeclaration> Jint.Parser.FunctionScope::<FunctionDeclarations>k__BackingField
	RuntimeObject* ___U3CFunctionDeclarationsU3Ek__BackingField_0;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.VariableDeclaration> Jint.Parser.FunctionScope::<VariableDeclarations>k__BackingField
	RuntimeObject* ___U3CVariableDeclarationsU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(FunctionScope_t18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434, ___U3CFunctionDeclarationsU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CFunctionDeclarationsU3Ek__BackingField_0() const { return ___U3CFunctionDeclarationsU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CFunctionDeclarationsU3Ek__BackingField_0() { return &___U3CFunctionDeclarationsU3Ek__BackingField_0; }
	inline void set_U3CFunctionDeclarationsU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CFunctionDeclarationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionDeclarationsU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(FunctionScope_t18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434, ___U3CVariableDeclarationsU3Ek__BackingField_1)); }
	inline RuntimeObject* get_U3CVariableDeclarationsU3Ek__BackingField_1() const { return ___U3CVariableDeclarationsU3Ek__BackingField_1; }
	inline RuntimeObject** get_address_of_U3CVariableDeclarationsU3Ek__BackingField_1() { return &___U3CVariableDeclarationsU3Ek__BackingField_1; }
	inline void set_U3CVariableDeclarationsU3Ek__BackingField_1(RuntimeObject* value)
	{
		___U3CVariableDeclarationsU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableDeclarationsU3Ek__BackingField_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONSCOPE_T18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434_H
#ifndef LOCATIONMARKER_T27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8_H
#define LOCATIONMARKER_T27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.JavaScriptParser/LocationMarker
struct  LocationMarker_t27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8  : public RuntimeObject
{
public:
	// System.Int32[] Jint.Parser.JavaScriptParser/LocationMarker::_marker
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ____marker_0;

public:
	inline static int32_t get_offset_of__marker_0() { return static_cast<int32_t>(offsetof(LocationMarker_t27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8, ____marker_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get__marker_0() const { return ____marker_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of__marker_0() { return &____marker_0; }
	inline void set__marker_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		____marker_0 = value;
		Il2CppCodeGenWriteBarrier((&____marker_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATIONMARKER_T27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8_H
#ifndef REGEXES_T8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_H
#define REGEXES_T8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.JavaScriptParser/Regexes
struct  Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B  : public RuntimeObject
{
public:

public:
};

struct Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex Jint.Parser.JavaScriptParser/Regexes::NonAsciiIdentifierStart
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___NonAsciiIdentifierStart_0;
	// System.Text.RegularExpressions.Regex Jint.Parser.JavaScriptParser/Regexes::NonAsciiIdentifierPart
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___NonAsciiIdentifierPart_1;

public:
	inline static int32_t get_offset_of_NonAsciiIdentifierStart_0() { return static_cast<int32_t>(offsetof(Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_StaticFields, ___NonAsciiIdentifierStart_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_NonAsciiIdentifierStart_0() const { return ___NonAsciiIdentifierStart_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_NonAsciiIdentifierStart_0() { return &___NonAsciiIdentifierStart_0; }
	inline void set_NonAsciiIdentifierStart_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___NonAsciiIdentifierStart_0 = value;
		Il2CppCodeGenWriteBarrier((&___NonAsciiIdentifierStart_0), value);
	}

	inline static int32_t get_offset_of_NonAsciiIdentifierPart_1() { return static_cast<int32_t>(offsetof(Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_StaticFields, ___NonAsciiIdentifierPart_1)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_NonAsciiIdentifierPart_1() const { return ___NonAsciiIdentifierPart_1; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_NonAsciiIdentifierPart_1() { return &___NonAsciiIdentifierPart_1; }
	inline void set_NonAsciiIdentifierPart_1(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___NonAsciiIdentifierPart_1 = value;
		Il2CppCodeGenWriteBarrier((&___NonAsciiIdentifierPart_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REGEXES_T8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_H
#ifndef MESSAGES_TA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_H
#define MESSAGES_TA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Messages
struct  Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8  : public RuntimeObject
{
public:

public:
};

struct Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields
{
public:
	// System.String Jint.Parser.Messages::UnexpectedToken
	String_t* ___UnexpectedToken_0;
	// System.String Jint.Parser.Messages::UnexpectedNumber
	String_t* ___UnexpectedNumber_1;
	// System.String Jint.Parser.Messages::UnexpectedString
	String_t* ___UnexpectedString_2;
	// System.String Jint.Parser.Messages::UnexpectedIdentifier
	String_t* ___UnexpectedIdentifier_3;
	// System.String Jint.Parser.Messages::UnexpectedReserved
	String_t* ___UnexpectedReserved_4;
	// System.String Jint.Parser.Messages::UnexpectedEOS
	String_t* ___UnexpectedEOS_5;
	// System.String Jint.Parser.Messages::NewlineAfterThrow
	String_t* ___NewlineAfterThrow_6;
	// System.String Jint.Parser.Messages::InvalidRegExp
	String_t* ___InvalidRegExp_7;
	// System.String Jint.Parser.Messages::UnterminatedRegExp
	String_t* ___UnterminatedRegExp_8;
	// System.String Jint.Parser.Messages::InvalidLHSInAssignment
	String_t* ___InvalidLHSInAssignment_9;
	// System.String Jint.Parser.Messages::InvalidLHSInForIn
	String_t* ___InvalidLHSInForIn_10;
	// System.String Jint.Parser.Messages::MultipleDefaultsInSwitch
	String_t* ___MultipleDefaultsInSwitch_11;
	// System.String Jint.Parser.Messages::NoCatchOrFinally
	String_t* ___NoCatchOrFinally_12;
	// System.String Jint.Parser.Messages::UnknownLabel
	String_t* ___UnknownLabel_13;
	// System.String Jint.Parser.Messages::Redeclaration
	String_t* ___Redeclaration_14;
	// System.String Jint.Parser.Messages::IllegalContinue
	String_t* ___IllegalContinue_15;
	// System.String Jint.Parser.Messages::IllegalBreak
	String_t* ___IllegalBreak_16;
	// System.String Jint.Parser.Messages::IllegalReturn
	String_t* ___IllegalReturn_17;
	// System.String Jint.Parser.Messages::StrictModeWith
	String_t* ___StrictModeWith_18;
	// System.String Jint.Parser.Messages::StrictCatchVariable
	String_t* ___StrictCatchVariable_19;
	// System.String Jint.Parser.Messages::StrictVarName
	String_t* ___StrictVarName_20;
	// System.String Jint.Parser.Messages::StrictParamName
	String_t* ___StrictParamName_21;
	// System.String Jint.Parser.Messages::StrictParamDupe
	String_t* ___StrictParamDupe_22;
	// System.String Jint.Parser.Messages::StrictFunctionName
	String_t* ___StrictFunctionName_23;
	// System.String Jint.Parser.Messages::StrictOctalLiteral
	String_t* ___StrictOctalLiteral_24;
	// System.String Jint.Parser.Messages::StrictDelete
	String_t* ___StrictDelete_25;
	// System.String Jint.Parser.Messages::StrictDuplicateProperty
	String_t* ___StrictDuplicateProperty_26;
	// System.String Jint.Parser.Messages::AccessorDataProperty
	String_t* ___AccessorDataProperty_27;
	// System.String Jint.Parser.Messages::AccessorGetSet
	String_t* ___AccessorGetSet_28;
	// System.String Jint.Parser.Messages::StrictLHSAssignment
	String_t* ___StrictLHSAssignment_29;
	// System.String Jint.Parser.Messages::StrictLHSPostfix
	String_t* ___StrictLHSPostfix_30;
	// System.String Jint.Parser.Messages::StrictLHSPrefix
	String_t* ___StrictLHSPrefix_31;
	// System.String Jint.Parser.Messages::StrictReservedWord
	String_t* ___StrictReservedWord_32;

public:
	inline static int32_t get_offset_of_UnexpectedToken_0() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnexpectedToken_0)); }
	inline String_t* get_UnexpectedToken_0() const { return ___UnexpectedToken_0; }
	inline String_t** get_address_of_UnexpectedToken_0() { return &___UnexpectedToken_0; }
	inline void set_UnexpectedToken_0(String_t* value)
	{
		___UnexpectedToken_0 = value;
		Il2CppCodeGenWriteBarrier((&___UnexpectedToken_0), value);
	}

	inline static int32_t get_offset_of_UnexpectedNumber_1() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnexpectedNumber_1)); }
	inline String_t* get_UnexpectedNumber_1() const { return ___UnexpectedNumber_1; }
	inline String_t** get_address_of_UnexpectedNumber_1() { return &___UnexpectedNumber_1; }
	inline void set_UnexpectedNumber_1(String_t* value)
	{
		___UnexpectedNumber_1 = value;
		Il2CppCodeGenWriteBarrier((&___UnexpectedNumber_1), value);
	}

	inline static int32_t get_offset_of_UnexpectedString_2() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnexpectedString_2)); }
	inline String_t* get_UnexpectedString_2() const { return ___UnexpectedString_2; }
	inline String_t** get_address_of_UnexpectedString_2() { return &___UnexpectedString_2; }
	inline void set_UnexpectedString_2(String_t* value)
	{
		___UnexpectedString_2 = value;
		Il2CppCodeGenWriteBarrier((&___UnexpectedString_2), value);
	}

	inline static int32_t get_offset_of_UnexpectedIdentifier_3() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnexpectedIdentifier_3)); }
	inline String_t* get_UnexpectedIdentifier_3() const { return ___UnexpectedIdentifier_3; }
	inline String_t** get_address_of_UnexpectedIdentifier_3() { return &___UnexpectedIdentifier_3; }
	inline void set_UnexpectedIdentifier_3(String_t* value)
	{
		___UnexpectedIdentifier_3 = value;
		Il2CppCodeGenWriteBarrier((&___UnexpectedIdentifier_3), value);
	}

	inline static int32_t get_offset_of_UnexpectedReserved_4() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnexpectedReserved_4)); }
	inline String_t* get_UnexpectedReserved_4() const { return ___UnexpectedReserved_4; }
	inline String_t** get_address_of_UnexpectedReserved_4() { return &___UnexpectedReserved_4; }
	inline void set_UnexpectedReserved_4(String_t* value)
	{
		___UnexpectedReserved_4 = value;
		Il2CppCodeGenWriteBarrier((&___UnexpectedReserved_4), value);
	}

	inline static int32_t get_offset_of_UnexpectedEOS_5() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnexpectedEOS_5)); }
	inline String_t* get_UnexpectedEOS_5() const { return ___UnexpectedEOS_5; }
	inline String_t** get_address_of_UnexpectedEOS_5() { return &___UnexpectedEOS_5; }
	inline void set_UnexpectedEOS_5(String_t* value)
	{
		___UnexpectedEOS_5 = value;
		Il2CppCodeGenWriteBarrier((&___UnexpectedEOS_5), value);
	}

	inline static int32_t get_offset_of_NewlineAfterThrow_6() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___NewlineAfterThrow_6)); }
	inline String_t* get_NewlineAfterThrow_6() const { return ___NewlineAfterThrow_6; }
	inline String_t** get_address_of_NewlineAfterThrow_6() { return &___NewlineAfterThrow_6; }
	inline void set_NewlineAfterThrow_6(String_t* value)
	{
		___NewlineAfterThrow_6 = value;
		Il2CppCodeGenWriteBarrier((&___NewlineAfterThrow_6), value);
	}

	inline static int32_t get_offset_of_InvalidRegExp_7() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___InvalidRegExp_7)); }
	inline String_t* get_InvalidRegExp_7() const { return ___InvalidRegExp_7; }
	inline String_t** get_address_of_InvalidRegExp_7() { return &___InvalidRegExp_7; }
	inline void set_InvalidRegExp_7(String_t* value)
	{
		___InvalidRegExp_7 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidRegExp_7), value);
	}

	inline static int32_t get_offset_of_UnterminatedRegExp_8() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnterminatedRegExp_8)); }
	inline String_t* get_UnterminatedRegExp_8() const { return ___UnterminatedRegExp_8; }
	inline String_t** get_address_of_UnterminatedRegExp_8() { return &___UnterminatedRegExp_8; }
	inline void set_UnterminatedRegExp_8(String_t* value)
	{
		___UnterminatedRegExp_8 = value;
		Il2CppCodeGenWriteBarrier((&___UnterminatedRegExp_8), value);
	}

	inline static int32_t get_offset_of_InvalidLHSInAssignment_9() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___InvalidLHSInAssignment_9)); }
	inline String_t* get_InvalidLHSInAssignment_9() const { return ___InvalidLHSInAssignment_9; }
	inline String_t** get_address_of_InvalidLHSInAssignment_9() { return &___InvalidLHSInAssignment_9; }
	inline void set_InvalidLHSInAssignment_9(String_t* value)
	{
		___InvalidLHSInAssignment_9 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidLHSInAssignment_9), value);
	}

	inline static int32_t get_offset_of_InvalidLHSInForIn_10() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___InvalidLHSInForIn_10)); }
	inline String_t* get_InvalidLHSInForIn_10() const { return ___InvalidLHSInForIn_10; }
	inline String_t** get_address_of_InvalidLHSInForIn_10() { return &___InvalidLHSInForIn_10; }
	inline void set_InvalidLHSInForIn_10(String_t* value)
	{
		___InvalidLHSInForIn_10 = value;
		Il2CppCodeGenWriteBarrier((&___InvalidLHSInForIn_10), value);
	}

	inline static int32_t get_offset_of_MultipleDefaultsInSwitch_11() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___MultipleDefaultsInSwitch_11)); }
	inline String_t* get_MultipleDefaultsInSwitch_11() const { return ___MultipleDefaultsInSwitch_11; }
	inline String_t** get_address_of_MultipleDefaultsInSwitch_11() { return &___MultipleDefaultsInSwitch_11; }
	inline void set_MultipleDefaultsInSwitch_11(String_t* value)
	{
		___MultipleDefaultsInSwitch_11 = value;
		Il2CppCodeGenWriteBarrier((&___MultipleDefaultsInSwitch_11), value);
	}

	inline static int32_t get_offset_of_NoCatchOrFinally_12() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___NoCatchOrFinally_12)); }
	inline String_t* get_NoCatchOrFinally_12() const { return ___NoCatchOrFinally_12; }
	inline String_t** get_address_of_NoCatchOrFinally_12() { return &___NoCatchOrFinally_12; }
	inline void set_NoCatchOrFinally_12(String_t* value)
	{
		___NoCatchOrFinally_12 = value;
		Il2CppCodeGenWriteBarrier((&___NoCatchOrFinally_12), value);
	}

	inline static int32_t get_offset_of_UnknownLabel_13() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___UnknownLabel_13)); }
	inline String_t* get_UnknownLabel_13() const { return ___UnknownLabel_13; }
	inline String_t** get_address_of_UnknownLabel_13() { return &___UnknownLabel_13; }
	inline void set_UnknownLabel_13(String_t* value)
	{
		___UnknownLabel_13 = value;
		Il2CppCodeGenWriteBarrier((&___UnknownLabel_13), value);
	}

	inline static int32_t get_offset_of_Redeclaration_14() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___Redeclaration_14)); }
	inline String_t* get_Redeclaration_14() const { return ___Redeclaration_14; }
	inline String_t** get_address_of_Redeclaration_14() { return &___Redeclaration_14; }
	inline void set_Redeclaration_14(String_t* value)
	{
		___Redeclaration_14 = value;
		Il2CppCodeGenWriteBarrier((&___Redeclaration_14), value);
	}

	inline static int32_t get_offset_of_IllegalContinue_15() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___IllegalContinue_15)); }
	inline String_t* get_IllegalContinue_15() const { return ___IllegalContinue_15; }
	inline String_t** get_address_of_IllegalContinue_15() { return &___IllegalContinue_15; }
	inline void set_IllegalContinue_15(String_t* value)
	{
		___IllegalContinue_15 = value;
		Il2CppCodeGenWriteBarrier((&___IllegalContinue_15), value);
	}

	inline static int32_t get_offset_of_IllegalBreak_16() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___IllegalBreak_16)); }
	inline String_t* get_IllegalBreak_16() const { return ___IllegalBreak_16; }
	inline String_t** get_address_of_IllegalBreak_16() { return &___IllegalBreak_16; }
	inline void set_IllegalBreak_16(String_t* value)
	{
		___IllegalBreak_16 = value;
		Il2CppCodeGenWriteBarrier((&___IllegalBreak_16), value);
	}

	inline static int32_t get_offset_of_IllegalReturn_17() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___IllegalReturn_17)); }
	inline String_t* get_IllegalReturn_17() const { return ___IllegalReturn_17; }
	inline String_t** get_address_of_IllegalReturn_17() { return &___IllegalReturn_17; }
	inline void set_IllegalReturn_17(String_t* value)
	{
		___IllegalReturn_17 = value;
		Il2CppCodeGenWriteBarrier((&___IllegalReturn_17), value);
	}

	inline static int32_t get_offset_of_StrictModeWith_18() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictModeWith_18)); }
	inline String_t* get_StrictModeWith_18() const { return ___StrictModeWith_18; }
	inline String_t** get_address_of_StrictModeWith_18() { return &___StrictModeWith_18; }
	inline void set_StrictModeWith_18(String_t* value)
	{
		___StrictModeWith_18 = value;
		Il2CppCodeGenWriteBarrier((&___StrictModeWith_18), value);
	}

	inline static int32_t get_offset_of_StrictCatchVariable_19() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictCatchVariable_19)); }
	inline String_t* get_StrictCatchVariable_19() const { return ___StrictCatchVariable_19; }
	inline String_t** get_address_of_StrictCatchVariable_19() { return &___StrictCatchVariable_19; }
	inline void set_StrictCatchVariable_19(String_t* value)
	{
		___StrictCatchVariable_19 = value;
		Il2CppCodeGenWriteBarrier((&___StrictCatchVariable_19), value);
	}

	inline static int32_t get_offset_of_StrictVarName_20() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictVarName_20)); }
	inline String_t* get_StrictVarName_20() const { return ___StrictVarName_20; }
	inline String_t** get_address_of_StrictVarName_20() { return &___StrictVarName_20; }
	inline void set_StrictVarName_20(String_t* value)
	{
		___StrictVarName_20 = value;
		Il2CppCodeGenWriteBarrier((&___StrictVarName_20), value);
	}

	inline static int32_t get_offset_of_StrictParamName_21() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictParamName_21)); }
	inline String_t* get_StrictParamName_21() const { return ___StrictParamName_21; }
	inline String_t** get_address_of_StrictParamName_21() { return &___StrictParamName_21; }
	inline void set_StrictParamName_21(String_t* value)
	{
		___StrictParamName_21 = value;
		Il2CppCodeGenWriteBarrier((&___StrictParamName_21), value);
	}

	inline static int32_t get_offset_of_StrictParamDupe_22() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictParamDupe_22)); }
	inline String_t* get_StrictParamDupe_22() const { return ___StrictParamDupe_22; }
	inline String_t** get_address_of_StrictParamDupe_22() { return &___StrictParamDupe_22; }
	inline void set_StrictParamDupe_22(String_t* value)
	{
		___StrictParamDupe_22 = value;
		Il2CppCodeGenWriteBarrier((&___StrictParamDupe_22), value);
	}

	inline static int32_t get_offset_of_StrictFunctionName_23() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictFunctionName_23)); }
	inline String_t* get_StrictFunctionName_23() const { return ___StrictFunctionName_23; }
	inline String_t** get_address_of_StrictFunctionName_23() { return &___StrictFunctionName_23; }
	inline void set_StrictFunctionName_23(String_t* value)
	{
		___StrictFunctionName_23 = value;
		Il2CppCodeGenWriteBarrier((&___StrictFunctionName_23), value);
	}

	inline static int32_t get_offset_of_StrictOctalLiteral_24() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictOctalLiteral_24)); }
	inline String_t* get_StrictOctalLiteral_24() const { return ___StrictOctalLiteral_24; }
	inline String_t** get_address_of_StrictOctalLiteral_24() { return &___StrictOctalLiteral_24; }
	inline void set_StrictOctalLiteral_24(String_t* value)
	{
		___StrictOctalLiteral_24 = value;
		Il2CppCodeGenWriteBarrier((&___StrictOctalLiteral_24), value);
	}

	inline static int32_t get_offset_of_StrictDelete_25() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictDelete_25)); }
	inline String_t* get_StrictDelete_25() const { return ___StrictDelete_25; }
	inline String_t** get_address_of_StrictDelete_25() { return &___StrictDelete_25; }
	inline void set_StrictDelete_25(String_t* value)
	{
		___StrictDelete_25 = value;
		Il2CppCodeGenWriteBarrier((&___StrictDelete_25), value);
	}

	inline static int32_t get_offset_of_StrictDuplicateProperty_26() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictDuplicateProperty_26)); }
	inline String_t* get_StrictDuplicateProperty_26() const { return ___StrictDuplicateProperty_26; }
	inline String_t** get_address_of_StrictDuplicateProperty_26() { return &___StrictDuplicateProperty_26; }
	inline void set_StrictDuplicateProperty_26(String_t* value)
	{
		___StrictDuplicateProperty_26 = value;
		Il2CppCodeGenWriteBarrier((&___StrictDuplicateProperty_26), value);
	}

	inline static int32_t get_offset_of_AccessorDataProperty_27() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___AccessorDataProperty_27)); }
	inline String_t* get_AccessorDataProperty_27() const { return ___AccessorDataProperty_27; }
	inline String_t** get_address_of_AccessorDataProperty_27() { return &___AccessorDataProperty_27; }
	inline void set_AccessorDataProperty_27(String_t* value)
	{
		___AccessorDataProperty_27 = value;
		Il2CppCodeGenWriteBarrier((&___AccessorDataProperty_27), value);
	}

	inline static int32_t get_offset_of_AccessorGetSet_28() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___AccessorGetSet_28)); }
	inline String_t* get_AccessorGetSet_28() const { return ___AccessorGetSet_28; }
	inline String_t** get_address_of_AccessorGetSet_28() { return &___AccessorGetSet_28; }
	inline void set_AccessorGetSet_28(String_t* value)
	{
		___AccessorGetSet_28 = value;
		Il2CppCodeGenWriteBarrier((&___AccessorGetSet_28), value);
	}

	inline static int32_t get_offset_of_StrictLHSAssignment_29() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictLHSAssignment_29)); }
	inline String_t* get_StrictLHSAssignment_29() const { return ___StrictLHSAssignment_29; }
	inline String_t** get_address_of_StrictLHSAssignment_29() { return &___StrictLHSAssignment_29; }
	inline void set_StrictLHSAssignment_29(String_t* value)
	{
		___StrictLHSAssignment_29 = value;
		Il2CppCodeGenWriteBarrier((&___StrictLHSAssignment_29), value);
	}

	inline static int32_t get_offset_of_StrictLHSPostfix_30() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictLHSPostfix_30)); }
	inline String_t* get_StrictLHSPostfix_30() const { return ___StrictLHSPostfix_30; }
	inline String_t** get_address_of_StrictLHSPostfix_30() { return &___StrictLHSPostfix_30; }
	inline void set_StrictLHSPostfix_30(String_t* value)
	{
		___StrictLHSPostfix_30 = value;
		Il2CppCodeGenWriteBarrier((&___StrictLHSPostfix_30), value);
	}

	inline static int32_t get_offset_of_StrictLHSPrefix_31() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictLHSPrefix_31)); }
	inline String_t* get_StrictLHSPrefix_31() const { return ___StrictLHSPrefix_31; }
	inline String_t** get_address_of_StrictLHSPrefix_31() { return &___StrictLHSPrefix_31; }
	inline void set_StrictLHSPrefix_31(String_t* value)
	{
		___StrictLHSPrefix_31 = value;
		Il2CppCodeGenWriteBarrier((&___StrictLHSPrefix_31), value);
	}

	inline static int32_t get_offset_of_StrictReservedWord_32() { return static_cast<int32_t>(offsetof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields, ___StrictReservedWord_32)); }
	inline String_t* get_StrictReservedWord_32() const { return ___StrictReservedWord_32; }
	inline String_t** get_address_of_StrictReservedWord_32() { return &___StrictReservedWord_32; }
	inline void set_StrictReservedWord_32(String_t* value)
	{
		___StrictReservedWord_32 = value;
		Il2CppCodeGenWriteBarrier((&___StrictReservedWord_32), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MESSAGES_TA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_H
#ifndef PARSEREXTENSIONS_T25486BCFA5ACD7E155AB15BE956C13D919E9563A_H
#define PARSEREXTENSIONS_T25486BCFA5ACD7E155AB15BE956C13D919E9563A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.ParserExtensions
struct  ParserExtensions_t25486BCFA5ACD7E155AB15BE956C13D919E9563A  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEREXTENSIONS_T25486BCFA5ACD7E155AB15BE956C13D919E9563A_H
#ifndef PARSEROPTIONS_T944C18524B8847EFDD587A1C78B0CF4750F0B672_H
#define PARSEROPTIONS_T944C18524B8847EFDD587A1C78B0CF4750F0B672_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.ParserOptions
struct  ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672  : public RuntimeObject
{
public:
	// System.String Jint.Parser.ParserOptions::<Source>k__BackingField
	String_t* ___U3CSourceU3Ek__BackingField_0;
	// System.Boolean Jint.Parser.ParserOptions::<Tokens>k__BackingField
	bool ___U3CTokensU3Ek__BackingField_1;
	// System.Boolean Jint.Parser.ParserOptions::<Comment>k__BackingField
	bool ___U3CCommentU3Ek__BackingField_2;
	// System.Boolean Jint.Parser.ParserOptions::<Tolerant>k__BackingField
	bool ___U3CTolerantU3Ek__BackingField_3;

public:
	inline static int32_t get_offset_of_U3CSourceU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672, ___U3CSourceU3Ek__BackingField_0)); }
	inline String_t* get_U3CSourceU3Ek__BackingField_0() const { return ___U3CSourceU3Ek__BackingField_0; }
	inline String_t** get_address_of_U3CSourceU3Ek__BackingField_0() { return &___U3CSourceU3Ek__BackingField_0; }
	inline void set_U3CSourceU3Ek__BackingField_0(String_t* value)
	{
		___U3CSourceU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSourceU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CTokensU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672, ___U3CTokensU3Ek__BackingField_1)); }
	inline bool get_U3CTokensU3Ek__BackingField_1() const { return ___U3CTokensU3Ek__BackingField_1; }
	inline bool* get_address_of_U3CTokensU3Ek__BackingField_1() { return &___U3CTokensU3Ek__BackingField_1; }
	inline void set_U3CTokensU3Ek__BackingField_1(bool value)
	{
		___U3CTokensU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CCommentU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672, ___U3CCommentU3Ek__BackingField_2)); }
	inline bool get_U3CCommentU3Ek__BackingField_2() const { return ___U3CCommentU3Ek__BackingField_2; }
	inline bool* get_address_of_U3CCommentU3Ek__BackingField_2() { return &___U3CCommentU3Ek__BackingField_2; }
	inline void set_U3CCommentU3Ek__BackingField_2(bool value)
	{
		___U3CCommentU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CTolerantU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672, ___U3CTolerantU3Ek__BackingField_3)); }
	inline bool get_U3CTolerantU3Ek__BackingField_3() const { return ___U3CTolerantU3Ek__BackingField_3; }
	inline bool* get_address_of_U3CTolerantU3Ek__BackingField_3() { return &___U3CTolerantU3Ek__BackingField_3; }
	inline void set_U3CTolerantU3Ek__BackingField_3(bool value)
	{
		___U3CTolerantU3Ek__BackingField_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEROPTIONS_T944C18524B8847EFDD587A1C78B0CF4750F0B672_H
#ifndef VARIABLESCOPE_TA73D304160DEDDF9AE542B6C15D74A651544C585_H
#define VARIABLESCOPE_TA73D304160DEDDF9AE542B6C15D74A651544C585_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.VariableScope
struct  VariableScope_tA73D304160DEDDF9AE542B6C15D74A651544C585  : public RuntimeObject
{
public:
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.VariableDeclaration> Jint.Parser.VariableScope::<VariableDeclarations>k__BackingField
	RuntimeObject* ___U3CVariableDeclarationsU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(VariableScope_tA73D304160DEDDF9AE542B6C15D74A651544C585, ___U3CVariableDeclarationsU3Ek__BackingField_0)); }
	inline RuntimeObject* get_U3CVariableDeclarationsU3Ek__BackingField_0() const { return ___U3CVariableDeclarationsU3Ek__BackingField_0; }
	inline RuntimeObject** get_address_of_U3CVariableDeclarationsU3Ek__BackingField_0() { return &___U3CVariableDeclarationsU3Ek__BackingField_0; }
	inline void set_U3CVariableDeclarationsU3Ek__BackingField_0(RuntimeObject* value)
	{
		___U3CVariableDeclarationsU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableDeclarationsU3Ek__BackingField_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLESCOPE_TA73D304160DEDDF9AE542B6C15D74A651544C585_H
#ifndef CALLSTACKELEMENTCOMPARER_TC9CD7CCFD0DDF3EB0258D8A40536912ED43B1B6C_H
#define CALLSTACKELEMENTCOMPARER_TC9CD7CCFD0DDF3EB0258D8A40536912ED43B1B6C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.CallStack.CallStackElementComparer
struct  CallStackElementComparer_tC9CD7CCFD0DDF3EB0258D8A40536912ED43B1B6C  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLSTACKELEMENTCOMPARER_TC9CD7CCFD0DDF3EB0258D8A40536912ED43B1B6C_H
#ifndef JINTCALLSTACK_TC35F241423CC5B5C3185463D6476DF0223D133E6_H
#define JINTCALLSTACK_TC35F241423CC5B5C3185463D6476DF0223D133E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.CallStack.JintCallStack
struct  JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<Jint.Runtime.CallStackElement> Jint.Runtime.CallStack.JintCallStack::_stack
	Stack_1_tD2EAC4775133AB949BFC40D737A35CBC6CDED11B * ____stack_0;
	// System.Collections.Generic.Dictionary`2<Jint.Runtime.CallStackElement,System.Int32> Jint.Runtime.CallStack.JintCallStack::_statistics
	Dictionary_2_t10E2334BD922D6E4791209EC9F1821C67F397912 * ____statistics_1;

public:
	inline static int32_t get_offset_of__stack_0() { return static_cast<int32_t>(offsetof(JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6, ____stack_0)); }
	inline Stack_1_tD2EAC4775133AB949BFC40D737A35CBC6CDED11B * get__stack_0() const { return ____stack_0; }
	inline Stack_1_tD2EAC4775133AB949BFC40D737A35CBC6CDED11B ** get_address_of__stack_0() { return &____stack_0; }
	inline void set__stack_0(Stack_1_tD2EAC4775133AB949BFC40D737A35CBC6CDED11B * value)
	{
		____stack_0 = value;
		Il2CppCodeGenWriteBarrier((&____stack_0), value);
	}

	inline static int32_t get_offset_of__statistics_1() { return static_cast<int32_t>(offsetof(JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6, ____statistics_1)); }
	inline Dictionary_2_t10E2334BD922D6E4791209EC9F1821C67F397912 * get__statistics_1() const { return ____statistics_1; }
	inline Dictionary_2_t10E2334BD922D6E4791209EC9F1821C67F397912 ** get_address_of__statistics_1() { return &____statistics_1; }
	inline void set__statistics_1(Dictionary_2_t10E2334BD922D6E4791209EC9F1821C67F397912 * value)
	{
		____statistics_1 = value;
		Il2CppCodeGenWriteBarrier((&____statistics_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JINTCALLSTACK_TC35F241423CC5B5C3185463D6476DF0223D133E6_H
#ifndef U3CU3EC_TF62F3D23D314BE616B33845B3BC13B175B525056_H
#define U3CU3EC_TF62F3D23D314BE616B33845B3BC13B175B525056_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.CallStack.JintCallStack/<>c
struct  U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056_StaticFields
{
public:
	// Jint.Runtime.CallStack.JintCallStack/<>c Jint.Runtime.CallStack.JintCallStack/<>c::<>9
	U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056 * ___U3CU3E9_0;
	// System.Func`2<Jint.Runtime.CallStackElement,System.String> Jint.Runtime.CallStack.JintCallStack/<>c::<>9__6_0
	Func_2_tBD3331CD03481BE2DB45C07A63C2185894EAEE6E * ___U3CU3E9__6_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056_StaticFields, ___U3CU3E9__6_0_1)); }
	inline Func_2_tBD3331CD03481BE2DB45C07A63C2185894EAEE6E * get_U3CU3E9__6_0_1() const { return ___U3CU3E9__6_0_1; }
	inline Func_2_tBD3331CD03481BE2DB45C07A63C2185894EAEE6E ** get_address_of_U3CU3E9__6_0_1() { return &___U3CU3E9__6_0_1; }
	inline void set_U3CU3E9__6_0_1(Func_2_tBD3331CD03481BE2DB45C07A63C2185894EAEE6E * value)
	{
		___U3CU3E9__6_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TF62F3D23D314BE616B33845B3BC13B175B525056_H
#ifndef BREAKPOINT_T50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688_H
#define BREAKPOINT_T50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Debugger.BreakPoint
struct  BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688  : public RuntimeObject
{
public:
	// System.Int32 Jint.Runtime.Debugger.BreakPoint::<Line>k__BackingField
	int32_t ___U3CLineU3Ek__BackingField_0;
	// System.Int32 Jint.Runtime.Debugger.BreakPoint::<Char>k__BackingField
	int32_t ___U3CCharU3Ek__BackingField_1;
	// System.String Jint.Runtime.Debugger.BreakPoint::<Condition>k__BackingField
	String_t* ___U3CConditionU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLineU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688, ___U3CLineU3Ek__BackingField_0)); }
	inline int32_t get_U3CLineU3Ek__BackingField_0() const { return ___U3CLineU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CLineU3Ek__BackingField_0() { return &___U3CLineU3Ek__BackingField_0; }
	inline void set_U3CLineU3Ek__BackingField_0(int32_t value)
	{
		___U3CLineU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CCharU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688, ___U3CCharU3Ek__BackingField_1)); }
	inline int32_t get_U3CCharU3Ek__BackingField_1() const { return ___U3CCharU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CCharU3Ek__BackingField_1() { return &___U3CCharU3Ek__BackingField_1; }
	inline void set_U3CCharU3Ek__BackingField_1(int32_t value)
	{
		___U3CCharU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CConditionU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688, ___U3CConditionU3Ek__BackingField_2)); }
	inline String_t* get_U3CConditionU3Ek__BackingField_2() const { return ___U3CConditionU3Ek__BackingField_2; }
	inline String_t** get_address_of_U3CConditionU3Ek__BackingField_2() { return &___U3CConditionU3Ek__BackingField_2; }
	inline void set_U3CConditionU3Ek__BackingField_2(String_t* value)
	{
		___U3CConditionU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CConditionU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BREAKPOINT_T50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688_H
#ifndef U3CU3EC__DISPLAYCLASS7_0_T024887E8EA9F24EFD58C9231F317C5F16E662029_H
#define U3CU3EC__DISPLAYCLASS7_0_T024887E8EA9F24EFD58C9231F317C5F16E662029_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Debugger.DebugHandler/<>c__DisplayClass7_0
struct  U3CU3Ec__DisplayClass7_0_t024887E8EA9F24EFD58C9231F317C5F16E662029  : public RuntimeObject
{
public:
	// Jint.Parser.Ast.Statement Jint.Runtime.Debugger.DebugHandler/<>c__DisplayClass7_0::statement
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___statement_0;
	// Jint.Runtime.Debugger.DebugHandler Jint.Runtime.Debugger.DebugHandler/<>c__DisplayClass7_0::<>4__this
	DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_statement_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t024887E8EA9F24EFD58C9231F317C5F16E662029, ___statement_0)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_statement_0() const { return ___statement_0; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_statement_0() { return &___statement_0; }
	inline void set_statement_0(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___statement_0 = value;
		Il2CppCodeGenWriteBarrier((&___statement_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass7_0_t024887E8EA9F24EFD58C9231F317C5F16E662029, ___U3CU3E4__this_1)); }
	inline DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS7_0_T024887E8EA9F24EFD58C9231F317C5F16E662029_H
#ifndef BINDING_T56830C02382DE83D9086BFDA0EDD0520D9E1781D_H
#define BINDING_T56830C02382DE83D9086BFDA0EDD0520D9E1781D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.Binding
struct  Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D  : public RuntimeObject
{
public:
	// Jint.Native.JsValue Jint.Runtime.Environments.Binding::Value
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___Value_0;
	// System.Boolean Jint.Runtime.Environments.Binding::CanBeDeleted
	bool ___CanBeDeleted_1;
	// System.Boolean Jint.Runtime.Environments.Binding::Mutable
	bool ___Mutable_2;

public:
	inline static int32_t get_offset_of_Value_0() { return static_cast<int32_t>(offsetof(Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D, ___Value_0)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_Value_0() const { return ___Value_0; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_Value_0() { return &___Value_0; }
	inline void set_Value_0(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___Value_0 = value;
		Il2CppCodeGenWriteBarrier((&___Value_0), value);
	}

	inline static int32_t get_offset_of_CanBeDeleted_1() { return static_cast<int32_t>(offsetof(Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D, ___CanBeDeleted_1)); }
	inline bool get_CanBeDeleted_1() const { return ___CanBeDeleted_1; }
	inline bool* get_address_of_CanBeDeleted_1() { return &___CanBeDeleted_1; }
	inline void set_CanBeDeleted_1(bool value)
	{
		___CanBeDeleted_1 = value;
	}

	inline static int32_t get_offset_of_Mutable_2() { return static_cast<int32_t>(offsetof(Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D, ___Mutable_2)); }
	inline bool get_Mutable_2() const { return ___Mutable_2; }
	inline bool* get_address_of_Mutable_2() { return &___Mutable_2; }
	inline void set_Mutable_2(bool value)
	{
		___Mutable_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINDING_T56830C02382DE83D9086BFDA0EDD0520D9E1781D_H
#ifndef EXECUTIONCONTEXT_TCBE07A5E359EDD941E06F143251E5C20DCF910FE_H
#define EXECUTIONCONTEXT_TCBE07A5E359EDD941E06F143251E5C20DCF910FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.ExecutionContext
struct  ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE  : public RuntimeObject
{
public:
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Runtime.Environments.ExecutionContext::<LexicalEnvironment>k__BackingField
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ___U3CLexicalEnvironmentU3Ek__BackingField_0;
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Runtime.Environments.ExecutionContext::<VariableEnvironment>k__BackingField
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ___U3CVariableEnvironmentU3Ek__BackingField_1;
	// Jint.Native.JsValue Jint.Runtime.Environments.ExecutionContext::<ThisBinding>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CThisBindingU3Ek__BackingField_2;

public:
	inline static int32_t get_offset_of_U3CLexicalEnvironmentU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE, ___U3CLexicalEnvironmentU3Ek__BackingField_0)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get_U3CLexicalEnvironmentU3Ek__BackingField_0() const { return ___U3CLexicalEnvironmentU3Ek__BackingField_0; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of_U3CLexicalEnvironmentU3Ek__BackingField_0() { return &___U3CLexicalEnvironmentU3Ek__BackingField_0; }
	inline void set_U3CLexicalEnvironmentU3Ek__BackingField_0(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		___U3CLexicalEnvironmentU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLexicalEnvironmentU3Ek__BackingField_0), value);
	}

	inline static int32_t get_offset_of_U3CVariableEnvironmentU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE, ___U3CVariableEnvironmentU3Ek__BackingField_1)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get_U3CVariableEnvironmentU3Ek__BackingField_1() const { return ___U3CVariableEnvironmentU3Ek__BackingField_1; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of_U3CVariableEnvironmentU3Ek__BackingField_1() { return &___U3CVariableEnvironmentU3Ek__BackingField_1; }
	inline void set_U3CVariableEnvironmentU3Ek__BackingField_1(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		___U3CVariableEnvironmentU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableEnvironmentU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CThisBindingU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE, ___U3CThisBindingU3Ek__BackingField_2)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CThisBindingU3Ek__BackingField_2() const { return ___U3CThisBindingU3Ek__BackingField_2; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CThisBindingU3Ek__BackingField_2() { return &___U3CThisBindingU3Ek__BackingField_2; }
	inline void set_U3CThisBindingU3Ek__BackingField_2(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CThisBindingU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CThisBindingU3Ek__BackingField_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXECUTIONCONTEXT_TCBE07A5E359EDD941E06F143251E5C20DCF910FE_H
#ifndef LEXICALENVIRONMENT_T9181F8CD8E041687DE252EA6C808F36174248026_H
#define LEXICALENVIRONMENT_T9181F8CD8E041687DE252EA6C808F36174248026_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.LexicalEnvironment
struct  LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026  : public RuntimeObject
{
public:
	// Jint.Runtime.Environments.EnvironmentRecord Jint.Runtime.Environments.LexicalEnvironment::_record
	EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B * ____record_0;
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Runtime.Environments.LexicalEnvironment::_outer
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ____outer_1;

public:
	inline static int32_t get_offset_of__record_0() { return static_cast<int32_t>(offsetof(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026, ____record_0)); }
	inline EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B * get__record_0() const { return ____record_0; }
	inline EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B ** get_address_of__record_0() { return &____record_0; }
	inline void set__record_0(EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B * value)
	{
		____record_0 = value;
		Il2CppCodeGenWriteBarrier((&____record_0), value);
	}

	inline static int32_t get_offset_of__outer_1() { return static_cast<int32_t>(offsetof(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026, ____outer_1)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get__outer_1() const { return ____outer_1; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of__outer_1() { return &____outer_1; }
	inline void set__outer_1(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		____outer_1 = value;
		Il2CppCodeGenWriteBarrier((&____outer_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LEXICALENVIRONMENT_T9181F8CD8E041687DE252EA6C808F36174248026_H
#ifndef U3CU3EC_TE510622EA06869BD70430A0AF1278748E2300202_H
#define U3CU3EC_TE510622EA06869BD70430A0AF1278748E2300202_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.ObjectEnvironmentRecord/<>c
struct  U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202_StaticFields
{
public:
	// Jint.Runtime.Environments.ObjectEnvironmentRecord/<>c Jint.Runtime.Environments.ObjectEnvironmentRecord/<>c::<>9
	U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202 * ___U3CU3E9_0;
	// System.Func`2<System.Collections.Generic.KeyValuePair`2<System.String,Jint.Runtime.Descriptors.PropertyDescriptor>,System.String> Jint.Runtime.Environments.ObjectEnvironmentRecord/<>c::<>9__10_0
	Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * ___U3CU3E9__10_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__10_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202_StaticFields, ___U3CU3E9__10_0_1)); }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * get_U3CU3E9__10_0_1() const { return ___U3CU3E9__10_0_1; }
	inline Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 ** get_address_of_U3CU3E9__10_0_1() { return &___U3CU3E9__10_0_1; }
	inline void set_U3CU3E9__10_0_1(Func_2_t614EC1F5A8DA6EF71EEEBA1AEEC039105A21B759 * value)
	{
		___U3CU3E9__10_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__10_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TE510622EA06869BD70430A0AF1278748E2300202_H
#ifndef U3CU3EC_TCFA2BF4DE783609BD160CDA34A1848874AB5E799_H
#define U3CU3EC_TCFA2BF4DE783609BD160CDA34A1848874AB5E799_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.MethodInfoFunctionInstance/<>c
struct  U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799_StaticFields
{
public:
	// Jint.Runtime.Interop.MethodInfoFunctionInstance/<>c Jint.Runtime.Interop.MethodInfoFunctionInstance/<>c::<>9
	U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.ParameterInfo,System.Boolean> Jint.Runtime.Interop.MethodInfoFunctionInstance/<>c::<>9__4_0
	Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A * ___U3CU3E9__4_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__4_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799_StaticFields, ___U3CU3E9__4_0_1)); }
	inline Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A * get_U3CU3E9__4_0_1() const { return ___U3CU3E9__4_0_1; }
	inline Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A ** get_address_of_U3CU3E9__4_0_1() { return &___U3CU3E9__4_0_1; }
	inline void set_U3CU3E9__4_0_1(Func_2_t61E54F48AD8F1DC354B46A3826709996DB41375A * value)
	{
		___U3CU3E9__4_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__4_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_TCFA2BF4DE783609BD160CDA34A1848874AB5E799_H
#ifndef U3CU3EC_T78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_H
#define U3CU3EC_T78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.ObjectWrapper/<>c
struct  U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields
{
public:
	// Jint.Runtime.Interop.ObjectWrapper/<>c Jint.Runtime.Interop.ObjectWrapper/<>c::<>9
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231 * ___U3CU3E9_0;
	// System.Func`2<System.Reflection.PropertyInfo,System.Boolean> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_3
	Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * ___U3CU3E9__6_3_1;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_4
	Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 * ___U3CU3E9__6_4_2;
	// System.Func`3<System.Type,System.Reflection.PropertyInfo,<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_5
	Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C * ___U3CU3E9__6_5_3;
	// System.Func`2<<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>,System.Reflection.PropertyInfo> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_7
	Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC * ___U3CU3E9__6_7_4;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.MethodInfo>> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_8
	Func_2_tE1A6E27E679D795C2ED310502F7ABD12212F4568 * ___U3CU3E9__6_8_5;
	// System.Func`3<System.Type,System.Reflection.MethodInfo,<>f__AnonymousType1`2<System.Type,System.Reflection.MethodInfo>> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_9
	Func_3_t4E3965D24D0CCD653477F41A012FD12131BFEF7F * ___U3CU3E9__6_9_6;
	// System.Func`2<<>f__AnonymousType1`2<System.Type,System.Reflection.MethodInfo>,System.Reflection.MethodInfo> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_11
	Func_2_tBCEAEF263E293ADF6DAB97525CFB89FD930716B1 * ___U3CU3E9__6_11_7;
	// System.Func`2<System.Type,System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_12
	Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 * ___U3CU3E9__6_12_8;
	// System.Func`3<System.Type,System.Reflection.PropertyInfo,<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_13
	Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C * ___U3CU3E9__6_13_9;
	// System.Func`2<<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>,System.Boolean> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_14
	Func_2_tC8337B4C10DC835F5F9A6BE8E4C9BB00DE98DE2A * ___U3CU3E9__6_14_10;
	// System.Func`2<<>f__AnonymousType0`2<System.Type,System.Reflection.PropertyInfo>,System.Reflection.PropertyInfo> Jint.Runtime.Interop.ObjectWrapper/<>c::<>9__6_15
	Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC * ___U3CU3E9__6_15_11;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_3_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_3_1)); }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * get_U3CU3E9__6_3_1() const { return ___U3CU3E9__6_3_1; }
	inline Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C ** get_address_of_U3CU3E9__6_3_1() { return &___U3CU3E9__6_3_1; }
	inline void set_U3CU3E9__6_3_1(Func_2_tF059A6F3E6CDF153D0E7F4B9A9B8AB75DBA4BB7C * value)
	{
		___U3CU3E9__6_3_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_3_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_4_2() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_4_2)); }
	inline Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 * get_U3CU3E9__6_4_2() const { return ___U3CU3E9__6_4_2; }
	inline Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 ** get_address_of_U3CU3E9__6_4_2() { return &___U3CU3E9__6_4_2; }
	inline void set_U3CU3E9__6_4_2(Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 * value)
	{
		___U3CU3E9__6_4_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_4_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_5_3() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_5_3)); }
	inline Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C * get_U3CU3E9__6_5_3() const { return ___U3CU3E9__6_5_3; }
	inline Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C ** get_address_of_U3CU3E9__6_5_3() { return &___U3CU3E9__6_5_3; }
	inline void set_U3CU3E9__6_5_3(Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C * value)
	{
		___U3CU3E9__6_5_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_5_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_7_4() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_7_4)); }
	inline Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC * get_U3CU3E9__6_7_4() const { return ___U3CU3E9__6_7_4; }
	inline Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC ** get_address_of_U3CU3E9__6_7_4() { return &___U3CU3E9__6_7_4; }
	inline void set_U3CU3E9__6_7_4(Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC * value)
	{
		___U3CU3E9__6_7_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_7_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_8_5() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_8_5)); }
	inline Func_2_tE1A6E27E679D795C2ED310502F7ABD12212F4568 * get_U3CU3E9__6_8_5() const { return ___U3CU3E9__6_8_5; }
	inline Func_2_tE1A6E27E679D795C2ED310502F7ABD12212F4568 ** get_address_of_U3CU3E9__6_8_5() { return &___U3CU3E9__6_8_5; }
	inline void set_U3CU3E9__6_8_5(Func_2_tE1A6E27E679D795C2ED310502F7ABD12212F4568 * value)
	{
		___U3CU3E9__6_8_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_8_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_9_6() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_9_6)); }
	inline Func_3_t4E3965D24D0CCD653477F41A012FD12131BFEF7F * get_U3CU3E9__6_9_6() const { return ___U3CU3E9__6_9_6; }
	inline Func_3_t4E3965D24D0CCD653477F41A012FD12131BFEF7F ** get_address_of_U3CU3E9__6_9_6() { return &___U3CU3E9__6_9_6; }
	inline void set_U3CU3E9__6_9_6(Func_3_t4E3965D24D0CCD653477F41A012FD12131BFEF7F * value)
	{
		___U3CU3E9__6_9_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_9_6), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_11_7() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_11_7)); }
	inline Func_2_tBCEAEF263E293ADF6DAB97525CFB89FD930716B1 * get_U3CU3E9__6_11_7() const { return ___U3CU3E9__6_11_7; }
	inline Func_2_tBCEAEF263E293ADF6DAB97525CFB89FD930716B1 ** get_address_of_U3CU3E9__6_11_7() { return &___U3CU3E9__6_11_7; }
	inline void set_U3CU3E9__6_11_7(Func_2_tBCEAEF263E293ADF6DAB97525CFB89FD930716B1 * value)
	{
		___U3CU3E9__6_11_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_11_7), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_12_8() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_12_8)); }
	inline Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 * get_U3CU3E9__6_12_8() const { return ___U3CU3E9__6_12_8; }
	inline Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 ** get_address_of_U3CU3E9__6_12_8() { return &___U3CU3E9__6_12_8; }
	inline void set_U3CU3E9__6_12_8(Func_2_t0D176D8B1628990556B4B45FED3E19C347647B43 * value)
	{
		___U3CU3E9__6_12_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_12_8), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_13_9() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_13_9)); }
	inline Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C * get_U3CU3E9__6_13_9() const { return ___U3CU3E9__6_13_9; }
	inline Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C ** get_address_of_U3CU3E9__6_13_9() { return &___U3CU3E9__6_13_9; }
	inline void set_U3CU3E9__6_13_9(Func_3_t4B6F1AE30C338E58D82FC184519021FB32B6D30C * value)
	{
		___U3CU3E9__6_13_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_13_9), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_14_10() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_14_10)); }
	inline Func_2_tC8337B4C10DC835F5F9A6BE8E4C9BB00DE98DE2A * get_U3CU3E9__6_14_10() const { return ___U3CU3E9__6_14_10; }
	inline Func_2_tC8337B4C10DC835F5F9A6BE8E4C9BB00DE98DE2A ** get_address_of_U3CU3E9__6_14_10() { return &___U3CU3E9__6_14_10; }
	inline void set_U3CU3E9__6_14_10(Func_2_tC8337B4C10DC835F5F9A6BE8E4C9BB00DE98DE2A * value)
	{
		___U3CU3E9__6_14_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_14_10), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__6_15_11() { return static_cast<int32_t>(offsetof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields, ___U3CU3E9__6_15_11)); }
	inline Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC * get_U3CU3E9__6_15_11() const { return ___U3CU3E9__6_15_11; }
	inline Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC ** get_address_of_U3CU3E9__6_15_11() { return &___U3CU3E9__6_15_11; }
	inline void set_U3CU3E9__6_15_11(Func_2_t7A16DEE1C8B3A91C4F2A7242B3B75E2F41EA5ECC * value)
	{
		___U3CU3E9__6_15_11 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__6_15_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_H
#ifndef U3CU3EC__DISPLAYCLASS6_0_T4B40E563CC434DA33BC857EFE7A03C911BB984D5_H
#define U3CU3EC__DISPLAYCLASS6_0_T4B40E563CC434DA33BC857EFE7A03C911BB984D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.ObjectWrapper/<>c__DisplayClass6_0
struct  U3CU3Ec__DisplayClass6_0_t4B40E563CC434DA33BC857EFE7A03C911BB984D5  : public RuntimeObject
{
public:
	// System.String Jint.Runtime.Interop.ObjectWrapper/<>c__DisplayClass6_0::propertyName
	String_t* ___propertyName_0;
	// Jint.Runtime.Interop.ObjectWrapper Jint.Runtime.Interop.ObjectWrapper/<>c__DisplayClass6_0::<>4__this
	ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5 * ___U3CU3E4__this_1;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t4B40E563CC434DA33BC857EFE7A03C911BB984D5, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass6_0_t4B40E563CC434DA33BC857EFE7A03C911BB984D5, ___U3CU3E4__this_1)); }
	inline ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5 * get_U3CU3E4__this_1() const { return ___U3CU3E4__this_1; }
	inline ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5 ** get_address_of_U3CU3E4__this_1() { return &___U3CU3E4__this_1; }
	inline void set_U3CU3E4__this_1(ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5 * value)
	{
		___U3CU3E4__this_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS6_0_T4B40E563CC434DA33BC857EFE7A03C911BB984D5_H
#ifndef U3CU3EC__DISPLAYCLASS12_0_T83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC_H
#define U3CU3EC__DISPLAYCLASS12_0_T83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.TypeReference/<>c__DisplayClass12_0
struct  U3CU3Ec__DisplayClass12_0_t83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC  : public RuntimeObject
{
public:
	// System.String Jint.Runtime.Interop.TypeReference/<>c__DisplayClass12_0::propertyName
	String_t* ___propertyName_0;

public:
	inline static int32_t get_offset_of_propertyName_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass12_0_t83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC, ___propertyName_0)); }
	inline String_t* get_propertyName_0() const { return ___propertyName_0; }
	inline String_t** get_address_of_propertyName_0() { return &___propertyName_0; }
	inline void set_propertyName_0(String_t* value)
	{
		___propertyName_0 = value;
		Il2CppCodeGenWriteBarrier((&___propertyName_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS12_0_T83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#define FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Native.Function.FunctionInstance
struct  FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// Jint.Engine Jint.Native.Function.FunctionInstance::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;
	// Jint.Runtime.Environments.LexicalEnvironment Jint.Native.Function.FunctionInstance::<Scope>k__BackingField
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * ___U3CScopeU3Ek__BackingField_5;
	// System.String[] Jint.Native.Function.FunctionInstance::<FormalParameters>k__BackingField
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___U3CFormalParametersU3Ek__BackingField_6;
	// System.Boolean Jint.Native.Function.FunctionInstance::<Strict>k__BackingField
	bool ___U3CStrictU3Ek__BackingField_7;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}

	inline static int32_t get_offset_of_U3CScopeU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CScopeU3Ek__BackingField_5)); }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * get_U3CScopeU3Ek__BackingField_5() const { return ___U3CScopeU3Ek__BackingField_5; }
	inline LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 ** get_address_of_U3CScopeU3Ek__BackingField_5() { return &___U3CScopeU3Ek__BackingField_5; }
	inline void set_U3CScopeU3Ek__BackingField_5(LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026 * value)
	{
		___U3CScopeU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CScopeU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CFormalParametersU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CFormalParametersU3Ek__BackingField_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_U3CFormalParametersU3Ek__BackingField_6() const { return ___U3CFormalParametersU3Ek__BackingField_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_U3CFormalParametersU3Ek__BackingField_6() { return &___U3CFormalParametersU3Ek__BackingField_6; }
	inline void set_U3CFormalParametersU3Ek__BackingField_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___U3CFormalParametersU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFormalParametersU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CStrictU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458, ___U3CStrictU3Ek__BackingField_7)); }
	inline bool get_U3CStrictU3Ek__BackingField_7() const { return ___U3CStrictU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CStrictU3Ek__BackingField_7() { return &___U3CStrictU3Ek__BackingField_7; }
	inline void set_U3CStrictU3Ek__BackingField_7(bool value)
	{
		___U3CStrictU3Ek__BackingField_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONINSTANCE_TE38D99DCDA862B7930D886A656437288185E5458_H
#ifndef PARSEDPARAMETERS_T8523E604AD6C24C3157FCF091C97B1C38EAB5D7A_H
#define PARSEDPARAMETERS_T8523E604AD6C24C3157FCF091C97B1C38EAB5D7A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.JavaScriptParser/ParsedParameters
struct  ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A 
{
public:
	// Jint.Parser.Token Jint.Parser.JavaScriptParser/ParsedParameters::FirstRestricted
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___FirstRestricted_0;
	// System.String Jint.Parser.JavaScriptParser/ParsedParameters::Message
	String_t* ___Message_1;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Identifier> Jint.Parser.JavaScriptParser/ParsedParameters::Parameters
	RuntimeObject* ___Parameters_2;
	// Jint.Parser.Token Jint.Parser.JavaScriptParser/ParsedParameters::Stricted
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___Stricted_3;

public:
	inline static int32_t get_offset_of_FirstRestricted_0() { return static_cast<int32_t>(offsetof(ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A, ___FirstRestricted_0)); }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * get_FirstRestricted_0() const { return ___FirstRestricted_0; }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 ** get_address_of_FirstRestricted_0() { return &___FirstRestricted_0; }
	inline void set_FirstRestricted_0(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * value)
	{
		___FirstRestricted_0 = value;
		Il2CppCodeGenWriteBarrier((&___FirstRestricted_0), value);
	}

	inline static int32_t get_offset_of_Message_1() { return static_cast<int32_t>(offsetof(ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A, ___Message_1)); }
	inline String_t* get_Message_1() const { return ___Message_1; }
	inline String_t** get_address_of_Message_1() { return &___Message_1; }
	inline void set_Message_1(String_t* value)
	{
		___Message_1 = value;
		Il2CppCodeGenWriteBarrier((&___Message_1), value);
	}

	inline static int32_t get_offset_of_Parameters_2() { return static_cast<int32_t>(offsetof(ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A, ___Parameters_2)); }
	inline RuntimeObject* get_Parameters_2() const { return ___Parameters_2; }
	inline RuntimeObject** get_address_of_Parameters_2() { return &___Parameters_2; }
	inline void set_Parameters_2(RuntimeObject* value)
	{
		___Parameters_2 = value;
		Il2CppCodeGenWriteBarrier((&___Parameters_2), value);
	}

	inline static int32_t get_offset_of_Stricted_3() { return static_cast<int32_t>(offsetof(ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A, ___Stricted_3)); }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * get_Stricted_3() const { return ___Stricted_3; }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 ** get_address_of_Stricted_3() { return &___Stricted_3; }
	inline void set_Stricted_3(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * value)
	{
		___Stricted_3 = value;
		Il2CppCodeGenWriteBarrier((&___Stricted_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Jint.Parser.JavaScriptParser/ParsedParameters
struct ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A_marshaled_pinvoke
{
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___FirstRestricted_0;
	char* ___Message_1;
	RuntimeObject* ___Parameters_2;
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___Stricted_3;
};
// Native definition for COM marshalling of Jint.Parser.JavaScriptParser/ParsedParameters
struct ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A_marshaled_com
{
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___FirstRestricted_0;
	Il2CppChar* ___Message_1;
	RuntimeObject* ___Parameters_2;
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___Stricted_3;
};
#endif // PARSEDPARAMETERS_T8523E604AD6C24C3157FCF091C97B1C38EAB5D7A_H
#ifndef PARSEREXCEPTION_T949C8B2CFA221CFB0E3199560448F8364467C120_H
#define PARSEREXCEPTION_T949C8B2CFA221CFB0E3199560448F8364467C120_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.ParserException
struct  ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120  : public Exception_t
{
public:
	// System.Int32 Jint.Parser.ParserException::Column
	int32_t ___Column_17;
	// System.String Jint.Parser.ParserException::Description
	String_t* ___Description_18;
	// System.Int32 Jint.Parser.ParserException::Index
	int32_t ___Index_19;
	// System.Int32 Jint.Parser.ParserException::LineNumber
	int32_t ___LineNumber_20;

public:
	inline static int32_t get_offset_of_Column_17() { return static_cast<int32_t>(offsetof(ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120, ___Column_17)); }
	inline int32_t get_Column_17() const { return ___Column_17; }
	inline int32_t* get_address_of_Column_17() { return &___Column_17; }
	inline void set_Column_17(int32_t value)
	{
		___Column_17 = value;
	}

	inline static int32_t get_offset_of_Description_18() { return static_cast<int32_t>(offsetof(ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120, ___Description_18)); }
	inline String_t* get_Description_18() const { return ___Description_18; }
	inline String_t** get_address_of_Description_18() { return &___Description_18; }
	inline void set_Description_18(String_t* value)
	{
		___Description_18 = value;
		Il2CppCodeGenWriteBarrier((&___Description_18), value);
	}

	inline static int32_t get_offset_of_Index_19() { return static_cast<int32_t>(offsetof(ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120, ___Index_19)); }
	inline int32_t get_Index_19() const { return ___Index_19; }
	inline int32_t* get_address_of_Index_19() { return &___Index_19; }
	inline void set_Index_19(int32_t value)
	{
		___Index_19 = value;
	}

	inline static int32_t get_offset_of_LineNumber_20() { return static_cast<int32_t>(offsetof(ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120, ___LineNumber_20)); }
	inline int32_t get_LineNumber_20() const { return ___LineNumber_20; }
	inline int32_t* get_address_of_LineNumber_20() { return &___LineNumber_20; }
	inline void set_LineNumber_20(int32_t value)
	{
		___LineNumber_20 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSEREXCEPTION_T949C8B2CFA221CFB0E3199560448F8364467C120_H
#ifndef POSITION_T53796EA606FAC08CB873A12EF2C392F957F9D509_H
#define POSITION_T53796EA606FAC08CB873A12EF2C392F957F9D509_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Position
struct  Position_t53796EA606FAC08CB873A12EF2C392F957F9D509 
{
public:
	// System.Int32 Jint.Parser.Position::Line
	int32_t ___Line_0;
	// System.Int32 Jint.Parser.Position::Column
	int32_t ___Column_1;

public:
	inline static int32_t get_offset_of_Line_0() { return static_cast<int32_t>(offsetof(Position_t53796EA606FAC08CB873A12EF2C392F957F9D509, ___Line_0)); }
	inline int32_t get_Line_0() const { return ___Line_0; }
	inline int32_t* get_address_of_Line_0() { return &___Line_0; }
	inline void set_Line_0(int32_t value)
	{
		___Line_0 = value;
	}

	inline static int32_t get_offset_of_Column_1() { return static_cast<int32_t>(offsetof(Position_t53796EA606FAC08CB873A12EF2C392F957F9D509, ___Column_1)); }
	inline int32_t get_Column_1() const { return ___Column_1; }
	inline int32_t* get_address_of_Column_1() { return &___Column_1; }
	inline void set_Column_1(int32_t value)
	{
		___Column_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // POSITION_T53796EA606FAC08CB873A12EF2C392F957F9D509_H
#ifndef STATE_TE5E4EE87409DF109801A5725B4A75C00E666E86A_H
#define STATE_TE5E4EE87409DF109801A5725B4A75C00E666E86A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.State
struct  State_tE5E4EE87409DF109801A5725B4A75C00E666E86A 
{
public:
	// System.Int32 Jint.Parser.State::LastCommentStart
	int32_t ___LastCommentStart_0;
	// System.Boolean Jint.Parser.State::AllowIn
	bool ___AllowIn_1;
	// System.Collections.Generic.HashSet`1<System.String> Jint.Parser.State::LabelSet
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___LabelSet_2;
	// System.Boolean Jint.Parser.State::InFunctionBody
	bool ___InFunctionBody_3;
	// System.Boolean Jint.Parser.State::InIteration
	bool ___InIteration_4;
	// System.Boolean Jint.Parser.State::InSwitch
	bool ___InSwitch_5;
	// System.Collections.Generic.Stack`1<System.Int32> Jint.Parser.State::MarkerStack
	Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * ___MarkerStack_6;

public:
	inline static int32_t get_offset_of_LastCommentStart_0() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___LastCommentStart_0)); }
	inline int32_t get_LastCommentStart_0() const { return ___LastCommentStart_0; }
	inline int32_t* get_address_of_LastCommentStart_0() { return &___LastCommentStart_0; }
	inline void set_LastCommentStart_0(int32_t value)
	{
		___LastCommentStart_0 = value;
	}

	inline static int32_t get_offset_of_AllowIn_1() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___AllowIn_1)); }
	inline bool get_AllowIn_1() const { return ___AllowIn_1; }
	inline bool* get_address_of_AllowIn_1() { return &___AllowIn_1; }
	inline void set_AllowIn_1(bool value)
	{
		___AllowIn_1 = value;
	}

	inline static int32_t get_offset_of_LabelSet_2() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___LabelSet_2)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_LabelSet_2() const { return ___LabelSet_2; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_LabelSet_2() { return &___LabelSet_2; }
	inline void set_LabelSet_2(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___LabelSet_2 = value;
		Il2CppCodeGenWriteBarrier((&___LabelSet_2), value);
	}

	inline static int32_t get_offset_of_InFunctionBody_3() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___InFunctionBody_3)); }
	inline bool get_InFunctionBody_3() const { return ___InFunctionBody_3; }
	inline bool* get_address_of_InFunctionBody_3() { return &___InFunctionBody_3; }
	inline void set_InFunctionBody_3(bool value)
	{
		___InFunctionBody_3 = value;
	}

	inline static int32_t get_offset_of_InIteration_4() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___InIteration_4)); }
	inline bool get_InIteration_4() const { return ___InIteration_4; }
	inline bool* get_address_of_InIteration_4() { return &___InIteration_4; }
	inline void set_InIteration_4(bool value)
	{
		___InIteration_4 = value;
	}

	inline static int32_t get_offset_of_InSwitch_5() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___InSwitch_5)); }
	inline bool get_InSwitch_5() const { return ___InSwitch_5; }
	inline bool* get_address_of_InSwitch_5() { return &___InSwitch_5; }
	inline void set_InSwitch_5(bool value)
	{
		___InSwitch_5 = value;
	}

	inline static int32_t get_offset_of_MarkerStack_6() { return static_cast<int32_t>(offsetof(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A, ___MarkerStack_6)); }
	inline Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * get_MarkerStack_6() const { return ___MarkerStack_6; }
	inline Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A ** get_address_of_MarkerStack_6() { return &___MarkerStack_6; }
	inline void set_MarkerStack_6(Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * value)
	{
		___MarkerStack_6 = value;
		Il2CppCodeGenWriteBarrier((&___MarkerStack_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Jint.Parser.State
struct State_tE5E4EE87409DF109801A5725B4A75C00E666E86A_marshaled_pinvoke
{
	int32_t ___LastCommentStart_0;
	int32_t ___AllowIn_1;
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___LabelSet_2;
	int32_t ___InFunctionBody_3;
	int32_t ___InIteration_4;
	int32_t ___InSwitch_5;
	Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * ___MarkerStack_6;
};
// Native definition for COM marshalling of Jint.Parser.State
struct State_tE5E4EE87409DF109801A5725B4A75C00E666E86A_marshaled_com
{
	int32_t ___LastCommentStart_0;
	int32_t ___AllowIn_1;
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___LabelSet_2;
	int32_t ___InFunctionBody_3;
	int32_t ___InIteration_4;
	int32_t ___InSwitch_5;
	Stack_1_t09A8EBB6E2F5CCF7E2E67BDECA461795F93D523A * ___MarkerStack_6;
};
#endif // STATE_TE5E4EE87409DF109801A5725B4A75C00E666E86A_H
#ifndef DEBUGINFORMATION_T1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C_H
#define DEBUGINFORMATION_T1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Debugger.DebugInformation
struct  DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Collections.Generic.Stack`1<System.String> Jint.Runtime.Debugger.DebugInformation::<CallStack>k__BackingField
	Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * ___U3CCallStackU3Ek__BackingField_1;
	// Jint.Parser.Ast.Statement Jint.Runtime.Debugger.DebugInformation::<CurrentStatement>k__BackingField
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___U3CCurrentStatementU3Ek__BackingField_2;
	// System.Collections.Generic.Dictionary`2<System.String,Jint.Native.JsValue> Jint.Runtime.Debugger.DebugInformation::<Locals>k__BackingField
	Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B * ___U3CLocalsU3Ek__BackingField_3;
	// System.Collections.Generic.Dictionary`2<System.String,Jint.Native.JsValue> Jint.Runtime.Debugger.DebugInformation::<Globals>k__BackingField
	Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B * ___U3CGlobalsU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CCallStackU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C, ___U3CCallStackU3Ek__BackingField_1)); }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * get_U3CCallStackU3Ek__BackingField_1() const { return ___U3CCallStackU3Ek__BackingField_1; }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 ** get_address_of_U3CCallStackU3Ek__BackingField_1() { return &___U3CCallStackU3Ek__BackingField_1; }
	inline void set_U3CCallStackU3Ek__BackingField_1(Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * value)
	{
		___U3CCallStackU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCallStackU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CCurrentStatementU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C, ___U3CCurrentStatementU3Ek__BackingField_2)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_U3CCurrentStatementU3Ek__BackingField_2() const { return ___U3CCurrentStatementU3Ek__BackingField_2; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_U3CCurrentStatementU3Ek__BackingField_2() { return &___U3CCurrentStatementU3Ek__BackingField_2; }
	inline void set_U3CCurrentStatementU3Ek__BackingField_2(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___U3CCurrentStatementU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CCurrentStatementU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CLocalsU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C, ___U3CLocalsU3Ek__BackingField_3)); }
	inline Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B * get_U3CLocalsU3Ek__BackingField_3() const { return ___U3CLocalsU3Ek__BackingField_3; }
	inline Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B ** get_address_of_U3CLocalsU3Ek__BackingField_3() { return &___U3CLocalsU3Ek__BackingField_3; }
	inline void set_U3CLocalsU3Ek__BackingField_3(Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B * value)
	{
		___U3CLocalsU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CLocalsU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CGlobalsU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C, ___U3CGlobalsU3Ek__BackingField_4)); }
	inline Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B * get_U3CGlobalsU3Ek__BackingField_4() const { return ___U3CGlobalsU3Ek__BackingField_4; }
	inline Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B ** get_address_of_U3CGlobalsU3Ek__BackingField_4() { return &___U3CGlobalsU3Ek__BackingField_4; }
	inline void set_U3CGlobalsU3Ek__BackingField_4(Dictionary_2_tE7ACF5874D4D68340D4551DF7C2228271F587A0B * value)
	{
		___U3CGlobalsU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGlobalsU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGINFORMATION_T1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C_H
#ifndef ENVIRONMENTRECORD_T81E449E16161905BFB3BD8D0596E82E2619FB53B_H
#define ENVIRONMENTRECORD_T81E449E16161905BFB3BD8D0596E82E2619FB53B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.EnvironmentRecord
struct  EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENVIRONMENTRECORD_T81E449E16161905BFB3BD8D0596E82E2619FB53B_H
#ifndef NAMESPACEREFERENCE_TDE03F4B45F487622D8D00B08AA7B0143097C203D_H
#define NAMESPACEREFERENCE_TDE03F4B45F487622D8D00B08AA7B0143097C203D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.NamespaceReference
struct  NamespaceReference_tDE03F4B45F487622D8D00B08AA7B0143097C203D  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// System.String Jint.Runtime.Interop.NamespaceReference::_path
	String_t* ____path_4;

public:
	inline static int32_t get_offset_of__path_4() { return static_cast<int32_t>(offsetof(NamespaceReference_tDE03F4B45F487622D8D00B08AA7B0143097C203D, ____path_4)); }
	inline String_t* get__path_4() const { return ____path_4; }
	inline String_t** get_address_of__path_4() { return &____path_4; }
	inline void set__path_4(String_t* value)
	{
		____path_4 = value;
		Il2CppCodeGenWriteBarrier((&____path_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEREFERENCE_TDE03F4B45F487622D8D00B08AA7B0143097C203D_H
#ifndef OBJECTWRAPPER_TDD4EA894ED1C73DF983BE152B6067702D9BB5EB5_H
#define OBJECTWRAPPER_TDD4EA894ED1C73DF983BE152B6067702D9BB5EB5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.ObjectWrapper
struct  ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5  : public ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900
{
public:
	// System.Object Jint.Runtime.Interop.ObjectWrapper::<Target>k__BackingField
	RuntimeObject * ___U3CTargetU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CTargetU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5, ___U3CTargetU3Ek__BackingField_4)); }
	inline RuntimeObject * get_U3CTargetU3Ek__BackingField_4() const { return ___U3CTargetU3Ek__BackingField_4; }
	inline RuntimeObject ** get_address_of_U3CTargetU3Ek__BackingField_4() { return &___U3CTargetU3Ek__BackingField_4; }
	inline void set_U3CTargetU3Ek__BackingField_4(RuntimeObject * value)
	{
		___U3CTargetU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTargetU3Ek__BackingField_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTWRAPPER_TDD4EA894ED1C73DF983BE152B6067702D9BB5EB5_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#define NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Boolean>
struct  Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 
{
public:
	// T System.Nullable`1::value
	bool ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___value_0)); }
	inline bool get_value_0() const { return ___value_0; }
	inline bool* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(bool value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T9E6A67BECE376F0623B5C857F5674A0311C41793_H
#ifndef NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#define NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Nullable`1<System.Int32>
struct  Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB 
{
public:
	// T System.Nullable`1::value
	int32_t ___value_0;
	// System.Boolean System.Nullable`1::has_value
	bool ___has_value_1;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___value_0)); }
	inline int32_t get_value_0() const { return ___value_0; }
	inline int32_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(int32_t value)
	{
		___value_0 = value;
	}

	inline static int32_t get_offset_of_has_value_1() { return static_cast<int32_t>(offsetof(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB, ___has_value_1)); }
	inline bool get_has_value_1() const { return ___has_value_1; }
	inline bool* get_address_of_has_value_1() { return &___has_value_1; }
	inline void set_has_value_1(bool value)
	{
		___has_value_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NULLABLE_1_T0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB_H
#ifndef ASSIGNMENTOPERATOR_TADB31EB9CA5217BCD25CBE630A560E9F33EC9E41_H
#define ASSIGNMENTOPERATOR_TADB31EB9CA5217BCD25CBE630A560E9F33EC9E41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.AssignmentOperator
struct  AssignmentOperator_tADB31EB9CA5217BCD25CBE630A560E9F33EC9E41 
{
public:
	// System.Int32 Jint.Parser.Ast.AssignmentOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AssignmentOperator_tADB31EB9CA5217BCD25CBE630A560E9F33EC9E41, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIGNMENTOPERATOR_TADB31EB9CA5217BCD25CBE630A560E9F33EC9E41_H
#ifndef BINARYOPERATOR_T5651229A29A80B1D899E3764D062DE8E4BC0A5E6_H
#define BINARYOPERATOR_T5651229A29A80B1D899E3764D062DE8E4BC0A5E6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.BinaryOperator
struct  BinaryOperator_t5651229A29A80B1D899E3764D062DE8E4BC0A5E6 
{
public:
	// System.Int32 Jint.Parser.Ast.BinaryOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BinaryOperator_t5651229A29A80B1D899E3764D062DE8E4BC0A5E6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYOPERATOR_T5651229A29A80B1D899E3764D062DE8E4BC0A5E6_H
#ifndef LOGICALOPERATOR_TD30BF74503BE6556A2EBBA96A165809F4050F716_H
#define LOGICALOPERATOR_TD30BF74503BE6556A2EBBA96A165809F4050F716_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.LogicalOperator
struct  LogicalOperator_tD30BF74503BE6556A2EBBA96A165809F4050F716 
{
public:
	// System.Int32 Jint.Parser.Ast.LogicalOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(LogicalOperator_tD30BF74503BE6556A2EBBA96A165809F4050F716, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGICALOPERATOR_TD30BF74503BE6556A2EBBA96A165809F4050F716_H
#ifndef PROPERTYKIND_T894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7_H
#define PROPERTYKIND_T894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.PropertyKind
struct  PropertyKind_t894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7 
{
public:
	// System.Int32 Jint.Parser.Ast.PropertyKind::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(PropertyKind_t894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYKIND_T894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7_H
#ifndef SYNTAXNODES_TE429806CC3C5D818D51936071CF565AA6B61D4C6_H
#define SYNTAXNODES_TE429806CC3C5D818D51936071CF565AA6B61D4C6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SyntaxNodes
struct  SyntaxNodes_tE429806CC3C5D818D51936071CF565AA6B61D4C6 
{
public:
	// System.Int32 Jint.Parser.Ast.SyntaxNodes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SyntaxNodes_tE429806CC3C5D818D51936071CF565AA6B61D4C6, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXNODES_TE429806CC3C5D818D51936071CF565AA6B61D4C6_H
#ifndef UNARYOPERATOR_T1191338D6F31B2D463708FAB982A7CDB841F0255_H
#define UNARYOPERATOR_T1191338D6F31B2D463708FAB982A7CDB841F0255_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.UnaryOperator
struct  UnaryOperator_t1191338D6F31B2D463708FAB982A7CDB841F0255 
{
public:
	// System.Int32 Jint.Parser.Ast.UnaryOperator::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(UnaryOperator_t1191338D6F31B2D463708FAB982A7CDB841F0255, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNARYOPERATOR_T1191338D6F31B2D463708FAB982A7CDB841F0255_H
#ifndef JAVASCRIPTPARSER_TC33321877DC5FF0F10EC4092D2CC04BE25064E72_H
#define JAVASCRIPTPARSER_TC33321877DC5FF0F10EC4092D2CC04BE25064E72_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.JavaScriptParser
struct  JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72  : public RuntimeObject
{
public:
	// Jint.Parser.JavaScriptParser/Extra Jint.Parser.JavaScriptParser::_extra
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5 * ____extra_3;
	// System.Int32 Jint.Parser.JavaScriptParser::_index
	int32_t ____index_4;
	// System.Int32 Jint.Parser.JavaScriptParser::_length
	int32_t ____length_5;
	// System.Int32 Jint.Parser.JavaScriptParser::_lineNumber
	int32_t ____lineNumber_6;
	// System.Int32 Jint.Parser.JavaScriptParser::_lineStart
	int32_t ____lineStart_7;
	// Jint.Parser.Location Jint.Parser.JavaScriptParser::_location
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ____location_8;
	// Jint.Parser.Token Jint.Parser.JavaScriptParser::_lookahead
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ____lookahead_9;
	// System.String Jint.Parser.JavaScriptParser::_source
	String_t* ____source_10;
	// Jint.Parser.State Jint.Parser.JavaScriptParser::_state
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A  ____state_11;
	// System.Boolean Jint.Parser.JavaScriptParser::_strict
	bool ____strict_12;
	// System.Collections.Generic.Stack`1<Jint.Parser.IVariableScope> Jint.Parser.JavaScriptParser::_variableScopes
	Stack_1_tC53B481C4C5615750A5D4269B4C01695C4326640 * ____variableScopes_13;
	// System.Collections.Generic.Stack`1<Jint.Parser.IFunctionScope> Jint.Parser.JavaScriptParser::_functionScopes
	Stack_1_t4C250BB2D2BCCCC50AFECE824126AAB97836ECC4 * ____functionScopes_14;

public:
	inline static int32_t get_offset_of__extra_3() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____extra_3)); }
	inline Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5 * get__extra_3() const { return ____extra_3; }
	inline Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5 ** get_address_of__extra_3() { return &____extra_3; }
	inline void set__extra_3(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5 * value)
	{
		____extra_3 = value;
		Il2CppCodeGenWriteBarrier((&____extra_3), value);
	}

	inline static int32_t get_offset_of__index_4() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____index_4)); }
	inline int32_t get__index_4() const { return ____index_4; }
	inline int32_t* get_address_of__index_4() { return &____index_4; }
	inline void set__index_4(int32_t value)
	{
		____index_4 = value;
	}

	inline static int32_t get_offset_of__length_5() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____length_5)); }
	inline int32_t get__length_5() const { return ____length_5; }
	inline int32_t* get_address_of__length_5() { return &____length_5; }
	inline void set__length_5(int32_t value)
	{
		____length_5 = value;
	}

	inline static int32_t get_offset_of__lineNumber_6() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____lineNumber_6)); }
	inline int32_t get__lineNumber_6() const { return ____lineNumber_6; }
	inline int32_t* get_address_of__lineNumber_6() { return &____lineNumber_6; }
	inline void set__lineNumber_6(int32_t value)
	{
		____lineNumber_6 = value;
	}

	inline static int32_t get_offset_of__lineStart_7() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____lineStart_7)); }
	inline int32_t get__lineStart_7() const { return ____lineStart_7; }
	inline int32_t* get_address_of__lineStart_7() { return &____lineStart_7; }
	inline void set__lineStart_7(int32_t value)
	{
		____lineStart_7 = value;
	}

	inline static int32_t get_offset_of__location_8() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____location_8)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get__location_8() const { return ____location_8; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of__location_8() { return &____location_8; }
	inline void set__location_8(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		____location_8 = value;
		Il2CppCodeGenWriteBarrier((&____location_8), value);
	}

	inline static int32_t get_offset_of__lookahead_9() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____lookahead_9)); }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * get__lookahead_9() const { return ____lookahead_9; }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 ** get_address_of__lookahead_9() { return &____lookahead_9; }
	inline void set__lookahead_9(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * value)
	{
		____lookahead_9 = value;
		Il2CppCodeGenWriteBarrier((&____lookahead_9), value);
	}

	inline static int32_t get_offset_of__source_10() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____source_10)); }
	inline String_t* get__source_10() const { return ____source_10; }
	inline String_t** get_address_of__source_10() { return &____source_10; }
	inline void set__source_10(String_t* value)
	{
		____source_10 = value;
		Il2CppCodeGenWriteBarrier((&____source_10), value);
	}

	inline static int32_t get_offset_of__state_11() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____state_11)); }
	inline State_tE5E4EE87409DF109801A5725B4A75C00E666E86A  get__state_11() const { return ____state_11; }
	inline State_tE5E4EE87409DF109801A5725B4A75C00E666E86A * get_address_of__state_11() { return &____state_11; }
	inline void set__state_11(State_tE5E4EE87409DF109801A5725B4A75C00E666E86A  value)
	{
		____state_11 = value;
	}

	inline static int32_t get_offset_of__strict_12() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____strict_12)); }
	inline bool get__strict_12() const { return ____strict_12; }
	inline bool* get_address_of__strict_12() { return &____strict_12; }
	inline void set__strict_12(bool value)
	{
		____strict_12 = value;
	}

	inline static int32_t get_offset_of__variableScopes_13() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____variableScopes_13)); }
	inline Stack_1_tC53B481C4C5615750A5D4269B4C01695C4326640 * get__variableScopes_13() const { return ____variableScopes_13; }
	inline Stack_1_tC53B481C4C5615750A5D4269B4C01695C4326640 ** get_address_of__variableScopes_13() { return &____variableScopes_13; }
	inline void set__variableScopes_13(Stack_1_tC53B481C4C5615750A5D4269B4C01695C4326640 * value)
	{
		____variableScopes_13 = value;
		Il2CppCodeGenWriteBarrier((&____variableScopes_13), value);
	}

	inline static int32_t get_offset_of__functionScopes_14() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72, ____functionScopes_14)); }
	inline Stack_1_t4C250BB2D2BCCCC50AFECE824126AAB97836ECC4 * get__functionScopes_14() const { return ____functionScopes_14; }
	inline Stack_1_t4C250BB2D2BCCCC50AFECE824126AAB97836ECC4 ** get_address_of__functionScopes_14() { return &____functionScopes_14; }
	inline void set__functionScopes_14(Stack_1_t4C250BB2D2BCCCC50AFECE824126AAB97836ECC4 * value)
	{
		____functionScopes_14 = value;
		Il2CppCodeGenWriteBarrier((&____functionScopes_14), value);
	}
};

struct JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields
{
public:
	// System.Collections.Generic.HashSet`1<System.String> Jint.Parser.JavaScriptParser::Keywords
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___Keywords_0;
	// System.Collections.Generic.HashSet`1<System.String> Jint.Parser.JavaScriptParser::StrictModeReservedWords
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___StrictModeReservedWords_1;
	// System.Collections.Generic.HashSet`1<System.String> Jint.Parser.JavaScriptParser::FutureReservedWords
	HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * ___FutureReservedWords_2;

public:
	inline static int32_t get_offset_of_Keywords_0() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields, ___Keywords_0)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_Keywords_0() const { return ___Keywords_0; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_Keywords_0() { return &___Keywords_0; }
	inline void set_Keywords_0(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___Keywords_0 = value;
		Il2CppCodeGenWriteBarrier((&___Keywords_0), value);
	}

	inline static int32_t get_offset_of_StrictModeReservedWords_1() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields, ___StrictModeReservedWords_1)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_StrictModeReservedWords_1() const { return ___StrictModeReservedWords_1; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_StrictModeReservedWords_1() { return &___StrictModeReservedWords_1; }
	inline void set_StrictModeReservedWords_1(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___StrictModeReservedWords_1 = value;
		Il2CppCodeGenWriteBarrier((&___StrictModeReservedWords_1), value);
	}

	inline static int32_t get_offset_of_FutureReservedWords_2() { return static_cast<int32_t>(offsetof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields, ___FutureReservedWords_2)); }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * get_FutureReservedWords_2() const { return ___FutureReservedWords_2; }
	inline HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A ** get_address_of_FutureReservedWords_2() { return &___FutureReservedWords_2; }
	inline void set_FutureReservedWords_2(HashSet_1_tAF31CD0D5B1F27E354D2DA33F8CF07F490E8C98A * value)
	{
		___FutureReservedWords_2 = value;
		Il2CppCodeGenWriteBarrier((&___FutureReservedWords_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // JAVASCRIPTPARSER_TC33321877DC5FF0F10EC4092D2CC04BE25064E72_H
#ifndef EXTRA_TDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5_H
#define EXTRA_TDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.JavaScriptParser/Extra
struct  Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5  : public RuntimeObject
{
public:
	// System.Nullable`1<System.Int32> Jint.Parser.JavaScriptParser/Extra::Loc
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___Loc_0;
	// System.Int32[] Jint.Parser.JavaScriptParser/Extra::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_1;
	// System.String Jint.Parser.JavaScriptParser/Extra::Source
	String_t* ___Source_2;
	// System.Collections.Generic.List`1<Jint.Parser.Comment> Jint.Parser.JavaScriptParser/Extra::Comments
	List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 * ___Comments_3;
	// System.Collections.Generic.List`1<Jint.Parser.Token> Jint.Parser.JavaScriptParser/Extra::Tokens
	List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 * ___Tokens_4;
	// System.Collections.Generic.List`1<Jint.Parser.ParserException> Jint.Parser.JavaScriptParser/Extra::Errors
	List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 * ___Errors_5;

public:
	inline static int32_t get_offset_of_Loc_0() { return static_cast<int32_t>(offsetof(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5, ___Loc_0)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_Loc_0() const { return ___Loc_0; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_Loc_0() { return &___Loc_0; }
	inline void set_Loc_0(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___Loc_0 = value;
	}

	inline static int32_t get_offset_of_Range_1() { return static_cast<int32_t>(offsetof(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5, ___Range_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_1() const { return ___Range_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_1() { return &___Range_1; }
	inline void set_Range_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_1 = value;
		Il2CppCodeGenWriteBarrier((&___Range_1), value);
	}

	inline static int32_t get_offset_of_Source_2() { return static_cast<int32_t>(offsetof(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5, ___Source_2)); }
	inline String_t* get_Source_2() const { return ___Source_2; }
	inline String_t** get_address_of_Source_2() { return &___Source_2; }
	inline void set_Source_2(String_t* value)
	{
		___Source_2 = value;
		Il2CppCodeGenWriteBarrier((&___Source_2), value);
	}

	inline static int32_t get_offset_of_Comments_3() { return static_cast<int32_t>(offsetof(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5, ___Comments_3)); }
	inline List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 * get_Comments_3() const { return ___Comments_3; }
	inline List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 ** get_address_of_Comments_3() { return &___Comments_3; }
	inline void set_Comments_3(List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 * value)
	{
		___Comments_3 = value;
		Il2CppCodeGenWriteBarrier((&___Comments_3), value);
	}

	inline static int32_t get_offset_of_Tokens_4() { return static_cast<int32_t>(offsetof(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5, ___Tokens_4)); }
	inline List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 * get_Tokens_4() const { return ___Tokens_4; }
	inline List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 ** get_address_of_Tokens_4() { return &___Tokens_4; }
	inline void set_Tokens_4(List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 * value)
	{
		___Tokens_4 = value;
		Il2CppCodeGenWriteBarrier((&___Tokens_4), value);
	}

	inline static int32_t get_offset_of_Errors_5() { return static_cast<int32_t>(offsetof(Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5, ___Errors_5)); }
	inline List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 * get_Errors_5() const { return ___Errors_5; }
	inline List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 ** get_address_of_Errors_5() { return &___Errors_5; }
	inline void set_Errors_5(List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 * value)
	{
		___Errors_5 = value;
		Il2CppCodeGenWriteBarrier((&___Errors_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXTRA_TDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5_H
#ifndef LOCATION_T9639547EECD85868A9B1627343BF1C80DE46B783_H
#define LOCATION_T9639547EECD85868A9B1627343BF1C80DE46B783_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Location
struct  Location_t9639547EECD85868A9B1627343BF1C80DE46B783  : public RuntimeObject
{
public:
	// Jint.Parser.Position Jint.Parser.Location::Start
	Position_t53796EA606FAC08CB873A12EF2C392F957F9D509  ___Start_0;
	// Jint.Parser.Position Jint.Parser.Location::End
	Position_t53796EA606FAC08CB873A12EF2C392F957F9D509  ___End_1;
	// System.String Jint.Parser.Location::Source
	String_t* ___Source_2;

public:
	inline static int32_t get_offset_of_Start_0() { return static_cast<int32_t>(offsetof(Location_t9639547EECD85868A9B1627343BF1C80DE46B783, ___Start_0)); }
	inline Position_t53796EA606FAC08CB873A12EF2C392F957F9D509  get_Start_0() const { return ___Start_0; }
	inline Position_t53796EA606FAC08CB873A12EF2C392F957F9D509 * get_address_of_Start_0() { return &___Start_0; }
	inline void set_Start_0(Position_t53796EA606FAC08CB873A12EF2C392F957F9D509  value)
	{
		___Start_0 = value;
	}

	inline static int32_t get_offset_of_End_1() { return static_cast<int32_t>(offsetof(Location_t9639547EECD85868A9B1627343BF1C80DE46B783, ___End_1)); }
	inline Position_t53796EA606FAC08CB873A12EF2C392F957F9D509  get_End_1() const { return ___End_1; }
	inline Position_t53796EA606FAC08CB873A12EF2C392F957F9D509 * get_address_of_End_1() { return &___End_1; }
	inline void set_End_1(Position_t53796EA606FAC08CB873A12EF2C392F957F9D509  value)
	{
		___End_1 = value;
	}

	inline static int32_t get_offset_of_Source_2() { return static_cast<int32_t>(offsetof(Location_t9639547EECD85868A9B1627343BF1C80DE46B783, ___Source_2)); }
	inline String_t* get_Source_2() const { return ___Source_2; }
	inline String_t** get_address_of_Source_2() { return &___Source_2; }
	inline void set_Source_2(String_t* value)
	{
		___Source_2 = value;
		Il2CppCodeGenWriteBarrier((&___Source_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOCATION_T9639547EECD85868A9B1627343BF1C80DE46B783_H
#ifndef TOKENS_T76408800A95ECB3912AC7B31208B6E2531669559_H
#define TOKENS_T76408800A95ECB3912AC7B31208B6E2531669559_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Tokens
struct  Tokens_t76408800A95ECB3912AC7B31208B6E2531669559 
{
public:
	// System.Int32 Jint.Parser.Tokens::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Tokens_t76408800A95ECB3912AC7B31208B6E2531669559, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKENS_T76408800A95ECB3912AC7B31208B6E2531669559_H
#ifndef STEPMODE_TF8A960EEDD1BEF8767A1D32BC546D0BB17792D23_H
#define STEPMODE_TF8A960EEDD1BEF8767A1D32BC546D0BB17792D23_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Debugger.StepMode
struct  StepMode_tF8A960EEDD1BEF8767A1D32BC546D0BB17792D23 
{
public:
	// System.Int32 Jint.Runtime.Debugger.StepMode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(StepMode_tF8A960EEDD1BEF8767A1D32BC546D0BB17792D23, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STEPMODE_TF8A960EEDD1BEF8767A1D32BC546D0BB17792D23_H
#ifndef PROPERTYDESCRIPTOR_TC246E1A601A5DD8127DE7859409B89D3B1939A73_H
#define PROPERTYDESCRIPTOR_TC246E1A601A5DD8127DE7859409B89D3B1939A73_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Descriptors.PropertyDescriptor
struct  PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73  : public RuntimeObject
{
public:
	// Jint.Native.JsValue Jint.Runtime.Descriptors.PropertyDescriptor::<Get>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CGetU3Ek__BackingField_1;
	// Jint.Native.JsValue Jint.Runtime.Descriptors.PropertyDescriptor::<Set>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CSetU3Ek__BackingField_2;
	// System.Nullable`1<System.Boolean> Jint.Runtime.Descriptors.PropertyDescriptor::<Enumerable>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CEnumerableU3Ek__BackingField_3;
	// System.Nullable`1<System.Boolean> Jint.Runtime.Descriptors.PropertyDescriptor::<Writable>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CWritableU3Ek__BackingField_4;
	// System.Nullable`1<System.Boolean> Jint.Runtime.Descriptors.PropertyDescriptor::<Configurable>k__BackingField
	Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  ___U3CConfigurableU3Ek__BackingField_5;
	// Jint.Native.JsValue Jint.Runtime.Descriptors.PropertyDescriptor::<Value>k__BackingField
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___U3CValueU3Ek__BackingField_6;

public:
	inline static int32_t get_offset_of_U3CGetU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73, ___U3CGetU3Ek__BackingField_1)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CGetU3Ek__BackingField_1() const { return ___U3CGetU3Ek__BackingField_1; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CGetU3Ek__BackingField_1() { return &___U3CGetU3Ek__BackingField_1; }
	inline void set_U3CGetU3Ek__BackingField_1(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CGetU3Ek__BackingField_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CGetU3Ek__BackingField_1), value);
	}

	inline static int32_t get_offset_of_U3CSetU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73, ___U3CSetU3Ek__BackingField_2)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CSetU3Ek__BackingField_2() const { return ___U3CSetU3Ek__BackingField_2; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CSetU3Ek__BackingField_2() { return &___U3CSetU3Ek__BackingField_2; }
	inline void set_U3CSetU3Ek__BackingField_2(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CSetU3Ek__BackingField_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CSetU3Ek__BackingField_2), value);
	}

	inline static int32_t get_offset_of_U3CEnumerableU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73, ___U3CEnumerableU3Ek__BackingField_3)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CEnumerableU3Ek__BackingField_3() const { return ___U3CEnumerableU3Ek__BackingField_3; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CEnumerableU3Ek__BackingField_3() { return &___U3CEnumerableU3Ek__BackingField_3; }
	inline void set_U3CEnumerableU3Ek__BackingField_3(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CEnumerableU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CWritableU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73, ___U3CWritableU3Ek__BackingField_4)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CWritableU3Ek__BackingField_4() const { return ___U3CWritableU3Ek__BackingField_4; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CWritableU3Ek__BackingField_4() { return &___U3CWritableU3Ek__BackingField_4; }
	inline void set_U3CWritableU3Ek__BackingField_4(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CWritableU3Ek__BackingField_4 = value;
	}

	inline static int32_t get_offset_of_U3CConfigurableU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73, ___U3CConfigurableU3Ek__BackingField_5)); }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  get_U3CConfigurableU3Ek__BackingField_5() const { return ___U3CConfigurableU3Ek__BackingField_5; }
	inline Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793 * get_address_of_U3CConfigurableU3Ek__BackingField_5() { return &___U3CConfigurableU3Ek__BackingField_5; }
	inline void set_U3CConfigurableU3Ek__BackingField_5(Nullable_1_t9E6A67BECE376F0623B5C857F5674A0311C41793  value)
	{
		___U3CConfigurableU3Ek__BackingField_5 = value;
	}

	inline static int32_t get_offset_of_U3CValueU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73, ___U3CValueU3Ek__BackingField_6)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_U3CValueU3Ek__BackingField_6() const { return ___U3CValueU3Ek__BackingField_6; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_U3CValueU3Ek__BackingField_6() { return &___U3CValueU3Ek__BackingField_6; }
	inline void set_U3CValueU3Ek__BackingField_6(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___U3CValueU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CValueU3Ek__BackingField_6), value);
	}
};

struct PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73_StaticFields
{
public:
	// Jint.Runtime.Descriptors.PropertyDescriptor Jint.Runtime.Descriptors.PropertyDescriptor::Undefined
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * ___Undefined_0;

public:
	inline static int32_t get_offset_of_Undefined_0() { return static_cast<int32_t>(offsetof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73_StaticFields, ___Undefined_0)); }
	inline PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * get_Undefined_0() const { return ___Undefined_0; }
	inline PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 ** get_address_of_Undefined_0() { return &___Undefined_0; }
	inline void set_Undefined_0(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73 * value)
	{
		___Undefined_0 = value;
		Il2CppCodeGenWriteBarrier((&___Undefined_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYDESCRIPTOR_TC246E1A601A5DD8127DE7859409B89D3B1939A73_H
#ifndef DECLARATIVEENVIRONMENTRECORD_TEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3_H
#define DECLARATIVEENVIRONMENTRECORD_TEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.DeclarativeEnvironmentRecord
struct  DeclarativeEnvironmentRecord_tEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3  : public EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B
{
public:
	// Jint.Engine Jint.Runtime.Environments.DeclarativeEnvironmentRecord::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;
	// System.Collections.Generic.IDictionary`2<System.String,Jint.Runtime.Environments.Binding> Jint.Runtime.Environments.DeclarativeEnvironmentRecord::_bindings
	RuntimeObject* ____bindings_5;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(DeclarativeEnvironmentRecord_tEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}

	inline static int32_t get_offset_of__bindings_5() { return static_cast<int32_t>(offsetof(DeclarativeEnvironmentRecord_tEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3, ____bindings_5)); }
	inline RuntimeObject* get__bindings_5() const { return ____bindings_5; }
	inline RuntimeObject** get_address_of__bindings_5() { return &____bindings_5; }
	inline void set__bindings_5(RuntimeObject* value)
	{
		____bindings_5 = value;
		Il2CppCodeGenWriteBarrier((&____bindings_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECLARATIVEENVIRONMENTRECORD_TEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3_H
#ifndef OBJECTENVIRONMENTRECORD_T735FE784390C989CDD891FE4BEA0C938D71F1F64_H
#define OBJECTENVIRONMENTRECORD_T735FE784390C989CDD891FE4BEA0C938D71F1F64_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Environments.ObjectEnvironmentRecord
struct  ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64  : public EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B
{
public:
	// Jint.Engine Jint.Runtime.Environments.ObjectEnvironmentRecord::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_4;
	// Jint.Native.Object.ObjectInstance Jint.Runtime.Environments.ObjectEnvironmentRecord::_bindingObject
	ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * ____bindingObject_5;
	// System.Boolean Jint.Runtime.Environments.ObjectEnvironmentRecord::_provideThis
	bool ____provideThis_6;

public:
	inline static int32_t get_offset_of__engine_4() { return static_cast<int32_t>(offsetof(ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64, ____engine_4)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_4() const { return ____engine_4; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_4() { return &____engine_4; }
	inline void set__engine_4(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_4 = value;
		Il2CppCodeGenWriteBarrier((&____engine_4), value);
	}

	inline static int32_t get_offset_of__bindingObject_5() { return static_cast<int32_t>(offsetof(ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64, ____bindingObject_5)); }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * get__bindingObject_5() const { return ____bindingObject_5; }
	inline ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 ** get_address_of__bindingObject_5() { return &____bindingObject_5; }
	inline void set__bindingObject_5(ObjectInstance_t9A33413FF3D113AEBA2D280D5498C04222E32900 * value)
	{
		____bindingObject_5 = value;
		Il2CppCodeGenWriteBarrier((&____bindingObject_5), value);
	}

	inline static int32_t get_offset_of__provideThis_6() { return static_cast<int32_t>(offsetof(ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64, ____provideThis_6)); }
	inline bool get__provideThis_6() const { return ____provideThis_6; }
	inline bool* get_address_of__provideThis_6() { return &____provideThis_6; }
	inline void set__provideThis_6(bool value)
	{
		____provideThis_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTENVIRONMENTRECORD_T735FE784390C989CDD891FE4BEA0C938D71F1F64_H
#ifndef METHODINFOFUNCTIONINSTANCE_T483F3C5DB3B58053A1233B23940AF856A467DE5C_H
#define METHODINFOFUNCTIONINSTANCE_T483F3C5DB3B58053A1233B23940AF856A467DE5C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.MethodInfoFunctionInstance
struct  MethodInfoFunctionInstance_t483F3C5DB3B58053A1233B23940AF856A467DE5C  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.Reflection.MethodInfo[] Jint.Runtime.Interop.MethodInfoFunctionInstance::_methods
	MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* ____methods_8;

public:
	inline static int32_t get_offset_of__methods_8() { return static_cast<int32_t>(offsetof(MethodInfoFunctionInstance_t483F3C5DB3B58053A1233B23940AF856A467DE5C, ____methods_8)); }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* get__methods_8() const { return ____methods_8; }
	inline MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B** get_address_of__methods_8() { return &____methods_8; }
	inline void set__methods_8(MethodInfoU5BU5D_t93E968F23AF2DB5CFCFF13BE775A0E222C03586B* value)
	{
		____methods_8 = value;
		Il2CppCodeGenWriteBarrier((&____methods_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODINFOFUNCTIONINSTANCE_T483F3C5DB3B58053A1233B23940AF856A467DE5C_H
#ifndef SETTERFUNCTIONINSTANCE_T324F381539B5C559E10081DB06D8BD0BBD454B09_H
#define SETTERFUNCTIONINSTANCE_T324F381539B5C559E10081DB06D8BD0BBD454B09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.SetterFunctionInstance
struct  SetterFunctionInstance_t324F381539B5C559E10081DB06D8BD0BBD454B09  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.Action`2<Jint.Native.JsValue,Jint.Native.JsValue> Jint.Runtime.Interop.SetterFunctionInstance::_setter
	Action_2_t06F7602F0814DDCDF44485CB5E571029718A58BC * ____setter_8;

public:
	inline static int32_t get_offset_of__setter_8() { return static_cast<int32_t>(offsetof(SetterFunctionInstance_t324F381539B5C559E10081DB06D8BD0BBD454B09, ____setter_8)); }
	inline Action_2_t06F7602F0814DDCDF44485CB5E571029718A58BC * get__setter_8() const { return ____setter_8; }
	inline Action_2_t06F7602F0814DDCDF44485CB5E571029718A58BC ** get_address_of__setter_8() { return &____setter_8; }
	inline void set__setter_8(Action_2_t06F7602F0814DDCDF44485CB5E571029718A58BC * value)
	{
		____setter_8 = value;
		Il2CppCodeGenWriteBarrier((&____setter_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SETTERFUNCTIONINSTANCE_T324F381539B5C559E10081DB06D8BD0BBD454B09_H
#ifndef TYPEREFERENCE_T5D3C2ED721325BFEDF31379C3E93D40F5AC17892_H
#define TYPEREFERENCE_T5D3C2ED721325BFEDF31379C3E93D40F5AC17892_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Interop.TypeReference
struct  TypeReference_t5D3C2ED721325BFEDF31379C3E93D40F5AC17892  : public FunctionInstance_tE38D99DCDA862B7930D886A656437288185E5458
{
public:
	// System.Type Jint.Runtime.Interop.TypeReference::<Type>k__BackingField
	Type_t * ___U3CTypeU3Ek__BackingField_8;

public:
	inline static int32_t get_offset_of_U3CTypeU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(TypeReference_t5D3C2ED721325BFEDF31379C3E93D40F5AC17892, ___U3CTypeU3Ek__BackingField_8)); }
	inline Type_t * get_U3CTypeU3Ek__BackingField_8() const { return ___U3CTypeU3Ek__BackingField_8; }
	inline Type_t ** get_address_of_U3CTypeU3Ek__BackingField_8() { return &___U3CTypeU3Ek__BackingField_8; }
	inline void set_U3CTypeU3Ek__BackingField_8(Type_t * value)
	{
		___U3CTypeU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CTypeU3Ek__BackingField_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TYPEREFERENCE_T5D3C2ED721325BFEDF31379C3E93D40F5AC17892_H
#ifndef SYNTAXNODE_T7493862A6C53ED276ECECF7246A8DFE2851C880B_H
#define SYNTAXNODE_T7493862A6C53ED276ECECF7246A8DFE2851C880B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SyntaxNode
struct  SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B  : public RuntimeObject
{
public:
	// Jint.Parser.Ast.SyntaxNodes Jint.Parser.Ast.SyntaxNode::Type
	int32_t ___Type_0;
	// System.Int32[] Jint.Parser.Ast.SyntaxNode::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_1;
	// Jint.Parser.Location Jint.Parser.Ast.SyntaxNode::Location
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ___Location_2;

public:
	inline static int32_t get_offset_of_Type_0() { return static_cast<int32_t>(offsetof(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B, ___Type_0)); }
	inline int32_t get_Type_0() const { return ___Type_0; }
	inline int32_t* get_address_of_Type_0() { return &___Type_0; }
	inline void set_Type_0(int32_t value)
	{
		___Type_0 = value;
	}

	inline static int32_t get_offset_of_Range_1() { return static_cast<int32_t>(offsetof(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B, ___Range_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_1() const { return ___Range_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_1() { return &___Range_1; }
	inline void set_Range_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_1 = value;
		Il2CppCodeGenWriteBarrier((&___Range_1), value);
	}

	inline static int32_t get_offset_of_Location_2() { return static_cast<int32_t>(offsetof(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B, ___Location_2)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get_Location_2() const { return ___Location_2; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of_Location_2() { return &___Location_2; }
	inline void set_Location_2(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		___Location_2 = value;
		Il2CppCodeGenWriteBarrier((&___Location_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYNTAXNODE_T7493862A6C53ED276ECECF7246A8DFE2851C880B_H
#ifndef TOKEN_T2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_H
#define TOKEN_T2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Token
struct  Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37  : public RuntimeObject
{
public:
	// Jint.Parser.Tokens Jint.Parser.Token::Type
	int32_t ___Type_1;
	// System.String Jint.Parser.Token::Literal
	String_t* ___Literal_2;
	// System.Object Jint.Parser.Token::Value
	RuntimeObject * ___Value_3;
	// System.Int32[] Jint.Parser.Token::Range
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___Range_4;
	// System.Nullable`1<System.Int32> Jint.Parser.Token::LineNumber
	Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  ___LineNumber_5;
	// System.Int32 Jint.Parser.Token::LineStart
	int32_t ___LineStart_6;
	// System.Boolean Jint.Parser.Token::Octal
	bool ___Octal_7;
	// Jint.Parser.Location Jint.Parser.Token::Location
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * ___Location_8;
	// System.Int32 Jint.Parser.Token::Precedence
	int32_t ___Precedence_9;

public:
	inline static int32_t get_offset_of_Type_1() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Type_1)); }
	inline int32_t get_Type_1() const { return ___Type_1; }
	inline int32_t* get_address_of_Type_1() { return &___Type_1; }
	inline void set_Type_1(int32_t value)
	{
		___Type_1 = value;
	}

	inline static int32_t get_offset_of_Literal_2() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Literal_2)); }
	inline String_t* get_Literal_2() const { return ___Literal_2; }
	inline String_t** get_address_of_Literal_2() { return &___Literal_2; }
	inline void set_Literal_2(String_t* value)
	{
		___Literal_2 = value;
		Il2CppCodeGenWriteBarrier((&___Literal_2), value);
	}

	inline static int32_t get_offset_of_Value_3() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Value_3)); }
	inline RuntimeObject * get_Value_3() const { return ___Value_3; }
	inline RuntimeObject ** get_address_of_Value_3() { return &___Value_3; }
	inline void set_Value_3(RuntimeObject * value)
	{
		___Value_3 = value;
		Il2CppCodeGenWriteBarrier((&___Value_3), value);
	}

	inline static int32_t get_offset_of_Range_4() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Range_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_Range_4() const { return ___Range_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_Range_4() { return &___Range_4; }
	inline void set_Range_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___Range_4 = value;
		Il2CppCodeGenWriteBarrier((&___Range_4), value);
	}

	inline static int32_t get_offset_of_LineNumber_5() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___LineNumber_5)); }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  get_LineNumber_5() const { return ___LineNumber_5; }
	inline Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB * get_address_of_LineNumber_5() { return &___LineNumber_5; }
	inline void set_LineNumber_5(Nullable_1_t0D03270832B3FFDDC0E7C2D89D4A0EA25376A1EB  value)
	{
		___LineNumber_5 = value;
	}

	inline static int32_t get_offset_of_LineStart_6() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___LineStart_6)); }
	inline int32_t get_LineStart_6() const { return ___LineStart_6; }
	inline int32_t* get_address_of_LineStart_6() { return &___LineStart_6; }
	inline void set_LineStart_6(int32_t value)
	{
		___LineStart_6 = value;
	}

	inline static int32_t get_offset_of_Octal_7() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Octal_7)); }
	inline bool get_Octal_7() const { return ___Octal_7; }
	inline bool* get_address_of_Octal_7() { return &___Octal_7; }
	inline void set_Octal_7(bool value)
	{
		___Octal_7 = value;
	}

	inline static int32_t get_offset_of_Location_8() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Location_8)); }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * get_Location_8() const { return ___Location_8; }
	inline Location_t9639547EECD85868A9B1627343BF1C80DE46B783 ** get_address_of_Location_8() { return &___Location_8; }
	inline void set_Location_8(Location_t9639547EECD85868A9B1627343BF1C80DE46B783 * value)
	{
		___Location_8 = value;
		Il2CppCodeGenWriteBarrier((&___Location_8), value);
	}

	inline static int32_t get_offset_of_Precedence_9() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37, ___Precedence_9)); }
	inline int32_t get_Precedence_9() const { return ___Precedence_9; }
	inline int32_t* get_address_of_Precedence_9() { return &___Precedence_9; }
	inline void set_Precedence_9(int32_t value)
	{
		___Precedence_9 = value;
	}
};

struct Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_StaticFields
{
public:
	// Jint.Parser.Token Jint.Parser.Token::Empty
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_StaticFields, ___Empty_0)); }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * get_Empty_0() const { return ___Empty_0; }
	inline Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37 * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_H
#ifndef DEBUGHANDLER_T42446EF263A027A800A0BF5BD64A06674E4A6296_H
#define DEBUGHANDLER_T42446EF263A027A800A0BF5BD64A06674E4A6296_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Debugger.DebugHandler
struct  DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296  : public RuntimeObject
{
public:
	// System.Collections.Generic.Stack`1<System.String> Jint.Runtime.Debugger.DebugHandler::_debugCallStack
	Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * ____debugCallStack_0;
	// Jint.Runtime.Debugger.StepMode Jint.Runtime.Debugger.DebugHandler::_stepMode
	int32_t ____stepMode_1;
	// System.Int32 Jint.Runtime.Debugger.DebugHandler::_callBackStepOverDepth
	int32_t ____callBackStepOverDepth_2;
	// Jint.Engine Jint.Runtime.Debugger.DebugHandler::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_3;

public:
	inline static int32_t get_offset_of__debugCallStack_0() { return static_cast<int32_t>(offsetof(DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296, ____debugCallStack_0)); }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * get__debugCallStack_0() const { return ____debugCallStack_0; }
	inline Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 ** get_address_of__debugCallStack_0() { return &____debugCallStack_0; }
	inline void set__debugCallStack_0(Stack_1_t13ADD3EDE9022020A434D32784D651F9FE027067 * value)
	{
		____debugCallStack_0 = value;
		Il2CppCodeGenWriteBarrier((&____debugCallStack_0), value);
	}

	inline static int32_t get_offset_of__stepMode_1() { return static_cast<int32_t>(offsetof(DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296, ____stepMode_1)); }
	inline int32_t get__stepMode_1() const { return ____stepMode_1; }
	inline int32_t* get_address_of__stepMode_1() { return &____stepMode_1; }
	inline void set__stepMode_1(int32_t value)
	{
		____stepMode_1 = value;
	}

	inline static int32_t get_offset_of__callBackStepOverDepth_2() { return static_cast<int32_t>(offsetof(DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296, ____callBackStepOverDepth_2)); }
	inline int32_t get__callBackStepOverDepth_2() const { return ____callBackStepOverDepth_2; }
	inline int32_t* get_address_of__callBackStepOverDepth_2() { return &____callBackStepOverDepth_2; }
	inline void set__callBackStepOverDepth_2(int32_t value)
	{
		____callBackStepOverDepth_2 = value;
	}

	inline static int32_t get_offset_of__engine_3() { return static_cast<int32_t>(offsetof(DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296, ____engine_3)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_3() const { return ____engine_3; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_3() { return &____engine_3; }
	inline void set__engine_3(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_3 = value;
		Il2CppCodeGenWriteBarrier((&____engine_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGHANDLER_T42446EF263A027A800A0BF5BD64A06674E4A6296_H
#ifndef CLRACCESSDESCRIPTOR_T3D6D255735AD3760BB73CB59079B8289FB0E6AD1_H
#define CLRACCESSDESCRIPTOR_T3D6D255735AD3760BB73CB59079B8289FB0E6AD1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Descriptors.Specialized.ClrAccessDescriptor
struct  ClrAccessDescriptor_t3D6D255735AD3760BB73CB59079B8289FB0E6AD1  : public PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CLRACCESSDESCRIPTOR_T3D6D255735AD3760BB73CB59079B8289FB0E6AD1_H
#ifndef FIELDINFODESCRIPTOR_TE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E_H
#define FIELDINFODESCRIPTOR_TE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Descriptors.Specialized.FieldInfoDescriptor
struct  FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E  : public PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73
{
public:
	// Jint.Engine Jint.Runtime.Descriptors.Specialized.FieldInfoDescriptor::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_7;
	// System.Reflection.FieldInfo Jint.Runtime.Descriptors.Specialized.FieldInfoDescriptor::_fieldInfo
	FieldInfo_t * ____fieldInfo_8;
	// System.Object Jint.Runtime.Descriptors.Specialized.FieldInfoDescriptor::_item
	RuntimeObject * ____item_9;

public:
	inline static int32_t get_offset_of__engine_7() { return static_cast<int32_t>(offsetof(FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E, ____engine_7)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_7() const { return ____engine_7; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_7() { return &____engine_7; }
	inline void set__engine_7(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_7 = value;
		Il2CppCodeGenWriteBarrier((&____engine_7), value);
	}

	inline static int32_t get_offset_of__fieldInfo_8() { return static_cast<int32_t>(offsetof(FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E, ____fieldInfo_8)); }
	inline FieldInfo_t * get__fieldInfo_8() const { return ____fieldInfo_8; }
	inline FieldInfo_t ** get_address_of__fieldInfo_8() { return &____fieldInfo_8; }
	inline void set__fieldInfo_8(FieldInfo_t * value)
	{
		____fieldInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&____fieldInfo_8), value);
	}

	inline static int32_t get_offset_of__item_9() { return static_cast<int32_t>(offsetof(FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E, ____item_9)); }
	inline RuntimeObject * get__item_9() const { return ____item_9; }
	inline RuntimeObject ** get_address_of__item_9() { return &____item_9; }
	inline void set__item_9(RuntimeObject * value)
	{
		____item_9 = value;
		Il2CppCodeGenWriteBarrier((&____item_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FIELDINFODESCRIPTOR_TE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E_H
#ifndef INDEXDESCRIPTOR_T8BA0E96659D6D4601AFD470C61271904598C5A99_H
#define INDEXDESCRIPTOR_T8BA0E96659D6D4601AFD470C61271904598C5A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Descriptors.Specialized.IndexDescriptor
struct  IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99  : public PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73
{
public:
	// Jint.Engine Jint.Runtime.Descriptors.Specialized.IndexDescriptor::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_7;
	// System.Object Jint.Runtime.Descriptors.Specialized.IndexDescriptor::_key
	RuntimeObject * ____key_8;
	// System.Object Jint.Runtime.Descriptors.Specialized.IndexDescriptor::_item
	RuntimeObject * ____item_9;
	// System.Reflection.PropertyInfo Jint.Runtime.Descriptors.Specialized.IndexDescriptor::_indexer
	PropertyInfo_t * ____indexer_10;
	// System.Reflection.MethodInfo Jint.Runtime.Descriptors.Specialized.IndexDescriptor::_containsKey
	MethodInfo_t * ____containsKey_11;

public:
	inline static int32_t get_offset_of__engine_7() { return static_cast<int32_t>(offsetof(IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99, ____engine_7)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_7() const { return ____engine_7; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_7() { return &____engine_7; }
	inline void set__engine_7(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_7 = value;
		Il2CppCodeGenWriteBarrier((&____engine_7), value);
	}

	inline static int32_t get_offset_of__key_8() { return static_cast<int32_t>(offsetof(IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99, ____key_8)); }
	inline RuntimeObject * get__key_8() const { return ____key_8; }
	inline RuntimeObject ** get_address_of__key_8() { return &____key_8; }
	inline void set__key_8(RuntimeObject * value)
	{
		____key_8 = value;
		Il2CppCodeGenWriteBarrier((&____key_8), value);
	}

	inline static int32_t get_offset_of__item_9() { return static_cast<int32_t>(offsetof(IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99, ____item_9)); }
	inline RuntimeObject * get__item_9() const { return ____item_9; }
	inline RuntimeObject ** get_address_of__item_9() { return &____item_9; }
	inline void set__item_9(RuntimeObject * value)
	{
		____item_9 = value;
		Il2CppCodeGenWriteBarrier((&____item_9), value);
	}

	inline static int32_t get_offset_of__indexer_10() { return static_cast<int32_t>(offsetof(IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99, ____indexer_10)); }
	inline PropertyInfo_t * get__indexer_10() const { return ____indexer_10; }
	inline PropertyInfo_t ** get_address_of__indexer_10() { return &____indexer_10; }
	inline void set__indexer_10(PropertyInfo_t * value)
	{
		____indexer_10 = value;
		Il2CppCodeGenWriteBarrier((&____indexer_10), value);
	}

	inline static int32_t get_offset_of__containsKey_11() { return static_cast<int32_t>(offsetof(IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99, ____containsKey_11)); }
	inline MethodInfo_t * get__containsKey_11() const { return ____containsKey_11; }
	inline MethodInfo_t ** get_address_of__containsKey_11() { return &____containsKey_11; }
	inline void set__containsKey_11(MethodInfo_t * value)
	{
		____containsKey_11 = value;
		Il2CppCodeGenWriteBarrier((&____containsKey_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INDEXDESCRIPTOR_T8BA0E96659D6D4601AFD470C61271904598C5A99_H
#ifndef PROPERTYINFODESCRIPTOR_T568A54E6928FF78BB2DF8A84F508069A2FE90EBB_H
#define PROPERTYINFODESCRIPTOR_T568A54E6928FF78BB2DF8A84F508069A2FE90EBB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Runtime.Descriptors.Specialized.PropertyInfoDescriptor
struct  PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB  : public PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73
{
public:
	// Jint.Engine Jint.Runtime.Descriptors.Specialized.PropertyInfoDescriptor::_engine
	Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * ____engine_7;
	// System.Reflection.PropertyInfo Jint.Runtime.Descriptors.Specialized.PropertyInfoDescriptor::_propertyInfo
	PropertyInfo_t * ____propertyInfo_8;
	// System.Object Jint.Runtime.Descriptors.Specialized.PropertyInfoDescriptor::_item
	RuntimeObject * ____item_9;

public:
	inline static int32_t get_offset_of__engine_7() { return static_cast<int32_t>(offsetof(PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB, ____engine_7)); }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * get__engine_7() const { return ____engine_7; }
	inline Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D ** get_address_of__engine_7() { return &____engine_7; }
	inline void set__engine_7(Engine_t8151E2FED9079EEBC1053A7D9A6C9F54AE417A8D * value)
	{
		____engine_7 = value;
		Il2CppCodeGenWriteBarrier((&____engine_7), value);
	}

	inline static int32_t get_offset_of__propertyInfo_8() { return static_cast<int32_t>(offsetof(PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB, ____propertyInfo_8)); }
	inline PropertyInfo_t * get__propertyInfo_8() const { return ____propertyInfo_8; }
	inline PropertyInfo_t ** get_address_of__propertyInfo_8() { return &____propertyInfo_8; }
	inline void set__propertyInfo_8(PropertyInfo_t * value)
	{
		____propertyInfo_8 = value;
		Il2CppCodeGenWriteBarrier((&____propertyInfo_8), value);
	}

	inline static int32_t get_offset_of__item_9() { return static_cast<int32_t>(offsetof(PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB, ____item_9)); }
	inline RuntimeObject * get__item_9() const { return ____item_9; }
	inline RuntimeObject ** get_address_of__item_9() { return &____item_9; }
	inline void set__item_9(RuntimeObject * value)
	{
		____item_9 = value;
		Il2CppCodeGenWriteBarrier((&____item_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTYINFODESCRIPTOR_T568A54E6928FF78BB2DF8A84F508069A2FE90EBB_H
#ifndef EXPRESSION_TAC599936AD6D021EA6A3A695C803975140919ADB_H
#define EXPRESSION_TAC599936AD6D021EA6A3A695C803975140919ADB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Expression
struct  Expression_tAC599936AD6D021EA6A3A695C803975140919ADB  : public SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSION_TAC599936AD6D021EA6A3A695C803975140919ADB_H
#ifndef STATEMENT_TDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0_H
#define STATEMENT_TDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Statement
struct  Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0  : public SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B
{
public:
	// System.String Jint.Parser.Ast.Statement::LabelSet
	String_t* ___LabelSet_3;

public:
	inline static int32_t get_offset_of_LabelSet_3() { return static_cast<int32_t>(offsetof(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0, ___LabelSet_3)); }
	inline String_t* get_LabelSet_3() const { return ___LabelSet_3; }
	inline String_t** get_address_of_LabelSet_3() { return &___LabelSet_3; }
	inline void set_LabelSet_3(String_t* value)
	{
		___LabelSet_3 = value;
		Il2CppCodeGenWriteBarrier((&___LabelSet_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEMENT_TDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0_H
#ifndef SWITCHCASE_T88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449_H
#define SWITCHCASE_T88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SwitchCase
struct  SwitchCase_t88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449  : public SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.SwitchCase::Test
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Test_3;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Statement> Jint.Parser.Ast.SwitchCase::Consequent
	RuntimeObject* ___Consequent_4;

public:
	inline static int32_t get_offset_of_Test_3() { return static_cast<int32_t>(offsetof(SwitchCase_t88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449, ___Test_3)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Test_3() const { return ___Test_3; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Test_3() { return &___Test_3; }
	inline void set_Test_3(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Test_3 = value;
		Il2CppCodeGenWriteBarrier((&___Test_3), value);
	}

	inline static int32_t get_offset_of_Consequent_4() { return static_cast<int32_t>(offsetof(SwitchCase_t88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449, ___Consequent_4)); }
	inline RuntimeObject* get_Consequent_4() const { return ___Consequent_4; }
	inline RuntimeObject** get_address_of_Consequent_4() { return &___Consequent_4; }
	inline void set_Consequent_4(RuntimeObject* value)
	{
		___Consequent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Consequent_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHCASE_T88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449_H
#ifndef ARRAYEXPRESSION_T46B16E6EFAB06AC476919129BCE7A2440B8B4040_H
#define ARRAYEXPRESSION_T46B16E6EFAB06AC476919129BCE7A2440B8B4040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ArrayExpression
struct  ArrayExpression_t46B16E6EFAB06AC476919129BCE7A2440B8B4040  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Expression> Jint.Parser.Ast.ArrayExpression::Elements
	RuntimeObject* ___Elements_3;

public:
	inline static int32_t get_offset_of_Elements_3() { return static_cast<int32_t>(offsetof(ArrayExpression_t46B16E6EFAB06AC476919129BCE7A2440B8B4040, ___Elements_3)); }
	inline RuntimeObject* get_Elements_3() const { return ___Elements_3; }
	inline RuntimeObject** get_address_of_Elements_3() { return &___Elements_3; }
	inline void set_Elements_3(RuntimeObject* value)
	{
		___Elements_3 = value;
		Il2CppCodeGenWriteBarrier((&___Elements_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ARRAYEXPRESSION_T46B16E6EFAB06AC476919129BCE7A2440B8B4040_H
#ifndef ASSIGNMENTEXPRESSION_T00F2EE5EDE8771D96BE81D39257823B4140EE55E_H
#define ASSIGNMENTEXPRESSION_T00F2EE5EDE8771D96BE81D39257823B4140EE55E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.AssignmentExpression
struct  AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.AssignmentOperator Jint.Parser.Ast.AssignmentExpression::Operator
	int32_t ___Operator_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.AssignmentExpression::Left
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Left_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.AssignmentExpression::Right
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Right_5;

public:
	inline static int32_t get_offset_of_Operator_3() { return static_cast<int32_t>(offsetof(AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E, ___Operator_3)); }
	inline int32_t get_Operator_3() const { return ___Operator_3; }
	inline int32_t* get_address_of_Operator_3() { return &___Operator_3; }
	inline void set_Operator_3(int32_t value)
	{
		___Operator_3 = value;
	}

	inline static int32_t get_offset_of_Left_4() { return static_cast<int32_t>(offsetof(AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E, ___Left_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Left_4() const { return ___Left_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Left_4() { return &___Left_4; }
	inline void set_Left_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Left_4 = value;
		Il2CppCodeGenWriteBarrier((&___Left_4), value);
	}

	inline static int32_t get_offset_of_Right_5() { return static_cast<int32_t>(offsetof(AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E, ___Right_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Right_5() const { return ___Right_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Right_5() { return &___Right_5; }
	inline void set_Right_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Right_5 = value;
		Il2CppCodeGenWriteBarrier((&___Right_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ASSIGNMENTEXPRESSION_T00F2EE5EDE8771D96BE81D39257823B4140EE55E_H
#ifndef BINARYEXPRESSION_T45F2093E7956D81E1A63CB502D06A8E4E910E411_H
#define BINARYEXPRESSION_T45F2093E7956D81E1A63CB502D06A8E4E910E411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.BinaryExpression
struct  BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.BinaryOperator Jint.Parser.Ast.BinaryExpression::Operator
	int32_t ___Operator_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.BinaryExpression::Left
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Left_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.BinaryExpression::Right
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Right_5;

public:
	inline static int32_t get_offset_of_Operator_3() { return static_cast<int32_t>(offsetof(BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411, ___Operator_3)); }
	inline int32_t get_Operator_3() const { return ___Operator_3; }
	inline int32_t* get_address_of_Operator_3() { return &___Operator_3; }
	inline void set_Operator_3(int32_t value)
	{
		___Operator_3 = value;
	}

	inline static int32_t get_offset_of_Left_4() { return static_cast<int32_t>(offsetof(BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411, ___Left_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Left_4() const { return ___Left_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Left_4() { return &___Left_4; }
	inline void set_Left_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Left_4 = value;
		Il2CppCodeGenWriteBarrier((&___Left_4), value);
	}

	inline static int32_t get_offset_of_Right_5() { return static_cast<int32_t>(offsetof(BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411, ___Right_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Right_5() const { return ___Right_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Right_5() { return &___Right_5; }
	inline void set_Right_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Right_5 = value;
		Il2CppCodeGenWriteBarrier((&___Right_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYEXPRESSION_T45F2093E7956D81E1A63CB502D06A8E4E910E411_H
#ifndef BLOCKSTATEMENT_T8441FA49BCE7A845BB4B30AA28D0980133EA216E_H
#define BLOCKSTATEMENT_T8441FA49BCE7A845BB4B30AA28D0980133EA216E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.BlockStatement
struct  BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Statement> Jint.Parser.Ast.BlockStatement::Body
	RuntimeObject* ___Body_4;

public:
	inline static int32_t get_offset_of_Body_4() { return static_cast<int32_t>(offsetof(BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E, ___Body_4)); }
	inline RuntimeObject* get_Body_4() const { return ___Body_4; }
	inline RuntimeObject** get_address_of_Body_4() { return &___Body_4; }
	inline void set_Body_4(RuntimeObject* value)
	{
		___Body_4 = value;
		Il2CppCodeGenWriteBarrier((&___Body_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BLOCKSTATEMENT_T8441FA49BCE7A845BB4B30AA28D0980133EA216E_H
#ifndef BREAKSTATEMENT_T640A809473C0484A423069D6DB0BA1D4A4A2B5DC_H
#define BREAKSTATEMENT_T640A809473C0484A423069D6DB0BA1D4A4A2B5DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.BreakStatement
struct  BreakStatement_t640A809473C0484A423069D6DB0BA1D4A4A2B5DC  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.BreakStatement::Label
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___Label_4;

public:
	inline static int32_t get_offset_of_Label_4() { return static_cast<int32_t>(offsetof(BreakStatement_t640A809473C0484A423069D6DB0BA1D4A4A2B5DC, ___Label_4)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_Label_4() const { return ___Label_4; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_Label_4() { return &___Label_4; }
	inline void set_Label_4(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___Label_4 = value;
		Il2CppCodeGenWriteBarrier((&___Label_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BREAKSTATEMENT_T640A809473C0484A423069D6DB0BA1D4A4A2B5DC_H
#ifndef CALLEXPRESSION_T81E1C5DDDF4A0D866F4CA130E9C6142641B072BB_H
#define CALLEXPRESSION_T81E1C5DDDF4A0D866F4CA130E9C6142641B072BB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.CallExpression
struct  CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.CallExpression::Callee
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Callee_3;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.Expression> Jint.Parser.Ast.CallExpression::Arguments
	RuntimeObject* ___Arguments_4;
	// System.Boolean Jint.Parser.Ast.CallExpression::Cached
	bool ___Cached_5;
	// System.Boolean Jint.Parser.Ast.CallExpression::CanBeCached
	bool ___CanBeCached_6;
	// Jint.Native.JsValue[] Jint.Parser.Ast.CallExpression::CachedArguments
	JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* ___CachedArguments_7;

public:
	inline static int32_t get_offset_of_Callee_3() { return static_cast<int32_t>(offsetof(CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB, ___Callee_3)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Callee_3() const { return ___Callee_3; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Callee_3() { return &___Callee_3; }
	inline void set_Callee_3(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Callee_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callee_3), value);
	}

	inline static int32_t get_offset_of_Arguments_4() { return static_cast<int32_t>(offsetof(CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB, ___Arguments_4)); }
	inline RuntimeObject* get_Arguments_4() const { return ___Arguments_4; }
	inline RuntimeObject** get_address_of_Arguments_4() { return &___Arguments_4; }
	inline void set_Arguments_4(RuntimeObject* value)
	{
		___Arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___Arguments_4), value);
	}

	inline static int32_t get_offset_of_Cached_5() { return static_cast<int32_t>(offsetof(CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB, ___Cached_5)); }
	inline bool get_Cached_5() const { return ___Cached_5; }
	inline bool* get_address_of_Cached_5() { return &___Cached_5; }
	inline void set_Cached_5(bool value)
	{
		___Cached_5 = value;
	}

	inline static int32_t get_offset_of_CanBeCached_6() { return static_cast<int32_t>(offsetof(CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB, ___CanBeCached_6)); }
	inline bool get_CanBeCached_6() const { return ___CanBeCached_6; }
	inline bool* get_address_of_CanBeCached_6() { return &___CanBeCached_6; }
	inline void set_CanBeCached_6(bool value)
	{
		___CanBeCached_6 = value;
	}

	inline static int32_t get_offset_of_CachedArguments_7() { return static_cast<int32_t>(offsetof(CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB, ___CachedArguments_7)); }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* get_CachedArguments_7() const { return ___CachedArguments_7; }
	inline JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88** get_address_of_CachedArguments_7() { return &___CachedArguments_7; }
	inline void set_CachedArguments_7(JsValueU5BU5D_tF3731D18ADF663E5DEC6DA74176A0A8A2FB2EE88* value)
	{
		___CachedArguments_7 = value;
		Il2CppCodeGenWriteBarrier((&___CachedArguments_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CALLEXPRESSION_T81E1C5DDDF4A0D866F4CA130E9C6142641B072BB_H
#ifndef CATCHCLAUSE_T80ADAE100F384254ED8138A2DE08AA18DCA38CDD_H
#define CATCHCLAUSE_T80ADAE100F384254ED8138A2DE08AA18DCA38CDD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.CatchClause
struct  CatchClause_t80ADAE100F384254ED8138A2DE08AA18DCA38CDD  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.CatchClause::Param
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___Param_4;
	// Jint.Parser.Ast.BlockStatement Jint.Parser.Ast.CatchClause::Body
	BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E * ___Body_5;

public:
	inline static int32_t get_offset_of_Param_4() { return static_cast<int32_t>(offsetof(CatchClause_t80ADAE100F384254ED8138A2DE08AA18DCA38CDD, ___Param_4)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_Param_4() const { return ___Param_4; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_Param_4() { return &___Param_4; }
	inline void set_Param_4(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___Param_4 = value;
		Il2CppCodeGenWriteBarrier((&___Param_4), value);
	}

	inline static int32_t get_offset_of_Body_5() { return static_cast<int32_t>(offsetof(CatchClause_t80ADAE100F384254ED8138A2DE08AA18DCA38CDD, ___Body_5)); }
	inline BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E * get_Body_5() const { return ___Body_5; }
	inline BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E ** get_address_of_Body_5() { return &___Body_5; }
	inline void set_Body_5(BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E * value)
	{
		___Body_5 = value;
		Il2CppCodeGenWriteBarrier((&___Body_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CATCHCLAUSE_T80ADAE100F384254ED8138A2DE08AA18DCA38CDD_H
#ifndef CONDITIONALEXPRESSION_T52703455D8FF8C760F672D9A508C6873E456E046_H
#define CONDITIONALEXPRESSION_T52703455D8FF8C760F672D9A508C6873E456E046_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ConditionalExpression
struct  ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ConditionalExpression::Test
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Test_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ConditionalExpression::Consequent
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Consequent_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ConditionalExpression::Alternate
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Alternate_5;

public:
	inline static int32_t get_offset_of_Test_3() { return static_cast<int32_t>(offsetof(ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046, ___Test_3)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Test_3() const { return ___Test_3; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Test_3() { return &___Test_3; }
	inline void set_Test_3(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Test_3 = value;
		Il2CppCodeGenWriteBarrier((&___Test_3), value);
	}

	inline static int32_t get_offset_of_Consequent_4() { return static_cast<int32_t>(offsetof(ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046, ___Consequent_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Consequent_4() const { return ___Consequent_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Consequent_4() { return &___Consequent_4; }
	inline void set_Consequent_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Consequent_4 = value;
		Il2CppCodeGenWriteBarrier((&___Consequent_4), value);
	}

	inline static int32_t get_offset_of_Alternate_5() { return static_cast<int32_t>(offsetof(ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046, ___Alternate_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Alternate_5() const { return ___Alternate_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Alternate_5() { return &___Alternate_5; }
	inline void set_Alternate_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Alternate_5 = value;
		Il2CppCodeGenWriteBarrier((&___Alternate_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONDITIONALEXPRESSION_T52703455D8FF8C760F672D9A508C6873E456E046_H
#ifndef CONTINUESTATEMENT_T7BCF4378D59BCF5DE09CC365E697E4D7D702905C_H
#define CONTINUESTATEMENT_T7BCF4378D59BCF5DE09CC365E697E4D7D702905C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ContinueStatement
struct  ContinueStatement_t7BCF4378D59BCF5DE09CC365E697E4D7D702905C  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.ContinueStatement::Label
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___Label_4;

public:
	inline static int32_t get_offset_of_Label_4() { return static_cast<int32_t>(offsetof(ContinueStatement_t7BCF4378D59BCF5DE09CC365E697E4D7D702905C, ___Label_4)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_Label_4() const { return ___Label_4; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_Label_4() { return &___Label_4; }
	inline void set_Label_4(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___Label_4 = value;
		Il2CppCodeGenWriteBarrier((&___Label_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CONTINUESTATEMENT_T7BCF4378D59BCF5DE09CC365E697E4D7D702905C_H
#ifndef DEBUGGERSTATEMENT_T25A052428FF0DD88B55C026CEB9B34E895C1CB28_H
#define DEBUGGERSTATEMENT_T25A052428FF0DD88B55C026CEB9B34E895C1CB28_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.DebuggerStatement
struct  DebuggerStatement_t25A052428FF0DD88B55C026CEB9B34E895C1CB28  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DEBUGGERSTATEMENT_T25A052428FF0DD88B55C026CEB9B34E895C1CB28_H
#ifndef DOWHILESTATEMENT_T5F178F4A9B61D35DEDA8C705B2576DA6166E818C_H
#define DOWHILESTATEMENT_T5F178F4A9B61D35DEDA8C705B2576DA6166E818C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.DoWhileStatement
struct  DoWhileStatement_t5F178F4A9B61D35DEDA8C705B2576DA6166E818C  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.DoWhileStatement::Body
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Body_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.DoWhileStatement::Test
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Test_5;

public:
	inline static int32_t get_offset_of_Body_4() { return static_cast<int32_t>(offsetof(DoWhileStatement_t5F178F4A9B61D35DEDA8C705B2576DA6166E818C, ___Body_4)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Body_4() const { return ___Body_4; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Body_4() { return &___Body_4; }
	inline void set_Body_4(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Body_4 = value;
		Il2CppCodeGenWriteBarrier((&___Body_4), value);
	}

	inline static int32_t get_offset_of_Test_5() { return static_cast<int32_t>(offsetof(DoWhileStatement_t5F178F4A9B61D35DEDA8C705B2576DA6166E818C, ___Test_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Test_5() const { return ___Test_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Test_5() { return &___Test_5; }
	inline void set_Test_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Test_5 = value;
		Il2CppCodeGenWriteBarrier((&___Test_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DOWHILESTATEMENT_T5F178F4A9B61D35DEDA8C705B2576DA6166E818C_H
#ifndef EMPTYSTATEMENT_TA94A7F5D1B44C4697B622066D3457B293E2B9A48_H
#define EMPTYSTATEMENT_TA94A7F5D1B44C4697B622066D3457B293E2B9A48_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.EmptyStatement
struct  EmptyStatement_tA94A7F5D1B44C4697B622066D3457B293E2B9A48  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYSTATEMENT_TA94A7F5D1B44C4697B622066D3457B293E2B9A48_H
#ifndef EXPRESSIONSTATEMENT_T1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC_H
#define EXPRESSIONSTATEMENT_T1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ExpressionStatement
struct  ExpressionStatement_t1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ExpressionStatement::Expression
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Expression_4;

public:
	inline static int32_t get_offset_of_Expression_4() { return static_cast<int32_t>(offsetof(ExpressionStatement_t1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC, ___Expression_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Expression_4() const { return ___Expression_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Expression_4() { return &___Expression_4; }
	inline void set_Expression_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Expression_4 = value;
		Il2CppCodeGenWriteBarrier((&___Expression_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EXPRESSIONSTATEMENT_T1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC_H
#ifndef FORINSTATEMENT_TBECC7E626B3C0995947F27F07CC6843114400AAB_H
#define FORINSTATEMENT_TBECC7E626B3C0995947F27F07CC6843114400AAB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ForInStatement
struct  ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.SyntaxNode Jint.Parser.Ast.ForInStatement::Left
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * ___Left_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ForInStatement::Right
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Right_5;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.ForInStatement::Body
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Body_6;
	// System.Boolean Jint.Parser.Ast.ForInStatement::Each
	bool ___Each_7;

public:
	inline static int32_t get_offset_of_Left_4() { return static_cast<int32_t>(offsetof(ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB, ___Left_4)); }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * get_Left_4() const { return ___Left_4; }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B ** get_address_of_Left_4() { return &___Left_4; }
	inline void set_Left_4(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * value)
	{
		___Left_4 = value;
		Il2CppCodeGenWriteBarrier((&___Left_4), value);
	}

	inline static int32_t get_offset_of_Right_5() { return static_cast<int32_t>(offsetof(ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB, ___Right_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Right_5() const { return ___Right_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Right_5() { return &___Right_5; }
	inline void set_Right_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Right_5 = value;
		Il2CppCodeGenWriteBarrier((&___Right_5), value);
	}

	inline static int32_t get_offset_of_Body_6() { return static_cast<int32_t>(offsetof(ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB, ___Body_6)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Body_6() const { return ___Body_6; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Body_6() { return &___Body_6; }
	inline void set_Body_6(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Body_6 = value;
		Il2CppCodeGenWriteBarrier((&___Body_6), value);
	}

	inline static int32_t get_offset_of_Each_7() { return static_cast<int32_t>(offsetof(ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB, ___Each_7)); }
	inline bool get_Each_7() const { return ___Each_7; }
	inline bool* get_address_of_Each_7() { return &___Each_7; }
	inline void set_Each_7(bool value)
	{
		___Each_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORINSTATEMENT_TBECC7E626B3C0995947F27F07CC6843114400AAB_H
#ifndef FORSTATEMENT_T6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1_H
#define FORSTATEMENT_T6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ForStatement
struct  ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.SyntaxNode Jint.Parser.Ast.ForStatement::Init
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * ___Init_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ForStatement::Test
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Test_5;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ForStatement::Update
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Update_6;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.ForStatement::Body
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Body_7;

public:
	inline static int32_t get_offset_of_Init_4() { return static_cast<int32_t>(offsetof(ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1, ___Init_4)); }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * get_Init_4() const { return ___Init_4; }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B ** get_address_of_Init_4() { return &___Init_4; }
	inline void set_Init_4(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * value)
	{
		___Init_4 = value;
		Il2CppCodeGenWriteBarrier((&___Init_4), value);
	}

	inline static int32_t get_offset_of_Test_5() { return static_cast<int32_t>(offsetof(ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1, ___Test_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Test_5() const { return ___Test_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Test_5() { return &___Test_5; }
	inline void set_Test_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Test_5 = value;
		Il2CppCodeGenWriteBarrier((&___Test_5), value);
	}

	inline static int32_t get_offset_of_Update_6() { return static_cast<int32_t>(offsetof(ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1, ___Update_6)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Update_6() const { return ___Update_6; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Update_6() { return &___Update_6; }
	inline void set_Update_6(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Update_6 = value;
		Il2CppCodeGenWriteBarrier((&___Update_6), value);
	}

	inline static int32_t get_offset_of_Body_7() { return static_cast<int32_t>(offsetof(ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1, ___Body_7)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Body_7() const { return ___Body_7; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Body_7() { return &___Body_7; }
	inline void set_Body_7(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Body_7 = value;
		Il2CppCodeGenWriteBarrier((&___Body_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FORSTATEMENT_T6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1_H
#ifndef FUNCTIONDECLARATION_TD82FBCBF055374B5A8051C961098576C24786C7D_H
#define FUNCTIONDECLARATION_TD82FBCBF055374B5A8051C961098576C24786C7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.FunctionDeclaration
struct  FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.FunctionDeclaration::<Id>k__BackingField
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___U3CIdU3Ek__BackingField_4;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Identifier> Jint.Parser.Ast.FunctionDeclaration::<Parameters>k__BackingField
	RuntimeObject* ___U3CParametersU3Ek__BackingField_5;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.FunctionDeclaration::<Body>k__BackingField
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___U3CBodyU3Ek__BackingField_6;
	// System.Boolean Jint.Parser.Ast.FunctionDeclaration::<Strict>k__BackingField
	bool ___U3CStrictU3Ek__BackingField_7;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.VariableDeclaration> Jint.Parser.Ast.FunctionDeclaration::<VariableDeclarations>k__BackingField
	RuntimeObject* ___U3CVariableDeclarationsU3Ek__BackingField_8;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Expression> Jint.Parser.Ast.FunctionDeclaration::Defaults
	RuntimeObject* ___Defaults_9;
	// Jint.Parser.Ast.SyntaxNode Jint.Parser.Ast.FunctionDeclaration::Rest
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * ___Rest_10;
	// System.Boolean Jint.Parser.Ast.FunctionDeclaration::Generator
	bool ___Generator_11;
	// System.Boolean Jint.Parser.Ast.FunctionDeclaration::Expression
	bool ___Expression_12;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.FunctionDeclaration> Jint.Parser.Ast.FunctionDeclaration::<FunctionDeclarations>k__BackingField
	RuntimeObject* ___U3CFunctionDeclarationsU3Ek__BackingField_13;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___U3CIdU3Ek__BackingField_4)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_U3CIdU3Ek__BackingField_4() const { return ___U3CIdU3Ek__BackingField_4; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_U3CIdU3Ek__BackingField_4() { return &___U3CIdU3Ek__BackingField_4; }
	inline void set_U3CIdU3Ek__BackingField_4(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___U3CIdU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___U3CParametersU3Ek__BackingField_5)); }
	inline RuntimeObject* get_U3CParametersU3Ek__BackingField_5() const { return ___U3CParametersU3Ek__BackingField_5; }
	inline RuntimeObject** get_address_of_U3CParametersU3Ek__BackingField_5() { return &___U3CParametersU3Ek__BackingField_5; }
	inline void set_U3CParametersU3Ek__BackingField_5(RuntimeObject* value)
	{
		___U3CParametersU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CBodyU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___U3CBodyU3Ek__BackingField_6)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_U3CBodyU3Ek__BackingField_6() const { return ___U3CBodyU3Ek__BackingField_6; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_U3CBodyU3Ek__BackingField_6() { return &___U3CBodyU3Ek__BackingField_6; }
	inline void set_U3CBodyU3Ek__BackingField_6(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___U3CBodyU3Ek__BackingField_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBodyU3Ek__BackingField_6), value);
	}

	inline static int32_t get_offset_of_U3CStrictU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___U3CStrictU3Ek__BackingField_7)); }
	inline bool get_U3CStrictU3Ek__BackingField_7() const { return ___U3CStrictU3Ek__BackingField_7; }
	inline bool* get_address_of_U3CStrictU3Ek__BackingField_7() { return &___U3CStrictU3Ek__BackingField_7; }
	inline void set_U3CStrictU3Ek__BackingField_7(bool value)
	{
		___U3CStrictU3Ek__BackingField_7 = value;
	}

	inline static int32_t get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___U3CVariableDeclarationsU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CVariableDeclarationsU3Ek__BackingField_8() const { return ___U3CVariableDeclarationsU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CVariableDeclarationsU3Ek__BackingField_8() { return &___U3CVariableDeclarationsU3Ek__BackingField_8; }
	inline void set_U3CVariableDeclarationsU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CVariableDeclarationsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableDeclarationsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_Defaults_9() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___Defaults_9)); }
	inline RuntimeObject* get_Defaults_9() const { return ___Defaults_9; }
	inline RuntimeObject** get_address_of_Defaults_9() { return &___Defaults_9; }
	inline void set_Defaults_9(RuntimeObject* value)
	{
		___Defaults_9 = value;
		Il2CppCodeGenWriteBarrier((&___Defaults_9), value);
	}

	inline static int32_t get_offset_of_Rest_10() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___Rest_10)); }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * get_Rest_10() const { return ___Rest_10; }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B ** get_address_of_Rest_10() { return &___Rest_10; }
	inline void set_Rest_10(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * value)
	{
		___Rest_10 = value;
		Il2CppCodeGenWriteBarrier((&___Rest_10), value);
	}

	inline static int32_t get_offset_of_Generator_11() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___Generator_11)); }
	inline bool get_Generator_11() const { return ___Generator_11; }
	inline bool* get_address_of_Generator_11() { return &___Generator_11; }
	inline void set_Generator_11(bool value)
	{
		___Generator_11 = value;
	}

	inline static int32_t get_offset_of_Expression_12() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___Expression_12)); }
	inline bool get_Expression_12() const { return ___Expression_12; }
	inline bool* get_address_of_Expression_12() { return &___Expression_12; }
	inline void set_Expression_12(bool value)
	{
		___Expression_12 = value;
	}

	inline static int32_t get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_13() { return static_cast<int32_t>(offsetof(FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D, ___U3CFunctionDeclarationsU3Ek__BackingField_13)); }
	inline RuntimeObject* get_U3CFunctionDeclarationsU3Ek__BackingField_13() const { return ___U3CFunctionDeclarationsU3Ek__BackingField_13; }
	inline RuntimeObject** get_address_of_U3CFunctionDeclarationsU3Ek__BackingField_13() { return &___U3CFunctionDeclarationsU3Ek__BackingField_13; }
	inline void set_U3CFunctionDeclarationsU3Ek__BackingField_13(RuntimeObject* value)
	{
		___U3CFunctionDeclarationsU3Ek__BackingField_13 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionDeclarationsU3Ek__BackingField_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONDECLARATION_TD82FBCBF055374B5A8051C961098576C24786C7D_H
#ifndef FUNCTIONEXPRESSION_TD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A_H
#define FUNCTIONEXPRESSION_TD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.FunctionExpression
struct  FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.FunctionExpression::<Id>k__BackingField
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___U3CIdU3Ek__BackingField_3;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Identifier> Jint.Parser.Ast.FunctionExpression::<Parameters>k__BackingField
	RuntimeObject* ___U3CParametersU3Ek__BackingField_4;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.FunctionExpression::<Body>k__BackingField
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___U3CBodyU3Ek__BackingField_5;
	// System.Boolean Jint.Parser.Ast.FunctionExpression::<Strict>k__BackingField
	bool ___U3CStrictU3Ek__BackingField_6;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.VariableDeclaration> Jint.Parser.Ast.FunctionExpression::<VariableDeclarations>k__BackingField
	RuntimeObject* ___U3CVariableDeclarationsU3Ek__BackingField_7;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.FunctionDeclaration> Jint.Parser.Ast.FunctionExpression::<FunctionDeclarations>k__BackingField
	RuntimeObject* ___U3CFunctionDeclarationsU3Ek__BackingField_8;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Expression> Jint.Parser.Ast.FunctionExpression::Defaults
	RuntimeObject* ___Defaults_9;
	// Jint.Parser.Ast.SyntaxNode Jint.Parser.Ast.FunctionExpression::Rest
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * ___Rest_10;
	// System.Boolean Jint.Parser.Ast.FunctionExpression::Generator
	bool ___Generator_11;
	// System.Boolean Jint.Parser.Ast.FunctionExpression::Expression
	bool ___Expression_12;

public:
	inline static int32_t get_offset_of_U3CIdU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___U3CIdU3Ek__BackingField_3)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_U3CIdU3Ek__BackingField_3() const { return ___U3CIdU3Ek__BackingField_3; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_U3CIdU3Ek__BackingField_3() { return &___U3CIdU3Ek__BackingField_3; }
	inline void set_U3CIdU3Ek__BackingField_3(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___U3CIdU3Ek__BackingField_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CIdU3Ek__BackingField_3), value);
	}

	inline static int32_t get_offset_of_U3CParametersU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___U3CParametersU3Ek__BackingField_4)); }
	inline RuntimeObject* get_U3CParametersU3Ek__BackingField_4() const { return ___U3CParametersU3Ek__BackingField_4; }
	inline RuntimeObject** get_address_of_U3CParametersU3Ek__BackingField_4() { return &___U3CParametersU3Ek__BackingField_4; }
	inline void set_U3CParametersU3Ek__BackingField_4(RuntimeObject* value)
	{
		___U3CParametersU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CParametersU3Ek__BackingField_4), value);
	}

	inline static int32_t get_offset_of_U3CBodyU3Ek__BackingField_5() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___U3CBodyU3Ek__BackingField_5)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_U3CBodyU3Ek__BackingField_5() const { return ___U3CBodyU3Ek__BackingField_5; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_U3CBodyU3Ek__BackingField_5() { return &___U3CBodyU3Ek__BackingField_5; }
	inline void set_U3CBodyU3Ek__BackingField_5(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___U3CBodyU3Ek__BackingField_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CBodyU3Ek__BackingField_5), value);
	}

	inline static int32_t get_offset_of_U3CStrictU3Ek__BackingField_6() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___U3CStrictU3Ek__BackingField_6)); }
	inline bool get_U3CStrictU3Ek__BackingField_6() const { return ___U3CStrictU3Ek__BackingField_6; }
	inline bool* get_address_of_U3CStrictU3Ek__BackingField_6() { return &___U3CStrictU3Ek__BackingField_6; }
	inline void set_U3CStrictU3Ek__BackingField_6(bool value)
	{
		___U3CStrictU3Ek__BackingField_6 = value;
	}

	inline static int32_t get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_7() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___U3CVariableDeclarationsU3Ek__BackingField_7)); }
	inline RuntimeObject* get_U3CVariableDeclarationsU3Ek__BackingField_7() const { return ___U3CVariableDeclarationsU3Ek__BackingField_7; }
	inline RuntimeObject** get_address_of_U3CVariableDeclarationsU3Ek__BackingField_7() { return &___U3CVariableDeclarationsU3Ek__BackingField_7; }
	inline void set_U3CVariableDeclarationsU3Ek__BackingField_7(RuntimeObject* value)
	{
		___U3CVariableDeclarationsU3Ek__BackingField_7 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableDeclarationsU3Ek__BackingField_7), value);
	}

	inline static int32_t get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_8() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___U3CFunctionDeclarationsU3Ek__BackingField_8)); }
	inline RuntimeObject* get_U3CFunctionDeclarationsU3Ek__BackingField_8() const { return ___U3CFunctionDeclarationsU3Ek__BackingField_8; }
	inline RuntimeObject** get_address_of_U3CFunctionDeclarationsU3Ek__BackingField_8() { return &___U3CFunctionDeclarationsU3Ek__BackingField_8; }
	inline void set_U3CFunctionDeclarationsU3Ek__BackingField_8(RuntimeObject* value)
	{
		___U3CFunctionDeclarationsU3Ek__BackingField_8 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionDeclarationsU3Ek__BackingField_8), value);
	}

	inline static int32_t get_offset_of_Defaults_9() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___Defaults_9)); }
	inline RuntimeObject* get_Defaults_9() const { return ___Defaults_9; }
	inline RuntimeObject** get_address_of_Defaults_9() { return &___Defaults_9; }
	inline void set_Defaults_9(RuntimeObject* value)
	{
		___Defaults_9 = value;
		Il2CppCodeGenWriteBarrier((&___Defaults_9), value);
	}

	inline static int32_t get_offset_of_Rest_10() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___Rest_10)); }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * get_Rest_10() const { return ___Rest_10; }
	inline SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B ** get_address_of_Rest_10() { return &___Rest_10; }
	inline void set_Rest_10(SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B * value)
	{
		___Rest_10 = value;
		Il2CppCodeGenWriteBarrier((&___Rest_10), value);
	}

	inline static int32_t get_offset_of_Generator_11() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___Generator_11)); }
	inline bool get_Generator_11() const { return ___Generator_11; }
	inline bool* get_address_of_Generator_11() { return &___Generator_11; }
	inline void set_Generator_11(bool value)
	{
		___Generator_11 = value;
	}

	inline static int32_t get_offset_of_Expression_12() { return static_cast<int32_t>(offsetof(FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A, ___Expression_12)); }
	inline bool get_Expression_12() const { return ___Expression_12; }
	inline bool* get_address_of_Expression_12() { return &___Expression_12; }
	inline void set_Expression_12(bool value)
	{
		___Expression_12 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FUNCTIONEXPRESSION_TD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A_H
#ifndef IDENTIFIER_T50FB8837E02860B417C98D78294E352C617900EB_H
#define IDENTIFIER_T50FB8837E02860B417C98D78294E352C617900EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Identifier
struct  Identifier_t50FB8837E02860B417C98D78294E352C617900EB  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// System.String Jint.Parser.Ast.Identifier::Name
	String_t* ___Name_3;

public:
	inline static int32_t get_offset_of_Name_3() { return static_cast<int32_t>(offsetof(Identifier_t50FB8837E02860B417C98D78294E352C617900EB, ___Name_3)); }
	inline String_t* get_Name_3() const { return ___Name_3; }
	inline String_t** get_address_of_Name_3() { return &___Name_3; }
	inline void set_Name_3(String_t* value)
	{
		___Name_3 = value;
		Il2CppCodeGenWriteBarrier((&___Name_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IDENTIFIER_T50FB8837E02860B417C98D78294E352C617900EB_H
#ifndef IFSTATEMENT_T3C79E5209248C5BA64B68083F5FD124FE10D254F_H
#define IFSTATEMENT_T3C79E5209248C5BA64B68083F5FD124FE10D254F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.IfStatement
struct  IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.IfStatement::Test
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Test_4;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.IfStatement::Consequent
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Consequent_5;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.IfStatement::Alternate
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Alternate_6;

public:
	inline static int32_t get_offset_of_Test_4() { return static_cast<int32_t>(offsetof(IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F, ___Test_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Test_4() const { return ___Test_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Test_4() { return &___Test_4; }
	inline void set_Test_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Test_4 = value;
		Il2CppCodeGenWriteBarrier((&___Test_4), value);
	}

	inline static int32_t get_offset_of_Consequent_5() { return static_cast<int32_t>(offsetof(IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F, ___Consequent_5)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Consequent_5() const { return ___Consequent_5; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Consequent_5() { return &___Consequent_5; }
	inline void set_Consequent_5(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Consequent_5 = value;
		Il2CppCodeGenWriteBarrier((&___Consequent_5), value);
	}

	inline static int32_t get_offset_of_Alternate_6() { return static_cast<int32_t>(offsetof(IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F, ___Alternate_6)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Alternate_6() const { return ___Alternate_6; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Alternate_6() { return &___Alternate_6; }
	inline void set_Alternate_6(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Alternate_6 = value;
		Il2CppCodeGenWriteBarrier((&___Alternate_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IFSTATEMENT_T3C79E5209248C5BA64B68083F5FD124FE10D254F_H
#ifndef LABELLEDSTATEMENT_TB8922BCE8FE1571B78A306732F3C013F0BB61D25_H
#define LABELLEDSTATEMENT_TB8922BCE8FE1571B78A306732F3C013F0BB61D25_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.LabelledStatement
struct  LabelledStatement_tB8922BCE8FE1571B78A306732F3C013F0BB61D25  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Identifier Jint.Parser.Ast.LabelledStatement::Label
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB * ___Label_4;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.LabelledStatement::Body
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Body_5;

public:
	inline static int32_t get_offset_of_Label_4() { return static_cast<int32_t>(offsetof(LabelledStatement_tB8922BCE8FE1571B78A306732F3C013F0BB61D25, ___Label_4)); }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB * get_Label_4() const { return ___Label_4; }
	inline Identifier_t50FB8837E02860B417C98D78294E352C617900EB ** get_address_of_Label_4() { return &___Label_4; }
	inline void set_Label_4(Identifier_t50FB8837E02860B417C98D78294E352C617900EB * value)
	{
		___Label_4 = value;
		Il2CppCodeGenWriteBarrier((&___Label_4), value);
	}

	inline static int32_t get_offset_of_Body_5() { return static_cast<int32_t>(offsetof(LabelledStatement_tB8922BCE8FE1571B78A306732F3C013F0BB61D25, ___Body_5)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Body_5() const { return ___Body_5; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Body_5() { return &___Body_5; }
	inline void set_Body_5(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Body_5 = value;
		Il2CppCodeGenWriteBarrier((&___Body_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LABELLEDSTATEMENT_TB8922BCE8FE1571B78A306732F3C013F0BB61D25_H
#ifndef LITERAL_TAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9_H
#define LITERAL_TAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Literal
struct  Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// System.Object Jint.Parser.Ast.Literal::Value
	RuntimeObject * ___Value_3;
	// System.String Jint.Parser.Ast.Literal::Raw
	String_t* ___Raw_4;
	// System.Boolean Jint.Parser.Ast.Literal::Cached
	bool ___Cached_5;
	// Jint.Native.JsValue Jint.Parser.Ast.Literal::CachedValue
	JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * ___CachedValue_6;

public:
	inline static int32_t get_offset_of_Value_3() { return static_cast<int32_t>(offsetof(Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9, ___Value_3)); }
	inline RuntimeObject * get_Value_3() const { return ___Value_3; }
	inline RuntimeObject ** get_address_of_Value_3() { return &___Value_3; }
	inline void set_Value_3(RuntimeObject * value)
	{
		___Value_3 = value;
		Il2CppCodeGenWriteBarrier((&___Value_3), value);
	}

	inline static int32_t get_offset_of_Raw_4() { return static_cast<int32_t>(offsetof(Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9, ___Raw_4)); }
	inline String_t* get_Raw_4() const { return ___Raw_4; }
	inline String_t** get_address_of_Raw_4() { return &___Raw_4; }
	inline void set_Raw_4(String_t* value)
	{
		___Raw_4 = value;
		Il2CppCodeGenWriteBarrier((&___Raw_4), value);
	}

	inline static int32_t get_offset_of_Cached_5() { return static_cast<int32_t>(offsetof(Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9, ___Cached_5)); }
	inline bool get_Cached_5() const { return ___Cached_5; }
	inline bool* get_address_of_Cached_5() { return &___Cached_5; }
	inline void set_Cached_5(bool value)
	{
		___Cached_5 = value;
	}

	inline static int32_t get_offset_of_CachedValue_6() { return static_cast<int32_t>(offsetof(Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9, ___CachedValue_6)); }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * get_CachedValue_6() const { return ___CachedValue_6; }
	inline JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 ** get_address_of_CachedValue_6() { return &___CachedValue_6; }
	inline void set_CachedValue_6(JsValue_t822B6795737DB1350A3D0DFAB03CFD4371350D19 * value)
	{
		___CachedValue_6 = value;
		Il2CppCodeGenWriteBarrier((&___CachedValue_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LITERAL_TAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9_H
#ifndef LOGICALEXPRESSION_T1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B_H
#define LOGICALEXPRESSION_T1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.LogicalExpression
struct  LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.LogicalOperator Jint.Parser.Ast.LogicalExpression::Operator
	int32_t ___Operator_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.LogicalExpression::Left
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Left_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.LogicalExpression::Right
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Right_5;

public:
	inline static int32_t get_offset_of_Operator_3() { return static_cast<int32_t>(offsetof(LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B, ___Operator_3)); }
	inline int32_t get_Operator_3() const { return ___Operator_3; }
	inline int32_t* get_address_of_Operator_3() { return &___Operator_3; }
	inline void set_Operator_3(int32_t value)
	{
		___Operator_3 = value;
	}

	inline static int32_t get_offset_of_Left_4() { return static_cast<int32_t>(offsetof(LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B, ___Left_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Left_4() const { return ___Left_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Left_4() { return &___Left_4; }
	inline void set_Left_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Left_4 = value;
		Il2CppCodeGenWriteBarrier((&___Left_4), value);
	}

	inline static int32_t get_offset_of_Right_5() { return static_cast<int32_t>(offsetof(LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B, ___Right_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Right_5() const { return ___Right_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Right_5() { return &___Right_5; }
	inline void set_Right_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Right_5 = value;
		Il2CppCodeGenWriteBarrier((&___Right_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LOGICALEXPRESSION_T1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B_H
#ifndef MEMBEREXPRESSION_T4B86B0602E269576629DE62F496E2EDA5CD2E0D6_H
#define MEMBEREXPRESSION_T4B86B0602E269576629DE62F496E2EDA5CD2E0D6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.MemberExpression
struct  MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.MemberExpression::Object
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Object_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.MemberExpression::Property
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Property_4;
	// System.Boolean Jint.Parser.Ast.MemberExpression::Computed
	bool ___Computed_5;

public:
	inline static int32_t get_offset_of_Object_3() { return static_cast<int32_t>(offsetof(MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6, ___Object_3)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Object_3() const { return ___Object_3; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Object_3() { return &___Object_3; }
	inline void set_Object_3(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Object_3 = value;
		Il2CppCodeGenWriteBarrier((&___Object_3), value);
	}

	inline static int32_t get_offset_of_Property_4() { return static_cast<int32_t>(offsetof(MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6, ___Property_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Property_4() const { return ___Property_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Property_4() { return &___Property_4; }
	inline void set_Property_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Property_4 = value;
		Il2CppCodeGenWriteBarrier((&___Property_4), value);
	}

	inline static int32_t get_offset_of_Computed_5() { return static_cast<int32_t>(offsetof(MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6, ___Computed_5)); }
	inline bool get_Computed_5() const { return ___Computed_5; }
	inline bool* get_address_of_Computed_5() { return &___Computed_5; }
	inline void set_Computed_5(bool value)
	{
		___Computed_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MEMBEREXPRESSION_T4B86B0602E269576629DE62F496E2EDA5CD2E0D6_H
#ifndef NEWEXPRESSION_T6806CEDF7179847105B6CC4CB3076DF69A79B221_H
#define NEWEXPRESSION_T6806CEDF7179847105B6CC4CB3076DF69A79B221_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.NewExpression
struct  NewExpression_t6806CEDF7179847105B6CC4CB3076DF69A79B221  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.NewExpression::Callee
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Callee_3;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Expression> Jint.Parser.Ast.NewExpression::Arguments
	RuntimeObject* ___Arguments_4;

public:
	inline static int32_t get_offset_of_Callee_3() { return static_cast<int32_t>(offsetof(NewExpression_t6806CEDF7179847105B6CC4CB3076DF69A79B221, ___Callee_3)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Callee_3() const { return ___Callee_3; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Callee_3() { return &___Callee_3; }
	inline void set_Callee_3(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Callee_3 = value;
		Il2CppCodeGenWriteBarrier((&___Callee_3), value);
	}

	inline static int32_t get_offset_of_Arguments_4() { return static_cast<int32_t>(offsetof(NewExpression_t6806CEDF7179847105B6CC4CB3076DF69A79B221, ___Arguments_4)); }
	inline RuntimeObject* get_Arguments_4() const { return ___Arguments_4; }
	inline RuntimeObject** get_address_of_Arguments_4() { return &___Arguments_4; }
	inline void set_Arguments_4(RuntimeObject* value)
	{
		___Arguments_4 = value;
		Il2CppCodeGenWriteBarrier((&___Arguments_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NEWEXPRESSION_T6806CEDF7179847105B6CC4CB3076DF69A79B221_H
#ifndef OBJECTEXPRESSION_T3FF8030FF79A4557E7D975262E48E9CA9B109022_H
#define OBJECTEXPRESSION_T3FF8030FF79A4557E7D975262E48E9CA9B109022_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ObjectExpression
struct  ObjectExpression_t3FF8030FF79A4557E7D975262E48E9CA9B109022  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Property> Jint.Parser.Ast.ObjectExpression::Properties
	RuntimeObject* ___Properties_3;

public:
	inline static int32_t get_offset_of_Properties_3() { return static_cast<int32_t>(offsetof(ObjectExpression_t3FF8030FF79A4557E7D975262E48E9CA9B109022, ___Properties_3)); }
	inline RuntimeObject* get_Properties_3() const { return ___Properties_3; }
	inline RuntimeObject** get_address_of_Properties_3() { return &___Properties_3; }
	inline void set_Properties_3(RuntimeObject* value)
	{
		___Properties_3 = value;
		Il2CppCodeGenWriteBarrier((&___Properties_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // OBJECTEXPRESSION_T3FF8030FF79A4557E7D975262E48E9CA9B109022_H
#ifndef PROGRAM_TB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB_H
#define PROGRAM_TB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Program
struct  Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// System.Collections.Generic.ICollection`1<Jint.Parser.Ast.Statement> Jint.Parser.Ast.Program::Body
	RuntimeObject* ___Body_4;
	// System.Collections.Generic.List`1<Jint.Parser.Comment> Jint.Parser.Ast.Program::Comments
	List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 * ___Comments_5;
	// System.Collections.Generic.List`1<Jint.Parser.Token> Jint.Parser.Ast.Program::Tokens
	List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 * ___Tokens_6;
	// System.Collections.Generic.List`1<Jint.Parser.ParserException> Jint.Parser.Ast.Program::Errors
	List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 * ___Errors_7;
	// System.Boolean Jint.Parser.Ast.Program::Strict
	bool ___Strict_8;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.VariableDeclaration> Jint.Parser.Ast.Program::<VariableDeclarations>k__BackingField
	RuntimeObject* ___U3CVariableDeclarationsU3Ek__BackingField_9;
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.FunctionDeclaration> Jint.Parser.Ast.Program::<FunctionDeclarations>k__BackingField
	RuntimeObject* ___U3CFunctionDeclarationsU3Ek__BackingField_10;

public:
	inline static int32_t get_offset_of_Body_4() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___Body_4)); }
	inline RuntimeObject* get_Body_4() const { return ___Body_4; }
	inline RuntimeObject** get_address_of_Body_4() { return &___Body_4; }
	inline void set_Body_4(RuntimeObject* value)
	{
		___Body_4 = value;
		Il2CppCodeGenWriteBarrier((&___Body_4), value);
	}

	inline static int32_t get_offset_of_Comments_5() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___Comments_5)); }
	inline List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 * get_Comments_5() const { return ___Comments_5; }
	inline List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 ** get_address_of_Comments_5() { return &___Comments_5; }
	inline void set_Comments_5(List_1_t5568459CBFCEB189F889690E2846CB4783B8C909 * value)
	{
		___Comments_5 = value;
		Il2CppCodeGenWriteBarrier((&___Comments_5), value);
	}

	inline static int32_t get_offset_of_Tokens_6() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___Tokens_6)); }
	inline List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 * get_Tokens_6() const { return ___Tokens_6; }
	inline List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 ** get_address_of_Tokens_6() { return &___Tokens_6; }
	inline void set_Tokens_6(List_1_t69A649E79F6E79F13E6F251F0CE423AC0D541905 * value)
	{
		___Tokens_6 = value;
		Il2CppCodeGenWriteBarrier((&___Tokens_6), value);
	}

	inline static int32_t get_offset_of_Errors_7() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___Errors_7)); }
	inline List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 * get_Errors_7() const { return ___Errors_7; }
	inline List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 ** get_address_of_Errors_7() { return &___Errors_7; }
	inline void set_Errors_7(List_1_t56DC8C142C1135C5DEA8424BF1A832BF6463B6C1 * value)
	{
		___Errors_7 = value;
		Il2CppCodeGenWriteBarrier((&___Errors_7), value);
	}

	inline static int32_t get_offset_of_Strict_8() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___Strict_8)); }
	inline bool get_Strict_8() const { return ___Strict_8; }
	inline bool* get_address_of_Strict_8() { return &___Strict_8; }
	inline void set_Strict_8(bool value)
	{
		___Strict_8 = value;
	}

	inline static int32_t get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_9() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___U3CVariableDeclarationsU3Ek__BackingField_9)); }
	inline RuntimeObject* get_U3CVariableDeclarationsU3Ek__BackingField_9() const { return ___U3CVariableDeclarationsU3Ek__BackingField_9; }
	inline RuntimeObject** get_address_of_U3CVariableDeclarationsU3Ek__BackingField_9() { return &___U3CVariableDeclarationsU3Ek__BackingField_9; }
	inline void set_U3CVariableDeclarationsU3Ek__BackingField_9(RuntimeObject* value)
	{
		___U3CVariableDeclarationsU3Ek__BackingField_9 = value;
		Il2CppCodeGenWriteBarrier((&___U3CVariableDeclarationsU3Ek__BackingField_9), value);
	}

	inline static int32_t get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_10() { return static_cast<int32_t>(offsetof(Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB, ___U3CFunctionDeclarationsU3Ek__BackingField_10)); }
	inline RuntimeObject* get_U3CFunctionDeclarationsU3Ek__BackingField_10() const { return ___U3CFunctionDeclarationsU3Ek__BackingField_10; }
	inline RuntimeObject** get_address_of_U3CFunctionDeclarationsU3Ek__BackingField_10() { return &___U3CFunctionDeclarationsU3Ek__BackingField_10; }
	inline void set_U3CFunctionDeclarationsU3Ek__BackingField_10(RuntimeObject* value)
	{
		___U3CFunctionDeclarationsU3Ek__BackingField_10 = value;
		Il2CppCodeGenWriteBarrier((&___U3CFunctionDeclarationsU3Ek__BackingField_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROGRAM_TB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB_H
#ifndef PROPERTY_T5570E62DC83576B24671CFB29C9FDA71C32EDF56_H
#define PROPERTY_T5570E62DC83576B24671CFB29C9FDA71C32EDF56_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.Property
struct  Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.PropertyKind Jint.Parser.Ast.Property::Kind
	int32_t ___Kind_3;
	// Jint.Parser.Ast.IPropertyKeyExpression Jint.Parser.Ast.Property::Key
	RuntimeObject* ___Key_4;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.Property::Value
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Value_5;

public:
	inline static int32_t get_offset_of_Kind_3() { return static_cast<int32_t>(offsetof(Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56, ___Kind_3)); }
	inline int32_t get_Kind_3() const { return ___Kind_3; }
	inline int32_t* get_address_of_Kind_3() { return &___Kind_3; }
	inline void set_Kind_3(int32_t value)
	{
		___Kind_3 = value;
	}

	inline static int32_t get_offset_of_Key_4() { return static_cast<int32_t>(offsetof(Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56, ___Key_4)); }
	inline RuntimeObject* get_Key_4() const { return ___Key_4; }
	inline RuntimeObject** get_address_of_Key_4() { return &___Key_4; }
	inline void set_Key_4(RuntimeObject* value)
	{
		___Key_4 = value;
		Il2CppCodeGenWriteBarrier((&___Key_4), value);
	}

	inline static int32_t get_offset_of_Value_5() { return static_cast<int32_t>(offsetof(Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56, ___Value_5)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Value_5() const { return ___Value_5; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Value_5() { return &___Value_5; }
	inline void set_Value_5(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Value_5 = value;
		Il2CppCodeGenWriteBarrier((&___Value_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROPERTY_T5570E62DC83576B24671CFB29C9FDA71C32EDF56_H
#ifndef RETURNSTATEMENT_TD75187A0A5A4D5273C527CE9E1A14F536AD78052_H
#define RETURNSTATEMENT_TD75187A0A5A4D5273C527CE9E1A14F536AD78052_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ReturnStatement
struct  ReturnStatement_tD75187A0A5A4D5273C527CE9E1A14F536AD78052  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ReturnStatement::Argument
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Argument_4;

public:
	inline static int32_t get_offset_of_Argument_4() { return static_cast<int32_t>(offsetof(ReturnStatement_tD75187A0A5A4D5273C527CE9E1A14F536AD78052, ___Argument_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Argument_4() const { return ___Argument_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Argument_4() { return &___Argument_4; }
	inline void set_Argument_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Argument_4 = value;
		Il2CppCodeGenWriteBarrier((&___Argument_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RETURNSTATEMENT_TD75187A0A5A4D5273C527CE9E1A14F536AD78052_H
#ifndef SEQUENCEEXPRESSION_T424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1_H
#define SEQUENCEEXPRESSION_T424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SequenceExpression
struct  SequenceExpression_t424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// System.Collections.Generic.IList`1<Jint.Parser.Ast.Expression> Jint.Parser.Ast.SequenceExpression::Expressions
	RuntimeObject* ___Expressions_3;

public:
	inline static int32_t get_offset_of_Expressions_3() { return static_cast<int32_t>(offsetof(SequenceExpression_t424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1, ___Expressions_3)); }
	inline RuntimeObject* get_Expressions_3() const { return ___Expressions_3; }
	inline RuntimeObject** get_address_of_Expressions_3() { return &___Expressions_3; }
	inline void set_Expressions_3(RuntimeObject* value)
	{
		___Expressions_3 = value;
		Il2CppCodeGenWriteBarrier((&___Expressions_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SEQUENCEEXPRESSION_T424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1_H
#ifndef SWITCHSTATEMENT_T7FEFC31902A5B229EC0382398027E79503785F54_H
#define SWITCHSTATEMENT_T7FEFC31902A5B229EC0382398027E79503785F54_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.SwitchStatement
struct  SwitchStatement_t7FEFC31902A5B229EC0382398027E79503785F54  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.SwitchStatement::Discriminant
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Discriminant_4;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.SwitchCase> Jint.Parser.Ast.SwitchStatement::Cases
	RuntimeObject* ___Cases_5;

public:
	inline static int32_t get_offset_of_Discriminant_4() { return static_cast<int32_t>(offsetof(SwitchStatement_t7FEFC31902A5B229EC0382398027E79503785F54, ___Discriminant_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Discriminant_4() const { return ___Discriminant_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Discriminant_4() { return &___Discriminant_4; }
	inline void set_Discriminant_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Discriminant_4 = value;
		Il2CppCodeGenWriteBarrier((&___Discriminant_4), value);
	}

	inline static int32_t get_offset_of_Cases_5() { return static_cast<int32_t>(offsetof(SwitchStatement_t7FEFC31902A5B229EC0382398027E79503785F54, ___Cases_5)); }
	inline RuntimeObject* get_Cases_5() const { return ___Cases_5; }
	inline RuntimeObject** get_address_of_Cases_5() { return &___Cases_5; }
	inline void set_Cases_5(RuntimeObject* value)
	{
		___Cases_5 = value;
		Il2CppCodeGenWriteBarrier((&___Cases_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SWITCHSTATEMENT_T7FEFC31902A5B229EC0382398027E79503785F54_H
#ifndef THISEXPRESSION_TF6731B45D761FDE36F0D97FB7A225F7E6146D47A_H
#define THISEXPRESSION_TF6731B45D761FDE36F0D97FB7A225F7E6146D47A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ThisExpression
struct  ThisExpression_tF6731B45D761FDE36F0D97FB7A225F7E6146D47A  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THISEXPRESSION_TF6731B45D761FDE36F0D97FB7A225F7E6146D47A_H
#ifndef THROWSTATEMENT_TEA363AC9843796E5212BF75C4E20496283B67B71_H
#define THROWSTATEMENT_TEA363AC9843796E5212BF75C4E20496283B67B71_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.ThrowStatement
struct  ThrowStatement_tEA363AC9843796E5212BF75C4E20496283B67B71  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.ThrowStatement::Argument
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Argument_4;

public:
	inline static int32_t get_offset_of_Argument_4() { return static_cast<int32_t>(offsetof(ThrowStatement_tEA363AC9843796E5212BF75C4E20496283B67B71, ___Argument_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Argument_4() const { return ___Argument_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Argument_4() { return &___Argument_4; }
	inline void set_Argument_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Argument_4 = value;
		Il2CppCodeGenWriteBarrier((&___Argument_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // THROWSTATEMENT_TEA363AC9843796E5212BF75C4E20496283B67B71_H
#ifndef TRYSTATEMENT_TCCB2F3192201292BE16CF157EDB721DE614CABA5_H
#define TRYSTATEMENT_TCCB2F3192201292BE16CF157EDB721DE614CABA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.TryStatement
struct  TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.TryStatement::Block
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Block_4;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.Statement> Jint.Parser.Ast.TryStatement::GuardedHandlers
	RuntimeObject* ___GuardedHandlers_5;
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.CatchClause> Jint.Parser.Ast.TryStatement::Handlers
	RuntimeObject* ___Handlers_6;
	// Jint.Parser.Ast.Statement Jint.Parser.Ast.TryStatement::Finalizer
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * ___Finalizer_7;

public:
	inline static int32_t get_offset_of_Block_4() { return static_cast<int32_t>(offsetof(TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5, ___Block_4)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Block_4() const { return ___Block_4; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Block_4() { return &___Block_4; }
	inline void set_Block_4(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Block_4 = value;
		Il2CppCodeGenWriteBarrier((&___Block_4), value);
	}

	inline static int32_t get_offset_of_GuardedHandlers_5() { return static_cast<int32_t>(offsetof(TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5, ___GuardedHandlers_5)); }
	inline RuntimeObject* get_GuardedHandlers_5() const { return ___GuardedHandlers_5; }
	inline RuntimeObject** get_address_of_GuardedHandlers_5() { return &___GuardedHandlers_5; }
	inline void set_GuardedHandlers_5(RuntimeObject* value)
	{
		___GuardedHandlers_5 = value;
		Il2CppCodeGenWriteBarrier((&___GuardedHandlers_5), value);
	}

	inline static int32_t get_offset_of_Handlers_6() { return static_cast<int32_t>(offsetof(TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5, ___Handlers_6)); }
	inline RuntimeObject* get_Handlers_6() const { return ___Handlers_6; }
	inline RuntimeObject** get_address_of_Handlers_6() { return &___Handlers_6; }
	inline void set_Handlers_6(RuntimeObject* value)
	{
		___Handlers_6 = value;
		Il2CppCodeGenWriteBarrier((&___Handlers_6), value);
	}

	inline static int32_t get_offset_of_Finalizer_7() { return static_cast<int32_t>(offsetof(TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5, ___Finalizer_7)); }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * get_Finalizer_7() const { return ___Finalizer_7; }
	inline Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 ** get_address_of_Finalizer_7() { return &___Finalizer_7; }
	inline void set_Finalizer_7(Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0 * value)
	{
		___Finalizer_7 = value;
		Il2CppCodeGenWriteBarrier((&___Finalizer_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TRYSTATEMENT_TCCB2F3192201292BE16CF157EDB721DE614CABA5_H
#ifndef UNARYEXPRESSION_TD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC_H
#define UNARYEXPRESSION_TD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.UnaryExpression
struct  UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC  : public Expression_tAC599936AD6D021EA6A3A695C803975140919ADB
{
public:
	// Jint.Parser.Ast.UnaryOperator Jint.Parser.Ast.UnaryExpression::Operator
	int32_t ___Operator_3;
	// Jint.Parser.Ast.Expression Jint.Parser.Ast.UnaryExpression::Argument
	Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * ___Argument_4;
	// System.Boolean Jint.Parser.Ast.UnaryExpression::Prefix
	bool ___Prefix_5;

public:
	inline static int32_t get_offset_of_Operator_3() { return static_cast<int32_t>(offsetof(UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC, ___Operator_3)); }
	inline int32_t get_Operator_3() const { return ___Operator_3; }
	inline int32_t* get_address_of_Operator_3() { return &___Operator_3; }
	inline void set_Operator_3(int32_t value)
	{
		___Operator_3 = value;
	}

	inline static int32_t get_offset_of_Argument_4() { return static_cast<int32_t>(offsetof(UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC, ___Argument_4)); }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * get_Argument_4() const { return ___Argument_4; }
	inline Expression_tAC599936AD6D021EA6A3A695C803975140919ADB ** get_address_of_Argument_4() { return &___Argument_4; }
	inline void set_Argument_4(Expression_tAC599936AD6D021EA6A3A695C803975140919ADB * value)
	{
		___Argument_4 = value;
		Il2CppCodeGenWriteBarrier((&___Argument_4), value);
	}

	inline static int32_t get_offset_of_Prefix_5() { return static_cast<int32_t>(offsetof(UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC, ___Prefix_5)); }
	inline bool get_Prefix_5() const { return ___Prefix_5; }
	inline bool* get_address_of_Prefix_5() { return &___Prefix_5; }
	inline void set_Prefix_5(bool value)
	{
		___Prefix_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNARYEXPRESSION_TD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC_H
#ifndef VARIABLEDECLARATION_TA8DB09671730A13B048BE8425A3B08C156721952_H
#define VARIABLEDECLARATION_TA8DB09671730A13B048BE8425A3B08C156721952_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.VariableDeclaration
struct  VariableDeclaration_tA8DB09671730A13B048BE8425A3B08C156721952  : public Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0
{
public:
	// System.Collections.Generic.IEnumerable`1<Jint.Parser.Ast.VariableDeclarator> Jint.Parser.Ast.VariableDeclaration::Declarations
	RuntimeObject* ___Declarations_4;
	// System.String Jint.Parser.Ast.VariableDeclaration::Kind
	String_t* ___Kind_5;

public:
	inline static int32_t get_offset_of_Declarations_4() { return static_cast<int32_t>(offsetof(VariableDeclaration_tA8DB09671730A13B048BE8425A3B08C156721952, ___Declarations_4)); }
	inline RuntimeObject* get_Declarations_4() const { return ___Declarations_4; }
	inline RuntimeObject** get_address_of_Declarations_4() { return &___Declarations_4; }
	inline void set_Declarations_4(RuntimeObject* value)
	{
		___Declarations_4 = value;
		Il2CppCodeGenWriteBarrier((&___Declarations_4), value);
	}

	inline static int32_t get_offset_of_Kind_5() { return static_cast<int32_t>(offsetof(VariableDeclaration_tA8DB09671730A13B048BE8425A3B08C156721952, ___Kind_5)); }
	inline String_t* get_Kind_5() const { return ___Kind_5; }
	inline String_t** get_address_of_Kind_5() { return &___Kind_5; }
	inline void set_Kind_5(String_t* value)
	{
		___Kind_5 = value;
		Il2CppCodeGenWriteBarrier((&___Kind_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VARIABLEDECLARATION_TA8DB09671730A13B048BE8425A3B08C156721952_H
#ifndef UPDATEEXPRESSION_T8AD17AC0304ECB931515971F92BAC97FD67AA653_H
#define UPDATEEXPRESSION_T8AD17AC0304ECB931515971F92BAC97FD67AA653_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Jint.Parser.Ast.UpdateExpression
struct  UpdateExpression_t8AD17AC0304ECB931515971F92BAC97FD67AA653  : public UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UPDATEEXPRESSION_T8AD17AC0304ECB931515971F92BAC97FD67AA653_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5600 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5601 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5602 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5603 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5604 = { sizeof (MethodInfoFunctionInstance_t483F3C5DB3B58053A1233B23940AF856A467DE5C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5604[1] = 
{
	MethodInfoFunctionInstance_t483F3C5DB3B58053A1233B23940AF856A467DE5C::get_offset_of__methods_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5605 = { sizeof (U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799), -1, sizeof(U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5605[2] = 
{
	U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tCFA2BF4DE783609BD160CDA34A1848874AB5E799_StaticFields::get_offset_of_U3CU3E9__4_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5606 = { sizeof (NamespaceReference_tDE03F4B45F487622D8D00B08AA7B0143097C203D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5606[1] = 
{
	NamespaceReference_tDE03F4B45F487622D8D00B08AA7B0143097C203D::get_offset_of__path_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5607 = { sizeof (ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5607[1] = 
{
	ObjectWrapper_tDD4EA894ED1C73DF983BE152B6067702D9BB5EB5::get_offset_of_U3CTargetU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5608 = { sizeof (U3CU3Ec__DisplayClass6_0_t4B40E563CC434DA33BC857EFE7A03C911BB984D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5608[2] = 
{
	U3CU3Ec__DisplayClass6_0_t4B40E563CC434DA33BC857EFE7A03C911BB984D5::get_offset_of_propertyName_0(),
	U3CU3Ec__DisplayClass6_0_t4B40E563CC434DA33BC857EFE7A03C911BB984D5::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5609 = { sizeof (U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231), -1, sizeof(U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5609[12] = 
{
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_3_1(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_4_2(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_5_3(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_7_4(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_8_5(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_9_6(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_11_7(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_12_8(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_13_9(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_14_10(),
	U3CU3Ec_t78F11CC72D53DA526A9B9CBB3BB2EBF05C042231_StaticFields::get_offset_of_U3CU3E9__6_15_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5610 = { sizeof (SetterFunctionInstance_t324F381539B5C559E10081DB06D8BD0BBD454B09), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5610[1] = 
{
	SetterFunctionInstance_t324F381539B5C559E10081DB06D8BD0BBD454B09::get_offset_of__setter_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5611 = { sizeof (TypeReference_t5D3C2ED721325BFEDF31379C3E93D40F5AC17892), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5611[1] = 
{
	TypeReference_t5D3C2ED721325BFEDF31379C3E93D40F5AC17892::get_offset_of_U3CTypeU3Ek__BackingField_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5612 = { sizeof (U3CU3Ec__DisplayClass12_0_t83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5612[1] = 
{
	U3CU3Ec__DisplayClass12_0_t83A8EB8CFF034C2E40D2CF53E5C50A9D8F80F5DC::get_offset_of_propertyName_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5613 = { sizeof (Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5613[3] = 
{
	Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D::get_offset_of_Value_0(),
	Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D::get_offset_of_CanBeDeleted_1(),
	Binding_t56830C02382DE83D9086BFDA0EDD0520D9E1781D::get_offset_of_Mutable_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5614 = { sizeof (DeclarativeEnvironmentRecord_tEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5614[2] = 
{
	DeclarativeEnvironmentRecord_tEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3::get_offset_of__engine_4(),
	DeclarativeEnvironmentRecord_tEF12BBF3AE9E21C1EC0847AD5A353CCB3BE28FA3::get_offset_of__bindings_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5615 = { sizeof (EnvironmentRecord_t81E449E16161905BFB3BD8D0596E82E2619FB53B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5616 = { sizeof (ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5616[3] = 
{
	ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE::get_offset_of_U3CLexicalEnvironmentU3Ek__BackingField_0(),
	ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE::get_offset_of_U3CVariableEnvironmentU3Ek__BackingField_1(),
	ExecutionContext_tCBE07A5E359EDD941E06F143251E5C20DCF910FE::get_offset_of_U3CThisBindingU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5617 = { sizeof (LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5617[2] = 
{
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026::get_offset_of__record_0(),
	LexicalEnvironment_t9181F8CD8E041687DE252EA6C808F36174248026::get_offset_of__outer_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5618 = { sizeof (ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5618[3] = 
{
	ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64::get_offset_of__engine_4(),
	ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64::get_offset_of__bindingObject_5(),
	ObjectEnvironmentRecord_t735FE784390C989CDD891FE4BEA0C938D71F1F64::get_offset_of__provideThis_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5619 = { sizeof (U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202), -1, sizeof(U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5619[2] = 
{
	U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tE510622EA06869BD70430A0AF1278748E2300202_StaticFields::get_offset_of_U3CU3E9__10_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5620 = { sizeof (PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73), -1, sizeof(PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5620[7] = 
{
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73_StaticFields::get_offset_of_Undefined_0(),
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73::get_offset_of_U3CGetU3Ek__BackingField_1(),
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73::get_offset_of_U3CSetU3Ek__BackingField_2(),
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73::get_offset_of_U3CEnumerableU3Ek__BackingField_3(),
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73::get_offset_of_U3CWritableU3Ek__BackingField_4(),
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73::get_offset_of_U3CConfigurableU3Ek__BackingField_5(),
	PropertyDescriptor_tC246E1A601A5DD8127DE7859409B89D3B1939A73::get_offset_of_U3CValueU3Ek__BackingField_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5621 = { sizeof (ClrAccessDescriptor_t3D6D255735AD3760BB73CB59079B8289FB0E6AD1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5622 = { sizeof (FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5622[3] = 
{
	FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E::get_offset_of__engine_7(),
	FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E::get_offset_of__fieldInfo_8(),
	FieldInfoDescriptor_tE3D330AEB2A3533157DD5A2D1B6F53CDC54D381E::get_offset_of__item_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5623 = { sizeof (IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5623[5] = 
{
	IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99::get_offset_of__engine_7(),
	IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99::get_offset_of__key_8(),
	IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99::get_offset_of__item_9(),
	IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99::get_offset_of__indexer_10(),
	IndexDescriptor_t8BA0E96659D6D4601AFD470C61271904598C5A99::get_offset_of__containsKey_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5624 = { sizeof (PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5624[3] = 
{
	PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB::get_offset_of__engine_7(),
	PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB::get_offset_of__propertyInfo_8(),
	PropertyInfoDescriptor_t568A54E6928FF78BB2DF8A84F508069A2FE90EBB::get_offset_of__item_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5625 = { sizeof (BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5625[3] = 
{
	BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688::get_offset_of_U3CLineU3Ek__BackingField_0(),
	BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688::get_offset_of_U3CCharU3Ek__BackingField_1(),
	BreakPoint_t50BE5BFB3CB9595048CEFAC2AC5DA1ADC3C13688::get_offset_of_U3CConditionU3Ek__BackingField_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5626 = { sizeof (DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5626[4] = 
{
	DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296::get_offset_of__debugCallStack_0(),
	DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296::get_offset_of__stepMode_1(),
	DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296::get_offset_of__callBackStepOverDepth_2(),
	DebugHandler_t42446EF263A027A800A0BF5BD64A06674E4A6296::get_offset_of__engine_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5627 = { sizeof (U3CU3Ec__DisplayClass7_0_t024887E8EA9F24EFD58C9231F317C5F16E662029), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5627[2] = 
{
	U3CU3Ec__DisplayClass7_0_t024887E8EA9F24EFD58C9231F317C5F16E662029::get_offset_of_statement_0(),
	U3CU3Ec__DisplayClass7_0_t024887E8EA9F24EFD58C9231F317C5F16E662029::get_offset_of_U3CU3E4__this_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5628 = { sizeof (DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5628[4] = 
{
	DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C::get_offset_of_U3CCallStackU3Ek__BackingField_1(),
	DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C::get_offset_of_U3CCurrentStatementU3Ek__BackingField_2(),
	DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C::get_offset_of_U3CLocalsU3Ek__BackingField_3(),
	DebugInformation_t1C019A6FE9EC7D5A367D194949D83E3E49EDCB8C::get_offset_of_U3CGlobalsU3Ek__BackingField_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5629 = { sizeof (StepMode_tF8A960EEDD1BEF8767A1D32BC546D0BB17792D23)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5629[5] = 
{
	StepMode_tF8A960EEDD1BEF8767A1D32BC546D0BB17792D23::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5630 = { sizeof (CallStackElementComparer_tC9CD7CCFD0DDF3EB0258D8A40536912ED43B1B6C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5631 = { sizeof (JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5631[2] = 
{
	JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6::get_offset_of__stack_0(),
	JintCallStack_tC35F241423CC5B5C3185463D6476DF0223D133E6::get_offset_of__statistics_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5632 = { sizeof (U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056), -1, sizeof(U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5632[2] = 
{
	U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_tF62F3D23D314BE616B33845B3BC13B175B525056_StaticFields::get_offset_of_U3CU3E9__6_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5633 = { sizeof (Comment_t3B095647E0F42D26F18855F536600AB51687CAC1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5633[4] = 
{
	Comment_t3B095647E0F42D26F18855F536600AB51687CAC1::get_offset_of_Location_0(),
	Comment_t3B095647E0F42D26F18855F536600AB51687CAC1::get_offset_of_Range_1(),
	Comment_t3B095647E0F42D26F18855F536600AB51687CAC1::get_offset_of_Type_2(),
	Comment_t3B095647E0F42D26F18855F536600AB51687CAC1::get_offset_of_Value_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5634 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5635 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5636 = { sizeof (FunctionScope_t18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5636[2] = 
{
	FunctionScope_t18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434::get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_0(),
	FunctionScope_t18E773CB82EDE2D365DF6A29CCFF2E6D9FB17434::get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5637 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5638 = { sizeof (VariableScope_tA73D304160DEDDF9AE542B6C15D74A651544C585), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5638[1] = 
{
	VariableScope_tA73D304160DEDDF9AE542B6C15D74A651544C585::get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5639 = { sizeof (JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72), -1, sizeof(JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5639[15] = 
{
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields::get_offset_of_Keywords_0(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields::get_offset_of_StrictModeReservedWords_1(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72_StaticFields::get_offset_of_FutureReservedWords_2(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__extra_3(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__index_4(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__length_5(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__lineNumber_6(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__lineStart_7(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__location_8(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__lookahead_9(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__source_10(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__state_11(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__strict_12(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__variableScopes_13(),
	JavaScriptParser_tC33321877DC5FF0F10EC4092D2CC04BE25064E72::get_offset_of__functionScopes_14(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5640 = { sizeof (Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5640[6] = 
{
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5::get_offset_of_Loc_0(),
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5::get_offset_of_Range_1(),
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5::get_offset_of_Source_2(),
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5::get_offset_of_Comments_3(),
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5::get_offset_of_Tokens_4(),
	Extra_tDA7FD4EE7A486B0D379D446CFBED4D247BCEEBA5::get_offset_of_Errors_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5641 = { sizeof (LocationMarker_t27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5641[1] = 
{
	LocationMarker_t27BCCC9E9E0EC094E3B227F1AC9C6F9749E5AFD8::get_offset_of__marker_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5642 = { sizeof (ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5642[4] = 
{
	ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A::get_offset_of_FirstRestricted_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A::get_offset_of_Message_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A::get_offset_of_Parameters_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	ParsedParameters_t8523E604AD6C24C3157FCF091C97B1C38EAB5D7A::get_offset_of_Stricted_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5643 = { sizeof (Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B), -1, sizeof(Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5643[2] = 
{
	Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_StaticFields::get_offset_of_NonAsciiIdentifierStart_0(),
	Regexes_t8176CAF25B2B55B1D72FFF449E318A1C54BB6B6B_StaticFields::get_offset_of_NonAsciiIdentifierPart_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5644 = { sizeof (Location_t9639547EECD85868A9B1627343BF1C80DE46B783), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5644[3] = 
{
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783::get_offset_of_Start_0(),
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783::get_offset_of_End_1(),
	Location_t9639547EECD85868A9B1627343BF1C80DE46B783::get_offset_of_Source_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5645 = { sizeof (Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8), -1, sizeof(Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5645[33] = 
{
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnexpectedToken_0(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnexpectedNumber_1(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnexpectedString_2(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnexpectedIdentifier_3(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnexpectedReserved_4(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnexpectedEOS_5(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_NewlineAfterThrow_6(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_InvalidRegExp_7(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnterminatedRegExp_8(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_InvalidLHSInAssignment_9(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_InvalidLHSInForIn_10(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_MultipleDefaultsInSwitch_11(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_NoCatchOrFinally_12(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_UnknownLabel_13(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_Redeclaration_14(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_IllegalContinue_15(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_IllegalBreak_16(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_IllegalReturn_17(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictModeWith_18(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictCatchVariable_19(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictVarName_20(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictParamName_21(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictParamDupe_22(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictFunctionName_23(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictOctalLiteral_24(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictDelete_25(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictDuplicateProperty_26(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_AccessorDataProperty_27(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_AccessorGetSet_28(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictLHSAssignment_29(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictLHSPostfix_30(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictLHSPrefix_31(),
	Messages_tA5D06E0F7E9F9D6FF6E171C696E0089B6555B5A8_StaticFields::get_offset_of_StrictReservedWord_32(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5646 = { sizeof (ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5646[4] = 
{
	ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120::get_offset_of_Column_17(),
	ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120::get_offset_of_Description_18(),
	ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120::get_offset_of_Index_19(),
	ParserException_t949C8B2CFA221CFB0E3199560448F8364467C120::get_offset_of_LineNumber_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5647 = { sizeof (ParserExtensions_t25486BCFA5ACD7E155AB15BE956C13D919E9563A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5648 = { sizeof (ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5648[4] = 
{
	ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672::get_offset_of_U3CSourceU3Ek__BackingField_0(),
	ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672::get_offset_of_U3CTokensU3Ek__BackingField_1(),
	ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672::get_offset_of_U3CCommentU3Ek__BackingField_2(),
	ParserOptions_t944C18524B8847EFDD587A1C78B0CF4750F0B672::get_offset_of_U3CTolerantU3Ek__BackingField_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5649 = { sizeof (Position_t53796EA606FAC08CB873A12EF2C392F957F9D509)+ sizeof (RuntimeObject), sizeof(Position_t53796EA606FAC08CB873A12EF2C392F957F9D509 ), 0, 0 };
extern const int32_t g_FieldOffsetTable5649[2] = 
{
	Position_t53796EA606FAC08CB873A12EF2C392F957F9D509::get_offset_of_Line_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Position_t53796EA606FAC08CB873A12EF2C392F957F9D509::get_offset_of_Column_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5650 = { sizeof (State_tE5E4EE87409DF109801A5725B4A75C00E666E86A)+ sizeof (RuntimeObject), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5650[7] = 
{
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_LastCommentStart_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_AllowIn_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_LabelSet_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_InFunctionBody_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_InIteration_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_InSwitch_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	State_tE5E4EE87409DF109801A5725B4A75C00E666E86A::get_offset_of_MarkerStack_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5651 = { sizeof (Tokens_t76408800A95ECB3912AC7B31208B6E2531669559)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5651[10] = 
{
	Tokens_t76408800A95ECB3912AC7B31208B6E2531669559::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5652 = { sizeof (Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37), -1, sizeof(Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable5652[10] = 
{
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37_StaticFields::get_offset_of_Empty_0(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Type_1(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Literal_2(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Value_3(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Range_4(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_LineNumber_5(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_LineStart_6(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Octal_7(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Location_8(),
	Token_t2ED7D92D91BC61AAB1E6B2928BCCE9DA0F556D37::get_offset_of_Precedence_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5653 = { sizeof (ArrayExpression_t46B16E6EFAB06AC476919129BCE7A2440B8B4040), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5653[1] = 
{
	ArrayExpression_t46B16E6EFAB06AC476919129BCE7A2440B8B4040::get_offset_of_Elements_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5654 = { sizeof (AssignmentOperator_tADB31EB9CA5217BCD25CBE630A560E9F33EC9E41)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5654[13] = 
{
	AssignmentOperator_tADB31EB9CA5217BCD25CBE630A560E9F33EC9E41::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5655 = { sizeof (AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5655[3] = 
{
	AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E::get_offset_of_Operator_3(),
	AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E::get_offset_of_Left_4(),
	AssignmentExpression_t00F2EE5EDE8771D96BE81D39257823B4140EE55E::get_offset_of_Right_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5656 = { sizeof (BinaryOperator_t5651229A29A80B1D899E3764D062DE8E4BC0A5E6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5656[22] = 
{
	BinaryOperator_t5651229A29A80B1D899E3764D062DE8E4BC0A5E6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5657 = { sizeof (BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5657[3] = 
{
	BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411::get_offset_of_Operator_3(),
	BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411::get_offset_of_Left_4(),
	BinaryExpression_t45F2093E7956D81E1A63CB502D06A8E4E910E411::get_offset_of_Right_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5658 = { sizeof (BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5658[1] = 
{
	BlockStatement_t8441FA49BCE7A845BB4B30AA28D0980133EA216E::get_offset_of_Body_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5659 = { sizeof (BreakStatement_t640A809473C0484A423069D6DB0BA1D4A4A2B5DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5659[1] = 
{
	BreakStatement_t640A809473C0484A423069D6DB0BA1D4A4A2B5DC::get_offset_of_Label_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5660 = { sizeof (CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5660[5] = 
{
	CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB::get_offset_of_Callee_3(),
	CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB::get_offset_of_Arguments_4(),
	CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB::get_offset_of_Cached_5(),
	CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB::get_offset_of_CanBeCached_6(),
	CallExpression_t81E1C5DDDF4A0D866F4CA130E9C6142641B072BB::get_offset_of_CachedArguments_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5661 = { sizeof (CatchClause_t80ADAE100F384254ED8138A2DE08AA18DCA38CDD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5661[2] = 
{
	CatchClause_t80ADAE100F384254ED8138A2DE08AA18DCA38CDD::get_offset_of_Param_4(),
	CatchClause_t80ADAE100F384254ED8138A2DE08AA18DCA38CDD::get_offset_of_Body_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5662 = { sizeof (ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5662[3] = 
{
	ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046::get_offset_of_Test_3(),
	ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046::get_offset_of_Consequent_4(),
	ConditionalExpression_t52703455D8FF8C760F672D9A508C6873E456E046::get_offset_of_Alternate_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5663 = { sizeof (ContinueStatement_t7BCF4378D59BCF5DE09CC365E697E4D7D702905C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5663[1] = 
{
	ContinueStatement_t7BCF4378D59BCF5DE09CC365E697E4D7D702905C::get_offset_of_Label_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5664 = { sizeof (DebuggerStatement_t25A052428FF0DD88B55C026CEB9B34E895C1CB28), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5665 = { sizeof (DoWhileStatement_t5F178F4A9B61D35DEDA8C705B2576DA6166E818C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5665[2] = 
{
	DoWhileStatement_t5F178F4A9B61D35DEDA8C705B2576DA6166E818C::get_offset_of_Body_4(),
	DoWhileStatement_t5F178F4A9B61D35DEDA8C705B2576DA6166E818C::get_offset_of_Test_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5666 = { sizeof (EmptyStatement_tA94A7F5D1B44C4697B622066D3457B293E2B9A48), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5667 = { sizeof (Expression_tAC599936AD6D021EA6A3A695C803975140919ADB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5668 = { sizeof (ExpressionStatement_t1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5668[1] = 
{
	ExpressionStatement_t1A67D8CA29C9A18472A2A9A3EB0063F63F69A8CC::get_offset_of_Expression_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5669 = { sizeof (ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5669[4] = 
{
	ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB::get_offset_of_Left_4(),
	ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB::get_offset_of_Right_5(),
	ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB::get_offset_of_Body_6(),
	ForInStatement_tBECC7E626B3C0995947F27F07CC6843114400AAB::get_offset_of_Each_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5670 = { sizeof (ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5670[4] = 
{
	ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1::get_offset_of_Init_4(),
	ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1::get_offset_of_Test_5(),
	ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1::get_offset_of_Update_6(),
	ForStatement_t6C3DFF2A02EC63BDED68ACA35E8CFBA6012EF7C1::get_offset_of_Body_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5671 = { sizeof (FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5671[10] = 
{
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_U3CIdU3Ek__BackingField_4(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_U3CParametersU3Ek__BackingField_5(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_U3CBodyU3Ek__BackingField_6(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_U3CStrictU3Ek__BackingField_7(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_8(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_Defaults_9(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_Rest_10(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_Generator_11(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_Expression_12(),
	FunctionDeclaration_tD82FBCBF055374B5A8051C961098576C24786C7D::get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5672 = { sizeof (FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5672[10] = 
{
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_U3CIdU3Ek__BackingField_3(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_U3CParametersU3Ek__BackingField_4(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_U3CBodyU3Ek__BackingField_5(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_U3CStrictU3Ek__BackingField_6(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_7(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_8(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_Defaults_9(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_Rest_10(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_Generator_11(),
	FunctionExpression_tD87DE7CE8EE4CB8CACA202DEAF1F11CAB30B8F5A::get_offset_of_Expression_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5673 = { sizeof (Identifier_t50FB8837E02860B417C98D78294E352C617900EB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5673[1] = 
{
	Identifier_t50FB8837E02860B417C98D78294E352C617900EB::get_offset_of_Name_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5674 = { sizeof (IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5674[3] = 
{
	IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F::get_offset_of_Test_4(),
	IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F::get_offset_of_Consequent_5(),
	IfStatement_t3C79E5209248C5BA64B68083F5FD124FE10D254F::get_offset_of_Alternate_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5675 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5676 = { sizeof (LabelledStatement_tB8922BCE8FE1571B78A306732F3C013F0BB61D25), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5676[2] = 
{
	LabelledStatement_tB8922BCE8FE1571B78A306732F3C013F0BB61D25::get_offset_of_Label_4(),
	LabelledStatement_tB8922BCE8FE1571B78A306732F3C013F0BB61D25::get_offset_of_Body_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5677 = { sizeof (Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5677[4] = 
{
	Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9::get_offset_of_Value_3(),
	Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9::get_offset_of_Raw_4(),
	Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9::get_offset_of_Cached_5(),
	Literal_tAB837ECC763DDAE59771BA90B5B3AAE6D083AEB9::get_offset_of_CachedValue_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5678 = { sizeof (LogicalOperator_tD30BF74503BE6556A2EBBA96A165809F4050F716)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5678[3] = 
{
	LogicalOperator_tD30BF74503BE6556A2EBBA96A165809F4050F716::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5679 = { sizeof (LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5679[3] = 
{
	LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B::get_offset_of_Operator_3(),
	LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B::get_offset_of_Left_4(),
	LogicalExpression_t1C42626E61B8E209BB7A9693B3B0CC17CB59DF5B::get_offset_of_Right_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5680 = { sizeof (MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5680[3] = 
{
	MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6::get_offset_of_Object_3(),
	MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6::get_offset_of_Property_4(),
	MemberExpression_t4B86B0602E269576629DE62F496E2EDA5CD2E0D6::get_offset_of_Computed_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5681 = { sizeof (NewExpression_t6806CEDF7179847105B6CC4CB3076DF69A79B221), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5681[2] = 
{
	NewExpression_t6806CEDF7179847105B6CC4CB3076DF69A79B221::get_offset_of_Callee_3(),
	NewExpression_t6806CEDF7179847105B6CC4CB3076DF69A79B221::get_offset_of_Arguments_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5682 = { sizeof (ObjectExpression_t3FF8030FF79A4557E7D975262E48E9CA9B109022), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5682[1] = 
{
	ObjectExpression_t3FF8030FF79A4557E7D975262E48E9CA9B109022::get_offset_of_Properties_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5683 = { sizeof (Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5683[7] = 
{
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_Body_4(),
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_Comments_5(),
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_Tokens_6(),
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_Errors_7(),
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_Strict_8(),
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_U3CVariableDeclarationsU3Ek__BackingField_9(),
	Program_tB74081ED38A9DDD5BD1AEFAEFF6F785A32A06CFB::get_offset_of_U3CFunctionDeclarationsU3Ek__BackingField_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5684 = { sizeof (PropertyKind_t894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5684[4] = 
{
	PropertyKind_t894D5EFA052D1B5597DC2CFF710FDDCA266D4AE7::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5685 = { sizeof (Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5685[3] = 
{
	Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56::get_offset_of_Kind_3(),
	Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56::get_offset_of_Key_4(),
	Property_t5570E62DC83576B24671CFB29C9FDA71C32EDF56::get_offset_of_Value_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5686 = { sizeof (ReturnStatement_tD75187A0A5A4D5273C527CE9E1A14F536AD78052), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5686[1] = 
{
	ReturnStatement_tD75187A0A5A4D5273C527CE9E1A14F536AD78052::get_offset_of_Argument_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5687 = { sizeof (SequenceExpression_t424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5687[1] = 
{
	SequenceExpression_t424BFDDDB475AF2FF41CCF84BC8A106EF7C064F1::get_offset_of_Expressions_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5688 = { sizeof (Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5688[1] = 
{
	Statement_tDC416B15E62E76A9E29BB8333A2361FEBDF4D6F0::get_offset_of_LabelSet_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5689 = { sizeof (SwitchCase_t88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5689[2] = 
{
	SwitchCase_t88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449::get_offset_of_Test_3(),
	SwitchCase_t88DE9CAC7A7B63B5AB7FD46DA372546CDD25B449::get_offset_of_Consequent_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5690 = { sizeof (SwitchStatement_t7FEFC31902A5B229EC0382398027E79503785F54), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5690[2] = 
{
	SwitchStatement_t7FEFC31902A5B229EC0382398027E79503785F54::get_offset_of_Discriminant_4(),
	SwitchStatement_t7FEFC31902A5B229EC0382398027E79503785F54::get_offset_of_Cases_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5691 = { sizeof (SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5691[3] = 
{
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B::get_offset_of_Type_0(),
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B::get_offset_of_Range_1(),
	SyntaxNode_t7493862A6C53ED276ECECF7246A8DFE2851C880B::get_offset_of_Location_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5692 = { sizeof (SyntaxNodes_tE429806CC3C5D818D51936071CF565AA6B61D4C6)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5692[42] = 
{
	SyntaxNodes_tE429806CC3C5D818D51936071CF565AA6B61D4C6::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5693 = { sizeof (ThisExpression_tF6731B45D761FDE36F0D97FB7A225F7E6146D47A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5694 = { sizeof (ThrowStatement_tEA363AC9843796E5212BF75C4E20496283B67B71), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5694[1] = 
{
	ThrowStatement_tEA363AC9843796E5212BF75C4E20496283B67B71::get_offset_of_Argument_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5695 = { sizeof (TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5695[4] = 
{
	TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5::get_offset_of_Block_4(),
	TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5::get_offset_of_GuardedHandlers_5(),
	TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5::get_offset_of_Handlers_6(),
	TryStatement_tCCB2F3192201292BE16CF157EDB721DE614CABA5::get_offset_of_Finalizer_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5696 = { sizeof (UnaryOperator_t1191338D6F31B2D463708FAB982A7CDB841F0255)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable5696[10] = 
{
	UnaryOperator_t1191338D6F31B2D463708FAB982A7CDB841F0255::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5697 = { sizeof (UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5697[3] = 
{
	UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC::get_offset_of_Operator_3(),
	UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC::get_offset_of_Argument_4(),
	UnaryExpression_tD7FCB17C7B963D96A73F45E7764A0FE95A7D13DC::get_offset_of_Prefix_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5698 = { sizeof (UpdateExpression_t8AD17AC0304ECB931515971F92BAC97FD67AA653), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize5699 = { sizeof (VariableDeclaration_tA8DB09671730A13B048BE8425A3B08C156721952), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable5699[2] = 
{
	VariableDeclaration_tA8DB09671730A13B048BE8425A3B08C156721952::get_offset_of_Declarations_4(),
	VariableDeclaration_tA8DB09671730A13B048BE8425A3B08C156721952::get_offset_of_Kind_5(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
