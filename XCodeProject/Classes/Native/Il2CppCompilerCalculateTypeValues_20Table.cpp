﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.AsyncCallback
struct AsyncCallback_t3F3DA3BEDAEE81DD1D24125DF8EB30E85EE14DA4;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.ArrayList
struct ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4;
// System.Collections.Generic.Dictionary`2<System.String,System.Boolean>
struct Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB;
// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Schema.SchemaNotation>
struct Dictionary_2_t49DF5C20AE5B7EF983AFCFF5867364BA82492C70;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef>
struct Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl>
struct Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaEntity>
struct Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4;
// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.XmlQualifiedName>
struct Dictionary_2_t8F585B0FDF25A3B59F04BFC4617D77AF8AA45201;
// System.Collections.Generic.List`1<System.String>
struct List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3;
// System.Collections.Generic.List`1<System.Xml.IDtdDefaultAttributeInfo>
struct List_1_t2860FD4BD1AB958B9D33F7B140FDCC661ECEA601;
// System.Collections.Generic.List`1<System.Xml.Schema.RangePositionInfo>
struct List_1_t388CC0CE82E700639DA5F7C3343C82038A226785;
// System.Collections.Hashtable
struct Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Collections.SortedList
struct SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E;
// System.Collections.Stack
struct Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643;
// System.DelegateData
struct DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE;
// System.Delegate[]
struct DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.IAsyncResult
struct IAsyncResult_t8E194308510B375B42432981AE5E7488C458D598;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Text.RegularExpressions.Regex
struct Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF;
// System.Text.StringBuilder
struct StringBuilder_t;
// System.Type
struct Type_t;
// System.UInt32[]
struct UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB;
// System.Uri
struct Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// System.Xml.HWStack
struct HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE;
// System.Xml.IValidationEventHandling
struct IValidationEventHandling_tABD39B6B973C0A0DC259D55D8C4179A43ACAB41B;
// System.Xml.NameTable
struct NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C;
// System.Xml.PositionInfo
struct PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684;
// System.Xml.Schema.BitSet
struct BitSet_t0E4C53EC600670A4B74C5671553596978880138C;
// System.Xml.Schema.BitSet[]
struct BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69;
// System.Xml.Schema.CompiledIdentityConstraint[]
struct CompiledIdentityConstraintU5BU5D_t37449FE0550E678ACBF6DC60976D6A23E52021D0;
// System.Xml.Schema.ConstraintStruct[]
struct ConstraintStructU5BU5D_tED6CE54D279C7B76E48DCD7F1365C68C51DC8541;
// System.Xml.Schema.ContentValidator
struct ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA;
// System.Xml.Schema.DatatypeImplementation
struct DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836;
// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[]
struct SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7;
// System.Xml.Schema.DatatypeImplementation[]
struct DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36;
// System.Xml.Schema.DtdValidator/NamespaceManager
struct NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408;
// System.Xml.Schema.FacetsChecker
struct FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6;
// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map[]
struct MapU5BU5D_tAEE3CA984471631DDF5DF6E9567FDC4153FF5A27;
// System.Xml.Schema.IdRefNode
struct IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2;
// System.Xml.Schema.ParticleContentValidator
struct ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D;
// System.Xml.Schema.RestrictionFacets
struct RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4;
// System.Xml.Schema.SchemaAttDef
struct SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4;
// System.Xml.Schema.SchemaBuilder
struct SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7;
// System.Xml.Schema.SchemaElementDecl
struct SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466;
// System.Xml.Schema.SchemaInfo
struct SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41;
// System.Xml.Schema.SchemaNames
struct SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89;
// System.Xml.Schema.ValidationEventArgs
struct ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230;
// System.Xml.Schema.ValidationEventHandler
struct ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF;
// System.Xml.Schema.ValidationState
struct ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B;
// System.Xml.Schema.XdrBuilder/AttributeContent
struct AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290;
// System.Xml.Schema.XdrBuilder/DeclBaseInfo
struct DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B;
// System.Xml.Schema.XdrBuilder/ElementContent
struct ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E;
// System.Xml.Schema.XdrBuilder/GroupContent
struct GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE;
// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[]
struct XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B;
// System.Xml.Schema.XdrBuilder/XdrEntry
struct XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6;
// System.Xml.Schema.XdrBuilder/XdrEntry[]
struct XdrEntryU5BU5D_t56D734CC5235B53EB1D962BE3A20E43D82692200;
// System.Xml.Schema.XmlSchema
struct XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F;
// System.Xml.Schema.XmlSchemaAnyAttribute
struct XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078;
// System.Xml.Schema.XmlSchemaAttribute
struct XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B;
// System.Xml.Schema.XmlSchemaCollection
struct XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5;
// System.Xml.Schema.XmlSchemaCompilationSettings
struct XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC;
// System.Xml.Schema.XmlSchemaDatatype
struct XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550;
// System.Xml.Schema.XmlSchemaElement
struct XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D;
// System.Xml.Schema.XmlSchemaException
struct XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65;
// System.Xml.Schema.XmlSchemaObject
struct XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B;
// System.Xml.Schema.XmlSchemaObjectTable
struct XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833;
// System.Xml.Schema.XmlSchemaPatternFacet
struct XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322;
// System.Xml.Schema.XmlSchemaRedefine
struct XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B;
// System.Xml.Schema.XmlSchemaSet
struct XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F;
// System.Xml.Schema.XmlSchemaSimpleType
struct XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A;
// System.Xml.Schema.XmlSchemaSimpleType[]
struct XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B;
// System.Xml.Schema.XmlSchemaType
struct XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9;
// System.Xml.Schema.XmlValueConverter
struct XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E;
// System.Xml.XmlDocument
struct XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97;
// System.Xml.XmlNameTable
struct XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6;
// System.Xml.XmlNamespaceManager
struct XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F;
// System.Xml.XmlNamespaceManager/NamespaceDeclaration[]
struct NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84;
// System.Xml.XmlNode
struct XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB;
// System.Xml.XmlQualifiedName
struct XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD;
// System.Xml.XmlQualifiedName[]
struct XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B;
// System.Xml.XmlReader
struct XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB;
// System.Xml.XmlReaderSettings
struct XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65;
// System.Xml.XmlResolver
struct XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280;
// System.Xml.XmlValidatingReaderImpl
struct XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E;

struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;



#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#define EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.EventArgs
struct  EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E  : public RuntimeObject
{
public:

public:
};

struct EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields
{
public:
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * ___Empty_0;

public:
	inline static int32_t get_offset_of_Empty_0() { return static_cast<int32_t>(offsetof(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E_StaticFields, ___Empty_0)); }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * get_Empty_0() const { return ___Empty_0; }
	inline EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E ** get_address_of_Empty_0() { return &___Empty_0; }
	inline void set_Empty_0(EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E * value)
	{
		___Empty_0 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EVENTARGS_T8E6CA180BE0E56674C6407011A94BAF7C757352E_H
#ifndef EXCEPTION_T_H
#define EXCEPTION_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Exception
struct  Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((&____className_1), value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((&____message_2), value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((&____data_3), value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((&____innerException_4), value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((&____helpURL_5), value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((&____stackTrace_6), value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((&____stackTraceString_7), value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((&____remoteStackTraceString_8), value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((&____dynamicMethods_10), value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((&____source_12), value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((&____safeSerializationManager_13), value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((&___captured_traces_14), value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((&___native_trace_ips_15), value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_EDILock_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	intptr_t* ___native_trace_ips_15;
};
#endif // EXCEPTION_T_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#define BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseProcessor
struct  BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.BaseProcessor::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_0;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseProcessor::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_1;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.BaseProcessor::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_2;
	// System.Xml.Schema.XmlSchemaCompilationSettings System.Xml.Schema.BaseProcessor::compilationSettings
	XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * ___compilationSettings_3;
	// System.Int32 System.Xml.Schema.BaseProcessor::errorCount
	int32_t ___errorCount_4;
	// System.String System.Xml.Schema.BaseProcessor::NsXml
	String_t* ___NsXml_5;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___nameTable_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_schemaNames_1() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___schemaNames_1)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_1() const { return ___schemaNames_1; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_1() { return &___schemaNames_1; }
	inline void set_schemaNames_1(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_1), value);
	}

	inline static int32_t get_offset_of_eventHandler_2() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___eventHandler_2)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_2() const { return ___eventHandler_2; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_2() { return &___eventHandler_2; }
	inline void set_eventHandler_2(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_2 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_2), value);
	}

	inline static int32_t get_offset_of_compilationSettings_3() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___compilationSettings_3)); }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * get_compilationSettings_3() const { return ___compilationSettings_3; }
	inline XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC ** get_address_of_compilationSettings_3() { return &___compilationSettings_3; }
	inline void set_compilationSettings_3(XmlSchemaCompilationSettings_t33655A7BA800689EC37601FEFD33291F42B8ABBC * value)
	{
		___compilationSettings_3 = value;
		Il2CppCodeGenWriteBarrier((&___compilationSettings_3), value);
	}

	inline static int32_t get_offset_of_errorCount_4() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___errorCount_4)); }
	inline int32_t get_errorCount_4() const { return ___errorCount_4; }
	inline int32_t* get_address_of_errorCount_4() { return &___errorCount_4; }
	inline void set_errorCount_4(int32_t value)
	{
		___errorCount_4 = value;
	}

	inline static int32_t get_offset_of_NsXml_5() { return static_cast<int32_t>(offsetof(BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427, ___NsXml_5)); }
	inline String_t* get_NsXml_5() const { return ___NsXml_5; }
	inline String_t** get_address_of_NsXml_5() { return &___NsXml_5; }
	inline void set_NsXml_5(String_t* value)
	{
		___NsXml_5 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEPROCESSOR_T68F1D18B3D8A79746118A87DF029D5FA8E90D427_H
#ifndef BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#define BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BaseValidator
struct  BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaCollection System.Xml.Schema.BaseValidator::schemaCollection
	XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * ___schemaCollection_0;
	// System.Xml.IValidationEventHandling System.Xml.Schema.BaseValidator::eventHandling
	RuntimeObject* ___eventHandling_1;
	// System.Xml.XmlNameTable System.Xml.Schema.BaseValidator::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.BaseValidator::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_3;
	// System.Xml.PositionInfo System.Xml.Schema.BaseValidator::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_4;
	// System.Xml.XmlResolver System.Xml.Schema.BaseValidator::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_5;
	// System.Uri System.Xml.Schema.BaseValidator::baseUri
	Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * ___baseUri_6;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.BaseValidator::schemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___schemaInfo_7;
	// System.Xml.XmlValidatingReaderImpl System.Xml.Schema.BaseValidator::reader
	XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * ___reader_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.BaseValidator::elementName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___elementName_9;
	// System.Xml.Schema.ValidationState System.Xml.Schema.BaseValidator::context
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * ___context_10;
	// System.Text.StringBuilder System.Xml.Schema.BaseValidator::textValue
	StringBuilder_t * ___textValue_11;
	// System.String System.Xml.Schema.BaseValidator::textString
	String_t* ___textString_12;
	// System.Boolean System.Xml.Schema.BaseValidator::hasSibling
	bool ___hasSibling_13;
	// System.Boolean System.Xml.Schema.BaseValidator::checkDatatype
	bool ___checkDatatype_14;

public:
	inline static int32_t get_offset_of_schemaCollection_0() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaCollection_0)); }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * get_schemaCollection_0() const { return ___schemaCollection_0; }
	inline XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 ** get_address_of_schemaCollection_0() { return &___schemaCollection_0; }
	inline void set_schemaCollection_0(XmlSchemaCollection_tE0656D3C4ABC5A8CC5706871DEAD9E5305E892C5 * value)
	{
		___schemaCollection_0 = value;
		Il2CppCodeGenWriteBarrier((&___schemaCollection_0), value);
	}

	inline static int32_t get_offset_of_eventHandling_1() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___eventHandling_1)); }
	inline RuntimeObject* get_eventHandling_1() const { return ___eventHandling_1; }
	inline RuntimeObject** get_address_of_eventHandling_1() { return &___eventHandling_1; }
	inline void set_eventHandling_1(RuntimeObject* value)
	{
		___eventHandling_1 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandling_1), value);
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_schemaNames_3() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaNames_3)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_3() const { return ___schemaNames_3; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_3() { return &___schemaNames_3; }
	inline void set_schemaNames_3(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_3 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_3), value);
	}

	inline static int32_t get_offset_of_positionInfo_4() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___positionInfo_4)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_4() const { return ___positionInfo_4; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_4() { return &___positionInfo_4; }
	inline void set_positionInfo_4(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_4), value);
	}

	inline static int32_t get_offset_of_xmlResolver_5() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___xmlResolver_5)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_5() const { return ___xmlResolver_5; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_5() { return &___xmlResolver_5; }
	inline void set_xmlResolver_5(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_5 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_5), value);
	}

	inline static int32_t get_offset_of_baseUri_6() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___baseUri_6)); }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * get_baseUri_6() const { return ___baseUri_6; }
	inline Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E ** get_address_of_baseUri_6() { return &___baseUri_6; }
	inline void set_baseUri_6(Uri_t87E4A94B2901F5EEDD18AA72C3DB1B00E672D68E * value)
	{
		___baseUri_6 = value;
		Il2CppCodeGenWriteBarrier((&___baseUri_6), value);
	}

	inline static int32_t get_offset_of_schemaInfo_7() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___schemaInfo_7)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_schemaInfo_7() const { return ___schemaInfo_7; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_schemaInfo_7() { return &___schemaInfo_7; }
	inline void set_schemaInfo_7(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___schemaInfo_7 = value;
		Il2CppCodeGenWriteBarrier((&___schemaInfo_7), value);
	}

	inline static int32_t get_offset_of_reader_8() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___reader_8)); }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * get_reader_8() const { return ___reader_8; }
	inline XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E ** get_address_of_reader_8() { return &___reader_8; }
	inline void set_reader_8(XmlValidatingReaderImpl_t4841FCFEA747D75B9C6B7F4F6794672EC3F1F46E * value)
	{
		___reader_8 = value;
		Il2CppCodeGenWriteBarrier((&___reader_8), value);
	}

	inline static int32_t get_offset_of_elementName_9() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___elementName_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_elementName_9() const { return ___elementName_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_elementName_9() { return &___elementName_9; }
	inline void set_elementName_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___elementName_9 = value;
		Il2CppCodeGenWriteBarrier((&___elementName_9), value);
	}

	inline static int32_t get_offset_of_context_10() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___context_10)); }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * get_context_10() const { return ___context_10; }
	inline ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B ** get_address_of_context_10() { return &___context_10; }
	inline void set_context_10(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B * value)
	{
		___context_10 = value;
		Il2CppCodeGenWriteBarrier((&___context_10), value);
	}

	inline static int32_t get_offset_of_textValue_11() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textValue_11)); }
	inline StringBuilder_t * get_textValue_11() const { return ___textValue_11; }
	inline StringBuilder_t ** get_address_of_textValue_11() { return &___textValue_11; }
	inline void set_textValue_11(StringBuilder_t * value)
	{
		___textValue_11 = value;
		Il2CppCodeGenWriteBarrier((&___textValue_11), value);
	}

	inline static int32_t get_offset_of_textString_12() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___textString_12)); }
	inline String_t* get_textString_12() const { return ___textString_12; }
	inline String_t** get_address_of_textString_12() { return &___textString_12; }
	inline void set_textString_12(String_t* value)
	{
		___textString_12 = value;
		Il2CppCodeGenWriteBarrier((&___textString_12), value);
	}

	inline static int32_t get_offset_of_hasSibling_13() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___hasSibling_13)); }
	inline bool get_hasSibling_13() const { return ___hasSibling_13; }
	inline bool* get_address_of_hasSibling_13() { return &___hasSibling_13; }
	inline void set_hasSibling_13(bool value)
	{
		___hasSibling_13 = value;
	}

	inline static int32_t get_offset_of_checkDatatype_14() { return static_cast<int32_t>(offsetof(BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43, ___checkDatatype_14)); }
	inline bool get_checkDatatype_14() const { return ___checkDatatype_14; }
	inline bool* get_address_of_checkDatatype_14() { return &___checkDatatype_14; }
	inline void set_checkDatatype_14(bool value)
	{
		___checkDatatype_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BASEVALIDATOR_TB0AD5FCB23C61EC8841C153CEB7C77E569246B43_H
#ifndef FACETSCHECKER_T282414FF619686D9D855431F9A01C46BB6FDCBD6_H
#define FACETSCHECKER_T282414FF619686D9D855431F9A01C46BB6FDCBD6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker
struct  FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // FACETSCHECKER_T282414FF619686D9D855431F9A01C46BB6FDCBD6_H
#ifndef REDEFINEENTRY_TC1A902B85FDFB646528A0C10E852DFD952DAE14A_H
#define REDEFINEENTRY_TC1A902B85FDFB646528A0C10E852DFD952DAE14A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RedefineEntry
struct  RedefineEntry_tC1A902B85FDFB646528A0C10E852DFD952DAE14A  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchemaRedefine System.Xml.Schema.RedefineEntry::redefine
	XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B * ___redefine_0;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.RedefineEntry::schemaToUpdate
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schemaToUpdate_1;

public:
	inline static int32_t get_offset_of_redefine_0() { return static_cast<int32_t>(offsetof(RedefineEntry_tC1A902B85FDFB646528A0C10E852DFD952DAE14A, ___redefine_0)); }
	inline XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B * get_redefine_0() const { return ___redefine_0; }
	inline XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B ** get_address_of_redefine_0() { return &___redefine_0; }
	inline void set_redefine_0(XmlSchemaRedefine_t20EC66AD3FB69E2D3269753B97850EA9377EFD6B * value)
	{
		___redefine_0 = value;
		Il2CppCodeGenWriteBarrier((&___redefine_0), value);
	}

	inline static int32_t get_offset_of_schemaToUpdate_1() { return static_cast<int32_t>(offsetof(RedefineEntry_tC1A902B85FDFB646528A0C10E852DFD952DAE14A, ___schemaToUpdate_1)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schemaToUpdate_1() const { return ___schemaToUpdate_1; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schemaToUpdate_1() { return &___schemaToUpdate_1; }
	inline void set_schemaToUpdate_1(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schemaToUpdate_1 = value;
		Il2CppCodeGenWriteBarrier((&___schemaToUpdate_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // REDEFINEENTRY_TC1A902B85FDFB646528A0C10E852DFD952DAE14A_H
#ifndef SCHEMABUILDER_T6B2D813207751A3A8F89AB247E37E371F17450B7_H
#define SCHEMABUILDER_T6B2D813207751A3A8F89AB247E37E371F17450B7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaBuilder
struct  SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMABUILDER_T6B2D813207751A3A8F89AB247E37E371F17450B7_H
#ifndef SCHEMAENTITY_T242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99_H
#define SCHEMAENTITY_T242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaEntity
struct  SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaEntity::qname
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___qname_0;
	// System.String System.Xml.Schema.SchemaEntity::url
	String_t* ___url_1;
	// System.String System.Xml.Schema.SchemaEntity::pubid
	String_t* ___pubid_2;
	// System.String System.Xml.Schema.SchemaEntity::text
	String_t* ___text_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaEntity::ndata
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ndata_4;
	// System.Int32 System.Xml.Schema.SchemaEntity::lineNumber
	int32_t ___lineNumber_5;
	// System.Int32 System.Xml.Schema.SchemaEntity::linePosition
	int32_t ___linePosition_6;
	// System.Boolean System.Xml.Schema.SchemaEntity::isParameter
	bool ___isParameter_7;
	// System.Boolean System.Xml.Schema.SchemaEntity::isExternal
	bool ___isExternal_8;
	// System.Boolean System.Xml.Schema.SchemaEntity::parsingInProgress
	bool ___parsingInProgress_9;
	// System.Boolean System.Xml.Schema.SchemaEntity::isDeclaredInExternal
	bool ___isDeclaredInExternal_10;
	// System.String System.Xml.Schema.SchemaEntity::baseURI
	String_t* ___baseURI_11;
	// System.String System.Xml.Schema.SchemaEntity::declaredURI
	String_t* ___declaredURI_12;

public:
	inline static int32_t get_offset_of_qname_0() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___qname_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_qname_0() const { return ___qname_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_qname_0() { return &___qname_0; }
	inline void set_qname_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___qname_0 = value;
		Il2CppCodeGenWriteBarrier((&___qname_0), value);
	}

	inline static int32_t get_offset_of_url_1() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___url_1)); }
	inline String_t* get_url_1() const { return ___url_1; }
	inline String_t** get_address_of_url_1() { return &___url_1; }
	inline void set_url_1(String_t* value)
	{
		___url_1 = value;
		Il2CppCodeGenWriteBarrier((&___url_1), value);
	}

	inline static int32_t get_offset_of_pubid_2() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___pubid_2)); }
	inline String_t* get_pubid_2() const { return ___pubid_2; }
	inline String_t** get_address_of_pubid_2() { return &___pubid_2; }
	inline void set_pubid_2(String_t* value)
	{
		___pubid_2 = value;
		Il2CppCodeGenWriteBarrier((&___pubid_2), value);
	}

	inline static int32_t get_offset_of_text_3() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___text_3)); }
	inline String_t* get_text_3() const { return ___text_3; }
	inline String_t** get_address_of_text_3() { return &___text_3; }
	inline void set_text_3(String_t* value)
	{
		___text_3 = value;
		Il2CppCodeGenWriteBarrier((&___text_3), value);
	}

	inline static int32_t get_offset_of_ndata_4() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___ndata_4)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ndata_4() const { return ___ndata_4; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ndata_4() { return &___ndata_4; }
	inline void set_ndata_4(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ndata_4 = value;
		Il2CppCodeGenWriteBarrier((&___ndata_4), value);
	}

	inline static int32_t get_offset_of_lineNumber_5() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___lineNumber_5)); }
	inline int32_t get_lineNumber_5() const { return ___lineNumber_5; }
	inline int32_t* get_address_of_lineNumber_5() { return &___lineNumber_5; }
	inline void set_lineNumber_5(int32_t value)
	{
		___lineNumber_5 = value;
	}

	inline static int32_t get_offset_of_linePosition_6() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___linePosition_6)); }
	inline int32_t get_linePosition_6() const { return ___linePosition_6; }
	inline int32_t* get_address_of_linePosition_6() { return &___linePosition_6; }
	inline void set_linePosition_6(int32_t value)
	{
		___linePosition_6 = value;
	}

	inline static int32_t get_offset_of_isParameter_7() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___isParameter_7)); }
	inline bool get_isParameter_7() const { return ___isParameter_7; }
	inline bool* get_address_of_isParameter_7() { return &___isParameter_7; }
	inline void set_isParameter_7(bool value)
	{
		___isParameter_7 = value;
	}

	inline static int32_t get_offset_of_isExternal_8() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___isExternal_8)); }
	inline bool get_isExternal_8() const { return ___isExternal_8; }
	inline bool* get_address_of_isExternal_8() { return &___isExternal_8; }
	inline void set_isExternal_8(bool value)
	{
		___isExternal_8 = value;
	}

	inline static int32_t get_offset_of_parsingInProgress_9() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___parsingInProgress_9)); }
	inline bool get_parsingInProgress_9() const { return ___parsingInProgress_9; }
	inline bool* get_address_of_parsingInProgress_9() { return &___parsingInProgress_9; }
	inline void set_parsingInProgress_9(bool value)
	{
		___parsingInProgress_9 = value;
	}

	inline static int32_t get_offset_of_isDeclaredInExternal_10() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___isDeclaredInExternal_10)); }
	inline bool get_isDeclaredInExternal_10() const { return ___isDeclaredInExternal_10; }
	inline bool* get_address_of_isDeclaredInExternal_10() { return &___isDeclaredInExternal_10; }
	inline void set_isDeclaredInExternal_10(bool value)
	{
		___isDeclaredInExternal_10 = value;
	}

	inline static int32_t get_offset_of_baseURI_11() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___baseURI_11)); }
	inline String_t* get_baseURI_11() const { return ___baseURI_11; }
	inline String_t** get_address_of_baseURI_11() { return &___baseURI_11; }
	inline void set_baseURI_11(String_t* value)
	{
		___baseURI_11 = value;
		Il2CppCodeGenWriteBarrier((&___baseURI_11), value);
	}

	inline static int32_t get_offset_of_declaredURI_12() { return static_cast<int32_t>(offsetof(SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99, ___declaredURI_12)); }
	inline String_t* get_declaredURI_12() const { return ___declaredURI_12; }
	inline String_t** get_address_of_declaredURI_12() { return &___declaredURI_12; }
	inline void set_declaredURI_12(String_t* value)
	{
		___declaredURI_12 = value;
		Il2CppCodeGenWriteBarrier((&___declaredURI_12), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAENTITY_T242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99_H
#ifndef SCHEMANAMES_T15BD91B1C74FC6C9F9825D572D9C2F15726A1E89_H
#define SCHEMANAMES_T15BD91B1C74FC6C9F9825D572D9C2F15726A1E89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNames
struct  SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89  : public RuntimeObject
{
public:
	// System.Xml.XmlNameTable System.Xml.Schema.SchemaNames::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_0;
	// System.String System.Xml.Schema.SchemaNames::NsDataType
	String_t* ___NsDataType_1;
	// System.String System.Xml.Schema.SchemaNames::NsDataTypeAlias
	String_t* ___NsDataTypeAlias_2;
	// System.String System.Xml.Schema.SchemaNames::NsDataTypeOld
	String_t* ___NsDataTypeOld_3;
	// System.String System.Xml.Schema.SchemaNames::NsXml
	String_t* ___NsXml_4;
	// System.String System.Xml.Schema.SchemaNames::NsXmlNs
	String_t* ___NsXmlNs_5;
	// System.String System.Xml.Schema.SchemaNames::NsXdr
	String_t* ___NsXdr_6;
	// System.String System.Xml.Schema.SchemaNames::NsXdrAlias
	String_t* ___NsXdrAlias_7;
	// System.String System.Xml.Schema.SchemaNames::NsXs
	String_t* ___NsXs_8;
	// System.String System.Xml.Schema.SchemaNames::NsXsi
	String_t* ___NsXsi_9;
	// System.String System.Xml.Schema.SchemaNames::XsiType
	String_t* ___XsiType_10;
	// System.String System.Xml.Schema.SchemaNames::XsiNil
	String_t* ___XsiNil_11;
	// System.String System.Xml.Schema.SchemaNames::XsiSchemaLocation
	String_t* ___XsiSchemaLocation_12;
	// System.String System.Xml.Schema.SchemaNames::XsiNoNamespaceSchemaLocation
	String_t* ___XsiNoNamespaceSchemaLocation_13;
	// System.String System.Xml.Schema.SchemaNames::XsdSchema
	String_t* ___XsdSchema_14;
	// System.String System.Xml.Schema.SchemaNames::XdrSchema
	String_t* ___XdrSchema_15;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnPCData
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnPCData_16;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXml
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXml_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXmlNs
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXmlNs_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtDt
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtDt_19;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXmlLang
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXmlLang_20;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnName_21;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnType_22;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMaxOccurs
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnMaxOccurs_23;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMinOccurs
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnMinOccurs_24;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnInfinite
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnInfinite_25;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnModel
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnModel_26;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnOpen
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnOpen_27;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnClosed
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnClosed_28;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnContent
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnContent_29;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMixed
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnMixed_30;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEmpty
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnEmpty_31;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEltOnly
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnEltOnly_32;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnTextOnly
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnTextOnly_33;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnOrder
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnOrder_34;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSeq
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnSeq_35;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnOne
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnOne_36;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMany
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnMany_37;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnRequired
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnRequired_38;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnYes
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnYes_39;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNo
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnNo_40;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnString
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnString_41;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnID
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnID_42;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnIDRef
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnIDRef_43;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnIDRefs
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnIDRefs_44;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEntity
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnEntity_45;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEntities
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnEntities_46;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNmToken
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnNmToken_47;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNmTokens
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnNmTokens_48;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnEnumeration
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnEnumeration_49;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDefault
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDefault_50;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrSchema
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrSchema_51;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrElementType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrElementType_52;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrElement
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrElement_53;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrGroup
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrGroup_54;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrAttributeType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrAttributeType_55;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrAttribute
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrAttribute_56;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrDataType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrDataType_57;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrDescription
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrDescription_58;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrExtends
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrExtends_59;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXdrAliasSchema
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXdrAliasSchema_60;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtType_61;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtValues
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtValues_62;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMaxLength
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtMaxLength_63;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMinLength
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtMinLength_64;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMax
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtMax_65;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMin
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtMin_66;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMinExclusive
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtMinExclusive_67;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDtMaxExclusive
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDtMaxExclusive_68;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnTargetNamespace
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnTargetNamespace_69;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnVersion
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnVersion_70;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnFinalDefault
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnFinalDefault_71;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnBlockDefault
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnBlockDefault_72;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnFixed
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnFixed_73;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnAbstract
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnAbstract_74;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnBlock
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnBlock_75;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSubstitutionGroup
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnSubstitutionGroup_76;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnFinal
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnFinal_77;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNillable
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnNillable_78;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnRef
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnRef_79;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnBase
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnBase_80;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnDerivedBy
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnDerivedBy_81;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnNamespace
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnNamespace_82;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnProcessContents
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnProcessContents_83;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnRefer
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnRefer_84;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnPublic
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnPublic_85;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSystem
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnSystem_86;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSchemaLocation
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnSchemaLocation_87;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnValue
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnValue_88;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnUse
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnUse_89;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnForm
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnForm_90;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnElementFormDefault
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnElementFormDefault_91;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnAttributeFormDefault
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnAttributeFormDefault_92;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnItemType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnItemType_93;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnMemberTypes
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnMemberTypes_94;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXPath
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXPath_95;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSchema
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdSchema_96;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAnnotation
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAnnotation_97;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdInclude
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdInclude_98;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdImport
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdImport_99;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdElement
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdElement_100;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAttribute
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAttribute_101;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAttributeGroup
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAttributeGroup_102;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAnyAttribute
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAnyAttribute_103;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdGroup
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdGroup_104;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAll
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAll_105;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdChoice
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdChoice_106;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSequence
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdSequence_107;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAny
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAny_108;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdNotation
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdNotation_109;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSimpleType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdSimpleType_110;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdComplexType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdComplexType_111;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdUnique
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdUnique_112;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdKey
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdKey_113;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdKeyRef
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdKeyRef_114;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSelector
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdSelector_115;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdField
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdField_116;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMinExclusive
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdMinExclusive_117;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMinInclusive
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdMinInclusive_118;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMaxInclusive
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdMaxInclusive_119;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMaxExclusive
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdMaxExclusive_120;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdTotalDigits
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdTotalDigits_121;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdFractionDigits
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdFractionDigits_122;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdLength
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdLength_123;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMinLength
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdMinLength_124;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdMaxLength
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdMaxLength_125;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdEnumeration
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdEnumeration_126;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdPattern
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdPattern_127;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdDocumentation
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdDocumentation_128;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAppinfo
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAppinfo_129;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnSource
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnSource_130;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdComplexContent
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdComplexContent_131;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdSimpleContent
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdSimpleContent_132;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdRestriction
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdRestriction_133;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdExtension
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdExtension_134;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdUnion
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdUnion_135;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdList
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdList_136;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdWhiteSpace
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdWhiteSpace_137;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdRedefine
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdRedefine_138;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNames::QnXsdAnyType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnXsdAnyType_139;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.SchemaNames::TokenToQName
	XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* ___TokenToQName_140;

public:
	inline static int32_t get_offset_of_nameTable_0() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___nameTable_0)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_0() const { return ___nameTable_0; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_0() { return &___nameTable_0; }
	inline void set_nameTable_0(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_0 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_0), value);
	}

	inline static int32_t get_offset_of_NsDataType_1() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsDataType_1)); }
	inline String_t* get_NsDataType_1() const { return ___NsDataType_1; }
	inline String_t** get_address_of_NsDataType_1() { return &___NsDataType_1; }
	inline void set_NsDataType_1(String_t* value)
	{
		___NsDataType_1 = value;
		Il2CppCodeGenWriteBarrier((&___NsDataType_1), value);
	}

	inline static int32_t get_offset_of_NsDataTypeAlias_2() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsDataTypeAlias_2)); }
	inline String_t* get_NsDataTypeAlias_2() const { return ___NsDataTypeAlias_2; }
	inline String_t** get_address_of_NsDataTypeAlias_2() { return &___NsDataTypeAlias_2; }
	inline void set_NsDataTypeAlias_2(String_t* value)
	{
		___NsDataTypeAlias_2 = value;
		Il2CppCodeGenWriteBarrier((&___NsDataTypeAlias_2), value);
	}

	inline static int32_t get_offset_of_NsDataTypeOld_3() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsDataTypeOld_3)); }
	inline String_t* get_NsDataTypeOld_3() const { return ___NsDataTypeOld_3; }
	inline String_t** get_address_of_NsDataTypeOld_3() { return &___NsDataTypeOld_3; }
	inline void set_NsDataTypeOld_3(String_t* value)
	{
		___NsDataTypeOld_3 = value;
		Il2CppCodeGenWriteBarrier((&___NsDataTypeOld_3), value);
	}

	inline static int32_t get_offset_of_NsXml_4() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsXml_4)); }
	inline String_t* get_NsXml_4() const { return ___NsXml_4; }
	inline String_t** get_address_of_NsXml_4() { return &___NsXml_4; }
	inline void set_NsXml_4(String_t* value)
	{
		___NsXml_4 = value;
		Il2CppCodeGenWriteBarrier((&___NsXml_4), value);
	}

	inline static int32_t get_offset_of_NsXmlNs_5() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsXmlNs_5)); }
	inline String_t* get_NsXmlNs_5() const { return ___NsXmlNs_5; }
	inline String_t** get_address_of_NsXmlNs_5() { return &___NsXmlNs_5; }
	inline void set_NsXmlNs_5(String_t* value)
	{
		___NsXmlNs_5 = value;
		Il2CppCodeGenWriteBarrier((&___NsXmlNs_5), value);
	}

	inline static int32_t get_offset_of_NsXdr_6() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsXdr_6)); }
	inline String_t* get_NsXdr_6() const { return ___NsXdr_6; }
	inline String_t** get_address_of_NsXdr_6() { return &___NsXdr_6; }
	inline void set_NsXdr_6(String_t* value)
	{
		___NsXdr_6 = value;
		Il2CppCodeGenWriteBarrier((&___NsXdr_6), value);
	}

	inline static int32_t get_offset_of_NsXdrAlias_7() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsXdrAlias_7)); }
	inline String_t* get_NsXdrAlias_7() const { return ___NsXdrAlias_7; }
	inline String_t** get_address_of_NsXdrAlias_7() { return &___NsXdrAlias_7; }
	inline void set_NsXdrAlias_7(String_t* value)
	{
		___NsXdrAlias_7 = value;
		Il2CppCodeGenWriteBarrier((&___NsXdrAlias_7), value);
	}

	inline static int32_t get_offset_of_NsXs_8() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsXs_8)); }
	inline String_t* get_NsXs_8() const { return ___NsXs_8; }
	inline String_t** get_address_of_NsXs_8() { return &___NsXs_8; }
	inline void set_NsXs_8(String_t* value)
	{
		___NsXs_8 = value;
		Il2CppCodeGenWriteBarrier((&___NsXs_8), value);
	}

	inline static int32_t get_offset_of_NsXsi_9() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___NsXsi_9)); }
	inline String_t* get_NsXsi_9() const { return ___NsXsi_9; }
	inline String_t** get_address_of_NsXsi_9() { return &___NsXsi_9; }
	inline void set_NsXsi_9(String_t* value)
	{
		___NsXsi_9 = value;
		Il2CppCodeGenWriteBarrier((&___NsXsi_9), value);
	}

	inline static int32_t get_offset_of_XsiType_10() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___XsiType_10)); }
	inline String_t* get_XsiType_10() const { return ___XsiType_10; }
	inline String_t** get_address_of_XsiType_10() { return &___XsiType_10; }
	inline void set_XsiType_10(String_t* value)
	{
		___XsiType_10 = value;
		Il2CppCodeGenWriteBarrier((&___XsiType_10), value);
	}

	inline static int32_t get_offset_of_XsiNil_11() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___XsiNil_11)); }
	inline String_t* get_XsiNil_11() const { return ___XsiNil_11; }
	inline String_t** get_address_of_XsiNil_11() { return &___XsiNil_11; }
	inline void set_XsiNil_11(String_t* value)
	{
		___XsiNil_11 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNil_11), value);
	}

	inline static int32_t get_offset_of_XsiSchemaLocation_12() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___XsiSchemaLocation_12)); }
	inline String_t* get_XsiSchemaLocation_12() const { return ___XsiSchemaLocation_12; }
	inline String_t** get_address_of_XsiSchemaLocation_12() { return &___XsiSchemaLocation_12; }
	inline void set_XsiSchemaLocation_12(String_t* value)
	{
		___XsiSchemaLocation_12 = value;
		Il2CppCodeGenWriteBarrier((&___XsiSchemaLocation_12), value);
	}

	inline static int32_t get_offset_of_XsiNoNamespaceSchemaLocation_13() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___XsiNoNamespaceSchemaLocation_13)); }
	inline String_t* get_XsiNoNamespaceSchemaLocation_13() const { return ___XsiNoNamespaceSchemaLocation_13; }
	inline String_t** get_address_of_XsiNoNamespaceSchemaLocation_13() { return &___XsiNoNamespaceSchemaLocation_13; }
	inline void set_XsiNoNamespaceSchemaLocation_13(String_t* value)
	{
		___XsiNoNamespaceSchemaLocation_13 = value;
		Il2CppCodeGenWriteBarrier((&___XsiNoNamespaceSchemaLocation_13), value);
	}

	inline static int32_t get_offset_of_XsdSchema_14() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___XsdSchema_14)); }
	inline String_t* get_XsdSchema_14() const { return ___XsdSchema_14; }
	inline String_t** get_address_of_XsdSchema_14() { return &___XsdSchema_14; }
	inline void set_XsdSchema_14(String_t* value)
	{
		___XsdSchema_14 = value;
		Il2CppCodeGenWriteBarrier((&___XsdSchema_14), value);
	}

	inline static int32_t get_offset_of_XdrSchema_15() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___XdrSchema_15)); }
	inline String_t* get_XdrSchema_15() const { return ___XdrSchema_15; }
	inline String_t** get_address_of_XdrSchema_15() { return &___XdrSchema_15; }
	inline void set_XdrSchema_15(String_t* value)
	{
		___XdrSchema_15 = value;
		Il2CppCodeGenWriteBarrier((&___XdrSchema_15), value);
	}

	inline static int32_t get_offset_of_QnPCData_16() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnPCData_16)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnPCData_16() const { return ___QnPCData_16; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnPCData_16() { return &___QnPCData_16; }
	inline void set_QnPCData_16(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnPCData_16 = value;
		Il2CppCodeGenWriteBarrier((&___QnPCData_16), value);
	}

	inline static int32_t get_offset_of_QnXml_17() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXml_17)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXml_17() const { return ___QnXml_17; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXml_17() { return &___QnXml_17; }
	inline void set_QnXml_17(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXml_17 = value;
		Il2CppCodeGenWriteBarrier((&___QnXml_17), value);
	}

	inline static int32_t get_offset_of_QnXmlNs_18() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXmlNs_18)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXmlNs_18() const { return ___QnXmlNs_18; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXmlNs_18() { return &___QnXmlNs_18; }
	inline void set_QnXmlNs_18(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXmlNs_18 = value;
		Il2CppCodeGenWriteBarrier((&___QnXmlNs_18), value);
	}

	inline static int32_t get_offset_of_QnDtDt_19() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtDt_19)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtDt_19() const { return ___QnDtDt_19; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtDt_19() { return &___QnDtDt_19; }
	inline void set_QnDtDt_19(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtDt_19 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtDt_19), value);
	}

	inline static int32_t get_offset_of_QnXmlLang_20() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXmlLang_20)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXmlLang_20() const { return ___QnXmlLang_20; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXmlLang_20() { return &___QnXmlLang_20; }
	inline void set_QnXmlLang_20(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXmlLang_20 = value;
		Il2CppCodeGenWriteBarrier((&___QnXmlLang_20), value);
	}

	inline static int32_t get_offset_of_QnName_21() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnName_21)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnName_21() const { return ___QnName_21; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnName_21() { return &___QnName_21; }
	inline void set_QnName_21(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnName_21 = value;
		Il2CppCodeGenWriteBarrier((&___QnName_21), value);
	}

	inline static int32_t get_offset_of_QnType_22() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnType_22)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnType_22() const { return ___QnType_22; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnType_22() { return &___QnType_22; }
	inline void set_QnType_22(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnType_22 = value;
		Il2CppCodeGenWriteBarrier((&___QnType_22), value);
	}

	inline static int32_t get_offset_of_QnMaxOccurs_23() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnMaxOccurs_23)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnMaxOccurs_23() const { return ___QnMaxOccurs_23; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnMaxOccurs_23() { return &___QnMaxOccurs_23; }
	inline void set_QnMaxOccurs_23(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnMaxOccurs_23 = value;
		Il2CppCodeGenWriteBarrier((&___QnMaxOccurs_23), value);
	}

	inline static int32_t get_offset_of_QnMinOccurs_24() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnMinOccurs_24)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnMinOccurs_24() const { return ___QnMinOccurs_24; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnMinOccurs_24() { return &___QnMinOccurs_24; }
	inline void set_QnMinOccurs_24(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnMinOccurs_24 = value;
		Il2CppCodeGenWriteBarrier((&___QnMinOccurs_24), value);
	}

	inline static int32_t get_offset_of_QnInfinite_25() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnInfinite_25)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnInfinite_25() const { return ___QnInfinite_25; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnInfinite_25() { return &___QnInfinite_25; }
	inline void set_QnInfinite_25(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnInfinite_25 = value;
		Il2CppCodeGenWriteBarrier((&___QnInfinite_25), value);
	}

	inline static int32_t get_offset_of_QnModel_26() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnModel_26)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnModel_26() const { return ___QnModel_26; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnModel_26() { return &___QnModel_26; }
	inline void set_QnModel_26(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnModel_26 = value;
		Il2CppCodeGenWriteBarrier((&___QnModel_26), value);
	}

	inline static int32_t get_offset_of_QnOpen_27() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnOpen_27)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnOpen_27() const { return ___QnOpen_27; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnOpen_27() { return &___QnOpen_27; }
	inline void set_QnOpen_27(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnOpen_27 = value;
		Il2CppCodeGenWriteBarrier((&___QnOpen_27), value);
	}

	inline static int32_t get_offset_of_QnClosed_28() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnClosed_28)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnClosed_28() const { return ___QnClosed_28; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnClosed_28() { return &___QnClosed_28; }
	inline void set_QnClosed_28(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnClosed_28 = value;
		Il2CppCodeGenWriteBarrier((&___QnClosed_28), value);
	}

	inline static int32_t get_offset_of_QnContent_29() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnContent_29)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnContent_29() const { return ___QnContent_29; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnContent_29() { return &___QnContent_29; }
	inline void set_QnContent_29(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnContent_29 = value;
		Il2CppCodeGenWriteBarrier((&___QnContent_29), value);
	}

	inline static int32_t get_offset_of_QnMixed_30() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnMixed_30)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnMixed_30() const { return ___QnMixed_30; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnMixed_30() { return &___QnMixed_30; }
	inline void set_QnMixed_30(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnMixed_30 = value;
		Il2CppCodeGenWriteBarrier((&___QnMixed_30), value);
	}

	inline static int32_t get_offset_of_QnEmpty_31() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnEmpty_31)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnEmpty_31() const { return ___QnEmpty_31; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnEmpty_31() { return &___QnEmpty_31; }
	inline void set_QnEmpty_31(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnEmpty_31 = value;
		Il2CppCodeGenWriteBarrier((&___QnEmpty_31), value);
	}

	inline static int32_t get_offset_of_QnEltOnly_32() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnEltOnly_32)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnEltOnly_32() const { return ___QnEltOnly_32; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnEltOnly_32() { return &___QnEltOnly_32; }
	inline void set_QnEltOnly_32(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnEltOnly_32 = value;
		Il2CppCodeGenWriteBarrier((&___QnEltOnly_32), value);
	}

	inline static int32_t get_offset_of_QnTextOnly_33() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnTextOnly_33)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnTextOnly_33() const { return ___QnTextOnly_33; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnTextOnly_33() { return &___QnTextOnly_33; }
	inline void set_QnTextOnly_33(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnTextOnly_33 = value;
		Il2CppCodeGenWriteBarrier((&___QnTextOnly_33), value);
	}

	inline static int32_t get_offset_of_QnOrder_34() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnOrder_34)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnOrder_34() const { return ___QnOrder_34; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnOrder_34() { return &___QnOrder_34; }
	inline void set_QnOrder_34(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnOrder_34 = value;
		Il2CppCodeGenWriteBarrier((&___QnOrder_34), value);
	}

	inline static int32_t get_offset_of_QnSeq_35() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnSeq_35)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnSeq_35() const { return ___QnSeq_35; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnSeq_35() { return &___QnSeq_35; }
	inline void set_QnSeq_35(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnSeq_35 = value;
		Il2CppCodeGenWriteBarrier((&___QnSeq_35), value);
	}

	inline static int32_t get_offset_of_QnOne_36() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnOne_36)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnOne_36() const { return ___QnOne_36; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnOne_36() { return &___QnOne_36; }
	inline void set_QnOne_36(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnOne_36 = value;
		Il2CppCodeGenWriteBarrier((&___QnOne_36), value);
	}

	inline static int32_t get_offset_of_QnMany_37() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnMany_37)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnMany_37() const { return ___QnMany_37; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnMany_37() { return &___QnMany_37; }
	inline void set_QnMany_37(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnMany_37 = value;
		Il2CppCodeGenWriteBarrier((&___QnMany_37), value);
	}

	inline static int32_t get_offset_of_QnRequired_38() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnRequired_38)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnRequired_38() const { return ___QnRequired_38; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnRequired_38() { return &___QnRequired_38; }
	inline void set_QnRequired_38(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnRequired_38 = value;
		Il2CppCodeGenWriteBarrier((&___QnRequired_38), value);
	}

	inline static int32_t get_offset_of_QnYes_39() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnYes_39)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnYes_39() const { return ___QnYes_39; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnYes_39() { return &___QnYes_39; }
	inline void set_QnYes_39(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnYes_39 = value;
		Il2CppCodeGenWriteBarrier((&___QnYes_39), value);
	}

	inline static int32_t get_offset_of_QnNo_40() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnNo_40)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnNo_40() const { return ___QnNo_40; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnNo_40() { return &___QnNo_40; }
	inline void set_QnNo_40(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnNo_40 = value;
		Il2CppCodeGenWriteBarrier((&___QnNo_40), value);
	}

	inline static int32_t get_offset_of_QnString_41() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnString_41)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnString_41() const { return ___QnString_41; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnString_41() { return &___QnString_41; }
	inline void set_QnString_41(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnString_41 = value;
		Il2CppCodeGenWriteBarrier((&___QnString_41), value);
	}

	inline static int32_t get_offset_of_QnID_42() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnID_42)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnID_42() const { return ___QnID_42; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnID_42() { return &___QnID_42; }
	inline void set_QnID_42(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnID_42 = value;
		Il2CppCodeGenWriteBarrier((&___QnID_42), value);
	}

	inline static int32_t get_offset_of_QnIDRef_43() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnIDRef_43)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnIDRef_43() const { return ___QnIDRef_43; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnIDRef_43() { return &___QnIDRef_43; }
	inline void set_QnIDRef_43(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnIDRef_43 = value;
		Il2CppCodeGenWriteBarrier((&___QnIDRef_43), value);
	}

	inline static int32_t get_offset_of_QnIDRefs_44() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnIDRefs_44)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnIDRefs_44() const { return ___QnIDRefs_44; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnIDRefs_44() { return &___QnIDRefs_44; }
	inline void set_QnIDRefs_44(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnIDRefs_44 = value;
		Il2CppCodeGenWriteBarrier((&___QnIDRefs_44), value);
	}

	inline static int32_t get_offset_of_QnEntity_45() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnEntity_45)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnEntity_45() const { return ___QnEntity_45; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnEntity_45() { return &___QnEntity_45; }
	inline void set_QnEntity_45(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnEntity_45 = value;
		Il2CppCodeGenWriteBarrier((&___QnEntity_45), value);
	}

	inline static int32_t get_offset_of_QnEntities_46() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnEntities_46)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnEntities_46() const { return ___QnEntities_46; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnEntities_46() { return &___QnEntities_46; }
	inline void set_QnEntities_46(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnEntities_46 = value;
		Il2CppCodeGenWriteBarrier((&___QnEntities_46), value);
	}

	inline static int32_t get_offset_of_QnNmToken_47() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnNmToken_47)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnNmToken_47() const { return ___QnNmToken_47; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnNmToken_47() { return &___QnNmToken_47; }
	inline void set_QnNmToken_47(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnNmToken_47 = value;
		Il2CppCodeGenWriteBarrier((&___QnNmToken_47), value);
	}

	inline static int32_t get_offset_of_QnNmTokens_48() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnNmTokens_48)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnNmTokens_48() const { return ___QnNmTokens_48; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnNmTokens_48() { return &___QnNmTokens_48; }
	inline void set_QnNmTokens_48(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnNmTokens_48 = value;
		Il2CppCodeGenWriteBarrier((&___QnNmTokens_48), value);
	}

	inline static int32_t get_offset_of_QnEnumeration_49() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnEnumeration_49)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnEnumeration_49() const { return ___QnEnumeration_49; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnEnumeration_49() { return &___QnEnumeration_49; }
	inline void set_QnEnumeration_49(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnEnumeration_49 = value;
		Il2CppCodeGenWriteBarrier((&___QnEnumeration_49), value);
	}

	inline static int32_t get_offset_of_QnDefault_50() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDefault_50)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDefault_50() const { return ___QnDefault_50; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDefault_50() { return &___QnDefault_50; }
	inline void set_QnDefault_50(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDefault_50 = value;
		Il2CppCodeGenWriteBarrier((&___QnDefault_50), value);
	}

	inline static int32_t get_offset_of_QnXdrSchema_51() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrSchema_51)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrSchema_51() const { return ___QnXdrSchema_51; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrSchema_51() { return &___QnXdrSchema_51; }
	inline void set_QnXdrSchema_51(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrSchema_51 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrSchema_51), value);
	}

	inline static int32_t get_offset_of_QnXdrElementType_52() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrElementType_52)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrElementType_52() const { return ___QnXdrElementType_52; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrElementType_52() { return &___QnXdrElementType_52; }
	inline void set_QnXdrElementType_52(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrElementType_52 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrElementType_52), value);
	}

	inline static int32_t get_offset_of_QnXdrElement_53() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrElement_53)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrElement_53() const { return ___QnXdrElement_53; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrElement_53() { return &___QnXdrElement_53; }
	inline void set_QnXdrElement_53(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrElement_53 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrElement_53), value);
	}

	inline static int32_t get_offset_of_QnXdrGroup_54() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrGroup_54)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrGroup_54() const { return ___QnXdrGroup_54; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrGroup_54() { return &___QnXdrGroup_54; }
	inline void set_QnXdrGroup_54(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrGroup_54 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrGroup_54), value);
	}

	inline static int32_t get_offset_of_QnXdrAttributeType_55() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrAttributeType_55)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrAttributeType_55() const { return ___QnXdrAttributeType_55; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrAttributeType_55() { return &___QnXdrAttributeType_55; }
	inline void set_QnXdrAttributeType_55(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrAttributeType_55 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrAttributeType_55), value);
	}

	inline static int32_t get_offset_of_QnXdrAttribute_56() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrAttribute_56)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrAttribute_56() const { return ___QnXdrAttribute_56; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrAttribute_56() { return &___QnXdrAttribute_56; }
	inline void set_QnXdrAttribute_56(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrAttribute_56 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrAttribute_56), value);
	}

	inline static int32_t get_offset_of_QnXdrDataType_57() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrDataType_57)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrDataType_57() const { return ___QnXdrDataType_57; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrDataType_57() { return &___QnXdrDataType_57; }
	inline void set_QnXdrDataType_57(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrDataType_57 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrDataType_57), value);
	}

	inline static int32_t get_offset_of_QnXdrDescription_58() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrDescription_58)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrDescription_58() const { return ___QnXdrDescription_58; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrDescription_58() { return &___QnXdrDescription_58; }
	inline void set_QnXdrDescription_58(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrDescription_58 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrDescription_58), value);
	}

	inline static int32_t get_offset_of_QnXdrExtends_59() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrExtends_59)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrExtends_59() const { return ___QnXdrExtends_59; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrExtends_59() { return &___QnXdrExtends_59; }
	inline void set_QnXdrExtends_59(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrExtends_59 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrExtends_59), value);
	}

	inline static int32_t get_offset_of_QnXdrAliasSchema_60() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXdrAliasSchema_60)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXdrAliasSchema_60() const { return ___QnXdrAliasSchema_60; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXdrAliasSchema_60() { return &___QnXdrAliasSchema_60; }
	inline void set_QnXdrAliasSchema_60(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXdrAliasSchema_60 = value;
		Il2CppCodeGenWriteBarrier((&___QnXdrAliasSchema_60), value);
	}

	inline static int32_t get_offset_of_QnDtType_61() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtType_61)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtType_61() const { return ___QnDtType_61; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtType_61() { return &___QnDtType_61; }
	inline void set_QnDtType_61(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtType_61 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtType_61), value);
	}

	inline static int32_t get_offset_of_QnDtValues_62() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtValues_62)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtValues_62() const { return ___QnDtValues_62; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtValues_62() { return &___QnDtValues_62; }
	inline void set_QnDtValues_62(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtValues_62 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtValues_62), value);
	}

	inline static int32_t get_offset_of_QnDtMaxLength_63() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtMaxLength_63)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtMaxLength_63() const { return ___QnDtMaxLength_63; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtMaxLength_63() { return &___QnDtMaxLength_63; }
	inline void set_QnDtMaxLength_63(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtMaxLength_63 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtMaxLength_63), value);
	}

	inline static int32_t get_offset_of_QnDtMinLength_64() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtMinLength_64)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtMinLength_64() const { return ___QnDtMinLength_64; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtMinLength_64() { return &___QnDtMinLength_64; }
	inline void set_QnDtMinLength_64(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtMinLength_64 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtMinLength_64), value);
	}

	inline static int32_t get_offset_of_QnDtMax_65() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtMax_65)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtMax_65() const { return ___QnDtMax_65; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtMax_65() { return &___QnDtMax_65; }
	inline void set_QnDtMax_65(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtMax_65 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtMax_65), value);
	}

	inline static int32_t get_offset_of_QnDtMin_66() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtMin_66)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtMin_66() const { return ___QnDtMin_66; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtMin_66() { return &___QnDtMin_66; }
	inline void set_QnDtMin_66(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtMin_66 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtMin_66), value);
	}

	inline static int32_t get_offset_of_QnDtMinExclusive_67() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtMinExclusive_67)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtMinExclusive_67() const { return ___QnDtMinExclusive_67; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtMinExclusive_67() { return &___QnDtMinExclusive_67; }
	inline void set_QnDtMinExclusive_67(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtMinExclusive_67 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtMinExclusive_67), value);
	}

	inline static int32_t get_offset_of_QnDtMaxExclusive_68() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDtMaxExclusive_68)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDtMaxExclusive_68() const { return ___QnDtMaxExclusive_68; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDtMaxExclusive_68() { return &___QnDtMaxExclusive_68; }
	inline void set_QnDtMaxExclusive_68(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDtMaxExclusive_68 = value;
		Il2CppCodeGenWriteBarrier((&___QnDtMaxExclusive_68), value);
	}

	inline static int32_t get_offset_of_QnTargetNamespace_69() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnTargetNamespace_69)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnTargetNamespace_69() const { return ___QnTargetNamespace_69; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnTargetNamespace_69() { return &___QnTargetNamespace_69; }
	inline void set_QnTargetNamespace_69(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnTargetNamespace_69 = value;
		Il2CppCodeGenWriteBarrier((&___QnTargetNamespace_69), value);
	}

	inline static int32_t get_offset_of_QnVersion_70() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnVersion_70)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnVersion_70() const { return ___QnVersion_70; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnVersion_70() { return &___QnVersion_70; }
	inline void set_QnVersion_70(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnVersion_70 = value;
		Il2CppCodeGenWriteBarrier((&___QnVersion_70), value);
	}

	inline static int32_t get_offset_of_QnFinalDefault_71() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnFinalDefault_71)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnFinalDefault_71() const { return ___QnFinalDefault_71; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnFinalDefault_71() { return &___QnFinalDefault_71; }
	inline void set_QnFinalDefault_71(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnFinalDefault_71 = value;
		Il2CppCodeGenWriteBarrier((&___QnFinalDefault_71), value);
	}

	inline static int32_t get_offset_of_QnBlockDefault_72() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnBlockDefault_72)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnBlockDefault_72() const { return ___QnBlockDefault_72; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnBlockDefault_72() { return &___QnBlockDefault_72; }
	inline void set_QnBlockDefault_72(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnBlockDefault_72 = value;
		Il2CppCodeGenWriteBarrier((&___QnBlockDefault_72), value);
	}

	inline static int32_t get_offset_of_QnFixed_73() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnFixed_73)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnFixed_73() const { return ___QnFixed_73; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnFixed_73() { return &___QnFixed_73; }
	inline void set_QnFixed_73(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnFixed_73 = value;
		Il2CppCodeGenWriteBarrier((&___QnFixed_73), value);
	}

	inline static int32_t get_offset_of_QnAbstract_74() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnAbstract_74)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnAbstract_74() const { return ___QnAbstract_74; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnAbstract_74() { return &___QnAbstract_74; }
	inline void set_QnAbstract_74(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnAbstract_74 = value;
		Il2CppCodeGenWriteBarrier((&___QnAbstract_74), value);
	}

	inline static int32_t get_offset_of_QnBlock_75() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnBlock_75)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnBlock_75() const { return ___QnBlock_75; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnBlock_75() { return &___QnBlock_75; }
	inline void set_QnBlock_75(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnBlock_75 = value;
		Il2CppCodeGenWriteBarrier((&___QnBlock_75), value);
	}

	inline static int32_t get_offset_of_QnSubstitutionGroup_76() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnSubstitutionGroup_76)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnSubstitutionGroup_76() const { return ___QnSubstitutionGroup_76; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnSubstitutionGroup_76() { return &___QnSubstitutionGroup_76; }
	inline void set_QnSubstitutionGroup_76(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnSubstitutionGroup_76 = value;
		Il2CppCodeGenWriteBarrier((&___QnSubstitutionGroup_76), value);
	}

	inline static int32_t get_offset_of_QnFinal_77() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnFinal_77)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnFinal_77() const { return ___QnFinal_77; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnFinal_77() { return &___QnFinal_77; }
	inline void set_QnFinal_77(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnFinal_77 = value;
		Il2CppCodeGenWriteBarrier((&___QnFinal_77), value);
	}

	inline static int32_t get_offset_of_QnNillable_78() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnNillable_78)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnNillable_78() const { return ___QnNillable_78; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnNillable_78() { return &___QnNillable_78; }
	inline void set_QnNillable_78(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnNillable_78 = value;
		Il2CppCodeGenWriteBarrier((&___QnNillable_78), value);
	}

	inline static int32_t get_offset_of_QnRef_79() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnRef_79)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnRef_79() const { return ___QnRef_79; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnRef_79() { return &___QnRef_79; }
	inline void set_QnRef_79(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnRef_79 = value;
		Il2CppCodeGenWriteBarrier((&___QnRef_79), value);
	}

	inline static int32_t get_offset_of_QnBase_80() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnBase_80)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnBase_80() const { return ___QnBase_80; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnBase_80() { return &___QnBase_80; }
	inline void set_QnBase_80(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnBase_80 = value;
		Il2CppCodeGenWriteBarrier((&___QnBase_80), value);
	}

	inline static int32_t get_offset_of_QnDerivedBy_81() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnDerivedBy_81)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnDerivedBy_81() const { return ___QnDerivedBy_81; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnDerivedBy_81() { return &___QnDerivedBy_81; }
	inline void set_QnDerivedBy_81(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnDerivedBy_81 = value;
		Il2CppCodeGenWriteBarrier((&___QnDerivedBy_81), value);
	}

	inline static int32_t get_offset_of_QnNamespace_82() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnNamespace_82)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnNamespace_82() const { return ___QnNamespace_82; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnNamespace_82() { return &___QnNamespace_82; }
	inline void set_QnNamespace_82(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnNamespace_82 = value;
		Il2CppCodeGenWriteBarrier((&___QnNamespace_82), value);
	}

	inline static int32_t get_offset_of_QnProcessContents_83() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnProcessContents_83)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnProcessContents_83() const { return ___QnProcessContents_83; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnProcessContents_83() { return &___QnProcessContents_83; }
	inline void set_QnProcessContents_83(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnProcessContents_83 = value;
		Il2CppCodeGenWriteBarrier((&___QnProcessContents_83), value);
	}

	inline static int32_t get_offset_of_QnRefer_84() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnRefer_84)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnRefer_84() const { return ___QnRefer_84; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnRefer_84() { return &___QnRefer_84; }
	inline void set_QnRefer_84(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnRefer_84 = value;
		Il2CppCodeGenWriteBarrier((&___QnRefer_84), value);
	}

	inline static int32_t get_offset_of_QnPublic_85() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnPublic_85)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnPublic_85() const { return ___QnPublic_85; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnPublic_85() { return &___QnPublic_85; }
	inline void set_QnPublic_85(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnPublic_85 = value;
		Il2CppCodeGenWriteBarrier((&___QnPublic_85), value);
	}

	inline static int32_t get_offset_of_QnSystem_86() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnSystem_86)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnSystem_86() const { return ___QnSystem_86; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnSystem_86() { return &___QnSystem_86; }
	inline void set_QnSystem_86(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnSystem_86 = value;
		Il2CppCodeGenWriteBarrier((&___QnSystem_86), value);
	}

	inline static int32_t get_offset_of_QnSchemaLocation_87() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnSchemaLocation_87)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnSchemaLocation_87() const { return ___QnSchemaLocation_87; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnSchemaLocation_87() { return &___QnSchemaLocation_87; }
	inline void set_QnSchemaLocation_87(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnSchemaLocation_87 = value;
		Il2CppCodeGenWriteBarrier((&___QnSchemaLocation_87), value);
	}

	inline static int32_t get_offset_of_QnValue_88() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnValue_88)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnValue_88() const { return ___QnValue_88; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnValue_88() { return &___QnValue_88; }
	inline void set_QnValue_88(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnValue_88 = value;
		Il2CppCodeGenWriteBarrier((&___QnValue_88), value);
	}

	inline static int32_t get_offset_of_QnUse_89() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnUse_89)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnUse_89() const { return ___QnUse_89; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnUse_89() { return &___QnUse_89; }
	inline void set_QnUse_89(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnUse_89 = value;
		Il2CppCodeGenWriteBarrier((&___QnUse_89), value);
	}

	inline static int32_t get_offset_of_QnForm_90() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnForm_90)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnForm_90() const { return ___QnForm_90; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnForm_90() { return &___QnForm_90; }
	inline void set_QnForm_90(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnForm_90 = value;
		Il2CppCodeGenWriteBarrier((&___QnForm_90), value);
	}

	inline static int32_t get_offset_of_QnElementFormDefault_91() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnElementFormDefault_91)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnElementFormDefault_91() const { return ___QnElementFormDefault_91; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnElementFormDefault_91() { return &___QnElementFormDefault_91; }
	inline void set_QnElementFormDefault_91(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnElementFormDefault_91 = value;
		Il2CppCodeGenWriteBarrier((&___QnElementFormDefault_91), value);
	}

	inline static int32_t get_offset_of_QnAttributeFormDefault_92() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnAttributeFormDefault_92)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnAttributeFormDefault_92() const { return ___QnAttributeFormDefault_92; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnAttributeFormDefault_92() { return &___QnAttributeFormDefault_92; }
	inline void set_QnAttributeFormDefault_92(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnAttributeFormDefault_92 = value;
		Il2CppCodeGenWriteBarrier((&___QnAttributeFormDefault_92), value);
	}

	inline static int32_t get_offset_of_QnItemType_93() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnItemType_93)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnItemType_93() const { return ___QnItemType_93; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnItemType_93() { return &___QnItemType_93; }
	inline void set_QnItemType_93(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnItemType_93 = value;
		Il2CppCodeGenWriteBarrier((&___QnItemType_93), value);
	}

	inline static int32_t get_offset_of_QnMemberTypes_94() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnMemberTypes_94)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnMemberTypes_94() const { return ___QnMemberTypes_94; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnMemberTypes_94() { return &___QnMemberTypes_94; }
	inline void set_QnMemberTypes_94(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnMemberTypes_94 = value;
		Il2CppCodeGenWriteBarrier((&___QnMemberTypes_94), value);
	}

	inline static int32_t get_offset_of_QnXPath_95() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXPath_95)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXPath_95() const { return ___QnXPath_95; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXPath_95() { return &___QnXPath_95; }
	inline void set_QnXPath_95(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXPath_95 = value;
		Il2CppCodeGenWriteBarrier((&___QnXPath_95), value);
	}

	inline static int32_t get_offset_of_QnXsdSchema_96() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdSchema_96)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdSchema_96() const { return ___QnXsdSchema_96; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdSchema_96() { return &___QnXsdSchema_96; }
	inline void set_QnXsdSchema_96(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdSchema_96 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdSchema_96), value);
	}

	inline static int32_t get_offset_of_QnXsdAnnotation_97() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAnnotation_97)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAnnotation_97() const { return ___QnXsdAnnotation_97; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAnnotation_97() { return &___QnXsdAnnotation_97; }
	inline void set_QnXsdAnnotation_97(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAnnotation_97 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAnnotation_97), value);
	}

	inline static int32_t get_offset_of_QnXsdInclude_98() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdInclude_98)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdInclude_98() const { return ___QnXsdInclude_98; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdInclude_98() { return &___QnXsdInclude_98; }
	inline void set_QnXsdInclude_98(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdInclude_98 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdInclude_98), value);
	}

	inline static int32_t get_offset_of_QnXsdImport_99() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdImport_99)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdImport_99() const { return ___QnXsdImport_99; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdImport_99() { return &___QnXsdImport_99; }
	inline void set_QnXsdImport_99(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdImport_99 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdImport_99), value);
	}

	inline static int32_t get_offset_of_QnXsdElement_100() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdElement_100)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdElement_100() const { return ___QnXsdElement_100; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdElement_100() { return &___QnXsdElement_100; }
	inline void set_QnXsdElement_100(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdElement_100 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdElement_100), value);
	}

	inline static int32_t get_offset_of_QnXsdAttribute_101() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAttribute_101)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAttribute_101() const { return ___QnXsdAttribute_101; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAttribute_101() { return &___QnXsdAttribute_101; }
	inline void set_QnXsdAttribute_101(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAttribute_101 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAttribute_101), value);
	}

	inline static int32_t get_offset_of_QnXsdAttributeGroup_102() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAttributeGroup_102)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAttributeGroup_102() const { return ___QnXsdAttributeGroup_102; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAttributeGroup_102() { return &___QnXsdAttributeGroup_102; }
	inline void set_QnXsdAttributeGroup_102(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAttributeGroup_102 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAttributeGroup_102), value);
	}

	inline static int32_t get_offset_of_QnXsdAnyAttribute_103() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAnyAttribute_103)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAnyAttribute_103() const { return ___QnXsdAnyAttribute_103; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAnyAttribute_103() { return &___QnXsdAnyAttribute_103; }
	inline void set_QnXsdAnyAttribute_103(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAnyAttribute_103 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAnyAttribute_103), value);
	}

	inline static int32_t get_offset_of_QnXsdGroup_104() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdGroup_104)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdGroup_104() const { return ___QnXsdGroup_104; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdGroup_104() { return &___QnXsdGroup_104; }
	inline void set_QnXsdGroup_104(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdGroup_104 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdGroup_104), value);
	}

	inline static int32_t get_offset_of_QnXsdAll_105() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAll_105)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAll_105() const { return ___QnXsdAll_105; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAll_105() { return &___QnXsdAll_105; }
	inline void set_QnXsdAll_105(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAll_105 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAll_105), value);
	}

	inline static int32_t get_offset_of_QnXsdChoice_106() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdChoice_106)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdChoice_106() const { return ___QnXsdChoice_106; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdChoice_106() { return &___QnXsdChoice_106; }
	inline void set_QnXsdChoice_106(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdChoice_106 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdChoice_106), value);
	}

	inline static int32_t get_offset_of_QnXsdSequence_107() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdSequence_107)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdSequence_107() const { return ___QnXsdSequence_107; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdSequence_107() { return &___QnXsdSequence_107; }
	inline void set_QnXsdSequence_107(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdSequence_107 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdSequence_107), value);
	}

	inline static int32_t get_offset_of_QnXsdAny_108() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAny_108)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAny_108() const { return ___QnXsdAny_108; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAny_108() { return &___QnXsdAny_108; }
	inline void set_QnXsdAny_108(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAny_108 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAny_108), value);
	}

	inline static int32_t get_offset_of_QnXsdNotation_109() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdNotation_109)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdNotation_109() const { return ___QnXsdNotation_109; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdNotation_109() { return &___QnXsdNotation_109; }
	inline void set_QnXsdNotation_109(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdNotation_109 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdNotation_109), value);
	}

	inline static int32_t get_offset_of_QnXsdSimpleType_110() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdSimpleType_110)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdSimpleType_110() const { return ___QnXsdSimpleType_110; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdSimpleType_110() { return &___QnXsdSimpleType_110; }
	inline void set_QnXsdSimpleType_110(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdSimpleType_110 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdSimpleType_110), value);
	}

	inline static int32_t get_offset_of_QnXsdComplexType_111() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdComplexType_111)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdComplexType_111() const { return ___QnXsdComplexType_111; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdComplexType_111() { return &___QnXsdComplexType_111; }
	inline void set_QnXsdComplexType_111(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdComplexType_111 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdComplexType_111), value);
	}

	inline static int32_t get_offset_of_QnXsdUnique_112() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdUnique_112)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdUnique_112() const { return ___QnXsdUnique_112; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdUnique_112() { return &___QnXsdUnique_112; }
	inline void set_QnXsdUnique_112(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdUnique_112 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdUnique_112), value);
	}

	inline static int32_t get_offset_of_QnXsdKey_113() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdKey_113)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdKey_113() const { return ___QnXsdKey_113; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdKey_113() { return &___QnXsdKey_113; }
	inline void set_QnXsdKey_113(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdKey_113 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdKey_113), value);
	}

	inline static int32_t get_offset_of_QnXsdKeyRef_114() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdKeyRef_114)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdKeyRef_114() const { return ___QnXsdKeyRef_114; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdKeyRef_114() { return &___QnXsdKeyRef_114; }
	inline void set_QnXsdKeyRef_114(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdKeyRef_114 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdKeyRef_114), value);
	}

	inline static int32_t get_offset_of_QnXsdSelector_115() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdSelector_115)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdSelector_115() const { return ___QnXsdSelector_115; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdSelector_115() { return &___QnXsdSelector_115; }
	inline void set_QnXsdSelector_115(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdSelector_115 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdSelector_115), value);
	}

	inline static int32_t get_offset_of_QnXsdField_116() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdField_116)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdField_116() const { return ___QnXsdField_116; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdField_116() { return &___QnXsdField_116; }
	inline void set_QnXsdField_116(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdField_116 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdField_116), value);
	}

	inline static int32_t get_offset_of_QnXsdMinExclusive_117() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdMinExclusive_117)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdMinExclusive_117() const { return ___QnXsdMinExclusive_117; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdMinExclusive_117() { return &___QnXsdMinExclusive_117; }
	inline void set_QnXsdMinExclusive_117(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdMinExclusive_117 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdMinExclusive_117), value);
	}

	inline static int32_t get_offset_of_QnXsdMinInclusive_118() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdMinInclusive_118)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdMinInclusive_118() const { return ___QnXsdMinInclusive_118; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdMinInclusive_118() { return &___QnXsdMinInclusive_118; }
	inline void set_QnXsdMinInclusive_118(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdMinInclusive_118 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdMinInclusive_118), value);
	}

	inline static int32_t get_offset_of_QnXsdMaxInclusive_119() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdMaxInclusive_119)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdMaxInclusive_119() const { return ___QnXsdMaxInclusive_119; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdMaxInclusive_119() { return &___QnXsdMaxInclusive_119; }
	inline void set_QnXsdMaxInclusive_119(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdMaxInclusive_119 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdMaxInclusive_119), value);
	}

	inline static int32_t get_offset_of_QnXsdMaxExclusive_120() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdMaxExclusive_120)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdMaxExclusive_120() const { return ___QnXsdMaxExclusive_120; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdMaxExclusive_120() { return &___QnXsdMaxExclusive_120; }
	inline void set_QnXsdMaxExclusive_120(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdMaxExclusive_120 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdMaxExclusive_120), value);
	}

	inline static int32_t get_offset_of_QnXsdTotalDigits_121() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdTotalDigits_121)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdTotalDigits_121() const { return ___QnXsdTotalDigits_121; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdTotalDigits_121() { return &___QnXsdTotalDigits_121; }
	inline void set_QnXsdTotalDigits_121(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdTotalDigits_121 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdTotalDigits_121), value);
	}

	inline static int32_t get_offset_of_QnXsdFractionDigits_122() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdFractionDigits_122)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdFractionDigits_122() const { return ___QnXsdFractionDigits_122; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdFractionDigits_122() { return &___QnXsdFractionDigits_122; }
	inline void set_QnXsdFractionDigits_122(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdFractionDigits_122 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdFractionDigits_122), value);
	}

	inline static int32_t get_offset_of_QnXsdLength_123() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdLength_123)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdLength_123() const { return ___QnXsdLength_123; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdLength_123() { return &___QnXsdLength_123; }
	inline void set_QnXsdLength_123(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdLength_123 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdLength_123), value);
	}

	inline static int32_t get_offset_of_QnXsdMinLength_124() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdMinLength_124)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdMinLength_124() const { return ___QnXsdMinLength_124; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdMinLength_124() { return &___QnXsdMinLength_124; }
	inline void set_QnXsdMinLength_124(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdMinLength_124 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdMinLength_124), value);
	}

	inline static int32_t get_offset_of_QnXsdMaxLength_125() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdMaxLength_125)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdMaxLength_125() const { return ___QnXsdMaxLength_125; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdMaxLength_125() { return &___QnXsdMaxLength_125; }
	inline void set_QnXsdMaxLength_125(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdMaxLength_125 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdMaxLength_125), value);
	}

	inline static int32_t get_offset_of_QnXsdEnumeration_126() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdEnumeration_126)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdEnumeration_126() const { return ___QnXsdEnumeration_126; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdEnumeration_126() { return &___QnXsdEnumeration_126; }
	inline void set_QnXsdEnumeration_126(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdEnumeration_126 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdEnumeration_126), value);
	}

	inline static int32_t get_offset_of_QnXsdPattern_127() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdPattern_127)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdPattern_127() const { return ___QnXsdPattern_127; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdPattern_127() { return &___QnXsdPattern_127; }
	inline void set_QnXsdPattern_127(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdPattern_127 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdPattern_127), value);
	}

	inline static int32_t get_offset_of_QnXsdDocumentation_128() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdDocumentation_128)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdDocumentation_128() const { return ___QnXsdDocumentation_128; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdDocumentation_128() { return &___QnXsdDocumentation_128; }
	inline void set_QnXsdDocumentation_128(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdDocumentation_128 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdDocumentation_128), value);
	}

	inline static int32_t get_offset_of_QnXsdAppinfo_129() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAppinfo_129)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAppinfo_129() const { return ___QnXsdAppinfo_129; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAppinfo_129() { return &___QnXsdAppinfo_129; }
	inline void set_QnXsdAppinfo_129(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAppinfo_129 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAppinfo_129), value);
	}

	inline static int32_t get_offset_of_QnSource_130() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnSource_130)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnSource_130() const { return ___QnSource_130; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnSource_130() { return &___QnSource_130; }
	inline void set_QnSource_130(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnSource_130 = value;
		Il2CppCodeGenWriteBarrier((&___QnSource_130), value);
	}

	inline static int32_t get_offset_of_QnXsdComplexContent_131() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdComplexContent_131)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdComplexContent_131() const { return ___QnXsdComplexContent_131; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdComplexContent_131() { return &___QnXsdComplexContent_131; }
	inline void set_QnXsdComplexContent_131(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdComplexContent_131 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdComplexContent_131), value);
	}

	inline static int32_t get_offset_of_QnXsdSimpleContent_132() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdSimpleContent_132)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdSimpleContent_132() const { return ___QnXsdSimpleContent_132; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdSimpleContent_132() { return &___QnXsdSimpleContent_132; }
	inline void set_QnXsdSimpleContent_132(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdSimpleContent_132 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdSimpleContent_132), value);
	}

	inline static int32_t get_offset_of_QnXsdRestriction_133() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdRestriction_133)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdRestriction_133() const { return ___QnXsdRestriction_133; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdRestriction_133() { return &___QnXsdRestriction_133; }
	inline void set_QnXsdRestriction_133(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdRestriction_133 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdRestriction_133), value);
	}

	inline static int32_t get_offset_of_QnXsdExtension_134() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdExtension_134)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdExtension_134() const { return ___QnXsdExtension_134; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdExtension_134() { return &___QnXsdExtension_134; }
	inline void set_QnXsdExtension_134(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdExtension_134 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdExtension_134), value);
	}

	inline static int32_t get_offset_of_QnXsdUnion_135() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdUnion_135)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdUnion_135() const { return ___QnXsdUnion_135; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdUnion_135() { return &___QnXsdUnion_135; }
	inline void set_QnXsdUnion_135(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdUnion_135 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdUnion_135), value);
	}

	inline static int32_t get_offset_of_QnXsdList_136() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdList_136)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdList_136() const { return ___QnXsdList_136; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdList_136() { return &___QnXsdList_136; }
	inline void set_QnXsdList_136(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdList_136 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdList_136), value);
	}

	inline static int32_t get_offset_of_QnXsdWhiteSpace_137() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdWhiteSpace_137)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdWhiteSpace_137() const { return ___QnXsdWhiteSpace_137; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdWhiteSpace_137() { return &___QnXsdWhiteSpace_137; }
	inline void set_QnXsdWhiteSpace_137(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdWhiteSpace_137 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdWhiteSpace_137), value);
	}

	inline static int32_t get_offset_of_QnXsdRedefine_138() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdRedefine_138)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdRedefine_138() const { return ___QnXsdRedefine_138; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdRedefine_138() { return &___QnXsdRedefine_138; }
	inline void set_QnXsdRedefine_138(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdRedefine_138 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdRedefine_138), value);
	}

	inline static int32_t get_offset_of_QnXsdAnyType_139() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___QnXsdAnyType_139)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnXsdAnyType_139() const { return ___QnXsdAnyType_139; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnXsdAnyType_139() { return &___QnXsdAnyType_139; }
	inline void set_QnXsdAnyType_139(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnXsdAnyType_139 = value;
		Il2CppCodeGenWriteBarrier((&___QnXsdAnyType_139), value);
	}

	inline static int32_t get_offset_of_TokenToQName_140() { return static_cast<int32_t>(offsetof(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89, ___TokenToQName_140)); }
	inline XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* get_TokenToQName_140() const { return ___TokenToQName_140; }
	inline XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B** get_address_of_TokenToQName_140() { return &___TokenToQName_140; }
	inline void set_TokenToQName_140(XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* value)
	{
		___TokenToQName_140 = value;
		Il2CppCodeGenWriteBarrier((&___TokenToQName_140), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMANAMES_T15BD91B1C74FC6C9F9825D572D9C2F15726A1E89_H
#ifndef SCHEMANOTATION_TFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A_H
#define SCHEMANOTATION_TFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNotation
struct  SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaNotation::name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___name_0;
	// System.String System.Xml.Schema.SchemaNotation::systemLiteral
	String_t* ___systemLiteral_1;
	// System.String System.Xml.Schema.SchemaNotation::pubid
	String_t* ___pubid_2;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A, ___name_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_systemLiteral_1() { return static_cast<int32_t>(offsetof(SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A, ___systemLiteral_1)); }
	inline String_t* get_systemLiteral_1() const { return ___systemLiteral_1; }
	inline String_t** get_address_of_systemLiteral_1() { return &___systemLiteral_1; }
	inline void set_systemLiteral_1(String_t* value)
	{
		___systemLiteral_1 = value;
		Il2CppCodeGenWriteBarrier((&___systemLiteral_1), value);
	}

	inline static int32_t get_offset_of_pubid_2() { return static_cast<int32_t>(offsetof(SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A, ___pubid_2)); }
	inline String_t* get_pubid_2() const { return ___pubid_2; }
	inline String_t** get_address_of_pubid_2() { return &___pubid_2; }
	inline void set_pubid_2(String_t* value)
	{
		___pubid_2 = value;
		Il2CppCodeGenWriteBarrier((&___pubid_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMANOTATION_TFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A_H
#ifndef DECLBASEINFO_TDCAE45F69D14C5B4B582AEF4564C500A656D1C0B_H
#define DECLBASEINFO_TDCAE45F69D14C5B4B582AEF4564C500A656D1C0B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/DeclBaseInfo
struct  DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ____Name_0;
	// System.String System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Prefix
	String_t* ____Prefix_1;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XdrBuilder/DeclBaseInfo::_TypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ____TypeName_2;
	// System.String System.Xml.Schema.XdrBuilder/DeclBaseInfo::_TypePrefix
	String_t* ____TypePrefix_3;
	// System.Object System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Default
	RuntimeObject * ____Default_4;
	// System.Object System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Revises
	RuntimeObject * ____Revises_5;
	// System.UInt32 System.Xml.Schema.XdrBuilder/DeclBaseInfo::_MaxOccurs
	uint32_t ____MaxOccurs_6;
	// System.UInt32 System.Xml.Schema.XdrBuilder/DeclBaseInfo::_MinOccurs
	uint32_t ____MinOccurs_7;
	// System.Boolean System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Checking
	bool ____Checking_8;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.XdrBuilder/DeclBaseInfo::_ElementDecl
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ____ElementDecl_9;
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Attdef
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * ____Attdef_10;
	// System.Xml.Schema.XdrBuilder/DeclBaseInfo System.Xml.Schema.XdrBuilder/DeclBaseInfo::_Next
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * ____Next_11;

public:
	inline static int32_t get_offset_of__Name_0() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Name_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get__Name_0() const { return ____Name_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of__Name_0() { return &____Name_0; }
	inline void set__Name_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		____Name_0 = value;
		Il2CppCodeGenWriteBarrier((&____Name_0), value);
	}

	inline static int32_t get_offset_of__Prefix_1() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Prefix_1)); }
	inline String_t* get__Prefix_1() const { return ____Prefix_1; }
	inline String_t** get_address_of__Prefix_1() { return &____Prefix_1; }
	inline void set__Prefix_1(String_t* value)
	{
		____Prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&____Prefix_1), value);
	}

	inline static int32_t get_offset_of__TypeName_2() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____TypeName_2)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get__TypeName_2() const { return ____TypeName_2; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of__TypeName_2() { return &____TypeName_2; }
	inline void set__TypeName_2(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		____TypeName_2 = value;
		Il2CppCodeGenWriteBarrier((&____TypeName_2), value);
	}

	inline static int32_t get_offset_of__TypePrefix_3() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____TypePrefix_3)); }
	inline String_t* get__TypePrefix_3() const { return ____TypePrefix_3; }
	inline String_t** get_address_of__TypePrefix_3() { return &____TypePrefix_3; }
	inline void set__TypePrefix_3(String_t* value)
	{
		____TypePrefix_3 = value;
		Il2CppCodeGenWriteBarrier((&____TypePrefix_3), value);
	}

	inline static int32_t get_offset_of__Default_4() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Default_4)); }
	inline RuntimeObject * get__Default_4() const { return ____Default_4; }
	inline RuntimeObject ** get_address_of__Default_4() { return &____Default_4; }
	inline void set__Default_4(RuntimeObject * value)
	{
		____Default_4 = value;
		Il2CppCodeGenWriteBarrier((&____Default_4), value);
	}

	inline static int32_t get_offset_of__Revises_5() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Revises_5)); }
	inline RuntimeObject * get__Revises_5() const { return ____Revises_5; }
	inline RuntimeObject ** get_address_of__Revises_5() { return &____Revises_5; }
	inline void set__Revises_5(RuntimeObject * value)
	{
		____Revises_5 = value;
		Il2CppCodeGenWriteBarrier((&____Revises_5), value);
	}

	inline static int32_t get_offset_of__MaxOccurs_6() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____MaxOccurs_6)); }
	inline uint32_t get__MaxOccurs_6() const { return ____MaxOccurs_6; }
	inline uint32_t* get_address_of__MaxOccurs_6() { return &____MaxOccurs_6; }
	inline void set__MaxOccurs_6(uint32_t value)
	{
		____MaxOccurs_6 = value;
	}

	inline static int32_t get_offset_of__MinOccurs_7() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____MinOccurs_7)); }
	inline uint32_t get__MinOccurs_7() const { return ____MinOccurs_7; }
	inline uint32_t* get_address_of__MinOccurs_7() { return &____MinOccurs_7; }
	inline void set__MinOccurs_7(uint32_t value)
	{
		____MinOccurs_7 = value;
	}

	inline static int32_t get_offset_of__Checking_8() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Checking_8)); }
	inline bool get__Checking_8() const { return ____Checking_8; }
	inline bool* get_address_of__Checking_8() { return &____Checking_8; }
	inline void set__Checking_8(bool value)
	{
		____Checking_8 = value;
	}

	inline static int32_t get_offset_of__ElementDecl_9() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____ElementDecl_9)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get__ElementDecl_9() const { return ____ElementDecl_9; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of__ElementDecl_9() { return &____ElementDecl_9; }
	inline void set__ElementDecl_9(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		____ElementDecl_9 = value;
		Il2CppCodeGenWriteBarrier((&____ElementDecl_9), value);
	}

	inline static int32_t get_offset_of__Attdef_10() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Attdef_10)); }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * get__Attdef_10() const { return ____Attdef_10; }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 ** get_address_of__Attdef_10() { return &____Attdef_10; }
	inline void set__Attdef_10(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * value)
	{
		____Attdef_10 = value;
		Il2CppCodeGenWriteBarrier((&____Attdef_10), value);
	}

	inline static int32_t get_offset_of__Next_11() { return static_cast<int32_t>(offsetof(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B, ____Next_11)); }
	inline DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * get__Next_11() const { return ____Next_11; }
	inline DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B ** get_address_of__Next_11() { return &____Next_11; }
	inline void set__Next_11(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * value)
	{
		____Next_11 = value;
		Il2CppCodeGenWriteBarrier((&____Next_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECLBASEINFO_TDCAE45F69D14C5B4B582AEF4564C500A656D1C0B_H
#ifndef GROUPCONTENT_TA340212E7B02F065B9D0A4764545DD0360B13EBE_H
#define GROUPCONTENT_TA340212E7B02F065B9D0A4764545DD0360B13EBE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder/GroupContent
struct  GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE  : public RuntimeObject
{
public:
	// System.UInt32 System.Xml.Schema.XdrBuilder/GroupContent::_MinVal
	uint32_t ____MinVal_0;
	// System.UInt32 System.Xml.Schema.XdrBuilder/GroupContent::_MaxVal
	uint32_t ____MaxVal_1;
	// System.Boolean System.Xml.Schema.XdrBuilder/GroupContent::_HasMaxAttr
	bool ____HasMaxAttr_2;
	// System.Boolean System.Xml.Schema.XdrBuilder/GroupContent::_HasMinAttr
	bool ____HasMinAttr_3;
	// System.Int32 System.Xml.Schema.XdrBuilder/GroupContent::_Order
	int32_t ____Order_4;

public:
	inline static int32_t get_offset_of__MinVal_0() { return static_cast<int32_t>(offsetof(GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE, ____MinVal_0)); }
	inline uint32_t get__MinVal_0() const { return ____MinVal_0; }
	inline uint32_t* get_address_of__MinVal_0() { return &____MinVal_0; }
	inline void set__MinVal_0(uint32_t value)
	{
		____MinVal_0 = value;
	}

	inline static int32_t get_offset_of__MaxVal_1() { return static_cast<int32_t>(offsetof(GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE, ____MaxVal_1)); }
	inline uint32_t get__MaxVal_1() const { return ____MaxVal_1; }
	inline uint32_t* get_address_of__MaxVal_1() { return &____MaxVal_1; }
	inline void set__MaxVal_1(uint32_t value)
	{
		____MaxVal_1 = value;
	}

	inline static int32_t get_offset_of__HasMaxAttr_2() { return static_cast<int32_t>(offsetof(GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE, ____HasMaxAttr_2)); }
	inline bool get__HasMaxAttr_2() const { return ____HasMaxAttr_2; }
	inline bool* get_address_of__HasMaxAttr_2() { return &____HasMaxAttr_2; }
	inline void set__HasMaxAttr_2(bool value)
	{
		____HasMaxAttr_2 = value;
	}

	inline static int32_t get_offset_of__HasMinAttr_3() { return static_cast<int32_t>(offsetof(GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE, ____HasMinAttr_3)); }
	inline bool get__HasMinAttr_3() const { return ____HasMinAttr_3; }
	inline bool* get_address_of__HasMinAttr_3() { return &____HasMinAttr_3; }
	inline void set__HasMinAttr_3(bool value)
	{
		____HasMinAttr_3 = value;
	}

	inline static int32_t get_offset_of__Order_4() { return static_cast<int32_t>(offsetof(GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE, ____Order_4)); }
	inline int32_t get__Order_4() const { return ____Order_4; }
	inline int32_t* get_address_of__Order_4() { return &____Order_4; }
	inline void set__Order_4(int32_t value)
	{
		____Order_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GROUPCONTENT_TA340212E7B02F065B9D0A4764545DD0360B13EBE_H
#ifndef XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#define XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatype
struct  XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPE_T6D9535C4B3780086DF21646303E2350D40A5A550_H
#ifndef XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#define XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlNamespaceManager
struct  XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F  : public RuntimeObject
{
public:
	// System.Xml.XmlNamespaceManager/NamespaceDeclaration[] System.Xml.XmlNamespaceManager::nsdecls
	NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* ___nsdecls_0;
	// System.Int32 System.Xml.XmlNamespaceManager::lastDecl
	int32_t ___lastDecl_1;
	// System.Xml.XmlNameTable System.Xml.XmlNamespaceManager::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_2;
	// System.Int32 System.Xml.XmlNamespaceManager::scopeId
	int32_t ___scopeId_3;
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> System.Xml.XmlNamespaceManager::hashTable
	Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * ___hashTable_4;
	// System.Boolean System.Xml.XmlNamespaceManager::useHashtable
	bool ___useHashtable_5;
	// System.String System.Xml.XmlNamespaceManager::xml
	String_t* ___xml_6;
	// System.String System.Xml.XmlNamespaceManager::xmlNs
	String_t* ___xmlNs_7;

public:
	inline static int32_t get_offset_of_nsdecls_0() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___nsdecls_0)); }
	inline NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* get_nsdecls_0() const { return ___nsdecls_0; }
	inline NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84** get_address_of_nsdecls_0() { return &___nsdecls_0; }
	inline void set_nsdecls_0(NamespaceDeclarationU5BU5D_t92995CBBEC52166860E77B72B8EEFF00E4D64E84* value)
	{
		___nsdecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___nsdecls_0), value);
	}

	inline static int32_t get_offset_of_lastDecl_1() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___lastDecl_1)); }
	inline int32_t get_lastDecl_1() const { return ___lastDecl_1; }
	inline int32_t* get_address_of_lastDecl_1() { return &___lastDecl_1; }
	inline void set_lastDecl_1(int32_t value)
	{
		___lastDecl_1 = value;
	}

	inline static int32_t get_offset_of_nameTable_2() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___nameTable_2)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_2() const { return ___nameTable_2; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_2() { return &___nameTable_2; }
	inline void set_nameTable_2(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_2 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_2), value);
	}

	inline static int32_t get_offset_of_scopeId_3() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___scopeId_3)); }
	inline int32_t get_scopeId_3() const { return ___scopeId_3; }
	inline int32_t* get_address_of_scopeId_3() { return &___scopeId_3; }
	inline void set_scopeId_3(int32_t value)
	{
		___scopeId_3 = value;
	}

	inline static int32_t get_offset_of_hashTable_4() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___hashTable_4)); }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * get_hashTable_4() const { return ___hashTable_4; }
	inline Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB ** get_address_of_hashTable_4() { return &___hashTable_4; }
	inline void set_hashTable_4(Dictionary_2_tD6E204872BA9FD506A0287EF68E285BEB9EC0DFB * value)
	{
		___hashTable_4 = value;
		Il2CppCodeGenWriteBarrier((&___hashTable_4), value);
	}

	inline static int32_t get_offset_of_useHashtable_5() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___useHashtable_5)); }
	inline bool get_useHashtable_5() const { return ___useHashtable_5; }
	inline bool* get_address_of_useHashtable_5() { return &___useHashtable_5; }
	inline void set_useHashtable_5(bool value)
	{
		___useHashtable_5 = value;
	}

	inline static int32_t get_offset_of_xml_6() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___xml_6)); }
	inline String_t* get_xml_6() const { return ___xml_6; }
	inline String_t** get_address_of_xml_6() { return &___xml_6; }
	inline void set_xml_6(String_t* value)
	{
		___xml_6 = value;
		Il2CppCodeGenWriteBarrier((&___xml_6), value);
	}

	inline static int32_t get_offset_of_xmlNs_7() { return static_cast<int32_t>(offsetof(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F, ___xmlNs_7)); }
	inline String_t* get_xmlNs_7() const { return ___xmlNs_7; }
	inline String_t** get_address_of_xmlNs_7() { return &___xmlNs_7; }
	inline void set_xmlNs_7(String_t* value)
	{
		___xmlNs_7 = value;
		Il2CppCodeGenWriteBarrier((&___xmlNs_7), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLNAMESPACEMANAGER_T8323BEB96BBE8F75207DC2AAFE9430E7F473658F_H
#ifndef DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#define DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Decimal
struct  Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 
{
public:
	// System.Int32 System.Decimal::flags
	int32_t ___flags_14;
	// System.Int32 System.Decimal::hi
	int32_t ___hi_15;
	// System.Int32 System.Decimal::lo
	int32_t ___lo_16;
	// System.Int32 System.Decimal::mid
	int32_t ___mid_17;

public:
	inline static int32_t get_offset_of_flags_14() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___flags_14)); }
	inline int32_t get_flags_14() const { return ___flags_14; }
	inline int32_t* get_address_of_flags_14() { return &___flags_14; }
	inline void set_flags_14(int32_t value)
	{
		___flags_14 = value;
	}

	inline static int32_t get_offset_of_hi_15() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___hi_15)); }
	inline int32_t get_hi_15() const { return ___hi_15; }
	inline int32_t* get_address_of_hi_15() { return &___hi_15; }
	inline void set_hi_15(int32_t value)
	{
		___hi_15 = value;
	}

	inline static int32_t get_offset_of_lo_16() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___lo_16)); }
	inline int32_t get_lo_16() const { return ___lo_16; }
	inline int32_t* get_address_of_lo_16() { return &___lo_16; }
	inline void set_lo_16(int32_t value)
	{
		___lo_16 = value;
	}

	inline static int32_t get_offset_of_mid_17() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8, ___mid_17)); }
	inline int32_t get_mid_17() const { return ___mid_17; }
	inline int32_t* get_address_of_mid_17() { return &___mid_17; }
	inline void set_mid_17(int32_t value)
	{
		___mid_17 = value;
	}
};

struct Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields
{
public:
	// System.UInt32[] System.Decimal::Powers10
	UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* ___Powers10_6;
	// System.Decimal System.Decimal::Zero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___Zero_7;
	// System.Decimal System.Decimal::One
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___One_8;
	// System.Decimal System.Decimal::MinusOne
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinusOne_9;
	// System.Decimal System.Decimal::MaxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MaxValue_10;
	// System.Decimal System.Decimal::MinValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___MinValue_11;
	// System.Decimal System.Decimal::NearNegativeZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearNegativeZero_12;
	// System.Decimal System.Decimal::NearPositiveZero
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___NearPositiveZero_13;

public:
	inline static int32_t get_offset_of_Powers10_6() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Powers10_6)); }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* get_Powers10_6() const { return ___Powers10_6; }
	inline UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB** get_address_of_Powers10_6() { return &___Powers10_6; }
	inline void set_Powers10_6(UInt32U5BU5D_t9AA834AF2940E75BBF8E3F08FF0D20D266DB71CB* value)
	{
		___Powers10_6 = value;
		Il2CppCodeGenWriteBarrier((&___Powers10_6), value);
	}

	inline static int32_t get_offset_of_Zero_7() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___Zero_7)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_Zero_7() const { return ___Zero_7; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_Zero_7() { return &___Zero_7; }
	inline void set_Zero_7(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___Zero_7 = value;
	}

	inline static int32_t get_offset_of_One_8() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___One_8)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_One_8() const { return ___One_8; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_One_8() { return &___One_8; }
	inline void set_One_8(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___One_8 = value;
	}

	inline static int32_t get_offset_of_MinusOne_9() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinusOne_9)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinusOne_9() const { return ___MinusOne_9; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinusOne_9() { return &___MinusOne_9; }
	inline void set_MinusOne_9(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinusOne_9 = value;
	}

	inline static int32_t get_offset_of_MaxValue_10() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MaxValue_10)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MaxValue_10() const { return ___MaxValue_10; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MaxValue_10() { return &___MaxValue_10; }
	inline void set_MaxValue_10(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MaxValue_10 = value;
	}

	inline static int32_t get_offset_of_MinValue_11() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___MinValue_11)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_MinValue_11() const { return ___MinValue_11; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_MinValue_11() { return &___MinValue_11; }
	inline void set_MinValue_11(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___MinValue_11 = value;
	}

	inline static int32_t get_offset_of_NearNegativeZero_12() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearNegativeZero_12)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearNegativeZero_12() const { return ___NearNegativeZero_12; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearNegativeZero_12() { return &___NearNegativeZero_12; }
	inline void set_NearNegativeZero_12(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearNegativeZero_12 = value;
	}

	inline static int32_t get_offset_of_NearPositiveZero_13() { return static_cast<int32_t>(offsetof(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_StaticFields, ___NearPositiveZero_13)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_NearPositiveZero_13() const { return ___NearPositiveZero_13; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_NearPositiveZero_13() { return &___NearPositiveZero_13; }
	inline void set_NearPositiveZero_13(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___NearPositiveZero_13 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DECIMAL_T44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef INTPTR_T_H
#define INTPTR_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.IntPtr
struct  IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INTPTR_T_H
#ifndef SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#define SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.SystemException
struct  SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SYSTEMEXCEPTION_T5380468142AA850BE4A341D7AF3EAB9C78746782_H
#ifndef VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#define VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Void
struct  Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VOID_T22962CB4C05B1D89B55A6E1139F0E87A90987017_H
#ifndef BINARYFACETSCHECKER_T34E15822914D089ED851E76009B51CC2DEF3BB9D_H
#define BINARYFACETSCHECKER_T34E15822914D089ED851E76009B51CC2DEF3BB9D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.BinaryFacetsChecker
struct  BinaryFacetsChecker_t34E15822914D089ED851E76009B51CC2DEF3BB9D  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // BINARYFACETSCHECKER_T34E15822914D089ED851E76009B51CC2DEF3BB9D_H
#ifndef COMPILER_TDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89_H
#define COMPILER_TDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Compiler
struct  Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89  : public BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427
{
public:
	// System.String System.Xml.Schema.Compiler::restrictionErrorMsg
	String_t* ___restrictionErrorMsg_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::attributes
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributes_7;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::attributeGroups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___attributeGroups_8;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::elements
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___elements_9;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::schemaTypes
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___schemaTypes_10;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::groups
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___groups_11;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::notations
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___notations_12;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::examplars
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___examplars_13;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.Compiler::identityConstraints
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___identityConstraints_14;
	// System.Collections.Stack System.Xml.Schema.Compiler::complexTypeStack
	Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * ___complexTypeStack_15;
	// System.Collections.Hashtable System.Xml.Schema.Compiler::schemasToCompile
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___schemasToCompile_16;
	// System.Collections.Hashtable System.Xml.Schema.Compiler::importedSchemas
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___importedSchemas_17;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Compiler::schemaForSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schemaForSchema_18;

public:
	inline static int32_t get_offset_of_restrictionErrorMsg_6() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___restrictionErrorMsg_6)); }
	inline String_t* get_restrictionErrorMsg_6() const { return ___restrictionErrorMsg_6; }
	inline String_t** get_address_of_restrictionErrorMsg_6() { return &___restrictionErrorMsg_6; }
	inline void set_restrictionErrorMsg_6(String_t* value)
	{
		___restrictionErrorMsg_6 = value;
		Il2CppCodeGenWriteBarrier((&___restrictionErrorMsg_6), value);
	}

	inline static int32_t get_offset_of_attributes_7() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___attributes_7)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributes_7() const { return ___attributes_7; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributes_7() { return &___attributes_7; }
	inline void set_attributes_7(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___attributes_7), value);
	}

	inline static int32_t get_offset_of_attributeGroups_8() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___attributeGroups_8)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_attributeGroups_8() const { return ___attributeGroups_8; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_attributeGroups_8() { return &___attributeGroups_8; }
	inline void set_attributeGroups_8(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___attributeGroups_8 = value;
		Il2CppCodeGenWriteBarrier((&___attributeGroups_8), value);
	}

	inline static int32_t get_offset_of_elements_9() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___elements_9)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_elements_9() const { return ___elements_9; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_elements_9() { return &___elements_9; }
	inline void set_elements_9(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___elements_9 = value;
		Il2CppCodeGenWriteBarrier((&___elements_9), value);
	}

	inline static int32_t get_offset_of_schemaTypes_10() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___schemaTypes_10)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_schemaTypes_10() const { return ___schemaTypes_10; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_schemaTypes_10() { return &___schemaTypes_10; }
	inline void set_schemaTypes_10(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___schemaTypes_10 = value;
		Il2CppCodeGenWriteBarrier((&___schemaTypes_10), value);
	}

	inline static int32_t get_offset_of_groups_11() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___groups_11)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_groups_11() const { return ___groups_11; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_groups_11() { return &___groups_11; }
	inline void set_groups_11(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___groups_11 = value;
		Il2CppCodeGenWriteBarrier((&___groups_11), value);
	}

	inline static int32_t get_offset_of_notations_12() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___notations_12)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_notations_12() const { return ___notations_12; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_notations_12() { return &___notations_12; }
	inline void set_notations_12(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___notations_12 = value;
		Il2CppCodeGenWriteBarrier((&___notations_12), value);
	}

	inline static int32_t get_offset_of_examplars_13() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___examplars_13)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_examplars_13() const { return ___examplars_13; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_examplars_13() { return &___examplars_13; }
	inline void set_examplars_13(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___examplars_13 = value;
		Il2CppCodeGenWriteBarrier((&___examplars_13), value);
	}

	inline static int32_t get_offset_of_identityConstraints_14() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___identityConstraints_14)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_identityConstraints_14() const { return ___identityConstraints_14; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_identityConstraints_14() { return &___identityConstraints_14; }
	inline void set_identityConstraints_14(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___identityConstraints_14 = value;
		Il2CppCodeGenWriteBarrier((&___identityConstraints_14), value);
	}

	inline static int32_t get_offset_of_complexTypeStack_15() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___complexTypeStack_15)); }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * get_complexTypeStack_15() const { return ___complexTypeStack_15; }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 ** get_address_of_complexTypeStack_15() { return &___complexTypeStack_15; }
	inline void set_complexTypeStack_15(Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * value)
	{
		___complexTypeStack_15 = value;
		Il2CppCodeGenWriteBarrier((&___complexTypeStack_15), value);
	}

	inline static int32_t get_offset_of_schemasToCompile_16() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___schemasToCompile_16)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_schemasToCompile_16() const { return ___schemasToCompile_16; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_schemasToCompile_16() { return &___schemasToCompile_16; }
	inline void set_schemasToCompile_16(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___schemasToCompile_16 = value;
		Il2CppCodeGenWriteBarrier((&___schemasToCompile_16), value);
	}

	inline static int32_t get_offset_of_importedSchemas_17() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___importedSchemas_17)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_importedSchemas_17() const { return ___importedSchemas_17; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_importedSchemas_17() { return &___importedSchemas_17; }
	inline void set_importedSchemas_17(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___importedSchemas_17 = value;
		Il2CppCodeGenWriteBarrier((&___importedSchemas_17), value);
	}

	inline static int32_t get_offset_of_schemaForSchema_18() { return static_cast<int32_t>(offsetof(Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89, ___schemaForSchema_18)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schemaForSchema_18() const { return ___schemaForSchema_18; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schemaForSchema_18() { return &___schemaForSchema_18; }
	inline void set_schemaForSchema_18(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schemaForSchema_18 = value;
		Il2CppCodeGenWriteBarrier((&___schemaForSchema_18), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPILER_TDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89_H
#ifndef DATETIMEFACETSCHECKER_TD8BF6DE005C7EB84E9999EAD33A4C804EB45F5C4_H
#define DATETIMEFACETSCHECKER_TD8BF6DE005C7EB84E9999EAD33A4C804EB45F5C4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DateTimeFacetsChecker
struct  DateTimeFacetsChecker_tD8BF6DE005C7EB84E9999EAD33A4C804EB45F5C4  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATETIMEFACETSCHECKER_TD8BF6DE005C7EB84E9999EAD33A4C804EB45F5C4_H
#ifndef DTDVALIDATOR_TF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_H
#define DTDVALIDATOR_TF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DtdValidator
struct  DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD  : public BaseValidator_tB0AD5FCB23C61EC8841C153CEB7C77E569246B43
{
public:
	// System.Xml.HWStack System.Xml.Schema.DtdValidator::validationStack
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ___validationStack_16;
	// System.Collections.Hashtable System.Xml.Schema.DtdValidator::attPresence
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___attPresence_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DtdValidator::name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___name_18;
	// System.Collections.Hashtable System.Xml.Schema.DtdValidator::IDs
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___IDs_19;
	// System.Xml.Schema.IdRefNode System.Xml.Schema.DtdValidator::idRefListHead
	IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * ___idRefListHead_20;
	// System.Boolean System.Xml.Schema.DtdValidator::processIdentityConstraints
	bool ___processIdentityConstraints_21;

public:
	inline static int32_t get_offset_of_validationStack_16() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD, ___validationStack_16)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get_validationStack_16() const { return ___validationStack_16; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of_validationStack_16() { return &___validationStack_16; }
	inline void set_validationStack_16(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		___validationStack_16 = value;
		Il2CppCodeGenWriteBarrier((&___validationStack_16), value);
	}

	inline static int32_t get_offset_of_attPresence_17() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD, ___attPresence_17)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_attPresence_17() const { return ___attPresence_17; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_attPresence_17() { return &___attPresence_17; }
	inline void set_attPresence_17(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___attPresence_17 = value;
		Il2CppCodeGenWriteBarrier((&___attPresence_17), value);
	}

	inline static int32_t get_offset_of_name_18() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD, ___name_18)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_name_18() const { return ___name_18; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_name_18() { return &___name_18; }
	inline void set_name_18(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___name_18 = value;
		Il2CppCodeGenWriteBarrier((&___name_18), value);
	}

	inline static int32_t get_offset_of_IDs_19() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD, ___IDs_19)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_IDs_19() const { return ___IDs_19; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_IDs_19() { return &___IDs_19; }
	inline void set_IDs_19(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___IDs_19 = value;
		Il2CppCodeGenWriteBarrier((&___IDs_19), value);
	}

	inline static int32_t get_offset_of_idRefListHead_20() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD, ___idRefListHead_20)); }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * get_idRefListHead_20() const { return ___idRefListHead_20; }
	inline IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 ** get_address_of_idRefListHead_20() { return &___idRefListHead_20; }
	inline void set_idRefListHead_20(IdRefNode_t8E92E33D4D596CFC17454D41C07CD7627A8825E2 * value)
	{
		___idRefListHead_20 = value;
		Il2CppCodeGenWriteBarrier((&___idRefListHead_20), value);
	}

	inline static int32_t get_offset_of_processIdentityConstraints_21() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD, ___processIdentityConstraints_21)); }
	inline bool get_processIdentityConstraints_21() const { return ___processIdentityConstraints_21; }
	inline bool* get_address_of_processIdentityConstraints_21() { return &___processIdentityConstraints_21; }
	inline void set_processIdentityConstraints_21(bool value)
	{
		___processIdentityConstraints_21 = value;
	}
};

struct DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_StaticFields
{
public:
	// System.Xml.Schema.DtdValidator/NamespaceManager System.Xml.Schema.DtdValidator::namespaceManager
	NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408 * ___namespaceManager_15;

public:
	inline static int32_t get_offset_of_namespaceManager_15() { return static_cast<int32_t>(offsetof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_StaticFields, ___namespaceManager_15)); }
	inline NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408 * get_namespaceManager_15() const { return ___namespaceManager_15; }
	inline NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408 ** get_address_of_namespaceManager_15() { return &___namespaceManager_15; }
	inline void set_namespaceManager_15(NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408 * value)
	{
		___namespaceManager_15 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_15), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DTDVALIDATOR_TF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_H
#ifndef NAMESPACEMANAGER_T8A121C523956A538C9B6909B308301440F33F408_H
#define NAMESPACEMANAGER_T8A121C523956A538C9B6909B308301440F33F408_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DtdValidator/NamespaceManager
struct  NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408  : public XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACEMANAGER_T8A121C523956A538C9B6909B308301440F33F408_H
#ifndef DURATIONFACETSCHECKER_T147376E625825EF88985237BAF989648FEA5B4A5_H
#define DURATIONFACETSCHECKER_T147376E625825EF88985237BAF989648FEA5B4A5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DurationFacetsChecker
struct  DurationFacetsChecker_t147376E625825EF88985237BAF989648FEA5B4A5  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DURATIONFACETSCHECKER_T147376E625825EF88985237BAF989648FEA5B4A5_H
#ifndef MAP_T95CF8863CD8CAC88704F399964E9ED0524DD731D_H
#define MAP_T95CF8863CD8CAC88704F399964E9ED0524DD731D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct  Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D 
{
public:
	// System.Char System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::match
	Il2CppChar ___match_0;
	// System.String System.Xml.Schema.FacetsChecker/FacetsCompiler/Map::replacement
	String_t* ___replacement_1;

public:
	inline static int32_t get_offset_of_match_0() { return static_cast<int32_t>(offsetof(Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D, ___match_0)); }
	inline Il2CppChar get_match_0() const { return ___match_0; }
	inline Il2CppChar* get_address_of_match_0() { return &___match_0; }
	inline void set_match_0(Il2CppChar value)
	{
		___match_0 = value;
	}

	inline static int32_t get_offset_of_replacement_1() { return static_cast<int32_t>(offsetof(Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D, ___replacement_1)); }
	inline String_t* get_replacement_1() const { return ___replacement_1; }
	inline String_t** get_address_of_replacement_1() { return &___replacement_1; }
	inline void set_replacement_1(String_t* value)
	{
		___replacement_1 = value;
		Il2CppCodeGenWriteBarrier((&___replacement_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D_marshaled_pinvoke
{
	uint8_t ___match_0;
	char* ___replacement_1;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler/Map
struct Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D_marshaled_com
{
	uint8_t ___match_0;
	Il2CppChar* ___replacement_1;
};
#endif // MAP_T95CF8863CD8CAC88704F399964E9ED0524DD731D_H
#ifndef LISTFACETSCHECKER_TA6ADE6F856B37972B4A2F9A911BC452C04C589EB_H
#define LISTFACETSCHECKER_TA6ADE6F856B37972B4A2F9A911BC452C04C589EB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ListFacetsChecker
struct  ListFacetsChecker_tA6ADE6F856B37972B4A2F9A911BC452C04C589EB  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTFACETSCHECKER_TA6ADE6F856B37972B4A2F9A911BC452C04C589EB_H
#ifndef MISCFACETSCHECKER_TD88807B2E42CFA69E5A3BCA30E35D338D803B387_H
#define MISCFACETSCHECKER_TD88807B2E42CFA69E5A3BCA30E35D338D803B387_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.MiscFacetsChecker
struct  MiscFacetsChecker_tD88807B2E42CFA69E5A3BCA30E35D338D803B387  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // MISCFACETSCHECKER_TD88807B2E42CFA69E5A3BCA30E35D338D803B387_H
#ifndef NUMERIC2FACETSCHECKER_T5D69CF84FABD913681779290500406D1E84BDAA5_H
#define NUMERIC2FACETSCHECKER_T5D69CF84FABD913681779290500406D1E84BDAA5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Numeric2FacetsChecker
struct  Numeric2FacetsChecker_t5D69CF84FABD913681779290500406D1E84BDAA5  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERIC2FACETSCHECKER_T5D69CF84FABD913681779290500406D1E84BDAA5_H
#ifndef QNAMEFACETSCHECKER_T56B938AEAB5FB017392DE9855706BF1B0B5FB411_H
#define QNAMEFACETSCHECKER_T56B938AEAB5FB017392DE9855706BF1B0B5FB411_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.QNameFacetsChecker
struct  QNameFacetsChecker_t56B938AEAB5FB017392DE9855706BF1B0B5FB411  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // QNAMEFACETSCHECKER_T56B938AEAB5FB017392DE9855706BF1B0B5FB411_H
#ifndef SCHEMACOLLECTIONCOMPILER_TBDF40411A1DA3DEDDFF536F37442663EF77F7B6D_H
#define SCHEMACOLLECTIONCOMPILER_TBDF40411A1DA3DEDDFF536F37442663EF77F7B6D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionCompiler
struct  SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D  : public BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427
{
public:
	// System.Boolean System.Xml.Schema.SchemaCollectionCompiler::compileContentModel
	bool ___compileContentModel_6;
	// System.Xml.Schema.XmlSchemaObjectTable System.Xml.Schema.SchemaCollectionCompiler::examplars
	XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * ___examplars_7;
	// System.Collections.Stack System.Xml.Schema.SchemaCollectionCompiler::complexTypeStack
	Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * ___complexTypeStack_8;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.SchemaCollectionCompiler::schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schema_9;

public:
	inline static int32_t get_offset_of_compileContentModel_6() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D, ___compileContentModel_6)); }
	inline bool get_compileContentModel_6() const { return ___compileContentModel_6; }
	inline bool* get_address_of_compileContentModel_6() { return &___compileContentModel_6; }
	inline void set_compileContentModel_6(bool value)
	{
		___compileContentModel_6 = value;
	}

	inline static int32_t get_offset_of_examplars_7() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D, ___examplars_7)); }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * get_examplars_7() const { return ___examplars_7; }
	inline XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 ** get_address_of_examplars_7() { return &___examplars_7; }
	inline void set_examplars_7(XmlSchemaObjectTable_t8052FBDE5AB8FDD05FA48092BB0D511C496D3833 * value)
	{
		___examplars_7 = value;
		Il2CppCodeGenWriteBarrier((&___examplars_7), value);
	}

	inline static int32_t get_offset_of_complexTypeStack_8() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D, ___complexTypeStack_8)); }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * get_complexTypeStack_8() const { return ___complexTypeStack_8; }
	inline Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 ** get_address_of_complexTypeStack_8() { return &___complexTypeStack_8; }
	inline void set_complexTypeStack_8(Stack_t37723B68CC4FFD95F0F3D06A5D42D7DEE7569643 * value)
	{
		___complexTypeStack_8 = value;
		Il2CppCodeGenWriteBarrier((&___complexTypeStack_8), value);
	}

	inline static int32_t get_offset_of_schema_9() { return static_cast<int32_t>(offsetof(SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D, ___schema_9)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schema_9() const { return ___schema_9; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schema_9() { return &___schema_9; }
	inline void set_schema_9(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schema_9 = value;
		Il2CppCodeGenWriteBarrier((&___schema_9), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMACOLLECTIONCOMPILER_TBDF40411A1DA3DEDDFF536F37442663EF77F7B6D_H
#ifndef SCHEMANAMESPACEMANAGER_T3DF19809C72DF93A44C9479762DDECC0C84EC970_H
#define SCHEMANAMESPACEMANAGER_T3DF19809C72DF93A44C9479762DDECC0C84EC970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNamespaceManager
struct  SchemaNamespaceManager_t3DF19809C72DF93A44C9479762DDECC0C84EC970  : public XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F
{
public:
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.SchemaNamespaceManager::node
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___node_8;

public:
	inline static int32_t get_offset_of_node_8() { return static_cast<int32_t>(offsetof(SchemaNamespaceManager_t3DF19809C72DF93A44C9479762DDECC0C84EC970, ___node_8)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_node_8() const { return ___node_8; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_node_8() { return &___node_8; }
	inline void set_node_8(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___node_8 = value;
		Il2CppCodeGenWriteBarrier((&___node_8), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMANAMESPACEMANAGER_T3DF19809C72DF93A44C9479762DDECC0C84EC970_H
#ifndef STATEUNION_T9A043C39BC0361F60194C33113B629BD87A96EEF_H
#define STATEUNION_T9A043C39BC0361F60194C33113B629BD87A96EEF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StateUnion
struct  StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF 
{
public:
	union
	{
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::State
			int32_t ___State_0;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___State_0_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::AllElementsRequired
			int32_t ___AllElementsRequired_1;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___AllElementsRequired_1_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::CurPosIndex
			int32_t ___CurPosIndex_2;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___CurPosIndex_2_forAlignmentOnly;
		};
		#pragma pack(push, tp, 1)
		struct
		{
			// System.Int32 System.Xml.Schema.StateUnion::NumberOfRunningPos
			int32_t ___NumberOfRunningPos_3;
		};
		#pragma pack(pop, tp)
		struct
		{
			int32_t ___NumberOfRunningPos_3_forAlignmentOnly;
		};
	};

public:
	inline static int32_t get_offset_of_State_0() { return static_cast<int32_t>(offsetof(StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF, ___State_0)); }
	inline int32_t get_State_0() const { return ___State_0; }
	inline int32_t* get_address_of_State_0() { return &___State_0; }
	inline void set_State_0(int32_t value)
	{
		___State_0 = value;
	}

	inline static int32_t get_offset_of_AllElementsRequired_1() { return static_cast<int32_t>(offsetof(StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF, ___AllElementsRequired_1)); }
	inline int32_t get_AllElementsRequired_1() const { return ___AllElementsRequired_1; }
	inline int32_t* get_address_of_AllElementsRequired_1() { return &___AllElementsRequired_1; }
	inline void set_AllElementsRequired_1(int32_t value)
	{
		___AllElementsRequired_1 = value;
	}

	inline static int32_t get_offset_of_CurPosIndex_2() { return static_cast<int32_t>(offsetof(StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF, ___CurPosIndex_2)); }
	inline int32_t get_CurPosIndex_2() const { return ___CurPosIndex_2; }
	inline int32_t* get_address_of_CurPosIndex_2() { return &___CurPosIndex_2; }
	inline void set_CurPosIndex_2(int32_t value)
	{
		___CurPosIndex_2 = value;
	}

	inline static int32_t get_offset_of_NumberOfRunningPos_3() { return static_cast<int32_t>(offsetof(StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF, ___NumberOfRunningPos_3)); }
	inline int32_t get_NumberOfRunningPos_3() const { return ___NumberOfRunningPos_3; }
	inline int32_t* get_address_of_NumberOfRunningPos_3() { return &___NumberOfRunningPos_3; }
	inline void set_NumberOfRunningPos_3(int32_t value)
	{
		___NumberOfRunningPos_3 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STATEUNION_T9A043C39BC0361F60194C33113B629BD87A96EEF_H
#ifndef STRINGFACETSCHECKER_TBCB48F1B545187719429C40BE474CD7D6B0E93C0_H
#define STRINGFACETSCHECKER_TBCB48F1B545187719429C40BE474CD7D6B0E93C0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.StringFacetsChecker
struct  StringFacetsChecker_tBCB48F1B545187719429C40BE474CD7D6B0E93C0  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

struct StringFacetsChecker_tBCB48F1B545187719429C40BE474CD7D6B0E93C0_StaticFields
{
public:
	// System.Text.RegularExpressions.Regex System.Xml.Schema.StringFacetsChecker::languagePattern
	Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * ___languagePattern_0;

public:
	inline static int32_t get_offset_of_languagePattern_0() { return static_cast<int32_t>(offsetof(StringFacetsChecker_tBCB48F1B545187719429C40BE474CD7D6B0E93C0_StaticFields, ___languagePattern_0)); }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * get_languagePattern_0() const { return ___languagePattern_0; }
	inline Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF ** get_address_of_languagePattern_0() { return &___languagePattern_0; }
	inline void set_languagePattern_0(Regex_tFD46E63A462E852189FD6AB4E2B0B67C4D8FDBDF * value)
	{
		___languagePattern_0 = value;
		Il2CppCodeGenWriteBarrier((&___languagePattern_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // STRINGFACETSCHECKER_TBCB48F1B545187719429C40BE474CD7D6B0E93C0_H
#ifndef UNIONFACETSCHECKER_T6977021EC464B23381AD978173641C6E4B14840A_H
#define UNIONFACETSCHECKER_T6977021EC464B23381AD978173641C6E4B14840A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.UnionFacetsChecker
struct  UnionFacetsChecker_t6977021EC464B23381AD978173641C6E4B14840A  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // UNIONFACETSCHECKER_T6977021EC464B23381AD978173641C6E4B14840A_H
#ifndef XDRBUILDER_TD931491C090D5C32949D13F90BCAFC9A6380D3D2_H
#define XDRBUILDER_TD931491C090D5C32949D13F90BCAFC9A6380D3D2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XdrBuilder
struct  XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2  : public SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7
{
public:
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.XdrBuilder::_SchemaInfo
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ____SchemaInfo_14;
	// System.String System.Xml.Schema.XdrBuilder::_TargetNamespace
	String_t* ____TargetNamespace_15;
	// System.Xml.XmlReader System.Xml.Schema.XdrBuilder::_reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ____reader_16;
	// System.Xml.PositionInfo System.Xml.Schema.XdrBuilder::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_17;
	// System.Xml.Schema.ParticleContentValidator System.Xml.Schema.XdrBuilder::_contentValidator
	ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D * ____contentValidator_18;
	// System.Xml.Schema.XdrBuilder/XdrEntry System.Xml.Schema.XdrBuilder::_CurState
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 * ____CurState_19;
	// System.Xml.Schema.XdrBuilder/XdrEntry System.Xml.Schema.XdrBuilder::_NextState
	XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 * ____NextState_20;
	// System.Xml.HWStack System.Xml.Schema.XdrBuilder::_StateHistory
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ____StateHistory_21;
	// System.Xml.HWStack System.Xml.Schema.XdrBuilder::_GroupStack
	HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * ____GroupStack_22;
	// System.String System.Xml.Schema.XdrBuilder::_XdrName
	String_t* ____XdrName_23;
	// System.String System.Xml.Schema.XdrBuilder::_XdrPrefix
	String_t* ____XdrPrefix_24;
	// System.Xml.Schema.XdrBuilder/ElementContent System.Xml.Schema.XdrBuilder::_ElementDef
	ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E * ____ElementDef_25;
	// System.Xml.Schema.XdrBuilder/GroupContent System.Xml.Schema.XdrBuilder::_GroupDef
	GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE * ____GroupDef_26;
	// System.Xml.Schema.XdrBuilder/AttributeContent System.Xml.Schema.XdrBuilder::_AttributeDef
	AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290 * ____AttributeDef_27;
	// System.Xml.Schema.XdrBuilder/DeclBaseInfo System.Xml.Schema.XdrBuilder::_UndefinedAttributeTypes
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * ____UndefinedAttributeTypes_28;
	// System.Xml.Schema.XdrBuilder/DeclBaseInfo System.Xml.Schema.XdrBuilder::_BaseDecl
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * ____BaseDecl_29;
	// System.Xml.XmlNameTable System.Xml.Schema.XdrBuilder::_NameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ____NameTable_30;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.XdrBuilder::_SchemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ____SchemaNames_31;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XdrBuilder::_CurNsMgr
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ____CurNsMgr_32;
	// System.String System.Xml.Schema.XdrBuilder::_Text
	String_t* ____Text_33;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.XdrBuilder::validationEventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___validationEventHandler_34;
	// System.Collections.Hashtable System.Xml.Schema.XdrBuilder::_UndeclaredElements
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ____UndeclaredElements_35;
	// System.Xml.XmlResolver System.Xml.Schema.XdrBuilder::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_36;

public:
	inline static int32_t get_offset_of__SchemaInfo_14() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____SchemaInfo_14)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get__SchemaInfo_14() const { return ____SchemaInfo_14; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of__SchemaInfo_14() { return &____SchemaInfo_14; }
	inline void set__SchemaInfo_14(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		____SchemaInfo_14 = value;
		Il2CppCodeGenWriteBarrier((&____SchemaInfo_14), value);
	}

	inline static int32_t get_offset_of__TargetNamespace_15() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____TargetNamespace_15)); }
	inline String_t* get__TargetNamespace_15() const { return ____TargetNamespace_15; }
	inline String_t** get_address_of__TargetNamespace_15() { return &____TargetNamespace_15; }
	inline void set__TargetNamespace_15(String_t* value)
	{
		____TargetNamespace_15 = value;
		Il2CppCodeGenWriteBarrier((&____TargetNamespace_15), value);
	}

	inline static int32_t get_offset_of__reader_16() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____reader_16)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get__reader_16() const { return ____reader_16; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of__reader_16() { return &____reader_16; }
	inline void set__reader_16(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		____reader_16 = value;
		Il2CppCodeGenWriteBarrier((&____reader_16), value);
	}

	inline static int32_t get_offset_of_positionInfo_17() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ___positionInfo_17)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_17() const { return ___positionInfo_17; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_17() { return &___positionInfo_17; }
	inline void set_positionInfo_17(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_17 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_17), value);
	}

	inline static int32_t get_offset_of__contentValidator_18() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____contentValidator_18)); }
	inline ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D * get__contentValidator_18() const { return ____contentValidator_18; }
	inline ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D ** get_address_of__contentValidator_18() { return &____contentValidator_18; }
	inline void set__contentValidator_18(ParticleContentValidator_tFF273453F5523E636B8BE9C446AEE1F4090B8D1D * value)
	{
		____contentValidator_18 = value;
		Il2CppCodeGenWriteBarrier((&____contentValidator_18), value);
	}

	inline static int32_t get_offset_of__CurState_19() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____CurState_19)); }
	inline XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 * get__CurState_19() const { return ____CurState_19; }
	inline XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 ** get_address_of__CurState_19() { return &____CurState_19; }
	inline void set__CurState_19(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 * value)
	{
		____CurState_19 = value;
		Il2CppCodeGenWriteBarrier((&____CurState_19), value);
	}

	inline static int32_t get_offset_of__NextState_20() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____NextState_20)); }
	inline XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 * get__NextState_20() const { return ____NextState_20; }
	inline XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 ** get_address_of__NextState_20() { return &____NextState_20; }
	inline void set__NextState_20(XdrEntry_t3C189DCB0821841C390429A96D87E51B157480C6 * value)
	{
		____NextState_20 = value;
		Il2CppCodeGenWriteBarrier((&____NextState_20), value);
	}

	inline static int32_t get_offset_of__StateHistory_21() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____StateHistory_21)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get__StateHistory_21() const { return ____StateHistory_21; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of__StateHistory_21() { return &____StateHistory_21; }
	inline void set__StateHistory_21(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		____StateHistory_21 = value;
		Il2CppCodeGenWriteBarrier((&____StateHistory_21), value);
	}

	inline static int32_t get_offset_of__GroupStack_22() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____GroupStack_22)); }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * get__GroupStack_22() const { return ____GroupStack_22; }
	inline HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE ** get_address_of__GroupStack_22() { return &____GroupStack_22; }
	inline void set__GroupStack_22(HWStack_t2ECA5857AF5B3AF7C1BD51CEA64D131A6342BCDE * value)
	{
		____GroupStack_22 = value;
		Il2CppCodeGenWriteBarrier((&____GroupStack_22), value);
	}

	inline static int32_t get_offset_of__XdrName_23() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____XdrName_23)); }
	inline String_t* get__XdrName_23() const { return ____XdrName_23; }
	inline String_t** get_address_of__XdrName_23() { return &____XdrName_23; }
	inline void set__XdrName_23(String_t* value)
	{
		____XdrName_23 = value;
		Il2CppCodeGenWriteBarrier((&____XdrName_23), value);
	}

	inline static int32_t get_offset_of__XdrPrefix_24() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____XdrPrefix_24)); }
	inline String_t* get__XdrPrefix_24() const { return ____XdrPrefix_24; }
	inline String_t** get_address_of__XdrPrefix_24() { return &____XdrPrefix_24; }
	inline void set__XdrPrefix_24(String_t* value)
	{
		____XdrPrefix_24 = value;
		Il2CppCodeGenWriteBarrier((&____XdrPrefix_24), value);
	}

	inline static int32_t get_offset_of__ElementDef_25() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____ElementDef_25)); }
	inline ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E * get__ElementDef_25() const { return ____ElementDef_25; }
	inline ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E ** get_address_of__ElementDef_25() { return &____ElementDef_25; }
	inline void set__ElementDef_25(ElementContent_tB0084DB09B0B9A6BE2B4020A38D8CCC5A5AC741E * value)
	{
		____ElementDef_25 = value;
		Il2CppCodeGenWriteBarrier((&____ElementDef_25), value);
	}

	inline static int32_t get_offset_of__GroupDef_26() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____GroupDef_26)); }
	inline GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE * get__GroupDef_26() const { return ____GroupDef_26; }
	inline GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE ** get_address_of__GroupDef_26() { return &____GroupDef_26; }
	inline void set__GroupDef_26(GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE * value)
	{
		____GroupDef_26 = value;
		Il2CppCodeGenWriteBarrier((&____GroupDef_26), value);
	}

	inline static int32_t get_offset_of__AttributeDef_27() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____AttributeDef_27)); }
	inline AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290 * get__AttributeDef_27() const { return ____AttributeDef_27; }
	inline AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290 ** get_address_of__AttributeDef_27() { return &____AttributeDef_27; }
	inline void set__AttributeDef_27(AttributeContent_t3C46FE57445E1995CC5754D024077DFD26C0C290 * value)
	{
		____AttributeDef_27 = value;
		Il2CppCodeGenWriteBarrier((&____AttributeDef_27), value);
	}

	inline static int32_t get_offset_of__UndefinedAttributeTypes_28() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____UndefinedAttributeTypes_28)); }
	inline DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * get__UndefinedAttributeTypes_28() const { return ____UndefinedAttributeTypes_28; }
	inline DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B ** get_address_of__UndefinedAttributeTypes_28() { return &____UndefinedAttributeTypes_28; }
	inline void set__UndefinedAttributeTypes_28(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * value)
	{
		____UndefinedAttributeTypes_28 = value;
		Il2CppCodeGenWriteBarrier((&____UndefinedAttributeTypes_28), value);
	}

	inline static int32_t get_offset_of__BaseDecl_29() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____BaseDecl_29)); }
	inline DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * get__BaseDecl_29() const { return ____BaseDecl_29; }
	inline DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B ** get_address_of__BaseDecl_29() { return &____BaseDecl_29; }
	inline void set__BaseDecl_29(DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B * value)
	{
		____BaseDecl_29 = value;
		Il2CppCodeGenWriteBarrier((&____BaseDecl_29), value);
	}

	inline static int32_t get_offset_of__NameTable_30() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____NameTable_30)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get__NameTable_30() const { return ____NameTable_30; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of__NameTable_30() { return &____NameTable_30; }
	inline void set__NameTable_30(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		____NameTable_30 = value;
		Il2CppCodeGenWriteBarrier((&____NameTable_30), value);
	}

	inline static int32_t get_offset_of__SchemaNames_31() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____SchemaNames_31)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get__SchemaNames_31() const { return ____SchemaNames_31; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of__SchemaNames_31() { return &____SchemaNames_31; }
	inline void set__SchemaNames_31(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		____SchemaNames_31 = value;
		Il2CppCodeGenWriteBarrier((&____SchemaNames_31), value);
	}

	inline static int32_t get_offset_of__CurNsMgr_32() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____CurNsMgr_32)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get__CurNsMgr_32() const { return ____CurNsMgr_32; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of__CurNsMgr_32() { return &____CurNsMgr_32; }
	inline void set__CurNsMgr_32(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		____CurNsMgr_32 = value;
		Il2CppCodeGenWriteBarrier((&____CurNsMgr_32), value);
	}

	inline static int32_t get_offset_of__Text_33() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____Text_33)); }
	inline String_t* get__Text_33() const { return ____Text_33; }
	inline String_t** get_address_of__Text_33() { return &____Text_33; }
	inline void set__Text_33(String_t* value)
	{
		____Text_33 = value;
		Il2CppCodeGenWriteBarrier((&____Text_33), value);
	}

	inline static int32_t get_offset_of_validationEventHandler_34() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ___validationEventHandler_34)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_validationEventHandler_34() const { return ___validationEventHandler_34; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_validationEventHandler_34() { return &___validationEventHandler_34; }
	inline void set_validationEventHandler_34(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___validationEventHandler_34 = value;
		Il2CppCodeGenWriteBarrier((&___validationEventHandler_34), value);
	}

	inline static int32_t get_offset_of__UndeclaredElements_35() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ____UndeclaredElements_35)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get__UndeclaredElements_35() const { return ____UndeclaredElements_35; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of__UndeclaredElements_35() { return &____UndeclaredElements_35; }
	inline void set__UndeclaredElements_35(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		____UndeclaredElements_35 = value;
		Il2CppCodeGenWriteBarrier((&____UndeclaredElements_35), value);
	}

	inline static int32_t get_offset_of_xmlResolver_36() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2, ___xmlResolver_36)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_36() const { return ___xmlResolver_36; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_36() { return &___xmlResolver_36; }
	inline void set_xmlResolver_36(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_36 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_36), value);
	}
};

struct XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields
{
public:
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_Root_Element
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___S_XDR_Root_Element_0;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_Root_SubElements
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___S_XDR_Root_SubElements_1;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_ElementType_SubElements
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___S_XDR_ElementType_SubElements_2;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_AttributeType_SubElements
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___S_XDR_AttributeType_SubElements_3;
	// System.Int32[] System.Xml.Schema.XdrBuilder::S_XDR_Group_SubElements
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___S_XDR_Group_SubElements_4;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Root_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_Root_Attributes_5;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_ElementType_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_ElementType_Attributes_6;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_AttributeType_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_AttributeType_Attributes_7;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Element_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_Element_Attributes_8;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Attribute_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_Attribute_Attributes_9;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_Group_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_Group_Attributes_10;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_ElementDataType_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_ElementDataType_Attributes_11;
	// System.Xml.Schema.XdrBuilder/XdrAttributeEntry[] System.Xml.Schema.XdrBuilder::S_XDR_AttributeDataType_Attributes
	XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* ___S_XDR_AttributeDataType_Attributes_12;
	// System.Xml.Schema.XdrBuilder/XdrEntry[] System.Xml.Schema.XdrBuilder::S_SchemaEntries
	XdrEntryU5BU5D_t56D734CC5235B53EB1D962BE3A20E43D82692200* ___S_SchemaEntries_13;

public:
	inline static int32_t get_offset_of_S_XDR_Root_Element_0() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Root_Element_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_S_XDR_Root_Element_0() const { return ___S_XDR_Root_Element_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_S_XDR_Root_Element_0() { return &___S_XDR_Root_Element_0; }
	inline void set_S_XDR_Root_Element_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___S_XDR_Root_Element_0 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Root_Element_0), value);
	}

	inline static int32_t get_offset_of_S_XDR_Root_SubElements_1() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Root_SubElements_1)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_S_XDR_Root_SubElements_1() const { return ___S_XDR_Root_SubElements_1; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_S_XDR_Root_SubElements_1() { return &___S_XDR_Root_SubElements_1; }
	inline void set_S_XDR_Root_SubElements_1(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___S_XDR_Root_SubElements_1 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Root_SubElements_1), value);
	}

	inline static int32_t get_offset_of_S_XDR_ElementType_SubElements_2() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_ElementType_SubElements_2)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_S_XDR_ElementType_SubElements_2() const { return ___S_XDR_ElementType_SubElements_2; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_S_XDR_ElementType_SubElements_2() { return &___S_XDR_ElementType_SubElements_2; }
	inline void set_S_XDR_ElementType_SubElements_2(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___S_XDR_ElementType_SubElements_2 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_ElementType_SubElements_2), value);
	}

	inline static int32_t get_offset_of_S_XDR_AttributeType_SubElements_3() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_AttributeType_SubElements_3)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_S_XDR_AttributeType_SubElements_3() const { return ___S_XDR_AttributeType_SubElements_3; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_S_XDR_AttributeType_SubElements_3() { return &___S_XDR_AttributeType_SubElements_3; }
	inline void set_S_XDR_AttributeType_SubElements_3(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___S_XDR_AttributeType_SubElements_3 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_AttributeType_SubElements_3), value);
	}

	inline static int32_t get_offset_of_S_XDR_Group_SubElements_4() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Group_SubElements_4)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_S_XDR_Group_SubElements_4() const { return ___S_XDR_Group_SubElements_4; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_S_XDR_Group_SubElements_4() { return &___S_XDR_Group_SubElements_4; }
	inline void set_S_XDR_Group_SubElements_4(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___S_XDR_Group_SubElements_4 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Group_SubElements_4), value);
	}

	inline static int32_t get_offset_of_S_XDR_Root_Attributes_5() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Root_Attributes_5)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_Root_Attributes_5() const { return ___S_XDR_Root_Attributes_5; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_Root_Attributes_5() { return &___S_XDR_Root_Attributes_5; }
	inline void set_S_XDR_Root_Attributes_5(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_Root_Attributes_5 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Root_Attributes_5), value);
	}

	inline static int32_t get_offset_of_S_XDR_ElementType_Attributes_6() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_ElementType_Attributes_6)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_ElementType_Attributes_6() const { return ___S_XDR_ElementType_Attributes_6; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_ElementType_Attributes_6() { return &___S_XDR_ElementType_Attributes_6; }
	inline void set_S_XDR_ElementType_Attributes_6(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_ElementType_Attributes_6 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_ElementType_Attributes_6), value);
	}

	inline static int32_t get_offset_of_S_XDR_AttributeType_Attributes_7() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_AttributeType_Attributes_7)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_AttributeType_Attributes_7() const { return ___S_XDR_AttributeType_Attributes_7; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_AttributeType_Attributes_7() { return &___S_XDR_AttributeType_Attributes_7; }
	inline void set_S_XDR_AttributeType_Attributes_7(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_AttributeType_Attributes_7 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_AttributeType_Attributes_7), value);
	}

	inline static int32_t get_offset_of_S_XDR_Element_Attributes_8() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Element_Attributes_8)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_Element_Attributes_8() const { return ___S_XDR_Element_Attributes_8; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_Element_Attributes_8() { return &___S_XDR_Element_Attributes_8; }
	inline void set_S_XDR_Element_Attributes_8(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_Element_Attributes_8 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Element_Attributes_8), value);
	}

	inline static int32_t get_offset_of_S_XDR_Attribute_Attributes_9() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Attribute_Attributes_9)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_Attribute_Attributes_9() const { return ___S_XDR_Attribute_Attributes_9; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_Attribute_Attributes_9() { return &___S_XDR_Attribute_Attributes_9; }
	inline void set_S_XDR_Attribute_Attributes_9(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_Attribute_Attributes_9 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Attribute_Attributes_9), value);
	}

	inline static int32_t get_offset_of_S_XDR_Group_Attributes_10() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_Group_Attributes_10)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_Group_Attributes_10() const { return ___S_XDR_Group_Attributes_10; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_Group_Attributes_10() { return &___S_XDR_Group_Attributes_10; }
	inline void set_S_XDR_Group_Attributes_10(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_Group_Attributes_10 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_Group_Attributes_10), value);
	}

	inline static int32_t get_offset_of_S_XDR_ElementDataType_Attributes_11() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_ElementDataType_Attributes_11)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_ElementDataType_Attributes_11() const { return ___S_XDR_ElementDataType_Attributes_11; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_ElementDataType_Attributes_11() { return &___S_XDR_ElementDataType_Attributes_11; }
	inline void set_S_XDR_ElementDataType_Attributes_11(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_ElementDataType_Attributes_11 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_ElementDataType_Attributes_11), value);
	}

	inline static int32_t get_offset_of_S_XDR_AttributeDataType_Attributes_12() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_XDR_AttributeDataType_Attributes_12)); }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* get_S_XDR_AttributeDataType_Attributes_12() const { return ___S_XDR_AttributeDataType_Attributes_12; }
	inline XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B** get_address_of_S_XDR_AttributeDataType_Attributes_12() { return &___S_XDR_AttributeDataType_Attributes_12; }
	inline void set_S_XDR_AttributeDataType_Attributes_12(XdrAttributeEntryU5BU5D_t39CE0AD44B0B905E5A2E74975F637C787243320B* value)
	{
		___S_XDR_AttributeDataType_Attributes_12 = value;
		Il2CppCodeGenWriteBarrier((&___S_XDR_AttributeDataType_Attributes_12), value);
	}

	inline static int32_t get_offset_of_S_SchemaEntries_13() { return static_cast<int32_t>(offsetof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields, ___S_SchemaEntries_13)); }
	inline XdrEntryU5BU5D_t56D734CC5235B53EB1D962BE3A20E43D82692200* get_S_SchemaEntries_13() const { return ___S_SchemaEntries_13; }
	inline XdrEntryU5BU5D_t56D734CC5235B53EB1D962BE3A20E43D82692200** get_address_of_S_SchemaEntries_13() { return &___S_SchemaEntries_13; }
	inline void set_S_SchemaEntries_13(XdrEntryU5BU5D_t56D734CC5235B53EB1D962BE3A20E43D82692200* value)
	{
		___S_SchemaEntries_13 = value;
		Il2CppCodeGenWriteBarrier((&___S_SchemaEntries_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XDRBUILDER_TD931491C090D5C32949D13F90BCAFC9A6380D3D2_H
#ifndef XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#define XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.XmlCharType
struct  XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 
{
public:
	// System.Byte[] System.Xml.XmlCharType::charProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___charProperties_2;

public:
	inline static int32_t get_offset_of_charProperties_2() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9, ___charProperties_2)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_charProperties_2() const { return ___charProperties_2; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_charProperties_2() { return &___charProperties_2; }
	inline void set_charProperties_2(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___charProperties_2 = value;
		Il2CppCodeGenWriteBarrier((&___charProperties_2), value);
	}
};

struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields
{
public:
	// System.Object System.Xml.XmlCharType::s_Lock
	RuntimeObject * ___s_Lock_0;
	// System.Byte[] modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.XmlCharType::s_CharProperties
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___s_CharProperties_1;

public:
	inline static int32_t get_offset_of_s_Lock_0() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_Lock_0)); }
	inline RuntimeObject * get_s_Lock_0() const { return ___s_Lock_0; }
	inline RuntimeObject ** get_address_of_s_Lock_0() { return &___s_Lock_0; }
	inline void set_s_Lock_0(RuntimeObject * value)
	{
		___s_Lock_0 = value;
		Il2CppCodeGenWriteBarrier((&___s_Lock_0), value);
	}

	inline static int32_t get_offset_of_s_CharProperties_1() { return static_cast<int32_t>(offsetof(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_StaticFields, ___s_CharProperties_1)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_s_CharProperties_1() const { return ___s_CharProperties_1; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_s_CharProperties_1() { return &___s_CharProperties_1; }
	inline void set_s_CharProperties_1(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___s_CharProperties_1 = value;
		Il2CppCodeGenWriteBarrier((&___s_CharProperties_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_pinvoke
{
	uint8_t* ___charProperties_2;
};
// Native definition for COM marshalling of System.Xml.XmlCharType
struct XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_marshaled_com
{
	uint8_t* ___charProperties_2;
};
#endif // XMLCHARTYPE_T7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9_H
#ifndef DELEGATE_T_H
#define DELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Delegate
struct  Delegate_t  : public RuntimeObject
{
public:
	// System.IntPtr System.Delegate::method_ptr
	Il2CppMethodPointer ___method_ptr_0;
	// System.IntPtr System.Delegate::invoke_impl
	intptr_t ___invoke_impl_1;
	// System.Object System.Delegate::m_target
	RuntimeObject * ___m_target_2;
	// System.IntPtr System.Delegate::method
	intptr_t ___method_3;
	// System.IntPtr System.Delegate::delegate_trampoline
	intptr_t ___delegate_trampoline_4;
	// System.IntPtr System.Delegate::extra_arg
	intptr_t ___extra_arg_5;
	// System.IntPtr System.Delegate::method_code
	intptr_t ___method_code_6;
	// System.Reflection.MethodInfo System.Delegate::method_info
	MethodInfo_t * ___method_info_7;
	// System.Reflection.MethodInfo System.Delegate::original_method_info
	MethodInfo_t * ___original_method_info_8;
	// System.DelegateData System.Delegate::data
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	// System.Boolean System.Delegate::method_is_virtual
	bool ___method_is_virtual_10;

public:
	inline static int32_t get_offset_of_method_ptr_0() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_ptr_0)); }
	inline Il2CppMethodPointer get_method_ptr_0() const { return ___method_ptr_0; }
	inline Il2CppMethodPointer* get_address_of_method_ptr_0() { return &___method_ptr_0; }
	inline void set_method_ptr_0(Il2CppMethodPointer value)
	{
		___method_ptr_0 = value;
	}

	inline static int32_t get_offset_of_invoke_impl_1() { return static_cast<int32_t>(offsetof(Delegate_t, ___invoke_impl_1)); }
	inline intptr_t get_invoke_impl_1() const { return ___invoke_impl_1; }
	inline intptr_t* get_address_of_invoke_impl_1() { return &___invoke_impl_1; }
	inline void set_invoke_impl_1(intptr_t value)
	{
		___invoke_impl_1 = value;
	}

	inline static int32_t get_offset_of_m_target_2() { return static_cast<int32_t>(offsetof(Delegate_t, ___m_target_2)); }
	inline RuntimeObject * get_m_target_2() const { return ___m_target_2; }
	inline RuntimeObject ** get_address_of_m_target_2() { return &___m_target_2; }
	inline void set_m_target_2(RuntimeObject * value)
	{
		___m_target_2 = value;
		Il2CppCodeGenWriteBarrier((&___m_target_2), value);
	}

	inline static int32_t get_offset_of_method_3() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_3)); }
	inline intptr_t get_method_3() const { return ___method_3; }
	inline intptr_t* get_address_of_method_3() { return &___method_3; }
	inline void set_method_3(intptr_t value)
	{
		___method_3 = value;
	}

	inline static int32_t get_offset_of_delegate_trampoline_4() { return static_cast<int32_t>(offsetof(Delegate_t, ___delegate_trampoline_4)); }
	inline intptr_t get_delegate_trampoline_4() const { return ___delegate_trampoline_4; }
	inline intptr_t* get_address_of_delegate_trampoline_4() { return &___delegate_trampoline_4; }
	inline void set_delegate_trampoline_4(intptr_t value)
	{
		___delegate_trampoline_4 = value;
	}

	inline static int32_t get_offset_of_extra_arg_5() { return static_cast<int32_t>(offsetof(Delegate_t, ___extra_arg_5)); }
	inline intptr_t get_extra_arg_5() const { return ___extra_arg_5; }
	inline intptr_t* get_address_of_extra_arg_5() { return &___extra_arg_5; }
	inline void set_extra_arg_5(intptr_t value)
	{
		___extra_arg_5 = value;
	}

	inline static int32_t get_offset_of_method_code_6() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_code_6)); }
	inline intptr_t get_method_code_6() const { return ___method_code_6; }
	inline intptr_t* get_address_of_method_code_6() { return &___method_code_6; }
	inline void set_method_code_6(intptr_t value)
	{
		___method_code_6 = value;
	}

	inline static int32_t get_offset_of_method_info_7() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_info_7)); }
	inline MethodInfo_t * get_method_info_7() const { return ___method_info_7; }
	inline MethodInfo_t ** get_address_of_method_info_7() { return &___method_info_7; }
	inline void set_method_info_7(MethodInfo_t * value)
	{
		___method_info_7 = value;
		Il2CppCodeGenWriteBarrier((&___method_info_7), value);
	}

	inline static int32_t get_offset_of_original_method_info_8() { return static_cast<int32_t>(offsetof(Delegate_t, ___original_method_info_8)); }
	inline MethodInfo_t * get_original_method_info_8() const { return ___original_method_info_8; }
	inline MethodInfo_t ** get_address_of_original_method_info_8() { return &___original_method_info_8; }
	inline void set_original_method_info_8(MethodInfo_t * value)
	{
		___original_method_info_8 = value;
		Il2CppCodeGenWriteBarrier((&___original_method_info_8), value);
	}

	inline static int32_t get_offset_of_data_9() { return static_cast<int32_t>(offsetof(Delegate_t, ___data_9)); }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * get_data_9() const { return ___data_9; }
	inline DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE ** get_address_of_data_9() { return &___data_9; }
	inline void set_data_9(DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * value)
	{
		___data_9 = value;
		Il2CppCodeGenWriteBarrier((&___data_9), value);
	}

	inline static int32_t get_offset_of_method_is_virtual_10() { return static_cast<int32_t>(offsetof(Delegate_t, ___method_is_virtual_10)); }
	inline bool get_method_is_virtual_10() const { return ___method_is_virtual_10; }
	inline bool* get_address_of_method_is_virtual_10() { return &___method_is_virtual_10; }
	inline void set_method_is_virtual_10(bool value)
	{
		___method_is_virtual_10 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Delegate
struct Delegate_t_marshaled_pinvoke
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
// Native definition for COM marshalling of System.Delegate
struct Delegate_t_marshaled_com
{
	intptr_t ___method_ptr_0;
	intptr_t ___invoke_impl_1;
	Il2CppIUnknown* ___m_target_2;
	intptr_t ___method_3;
	intptr_t ___delegate_trampoline_4;
	intptr_t ___extra_arg_5;
	intptr_t ___method_code_6;
	MethodInfo_t * ___method_info_7;
	MethodInfo_t * ___original_method_info_8;
	DelegateData_t1BF9F691B56DAE5F8C28C5E084FDE94F15F27BBE * ___data_9;
	int32_t ___method_is_virtual_10;
};
#endif // DELEGATE_T_H
#ifndef ATTRIBUTEMATCHSTATE_T774FBBFD2CB98FEB43977A87396F624C329F5A59_H
#define ATTRIBUTEMATCHSTATE_T774FBBFD2CB98FEB43977A87396F624C329F5A59_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.AttributeMatchState
struct  AttributeMatchState_t774FBBFD2CB98FEB43977A87396F624C329F5A59 
{
public:
	// System.Int32 System.Xml.Schema.AttributeMatchState::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(AttributeMatchState_t774FBBFD2CB98FEB43977A87396F624C329F5A59, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ATTRIBUTEMATCHSTATE_T774FBBFD2CB98FEB43977A87396F624C329F5A59_H
#ifndef COMPOSITOR_TE5AE71A32402A0265385AABCE88457F1295A65E4_H
#define COMPOSITOR_TE5AE71A32402A0265385AABCE88457F1295A65E4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Compositor
struct  Compositor_tE5AE71A32402A0265385AABCE88457F1295A65E4 
{
public:
	// System.Int32 System.Xml.Schema.Compositor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compositor_tE5AE71A32402A0265385AABCE88457F1295A65E4, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITOR_TE5AE71A32402A0265385AABCE88457F1295A65E4_H
#ifndef LISTTYPE_T0322B2CB66D5748A27174A99131B5AD7A7ECDE0A_H
#define LISTTYPE_T0322B2CB66D5748A27174A99131B5AD7A7ECDE0A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceList/ListType
struct  ListType_t0322B2CB66D5748A27174A99131B5AD7A7ECDE0A 
{
public:
	// System.Int32 System.Xml.Schema.NamespaceList/ListType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(ListType_t0322B2CB66D5748A27174A99131B5AD7A7ECDE0A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LISTTYPE_T0322B2CB66D5748A27174A99131B5AD7A7ECDE0A_H
#ifndef NUMERIC10FACETSCHECKER_TE598B3CEA8A066DAE4A47471B80D3ABF017810DD_H
#define NUMERIC10FACETSCHECKER_TE598B3CEA8A066DAE4A47471B80D3ABF017810DD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Numeric10FacetsChecker
struct  Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD  : public FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6
{
public:
	// System.Decimal System.Xml.Schema.Numeric10FacetsChecker::maxValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___maxValue_1;
	// System.Decimal System.Xml.Schema.Numeric10FacetsChecker::minValue
	Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  ___minValue_2;

public:
	inline static int32_t get_offset_of_maxValue_1() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD, ___maxValue_1)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_maxValue_1() const { return ___maxValue_1; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_maxValue_1() { return &___maxValue_1; }
	inline void set_maxValue_1(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___maxValue_1 = value;
	}

	inline static int32_t get_offset_of_minValue_2() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD, ___minValue_2)); }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  get_minValue_2() const { return ___minValue_2; }
	inline Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8 * get_address_of_minValue_2() { return &___minValue_2; }
	inline void set_minValue_2(Decimal_t44EE9DA309A1BF848308DE4DDFC070CAE6D95EE8  value)
	{
		___minValue_2 = value;
	}
};

struct Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD_StaticFields
{
public:
	// System.Char[] System.Xml.Schema.Numeric10FacetsChecker::signs
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___signs_0;

public:
	inline static int32_t get_offset_of_signs_0() { return static_cast<int32_t>(offsetof(Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD_StaticFields, ___signs_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_signs_0() const { return ___signs_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_signs_0() { return &___signs_0; }
	inline void set_signs_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___signs_0 = value;
		Il2CppCodeGenWriteBarrier((&___signs_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NUMERIC10FACETSCHECKER_TE598B3CEA8A066DAE4A47471B80D3ABF017810DD_H
#ifndef RESTRICTIONFLAGS_T638CE72420C3F9885392EE7E57E54B5C9DEA7D8D_H
#define RESTRICTIONFLAGS_T638CE72420C3F9885392EE7E57E54B5C9DEA7D8D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.RestrictionFlags
struct  RestrictionFlags_t638CE72420C3F9885392EE7E57E54B5C9DEA7D8D 
{
public:
	// System.Int32 System.Xml.Schema.RestrictionFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(RestrictionFlags_t638CE72420C3F9885392EE7E57E54B5C9DEA7D8D, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESTRICTIONFLAGS_T638CE72420C3F9885392EE7E57E54B5C9DEA7D8D_H
#ifndef RESERVE_T4CDF234391D57884636B9A96328C5CFC79ACCF06_H
#define RESERVE_T4CDF234391D57884636B9A96328C5CFC79ACCF06_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaAttDef/Reserve
struct  Reserve_t4CDF234391D57884636B9A96328C5CFC79ACCF06 
{
public:
	// System.Int32 System.Xml.Schema.SchemaAttDef/Reserve::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Reserve_t4CDF234391D57884636B9A96328C5CFC79ACCF06, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESERVE_T4CDF234391D57884636B9A96328C5CFC79ACCF06_H
#ifndef COMPOSITOR_T7EC502C10E094F968544FD15F2EDB007B8C5C861_H
#define COMPOSITOR_T7EC502C10E094F968544FD15F2EDB007B8C5C861_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionPreprocessor/Compositor
struct  Compositor_t7EC502C10E094F968544FD15F2EDB007B8C5C861 
{
public:
	// System.Int32 System.Xml.Schema.SchemaCollectionPreprocessor/Compositor::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Compositor_t7EC502C10E094F968544FD15F2EDB007B8C5C861, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // COMPOSITOR_T7EC502C10E094F968544FD15F2EDB007B8C5C861_H
#ifndef USE_T8F6205841F1E8F0578F601AC12C06E6C645B0CC5_H
#define USE_T8F6205841F1E8F0578F601AC12C06E6C645B0CC5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaDeclBase/Use
struct  Use_t8F6205841F1E8F0578F601AC12C06E6C645B0CC5 
{
public:
	// System.Int32 System.Xml.Schema.SchemaDeclBase/Use::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Use_t8F6205841F1E8F0578F601AC12C06E6C645B0CC5, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // USE_T8F6205841F1E8F0578F601AC12C06E6C645B0CC5_H
#ifndef TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#define TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaNames/Token
struct  Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A 
{
public:
	// System.Int32 System.Xml.Schema.SchemaNames/Token::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // TOKEN_T29EB684F3183FA0B300324C6C6BA75403A81FC9A_H
#ifndef SCHEMATYPE_T0EE4FECE10D4045A0C84698FEFBA18D9C34992DF_H
#define SCHEMATYPE_T0EE4FECE10D4045A0C84698FEFBA18D9C34992DF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaType
struct  SchemaType_t0EE4FECE10D4045A0C84698FEFBA18D9C34992DF 
{
public:
	// System.Int32 System.Xml.Schema.SchemaType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(SchemaType_t0EE4FECE10D4045A0C84698FEFBA18D9C34992DF, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMATYPE_T0EE4FECE10D4045A0C84698FEFBA18D9C34992DF_H
#ifndef XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#define XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaContentProcessing
struct  XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaContentProcessing::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaContentProcessing_tD85295E4C6FA245DAE6F9E4E1C927E76678B197A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMACONTENTPROCESSING_TD85295E4C6FA245DAE6F9E4E1C927E76678B197A_H
#ifndef XMLSCHEMADATATYPEVARIETY_T26489B3A13FBF83AEBD1E94ECACD30AF091C9A02_H
#define XMLSCHEMADATATYPEVARIETY_T26489B3A13FBF83AEBD1E94ECACD30AF091C9A02_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDatatypeVariety
struct  XmlSchemaDatatypeVariety_t26489B3A13FBF83AEBD1E94ECACD30AF091C9A02 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDatatypeVariety::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaDatatypeVariety_t26489B3A13FBF83AEBD1E94ECACD30AF091C9A02, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADATATYPEVARIETY_T26489B3A13FBF83AEBD1E94ECACD30AF091C9A02_H
#ifndef XMLSCHEMADERIVATIONMETHOD_T9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58_H
#define XMLSCHEMADERIVATIONMETHOD_T9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaDerivationMethod
struct  XmlSchemaDerivationMethod_t9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaDerivationMethod::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaDerivationMethod_t9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMADERIVATIONMETHOD_T9C964CFFC430E62A1FF62E70B9FE2C976A3A5F58_H
#ifndef XMLSCHEMAEXCEPTION_T6E118FD214784A2E7DE004B99148C2C4CCC1EE65_H
#define XMLSCHEMAEXCEPTION_T6E118FD214784A2E7DE004B99148C2C4CCC1EE65_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaException
struct  XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.Xml.Schema.XmlSchemaException::res
	String_t* ___res_17;
	// System.String[] System.Xml.Schema.XmlSchemaException::args
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ___args_18;
	// System.String System.Xml.Schema.XmlSchemaException::sourceUri
	String_t* ___sourceUri_19;
	// System.Int32 System.Xml.Schema.XmlSchemaException::lineNumber
	int32_t ___lineNumber_20;
	// System.Int32 System.Xml.Schema.XmlSchemaException::linePosition
	int32_t ___linePosition_21;
	// System.Xml.Schema.XmlSchemaObject System.Xml.Schema.XmlSchemaException::sourceSchemaObject
	XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * ___sourceSchemaObject_22;
	// System.String System.Xml.Schema.XmlSchemaException::message
	String_t* ___message_23;

public:
	inline static int32_t get_offset_of_res_17() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___res_17)); }
	inline String_t* get_res_17() const { return ___res_17; }
	inline String_t** get_address_of_res_17() { return &___res_17; }
	inline void set_res_17(String_t* value)
	{
		___res_17 = value;
		Il2CppCodeGenWriteBarrier((&___res_17), value);
	}

	inline static int32_t get_offset_of_args_18() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___args_18)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get_args_18() const { return ___args_18; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of_args_18() { return &___args_18; }
	inline void set_args_18(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		___args_18 = value;
		Il2CppCodeGenWriteBarrier((&___args_18), value);
	}

	inline static int32_t get_offset_of_sourceUri_19() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___sourceUri_19)); }
	inline String_t* get_sourceUri_19() const { return ___sourceUri_19; }
	inline String_t** get_address_of_sourceUri_19() { return &___sourceUri_19; }
	inline void set_sourceUri_19(String_t* value)
	{
		___sourceUri_19 = value;
		Il2CppCodeGenWriteBarrier((&___sourceUri_19), value);
	}

	inline static int32_t get_offset_of_lineNumber_20() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___lineNumber_20)); }
	inline int32_t get_lineNumber_20() const { return ___lineNumber_20; }
	inline int32_t* get_address_of_lineNumber_20() { return &___lineNumber_20; }
	inline void set_lineNumber_20(int32_t value)
	{
		___lineNumber_20 = value;
	}

	inline static int32_t get_offset_of_linePosition_21() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___linePosition_21)); }
	inline int32_t get_linePosition_21() const { return ___linePosition_21; }
	inline int32_t* get_address_of_linePosition_21() { return &___linePosition_21; }
	inline void set_linePosition_21(int32_t value)
	{
		___linePosition_21 = value;
	}

	inline static int32_t get_offset_of_sourceSchemaObject_22() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___sourceSchemaObject_22)); }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * get_sourceSchemaObject_22() const { return ___sourceSchemaObject_22; }
	inline XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B ** get_address_of_sourceSchemaObject_22() { return &___sourceSchemaObject_22; }
	inline void set_sourceSchemaObject_22(XmlSchemaObject_tB5695348FF2B08149CAE95CD10F39F21EDB1F57B * value)
	{
		___sourceSchemaObject_22 = value;
		Il2CppCodeGenWriteBarrier((&___sourceSchemaObject_22), value);
	}

	inline static int32_t get_offset_of_message_23() { return static_cast<int32_t>(offsetof(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65, ___message_23)); }
	inline String_t* get_message_23() const { return ___message_23; }
	inline String_t** get_address_of_message_23() { return &___message_23; }
	inline void set_message_23(String_t* value)
	{
		___message_23 = value;
		Il2CppCodeGenWriteBarrier((&___message_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAEXCEPTION_T6E118FD214784A2E7DE004B99148C2C4CCC1EE65_H
#ifndef XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#define XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaForm
struct  XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaForm::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaForm_t86E3CD541907A35AC5B505EF118C3E20CC760276, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAFORM_T86E3CD541907A35AC5B505EF118C3E20CC760276_H
#ifndef INFERENCEOPTION_TDBA9EFB8F63C6E79C196133F252E07BF76503C80_H
#define INFERENCEOPTION_TDBA9EFB8F63C6E79C196133F252E07BF76503C80_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInference/InferenceOption
struct  InferenceOption_tDBA9EFB8F63C6E79C196133F252E07BF76503C80 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaInference/InferenceOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InferenceOption_tDBA9EFB8F63C6E79C196133F252E07BF76503C80, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INFERENCEOPTION_TDBA9EFB8F63C6E79C196133F252E07BF76503C80_H
#ifndef XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#define XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaValidity
struct  XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621 
{
public:
	// System.Int32 System.Xml.Schema.XmlSchemaValidity::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSchemaValidity_tFB9AEE7708C4FF08DC1564482F46991CF8FD0621, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAVALIDITY_TFB9AEE7708C4FF08DC1564482F46991CF8FD0621_H
#ifndef XMLSEVERITYTYPE_TC3AD578830B568AE8D5455F146A99305BEE0501A_H
#define XMLSEVERITYTYPE_TC3AD578830B568AE8D5455F146A99305BEE0501A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSeverityType
struct  XmlSeverityType_tC3AD578830B568AE8D5455F146A99305BEE0501A 
{
public:
	// System.Int32 System.Xml.Schema.XmlSeverityType::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlSeverityType_tC3AD578830B568AE8D5455F146A99305BEE0501A, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSEVERITYTYPE_TC3AD578830B568AE8D5455F146A99305BEE0501A_H
#ifndef XMLTYPECODE_T9C4AD5729574C591993F2559021E198BED5252A2_H
#define XMLTYPECODE_T9C4AD5729574C591993F2559021E198BED5252A2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlTypeCode
struct  XmlTypeCode_t9C4AD5729574C591993F2559021E198BED5252A2 
{
public:
	// System.Int32 System.Xml.Schema.XmlTypeCode::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XmlTypeCode_t9C4AD5729574C591993F2559021E198BED5252A2, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLTYPECODE_T9C4AD5729574C591993F2559021E198BED5252A2_H
#ifndef XSDDATETIMEFLAGS_TC892A580DE494CDB02FF5D29F914ECDC49F858C8_H
#define XSDDATETIMEFLAGS_TC892A580DE494CDB02FF5D29F914ECDC49F858C8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XsdDateTimeFlags
struct  XsdDateTimeFlags_tC892A580DE494CDB02FF5D29F914ECDC49F858C8 
{
public:
	// System.Int32 System.Xml.Schema.XsdDateTimeFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(XsdDateTimeFlags_tC892A580DE494CDB02FF5D29F914ECDC49F858C8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XSDDATETIMEFLAGS_TC892A580DE494CDB02FF5D29F914ECDC49F858C8_H
#ifndef MULTICASTDELEGATE_T_H
#define MULTICASTDELEGATE_T_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.MulticastDelegate
struct  MulticastDelegate_t  : public Delegate_t
{
public:
	// System.Delegate[] System.MulticastDelegate::delegates
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;

public:
	inline static int32_t get_offset_of_delegates_11() { return static_cast<int32_t>(offsetof(MulticastDelegate_t, ___delegates_11)); }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* get_delegates_11() const { return ___delegates_11; }
	inline DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86** get_address_of_delegates_11() { return &___delegates_11; }
	inline void set_delegates_11(DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* value)
	{
		___delegates_11 = value;
		Il2CppCodeGenWriteBarrier((&___delegates_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_pinvoke : public Delegate_t_marshaled_pinvoke
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
// Native definition for COM marshalling of System.MulticastDelegate
struct MulticastDelegate_t_marshaled_com : public Delegate_t_marshaled_com
{
	DelegateU5BU5D_tDFCDEE2A6322F96C0FE49AF47E9ADB8C4B294E86* ___delegates_11;
};
#endif // MULTICASTDELEGATE_T_H
#ifndef DATATYPEIMPLEMENTATION_TB5894E69998AF6871FDFC14B2A95AE5F07CBB836_H
#define DATATYPEIMPLEMENTATION_TB5894E69998AF6871FDFC14B2A95AE5F07CBB836_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.DatatypeImplementation
struct  DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836  : public XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550
{
public:
	// System.Xml.Schema.XmlSchemaDatatypeVariety System.Xml.Schema.DatatypeImplementation::variety
	int32_t ___variety_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.DatatypeImplementation::restriction
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * ___restriction_1;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::baseType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___baseType_2;
	// System.Xml.Schema.XmlValueConverter System.Xml.Schema.DatatypeImplementation::valueConverter
	XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * ___valueConverter_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.DatatypeImplementation::parentSchemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___parentSchemaType_4;

public:
	inline static int32_t get_offset_of_variety_0() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___variety_0)); }
	inline int32_t get_variety_0() const { return ___variety_0; }
	inline int32_t* get_address_of_variety_0() { return &___variety_0; }
	inline void set_variety_0(int32_t value)
	{
		___variety_0 = value;
	}

	inline static int32_t get_offset_of_restriction_1() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___restriction_1)); }
	inline RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * get_restriction_1() const { return ___restriction_1; }
	inline RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 ** get_address_of_restriction_1() { return &___restriction_1; }
	inline void set_restriction_1(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * value)
	{
		___restriction_1 = value;
		Il2CppCodeGenWriteBarrier((&___restriction_1), value);
	}

	inline static int32_t get_offset_of_baseType_2() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___baseType_2)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_baseType_2() const { return ___baseType_2; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_baseType_2() { return &___baseType_2; }
	inline void set_baseType_2(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___baseType_2 = value;
		Il2CppCodeGenWriteBarrier((&___baseType_2), value);
	}

	inline static int32_t get_offset_of_valueConverter_3() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___valueConverter_3)); }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * get_valueConverter_3() const { return ___valueConverter_3; }
	inline XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E ** get_address_of_valueConverter_3() { return &___valueConverter_3; }
	inline void set_valueConverter_3(XmlValueConverter_tC6BABF8791F0E88864F8707A10F63FBE9EA0D69E * value)
	{
		___valueConverter_3 = value;
		Il2CppCodeGenWriteBarrier((&___valueConverter_3), value);
	}

	inline static int32_t get_offset_of_parentSchemaType_4() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836, ___parentSchemaType_4)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_parentSchemaType_4() const { return ___parentSchemaType_4; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_parentSchemaType_4() { return &___parentSchemaType_4; }
	inline void set_parentSchemaType_4(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___parentSchemaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___parentSchemaType_4), value);
	}
};

struct DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields
{
public:
	// System.Collections.Hashtable System.Xml.Schema.DatatypeImplementation::builtinTypes
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___builtinTypes_5;
	// System.Xml.Schema.XmlSchemaSimpleType[] System.Xml.Schema.DatatypeImplementation::enumToTypeCode
	XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* ___enumToTypeCode_6;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anySimpleType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___anySimpleType_7;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::anyAtomicType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___anyAtomicType_8;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::untypedAtomicType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___untypedAtomicType_9;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::yearMonthDurationType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___yearMonthDurationType_10;
	// System.Xml.Schema.XmlSchemaSimpleType System.Xml.Schema.DatatypeImplementation::dayTimeDurationType
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___dayTimeDurationType_11;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::normalizedStringTypeV1Compat
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___normalizedStringTypeV1Compat_12;
	// System.Xml.Schema.XmlSchemaSimpleType modreq(System.Runtime.CompilerServices.IsVolatile) System.Xml.Schema.DatatypeImplementation::tokenTypeV1Compat
	XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * ___tokenTypeV1Compat_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnySimpleType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnAnySimpleType_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.DatatypeImplementation::QnAnyType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___QnAnyType_15;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::stringFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___stringFacetsChecker_16;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::miscFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___miscFacetsChecker_17;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::numeric2FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric2FacetsChecker_18;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::binaryFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___binaryFacetsChecker_19;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::dateTimeFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___dateTimeFacetsChecker_20;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::durationFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___durationFacetsChecker_21;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::listFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___listFacetsChecker_22;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::qnameFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___qnameFacetsChecker_23;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.DatatypeImplementation::unionFacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___unionFacetsChecker_24;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anySimpleType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_anySimpleType_25;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyURI
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_anyURI_26;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_base64Binary
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_base64Binary_27;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_boolean
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_boolean_28;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_byte
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_byte_29;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_char
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_char_30;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_date
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_date_31;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTime
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dateTime_32;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeNoTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dateTimeNoTz_33;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dateTimeTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dateTimeTz_34;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_day
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_day_35;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_decimal
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_decimal_36;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_double
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_double_37;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_doubleXdr
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_doubleXdr_38;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_duration
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_duration_39;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITY
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ENTITY_40;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENTITIES
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ENTITIES_41;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ENUMERATION
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ENUMERATION_42;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_fixed
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_fixed_43;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_float
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_float_44;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_floatXdr
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_floatXdr_45;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_hexBinary
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_hexBinary_46;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_ID
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_ID_47;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREF
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_IDREF_48;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_IDREFS
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_IDREFS_49;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_int
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_int_50;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_integer
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_integer_51;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_language
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_language_52;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_long
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_long_53;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_month
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_month_54;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_monthDay
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_monthDay_55;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_Name
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_Name_56;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NCName
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NCName_57;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_negativeInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_negativeInteger_58;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKEN
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NMTOKEN_59;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NMTOKENS
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NMTOKENS_60;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonNegativeInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_nonNegativeInteger_61;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_nonPositiveInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_nonPositiveInteger_62;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedString
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_normalizedString_63;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_NOTATION
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_NOTATION_64;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_positiveInteger
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_positiveInteger_65;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QName
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_QName_66;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_QNameXdr
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_QNameXdr_67;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_short
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_short_68;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_string
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_string_69;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_time
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_time_70;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeNoTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_timeNoTz_71;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_timeTz
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_timeTz_72;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_token
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_token_73;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedByte
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedByte_74;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedInt
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedInt_75;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedLong
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedLong_76;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_unsignedShort
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_unsignedShort_77;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_uuid
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_uuid_78;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_year
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_year_79;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonth
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_yearMonth_80;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_normalizedStringV1Compat
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_normalizedStringV1Compat_81;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_tokenV1Compat
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_tokenV1Compat_82;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_anyAtomicType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_anyAtomicType_83;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_dayTimeDuration
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_dayTimeDuration_84;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_untypedAtomicType
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_untypedAtomicType_85;
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.DatatypeImplementation::c_yearMonthDuration
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___c_yearMonthDuration_86;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypes
	DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* ___c_tokenizedTypes_87;
	// System.Xml.Schema.DatatypeImplementation[] System.Xml.Schema.DatatypeImplementation::c_tokenizedTypesXsd
	DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* ___c_tokenizedTypesXsd_88;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XdrTypes
	SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* ___c_XdrTypes_89;
	// System.Xml.Schema.DatatypeImplementation/SchemaDatatypeMap[] System.Xml.Schema.DatatypeImplementation::c_XsdTypes
	SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* ___c_XsdTypes_90;

public:
	inline static int32_t get_offset_of_builtinTypes_5() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___builtinTypes_5)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_builtinTypes_5() const { return ___builtinTypes_5; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_builtinTypes_5() { return &___builtinTypes_5; }
	inline void set_builtinTypes_5(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___builtinTypes_5 = value;
		Il2CppCodeGenWriteBarrier((&___builtinTypes_5), value);
	}

	inline static int32_t get_offset_of_enumToTypeCode_6() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___enumToTypeCode_6)); }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* get_enumToTypeCode_6() const { return ___enumToTypeCode_6; }
	inline XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B** get_address_of_enumToTypeCode_6() { return &___enumToTypeCode_6; }
	inline void set_enumToTypeCode_6(XmlSchemaSimpleTypeU5BU5D_t63A9F01B50EB12BB7D8DDD5EB70FE08F066DC59B* value)
	{
		___enumToTypeCode_6 = value;
		Il2CppCodeGenWriteBarrier((&___enumToTypeCode_6), value);
	}

	inline static int32_t get_offset_of_anySimpleType_7() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___anySimpleType_7)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_anySimpleType_7() const { return ___anySimpleType_7; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_anySimpleType_7() { return &___anySimpleType_7; }
	inline void set_anySimpleType_7(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___anySimpleType_7 = value;
		Il2CppCodeGenWriteBarrier((&___anySimpleType_7), value);
	}

	inline static int32_t get_offset_of_anyAtomicType_8() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___anyAtomicType_8)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_anyAtomicType_8() const { return ___anyAtomicType_8; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_anyAtomicType_8() { return &___anyAtomicType_8; }
	inline void set_anyAtomicType_8(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___anyAtomicType_8 = value;
		Il2CppCodeGenWriteBarrier((&___anyAtomicType_8), value);
	}

	inline static int32_t get_offset_of_untypedAtomicType_9() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___untypedAtomicType_9)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_untypedAtomicType_9() const { return ___untypedAtomicType_9; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_untypedAtomicType_9() { return &___untypedAtomicType_9; }
	inline void set_untypedAtomicType_9(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___untypedAtomicType_9 = value;
		Il2CppCodeGenWriteBarrier((&___untypedAtomicType_9), value);
	}

	inline static int32_t get_offset_of_yearMonthDurationType_10() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___yearMonthDurationType_10)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_yearMonthDurationType_10() const { return ___yearMonthDurationType_10; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_yearMonthDurationType_10() { return &___yearMonthDurationType_10; }
	inline void set_yearMonthDurationType_10(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___yearMonthDurationType_10 = value;
		Il2CppCodeGenWriteBarrier((&___yearMonthDurationType_10), value);
	}

	inline static int32_t get_offset_of_dayTimeDurationType_11() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___dayTimeDurationType_11)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_dayTimeDurationType_11() const { return ___dayTimeDurationType_11; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_dayTimeDurationType_11() { return &___dayTimeDurationType_11; }
	inline void set_dayTimeDurationType_11(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___dayTimeDurationType_11 = value;
		Il2CppCodeGenWriteBarrier((&___dayTimeDurationType_11), value);
	}

	inline static int32_t get_offset_of_normalizedStringTypeV1Compat_12() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___normalizedStringTypeV1Compat_12)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_normalizedStringTypeV1Compat_12() const { return ___normalizedStringTypeV1Compat_12; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_normalizedStringTypeV1Compat_12() { return &___normalizedStringTypeV1Compat_12; }
	inline void set_normalizedStringTypeV1Compat_12(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___normalizedStringTypeV1Compat_12 = value;
		Il2CppCodeGenWriteBarrier((&___normalizedStringTypeV1Compat_12), value);
	}

	inline static int32_t get_offset_of_tokenTypeV1Compat_13() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___tokenTypeV1Compat_13)); }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * get_tokenTypeV1Compat_13() const { return ___tokenTypeV1Compat_13; }
	inline XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A ** get_address_of_tokenTypeV1Compat_13() { return &___tokenTypeV1Compat_13; }
	inline void set_tokenTypeV1Compat_13(XmlSchemaSimpleType_t3E090F6F088E02B69D984EED6C2A209ACB42A68A * value)
	{
		___tokenTypeV1Compat_13 = value;
		Il2CppCodeGenWriteBarrier((&___tokenTypeV1Compat_13), value);
	}

	inline static int32_t get_offset_of_QnAnySimpleType_14() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___QnAnySimpleType_14)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnAnySimpleType_14() const { return ___QnAnySimpleType_14; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnAnySimpleType_14() { return &___QnAnySimpleType_14; }
	inline void set_QnAnySimpleType_14(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnAnySimpleType_14 = value;
		Il2CppCodeGenWriteBarrier((&___QnAnySimpleType_14), value);
	}

	inline static int32_t get_offset_of_QnAnyType_15() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___QnAnyType_15)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_QnAnyType_15() const { return ___QnAnyType_15; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_QnAnyType_15() { return &___QnAnyType_15; }
	inline void set_QnAnyType_15(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___QnAnyType_15 = value;
		Il2CppCodeGenWriteBarrier((&___QnAnyType_15), value);
	}

	inline static int32_t get_offset_of_stringFacetsChecker_16() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___stringFacetsChecker_16)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_stringFacetsChecker_16() const { return ___stringFacetsChecker_16; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_stringFacetsChecker_16() { return &___stringFacetsChecker_16; }
	inline void set_stringFacetsChecker_16(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___stringFacetsChecker_16 = value;
		Il2CppCodeGenWriteBarrier((&___stringFacetsChecker_16), value);
	}

	inline static int32_t get_offset_of_miscFacetsChecker_17() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___miscFacetsChecker_17)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_miscFacetsChecker_17() const { return ___miscFacetsChecker_17; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_miscFacetsChecker_17() { return &___miscFacetsChecker_17; }
	inline void set_miscFacetsChecker_17(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___miscFacetsChecker_17 = value;
		Il2CppCodeGenWriteBarrier((&___miscFacetsChecker_17), value);
	}

	inline static int32_t get_offset_of_numeric2FacetsChecker_18() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___numeric2FacetsChecker_18)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric2FacetsChecker_18() const { return ___numeric2FacetsChecker_18; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric2FacetsChecker_18() { return &___numeric2FacetsChecker_18; }
	inline void set_numeric2FacetsChecker_18(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric2FacetsChecker_18 = value;
		Il2CppCodeGenWriteBarrier((&___numeric2FacetsChecker_18), value);
	}

	inline static int32_t get_offset_of_binaryFacetsChecker_19() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___binaryFacetsChecker_19)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_binaryFacetsChecker_19() const { return ___binaryFacetsChecker_19; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_binaryFacetsChecker_19() { return &___binaryFacetsChecker_19; }
	inline void set_binaryFacetsChecker_19(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___binaryFacetsChecker_19 = value;
		Il2CppCodeGenWriteBarrier((&___binaryFacetsChecker_19), value);
	}

	inline static int32_t get_offset_of_dateTimeFacetsChecker_20() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___dateTimeFacetsChecker_20)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_dateTimeFacetsChecker_20() const { return ___dateTimeFacetsChecker_20; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_dateTimeFacetsChecker_20() { return &___dateTimeFacetsChecker_20; }
	inline void set_dateTimeFacetsChecker_20(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___dateTimeFacetsChecker_20 = value;
		Il2CppCodeGenWriteBarrier((&___dateTimeFacetsChecker_20), value);
	}

	inline static int32_t get_offset_of_durationFacetsChecker_21() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___durationFacetsChecker_21)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_durationFacetsChecker_21() const { return ___durationFacetsChecker_21; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_durationFacetsChecker_21() { return &___durationFacetsChecker_21; }
	inline void set_durationFacetsChecker_21(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___durationFacetsChecker_21 = value;
		Il2CppCodeGenWriteBarrier((&___durationFacetsChecker_21), value);
	}

	inline static int32_t get_offset_of_listFacetsChecker_22() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___listFacetsChecker_22)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_listFacetsChecker_22() const { return ___listFacetsChecker_22; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_listFacetsChecker_22() { return &___listFacetsChecker_22; }
	inline void set_listFacetsChecker_22(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___listFacetsChecker_22 = value;
		Il2CppCodeGenWriteBarrier((&___listFacetsChecker_22), value);
	}

	inline static int32_t get_offset_of_qnameFacetsChecker_23() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___qnameFacetsChecker_23)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_qnameFacetsChecker_23() const { return ___qnameFacetsChecker_23; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_qnameFacetsChecker_23() { return &___qnameFacetsChecker_23; }
	inline void set_qnameFacetsChecker_23(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___qnameFacetsChecker_23 = value;
		Il2CppCodeGenWriteBarrier((&___qnameFacetsChecker_23), value);
	}

	inline static int32_t get_offset_of_unionFacetsChecker_24() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___unionFacetsChecker_24)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_unionFacetsChecker_24() const { return ___unionFacetsChecker_24; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_unionFacetsChecker_24() { return &___unionFacetsChecker_24; }
	inline void set_unionFacetsChecker_24(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___unionFacetsChecker_24 = value;
		Il2CppCodeGenWriteBarrier((&___unionFacetsChecker_24), value);
	}

	inline static int32_t get_offset_of_c_anySimpleType_25() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_anySimpleType_25)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_anySimpleType_25() const { return ___c_anySimpleType_25; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_anySimpleType_25() { return &___c_anySimpleType_25; }
	inline void set_c_anySimpleType_25(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_anySimpleType_25 = value;
		Il2CppCodeGenWriteBarrier((&___c_anySimpleType_25), value);
	}

	inline static int32_t get_offset_of_c_anyURI_26() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_anyURI_26)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_anyURI_26() const { return ___c_anyURI_26; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_anyURI_26() { return &___c_anyURI_26; }
	inline void set_c_anyURI_26(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_anyURI_26 = value;
		Il2CppCodeGenWriteBarrier((&___c_anyURI_26), value);
	}

	inline static int32_t get_offset_of_c_base64Binary_27() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_base64Binary_27)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_base64Binary_27() const { return ___c_base64Binary_27; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_base64Binary_27() { return &___c_base64Binary_27; }
	inline void set_c_base64Binary_27(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_base64Binary_27 = value;
		Il2CppCodeGenWriteBarrier((&___c_base64Binary_27), value);
	}

	inline static int32_t get_offset_of_c_boolean_28() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_boolean_28)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_boolean_28() const { return ___c_boolean_28; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_boolean_28() { return &___c_boolean_28; }
	inline void set_c_boolean_28(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_boolean_28 = value;
		Il2CppCodeGenWriteBarrier((&___c_boolean_28), value);
	}

	inline static int32_t get_offset_of_c_byte_29() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_byte_29)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_byte_29() const { return ___c_byte_29; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_byte_29() { return &___c_byte_29; }
	inline void set_c_byte_29(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_byte_29 = value;
		Il2CppCodeGenWriteBarrier((&___c_byte_29), value);
	}

	inline static int32_t get_offset_of_c_char_30() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_char_30)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_char_30() const { return ___c_char_30; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_char_30() { return &___c_char_30; }
	inline void set_c_char_30(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_char_30 = value;
		Il2CppCodeGenWriteBarrier((&___c_char_30), value);
	}

	inline static int32_t get_offset_of_c_date_31() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_date_31)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_date_31() const { return ___c_date_31; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_date_31() { return &___c_date_31; }
	inline void set_c_date_31(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_date_31 = value;
		Il2CppCodeGenWriteBarrier((&___c_date_31), value);
	}

	inline static int32_t get_offset_of_c_dateTime_32() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dateTime_32)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dateTime_32() const { return ___c_dateTime_32; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dateTime_32() { return &___c_dateTime_32; }
	inline void set_c_dateTime_32(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dateTime_32 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTime_32), value);
	}

	inline static int32_t get_offset_of_c_dateTimeNoTz_33() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dateTimeNoTz_33)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dateTimeNoTz_33() const { return ___c_dateTimeNoTz_33; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dateTimeNoTz_33() { return &___c_dateTimeNoTz_33; }
	inline void set_c_dateTimeNoTz_33(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dateTimeNoTz_33 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTimeNoTz_33), value);
	}

	inline static int32_t get_offset_of_c_dateTimeTz_34() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dateTimeTz_34)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dateTimeTz_34() const { return ___c_dateTimeTz_34; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dateTimeTz_34() { return &___c_dateTimeTz_34; }
	inline void set_c_dateTimeTz_34(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dateTimeTz_34 = value;
		Il2CppCodeGenWriteBarrier((&___c_dateTimeTz_34), value);
	}

	inline static int32_t get_offset_of_c_day_35() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_day_35)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_day_35() const { return ___c_day_35; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_day_35() { return &___c_day_35; }
	inline void set_c_day_35(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_day_35 = value;
		Il2CppCodeGenWriteBarrier((&___c_day_35), value);
	}

	inline static int32_t get_offset_of_c_decimal_36() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_decimal_36)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_decimal_36() const { return ___c_decimal_36; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_decimal_36() { return &___c_decimal_36; }
	inline void set_c_decimal_36(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_decimal_36 = value;
		Il2CppCodeGenWriteBarrier((&___c_decimal_36), value);
	}

	inline static int32_t get_offset_of_c_double_37() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_double_37)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_double_37() const { return ___c_double_37; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_double_37() { return &___c_double_37; }
	inline void set_c_double_37(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_double_37 = value;
		Il2CppCodeGenWriteBarrier((&___c_double_37), value);
	}

	inline static int32_t get_offset_of_c_doubleXdr_38() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_doubleXdr_38)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_doubleXdr_38() const { return ___c_doubleXdr_38; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_doubleXdr_38() { return &___c_doubleXdr_38; }
	inline void set_c_doubleXdr_38(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_doubleXdr_38 = value;
		Il2CppCodeGenWriteBarrier((&___c_doubleXdr_38), value);
	}

	inline static int32_t get_offset_of_c_duration_39() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_duration_39)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_duration_39() const { return ___c_duration_39; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_duration_39() { return &___c_duration_39; }
	inline void set_c_duration_39(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_duration_39 = value;
		Il2CppCodeGenWriteBarrier((&___c_duration_39), value);
	}

	inline static int32_t get_offset_of_c_ENTITY_40() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ENTITY_40)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ENTITY_40() const { return ___c_ENTITY_40; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ENTITY_40() { return &___c_ENTITY_40; }
	inline void set_c_ENTITY_40(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ENTITY_40 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENTITY_40), value);
	}

	inline static int32_t get_offset_of_c_ENTITIES_41() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ENTITIES_41)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ENTITIES_41() const { return ___c_ENTITIES_41; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ENTITIES_41() { return &___c_ENTITIES_41; }
	inline void set_c_ENTITIES_41(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ENTITIES_41 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENTITIES_41), value);
	}

	inline static int32_t get_offset_of_c_ENUMERATION_42() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ENUMERATION_42)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ENUMERATION_42() const { return ___c_ENUMERATION_42; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ENUMERATION_42() { return &___c_ENUMERATION_42; }
	inline void set_c_ENUMERATION_42(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ENUMERATION_42 = value;
		Il2CppCodeGenWriteBarrier((&___c_ENUMERATION_42), value);
	}

	inline static int32_t get_offset_of_c_fixed_43() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_fixed_43)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_fixed_43() const { return ___c_fixed_43; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_fixed_43() { return &___c_fixed_43; }
	inline void set_c_fixed_43(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_fixed_43 = value;
		Il2CppCodeGenWriteBarrier((&___c_fixed_43), value);
	}

	inline static int32_t get_offset_of_c_float_44() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_float_44)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_float_44() const { return ___c_float_44; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_float_44() { return &___c_float_44; }
	inline void set_c_float_44(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_float_44 = value;
		Il2CppCodeGenWriteBarrier((&___c_float_44), value);
	}

	inline static int32_t get_offset_of_c_floatXdr_45() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_floatXdr_45)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_floatXdr_45() const { return ___c_floatXdr_45; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_floatXdr_45() { return &___c_floatXdr_45; }
	inline void set_c_floatXdr_45(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_floatXdr_45 = value;
		Il2CppCodeGenWriteBarrier((&___c_floatXdr_45), value);
	}

	inline static int32_t get_offset_of_c_hexBinary_46() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_hexBinary_46)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_hexBinary_46() const { return ___c_hexBinary_46; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_hexBinary_46() { return &___c_hexBinary_46; }
	inline void set_c_hexBinary_46(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_hexBinary_46 = value;
		Il2CppCodeGenWriteBarrier((&___c_hexBinary_46), value);
	}

	inline static int32_t get_offset_of_c_ID_47() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_ID_47)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_ID_47() const { return ___c_ID_47; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_ID_47() { return &___c_ID_47; }
	inline void set_c_ID_47(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_ID_47 = value;
		Il2CppCodeGenWriteBarrier((&___c_ID_47), value);
	}

	inline static int32_t get_offset_of_c_IDREF_48() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_IDREF_48)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_IDREF_48() const { return ___c_IDREF_48; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_IDREF_48() { return &___c_IDREF_48; }
	inline void set_c_IDREF_48(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_IDREF_48 = value;
		Il2CppCodeGenWriteBarrier((&___c_IDREF_48), value);
	}

	inline static int32_t get_offset_of_c_IDREFS_49() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_IDREFS_49)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_IDREFS_49() const { return ___c_IDREFS_49; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_IDREFS_49() { return &___c_IDREFS_49; }
	inline void set_c_IDREFS_49(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_IDREFS_49 = value;
		Il2CppCodeGenWriteBarrier((&___c_IDREFS_49), value);
	}

	inline static int32_t get_offset_of_c_int_50() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_int_50)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_int_50() const { return ___c_int_50; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_int_50() { return &___c_int_50; }
	inline void set_c_int_50(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_int_50 = value;
		Il2CppCodeGenWriteBarrier((&___c_int_50), value);
	}

	inline static int32_t get_offset_of_c_integer_51() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_integer_51)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_integer_51() const { return ___c_integer_51; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_integer_51() { return &___c_integer_51; }
	inline void set_c_integer_51(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_integer_51 = value;
		Il2CppCodeGenWriteBarrier((&___c_integer_51), value);
	}

	inline static int32_t get_offset_of_c_language_52() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_language_52)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_language_52() const { return ___c_language_52; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_language_52() { return &___c_language_52; }
	inline void set_c_language_52(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_language_52 = value;
		Il2CppCodeGenWriteBarrier((&___c_language_52), value);
	}

	inline static int32_t get_offset_of_c_long_53() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_long_53)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_long_53() const { return ___c_long_53; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_long_53() { return &___c_long_53; }
	inline void set_c_long_53(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_long_53 = value;
		Il2CppCodeGenWriteBarrier((&___c_long_53), value);
	}

	inline static int32_t get_offset_of_c_month_54() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_month_54)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_month_54() const { return ___c_month_54; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_month_54() { return &___c_month_54; }
	inline void set_c_month_54(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_month_54 = value;
		Il2CppCodeGenWriteBarrier((&___c_month_54), value);
	}

	inline static int32_t get_offset_of_c_monthDay_55() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_monthDay_55)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_monthDay_55() const { return ___c_monthDay_55; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_monthDay_55() { return &___c_monthDay_55; }
	inline void set_c_monthDay_55(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_monthDay_55 = value;
		Il2CppCodeGenWriteBarrier((&___c_monthDay_55), value);
	}

	inline static int32_t get_offset_of_c_Name_56() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_Name_56)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_Name_56() const { return ___c_Name_56; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_Name_56() { return &___c_Name_56; }
	inline void set_c_Name_56(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_Name_56 = value;
		Il2CppCodeGenWriteBarrier((&___c_Name_56), value);
	}

	inline static int32_t get_offset_of_c_NCName_57() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NCName_57)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NCName_57() const { return ___c_NCName_57; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NCName_57() { return &___c_NCName_57; }
	inline void set_c_NCName_57(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NCName_57 = value;
		Il2CppCodeGenWriteBarrier((&___c_NCName_57), value);
	}

	inline static int32_t get_offset_of_c_negativeInteger_58() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_negativeInteger_58)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_negativeInteger_58() const { return ___c_negativeInteger_58; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_negativeInteger_58() { return &___c_negativeInteger_58; }
	inline void set_c_negativeInteger_58(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_negativeInteger_58 = value;
		Il2CppCodeGenWriteBarrier((&___c_negativeInteger_58), value);
	}

	inline static int32_t get_offset_of_c_NMTOKEN_59() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NMTOKEN_59)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NMTOKEN_59() const { return ___c_NMTOKEN_59; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NMTOKEN_59() { return &___c_NMTOKEN_59; }
	inline void set_c_NMTOKEN_59(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NMTOKEN_59 = value;
		Il2CppCodeGenWriteBarrier((&___c_NMTOKEN_59), value);
	}

	inline static int32_t get_offset_of_c_NMTOKENS_60() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NMTOKENS_60)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NMTOKENS_60() const { return ___c_NMTOKENS_60; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NMTOKENS_60() { return &___c_NMTOKENS_60; }
	inline void set_c_NMTOKENS_60(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NMTOKENS_60 = value;
		Il2CppCodeGenWriteBarrier((&___c_NMTOKENS_60), value);
	}

	inline static int32_t get_offset_of_c_nonNegativeInteger_61() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_nonNegativeInteger_61)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_nonNegativeInteger_61() const { return ___c_nonNegativeInteger_61; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_nonNegativeInteger_61() { return &___c_nonNegativeInteger_61; }
	inline void set_c_nonNegativeInteger_61(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_nonNegativeInteger_61 = value;
		Il2CppCodeGenWriteBarrier((&___c_nonNegativeInteger_61), value);
	}

	inline static int32_t get_offset_of_c_nonPositiveInteger_62() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_nonPositiveInteger_62)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_nonPositiveInteger_62() const { return ___c_nonPositiveInteger_62; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_nonPositiveInteger_62() { return &___c_nonPositiveInteger_62; }
	inline void set_c_nonPositiveInteger_62(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_nonPositiveInteger_62 = value;
		Il2CppCodeGenWriteBarrier((&___c_nonPositiveInteger_62), value);
	}

	inline static int32_t get_offset_of_c_normalizedString_63() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_normalizedString_63)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_normalizedString_63() const { return ___c_normalizedString_63; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_normalizedString_63() { return &___c_normalizedString_63; }
	inline void set_c_normalizedString_63(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_normalizedString_63 = value;
		Il2CppCodeGenWriteBarrier((&___c_normalizedString_63), value);
	}

	inline static int32_t get_offset_of_c_NOTATION_64() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_NOTATION_64)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_NOTATION_64() const { return ___c_NOTATION_64; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_NOTATION_64() { return &___c_NOTATION_64; }
	inline void set_c_NOTATION_64(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_NOTATION_64 = value;
		Il2CppCodeGenWriteBarrier((&___c_NOTATION_64), value);
	}

	inline static int32_t get_offset_of_c_positiveInteger_65() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_positiveInteger_65)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_positiveInteger_65() const { return ___c_positiveInteger_65; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_positiveInteger_65() { return &___c_positiveInteger_65; }
	inline void set_c_positiveInteger_65(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_positiveInteger_65 = value;
		Il2CppCodeGenWriteBarrier((&___c_positiveInteger_65), value);
	}

	inline static int32_t get_offset_of_c_QName_66() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_QName_66)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_QName_66() const { return ___c_QName_66; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_QName_66() { return &___c_QName_66; }
	inline void set_c_QName_66(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_QName_66 = value;
		Il2CppCodeGenWriteBarrier((&___c_QName_66), value);
	}

	inline static int32_t get_offset_of_c_QNameXdr_67() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_QNameXdr_67)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_QNameXdr_67() const { return ___c_QNameXdr_67; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_QNameXdr_67() { return &___c_QNameXdr_67; }
	inline void set_c_QNameXdr_67(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_QNameXdr_67 = value;
		Il2CppCodeGenWriteBarrier((&___c_QNameXdr_67), value);
	}

	inline static int32_t get_offset_of_c_short_68() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_short_68)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_short_68() const { return ___c_short_68; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_short_68() { return &___c_short_68; }
	inline void set_c_short_68(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_short_68 = value;
		Il2CppCodeGenWriteBarrier((&___c_short_68), value);
	}

	inline static int32_t get_offset_of_c_string_69() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_string_69)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_string_69() const { return ___c_string_69; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_string_69() { return &___c_string_69; }
	inline void set_c_string_69(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_string_69 = value;
		Il2CppCodeGenWriteBarrier((&___c_string_69), value);
	}

	inline static int32_t get_offset_of_c_time_70() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_time_70)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_time_70() const { return ___c_time_70; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_time_70() { return &___c_time_70; }
	inline void set_c_time_70(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_time_70 = value;
		Il2CppCodeGenWriteBarrier((&___c_time_70), value);
	}

	inline static int32_t get_offset_of_c_timeNoTz_71() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_timeNoTz_71)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_timeNoTz_71() const { return ___c_timeNoTz_71; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_timeNoTz_71() { return &___c_timeNoTz_71; }
	inline void set_c_timeNoTz_71(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_timeNoTz_71 = value;
		Il2CppCodeGenWriteBarrier((&___c_timeNoTz_71), value);
	}

	inline static int32_t get_offset_of_c_timeTz_72() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_timeTz_72)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_timeTz_72() const { return ___c_timeTz_72; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_timeTz_72() { return &___c_timeTz_72; }
	inline void set_c_timeTz_72(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_timeTz_72 = value;
		Il2CppCodeGenWriteBarrier((&___c_timeTz_72), value);
	}

	inline static int32_t get_offset_of_c_token_73() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_token_73)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_token_73() const { return ___c_token_73; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_token_73() { return &___c_token_73; }
	inline void set_c_token_73(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_token_73 = value;
		Il2CppCodeGenWriteBarrier((&___c_token_73), value);
	}

	inline static int32_t get_offset_of_c_unsignedByte_74() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedByte_74)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedByte_74() const { return ___c_unsignedByte_74; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedByte_74() { return &___c_unsignedByte_74; }
	inline void set_c_unsignedByte_74(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedByte_74 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedByte_74), value);
	}

	inline static int32_t get_offset_of_c_unsignedInt_75() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedInt_75)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedInt_75() const { return ___c_unsignedInt_75; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedInt_75() { return &___c_unsignedInt_75; }
	inline void set_c_unsignedInt_75(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedInt_75 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedInt_75), value);
	}

	inline static int32_t get_offset_of_c_unsignedLong_76() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedLong_76)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedLong_76() const { return ___c_unsignedLong_76; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedLong_76() { return &___c_unsignedLong_76; }
	inline void set_c_unsignedLong_76(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedLong_76 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedLong_76), value);
	}

	inline static int32_t get_offset_of_c_unsignedShort_77() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_unsignedShort_77)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_unsignedShort_77() const { return ___c_unsignedShort_77; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_unsignedShort_77() { return &___c_unsignedShort_77; }
	inline void set_c_unsignedShort_77(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_unsignedShort_77 = value;
		Il2CppCodeGenWriteBarrier((&___c_unsignedShort_77), value);
	}

	inline static int32_t get_offset_of_c_uuid_78() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_uuid_78)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_uuid_78() const { return ___c_uuid_78; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_uuid_78() { return &___c_uuid_78; }
	inline void set_c_uuid_78(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_uuid_78 = value;
		Il2CppCodeGenWriteBarrier((&___c_uuid_78), value);
	}

	inline static int32_t get_offset_of_c_year_79() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_year_79)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_year_79() const { return ___c_year_79; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_year_79() { return &___c_year_79; }
	inline void set_c_year_79(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_year_79 = value;
		Il2CppCodeGenWriteBarrier((&___c_year_79), value);
	}

	inline static int32_t get_offset_of_c_yearMonth_80() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_yearMonth_80)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_yearMonth_80() const { return ___c_yearMonth_80; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_yearMonth_80() { return &___c_yearMonth_80; }
	inline void set_c_yearMonth_80(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_yearMonth_80 = value;
		Il2CppCodeGenWriteBarrier((&___c_yearMonth_80), value);
	}

	inline static int32_t get_offset_of_c_normalizedStringV1Compat_81() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_normalizedStringV1Compat_81)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_normalizedStringV1Compat_81() const { return ___c_normalizedStringV1Compat_81; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_normalizedStringV1Compat_81() { return &___c_normalizedStringV1Compat_81; }
	inline void set_c_normalizedStringV1Compat_81(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_normalizedStringV1Compat_81 = value;
		Il2CppCodeGenWriteBarrier((&___c_normalizedStringV1Compat_81), value);
	}

	inline static int32_t get_offset_of_c_tokenV1Compat_82() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_tokenV1Compat_82)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_tokenV1Compat_82() const { return ___c_tokenV1Compat_82; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_tokenV1Compat_82() { return &___c_tokenV1Compat_82; }
	inline void set_c_tokenV1Compat_82(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_tokenV1Compat_82 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenV1Compat_82), value);
	}

	inline static int32_t get_offset_of_c_anyAtomicType_83() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_anyAtomicType_83)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_anyAtomicType_83() const { return ___c_anyAtomicType_83; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_anyAtomicType_83() { return &___c_anyAtomicType_83; }
	inline void set_c_anyAtomicType_83(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_anyAtomicType_83 = value;
		Il2CppCodeGenWriteBarrier((&___c_anyAtomicType_83), value);
	}

	inline static int32_t get_offset_of_c_dayTimeDuration_84() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_dayTimeDuration_84)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_dayTimeDuration_84() const { return ___c_dayTimeDuration_84; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_dayTimeDuration_84() { return &___c_dayTimeDuration_84; }
	inline void set_c_dayTimeDuration_84(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_dayTimeDuration_84 = value;
		Il2CppCodeGenWriteBarrier((&___c_dayTimeDuration_84), value);
	}

	inline static int32_t get_offset_of_c_untypedAtomicType_85() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_untypedAtomicType_85)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_untypedAtomicType_85() const { return ___c_untypedAtomicType_85; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_untypedAtomicType_85() { return &___c_untypedAtomicType_85; }
	inline void set_c_untypedAtomicType_85(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_untypedAtomicType_85 = value;
		Il2CppCodeGenWriteBarrier((&___c_untypedAtomicType_85), value);
	}

	inline static int32_t get_offset_of_c_yearMonthDuration_86() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_yearMonthDuration_86)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_c_yearMonthDuration_86() const { return ___c_yearMonthDuration_86; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_c_yearMonthDuration_86() { return &___c_yearMonthDuration_86; }
	inline void set_c_yearMonthDuration_86(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___c_yearMonthDuration_86 = value;
		Il2CppCodeGenWriteBarrier((&___c_yearMonthDuration_86), value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypes_87() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_tokenizedTypes_87)); }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* get_c_tokenizedTypes_87() const { return ___c_tokenizedTypes_87; }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36** get_address_of_c_tokenizedTypes_87() { return &___c_tokenizedTypes_87; }
	inline void set_c_tokenizedTypes_87(DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* value)
	{
		___c_tokenizedTypes_87 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenizedTypes_87), value);
	}

	inline static int32_t get_offset_of_c_tokenizedTypesXsd_88() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_tokenizedTypesXsd_88)); }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* get_c_tokenizedTypesXsd_88() const { return ___c_tokenizedTypesXsd_88; }
	inline DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36** get_address_of_c_tokenizedTypesXsd_88() { return &___c_tokenizedTypesXsd_88; }
	inline void set_c_tokenizedTypesXsd_88(DatatypeImplementationU5BU5D_t0F5F2FAE1D0EECFD06FFE014BFC2A105C9C30C36* value)
	{
		___c_tokenizedTypesXsd_88 = value;
		Il2CppCodeGenWriteBarrier((&___c_tokenizedTypesXsd_88), value);
	}

	inline static int32_t get_offset_of_c_XdrTypes_89() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_XdrTypes_89)); }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* get_c_XdrTypes_89() const { return ___c_XdrTypes_89; }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7** get_address_of_c_XdrTypes_89() { return &___c_XdrTypes_89; }
	inline void set_c_XdrTypes_89(SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* value)
	{
		___c_XdrTypes_89 = value;
		Il2CppCodeGenWriteBarrier((&___c_XdrTypes_89), value);
	}

	inline static int32_t get_offset_of_c_XsdTypes_90() { return static_cast<int32_t>(offsetof(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836_StaticFields, ___c_XsdTypes_90)); }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* get_c_XsdTypes_90() const { return ___c_XsdTypes_90; }
	inline SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7** get_address_of_c_XsdTypes_90() { return &___c_XsdTypes_90; }
	inline void set_c_XsdTypes_90(SchemaDatatypeMapU5BU5D_tE0D01BD73889B644F2583397DC3D243C66A3EEA7* value)
	{
		___c_XsdTypes_90 = value;
		Il2CppCodeGenWriteBarrier((&___c_XsdTypes_90), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPEIMPLEMENTATION_TB5894E69998AF6871FDFC14B2A95AE5F07CBB836_H
#ifndef FACETSCOMPILER_T91E639F7F083D79393593CD660539AC912314233_H
#define FACETSCOMPILER_T91E639F7F083D79393593CD660539AC912314233_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.FacetsChecker/FacetsCompiler
struct  FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233 
{
public:
	// System.Xml.Schema.DatatypeImplementation System.Xml.Schema.FacetsChecker/FacetsCompiler::datatype
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___datatype_0;
	// System.Xml.Schema.RestrictionFacets System.Xml.Schema.FacetsChecker/FacetsCompiler::derivedRestriction
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * ___derivedRestriction_1;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::baseFlags
	int32_t ___baseFlags_2;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::baseFixedFlags
	int32_t ___baseFixedFlags_3;
	// System.Xml.Schema.RestrictionFlags System.Xml.Schema.FacetsChecker/FacetsCompiler::validRestrictionFlags
	int32_t ___validRestrictionFlags_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.FacetsChecker/FacetsCompiler::nonNegativeInt
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___nonNegativeInt_5;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.FacetsChecker/FacetsCompiler::builtInType
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___builtInType_6;
	// System.Xml.Schema.XmlTypeCode System.Xml.Schema.FacetsChecker/FacetsCompiler::builtInEnum
	int32_t ___builtInEnum_7;
	// System.Boolean System.Xml.Schema.FacetsChecker/FacetsCompiler::firstPattern
	bool ___firstPattern_8;
	// System.Text.StringBuilder System.Xml.Schema.FacetsChecker/FacetsCompiler::regStr
	StringBuilder_t * ___regStr_9;
	// System.Xml.Schema.XmlSchemaPatternFacet System.Xml.Schema.FacetsChecker/FacetsCompiler::pattern_facet
	XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * ___pattern_facet_10;

public:
	inline static int32_t get_offset_of_datatype_0() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___datatype_0)); }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * get_datatype_0() const { return ___datatype_0; }
	inline DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 ** get_address_of_datatype_0() { return &___datatype_0; }
	inline void set_datatype_0(DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * value)
	{
		___datatype_0 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_0), value);
	}

	inline static int32_t get_offset_of_derivedRestriction_1() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___derivedRestriction_1)); }
	inline RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * get_derivedRestriction_1() const { return ___derivedRestriction_1; }
	inline RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 ** get_address_of_derivedRestriction_1() { return &___derivedRestriction_1; }
	inline void set_derivedRestriction_1(RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * value)
	{
		___derivedRestriction_1 = value;
		Il2CppCodeGenWriteBarrier((&___derivedRestriction_1), value);
	}

	inline static int32_t get_offset_of_baseFlags_2() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___baseFlags_2)); }
	inline int32_t get_baseFlags_2() const { return ___baseFlags_2; }
	inline int32_t* get_address_of_baseFlags_2() { return &___baseFlags_2; }
	inline void set_baseFlags_2(int32_t value)
	{
		___baseFlags_2 = value;
	}

	inline static int32_t get_offset_of_baseFixedFlags_3() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___baseFixedFlags_3)); }
	inline int32_t get_baseFixedFlags_3() const { return ___baseFixedFlags_3; }
	inline int32_t* get_address_of_baseFixedFlags_3() { return &___baseFixedFlags_3; }
	inline void set_baseFixedFlags_3(int32_t value)
	{
		___baseFixedFlags_3 = value;
	}

	inline static int32_t get_offset_of_validRestrictionFlags_4() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___validRestrictionFlags_4)); }
	inline int32_t get_validRestrictionFlags_4() const { return ___validRestrictionFlags_4; }
	inline int32_t* get_address_of_validRestrictionFlags_4() { return &___validRestrictionFlags_4; }
	inline void set_validRestrictionFlags_4(int32_t value)
	{
		___validRestrictionFlags_4 = value;
	}

	inline static int32_t get_offset_of_nonNegativeInt_5() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___nonNegativeInt_5)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_nonNegativeInt_5() const { return ___nonNegativeInt_5; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_nonNegativeInt_5() { return &___nonNegativeInt_5; }
	inline void set_nonNegativeInt_5(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___nonNegativeInt_5 = value;
		Il2CppCodeGenWriteBarrier((&___nonNegativeInt_5), value);
	}

	inline static int32_t get_offset_of_builtInType_6() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___builtInType_6)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_builtInType_6() const { return ___builtInType_6; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_builtInType_6() { return &___builtInType_6; }
	inline void set_builtInType_6(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___builtInType_6 = value;
		Il2CppCodeGenWriteBarrier((&___builtInType_6), value);
	}

	inline static int32_t get_offset_of_builtInEnum_7() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___builtInEnum_7)); }
	inline int32_t get_builtInEnum_7() const { return ___builtInEnum_7; }
	inline int32_t* get_address_of_builtInEnum_7() { return &___builtInEnum_7; }
	inline void set_builtInEnum_7(int32_t value)
	{
		___builtInEnum_7 = value;
	}

	inline static int32_t get_offset_of_firstPattern_8() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___firstPattern_8)); }
	inline bool get_firstPattern_8() const { return ___firstPattern_8; }
	inline bool* get_address_of_firstPattern_8() { return &___firstPattern_8; }
	inline void set_firstPattern_8(bool value)
	{
		___firstPattern_8 = value;
	}

	inline static int32_t get_offset_of_regStr_9() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___regStr_9)); }
	inline StringBuilder_t * get_regStr_9() const { return ___regStr_9; }
	inline StringBuilder_t ** get_address_of_regStr_9() { return &___regStr_9; }
	inline void set_regStr_9(StringBuilder_t * value)
	{
		___regStr_9 = value;
		Il2CppCodeGenWriteBarrier((&___regStr_9), value);
	}

	inline static int32_t get_offset_of_pattern_facet_10() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233, ___pattern_facet_10)); }
	inline XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * get_pattern_facet_10() const { return ___pattern_facet_10; }
	inline XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 ** get_address_of_pattern_facet_10() { return &___pattern_facet_10; }
	inline void set_pattern_facet_10(XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * value)
	{
		___pattern_facet_10 = value;
		Il2CppCodeGenWriteBarrier((&___pattern_facet_10), value);
	}
};

struct FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker/FacetsCompiler/Map[] System.Xml.Schema.FacetsChecker/FacetsCompiler::c_map
	MapU5BU5D_tAEE3CA984471631DDF5DF6E9567FDC4153FF5A27* ___c_map_11;

public:
	inline static int32_t get_offset_of_c_map_11() { return static_cast<int32_t>(offsetof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233_StaticFields, ___c_map_11)); }
	inline MapU5BU5D_tAEE3CA984471631DDF5DF6E9567FDC4153FF5A27* get_c_map_11() const { return ___c_map_11; }
	inline MapU5BU5D_tAEE3CA984471631DDF5DF6E9567FDC4153FF5A27** get_address_of_c_map_11() { return &___c_map_11; }
	inline void set_c_map_11(MapU5BU5D_tAEE3CA984471631DDF5DF6E9567FDC4153FF5A27* value)
	{
		___c_map_11 = value;
		Il2CppCodeGenWriteBarrier((&___c_map_11), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler
struct FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233_marshaled_pinvoke
{
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___datatype_0;
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * ___derivedRestriction_1;
	int32_t ___baseFlags_2;
	int32_t ___baseFixedFlags_3;
	int32_t ___validRestrictionFlags_4;
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___nonNegativeInt_5;
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___builtInType_6;
	int32_t ___builtInEnum_7;
	int32_t ___firstPattern_8;
	char* ___regStr_9;
	XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * ___pattern_facet_10;
};
// Native definition for COM marshalling of System.Xml.Schema.FacetsChecker/FacetsCompiler
struct FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233_marshaled_com
{
	DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836 * ___datatype_0;
	RestrictionFacets_tB440DD7CAE146E138972858A02C18D3D13282AB4 * ___derivedRestriction_1;
	int32_t ___baseFlags_2;
	int32_t ___baseFixedFlags_3;
	int32_t ___validRestrictionFlags_4;
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___nonNegativeInt_5;
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___builtInType_6;
	int32_t ___builtInEnum_7;
	int32_t ___firstPattern_8;
	Il2CppChar* ___regStr_9;
	XmlSchemaPatternFacet_t0FE0B38A1CCE96CD76792E0662B892BA630FE322 * ___pattern_facet_10;
};
#endif // FACETSCOMPILER_T91E639F7F083D79393593CD660539AC912314233_H
#ifndef NAMESPACELIST_T333B54F374AA7F3348AE9A2FD0210A465BBD6B8C_H
#define NAMESPACELIST_T333B54F374AA7F3348AE9A2FD0210A465BBD6B8C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceList
struct  NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C  : public RuntimeObject
{
public:
	// System.Xml.Schema.NamespaceList/ListType System.Xml.Schema.NamespaceList::type
	int32_t ___type_0;
	// System.Collections.Hashtable System.Xml.Schema.NamespaceList::set
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___set_1;
	// System.String System.Xml.Schema.NamespaceList::targetNamespace
	String_t* ___targetNamespace_2;

public:
	inline static int32_t get_offset_of_type_0() { return static_cast<int32_t>(offsetof(NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C, ___type_0)); }
	inline int32_t get_type_0() const { return ___type_0; }
	inline int32_t* get_address_of_type_0() { return &___type_0; }
	inline void set_type_0(int32_t value)
	{
		___type_0 = value;
	}

	inline static int32_t get_offset_of_set_1() { return static_cast<int32_t>(offsetof(NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C, ___set_1)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_set_1() const { return ___set_1; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_set_1() { return &___set_1; }
	inline void set_set_1(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___set_1 = value;
		Il2CppCodeGenWriteBarrier((&___set_1), value);
	}

	inline static int32_t get_offset_of_targetNamespace_2() { return static_cast<int32_t>(offsetof(NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C, ___targetNamespace_2)); }
	inline String_t* get_targetNamespace_2() const { return ___targetNamespace_2; }
	inline String_t** get_address_of_targetNamespace_2() { return &___targetNamespace_2; }
	inline void set_targetNamespace_2(String_t* value)
	{
		___targetNamespace_2 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACELIST_T333B54F374AA7F3348AE9A2FD0210A465BBD6B8C_H
#ifndef PARSER_TAF8FC602A0D1411B91B8B88BABCB6E59F20EB920_H
#define PARSER_TAF8FC602A0D1411B91B8B88BABCB6E59F20EB920_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Parser
struct  Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920  : public RuntimeObject
{
public:
	// System.Xml.Schema.SchemaType System.Xml.Schema.Parser::schemaType
	int32_t ___schemaType_0;
	// System.Xml.XmlNameTable System.Xml.Schema.Parser::nameTable
	XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * ___nameTable_1;
	// System.Xml.Schema.SchemaNames System.Xml.Schema.Parser::schemaNames
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * ___schemaNames_2;
	// System.Xml.Schema.ValidationEventHandler System.Xml.Schema.Parser::eventHandler
	ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * ___eventHandler_3;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Parser::namespaceManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___namespaceManager_4;
	// System.Xml.XmlReader System.Xml.Schema.Parser::reader
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___reader_5;
	// System.Xml.PositionInfo System.Xml.Schema.Parser::positionInfo
	PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * ___positionInfo_6;
	// System.Boolean System.Xml.Schema.Parser::isProcessNamespaces
	bool ___isProcessNamespaces_7;
	// System.Int32 System.Xml.Schema.Parser::schemaXmlDepth
	int32_t ___schemaXmlDepth_8;
	// System.Int32 System.Xml.Schema.Parser::markupDepth
	int32_t ___markupDepth_9;
	// System.Xml.Schema.SchemaBuilder System.Xml.Schema.Parser::builder
	SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7 * ___builder_10;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Parser::schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schema_11;
	// System.Xml.Schema.SchemaInfo System.Xml.Schema.Parser::xdrSchema
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * ___xdrSchema_12;
	// System.Xml.XmlResolver System.Xml.Schema.Parser::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_13;
	// System.Xml.XmlDocument System.Xml.Schema.Parser::dummyDocument
	XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * ___dummyDocument_14;
	// System.Boolean System.Xml.Schema.Parser::processMarkup
	bool ___processMarkup_15;
	// System.Xml.XmlNode System.Xml.Schema.Parser::parentNode
	XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * ___parentNode_16;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.Parser::annotationNSManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___annotationNSManager_17;
	// System.String System.Xml.Schema.Parser::xmlns
	String_t* ___xmlns_18;
	// System.Xml.XmlCharType System.Xml.Schema.Parser::xmlCharType
	XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  ___xmlCharType_19;

public:
	inline static int32_t get_offset_of_schemaType_0() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___schemaType_0)); }
	inline int32_t get_schemaType_0() const { return ___schemaType_0; }
	inline int32_t* get_address_of_schemaType_0() { return &___schemaType_0; }
	inline void set_schemaType_0(int32_t value)
	{
		___schemaType_0 = value;
	}

	inline static int32_t get_offset_of_nameTable_1() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___nameTable_1)); }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * get_nameTable_1() const { return ___nameTable_1; }
	inline XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 ** get_address_of_nameTable_1() { return &___nameTable_1; }
	inline void set_nameTable_1(XmlNameTable_t3C1CDAB4E7D97DE41A409D6D9ADD2C10B1F281A6 * value)
	{
		___nameTable_1 = value;
		Il2CppCodeGenWriteBarrier((&___nameTable_1), value);
	}

	inline static int32_t get_offset_of_schemaNames_2() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___schemaNames_2)); }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * get_schemaNames_2() const { return ___schemaNames_2; }
	inline SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 ** get_address_of_schemaNames_2() { return &___schemaNames_2; }
	inline void set_schemaNames_2(SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89 * value)
	{
		___schemaNames_2 = value;
		Il2CppCodeGenWriteBarrier((&___schemaNames_2), value);
	}

	inline static int32_t get_offset_of_eventHandler_3() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___eventHandler_3)); }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * get_eventHandler_3() const { return ___eventHandler_3; }
	inline ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF ** get_address_of_eventHandler_3() { return &___eventHandler_3; }
	inline void set_eventHandler_3(ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF * value)
	{
		___eventHandler_3 = value;
		Il2CppCodeGenWriteBarrier((&___eventHandler_3), value);
	}

	inline static int32_t get_offset_of_namespaceManager_4() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___namespaceManager_4)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_namespaceManager_4() const { return ___namespaceManager_4; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_namespaceManager_4() { return &___namespaceManager_4; }
	inline void set_namespaceManager_4(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___namespaceManager_4 = value;
		Il2CppCodeGenWriteBarrier((&___namespaceManager_4), value);
	}

	inline static int32_t get_offset_of_reader_5() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___reader_5)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_reader_5() const { return ___reader_5; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_reader_5() { return &___reader_5; }
	inline void set_reader_5(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___reader_5 = value;
		Il2CppCodeGenWriteBarrier((&___reader_5), value);
	}

	inline static int32_t get_offset_of_positionInfo_6() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___positionInfo_6)); }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * get_positionInfo_6() const { return ___positionInfo_6; }
	inline PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 ** get_address_of_positionInfo_6() { return &___positionInfo_6; }
	inline void set_positionInfo_6(PositionInfo_tC35A3FA759E0F23D150BB8B774EC0391EA099684 * value)
	{
		___positionInfo_6 = value;
		Il2CppCodeGenWriteBarrier((&___positionInfo_6), value);
	}

	inline static int32_t get_offset_of_isProcessNamespaces_7() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___isProcessNamespaces_7)); }
	inline bool get_isProcessNamespaces_7() const { return ___isProcessNamespaces_7; }
	inline bool* get_address_of_isProcessNamespaces_7() { return &___isProcessNamespaces_7; }
	inline void set_isProcessNamespaces_7(bool value)
	{
		___isProcessNamespaces_7 = value;
	}

	inline static int32_t get_offset_of_schemaXmlDepth_8() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___schemaXmlDepth_8)); }
	inline int32_t get_schemaXmlDepth_8() const { return ___schemaXmlDepth_8; }
	inline int32_t* get_address_of_schemaXmlDepth_8() { return &___schemaXmlDepth_8; }
	inline void set_schemaXmlDepth_8(int32_t value)
	{
		___schemaXmlDepth_8 = value;
	}

	inline static int32_t get_offset_of_markupDepth_9() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___markupDepth_9)); }
	inline int32_t get_markupDepth_9() const { return ___markupDepth_9; }
	inline int32_t* get_address_of_markupDepth_9() { return &___markupDepth_9; }
	inline void set_markupDepth_9(int32_t value)
	{
		___markupDepth_9 = value;
	}

	inline static int32_t get_offset_of_builder_10() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___builder_10)); }
	inline SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7 * get_builder_10() const { return ___builder_10; }
	inline SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7 ** get_address_of_builder_10() { return &___builder_10; }
	inline void set_builder_10(SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7 * value)
	{
		___builder_10 = value;
		Il2CppCodeGenWriteBarrier((&___builder_10), value);
	}

	inline static int32_t get_offset_of_schema_11() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___schema_11)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schema_11() const { return ___schema_11; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schema_11() { return &___schema_11; }
	inline void set_schema_11(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schema_11 = value;
		Il2CppCodeGenWriteBarrier((&___schema_11), value);
	}

	inline static int32_t get_offset_of_xdrSchema_12() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___xdrSchema_12)); }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * get_xdrSchema_12() const { return ___xdrSchema_12; }
	inline SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 ** get_address_of_xdrSchema_12() { return &___xdrSchema_12; }
	inline void set_xdrSchema_12(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41 * value)
	{
		___xdrSchema_12 = value;
		Il2CppCodeGenWriteBarrier((&___xdrSchema_12), value);
	}

	inline static int32_t get_offset_of_xmlResolver_13() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___xmlResolver_13)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_13() const { return ___xmlResolver_13; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_13() { return &___xmlResolver_13; }
	inline void set_xmlResolver_13(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_13 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_13), value);
	}

	inline static int32_t get_offset_of_dummyDocument_14() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___dummyDocument_14)); }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * get_dummyDocument_14() const { return ___dummyDocument_14; }
	inline XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 ** get_address_of_dummyDocument_14() { return &___dummyDocument_14; }
	inline void set_dummyDocument_14(XmlDocument_t448325D04430147AF19F2955BD6A5F1551003C97 * value)
	{
		___dummyDocument_14 = value;
		Il2CppCodeGenWriteBarrier((&___dummyDocument_14), value);
	}

	inline static int32_t get_offset_of_processMarkup_15() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___processMarkup_15)); }
	inline bool get_processMarkup_15() const { return ___processMarkup_15; }
	inline bool* get_address_of_processMarkup_15() { return &___processMarkup_15; }
	inline void set_processMarkup_15(bool value)
	{
		___processMarkup_15 = value;
	}

	inline static int32_t get_offset_of_parentNode_16() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___parentNode_16)); }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * get_parentNode_16() const { return ___parentNode_16; }
	inline XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB ** get_address_of_parentNode_16() { return &___parentNode_16; }
	inline void set_parentNode_16(XmlNode_t07D70045D843753E4FE8AFE40FD36244E6B6D7FB * value)
	{
		___parentNode_16 = value;
		Il2CppCodeGenWriteBarrier((&___parentNode_16), value);
	}

	inline static int32_t get_offset_of_annotationNSManager_17() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___annotationNSManager_17)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_annotationNSManager_17() const { return ___annotationNSManager_17; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_annotationNSManager_17() { return &___annotationNSManager_17; }
	inline void set_annotationNSManager_17(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___annotationNSManager_17 = value;
		Il2CppCodeGenWriteBarrier((&___annotationNSManager_17), value);
	}

	inline static int32_t get_offset_of_xmlns_18() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___xmlns_18)); }
	inline String_t* get_xmlns_18() const { return ___xmlns_18; }
	inline String_t** get_address_of_xmlns_18() { return &___xmlns_18; }
	inline void set_xmlns_18(String_t* value)
	{
		___xmlns_18 = value;
		Il2CppCodeGenWriteBarrier((&___xmlns_18), value);
	}

	inline static int32_t get_offset_of_xmlCharType_19() { return static_cast<int32_t>(offsetof(Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920, ___xmlCharType_19)); }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  get_xmlCharType_19() const { return ___xmlCharType_19; }
	inline XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9 * get_address_of_xmlCharType_19() { return &___xmlCharType_19; }
	inline void set_xmlCharType_19(XmlCharType_t7F6CCEEB0A0BC8FC40F161B8928A766BE7B234E9  value)
	{
		___xmlCharType_19 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PARSER_TAF8FC602A0D1411B91B8B88BABCB6E59F20EB920_H
#ifndef PREPROCESSOR_TD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_H
#define PREPROCESSOR_TD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Preprocessor
struct  Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95  : public BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427
{
public:
	// System.String System.Xml.Schema.Preprocessor::Xmlns
	String_t* ___Xmlns_6;
	// System.String System.Xml.Schema.Preprocessor::NsXsi
	String_t* ___NsXsi_7;
	// System.String System.Xml.Schema.Preprocessor::targetNamespace
	String_t* ___targetNamespace_8;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Preprocessor::rootSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___rootSchema_9;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Preprocessor::currentSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___currentSchema_10;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.Preprocessor::elementFormDefault
	int32_t ___elementFormDefault_11;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.Preprocessor::attributeFormDefault
	int32_t ___attributeFormDefault_12;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.Preprocessor::blockDefault
	int32_t ___blockDefault_13;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.Preprocessor::finalDefault
	int32_t ___finalDefault_14;
	// System.Collections.Hashtable System.Xml.Schema.Preprocessor::schemaLocations
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___schemaLocations_15;
	// System.Collections.Hashtable System.Xml.Schema.Preprocessor::chameleonSchemas
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___chameleonSchemas_16;
	// System.Collections.Hashtable System.Xml.Schema.Preprocessor::referenceNamespaces
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___referenceNamespaces_17;
	// System.Collections.Hashtable System.Xml.Schema.Preprocessor::processedExternals
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___processedExternals_18;
	// System.Collections.SortedList System.Xml.Schema.Preprocessor::lockList
	SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * ___lockList_19;
	// System.Xml.XmlReaderSettings System.Xml.Schema.Preprocessor::readerSettings
	XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 * ___readerSettings_20;
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Preprocessor::rootSchemaForRedefine
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___rootSchemaForRedefine_21;
	// System.Collections.ArrayList System.Xml.Schema.Preprocessor::redefinedList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___redefinedList_22;
	// System.Xml.XmlResolver System.Xml.Schema.Preprocessor::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_24;

public:
	inline static int32_t get_offset_of_Xmlns_6() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___Xmlns_6)); }
	inline String_t* get_Xmlns_6() const { return ___Xmlns_6; }
	inline String_t** get_address_of_Xmlns_6() { return &___Xmlns_6; }
	inline void set_Xmlns_6(String_t* value)
	{
		___Xmlns_6 = value;
		Il2CppCodeGenWriteBarrier((&___Xmlns_6), value);
	}

	inline static int32_t get_offset_of_NsXsi_7() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___NsXsi_7)); }
	inline String_t* get_NsXsi_7() const { return ___NsXsi_7; }
	inline String_t** get_address_of_NsXsi_7() { return &___NsXsi_7; }
	inline void set_NsXsi_7(String_t* value)
	{
		___NsXsi_7 = value;
		Il2CppCodeGenWriteBarrier((&___NsXsi_7), value);
	}

	inline static int32_t get_offset_of_targetNamespace_8() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___targetNamespace_8)); }
	inline String_t* get_targetNamespace_8() const { return ___targetNamespace_8; }
	inline String_t** get_address_of_targetNamespace_8() { return &___targetNamespace_8; }
	inline void set_targetNamespace_8(String_t* value)
	{
		___targetNamespace_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_8), value);
	}

	inline static int32_t get_offset_of_rootSchema_9() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___rootSchema_9)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_rootSchema_9() const { return ___rootSchema_9; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_rootSchema_9() { return &___rootSchema_9; }
	inline void set_rootSchema_9(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___rootSchema_9 = value;
		Il2CppCodeGenWriteBarrier((&___rootSchema_9), value);
	}

	inline static int32_t get_offset_of_currentSchema_10() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___currentSchema_10)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_currentSchema_10() const { return ___currentSchema_10; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_currentSchema_10() { return &___currentSchema_10; }
	inline void set_currentSchema_10(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___currentSchema_10 = value;
		Il2CppCodeGenWriteBarrier((&___currentSchema_10), value);
	}

	inline static int32_t get_offset_of_elementFormDefault_11() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___elementFormDefault_11)); }
	inline int32_t get_elementFormDefault_11() const { return ___elementFormDefault_11; }
	inline int32_t* get_address_of_elementFormDefault_11() { return &___elementFormDefault_11; }
	inline void set_elementFormDefault_11(int32_t value)
	{
		___elementFormDefault_11 = value;
	}

	inline static int32_t get_offset_of_attributeFormDefault_12() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___attributeFormDefault_12)); }
	inline int32_t get_attributeFormDefault_12() const { return ___attributeFormDefault_12; }
	inline int32_t* get_address_of_attributeFormDefault_12() { return &___attributeFormDefault_12; }
	inline void set_attributeFormDefault_12(int32_t value)
	{
		___attributeFormDefault_12 = value;
	}

	inline static int32_t get_offset_of_blockDefault_13() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___blockDefault_13)); }
	inline int32_t get_blockDefault_13() const { return ___blockDefault_13; }
	inline int32_t* get_address_of_blockDefault_13() { return &___blockDefault_13; }
	inline void set_blockDefault_13(int32_t value)
	{
		___blockDefault_13 = value;
	}

	inline static int32_t get_offset_of_finalDefault_14() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___finalDefault_14)); }
	inline int32_t get_finalDefault_14() const { return ___finalDefault_14; }
	inline int32_t* get_address_of_finalDefault_14() { return &___finalDefault_14; }
	inline void set_finalDefault_14(int32_t value)
	{
		___finalDefault_14 = value;
	}

	inline static int32_t get_offset_of_schemaLocations_15() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___schemaLocations_15)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_schemaLocations_15() const { return ___schemaLocations_15; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_schemaLocations_15() { return &___schemaLocations_15; }
	inline void set_schemaLocations_15(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___schemaLocations_15 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocations_15), value);
	}

	inline static int32_t get_offset_of_chameleonSchemas_16() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___chameleonSchemas_16)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_chameleonSchemas_16() const { return ___chameleonSchemas_16; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_chameleonSchemas_16() { return &___chameleonSchemas_16; }
	inline void set_chameleonSchemas_16(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___chameleonSchemas_16 = value;
		Il2CppCodeGenWriteBarrier((&___chameleonSchemas_16), value);
	}

	inline static int32_t get_offset_of_referenceNamespaces_17() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___referenceNamespaces_17)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_referenceNamespaces_17() const { return ___referenceNamespaces_17; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_referenceNamespaces_17() { return &___referenceNamespaces_17; }
	inline void set_referenceNamespaces_17(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___referenceNamespaces_17 = value;
		Il2CppCodeGenWriteBarrier((&___referenceNamespaces_17), value);
	}

	inline static int32_t get_offset_of_processedExternals_18() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___processedExternals_18)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_processedExternals_18() const { return ___processedExternals_18; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_processedExternals_18() { return &___processedExternals_18; }
	inline void set_processedExternals_18(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___processedExternals_18 = value;
		Il2CppCodeGenWriteBarrier((&___processedExternals_18), value);
	}

	inline static int32_t get_offset_of_lockList_19() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___lockList_19)); }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * get_lockList_19() const { return ___lockList_19; }
	inline SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E ** get_address_of_lockList_19() { return &___lockList_19; }
	inline void set_lockList_19(SortedList_tC8B7CDE75652EC657C510034F127B9DFDE16BF4E * value)
	{
		___lockList_19 = value;
		Il2CppCodeGenWriteBarrier((&___lockList_19), value);
	}

	inline static int32_t get_offset_of_readerSettings_20() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___readerSettings_20)); }
	inline XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 * get_readerSettings_20() const { return ___readerSettings_20; }
	inline XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 ** get_address_of_readerSettings_20() { return &___readerSettings_20; }
	inline void set_readerSettings_20(XmlReaderSettings_t33E632C6BB215A5F2B4EAA81B289E027AF617F65 * value)
	{
		___readerSettings_20 = value;
		Il2CppCodeGenWriteBarrier((&___readerSettings_20), value);
	}

	inline static int32_t get_offset_of_rootSchemaForRedefine_21() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___rootSchemaForRedefine_21)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_rootSchemaForRedefine_21() const { return ___rootSchemaForRedefine_21; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_rootSchemaForRedefine_21() { return &___rootSchemaForRedefine_21; }
	inline void set_rootSchemaForRedefine_21(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___rootSchemaForRedefine_21 = value;
		Il2CppCodeGenWriteBarrier((&___rootSchemaForRedefine_21), value);
	}

	inline static int32_t get_offset_of_redefinedList_22() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___redefinedList_22)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_redefinedList_22() const { return ___redefinedList_22; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_redefinedList_22() { return &___redefinedList_22; }
	inline void set_redefinedList_22(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___redefinedList_22 = value;
		Il2CppCodeGenWriteBarrier((&___redefinedList_22), value);
	}

	inline static int32_t get_offset_of_xmlResolver_24() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95, ___xmlResolver_24)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_24() const { return ___xmlResolver_24; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_24() { return &___xmlResolver_24; }
	inline void set_xmlResolver_24(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_24 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_24), value);
	}
};

struct Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_StaticFields
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.Preprocessor::builtInSchemaForXmlNS
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___builtInSchemaForXmlNS_23;

public:
	inline static int32_t get_offset_of_builtInSchemaForXmlNS_23() { return static_cast<int32_t>(offsetof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_StaticFields, ___builtInSchemaForXmlNS_23)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_builtInSchemaForXmlNS_23() const { return ___builtInSchemaForXmlNS_23; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_builtInSchemaForXmlNS_23() { return &___builtInSchemaForXmlNS_23; }
	inline void set_builtInSchemaForXmlNS_23(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___builtInSchemaForXmlNS_23 = value;
		Il2CppCodeGenWriteBarrier((&___builtInSchemaForXmlNS_23), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREPROCESSOR_TD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_H
#ifndef SCHEMACOLLECTIONPREPROCESSOR_T75B7403D759DB36066607F965187B84D0EE25BA6_H
#define SCHEMACOLLECTIONPREPROCESSOR_T75B7403D759DB36066607F965187B84D0EE25BA6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaCollectionPreprocessor
struct  SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6  : public BaseProcessor_t68F1D18B3D8A79746118A87DF029D5FA8E90D427
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.SchemaCollectionPreprocessor::schema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___schema_6;
	// System.String System.Xml.Schema.SchemaCollectionPreprocessor::targetNamespace
	String_t* ___targetNamespace_7;
	// System.Boolean System.Xml.Schema.SchemaCollectionPreprocessor::buildinIncluded
	bool ___buildinIncluded_8;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.SchemaCollectionPreprocessor::elementFormDefault
	int32_t ___elementFormDefault_9;
	// System.Xml.Schema.XmlSchemaForm System.Xml.Schema.SchemaCollectionPreprocessor::attributeFormDefault
	int32_t ___attributeFormDefault_10;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaCollectionPreprocessor::blockDefault
	int32_t ___blockDefault_11;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaCollectionPreprocessor::finalDefault
	int32_t ___finalDefault_12;
	// System.Collections.Hashtable System.Xml.Schema.SchemaCollectionPreprocessor::schemaLocations
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___schemaLocations_13;
	// System.Collections.Hashtable System.Xml.Schema.SchemaCollectionPreprocessor::referenceNamespaces
	Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * ___referenceNamespaces_14;
	// System.String System.Xml.Schema.SchemaCollectionPreprocessor::Xmlns
	String_t* ___Xmlns_15;
	// System.Xml.XmlResolver System.Xml.Schema.SchemaCollectionPreprocessor::xmlResolver
	XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * ___xmlResolver_16;

public:
	inline static int32_t get_offset_of_schema_6() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___schema_6)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_schema_6() const { return ___schema_6; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_schema_6() { return &___schema_6; }
	inline void set_schema_6(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___schema_6 = value;
		Il2CppCodeGenWriteBarrier((&___schema_6), value);
	}

	inline static int32_t get_offset_of_targetNamespace_7() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___targetNamespace_7)); }
	inline String_t* get_targetNamespace_7() const { return ___targetNamespace_7; }
	inline String_t** get_address_of_targetNamespace_7() { return &___targetNamespace_7; }
	inline void set_targetNamespace_7(String_t* value)
	{
		___targetNamespace_7 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespace_7), value);
	}

	inline static int32_t get_offset_of_buildinIncluded_8() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___buildinIncluded_8)); }
	inline bool get_buildinIncluded_8() const { return ___buildinIncluded_8; }
	inline bool* get_address_of_buildinIncluded_8() { return &___buildinIncluded_8; }
	inline void set_buildinIncluded_8(bool value)
	{
		___buildinIncluded_8 = value;
	}

	inline static int32_t get_offset_of_elementFormDefault_9() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___elementFormDefault_9)); }
	inline int32_t get_elementFormDefault_9() const { return ___elementFormDefault_9; }
	inline int32_t* get_address_of_elementFormDefault_9() { return &___elementFormDefault_9; }
	inline void set_elementFormDefault_9(int32_t value)
	{
		___elementFormDefault_9 = value;
	}

	inline static int32_t get_offset_of_attributeFormDefault_10() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___attributeFormDefault_10)); }
	inline int32_t get_attributeFormDefault_10() const { return ___attributeFormDefault_10; }
	inline int32_t* get_address_of_attributeFormDefault_10() { return &___attributeFormDefault_10; }
	inline void set_attributeFormDefault_10(int32_t value)
	{
		___attributeFormDefault_10 = value;
	}

	inline static int32_t get_offset_of_blockDefault_11() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___blockDefault_11)); }
	inline int32_t get_blockDefault_11() const { return ___blockDefault_11; }
	inline int32_t* get_address_of_blockDefault_11() { return &___blockDefault_11; }
	inline void set_blockDefault_11(int32_t value)
	{
		___blockDefault_11 = value;
	}

	inline static int32_t get_offset_of_finalDefault_12() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___finalDefault_12)); }
	inline int32_t get_finalDefault_12() const { return ___finalDefault_12; }
	inline int32_t* get_address_of_finalDefault_12() { return &___finalDefault_12; }
	inline void set_finalDefault_12(int32_t value)
	{
		___finalDefault_12 = value;
	}

	inline static int32_t get_offset_of_schemaLocations_13() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___schemaLocations_13)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_schemaLocations_13() const { return ___schemaLocations_13; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_schemaLocations_13() { return &___schemaLocations_13; }
	inline void set_schemaLocations_13(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___schemaLocations_13 = value;
		Il2CppCodeGenWriteBarrier((&___schemaLocations_13), value);
	}

	inline static int32_t get_offset_of_referenceNamespaces_14() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___referenceNamespaces_14)); }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * get_referenceNamespaces_14() const { return ___referenceNamespaces_14; }
	inline Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 ** get_address_of_referenceNamespaces_14() { return &___referenceNamespaces_14; }
	inline void set_referenceNamespaces_14(Hashtable_t978F65B8006C8F5504B286526AEC6608FF983FC9 * value)
	{
		___referenceNamespaces_14 = value;
		Il2CppCodeGenWriteBarrier((&___referenceNamespaces_14), value);
	}

	inline static int32_t get_offset_of_Xmlns_15() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___Xmlns_15)); }
	inline String_t* get_Xmlns_15() const { return ___Xmlns_15; }
	inline String_t** get_address_of_Xmlns_15() { return &___Xmlns_15; }
	inline void set_Xmlns_15(String_t* value)
	{
		___Xmlns_15 = value;
		Il2CppCodeGenWriteBarrier((&___Xmlns_15), value);
	}

	inline static int32_t get_offset_of_xmlResolver_16() { return static_cast<int32_t>(offsetof(SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6, ___xmlResolver_16)); }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * get_xmlResolver_16() const { return ___xmlResolver_16; }
	inline XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 ** get_address_of_xmlResolver_16() { return &___xmlResolver_16; }
	inline void set_xmlResolver_16(XmlResolver_t93269F14B2F8750D040AED2FB7A303CE85016280 * value)
	{
		___xmlResolver_16 = value;
		Il2CppCodeGenWriteBarrier((&___xmlResolver_16), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMACOLLECTIONPREPROCESSOR_T75B7403D759DB36066607F965187B84D0EE25BA6_H
#ifndef SCHEMADECLBASE_T01E56CF5001308CDC9C9D6CC9E31648FC3F4772B_H
#define SCHEMADECLBASE_T01E56CF5001308CDC9C9D6CC9E31648FC3F4772B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaDeclBase
struct  SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B  : public RuntimeObject
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaDeclBase::name
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___name_0;
	// System.String System.Xml.Schema.SchemaDeclBase::prefix
	String_t* ___prefix_1;
	// System.Boolean System.Xml.Schema.SchemaDeclBase::isDeclaredInExternal
	bool ___isDeclaredInExternal_2;
	// System.Xml.Schema.SchemaDeclBase/Use System.Xml.Schema.SchemaDeclBase::presence
	int32_t ___presence_3;
	// System.Xml.Schema.XmlSchemaType System.Xml.Schema.SchemaDeclBase::schemaType
	XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * ___schemaType_4;
	// System.Xml.Schema.XmlSchemaDatatype System.Xml.Schema.SchemaDeclBase::datatype
	XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * ___datatype_5;
	// System.String System.Xml.Schema.SchemaDeclBase::defaultValueRaw
	String_t* ___defaultValueRaw_6;
	// System.Object System.Xml.Schema.SchemaDeclBase::defaultValueTyped
	RuntimeObject * ___defaultValueTyped_7;
	// System.Int64 System.Xml.Schema.SchemaDeclBase::maxLength
	int64_t ___maxLength_8;
	// System.Int64 System.Xml.Schema.SchemaDeclBase::minLength
	int64_t ___minLength_9;
	// System.Collections.Generic.List`1<System.String> System.Xml.Schema.SchemaDeclBase::values
	List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * ___values_10;

public:
	inline static int32_t get_offset_of_name_0() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___name_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_name_0() const { return ___name_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_name_0() { return &___name_0; }
	inline void set_name_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___name_0 = value;
		Il2CppCodeGenWriteBarrier((&___name_0), value);
	}

	inline static int32_t get_offset_of_prefix_1() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___prefix_1)); }
	inline String_t* get_prefix_1() const { return ___prefix_1; }
	inline String_t** get_address_of_prefix_1() { return &___prefix_1; }
	inline void set_prefix_1(String_t* value)
	{
		___prefix_1 = value;
		Il2CppCodeGenWriteBarrier((&___prefix_1), value);
	}

	inline static int32_t get_offset_of_isDeclaredInExternal_2() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___isDeclaredInExternal_2)); }
	inline bool get_isDeclaredInExternal_2() const { return ___isDeclaredInExternal_2; }
	inline bool* get_address_of_isDeclaredInExternal_2() { return &___isDeclaredInExternal_2; }
	inline void set_isDeclaredInExternal_2(bool value)
	{
		___isDeclaredInExternal_2 = value;
	}

	inline static int32_t get_offset_of_presence_3() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___presence_3)); }
	inline int32_t get_presence_3() const { return ___presence_3; }
	inline int32_t* get_address_of_presence_3() { return &___presence_3; }
	inline void set_presence_3(int32_t value)
	{
		___presence_3 = value;
	}

	inline static int32_t get_offset_of_schemaType_4() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___schemaType_4)); }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * get_schemaType_4() const { return ___schemaType_4; }
	inline XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 ** get_address_of_schemaType_4() { return &___schemaType_4; }
	inline void set_schemaType_4(XmlSchemaType_tE97B70BB8298B2D6FDDBCCB14EEE99EC4806BBA9 * value)
	{
		___schemaType_4 = value;
		Il2CppCodeGenWriteBarrier((&___schemaType_4), value);
	}

	inline static int32_t get_offset_of_datatype_5() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___datatype_5)); }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * get_datatype_5() const { return ___datatype_5; }
	inline XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 ** get_address_of_datatype_5() { return &___datatype_5; }
	inline void set_datatype_5(XmlSchemaDatatype_t6D9535C4B3780086DF21646303E2350D40A5A550 * value)
	{
		___datatype_5 = value;
		Il2CppCodeGenWriteBarrier((&___datatype_5), value);
	}

	inline static int32_t get_offset_of_defaultValueRaw_6() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___defaultValueRaw_6)); }
	inline String_t* get_defaultValueRaw_6() const { return ___defaultValueRaw_6; }
	inline String_t** get_address_of_defaultValueRaw_6() { return &___defaultValueRaw_6; }
	inline void set_defaultValueRaw_6(String_t* value)
	{
		___defaultValueRaw_6 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueRaw_6), value);
	}

	inline static int32_t get_offset_of_defaultValueTyped_7() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___defaultValueTyped_7)); }
	inline RuntimeObject * get_defaultValueTyped_7() const { return ___defaultValueTyped_7; }
	inline RuntimeObject ** get_address_of_defaultValueTyped_7() { return &___defaultValueTyped_7; }
	inline void set_defaultValueTyped_7(RuntimeObject * value)
	{
		___defaultValueTyped_7 = value;
		Il2CppCodeGenWriteBarrier((&___defaultValueTyped_7), value);
	}

	inline static int32_t get_offset_of_maxLength_8() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___maxLength_8)); }
	inline int64_t get_maxLength_8() const { return ___maxLength_8; }
	inline int64_t* get_address_of_maxLength_8() { return &___maxLength_8; }
	inline void set_maxLength_8(int64_t value)
	{
		___maxLength_8 = value;
	}

	inline static int32_t get_offset_of_minLength_9() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___minLength_9)); }
	inline int64_t get_minLength_9() const { return ___minLength_9; }
	inline int64_t* get_address_of_minLength_9() { return &___minLength_9; }
	inline void set_minLength_9(int64_t value)
	{
		___minLength_9 = value;
	}

	inline static int32_t get_offset_of_values_10() { return static_cast<int32_t>(offsetof(SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B, ___values_10)); }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * get_values_10() const { return ___values_10; }
	inline List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 ** get_address_of_values_10() { return &___values_10; }
	inline void set_values_10(List_1_tE8032E48C661C350FF9550E9063D595C0AB25CD3 * value)
	{
		___values_10 = value;
		Il2CppCodeGenWriteBarrier((&___values_10), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMADECLBASE_T01E56CF5001308CDC9C9D6CC9E31648FC3F4772B_H
#ifndef SCHEMAINFO_TD9774489795A78B9235BAD315E5242C974183A41_H
#define SCHEMAINFO_TD9774489795A78B9235BAD315E5242C974183A41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaInfo
struct  SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41  : public RuntimeObject
{
public:
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl> System.Xml.Schema.SchemaInfo::elementDecls
	Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * ___elementDecls_0;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl> System.Xml.Schema.SchemaInfo::undeclaredElementDecls
	Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * ___undeclaredElementDecls_1;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaEntity> System.Xml.Schema.SchemaInfo::generalEntities
	Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 * ___generalEntities_2;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaEntity> System.Xml.Schema.SchemaInfo::parameterEntities
	Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 * ___parameterEntities_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.SchemaInfo::docTypeName
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___docTypeName_4;
	// System.String System.Xml.Schema.SchemaInfo::internalDtdSubset
	String_t* ___internalDtdSubset_5;
	// System.Boolean System.Xml.Schema.SchemaInfo::hasNonCDataAttributes
	bool ___hasNonCDataAttributes_6;
	// System.Boolean System.Xml.Schema.SchemaInfo::hasDefaultAttributes
	bool ___hasDefaultAttributes_7;
	// System.Collections.Generic.Dictionary`2<System.String,System.Boolean> System.Xml.Schema.SchemaInfo::targetNamespaces
	Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B * ___targetNamespaces_8;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef> System.Xml.Schema.SchemaInfo::attributeDecls
	Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 * ___attributeDecls_9;
	// System.Int32 System.Xml.Schema.SchemaInfo::errorCount
	int32_t ___errorCount_10;
	// System.Xml.Schema.SchemaType System.Xml.Schema.SchemaInfo::schemaType
	int32_t ___schemaType_11;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaElementDecl> System.Xml.Schema.SchemaInfo::elementDeclsByType
	Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * ___elementDeclsByType_12;
	// System.Collections.Generic.Dictionary`2<System.String,System.Xml.Schema.SchemaNotation> System.Xml.Schema.SchemaInfo::notations
	Dictionary_2_t49DF5C20AE5B7EF983AFCFF5867364BA82492C70 * ___notations_13;

public:
	inline static int32_t get_offset_of_elementDecls_0() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___elementDecls_0)); }
	inline Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * get_elementDecls_0() const { return ___elementDecls_0; }
	inline Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 ** get_address_of_elementDecls_0() { return &___elementDecls_0; }
	inline void set_elementDecls_0(Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * value)
	{
		___elementDecls_0 = value;
		Il2CppCodeGenWriteBarrier((&___elementDecls_0), value);
	}

	inline static int32_t get_offset_of_undeclaredElementDecls_1() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___undeclaredElementDecls_1)); }
	inline Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * get_undeclaredElementDecls_1() const { return ___undeclaredElementDecls_1; }
	inline Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 ** get_address_of_undeclaredElementDecls_1() { return &___undeclaredElementDecls_1; }
	inline void set_undeclaredElementDecls_1(Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * value)
	{
		___undeclaredElementDecls_1 = value;
		Il2CppCodeGenWriteBarrier((&___undeclaredElementDecls_1), value);
	}

	inline static int32_t get_offset_of_generalEntities_2() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___generalEntities_2)); }
	inline Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 * get_generalEntities_2() const { return ___generalEntities_2; }
	inline Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 ** get_address_of_generalEntities_2() { return &___generalEntities_2; }
	inline void set_generalEntities_2(Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 * value)
	{
		___generalEntities_2 = value;
		Il2CppCodeGenWriteBarrier((&___generalEntities_2), value);
	}

	inline static int32_t get_offset_of_parameterEntities_3() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___parameterEntities_3)); }
	inline Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 * get_parameterEntities_3() const { return ___parameterEntities_3; }
	inline Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 ** get_address_of_parameterEntities_3() { return &___parameterEntities_3; }
	inline void set_parameterEntities_3(Dictionary_2_t9311F48415A23B087A8AB42B76BDCE3EE7963EF4 * value)
	{
		___parameterEntities_3 = value;
		Il2CppCodeGenWriteBarrier((&___parameterEntities_3), value);
	}

	inline static int32_t get_offset_of_docTypeName_4() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___docTypeName_4)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_docTypeName_4() const { return ___docTypeName_4; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_docTypeName_4() { return &___docTypeName_4; }
	inline void set_docTypeName_4(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___docTypeName_4 = value;
		Il2CppCodeGenWriteBarrier((&___docTypeName_4), value);
	}

	inline static int32_t get_offset_of_internalDtdSubset_5() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___internalDtdSubset_5)); }
	inline String_t* get_internalDtdSubset_5() const { return ___internalDtdSubset_5; }
	inline String_t** get_address_of_internalDtdSubset_5() { return &___internalDtdSubset_5; }
	inline void set_internalDtdSubset_5(String_t* value)
	{
		___internalDtdSubset_5 = value;
		Il2CppCodeGenWriteBarrier((&___internalDtdSubset_5), value);
	}

	inline static int32_t get_offset_of_hasNonCDataAttributes_6() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___hasNonCDataAttributes_6)); }
	inline bool get_hasNonCDataAttributes_6() const { return ___hasNonCDataAttributes_6; }
	inline bool* get_address_of_hasNonCDataAttributes_6() { return &___hasNonCDataAttributes_6; }
	inline void set_hasNonCDataAttributes_6(bool value)
	{
		___hasNonCDataAttributes_6 = value;
	}

	inline static int32_t get_offset_of_hasDefaultAttributes_7() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___hasDefaultAttributes_7)); }
	inline bool get_hasDefaultAttributes_7() const { return ___hasDefaultAttributes_7; }
	inline bool* get_address_of_hasDefaultAttributes_7() { return &___hasDefaultAttributes_7; }
	inline void set_hasDefaultAttributes_7(bool value)
	{
		___hasDefaultAttributes_7 = value;
	}

	inline static int32_t get_offset_of_targetNamespaces_8() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___targetNamespaces_8)); }
	inline Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B * get_targetNamespaces_8() const { return ___targetNamespaces_8; }
	inline Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B ** get_address_of_targetNamespaces_8() { return &___targetNamespaces_8; }
	inline void set_targetNamespaces_8(Dictionary_2_tC299681D95BE2E81CC7CBA606C4E9D07A00FA35B * value)
	{
		___targetNamespaces_8 = value;
		Il2CppCodeGenWriteBarrier((&___targetNamespaces_8), value);
	}

	inline static int32_t get_offset_of_attributeDecls_9() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___attributeDecls_9)); }
	inline Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 * get_attributeDecls_9() const { return ___attributeDecls_9; }
	inline Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 ** get_address_of_attributeDecls_9() { return &___attributeDecls_9; }
	inline void set_attributeDecls_9(Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 * value)
	{
		___attributeDecls_9 = value;
		Il2CppCodeGenWriteBarrier((&___attributeDecls_9), value);
	}

	inline static int32_t get_offset_of_errorCount_10() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___errorCount_10)); }
	inline int32_t get_errorCount_10() const { return ___errorCount_10; }
	inline int32_t* get_address_of_errorCount_10() { return &___errorCount_10; }
	inline void set_errorCount_10(int32_t value)
	{
		___errorCount_10 = value;
	}

	inline static int32_t get_offset_of_schemaType_11() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___schemaType_11)); }
	inline int32_t get_schemaType_11() const { return ___schemaType_11; }
	inline int32_t* get_address_of_schemaType_11() { return &___schemaType_11; }
	inline void set_schemaType_11(int32_t value)
	{
		___schemaType_11 = value;
	}

	inline static int32_t get_offset_of_elementDeclsByType_12() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___elementDeclsByType_12)); }
	inline Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * get_elementDeclsByType_12() const { return ___elementDeclsByType_12; }
	inline Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 ** get_address_of_elementDeclsByType_12() { return &___elementDeclsByType_12; }
	inline void set_elementDeclsByType_12(Dictionary_2_t8820E288E84B6C8B767808031F13493C54D26FC9 * value)
	{
		___elementDeclsByType_12 = value;
		Il2CppCodeGenWriteBarrier((&___elementDeclsByType_12), value);
	}

	inline static int32_t get_offset_of_notations_13() { return static_cast<int32_t>(offsetof(SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41, ___notations_13)); }
	inline Dictionary_2_t49DF5C20AE5B7EF983AFCFF5867364BA82492C70 * get_notations_13() const { return ___notations_13; }
	inline Dictionary_2_t49DF5C20AE5B7EF983AFCFF5867364BA82492C70 ** get_address_of_notations_13() { return &___notations_13; }
	inline void set_notations_13(Dictionary_2_t49DF5C20AE5B7EF983AFCFF5867364BA82492C70 * value)
	{
		___notations_13 = value;
		Il2CppCodeGenWriteBarrier((&___notations_13), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAINFO_TD9774489795A78B9235BAD315E5242C974183A41_H
#ifndef VALIDATIONEVENTARGS_T242553A3B613066E3FC046288572FB1E0D3DA230_H
#define VALIDATIONEVENTARGS_T242553A3B613066E3FC046288572FB1E0D3DA230_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventArgs
struct  ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230  : public EventArgs_t8E6CA180BE0E56674C6407011A94BAF7C757352E
{
public:
	// System.Xml.Schema.XmlSchemaException System.Xml.Schema.ValidationEventArgs::ex
	XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65 * ___ex_1;
	// System.Xml.Schema.XmlSeverityType System.Xml.Schema.ValidationEventArgs::severity
	int32_t ___severity_2;

public:
	inline static int32_t get_offset_of_ex_1() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230, ___ex_1)); }
	inline XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65 * get_ex_1() const { return ___ex_1; }
	inline XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65 ** get_address_of_ex_1() { return &___ex_1; }
	inline void set_ex_1(XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65 * value)
	{
		___ex_1 = value;
		Il2CppCodeGenWriteBarrier((&___ex_1), value);
	}

	inline static int32_t get_offset_of_severity_2() { return static_cast<int32_t>(offsetof(ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230, ___severity_2)); }
	inline int32_t get_severity_2() const { return ___severity_2; }
	inline int32_t* get_address_of_severity_2() { return &___severity_2; }
	inline void set_severity_2(int32_t value)
	{
		___severity_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTARGS_T242553A3B613066E3FC046288572FB1E0D3DA230_H
#ifndef VALIDATIONSTATE_T87AD0243195FC5C4B8643516747B92387F06A38B_H
#define VALIDATIONSTATE_T87AD0243195FC5C4B8643516747B92387F06A38B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationState
struct  ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B  : public RuntimeObject
{
public:
	// System.Boolean System.Xml.Schema.ValidationState::IsNill
	bool ___IsNill_0;
	// System.Boolean System.Xml.Schema.ValidationState::IsDefault
	bool ___IsDefault_1;
	// System.Boolean System.Xml.Schema.ValidationState::NeedValidateChildren
	bool ___NeedValidateChildren_2;
	// System.Boolean System.Xml.Schema.ValidationState::CheckRequiredAttribute
	bool ___CheckRequiredAttribute_3;
	// System.Boolean System.Xml.Schema.ValidationState::ValidationSkipped
	bool ___ValidationSkipped_4;
	// System.Xml.Schema.XmlSchemaContentProcessing System.Xml.Schema.ValidationState::ProcessContents
	int32_t ___ProcessContents_5;
	// System.Xml.Schema.XmlSchemaValidity System.Xml.Schema.ValidationState::Validity
	int32_t ___Validity_6;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.ValidationState::ElementDecl
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ___ElementDecl_7;
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.ValidationState::ElementDeclBeforeXsi
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ___ElementDeclBeforeXsi_8;
	// System.String System.Xml.Schema.ValidationState::LocalName
	String_t* ___LocalName_9;
	// System.String System.Xml.Schema.ValidationState::Namespace
	String_t* ___Namespace_10;
	// System.Xml.Schema.ConstraintStruct[] System.Xml.Schema.ValidationState::Constr
	ConstraintStructU5BU5D_tED6CE54D279C7B76E48DCD7F1365C68C51DC8541* ___Constr_11;
	// System.Xml.Schema.StateUnion System.Xml.Schema.ValidationState::CurrentState
	StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF  ___CurrentState_12;
	// System.Boolean System.Xml.Schema.ValidationState::HasMatched
	bool ___HasMatched_13;
	// System.Xml.Schema.BitSet[] System.Xml.Schema.ValidationState::CurPos
	BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* ___CurPos_14;
	// System.Xml.Schema.BitSet System.Xml.Schema.ValidationState::AllElementsSet
	BitSet_t0E4C53EC600670A4B74C5671553596978880138C * ___AllElementsSet_15;
	// System.Collections.Generic.List`1<System.Xml.Schema.RangePositionInfo> System.Xml.Schema.ValidationState::RunningPositions
	List_1_t388CC0CE82E700639DA5F7C3343C82038A226785 * ___RunningPositions_16;
	// System.Boolean System.Xml.Schema.ValidationState::TooComplex
	bool ___TooComplex_17;

public:
	inline static int32_t get_offset_of_IsNill_0() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___IsNill_0)); }
	inline bool get_IsNill_0() const { return ___IsNill_0; }
	inline bool* get_address_of_IsNill_0() { return &___IsNill_0; }
	inline void set_IsNill_0(bool value)
	{
		___IsNill_0 = value;
	}

	inline static int32_t get_offset_of_IsDefault_1() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___IsDefault_1)); }
	inline bool get_IsDefault_1() const { return ___IsDefault_1; }
	inline bool* get_address_of_IsDefault_1() { return &___IsDefault_1; }
	inline void set_IsDefault_1(bool value)
	{
		___IsDefault_1 = value;
	}

	inline static int32_t get_offset_of_NeedValidateChildren_2() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___NeedValidateChildren_2)); }
	inline bool get_NeedValidateChildren_2() const { return ___NeedValidateChildren_2; }
	inline bool* get_address_of_NeedValidateChildren_2() { return &___NeedValidateChildren_2; }
	inline void set_NeedValidateChildren_2(bool value)
	{
		___NeedValidateChildren_2 = value;
	}

	inline static int32_t get_offset_of_CheckRequiredAttribute_3() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___CheckRequiredAttribute_3)); }
	inline bool get_CheckRequiredAttribute_3() const { return ___CheckRequiredAttribute_3; }
	inline bool* get_address_of_CheckRequiredAttribute_3() { return &___CheckRequiredAttribute_3; }
	inline void set_CheckRequiredAttribute_3(bool value)
	{
		___CheckRequiredAttribute_3 = value;
	}

	inline static int32_t get_offset_of_ValidationSkipped_4() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___ValidationSkipped_4)); }
	inline bool get_ValidationSkipped_4() const { return ___ValidationSkipped_4; }
	inline bool* get_address_of_ValidationSkipped_4() { return &___ValidationSkipped_4; }
	inline void set_ValidationSkipped_4(bool value)
	{
		___ValidationSkipped_4 = value;
	}

	inline static int32_t get_offset_of_ProcessContents_5() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___ProcessContents_5)); }
	inline int32_t get_ProcessContents_5() const { return ___ProcessContents_5; }
	inline int32_t* get_address_of_ProcessContents_5() { return &___ProcessContents_5; }
	inline void set_ProcessContents_5(int32_t value)
	{
		___ProcessContents_5 = value;
	}

	inline static int32_t get_offset_of_Validity_6() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___Validity_6)); }
	inline int32_t get_Validity_6() const { return ___Validity_6; }
	inline int32_t* get_address_of_Validity_6() { return &___Validity_6; }
	inline void set_Validity_6(int32_t value)
	{
		___Validity_6 = value;
	}

	inline static int32_t get_offset_of_ElementDecl_7() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___ElementDecl_7)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get_ElementDecl_7() const { return ___ElementDecl_7; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of_ElementDecl_7() { return &___ElementDecl_7; }
	inline void set_ElementDecl_7(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		___ElementDecl_7 = value;
		Il2CppCodeGenWriteBarrier((&___ElementDecl_7), value);
	}

	inline static int32_t get_offset_of_ElementDeclBeforeXsi_8() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___ElementDeclBeforeXsi_8)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get_ElementDeclBeforeXsi_8() const { return ___ElementDeclBeforeXsi_8; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of_ElementDeclBeforeXsi_8() { return &___ElementDeclBeforeXsi_8; }
	inline void set_ElementDeclBeforeXsi_8(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		___ElementDeclBeforeXsi_8 = value;
		Il2CppCodeGenWriteBarrier((&___ElementDeclBeforeXsi_8), value);
	}

	inline static int32_t get_offset_of_LocalName_9() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___LocalName_9)); }
	inline String_t* get_LocalName_9() const { return ___LocalName_9; }
	inline String_t** get_address_of_LocalName_9() { return &___LocalName_9; }
	inline void set_LocalName_9(String_t* value)
	{
		___LocalName_9 = value;
		Il2CppCodeGenWriteBarrier((&___LocalName_9), value);
	}

	inline static int32_t get_offset_of_Namespace_10() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___Namespace_10)); }
	inline String_t* get_Namespace_10() const { return ___Namespace_10; }
	inline String_t** get_address_of_Namespace_10() { return &___Namespace_10; }
	inline void set_Namespace_10(String_t* value)
	{
		___Namespace_10 = value;
		Il2CppCodeGenWriteBarrier((&___Namespace_10), value);
	}

	inline static int32_t get_offset_of_Constr_11() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___Constr_11)); }
	inline ConstraintStructU5BU5D_tED6CE54D279C7B76E48DCD7F1365C68C51DC8541* get_Constr_11() const { return ___Constr_11; }
	inline ConstraintStructU5BU5D_tED6CE54D279C7B76E48DCD7F1365C68C51DC8541** get_address_of_Constr_11() { return &___Constr_11; }
	inline void set_Constr_11(ConstraintStructU5BU5D_tED6CE54D279C7B76E48DCD7F1365C68C51DC8541* value)
	{
		___Constr_11 = value;
		Il2CppCodeGenWriteBarrier((&___Constr_11), value);
	}

	inline static int32_t get_offset_of_CurrentState_12() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___CurrentState_12)); }
	inline StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF  get_CurrentState_12() const { return ___CurrentState_12; }
	inline StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF * get_address_of_CurrentState_12() { return &___CurrentState_12; }
	inline void set_CurrentState_12(StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF  value)
	{
		___CurrentState_12 = value;
	}

	inline static int32_t get_offset_of_HasMatched_13() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___HasMatched_13)); }
	inline bool get_HasMatched_13() const { return ___HasMatched_13; }
	inline bool* get_address_of_HasMatched_13() { return &___HasMatched_13; }
	inline void set_HasMatched_13(bool value)
	{
		___HasMatched_13 = value;
	}

	inline static int32_t get_offset_of_CurPos_14() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___CurPos_14)); }
	inline BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* get_CurPos_14() const { return ___CurPos_14; }
	inline BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69** get_address_of_CurPos_14() { return &___CurPos_14; }
	inline void set_CurPos_14(BitSetU5BU5D_tC61B9A480CE3460277DE0ABFA4551635BB337D69* value)
	{
		___CurPos_14 = value;
		Il2CppCodeGenWriteBarrier((&___CurPos_14), value);
	}

	inline static int32_t get_offset_of_AllElementsSet_15() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___AllElementsSet_15)); }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C * get_AllElementsSet_15() const { return ___AllElementsSet_15; }
	inline BitSet_t0E4C53EC600670A4B74C5671553596978880138C ** get_address_of_AllElementsSet_15() { return &___AllElementsSet_15; }
	inline void set_AllElementsSet_15(BitSet_t0E4C53EC600670A4B74C5671553596978880138C * value)
	{
		___AllElementsSet_15 = value;
		Il2CppCodeGenWriteBarrier((&___AllElementsSet_15), value);
	}

	inline static int32_t get_offset_of_RunningPositions_16() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___RunningPositions_16)); }
	inline List_1_t388CC0CE82E700639DA5F7C3343C82038A226785 * get_RunningPositions_16() const { return ___RunningPositions_16; }
	inline List_1_t388CC0CE82E700639DA5F7C3343C82038A226785 ** get_address_of_RunningPositions_16() { return &___RunningPositions_16; }
	inline void set_RunningPositions_16(List_1_t388CC0CE82E700639DA5F7C3343C82038A226785 * value)
	{
		___RunningPositions_16 = value;
		Il2CppCodeGenWriteBarrier((&___RunningPositions_16), value);
	}

	inline static int32_t get_offset_of_TooComplex_17() { return static_cast<int32_t>(offsetof(ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B, ___TooComplex_17)); }
	inline bool get_TooComplex_17() const { return ___TooComplex_17; }
	inline bool* get_address_of_TooComplex_17() { return &___TooComplex_17; }
	inline void set_TooComplex_17(bool value)
	{
		___TooComplex_17 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONSTATE_T87AD0243195FC5C4B8643516747B92387F06A38B_H
#ifndef XMLSCHEMAINFERENCE_TA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_H
#define XMLSCHEMAINFERENCE_TA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInference
struct  XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB  : public RuntimeObject
{
public:
	// System.Xml.Schema.XmlSchema System.Xml.Schema.XmlSchemaInference::rootSchema
	XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * ___rootSchema_21;
	// System.Xml.Schema.XmlSchemaSet System.Xml.Schema.XmlSchemaInference::schemaSet
	XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * ___schemaSet_22;
	// System.Xml.XmlReader System.Xml.Schema.XmlSchemaInference::xtr
	XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * ___xtr_23;
	// System.Xml.NameTable System.Xml.Schema.XmlSchemaInference::nametable
	NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C * ___nametable_24;
	// System.String System.Xml.Schema.XmlSchemaInference::TargetNamespace
	String_t* ___TargetNamespace_25;
	// System.Xml.XmlNamespaceManager System.Xml.Schema.XmlSchemaInference::NamespaceManager
	XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * ___NamespaceManager_26;
	// System.Collections.ArrayList System.Xml.Schema.XmlSchemaInference::schemaList
	ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * ___schemaList_27;
	// System.Xml.Schema.XmlSchemaInference/InferenceOption System.Xml.Schema.XmlSchemaInference::occurrence
	int32_t ___occurrence_28;
	// System.Xml.Schema.XmlSchemaInference/InferenceOption System.Xml.Schema.XmlSchemaInference::typeInference
	int32_t ___typeInference_29;

public:
	inline static int32_t get_offset_of_rootSchema_21() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___rootSchema_21)); }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * get_rootSchema_21() const { return ___rootSchema_21; }
	inline XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F ** get_address_of_rootSchema_21() { return &___rootSchema_21; }
	inline void set_rootSchema_21(XmlSchema_tBCBDA8467097049177257E2A0493EDF5C8FD477F * value)
	{
		___rootSchema_21 = value;
		Il2CppCodeGenWriteBarrier((&___rootSchema_21), value);
	}

	inline static int32_t get_offset_of_schemaSet_22() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___schemaSet_22)); }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * get_schemaSet_22() const { return ___schemaSet_22; }
	inline XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F ** get_address_of_schemaSet_22() { return &___schemaSet_22; }
	inline void set_schemaSet_22(XmlSchemaSet_tD92B4BF5F65FBF5B106399A36284FDC64E602F7F * value)
	{
		___schemaSet_22 = value;
		Il2CppCodeGenWriteBarrier((&___schemaSet_22), value);
	}

	inline static int32_t get_offset_of_xtr_23() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___xtr_23)); }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * get_xtr_23() const { return ___xtr_23; }
	inline XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB ** get_address_of_xtr_23() { return &___xtr_23; }
	inline void set_xtr_23(XmlReader_t13F08E3C651EB9F2AE882342BCD5E2CA86F29ABB * value)
	{
		___xtr_23 = value;
		Il2CppCodeGenWriteBarrier((&___xtr_23), value);
	}

	inline static int32_t get_offset_of_nametable_24() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___nametable_24)); }
	inline NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C * get_nametable_24() const { return ___nametable_24; }
	inline NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C ** get_address_of_nametable_24() { return &___nametable_24; }
	inline void set_nametable_24(NameTable_tB2F4359686539290B81EB39DCB1828BE81B11C8C * value)
	{
		___nametable_24 = value;
		Il2CppCodeGenWriteBarrier((&___nametable_24), value);
	}

	inline static int32_t get_offset_of_TargetNamespace_25() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___TargetNamespace_25)); }
	inline String_t* get_TargetNamespace_25() const { return ___TargetNamespace_25; }
	inline String_t** get_address_of_TargetNamespace_25() { return &___TargetNamespace_25; }
	inline void set_TargetNamespace_25(String_t* value)
	{
		___TargetNamespace_25 = value;
		Il2CppCodeGenWriteBarrier((&___TargetNamespace_25), value);
	}

	inline static int32_t get_offset_of_NamespaceManager_26() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___NamespaceManager_26)); }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * get_NamespaceManager_26() const { return ___NamespaceManager_26; }
	inline XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F ** get_address_of_NamespaceManager_26() { return &___NamespaceManager_26; }
	inline void set_NamespaceManager_26(XmlNamespaceManager_t8323BEB96BBE8F75207DC2AAFE9430E7F473658F * value)
	{
		___NamespaceManager_26 = value;
		Il2CppCodeGenWriteBarrier((&___NamespaceManager_26), value);
	}

	inline static int32_t get_offset_of_schemaList_27() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___schemaList_27)); }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * get_schemaList_27() const { return ___schemaList_27; }
	inline ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 ** get_address_of_schemaList_27() { return &___schemaList_27; }
	inline void set_schemaList_27(ArrayList_t4131E0C29C7E1B9BC9DFE37BEC41A5EB1481ADF4 * value)
	{
		___schemaList_27 = value;
		Il2CppCodeGenWriteBarrier((&___schemaList_27), value);
	}

	inline static int32_t get_offset_of_occurrence_28() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___occurrence_28)); }
	inline int32_t get_occurrence_28() const { return ___occurrence_28; }
	inline int32_t* get_address_of_occurrence_28() { return &___occurrence_28; }
	inline void set_occurrence_28(int32_t value)
	{
		___occurrence_28 = value;
	}

	inline static int32_t get_offset_of_typeInference_29() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB, ___typeInference_29)); }
	inline int32_t get_typeInference_29() const { return ___typeInference_29; }
	inline int32_t* get_address_of_typeInference_29() { return &___typeInference_29; }
	inline void set_typeInference_29(int32_t value)
	{
		___typeInference_29 = value;
	}
};

struct XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields
{
public:
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_boolean
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_boolean_0;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_byte
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_byte_1;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_unsignedByte
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_unsignedByte_2;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_short
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_short_3;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_unsignedShort
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_unsignedShort_4;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_int
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_int_5;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_unsignedInt
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_unsignedInt_6;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_long
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_long_7;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_unsignedLong
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_unsignedLong_8;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_integer
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_integer_9;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_decimal
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_decimal_10;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_float
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_float_11;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_double
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_double_12;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_duration
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_duration_13;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_dateTime
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_dateTime_14;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_time
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_time_15;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_date
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_date_16;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_gYearMonth
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_gYearMonth_17;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_string
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_string_18;
	// System.Xml.XmlQualifiedName System.Xml.Schema.XmlSchemaInference::ST_anySimpleType
	XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * ___ST_anySimpleType_19;
	// System.Xml.XmlQualifiedName[] System.Xml.Schema.XmlSchemaInference::SimpleTypes
	XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* ___SimpleTypes_20;

public:
	inline static int32_t get_offset_of_ST_boolean_0() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_boolean_0)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_boolean_0() const { return ___ST_boolean_0; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_boolean_0() { return &___ST_boolean_0; }
	inline void set_ST_boolean_0(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_boolean_0 = value;
		Il2CppCodeGenWriteBarrier((&___ST_boolean_0), value);
	}

	inline static int32_t get_offset_of_ST_byte_1() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_byte_1)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_byte_1() const { return ___ST_byte_1; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_byte_1() { return &___ST_byte_1; }
	inline void set_ST_byte_1(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_byte_1 = value;
		Il2CppCodeGenWriteBarrier((&___ST_byte_1), value);
	}

	inline static int32_t get_offset_of_ST_unsignedByte_2() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_unsignedByte_2)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_unsignedByte_2() const { return ___ST_unsignedByte_2; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_unsignedByte_2() { return &___ST_unsignedByte_2; }
	inline void set_ST_unsignedByte_2(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_unsignedByte_2 = value;
		Il2CppCodeGenWriteBarrier((&___ST_unsignedByte_2), value);
	}

	inline static int32_t get_offset_of_ST_short_3() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_short_3)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_short_3() const { return ___ST_short_3; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_short_3() { return &___ST_short_3; }
	inline void set_ST_short_3(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_short_3 = value;
		Il2CppCodeGenWriteBarrier((&___ST_short_3), value);
	}

	inline static int32_t get_offset_of_ST_unsignedShort_4() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_unsignedShort_4)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_unsignedShort_4() const { return ___ST_unsignedShort_4; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_unsignedShort_4() { return &___ST_unsignedShort_4; }
	inline void set_ST_unsignedShort_4(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_unsignedShort_4 = value;
		Il2CppCodeGenWriteBarrier((&___ST_unsignedShort_4), value);
	}

	inline static int32_t get_offset_of_ST_int_5() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_int_5)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_int_5() const { return ___ST_int_5; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_int_5() { return &___ST_int_5; }
	inline void set_ST_int_5(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_int_5 = value;
		Il2CppCodeGenWriteBarrier((&___ST_int_5), value);
	}

	inline static int32_t get_offset_of_ST_unsignedInt_6() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_unsignedInt_6)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_unsignedInt_6() const { return ___ST_unsignedInt_6; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_unsignedInt_6() { return &___ST_unsignedInt_6; }
	inline void set_ST_unsignedInt_6(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_unsignedInt_6 = value;
		Il2CppCodeGenWriteBarrier((&___ST_unsignedInt_6), value);
	}

	inline static int32_t get_offset_of_ST_long_7() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_long_7)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_long_7() const { return ___ST_long_7; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_long_7() { return &___ST_long_7; }
	inline void set_ST_long_7(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_long_7 = value;
		Il2CppCodeGenWriteBarrier((&___ST_long_7), value);
	}

	inline static int32_t get_offset_of_ST_unsignedLong_8() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_unsignedLong_8)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_unsignedLong_8() const { return ___ST_unsignedLong_8; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_unsignedLong_8() { return &___ST_unsignedLong_8; }
	inline void set_ST_unsignedLong_8(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_unsignedLong_8 = value;
		Il2CppCodeGenWriteBarrier((&___ST_unsignedLong_8), value);
	}

	inline static int32_t get_offset_of_ST_integer_9() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_integer_9)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_integer_9() const { return ___ST_integer_9; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_integer_9() { return &___ST_integer_9; }
	inline void set_ST_integer_9(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_integer_9 = value;
		Il2CppCodeGenWriteBarrier((&___ST_integer_9), value);
	}

	inline static int32_t get_offset_of_ST_decimal_10() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_decimal_10)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_decimal_10() const { return ___ST_decimal_10; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_decimal_10() { return &___ST_decimal_10; }
	inline void set_ST_decimal_10(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_decimal_10 = value;
		Il2CppCodeGenWriteBarrier((&___ST_decimal_10), value);
	}

	inline static int32_t get_offset_of_ST_float_11() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_float_11)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_float_11() const { return ___ST_float_11; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_float_11() { return &___ST_float_11; }
	inline void set_ST_float_11(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_float_11 = value;
		Il2CppCodeGenWriteBarrier((&___ST_float_11), value);
	}

	inline static int32_t get_offset_of_ST_double_12() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_double_12)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_double_12() const { return ___ST_double_12; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_double_12() { return &___ST_double_12; }
	inline void set_ST_double_12(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_double_12 = value;
		Il2CppCodeGenWriteBarrier((&___ST_double_12), value);
	}

	inline static int32_t get_offset_of_ST_duration_13() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_duration_13)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_duration_13() const { return ___ST_duration_13; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_duration_13() { return &___ST_duration_13; }
	inline void set_ST_duration_13(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_duration_13 = value;
		Il2CppCodeGenWriteBarrier((&___ST_duration_13), value);
	}

	inline static int32_t get_offset_of_ST_dateTime_14() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_dateTime_14)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_dateTime_14() const { return ___ST_dateTime_14; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_dateTime_14() { return &___ST_dateTime_14; }
	inline void set_ST_dateTime_14(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_dateTime_14 = value;
		Il2CppCodeGenWriteBarrier((&___ST_dateTime_14), value);
	}

	inline static int32_t get_offset_of_ST_time_15() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_time_15)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_time_15() const { return ___ST_time_15; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_time_15() { return &___ST_time_15; }
	inline void set_ST_time_15(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_time_15 = value;
		Il2CppCodeGenWriteBarrier((&___ST_time_15), value);
	}

	inline static int32_t get_offset_of_ST_date_16() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_date_16)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_date_16() const { return ___ST_date_16; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_date_16() { return &___ST_date_16; }
	inline void set_ST_date_16(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_date_16 = value;
		Il2CppCodeGenWriteBarrier((&___ST_date_16), value);
	}

	inline static int32_t get_offset_of_ST_gYearMonth_17() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_gYearMonth_17)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_gYearMonth_17() const { return ___ST_gYearMonth_17; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_gYearMonth_17() { return &___ST_gYearMonth_17; }
	inline void set_ST_gYearMonth_17(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_gYearMonth_17 = value;
		Il2CppCodeGenWriteBarrier((&___ST_gYearMonth_17), value);
	}

	inline static int32_t get_offset_of_ST_string_18() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_string_18)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_string_18() const { return ___ST_string_18; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_string_18() { return &___ST_string_18; }
	inline void set_ST_string_18(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_string_18 = value;
		Il2CppCodeGenWriteBarrier((&___ST_string_18), value);
	}

	inline static int32_t get_offset_of_ST_anySimpleType_19() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___ST_anySimpleType_19)); }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * get_ST_anySimpleType_19() const { return ___ST_anySimpleType_19; }
	inline XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD ** get_address_of_ST_anySimpleType_19() { return &___ST_anySimpleType_19; }
	inline void set_ST_anySimpleType_19(XmlQualifiedName_tF72E1729FE6150B6ADABFE331F26F5E743E15BAD * value)
	{
		___ST_anySimpleType_19 = value;
		Il2CppCodeGenWriteBarrier((&___ST_anySimpleType_19), value);
	}

	inline static int32_t get_offset_of_SimpleTypes_20() { return static_cast<int32_t>(offsetof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields, ___SimpleTypes_20)); }
	inline XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* get_SimpleTypes_20() const { return ___SimpleTypes_20; }
	inline XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B** get_address_of_SimpleTypes_20() { return &___SimpleTypes_20; }
	inline void set_SimpleTypes_20(XmlQualifiedNameU5BU5D_t375EB02A213AC919E02336B57A412BB761AFA18B* value)
	{
		___SimpleTypes_20 = value;
		Il2CppCodeGenWriteBarrier((&___SimpleTypes_20), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINFERENCE_TA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_H
#ifndef XMLSCHEMAINFERENCEEXCEPTION_TD9C6301FB52B70636F2CD8B24117B8A34067B42D_H
#define XMLSCHEMAINFERENCEEXCEPTION_TD9C6301FB52B70636F2CD8B24117B8A34067B42D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.XmlSchemaInferenceException
struct  XmlSchemaInferenceException_tD9C6301FB52B70636F2CD8B24117B8A34067B42D  : public XmlSchemaException_t6E118FD214784A2E7DE004B99148C2C4CCC1EE65
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // XMLSCHEMAINFERENCEEXCEPTION_TD9C6301FB52B70636F2CD8B24117B8A34067B42D_H
#ifndef DATATYPE_ANYSIMPLETYPE_TA6075F2CC91D824A74FCEEBA33F595BA46A8C232_H
#define DATATYPE_ANYSIMPLETYPE_TA6075F2CC91D824A74FCEEBA33F595BA46A8C232_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anySimpleType
struct  Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232  : public DatatypeImplementation_tB5894E69998AF6871FDFC14B2A95AE5F07CBB836
{
public:

public:
};

struct Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_anySimpleType::atomicValueType
	Type_t * ___atomicValueType_91;
	// System.Type System.Xml.Schema.Datatype_anySimpleType::listValueType
	Type_t * ___listValueType_92;

public:
	inline static int32_t get_offset_of_atomicValueType_91() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields, ___atomicValueType_91)); }
	inline Type_t * get_atomicValueType_91() const { return ___atomicValueType_91; }
	inline Type_t ** get_address_of_atomicValueType_91() { return &___atomicValueType_91; }
	inline void set_atomicValueType_91(Type_t * value)
	{
		___atomicValueType_91 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_91), value);
	}

	inline static int32_t get_offset_of_listValueType_92() { return static_cast<int32_t>(offsetof(Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232_StaticFields, ___listValueType_92)); }
	inline Type_t * get_listValueType_92() const { return ___listValueType_92; }
	inline Type_t ** get_address_of_listValueType_92() { return &___listValueType_92; }
	inline void set_listValueType_92(Type_t * value)
	{
		___listValueType_92 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_92), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYSIMPLETYPE_TA6075F2CC91D824A74FCEEBA33F595BA46A8C232_H
#ifndef NAMESPACELISTV1COMPAT_TE88585CC7445B43BF7754DA67FE104B88BE33F3B_H
#define NAMESPACELISTV1COMPAT_TE88585CC7445B43BF7754DA67FE104B88BE33F3B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.NamespaceListV1Compat
struct  NamespaceListV1Compat_tE88585CC7445B43BF7754DA67FE104B88BE33F3B  : public NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // NAMESPACELISTV1COMPAT_TE88585CC7445B43BF7754DA67FE104B88BE33F3B_H
#ifndef SCHEMAATTDEF_T6701AAE5762853EDC86D272CC2A79EABB36924A4_H
#define SCHEMAATTDEF_T6701AAE5762853EDC86D272CC2A79EABB36924A4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaAttDef
struct  SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4  : public SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B
{
public:
	// System.String System.Xml.Schema.SchemaAttDef::defExpanded
	String_t* ___defExpanded_11;
	// System.Int32 System.Xml.Schema.SchemaAttDef::lineNum
	int32_t ___lineNum_12;
	// System.Int32 System.Xml.Schema.SchemaAttDef::linePos
	int32_t ___linePos_13;
	// System.Int32 System.Xml.Schema.SchemaAttDef::valueLineNum
	int32_t ___valueLineNum_14;
	// System.Int32 System.Xml.Schema.SchemaAttDef::valueLinePos
	int32_t ___valueLinePos_15;
	// System.Xml.Schema.SchemaAttDef/Reserve System.Xml.Schema.SchemaAttDef::reserved
	int32_t ___reserved_16;
	// System.Boolean System.Xml.Schema.SchemaAttDef::defaultValueChecked
	bool ___defaultValueChecked_17;
	// System.Xml.Schema.XmlSchemaAttribute System.Xml.Schema.SchemaAttDef::schemaAttribute
	XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * ___schemaAttribute_18;

public:
	inline static int32_t get_offset_of_defExpanded_11() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___defExpanded_11)); }
	inline String_t* get_defExpanded_11() const { return ___defExpanded_11; }
	inline String_t** get_address_of_defExpanded_11() { return &___defExpanded_11; }
	inline void set_defExpanded_11(String_t* value)
	{
		___defExpanded_11 = value;
		Il2CppCodeGenWriteBarrier((&___defExpanded_11), value);
	}

	inline static int32_t get_offset_of_lineNum_12() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___lineNum_12)); }
	inline int32_t get_lineNum_12() const { return ___lineNum_12; }
	inline int32_t* get_address_of_lineNum_12() { return &___lineNum_12; }
	inline void set_lineNum_12(int32_t value)
	{
		___lineNum_12 = value;
	}

	inline static int32_t get_offset_of_linePos_13() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___linePos_13)); }
	inline int32_t get_linePos_13() const { return ___linePos_13; }
	inline int32_t* get_address_of_linePos_13() { return &___linePos_13; }
	inline void set_linePos_13(int32_t value)
	{
		___linePos_13 = value;
	}

	inline static int32_t get_offset_of_valueLineNum_14() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___valueLineNum_14)); }
	inline int32_t get_valueLineNum_14() const { return ___valueLineNum_14; }
	inline int32_t* get_address_of_valueLineNum_14() { return &___valueLineNum_14; }
	inline void set_valueLineNum_14(int32_t value)
	{
		___valueLineNum_14 = value;
	}

	inline static int32_t get_offset_of_valueLinePos_15() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___valueLinePos_15)); }
	inline int32_t get_valueLinePos_15() const { return ___valueLinePos_15; }
	inline int32_t* get_address_of_valueLinePos_15() { return &___valueLinePos_15; }
	inline void set_valueLinePos_15(int32_t value)
	{
		___valueLinePos_15 = value;
	}

	inline static int32_t get_offset_of_reserved_16() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___reserved_16)); }
	inline int32_t get_reserved_16() const { return ___reserved_16; }
	inline int32_t* get_address_of_reserved_16() { return &___reserved_16; }
	inline void set_reserved_16(int32_t value)
	{
		___reserved_16 = value;
	}

	inline static int32_t get_offset_of_defaultValueChecked_17() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___defaultValueChecked_17)); }
	inline bool get_defaultValueChecked_17() const { return ___defaultValueChecked_17; }
	inline bool* get_address_of_defaultValueChecked_17() { return &___defaultValueChecked_17; }
	inline void set_defaultValueChecked_17(bool value)
	{
		___defaultValueChecked_17 = value;
	}

	inline static int32_t get_offset_of_schemaAttribute_18() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4, ___schemaAttribute_18)); }
	inline XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * get_schemaAttribute_18() const { return ___schemaAttribute_18; }
	inline XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B ** get_address_of_schemaAttribute_18() { return &___schemaAttribute_18; }
	inline void set_schemaAttribute_18(XmlSchemaAttribute_tC31F76D28F8D593EFB409CD27FABFC32AF27E99B * value)
	{
		___schemaAttribute_18 = value;
		Il2CppCodeGenWriteBarrier((&___schemaAttribute_18), value);
	}
};

struct SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4_StaticFields
{
public:
	// System.Xml.Schema.SchemaAttDef System.Xml.Schema.SchemaAttDef::Empty
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * ___Empty_19;

public:
	inline static int32_t get_offset_of_Empty_19() { return static_cast<int32_t>(offsetof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4_StaticFields, ___Empty_19)); }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * get_Empty_19() const { return ___Empty_19; }
	inline SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 ** get_address_of_Empty_19() { return &___Empty_19; }
	inline void set_Empty_19(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4 * value)
	{
		___Empty_19 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_19), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAATTDEF_T6701AAE5762853EDC86D272CC2A79EABB36924A4_H
#ifndef SCHEMAELEMENTDECL_T1D19D717C111EFE96DCB86F7A029BE5E2616C466_H
#define SCHEMAELEMENTDECL_T1D19D717C111EFE96DCB86F7A029BE5E2616C466_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.SchemaElementDecl
struct  SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466  : public SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B
{
public:
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.Schema.SchemaAttDef> System.Xml.Schema.SchemaElementDecl::attdefs
	Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 * ___attdefs_11;
	// System.Collections.Generic.List`1<System.Xml.IDtdDefaultAttributeInfo> System.Xml.Schema.SchemaElementDecl::defaultAttdefs
	List_1_t2860FD4BD1AB958B9D33F7B140FDCC661ECEA601 * ___defaultAttdefs_12;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isIdDeclared
	bool ___isIdDeclared_13;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::hasNonCDataAttribute
	bool ___hasNonCDataAttribute_14;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isAbstract
	bool ___isAbstract_15;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isNillable
	bool ___isNillable_16;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::hasRequiredAttribute
	bool ___hasRequiredAttribute_17;
	// System.Boolean System.Xml.Schema.SchemaElementDecl::isNotationDeclared
	bool ___isNotationDeclared_18;
	// System.Collections.Generic.Dictionary`2<System.Xml.XmlQualifiedName,System.Xml.XmlQualifiedName> System.Xml.Schema.SchemaElementDecl::prohibitedAttributes
	Dictionary_2_t8F585B0FDF25A3B59F04BFC4617D77AF8AA45201 * ___prohibitedAttributes_19;
	// System.Xml.Schema.ContentValidator System.Xml.Schema.SchemaElementDecl::contentValidator
	ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * ___contentValidator_20;
	// System.Xml.Schema.XmlSchemaAnyAttribute System.Xml.Schema.SchemaElementDecl::anyAttribute
	XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * ___anyAttribute_21;
	// System.Xml.Schema.XmlSchemaDerivationMethod System.Xml.Schema.SchemaElementDecl::block
	int32_t ___block_22;
	// System.Xml.Schema.CompiledIdentityConstraint[] System.Xml.Schema.SchemaElementDecl::constraints
	CompiledIdentityConstraintU5BU5D_t37449FE0550E678ACBF6DC60976D6A23E52021D0* ___constraints_23;
	// System.Xml.Schema.XmlSchemaElement System.Xml.Schema.SchemaElementDecl::schemaElement
	XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * ___schemaElement_24;

public:
	inline static int32_t get_offset_of_attdefs_11() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___attdefs_11)); }
	inline Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 * get_attdefs_11() const { return ___attdefs_11; }
	inline Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 ** get_address_of_attdefs_11() { return &___attdefs_11; }
	inline void set_attdefs_11(Dictionary_2_t4827E5C3759E2764057CB4D7D67AF15BCDBA84C2 * value)
	{
		___attdefs_11 = value;
		Il2CppCodeGenWriteBarrier((&___attdefs_11), value);
	}

	inline static int32_t get_offset_of_defaultAttdefs_12() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___defaultAttdefs_12)); }
	inline List_1_t2860FD4BD1AB958B9D33F7B140FDCC661ECEA601 * get_defaultAttdefs_12() const { return ___defaultAttdefs_12; }
	inline List_1_t2860FD4BD1AB958B9D33F7B140FDCC661ECEA601 ** get_address_of_defaultAttdefs_12() { return &___defaultAttdefs_12; }
	inline void set_defaultAttdefs_12(List_1_t2860FD4BD1AB958B9D33F7B140FDCC661ECEA601 * value)
	{
		___defaultAttdefs_12 = value;
		Il2CppCodeGenWriteBarrier((&___defaultAttdefs_12), value);
	}

	inline static int32_t get_offset_of_isIdDeclared_13() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___isIdDeclared_13)); }
	inline bool get_isIdDeclared_13() const { return ___isIdDeclared_13; }
	inline bool* get_address_of_isIdDeclared_13() { return &___isIdDeclared_13; }
	inline void set_isIdDeclared_13(bool value)
	{
		___isIdDeclared_13 = value;
	}

	inline static int32_t get_offset_of_hasNonCDataAttribute_14() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___hasNonCDataAttribute_14)); }
	inline bool get_hasNonCDataAttribute_14() const { return ___hasNonCDataAttribute_14; }
	inline bool* get_address_of_hasNonCDataAttribute_14() { return &___hasNonCDataAttribute_14; }
	inline void set_hasNonCDataAttribute_14(bool value)
	{
		___hasNonCDataAttribute_14 = value;
	}

	inline static int32_t get_offset_of_isAbstract_15() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___isAbstract_15)); }
	inline bool get_isAbstract_15() const { return ___isAbstract_15; }
	inline bool* get_address_of_isAbstract_15() { return &___isAbstract_15; }
	inline void set_isAbstract_15(bool value)
	{
		___isAbstract_15 = value;
	}

	inline static int32_t get_offset_of_isNillable_16() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___isNillable_16)); }
	inline bool get_isNillable_16() const { return ___isNillable_16; }
	inline bool* get_address_of_isNillable_16() { return &___isNillable_16; }
	inline void set_isNillable_16(bool value)
	{
		___isNillable_16 = value;
	}

	inline static int32_t get_offset_of_hasRequiredAttribute_17() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___hasRequiredAttribute_17)); }
	inline bool get_hasRequiredAttribute_17() const { return ___hasRequiredAttribute_17; }
	inline bool* get_address_of_hasRequiredAttribute_17() { return &___hasRequiredAttribute_17; }
	inline void set_hasRequiredAttribute_17(bool value)
	{
		___hasRequiredAttribute_17 = value;
	}

	inline static int32_t get_offset_of_isNotationDeclared_18() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___isNotationDeclared_18)); }
	inline bool get_isNotationDeclared_18() const { return ___isNotationDeclared_18; }
	inline bool* get_address_of_isNotationDeclared_18() { return &___isNotationDeclared_18; }
	inline void set_isNotationDeclared_18(bool value)
	{
		___isNotationDeclared_18 = value;
	}

	inline static int32_t get_offset_of_prohibitedAttributes_19() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___prohibitedAttributes_19)); }
	inline Dictionary_2_t8F585B0FDF25A3B59F04BFC4617D77AF8AA45201 * get_prohibitedAttributes_19() const { return ___prohibitedAttributes_19; }
	inline Dictionary_2_t8F585B0FDF25A3B59F04BFC4617D77AF8AA45201 ** get_address_of_prohibitedAttributes_19() { return &___prohibitedAttributes_19; }
	inline void set_prohibitedAttributes_19(Dictionary_2_t8F585B0FDF25A3B59F04BFC4617D77AF8AA45201 * value)
	{
		___prohibitedAttributes_19 = value;
		Il2CppCodeGenWriteBarrier((&___prohibitedAttributes_19), value);
	}

	inline static int32_t get_offset_of_contentValidator_20() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___contentValidator_20)); }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * get_contentValidator_20() const { return ___contentValidator_20; }
	inline ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA ** get_address_of_contentValidator_20() { return &___contentValidator_20; }
	inline void set_contentValidator_20(ContentValidator_tF701012C2DEDAD821FACDC630F99D45BFCCC03AA * value)
	{
		___contentValidator_20 = value;
		Il2CppCodeGenWriteBarrier((&___contentValidator_20), value);
	}

	inline static int32_t get_offset_of_anyAttribute_21() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___anyAttribute_21)); }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * get_anyAttribute_21() const { return ___anyAttribute_21; }
	inline XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 ** get_address_of_anyAttribute_21() { return &___anyAttribute_21; }
	inline void set_anyAttribute_21(XmlSchemaAnyAttribute_t09BC4DE0DCA6086FA21F134EAE49241B32FAD078 * value)
	{
		___anyAttribute_21 = value;
		Il2CppCodeGenWriteBarrier((&___anyAttribute_21), value);
	}

	inline static int32_t get_offset_of_block_22() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___block_22)); }
	inline int32_t get_block_22() const { return ___block_22; }
	inline int32_t* get_address_of_block_22() { return &___block_22; }
	inline void set_block_22(int32_t value)
	{
		___block_22 = value;
	}

	inline static int32_t get_offset_of_constraints_23() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___constraints_23)); }
	inline CompiledIdentityConstraintU5BU5D_t37449FE0550E678ACBF6DC60976D6A23E52021D0* get_constraints_23() const { return ___constraints_23; }
	inline CompiledIdentityConstraintU5BU5D_t37449FE0550E678ACBF6DC60976D6A23E52021D0** get_address_of_constraints_23() { return &___constraints_23; }
	inline void set_constraints_23(CompiledIdentityConstraintU5BU5D_t37449FE0550E678ACBF6DC60976D6A23E52021D0* value)
	{
		___constraints_23 = value;
		Il2CppCodeGenWriteBarrier((&___constraints_23), value);
	}

	inline static int32_t get_offset_of_schemaElement_24() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466, ___schemaElement_24)); }
	inline XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * get_schemaElement_24() const { return ___schemaElement_24; }
	inline XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D ** get_address_of_schemaElement_24() { return &___schemaElement_24; }
	inline void set_schemaElement_24(XmlSchemaElement_tF3C95D404CFEB675C3BE8ECDC5C73FFD0F92568D * value)
	{
		___schemaElement_24 = value;
		Il2CppCodeGenWriteBarrier((&___schemaElement_24), value);
	}
};

struct SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466_StaticFields
{
public:
	// System.Xml.Schema.SchemaElementDecl System.Xml.Schema.SchemaElementDecl::Empty
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * ___Empty_25;

public:
	inline static int32_t get_offset_of_Empty_25() { return static_cast<int32_t>(offsetof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466_StaticFields, ___Empty_25)); }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * get_Empty_25() const { return ___Empty_25; }
	inline SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 ** get_address_of_Empty_25() { return &___Empty_25; }
	inline void set_Empty_25(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466 * value)
	{
		___Empty_25 = value;
		Il2CppCodeGenWriteBarrier((&___Empty_25), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCHEMAELEMENTDECL_T1D19D717C111EFE96DCB86F7A029BE5E2616C466_H
#ifndef VALIDATIONEVENTHANDLER_T4485151111870B499F014BC4EC05B066589AF4BF_H
#define VALIDATIONEVENTHANDLER_T4485151111870B499F014BC4EC05B066589AF4BF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.ValidationEventHandler
struct  ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF  : public MulticastDelegate_t
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // VALIDATIONEVENTHANDLER_T4485151111870B499F014BC4EC05B066589AF4BF_H
#ifndef DATATYPE_NOTATION_T1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_H
#define DATATYPE_NOTATION_T1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_NOTATION
struct  Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_NOTATION::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_NOTATION::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NOTATION_T1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_H
#ifndef DATATYPE_QNAME_T7993EC0C620D893BBAC71AB236FD7F188E2A5715_H
#define DATATYPE_QNAME_T7993EC0C620D893BBAC71AB236FD7F188E2A5715_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_QName
struct  Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_QName::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_QName::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_QNAME_T7993EC0C620D893BBAC71AB236FD7F188E2A5715_H
#ifndef DATATYPE_QNAMEXDR_TAA5A0F0CF528CA3E589104F18440F96E6A662D7D_H
#define DATATYPE_QNAMEXDR_TAA5A0F0CF528CA3E589104F18440F96E6A662D7D_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_QNameXdr
struct  Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_QNameXdr::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_QNameXdr::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_QNAMEXDR_TAA5A0F0CF528CA3E589104F18440F96E6A662D7D_H
#ifndef DATATYPE_ANYURI_T0B785E29DB189700A3F0269E9822DDEA573F1313_H
#define DATATYPE_ANYURI_T0B785E29DB189700A3F0269E9822DDEA573F1313_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_anyURI
struct  Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_anyURI::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_anyURI::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ANYURI_T0B785E29DB189700A3F0269E9822DDEA573F1313_H
#ifndef DATATYPE_BASE64BINARY_T9C6376D24073500D6657172197597F85F475F185_H
#define DATATYPE_BASE64BINARY_T9C6376D24073500D6657172197597F85F475F185_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_base64Binary
struct  Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_base64Binary::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_base64Binary::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_BASE64BINARY_T9C6376D24073500D6657172197597F85F475F185_H
#ifndef DATATYPE_CHAR_TBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_H
#define DATATYPE_CHAR_TBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_char
struct  Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_char::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_char::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_CHAR_TBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_H
#ifndef DATATYPE_DATETIMEBASE_T573E0786DE1269B42D9217F634B1226B41712353_H
#define DATATYPE_DATETIMEBASE_T573E0786DE1269B42D9217F634B1226B41712353_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTimeBase
struct  Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:
	// System.Xml.Schema.XsdDateTimeFlags System.Xml.Schema.Datatype_dateTimeBase::dateTimeFlags
	int32_t ___dateTimeFlags_95;

public:
	inline static int32_t get_offset_of_dateTimeFlags_95() { return static_cast<int32_t>(offsetof(Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353, ___dateTimeFlags_95)); }
	inline int32_t get_dateTimeFlags_95() const { return ___dateTimeFlags_95; }
	inline int32_t* get_address_of_dateTimeFlags_95() { return &___dateTimeFlags_95; }
	inline void set_dateTimeFlags_95(int32_t value)
	{
		___dateTimeFlags_95 = value;
	}
};

struct Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_dateTimeBase::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_dateTimeBase::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIMEBASE_T573E0786DE1269B42D9217F634B1226B41712353_H
#ifndef DATATYPE_DECIMAL_T3943079EC38B2D86AB31D0141D563FE2EC063F60_H
#define DATATYPE_DECIMAL_T3943079EC38B2D86AB31D0141D563FE2EC063F60_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_decimal
struct  Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_decimal::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_decimal::listValueType
	Type_t * ___listValueType_94;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_decimal::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_95;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_95() { return static_cast<int32_t>(offsetof(Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60_StaticFields, ___numeric10FacetsChecker_95)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_95() const { return ___numeric10FacetsChecker_95; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_95() { return &___numeric10FacetsChecker_95; }
	inline void set_numeric10FacetsChecker_95(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_95 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_95), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DECIMAL_T3943079EC38B2D86AB31D0141D563FE2EC063F60_H
#ifndef DATATYPE_DOUBLE_TD5564072018557D0CCFA5E2BFC9902E197F221F1_H
#define DATATYPE_DOUBLE_TD5564072018557D0CCFA5E2BFC9902E197F221F1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_double
struct  Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_double::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_double::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DOUBLE_TD5564072018557D0CCFA5E2BFC9902E197F221F1_H
#ifndef DATATYPE_FLOAT_T368DEB66F38BC7C4420BEFD96E4E4396C690DC09_H
#define DATATYPE_FLOAT_T368DEB66F38BC7C4420BEFD96E4E4396C690DC09_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_float
struct  Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_float::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_float::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FLOAT_T368DEB66F38BC7C4420BEFD96E4E4396C690DC09_H
#ifndef DATATYPE_HEXBINARY_T07DC59FE97A13F127E33761B615DAA27E05538C5_H
#define DATATYPE_HEXBINARY_T07DC59FE97A13F127E33761B615DAA27E05538C5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_hexBinary
struct  Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_hexBinary::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_hexBinary::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_HEXBINARY_T07DC59FE97A13F127E33761B615DAA27E05538C5_H
#ifndef DATATYPE_STRING_T99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2_H
#define DATATYPE_STRING_T99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_string
struct  Datatype_string_t99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_STRING_T99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2_H
#ifndef DATATYPE_UUID_T41832714DB16CAE4B2A752471268FEA913BA9261_H
#define DATATYPE_UUID_T41832714DB16CAE4B2A752471268FEA913BA9261_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_uuid
struct  Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261  : public Datatype_anySimpleType_tA6075F2CC91D824A74FCEEBA33F595BA46A8C232
{
public:

public:
};

struct Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_uuid::atomicValueType
	Type_t * ___atomicValueType_93;
	// System.Type System.Xml.Schema.Datatype_uuid::listValueType
	Type_t * ___listValueType_94;

public:
	inline static int32_t get_offset_of_atomicValueType_93() { return static_cast<int32_t>(offsetof(Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261_StaticFields, ___atomicValueType_93)); }
	inline Type_t * get_atomicValueType_93() const { return ___atomicValueType_93; }
	inline Type_t ** get_address_of_atomicValueType_93() { return &___atomicValueType_93; }
	inline void set_atomicValueType_93(Type_t * value)
	{
		___atomicValueType_93 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_93), value);
	}

	inline static int32_t get_offset_of_listValueType_94() { return static_cast<int32_t>(offsetof(Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261_StaticFields, ___listValueType_94)); }
	inline Type_t * get_listValueType_94() const { return ___listValueType_94; }
	inline Type_t ** get_address_of_listValueType_94() { return &___listValueType_94; }
	inline void set_listValueType_94(Type_t * value)
	{
		___listValueType_94 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_94), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UUID_T41832714DB16CAE4B2A752471268FEA913BA9261_H
#ifndef DATATYPE_DATE_TE8038D91441496FA0536248368640C6610494FC9_H
#define DATATYPE_DATE_TE8038D91441496FA0536248368640C6610494FC9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_date
struct  Datatype_date_tE8038D91441496FA0536248368640C6610494FC9  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATE_TE8038D91441496FA0536248368640C6610494FC9_H
#ifndef DATATYPE_DATETIME_T3B7EE99C29490E1897711256E01E7A9C2EC5D7C1_H
#define DATATYPE_DATETIME_T3B7EE99C29490E1897711256E01E7A9C2EC5D7C1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTime
struct  Datatype_dateTime_t3B7EE99C29490E1897711256E01E7A9C2EC5D7C1  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIME_T3B7EE99C29490E1897711256E01E7A9C2EC5D7C1_H
#ifndef DATATYPE_DATETIMENOTIMEZONE_TB38A3869FCE26CA891E346F278A75DD53C3C9E8A_H
#define DATATYPE_DATETIMENOTIMEZONE_TB38A3869FCE26CA891E346F278A75DD53C3C9E8A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTimeNoTimeZone
struct  Datatype_dateTimeNoTimeZone_tB38A3869FCE26CA891E346F278A75DD53C3C9E8A  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIMENOTIMEZONE_TB38A3869FCE26CA891E346F278A75DD53C3C9E8A_H
#ifndef DATATYPE_DATETIMETIMEZONE_T26637A58A0AEA601E60B74A13E84AF0B462AEC30_H
#define DATATYPE_DATETIMETIMEZONE_T26637A58A0AEA601E60B74A13E84AF0B462AEC30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_dateTimeTimeZone
struct  Datatype_dateTimeTimeZone_t26637A58A0AEA601E60B74A13E84AF0B462AEC30  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DATETIMETIMEZONE_T26637A58A0AEA601E60B74A13E84AF0B462AEC30_H
#ifndef DATATYPE_DAY_T9AA95BEEA6239157FAE3913BCB8EB642D9EEAFCA_H
#define DATATYPE_DAY_T9AA95BEEA6239157FAE3913BCB8EB642D9EEAFCA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_day
struct  Datatype_day_t9AA95BEEA6239157FAE3913BCB8EB642D9EEAFCA  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DAY_T9AA95BEEA6239157FAE3913BCB8EB642D9EEAFCA_H
#ifndef DATATYPE_DOUBLEXDR_T89272978D99AEE1C60486AB5A10A8E6512DB46CB_H
#define DATATYPE_DOUBLEXDR_T89272978D99AEE1C60486AB5A10A8E6512DB46CB_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_doubleXdr
struct  Datatype_doubleXdr_t89272978D99AEE1C60486AB5A10A8E6512DB46CB  : public Datatype_double_tD5564072018557D0CCFA5E2BFC9902E197F221F1
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_DOUBLEXDR_T89272978D99AEE1C60486AB5A10A8E6512DB46CB_H
#ifndef DATATYPE_FIXED_TC72574BB46878C56DCC8A72863E769293C98329E_H
#define DATATYPE_FIXED_TC72574BB46878C56DCC8A72863E769293C98329E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_fixed
struct  Datatype_fixed_tC72574BB46878C56DCC8A72863E769293C98329E  : public Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FIXED_TC72574BB46878C56DCC8A72863E769293C98329E_H
#ifndef DATATYPE_FLOATXDR_TB3D28790025F870EA84415F04A0C11D55254CAD7_H
#define DATATYPE_FLOATXDR_TB3D28790025F870EA84415F04A0C11D55254CAD7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_floatXdr
struct  Datatype_floatXdr_tB3D28790025F870EA84415F04A0C11D55254CAD7  : public Datatype_float_t368DEB66F38BC7C4420BEFD96E4E4396C690DC09
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_FLOATXDR_TB3D28790025F870EA84415F04A0C11D55254CAD7_H
#ifndef DATATYPE_INTEGER_T99C6B7493447CD4FE190F3A43C5C5619290AC5CC_H
#define DATATYPE_INTEGER_T99C6B7493447CD4FE190F3A43C5C5619290AC5CC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_integer
struct  Datatype_integer_t99C6B7493447CD4FE190F3A43C5C5619290AC5CC  : public Datatype_decimal_t3943079EC38B2D86AB31D0141D563FE2EC063F60
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_INTEGER_T99C6B7493447CD4FE190F3A43C5C5619290AC5CC_H
#ifndef DATATYPE_MONTH_TF9243E5E20EDEA28EBC203BED75465E4038F2414_H
#define DATATYPE_MONTH_TF9243E5E20EDEA28EBC203BED75465E4038F2414_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_month
struct  Datatype_month_tF9243E5E20EDEA28EBC203BED75465E4038F2414  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_MONTH_TF9243E5E20EDEA28EBC203BED75465E4038F2414_H
#ifndef DATATYPE_MONTHDAY_T6269B1DDEA4237E0BC7EB00F4FFE8D2C8FA106E1_H
#define DATATYPE_MONTHDAY_T6269B1DDEA4237E0BC7EB00F4FFE8D2C8FA106E1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_monthDay
struct  Datatype_monthDay_t6269B1DDEA4237E0BC7EB00F4FFE8D2C8FA106E1  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_MONTHDAY_T6269B1DDEA4237E0BC7EB00F4FFE8D2C8FA106E1_H
#ifndef DATATYPE_NORMALIZEDSTRING_T652AF623259AD1E631D5525B2D455ECAF1CC08C7_H
#define DATATYPE_NORMALIZEDSTRING_T652AF623259AD1E631D5525B2D455ECAF1CC08C7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_normalizedString
struct  Datatype_normalizedString_t652AF623259AD1E631D5525B2D455ECAF1CC08C7  : public Datatype_string_t99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NORMALIZEDSTRING_T652AF623259AD1E631D5525B2D455ECAF1CC08C7_H
#ifndef DATATYPE_NORMALIZEDSTRINGV1COMPAT_TF65D96870C161D4FB7A281B1D4249F2744AD0CDA_H
#define DATATYPE_NORMALIZEDSTRINGV1COMPAT_TF65D96870C161D4FB7A281B1D4249F2744AD0CDA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_normalizedStringV1Compat
struct  Datatype_normalizedStringV1Compat_tF65D96870C161D4FB7A281B1D4249F2744AD0CDA  : public Datatype_string_t99A2A08E58CE5D513A0C1BB3C4E9E06EE80935E2
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NORMALIZEDSTRINGV1COMPAT_TF65D96870C161D4FB7A281B1D4249F2744AD0CDA_H
#ifndef DATATYPE_TIME_TDC35793051A0FD77D955370F825F6F64E205201F_H
#define DATATYPE_TIME_TDC35793051A0FD77D955370F825F6F64E205201F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_time
struct  Datatype_time_tDC35793051A0FD77D955370F825F6F64E205201F  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TIME_TDC35793051A0FD77D955370F825F6F64E205201F_H
#ifndef DATATYPE_TIMENOTIMEZONE_TF4D280A3FF1A34193F6D92094BE1DA35D06BCFD4_H
#define DATATYPE_TIMENOTIMEZONE_TF4D280A3FF1A34193F6D92094BE1DA35D06BCFD4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_timeNoTimeZone
struct  Datatype_timeNoTimeZone_tF4D280A3FF1A34193F6D92094BE1DA35D06BCFD4  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TIMENOTIMEZONE_TF4D280A3FF1A34193F6D92094BE1DA35D06BCFD4_H
#ifndef DATATYPE_TIMETIMEZONE_TE477A7490FD8AA43B8D4D5F32DB5C0010AC26B41_H
#define DATATYPE_TIMETIMEZONE_TE477A7490FD8AA43B8D4D5F32DB5C0010AC26B41_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_timeTimeZone
struct  Datatype_timeTimeZone_tE477A7490FD8AA43B8D4D5F32DB5C0010AC26B41  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TIMETIMEZONE_TE477A7490FD8AA43B8D4D5F32DB5C0010AC26B41_H
#ifndef DATATYPE_YEAR_TAE9A96772EEFF0FD02512C8B526E60196A19D443_H
#define DATATYPE_YEAR_TAE9A96772EEFF0FD02512C8B526E60196A19D443_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_year
struct  Datatype_year_tAE9A96772EEFF0FD02512C8B526E60196A19D443  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_YEAR_TAE9A96772EEFF0FD02512C8B526E60196A19D443_H
#ifndef DATATYPE_YEARMONTH_T0CFBFE4C46B80B357E2F88FA99F1F008B50745DC_H
#define DATATYPE_YEARMONTH_T0CFBFE4C46B80B357E2F88FA99F1F008B50745DC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_yearMonth
struct  Datatype_yearMonth_t0CFBFE4C46B80B357E2F88FA99F1F008B50745DC  : public Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_YEARMONTH_T0CFBFE4C46B80B357E2F88FA99F1F008B50745DC_H
#ifndef DATATYPE_LONG_T8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_H
#define DATATYPE_LONG_T8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_long
struct  Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3  : public Datatype_integer_t99C6B7493447CD4FE190F3A43C5C5619290AC5CC
{
public:

public:
};

struct Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_long::atomicValueType
	Type_t * ___atomicValueType_96;
	// System.Type System.Xml.Schema.Datatype_long::listValueType
	Type_t * ___listValueType_97;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_long::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_98;

public:
	inline static int32_t get_offset_of_atomicValueType_96() { return static_cast<int32_t>(offsetof(Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields, ___atomicValueType_96)); }
	inline Type_t * get_atomicValueType_96() const { return ___atomicValueType_96; }
	inline Type_t ** get_address_of_atomicValueType_96() { return &___atomicValueType_96; }
	inline void set_atomicValueType_96(Type_t * value)
	{
		___atomicValueType_96 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_96), value);
	}

	inline static int32_t get_offset_of_listValueType_97() { return static_cast<int32_t>(offsetof(Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields, ___listValueType_97)); }
	inline Type_t * get_listValueType_97() const { return ___listValueType_97; }
	inline Type_t ** get_address_of_listValueType_97() { return &___listValueType_97; }
	inline void set_listValueType_97(Type_t * value)
	{
		___listValueType_97 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_97), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_98() { return static_cast<int32_t>(offsetof(Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields, ___numeric10FacetsChecker_98)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_98() const { return ___numeric10FacetsChecker_98; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_98() { return &___numeric10FacetsChecker_98; }
	inline void set_numeric10FacetsChecker_98(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_98 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_98), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_LONG_T8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_H
#ifndef DATATYPE_NONNEGATIVEINTEGER_T3D6BD75D9836160B5C0586D5F8B178962001BF62_H
#define DATATYPE_NONNEGATIVEINTEGER_T3D6BD75D9836160B5C0586D5F8B178962001BF62_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_nonNegativeInteger
struct  Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62  : public Datatype_integer_t99C6B7493447CD4FE190F3A43C5C5619290AC5CC
{
public:

public:
};

struct Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_nonNegativeInteger::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_96;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_96() { return static_cast<int32_t>(offsetof(Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62_StaticFields, ___numeric10FacetsChecker_96)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_96() const { return ___numeric10FacetsChecker_96; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_96() { return &___numeric10FacetsChecker_96; }
	inline void set_numeric10FacetsChecker_96(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_96 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_96), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NONNEGATIVEINTEGER_T3D6BD75D9836160B5C0586D5F8B178962001BF62_H
#ifndef DATATYPE_NONPOSITIVEINTEGER_T67540C91F10AD69AEE929F1142250F4E8DB9BA07_H
#define DATATYPE_NONPOSITIVEINTEGER_T67540C91F10AD69AEE929F1142250F4E8DB9BA07_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_nonPositiveInteger
struct  Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07  : public Datatype_integer_t99C6B7493447CD4FE190F3A43C5C5619290AC5CC
{
public:

public:
};

struct Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_nonPositiveInteger::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_96;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_96() { return static_cast<int32_t>(offsetof(Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07_StaticFields, ___numeric10FacetsChecker_96)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_96() const { return ___numeric10FacetsChecker_96; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_96() { return &___numeric10FacetsChecker_96; }
	inline void set_numeric10FacetsChecker_96(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_96 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_96), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NONPOSITIVEINTEGER_T67540C91F10AD69AEE929F1142250F4E8DB9BA07_H
#ifndef DATATYPE_TOKEN_T811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC_H
#define DATATYPE_TOKEN_T811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_token
struct  Datatype_token_t811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC  : public Datatype_normalizedString_t652AF623259AD1E631D5525B2D455ECAF1CC08C7
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TOKEN_T811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC_H
#ifndef DATATYPE_TOKENV1COMPAT_TF00A4DCE11FEE53DB7508812500DF04C8F1A739C_H
#define DATATYPE_TOKENV1COMPAT_TF00A4DCE11FEE53DB7508812500DF04C8F1A739C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_tokenV1Compat
struct  Datatype_tokenV1Compat_tF00A4DCE11FEE53DB7508812500DF04C8F1A739C  : public Datatype_normalizedStringV1Compat_tF65D96870C161D4FB7A281B1D4249F2744AD0CDA
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_TOKENV1COMPAT_TF00A4DCE11FEE53DB7508812500DF04C8F1A739C_H
#ifndef DATATYPE_NMTOKEN_T17CFFC978FEA1E66BA90F057792B6BFFB6BB9915_H
#define DATATYPE_NMTOKEN_T17CFFC978FEA1E66BA90F057792B6BFFB6BB9915_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_NMTOKEN
struct  Datatype_NMTOKEN_t17CFFC978FEA1E66BA90F057792B6BFFB6BB9915  : public Datatype_token_t811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NMTOKEN_T17CFFC978FEA1E66BA90F057792B6BFFB6BB9915_H
#ifndef DATATYPE_NAME_T3DB1A9AA4559CC0709780184FEAC5ADF5EFDBC46_H
#define DATATYPE_NAME_T3DB1A9AA4559CC0709780184FEAC5ADF5EFDBC46_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_Name
struct  Datatype_Name_t3DB1A9AA4559CC0709780184FEAC5ADF5EFDBC46  : public Datatype_token_t811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NAME_T3DB1A9AA4559CC0709780184FEAC5ADF5EFDBC46_H
#ifndef DATATYPE_INT_T32A2DAF1AF10277D4C6055090D6BA6FE6748C755_H
#define DATATYPE_INT_T32A2DAF1AF10277D4C6055090D6BA6FE6748C755_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_int
struct  Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755  : public Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3
{
public:

public:
};

struct Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_int::atomicValueType
	Type_t * ___atomicValueType_99;
	// System.Type System.Xml.Schema.Datatype_int::listValueType
	Type_t * ___listValueType_100;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_int::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_101;

public:
	inline static int32_t get_offset_of_atomicValueType_99() { return static_cast<int32_t>(offsetof(Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields, ___atomicValueType_99)); }
	inline Type_t * get_atomicValueType_99() const { return ___atomicValueType_99; }
	inline Type_t ** get_address_of_atomicValueType_99() { return &___atomicValueType_99; }
	inline void set_atomicValueType_99(Type_t * value)
	{
		___atomicValueType_99 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_99), value);
	}

	inline static int32_t get_offset_of_listValueType_100() { return static_cast<int32_t>(offsetof(Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields, ___listValueType_100)); }
	inline Type_t * get_listValueType_100() const { return ___listValueType_100; }
	inline Type_t ** get_address_of_listValueType_100() { return &___listValueType_100; }
	inline void set_listValueType_100(Type_t * value)
	{
		___listValueType_100 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_100), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_101() { return static_cast<int32_t>(offsetof(Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields, ___numeric10FacetsChecker_101)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_101() const { return ___numeric10FacetsChecker_101; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_101() { return &___numeric10FacetsChecker_101; }
	inline void set_numeric10FacetsChecker_101(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_101 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_101), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_INT_T32A2DAF1AF10277D4C6055090D6BA6FE6748C755_H
#ifndef DATATYPE_LANGUAGE_TCB5B7114F22C3831097890784CD7F624EFE9BEFE_H
#define DATATYPE_LANGUAGE_TCB5B7114F22C3831097890784CD7F624EFE9BEFE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_language
struct  Datatype_language_tCB5B7114F22C3831097890784CD7F624EFE9BEFE  : public Datatype_token_t811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_LANGUAGE_TCB5B7114F22C3831097890784CD7F624EFE9BEFE_H
#ifndef DATATYPE_NEGATIVEINTEGER_T028C1FA914A7D0A02A14925D5DF4784E1FF216D4_H
#define DATATYPE_NEGATIVEINTEGER_T028C1FA914A7D0A02A14925D5DF4784E1FF216D4_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_negativeInteger
struct  Datatype_negativeInteger_t028C1FA914A7D0A02A14925D5DF4784E1FF216D4  : public Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07
{
public:

public:
};

struct Datatype_negativeInteger_t028C1FA914A7D0A02A14925D5DF4784E1FF216D4_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_negativeInteger::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_97;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_97() { return static_cast<int32_t>(offsetof(Datatype_negativeInteger_t028C1FA914A7D0A02A14925D5DF4784E1FF216D4_StaticFields, ___numeric10FacetsChecker_97)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_97() const { return ___numeric10FacetsChecker_97; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_97() { return &___numeric10FacetsChecker_97; }
	inline void set_numeric10FacetsChecker_97(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_97 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_97), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NEGATIVEINTEGER_T028C1FA914A7D0A02A14925D5DF4784E1FF216D4_H
#ifndef DATATYPE_POSITIVEINTEGER_TBE8D810061BBF173E842CA699CA740F7541BF0AD_H
#define DATATYPE_POSITIVEINTEGER_TBE8D810061BBF173E842CA699CA740F7541BF0AD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_positiveInteger
struct  Datatype_positiveInteger_tBE8D810061BBF173E842CA699CA740F7541BF0AD  : public Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62
{
public:

public:
};

struct Datatype_positiveInteger_tBE8D810061BBF173E842CA699CA740F7541BF0AD_StaticFields
{
public:
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_positiveInteger::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_97;

public:
	inline static int32_t get_offset_of_numeric10FacetsChecker_97() { return static_cast<int32_t>(offsetof(Datatype_positiveInteger_tBE8D810061BBF173E842CA699CA740F7541BF0AD_StaticFields, ___numeric10FacetsChecker_97)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_97() const { return ___numeric10FacetsChecker_97; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_97() { return &___numeric10FacetsChecker_97; }
	inline void set_numeric10FacetsChecker_97(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_97 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_97), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_POSITIVEINTEGER_TBE8D810061BBF173E842CA699CA740F7541BF0AD_H
#ifndef DATATYPE_UNSIGNEDLONG_T82ADC913311BA0E255C03E8283D25F555D528C37_H
#define DATATYPE_UNSIGNEDLONG_T82ADC913311BA0E255C03E8283D25F555D528C37_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedLong
struct  Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37  : public Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62
{
public:

public:
};

struct Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedLong::atomicValueType
	Type_t * ___atomicValueType_97;
	// System.Type System.Xml.Schema.Datatype_unsignedLong::listValueType
	Type_t * ___listValueType_98;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedLong::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_99;

public:
	inline static int32_t get_offset_of_atomicValueType_97() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields, ___atomicValueType_97)); }
	inline Type_t * get_atomicValueType_97() const { return ___atomicValueType_97; }
	inline Type_t ** get_address_of_atomicValueType_97() { return &___atomicValueType_97; }
	inline void set_atomicValueType_97(Type_t * value)
	{
		___atomicValueType_97 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_97), value);
	}

	inline static int32_t get_offset_of_listValueType_98() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields, ___listValueType_98)); }
	inline Type_t * get_listValueType_98() const { return ___listValueType_98; }
	inline Type_t ** get_address_of_listValueType_98() { return &___listValueType_98; }
	inline void set_listValueType_98(Type_t * value)
	{
		___listValueType_98 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_98), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_99() { return static_cast<int32_t>(offsetof(Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields, ___numeric10FacetsChecker_99)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_99() const { return ___numeric10FacetsChecker_99; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_99() { return &___numeric10FacetsChecker_99; }
	inline void set_numeric10FacetsChecker_99(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_99 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_99), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDLONG_T82ADC913311BA0E255C03E8283D25F555D528C37_H
#ifndef DATATYPE_ENUMERATION_T8B137DDFC65E256AEC6C7EAC2167EBE020B392E9_H
#define DATATYPE_ENUMERATION_T8B137DDFC65E256AEC6C7EAC2167EBE020B392E9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_ENUMERATION
struct  Datatype_ENUMERATION_t8B137DDFC65E256AEC6C7EAC2167EBE020B392E9  : public Datatype_NMTOKEN_t17CFFC978FEA1E66BA90F057792B6BFFB6BB9915
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ENUMERATION_T8B137DDFC65E256AEC6C7EAC2167EBE020B392E9_H
#ifndef DATATYPE_NCNAME_T29EA53C078ECAD7F3C9EC236F7A4CB936BF62970_H
#define DATATYPE_NCNAME_T29EA53C078ECAD7F3C9EC236F7A4CB936BF62970_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_NCName
struct  Datatype_NCName_t29EA53C078ECAD7F3C9EC236F7A4CB936BF62970  : public Datatype_Name_t3DB1A9AA4559CC0709780184FEAC5ADF5EFDBC46
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_NCNAME_T29EA53C078ECAD7F3C9EC236F7A4CB936BF62970_H
#ifndef DATATYPE_SHORT_TD4B40037681531AE3395FC60B16FA87582D7E36C_H
#define DATATYPE_SHORT_TD4B40037681531AE3395FC60B16FA87582D7E36C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_short
struct  Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C  : public Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755
{
public:

public:
};

struct Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_short::atomicValueType
	Type_t * ___atomicValueType_102;
	// System.Type System.Xml.Schema.Datatype_short::listValueType
	Type_t * ___listValueType_103;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_short::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_104;

public:
	inline static int32_t get_offset_of_atomicValueType_102() { return static_cast<int32_t>(offsetof(Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields, ___atomicValueType_102)); }
	inline Type_t * get_atomicValueType_102() const { return ___atomicValueType_102; }
	inline Type_t ** get_address_of_atomicValueType_102() { return &___atomicValueType_102; }
	inline void set_atomicValueType_102(Type_t * value)
	{
		___atomicValueType_102 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_102), value);
	}

	inline static int32_t get_offset_of_listValueType_103() { return static_cast<int32_t>(offsetof(Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields, ___listValueType_103)); }
	inline Type_t * get_listValueType_103() const { return ___listValueType_103; }
	inline Type_t ** get_address_of_listValueType_103() { return &___listValueType_103; }
	inline void set_listValueType_103(Type_t * value)
	{
		___listValueType_103 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_103), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_104() { return static_cast<int32_t>(offsetof(Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields, ___numeric10FacetsChecker_104)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_104() const { return ___numeric10FacetsChecker_104; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_104() { return &___numeric10FacetsChecker_104; }
	inline void set_numeric10FacetsChecker_104(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_104 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_104), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_SHORT_TD4B40037681531AE3395FC60B16FA87582D7E36C_H
#ifndef DATATYPE_UNSIGNEDINT_T443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_H
#define DATATYPE_UNSIGNEDINT_T443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedInt
struct  Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3  : public Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37
{
public:

public:
};

struct Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedInt::atomicValueType
	Type_t * ___atomicValueType_100;
	// System.Type System.Xml.Schema.Datatype_unsignedInt::listValueType
	Type_t * ___listValueType_101;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedInt::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_102;

public:
	inline static int32_t get_offset_of_atomicValueType_100() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields, ___atomicValueType_100)); }
	inline Type_t * get_atomicValueType_100() const { return ___atomicValueType_100; }
	inline Type_t ** get_address_of_atomicValueType_100() { return &___atomicValueType_100; }
	inline void set_atomicValueType_100(Type_t * value)
	{
		___atomicValueType_100 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_100), value);
	}

	inline static int32_t get_offset_of_listValueType_101() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields, ___listValueType_101)); }
	inline Type_t * get_listValueType_101() const { return ___listValueType_101; }
	inline Type_t ** get_address_of_listValueType_101() { return &___listValueType_101; }
	inline void set_listValueType_101(Type_t * value)
	{
		___listValueType_101 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_101), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_102() { return static_cast<int32_t>(offsetof(Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields, ___numeric10FacetsChecker_102)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_102() const { return ___numeric10FacetsChecker_102; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_102() { return &___numeric10FacetsChecker_102; }
	inline void set_numeric10FacetsChecker_102(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_102 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_102), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDINT_T443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_H
#ifndef DATATYPE_ENTITY_T112F5B77BEAC25907AC28E7D04CF827676C4F612_H
#define DATATYPE_ENTITY_T112F5B77BEAC25907AC28E7D04CF827676C4F612_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_ENTITY
struct  Datatype_ENTITY_t112F5B77BEAC25907AC28E7D04CF827676C4F612  : public Datatype_NCName_t29EA53C078ECAD7F3C9EC236F7A4CB936BF62970
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ENTITY_T112F5B77BEAC25907AC28E7D04CF827676C4F612_H
#ifndef DATATYPE_ID_TACB9F9897ACC40D940F97D48E9F469B83A695811_H
#define DATATYPE_ID_TACB9F9897ACC40D940F97D48E9F469B83A695811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_ID
struct  Datatype_ID_tACB9F9897ACC40D940F97D48E9F469B83A695811  : public Datatype_NCName_t29EA53C078ECAD7F3C9EC236F7A4CB936BF62970
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_ID_TACB9F9897ACC40D940F97D48E9F469B83A695811_H
#ifndef DATATYPE_IDREF_TDFF85C859704CB4F8130291105A7126C236BCA30_H
#define DATATYPE_IDREF_TDFF85C859704CB4F8130291105A7126C236BCA30_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_IDREF
struct  Datatype_IDREF_tDFF85C859704CB4F8130291105A7126C236BCA30  : public Datatype_NCName_t29EA53C078ECAD7F3C9EC236F7A4CB936BF62970
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_IDREF_TDFF85C859704CB4F8130291105A7126C236BCA30_H
#ifndef DATATYPE_BYTE_TF416F50457EA9E7E545A3D882CB4C3014C881583_H
#define DATATYPE_BYTE_TF416F50457EA9E7E545A3D882CB4C3014C881583_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_byte
struct  Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583  : public Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C
{
public:

public:
};

struct Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_byte::atomicValueType
	Type_t * ___atomicValueType_105;
	// System.Type System.Xml.Schema.Datatype_byte::listValueType
	Type_t * ___listValueType_106;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_byte::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_107;

public:
	inline static int32_t get_offset_of_atomicValueType_105() { return static_cast<int32_t>(offsetof(Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields, ___atomicValueType_105)); }
	inline Type_t * get_atomicValueType_105() const { return ___atomicValueType_105; }
	inline Type_t ** get_address_of_atomicValueType_105() { return &___atomicValueType_105; }
	inline void set_atomicValueType_105(Type_t * value)
	{
		___atomicValueType_105 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_105), value);
	}

	inline static int32_t get_offset_of_listValueType_106() { return static_cast<int32_t>(offsetof(Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields, ___listValueType_106)); }
	inline Type_t * get_listValueType_106() const { return ___listValueType_106; }
	inline Type_t ** get_address_of_listValueType_106() { return &___listValueType_106; }
	inline void set_listValueType_106(Type_t * value)
	{
		___listValueType_106 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_106), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_107() { return static_cast<int32_t>(offsetof(Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields, ___numeric10FacetsChecker_107)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_107() const { return ___numeric10FacetsChecker_107; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_107() { return &___numeric10FacetsChecker_107; }
	inline void set_numeric10FacetsChecker_107(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_107 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_107), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_BYTE_TF416F50457EA9E7E545A3D882CB4C3014C881583_H
#ifndef DATATYPE_UNSIGNEDSHORT_TE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_H
#define DATATYPE_UNSIGNEDSHORT_TE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedShort
struct  Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E  : public Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3
{
public:

public:
};

struct Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedShort::atomicValueType
	Type_t * ___atomicValueType_103;
	// System.Type System.Xml.Schema.Datatype_unsignedShort::listValueType
	Type_t * ___listValueType_104;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedShort::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_105;

public:
	inline static int32_t get_offset_of_atomicValueType_103() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields, ___atomicValueType_103)); }
	inline Type_t * get_atomicValueType_103() const { return ___atomicValueType_103; }
	inline Type_t ** get_address_of_atomicValueType_103() { return &___atomicValueType_103; }
	inline void set_atomicValueType_103(Type_t * value)
	{
		___atomicValueType_103 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_103), value);
	}

	inline static int32_t get_offset_of_listValueType_104() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields, ___listValueType_104)); }
	inline Type_t * get_listValueType_104() const { return ___listValueType_104; }
	inline Type_t ** get_address_of_listValueType_104() { return &___listValueType_104; }
	inline void set_listValueType_104(Type_t * value)
	{
		___listValueType_104 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_104), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_105() { return static_cast<int32_t>(offsetof(Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields, ___numeric10FacetsChecker_105)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_105() const { return ___numeric10FacetsChecker_105; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_105() { return &___numeric10FacetsChecker_105; }
	inline void set_numeric10FacetsChecker_105(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_105 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_105), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDSHORT_TE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_H
#ifndef DATATYPE_UNSIGNEDBYTE_T485C5C1E2E309581DC97D926A22A8FAC573F7457_H
#define DATATYPE_UNSIGNEDBYTE_T485C5C1E2E309581DC97D926A22A8FAC573F7457_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Xml.Schema.Datatype_unsignedByte
struct  Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457  : public Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E
{
public:

public:
};

struct Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields
{
public:
	// System.Type System.Xml.Schema.Datatype_unsignedByte::atomicValueType
	Type_t * ___atomicValueType_106;
	// System.Type System.Xml.Schema.Datatype_unsignedByte::listValueType
	Type_t * ___listValueType_107;
	// System.Xml.Schema.FacetsChecker System.Xml.Schema.Datatype_unsignedByte::numeric10FacetsChecker
	FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * ___numeric10FacetsChecker_108;

public:
	inline static int32_t get_offset_of_atomicValueType_106() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields, ___atomicValueType_106)); }
	inline Type_t * get_atomicValueType_106() const { return ___atomicValueType_106; }
	inline Type_t ** get_address_of_atomicValueType_106() { return &___atomicValueType_106; }
	inline void set_atomicValueType_106(Type_t * value)
	{
		___atomicValueType_106 = value;
		Il2CppCodeGenWriteBarrier((&___atomicValueType_106), value);
	}

	inline static int32_t get_offset_of_listValueType_107() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields, ___listValueType_107)); }
	inline Type_t * get_listValueType_107() const { return ___listValueType_107; }
	inline Type_t ** get_address_of_listValueType_107() { return &___listValueType_107; }
	inline void set_listValueType_107(Type_t * value)
	{
		___listValueType_107 = value;
		Il2CppCodeGenWriteBarrier((&___listValueType_107), value);
	}

	inline static int32_t get_offset_of_numeric10FacetsChecker_108() { return static_cast<int32_t>(offsetof(Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields, ___numeric10FacetsChecker_108)); }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * get_numeric10FacetsChecker_108() const { return ___numeric10FacetsChecker_108; }
	inline FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 ** get_address_of_numeric10FacetsChecker_108() { return &___numeric10FacetsChecker_108; }
	inline void set_numeric10FacetsChecker_108(FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6 * value)
	{
		___numeric10FacetsChecker_108 = value;
		Il2CppCodeGenWriteBarrier((&___numeric10FacetsChecker_108), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // DATATYPE_UNSIGNEDBYTE_T485C5C1E2E309581DC97D926A22A8FAC573F7457_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2000 = { sizeof (Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353), -1, sizeof(Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2000[3] = 
{
	Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353_StaticFields::get_offset_of_listValueType_94(),
	Datatype_dateTimeBase_t573E0786DE1269B42D9217F634B1226B41712353::get_offset_of_dateTimeFlags_95(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2001 = { sizeof (Datatype_dateTimeNoTimeZone_tB38A3869FCE26CA891E346F278A75DD53C3C9E8A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2002 = { sizeof (Datatype_dateTimeTimeZone_t26637A58A0AEA601E60B74A13E84AF0B462AEC30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2003 = { sizeof (Datatype_dateTime_t3B7EE99C29490E1897711256E01E7A9C2EC5D7C1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2004 = { sizeof (Datatype_timeNoTimeZone_tF4D280A3FF1A34193F6D92094BE1DA35D06BCFD4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2005 = { sizeof (Datatype_timeTimeZone_tE477A7490FD8AA43B8D4D5F32DB5C0010AC26B41), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2006 = { sizeof (Datatype_time_tDC35793051A0FD77D955370F825F6F64E205201F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2007 = { sizeof (Datatype_date_tE8038D91441496FA0536248368640C6610494FC9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2008 = { sizeof (Datatype_yearMonth_t0CFBFE4C46B80B357E2F88FA99F1F008B50745DC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2009 = { sizeof (Datatype_year_tAE9A96772EEFF0FD02512C8B526E60196A19D443), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2010 = { sizeof (Datatype_monthDay_t6269B1DDEA4237E0BC7EB00F4FFE8D2C8FA106E1), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2011 = { sizeof (Datatype_day_t9AA95BEEA6239157FAE3913BCB8EB642D9EEAFCA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2012 = { sizeof (Datatype_month_tF9243E5E20EDEA28EBC203BED75465E4038F2414), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2013 = { sizeof (Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5), -1, sizeof(Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2013[2] = 
{
	Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_hexBinary_t07DC59FE97A13F127E33761B615DAA27E05538C5_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2014 = { sizeof (Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185), -1, sizeof(Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2014[2] = 
{
	Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_base64Binary_t9C6376D24073500D6657172197597F85F475F185_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2015 = { sizeof (Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313), -1, sizeof(Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2015[2] = 
{
	Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_anyURI_t0B785E29DB189700A3F0269E9822DDEA573F1313_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2016 = { sizeof (Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715), -1, sizeof(Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2016[2] = 
{
	Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_QName_t7993EC0C620D893BBAC71AB236FD7F188E2A5715_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2017 = { sizeof (Datatype_normalizedString_t652AF623259AD1E631D5525B2D455ECAF1CC08C7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2018 = { sizeof (Datatype_normalizedStringV1Compat_tF65D96870C161D4FB7A281B1D4249F2744AD0CDA), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2019 = { sizeof (Datatype_token_t811882BA35D8CFB23CB0CF2EC6F3F81F4B14CDBC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2020 = { sizeof (Datatype_tokenV1Compat_tF00A4DCE11FEE53DB7508812500DF04C8F1A739C), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2021 = { sizeof (Datatype_language_tCB5B7114F22C3831097890784CD7F624EFE9BEFE), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2022 = { sizeof (Datatype_NMTOKEN_t17CFFC978FEA1E66BA90F057792B6BFFB6BB9915), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2023 = { sizeof (Datatype_Name_t3DB1A9AA4559CC0709780184FEAC5ADF5EFDBC46), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2024 = { sizeof (Datatype_NCName_t29EA53C078ECAD7F3C9EC236F7A4CB936BF62970), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2025 = { sizeof (Datatype_ID_tACB9F9897ACC40D940F97D48E9F469B83A695811), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2026 = { sizeof (Datatype_IDREF_tDFF85C859704CB4F8130291105A7126C236BCA30), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2027 = { sizeof (Datatype_ENTITY_t112F5B77BEAC25907AC28E7D04CF827676C4F612), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2028 = { sizeof (Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040), -1, sizeof(Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2028[2] = 
{
	Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_NOTATION_t1D1F5E6B8B9FF9229A06EB15A3DB23A1D9AE2040_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2029 = { sizeof (Datatype_integer_t99C6B7493447CD4FE190F3A43C5C5619290AC5CC), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2030 = { sizeof (Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07), -1, sizeof(Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2030[1] = 
{
	Datatype_nonPositiveInteger_t67540C91F10AD69AEE929F1142250F4E8DB9BA07_StaticFields::get_offset_of_numeric10FacetsChecker_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2031 = { sizeof (Datatype_negativeInteger_t028C1FA914A7D0A02A14925D5DF4784E1FF216D4), -1, sizeof(Datatype_negativeInteger_t028C1FA914A7D0A02A14925D5DF4784E1FF216D4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2031[1] = 
{
	Datatype_negativeInteger_t028C1FA914A7D0A02A14925D5DF4784E1FF216D4_StaticFields::get_offset_of_numeric10FacetsChecker_97(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2032 = { sizeof (Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3), -1, sizeof(Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2032[3] = 
{
	Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields::get_offset_of_atomicValueType_96(),
	Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields::get_offset_of_listValueType_97(),
	Datatype_long_t8FE1FF17403DF5D19DB24DB0C76FE05247D2E8A3_StaticFields::get_offset_of_numeric10FacetsChecker_98(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2033 = { sizeof (Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755), -1, sizeof(Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2033[3] = 
{
	Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields::get_offset_of_atomicValueType_99(),
	Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields::get_offset_of_listValueType_100(),
	Datatype_int_t32A2DAF1AF10277D4C6055090D6BA6FE6748C755_StaticFields::get_offset_of_numeric10FacetsChecker_101(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2034 = { sizeof (Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C), -1, sizeof(Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2034[3] = 
{
	Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields::get_offset_of_atomicValueType_102(),
	Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields::get_offset_of_listValueType_103(),
	Datatype_short_tD4B40037681531AE3395FC60B16FA87582D7E36C_StaticFields::get_offset_of_numeric10FacetsChecker_104(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2035 = { sizeof (Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583), -1, sizeof(Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2035[3] = 
{
	Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields::get_offset_of_atomicValueType_105(),
	Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields::get_offset_of_listValueType_106(),
	Datatype_byte_tF416F50457EA9E7E545A3D882CB4C3014C881583_StaticFields::get_offset_of_numeric10FacetsChecker_107(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2036 = { sizeof (Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62), -1, sizeof(Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2036[1] = 
{
	Datatype_nonNegativeInteger_t3D6BD75D9836160B5C0586D5F8B178962001BF62_StaticFields::get_offset_of_numeric10FacetsChecker_96(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2037 = { sizeof (Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37), -1, sizeof(Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2037[3] = 
{
	Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields::get_offset_of_atomicValueType_97(),
	Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields::get_offset_of_listValueType_98(),
	Datatype_unsignedLong_t82ADC913311BA0E255C03E8283D25F555D528C37_StaticFields::get_offset_of_numeric10FacetsChecker_99(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2038 = { sizeof (Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3), -1, sizeof(Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2038[3] = 
{
	Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields::get_offset_of_atomicValueType_100(),
	Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields::get_offset_of_listValueType_101(),
	Datatype_unsignedInt_t443E2AD91BBF3BF7C272EE74A93D4C7C5CBC32E3_StaticFields::get_offset_of_numeric10FacetsChecker_102(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2039 = { sizeof (Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E), -1, sizeof(Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2039[3] = 
{
	Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields::get_offset_of_atomicValueType_103(),
	Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields::get_offset_of_listValueType_104(),
	Datatype_unsignedShort_tE75DFD8D7E26D195B4D3F20A9AE9B856CD881D4E_StaticFields::get_offset_of_numeric10FacetsChecker_105(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2040 = { sizeof (Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457), -1, sizeof(Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2040[3] = 
{
	Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields::get_offset_of_atomicValueType_106(),
	Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields::get_offset_of_listValueType_107(),
	Datatype_unsignedByte_t485C5C1E2E309581DC97D926A22A8FAC573F7457_StaticFields::get_offset_of_numeric10FacetsChecker_108(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2041 = { sizeof (Datatype_positiveInteger_tBE8D810061BBF173E842CA699CA740F7541BF0AD), -1, sizeof(Datatype_positiveInteger_tBE8D810061BBF173E842CA699CA740F7541BF0AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2041[1] = 
{
	Datatype_positiveInteger_tBE8D810061BBF173E842CA699CA740F7541BF0AD_StaticFields::get_offset_of_numeric10FacetsChecker_97(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2042 = { sizeof (Datatype_doubleXdr_t89272978D99AEE1C60486AB5A10A8E6512DB46CB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2043 = { sizeof (Datatype_floatXdr_tB3D28790025F870EA84415F04A0C11D55254CAD7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2044 = { sizeof (Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D), -1, sizeof(Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2044[2] = 
{
	Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_QNameXdr_tAA5A0F0CF528CA3E589104F18440F96E6A662D7D_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2045 = { sizeof (Datatype_ENUMERATION_t8B137DDFC65E256AEC6C7EAC2167EBE020B392E9), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2046 = { sizeof (Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE), -1, sizeof(Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2046[2] = 
{
	Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_char_tBBEAA1827B2D75F02D723EBAD8C61716DD236BEE_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2047 = { sizeof (Datatype_fixed_tC72574BB46878C56DCC8A72863E769293C98329E), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2048 = { sizeof (Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261), -1, sizeof(Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2048[2] = 
{
	Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261_StaticFields::get_offset_of_atomicValueType_93(),
	Datatype_uuid_t41832714DB16CAE4B2A752471268FEA913BA9261_StaticFields::get_offset_of_listValueType_94(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2049 = { sizeof (DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD), -1, sizeof(DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2049[7] = 
{
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD_StaticFields::get_offset_of_namespaceManager_15(),
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD::get_offset_of_validationStack_16(),
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD::get_offset_of_attPresence_17(),
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD::get_offset_of_name_18(),
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD::get_offset_of_IDs_19(),
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD::get_offset_of_idRefListHead_20(),
	DtdValidator_tF59970D29A90C3CAA4AED56E5625EFAA9DB287AD::get_offset_of_processIdentityConstraints_21(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2050 = { sizeof (NamespaceManager_t8A121C523956A538C9B6909B308301440F33F408), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2051 = { sizeof (FacetsChecker_t282414FF619686D9D855431F9A01C46BB6FDCBD6), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2052 = { sizeof (FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233)+ sizeof (RuntimeObject), -1, sizeof(FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2052[12] = 
{
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_datatype_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_derivedRestriction_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_baseFlags_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_baseFixedFlags_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_validRestrictionFlags_4() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_nonNegativeInt_5() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_builtInType_6() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_builtInEnum_7() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_firstPattern_8() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_regStr_9() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233::get_offset_of_pattern_facet_10() + static_cast<int32_t>(sizeof(RuntimeObject)),
	FacetsCompiler_t91E639F7F083D79393593CD660539AC912314233_StaticFields::get_offset_of_c_map_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2053 = { sizeof (Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D)+ sizeof (RuntimeObject), sizeof(Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D_marshaled_pinvoke), 0, 0 };
extern const int32_t g_FieldOffsetTable2053[2] = 
{
	Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D::get_offset_of_match_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	Map_t95CF8863CD8CAC88704F399964E9ED0524DD731D::get_offset_of_replacement_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2054 = { sizeof (Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD), -1, sizeof(Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2054[3] = 
{
	Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD_StaticFields::get_offset_of_signs_0(),
	Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD::get_offset_of_maxValue_1(),
	Numeric10FacetsChecker_tE598B3CEA8A066DAE4A47471B80D3ABF017810DD::get_offset_of_minValue_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2055 = { sizeof (Numeric2FacetsChecker_t5D69CF84FABD913681779290500406D1E84BDAA5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2056 = { sizeof (DurationFacetsChecker_t147376E625825EF88985237BAF989648FEA5B4A5), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2057 = { sizeof (DateTimeFacetsChecker_tD8BF6DE005C7EB84E9999EAD33A4C804EB45F5C4), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2058 = { sizeof (StringFacetsChecker_tBCB48F1B545187719429C40BE474CD7D6B0E93C0), -1, sizeof(StringFacetsChecker_tBCB48F1B545187719429C40BE474CD7D6B0E93C0_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2058[1] = 
{
	StringFacetsChecker_tBCB48F1B545187719429C40BE474CD7D6B0E93C0_StaticFields::get_offset_of_languagePattern_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2059 = { sizeof (QNameFacetsChecker_t56B938AEAB5FB017392DE9855706BF1B0B5FB411), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2060 = { sizeof (MiscFacetsChecker_tD88807B2E42CFA69E5A3BCA30E35D338D803B387), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2061 = { sizeof (BinaryFacetsChecker_t34E15822914D089ED851E76009B51CC2DEF3BB9D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2062 = { sizeof (ListFacetsChecker_tA6ADE6F856B37972B4A2F9A911BC452C04C589EB), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2063 = { sizeof (UnionFacetsChecker_t6977021EC464B23381AD978173641C6E4B14840A), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2064 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2065 = { sizeof (XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB), -1, sizeof(XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2065[30] = 
{
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_boolean_0(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_byte_1(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_unsignedByte_2(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_short_3(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_unsignedShort_4(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_int_5(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_unsignedInt_6(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_long_7(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_unsignedLong_8(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_integer_9(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_decimal_10(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_float_11(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_double_12(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_duration_13(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_dateTime_14(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_time_15(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_date_16(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_gYearMonth_17(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_string_18(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_ST_anySimpleType_19(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB_StaticFields::get_offset_of_SimpleTypes_20(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_rootSchema_21(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_schemaSet_22(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_xtr_23(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_nametable_24(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_TargetNamespace_25(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_NamespaceManager_26(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_schemaList_27(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_occurrence_28(),
	XmlSchemaInference_tA4076BCB7C5DC51CDEC7040922E5FC58E9F347CB::get_offset_of_typeInference_29(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2066 = { sizeof (InferenceOption_tDBA9EFB8F63C6E79C196133F252E07BF76503C80)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2066[3] = 
{
	InferenceOption_tDBA9EFB8F63C6E79C196133F252E07BF76503C80::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2067 = { sizeof (XmlSchemaInferenceException_tD9C6301FB52B70636F2CD8B24117B8A34067B42D), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2068 = { sizeof (NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2068[3] = 
{
	NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C::get_offset_of_type_0(),
	NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C::get_offset_of_set_1(),
	NamespaceList_t333B54F374AA7F3348AE9A2FD0210A465BBD6B8C::get_offset_of_targetNamespace_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2069 = { sizeof (ListType_t0322B2CB66D5748A27174A99131B5AD7A7ECDE0A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2069[4] = 
{
	ListType_t0322B2CB66D5748A27174A99131B5AD7A7ECDE0A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2070 = { sizeof (NamespaceListV1Compat_tE88585CC7445B43BF7754DA67FE104B88BE33F3B), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2071 = { sizeof (Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2071[20] = 
{
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_schemaType_0(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_nameTable_1(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_schemaNames_2(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_eventHandler_3(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_namespaceManager_4(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_reader_5(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_positionInfo_6(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_isProcessNamespaces_7(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_schemaXmlDepth_8(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_markupDepth_9(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_builder_10(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_schema_11(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_xdrSchema_12(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_xmlResolver_13(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_dummyDocument_14(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_processMarkup_15(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_parentNode_16(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_annotationNSManager_17(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_xmlns_18(),
	Parser_tAF8FC602A0D1411B91B8B88BABCB6E59F20EB920::get_offset_of_xmlCharType_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2072 = { sizeof (Compositor_tE5AE71A32402A0265385AABCE88457F1295A65E4)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2072[5] = 
{
	Compositor_tE5AE71A32402A0265385AABCE88457F1295A65E4::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2073 = { sizeof (RedefineEntry_tC1A902B85FDFB646528A0C10E852DFD952DAE14A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2073[2] = 
{
	RedefineEntry_tC1A902B85FDFB646528A0C10E852DFD952DAE14A::get_offset_of_redefine_0(),
	RedefineEntry_tC1A902B85FDFB646528A0C10E852DFD952DAE14A::get_offset_of_schemaToUpdate_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2074 = { sizeof (Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95), -1, sizeof(Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2074[19] = 
{
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_Xmlns_6(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_NsXsi_7(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_targetNamespace_8(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_rootSchema_9(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_currentSchema_10(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_elementFormDefault_11(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_attributeFormDefault_12(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_blockDefault_13(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_finalDefault_14(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_schemaLocations_15(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_chameleonSchemas_16(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_referenceNamespaces_17(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_processedExternals_18(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_lockList_19(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_readerSettings_20(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_rootSchemaForRedefine_21(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_redefinedList_22(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95_StaticFields::get_offset_of_builtInSchemaForXmlNS_23(),
	Preprocessor_tD4597BE3BBB8A4F56E72E7E98FF58D0D48660E95::get_offset_of_xmlResolver_24(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2075 = { sizeof (SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4), -1, sizeof(SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2075[9] = 
{
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_defExpanded_11(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_lineNum_12(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_linePos_13(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_valueLineNum_14(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_valueLinePos_15(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_reserved_16(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_defaultValueChecked_17(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4::get_offset_of_schemaAttribute_18(),
	SchemaAttDef_t6701AAE5762853EDC86D272CC2A79EABB36924A4_StaticFields::get_offset_of_Empty_19(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2076 = { sizeof (Reserve_t4CDF234391D57884636B9A96328C5CFC79ACCF06)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2076[4] = 
{
	Reserve_t4CDF234391D57884636B9A96328C5CFC79ACCF06::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2077 = { sizeof (SchemaBuilder_t6B2D813207751A3A8F89AB247E37E371F17450B7), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2078 = { sizeof (SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2078[4] = 
{
	SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D::get_offset_of_compileContentModel_6(),
	SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D::get_offset_of_examplars_7(),
	SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D::get_offset_of_complexTypeStack_8(),
	SchemaCollectionCompiler_tBDF40411A1DA3DEDDFF536F37442663EF77F7B6D::get_offset_of_schema_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2079 = { sizeof (SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2079[11] = 
{
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_schema_6(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_targetNamespace_7(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_buildinIncluded_8(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_elementFormDefault_9(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_attributeFormDefault_10(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_blockDefault_11(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_finalDefault_12(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_schemaLocations_13(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_referenceNamespaces_14(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_Xmlns_15(),
	SchemaCollectionPreprocessor_t75B7403D759DB36066607F965187B84D0EE25BA6::get_offset_of_xmlResolver_16(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2080 = { sizeof (Compositor_t7EC502C10E094F968544FD15F2EDB007B8C5C861)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2080[4] = 
{
	Compositor_t7EC502C10E094F968544FD15F2EDB007B8C5C861::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2081 = { sizeof (SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2081[11] = 
{
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_name_0(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_prefix_1(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_isDeclaredInExternal_2(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_presence_3(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_schemaType_4(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_datatype_5(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_defaultValueRaw_6(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_defaultValueTyped_7(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_maxLength_8(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_minLength_9(),
	SchemaDeclBase_t01E56CF5001308CDC9C9D6CC9E31648FC3F4772B::get_offset_of_values_10(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2082 = { sizeof (Use_t8F6205841F1E8F0578F601AC12C06E6C645B0CC5)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2082[6] = 
{
	Use_t8F6205841F1E8F0578F601AC12C06E6C645B0CC5::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2083 = { sizeof (SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466), -1, sizeof(SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2083[15] = 
{
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_attdefs_11(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_defaultAttdefs_12(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_isIdDeclared_13(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_hasNonCDataAttribute_14(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_isAbstract_15(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_isNillable_16(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_hasRequiredAttribute_17(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_isNotationDeclared_18(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_prohibitedAttributes_19(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_contentValidator_20(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_anyAttribute_21(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_block_22(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_constraints_23(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466::get_offset_of_schemaElement_24(),
	SchemaElementDecl_t1D19D717C111EFE96DCB86F7A029BE5E2616C466_StaticFields::get_offset_of_Empty_25(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2084 = { sizeof (SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2084[13] = 
{
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_qname_0(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_url_1(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_pubid_2(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_text_3(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_ndata_4(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_lineNumber_5(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_linePosition_6(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_isParameter_7(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_isExternal_8(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_parsingInProgress_9(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_isDeclaredInExternal_10(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_baseURI_11(),
	SchemaEntity_t242C3A5D0BEB3A39D02D3BE1B435D40C980E5A99::get_offset_of_declaredURI_12(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2085 = { sizeof (AttributeMatchState_t774FBBFD2CB98FEB43977A87396F624C329F5A59)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2085[11] = 
{
	AttributeMatchState_t774FBBFD2CB98FEB43977A87396F624C329F5A59::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2086 = { sizeof (SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2086[14] = 
{
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_elementDecls_0(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_undeclaredElementDecls_1(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_generalEntities_2(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_parameterEntities_3(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_docTypeName_4(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_internalDtdSubset_5(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_hasNonCDataAttributes_6(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_hasDefaultAttributes_7(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_targetNamespaces_8(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_attributeDecls_9(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_errorCount_10(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_schemaType_11(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_elementDeclsByType_12(),
	SchemaInfo_tD9774489795A78B9235BAD315E5242C974183A41::get_offset_of_notations_13(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2087 = { sizeof (SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2087[141] = 
{
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_nameTable_0(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsDataType_1(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsDataTypeAlias_2(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsDataTypeOld_3(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsXml_4(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsXmlNs_5(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsXdr_6(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsXdrAlias_7(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsXs_8(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_NsXsi_9(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_XsiType_10(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_XsiNil_11(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_XsiSchemaLocation_12(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_XsiNoNamespaceSchemaLocation_13(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_XsdSchema_14(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_XdrSchema_15(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnPCData_16(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXml_17(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXmlNs_18(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtDt_19(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXmlLang_20(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnName_21(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnType_22(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnMaxOccurs_23(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnMinOccurs_24(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnInfinite_25(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnModel_26(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnOpen_27(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnClosed_28(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnContent_29(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnMixed_30(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnEmpty_31(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnEltOnly_32(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnTextOnly_33(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnOrder_34(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnSeq_35(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnOne_36(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnMany_37(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnRequired_38(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnYes_39(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnNo_40(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnString_41(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnID_42(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnIDRef_43(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnIDRefs_44(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnEntity_45(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnEntities_46(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnNmToken_47(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnNmTokens_48(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnEnumeration_49(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDefault_50(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrSchema_51(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrElementType_52(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrElement_53(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrGroup_54(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrAttributeType_55(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrAttribute_56(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrDataType_57(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrDescription_58(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrExtends_59(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXdrAliasSchema_60(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtType_61(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtValues_62(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtMaxLength_63(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtMinLength_64(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtMax_65(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtMin_66(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtMinExclusive_67(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDtMaxExclusive_68(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnTargetNamespace_69(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnVersion_70(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnFinalDefault_71(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnBlockDefault_72(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnFixed_73(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnAbstract_74(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnBlock_75(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnSubstitutionGroup_76(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnFinal_77(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnNillable_78(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnRef_79(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnBase_80(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnDerivedBy_81(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnNamespace_82(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnProcessContents_83(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnRefer_84(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnPublic_85(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnSystem_86(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnSchemaLocation_87(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnValue_88(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnUse_89(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnForm_90(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnElementFormDefault_91(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnAttributeFormDefault_92(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnItemType_93(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnMemberTypes_94(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXPath_95(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdSchema_96(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAnnotation_97(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdInclude_98(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdImport_99(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdElement_100(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAttribute_101(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAttributeGroup_102(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAnyAttribute_103(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdGroup_104(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAll_105(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdChoice_106(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdSequence_107(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAny_108(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdNotation_109(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdSimpleType_110(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdComplexType_111(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdUnique_112(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdKey_113(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdKeyRef_114(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdSelector_115(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdField_116(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdMinExclusive_117(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdMinInclusive_118(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdMaxInclusive_119(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdMaxExclusive_120(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdTotalDigits_121(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdFractionDigits_122(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdLength_123(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdMinLength_124(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdMaxLength_125(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdEnumeration_126(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdPattern_127(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdDocumentation_128(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAppinfo_129(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnSource_130(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdComplexContent_131(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdSimpleContent_132(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdRestriction_133(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdExtension_134(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdUnion_135(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdList_136(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdWhiteSpace_137(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdRedefine_138(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_QnXsdAnyType_139(),
	SchemaNames_t15BD91B1C74FC6C9F9825D572D9C2F15726A1E89::get_offset_of_TokenToQName_140(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2088 = { sizeof (Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2088[124] = 
{
	Token_t29EB684F3183FA0B300324C6C6BA75403A81FC9A::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2089 = { sizeof (SchemaNamespaceManager_t3DF19809C72DF93A44C9479762DDECC0C84EC970), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2089[1] = 
{
	SchemaNamespaceManager_t3DF19809C72DF93A44C9479762DDECC0C84EC970::get_offset_of_node_8(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2090 = { sizeof (SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2090[3] = 
{
	SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A::get_offset_of_name_0(),
	SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A::get_offset_of_systemLiteral_1(),
	SchemaNotation_tFF41B5C1A3A42A06BE6A936AD4DC9CBBCFFA404A::get_offset_of_pubid_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2091 = { sizeof (Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2091[13] = 
{
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_restrictionErrorMsg_6(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_attributes_7(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_attributeGroups_8(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_elements_9(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_schemaTypes_10(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_groups_11(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_notations_12(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_examplars_13(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_identityConstraints_14(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_complexTypeStack_15(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_schemasToCompile_16(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_importedSchemas_17(),
	Compiler_tDAED4A04EAC4AC25E66CCEC00A923DA5176F2E89::get_offset_of_schemaForSchema_18(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2092 = { sizeof (SchemaType_t0EE4FECE10D4045A0C84698FEFBA18D9C34992DF)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
extern const int32_t g_FieldOffsetTable2092[5] = 
{
	SchemaType_t0EE4FECE10D4045A0C84698FEFBA18D9C34992DF::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2093 = { sizeof (ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2093[2] = 
{
	ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230::get_offset_of_ex_1(),
	ValidationEventArgs_t242553A3B613066E3FC046288572FB1E0D3DA230::get_offset_of_severity_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2094 = { sizeof (ValidationEventHandler_t4485151111870B499F014BC4EC05B066589AF4BF), sizeof(Il2CppMethodPointer), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2095 = { sizeof (StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF)+ sizeof (RuntimeObject), sizeof(StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF ), 0, 0 };
extern const int32_t g_FieldOffsetTable2095[4] = 
{
	StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF::get_offset_of_State_0() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF::get_offset_of_AllElementsRequired_1() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF::get_offset_of_CurPosIndex_2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	StateUnion_t9A043C39BC0361F60194C33113B629BD87A96EEF::get_offset_of_NumberOfRunningPos_3() + static_cast<int32_t>(sizeof(RuntimeObject)),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2096 = { sizeof (ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2096[18] = 
{
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_IsNill_0(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_IsDefault_1(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_NeedValidateChildren_2(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_CheckRequiredAttribute_3(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_ValidationSkipped_4(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_ProcessContents_5(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_Validity_6(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_ElementDecl_7(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_ElementDeclBeforeXsi_8(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_LocalName_9(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_Namespace_10(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_Constr_11(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_CurrentState_12(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_HasMatched_13(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_CurPos_14(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_AllElementsSet_15(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_RunningPositions_16(),
	ValidationState_t87AD0243195FC5C4B8643516747B92387F06A38B::get_offset_of_TooComplex_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2097 = { sizeof (XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2), -1, sizeof(XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable2097[37] = 
{
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Root_Element_0(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Root_SubElements_1(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_ElementType_SubElements_2(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_AttributeType_SubElements_3(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Group_SubElements_4(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Root_Attributes_5(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_ElementType_Attributes_6(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_AttributeType_Attributes_7(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Element_Attributes_8(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Attribute_Attributes_9(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_Group_Attributes_10(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_ElementDataType_Attributes_11(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_XDR_AttributeDataType_Attributes_12(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2_StaticFields::get_offset_of_S_SchemaEntries_13(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__SchemaInfo_14(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__TargetNamespace_15(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__reader_16(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of_positionInfo_17(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__contentValidator_18(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__CurState_19(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__NextState_20(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__StateHistory_21(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__GroupStack_22(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__XdrName_23(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__XdrPrefix_24(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__ElementDef_25(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__GroupDef_26(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__AttributeDef_27(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__UndefinedAttributeTypes_28(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__BaseDecl_29(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__NameTable_30(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__SchemaNames_31(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__CurNsMgr_32(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__Text_33(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of_validationEventHandler_34(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of__UndeclaredElements_35(),
	XdrBuilder_tD931491C090D5C32949D13F90BCAFC9A6380D3D2::get_offset_of_xmlResolver_36(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2098 = { sizeof (DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2098[12] = 
{
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Name_0(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Prefix_1(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__TypeName_2(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__TypePrefix_3(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Default_4(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Revises_5(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__MaxOccurs_6(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__MinOccurs_7(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Checking_8(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__ElementDecl_9(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Attdef_10(),
	DeclBaseInfo_tDCAE45F69D14C5B4B582AEF4564C500A656D1C0B::get_offset_of__Next_11(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize2099 = { sizeof (GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable2099[5] = 
{
	GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE::get_offset_of__MinVal_0(),
	GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE::get_offset_of__MaxVal_1(),
	GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE::get_offset_of__HasMaxAttr_2(),
	GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE::get_offset_of__HasMinAttr_3(),
	GroupContent_tA340212E7B02F065B9D0A4764545DD0360B13EBE::get_offset_of__Order_4(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
