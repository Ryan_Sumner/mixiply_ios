﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2<Zenject.PrefabResourceSingletonProviderCreator/PrefabId,Zenject.IPrefabInstantiator>
struct Dictionary_2_t036520DC7F6A25275BF333DAD200A2E127365F66;
// System.Collections.Generic.Dictionary`2<Zenject.PrefabSingletonProviderCreator/PrefabId,Zenject.IPrefabInstantiator>
struct Dictionary_2_t695060A2E0C3A93231AB54496586F254B36BAB84;
// System.Collections.Generic.HashSet`1<System.Object>
struct HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<System.Object>>
struct IEnumerator_1_t883C7A6A7B96519C2EFA6500ED941131CCF16129;
// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject>
struct IEnumerator_1_t17C77FE93B057D368EC24057B5FD86F1543E35F4;
// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo>
struct IEnumerator_1_tFA8D8F79D49027B6879F69EF42EF8DC63E776606;
// System.Collections.Generic.List`1<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>>
struct List_1_t9AC73A41C48344C2C96A2DBD179618B08EB43137;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D;
// System.Collections.Generic.List`1<Zenject.DiContainer>
struct List_1_t613607F896640D94824672D01078AB4BBFA62166;
// System.Collections.Generic.List`1<Zenject.TypeValuePair>
struct List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7;
// System.Func`2<UnityEngine.Object,UnityEngine.Object>
struct Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89;
// System.Func`2<Zenject.InjectContext,System.Object>
struct Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191;
// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject>
struct Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16;
// System.String
struct String_t;
// System.Type
struct Type_t;
// UnityEngine.GameObject
struct GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// Zenject.AddToCurrentGameObjectComponentProvider
struct AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C;
// Zenject.AddToGameObjectComponentProviderBase
struct AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A;
// Zenject.BindingId
struct BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1;
// Zenject.CachedProvider
struct CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8;
// Zenject.DiContainer
struct DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9;
// Zenject.EmptyGameObjectProvider
struct EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8;
// Zenject.GameObjectCreationParameters
struct GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4;
// Zenject.GetFromPrefabComponentProvider
struct GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003;
// Zenject.IPrefabInstantiator
struct IPrefabInstantiator_t075905F00BEED2A67E16E8748273944C09120E1F;
// Zenject.IPrefabProvider
struct IPrefabProvider_t03CCCDA76ECC8346AB1BF4B6BEB0BAD1209DABB0;
// Zenject.IProvider
struct IProvider_t5CB6F6DB730623507AEEA8FF558BFF4690973231;
// Zenject.InjectContext
struct InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649;
// Zenject.InstanceProvider
struct InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2;
// Zenject.InstantiateOnPrefabComponentProvider
struct InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192;
// Zenject.MethodProviderUntyped
struct MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA;
// Zenject.PrefabGameObjectProvider
struct PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22;
// Zenject.PrefabInstantiator
struct PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE;
// Zenject.PrefabInstantiatorCached
struct PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943;
// Zenject.ResolveProvider
struct ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0;
// Zenject.ResourceProvider
struct ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C;
// Zenject.ScriptableObjectResourceProvider
struct ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054;
// Zenject.SingletonMarkRegistry
struct SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751;




#ifndef RUNTIMEOBJECT_H
#define RUNTIMEOBJECT_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RUNTIMEOBJECT_H
#ifndef VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#define VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.ValueType
struct  ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};
#endif // VALUETYPE_T4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_H
#ifndef ADDTOCURRENTGAMEOBJECTCOMPONENTPROVIDER_T0C29BFB8B00F95804A40BDF7967D9E1867D8005C_H
#define ADDTOCURRENTGAMEOBJECTCOMPONENTPROVIDER_T0C29BFB8B00F95804A40BDF7967D9E1867D8005C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToCurrentGameObjectComponentProvider
struct  AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C  : public RuntimeObject
{
public:
	// System.Object Zenject.AddToCurrentGameObjectComponentProvider::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_0;
	// System.Type Zenject.AddToCurrentGameObjectComponentProvider::_componentType
	Type_t * ____componentType_1;
	// Zenject.DiContainer Zenject.AddToCurrentGameObjectComponentProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToCurrentGameObjectComponentProvider::_extraArguments
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArguments_3;

public:
	inline static int32_t get_offset_of__concreteIdentifier_0() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C, ____concreteIdentifier_0)); }
	inline RuntimeObject * get__concreteIdentifier_0() const { return ____concreteIdentifier_0; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_0() { return &____concreteIdentifier_0; }
	inline void set__concreteIdentifier_0(RuntimeObject * value)
	{
		____concreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C, ____extraArguments_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOCURRENTGAMEOBJECTCOMPONENTPROVIDER_T0C29BFB8B00F95804A40BDF7967D9E1867D8005C_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__12_T945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__12_T945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12
struct  U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8  : public RuntimeObject
{
public:
	// System.Int32 Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.AddToCurrentGameObjectComponentProvider Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12::<>4__this
	AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Object Zenject.AddToCurrentGameObjectComponentProvider/<GetAllInstancesWithInjectSplit>d__12::<instance>5__2
	RuntimeObject * ___U3CinstanceU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8, ___U3CU3E4__this_3)); }
	inline AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8, ___U3CinstanceU3E5__2_5)); }
	inline RuntimeObject * get_U3CinstanceU3E5__2_5() const { return ___U3CinstanceU3E5__2_5; }
	inline RuntimeObject ** get_address_of_U3CinstanceU3E5__2_5() { return &___U3CinstanceU3E5__2_5; }
	inline void set_U3CinstanceU3E5__2_5(RuntimeObject * value)
	{
		___U3CinstanceU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__12_T945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8_H
#ifndef ADDTOGAMEOBJECTCOMPONENTPROVIDERBASE_T20AF22D871709F7F5E792D74A854C96D9720009A_H
#define ADDTOGAMEOBJECTCOMPONENTPROVIDERBASE_T20AF22D871709F7F5E792D74A854C96D9720009A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToGameObjectComponentProviderBase
struct  AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A  : public RuntimeObject
{
public:
	// System.Object Zenject.AddToGameObjectComponentProviderBase::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_0;
	// System.Type Zenject.AddToGameObjectComponentProviderBase::_componentType
	Type_t * ____componentType_1;
	// Zenject.DiContainer Zenject.AddToGameObjectComponentProviderBase::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToGameObjectComponentProviderBase::_extraArguments
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArguments_3;

public:
	inline static int32_t get_offset_of__concreteIdentifier_0() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A, ____concreteIdentifier_0)); }
	inline RuntimeObject * get__concreteIdentifier_0() const { return ____concreteIdentifier_0; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_0() { return &____concreteIdentifier_0; }
	inline void set__concreteIdentifier_0(RuntimeObject * value)
	{
		____concreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A, ____extraArguments_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOGAMEOBJECTCOMPONENTPROVIDERBASE_T20AF22D871709F7F5E792D74A854C96D9720009A_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__14_T9E97BD0480963043973B6AED4099227B16043523_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__14_T9E97BD0480963043973B6AED4099227B16043523_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14
struct  U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523  : public RuntimeObject
{
public:
	// System.Int32 Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.AddToGameObjectComponentProviderBase Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::<>4__this
	AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Object Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::<instance>5__2
	RuntimeObject * ___U3CinstanceU3E5__2_5;
	// UnityEngine.GameObject Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::<gameObj>5__3
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CgameObjU3E5__3_6;
	// System.Boolean Zenject.AddToGameObjectComponentProviderBase/<GetAllInstancesWithInjectSplit>d__14::<wasActive>5__4
	bool ___U3CwasActiveU3E5__4_7;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___U3CU3E4__this_3)); }
	inline AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CinstanceU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___U3CinstanceU3E5__2_5)); }
	inline RuntimeObject * get_U3CinstanceU3E5__2_5() const { return ___U3CinstanceU3E5__2_5; }
	inline RuntimeObject ** get_address_of_U3CinstanceU3E5__2_5() { return &___U3CinstanceU3E5__2_5; }
	inline void set_U3CinstanceU3E5__2_5(RuntimeObject * value)
	{
		___U3CinstanceU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CinstanceU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3CgameObjU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___U3CgameObjU3E5__3_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CgameObjU3E5__3_6() const { return ___U3CgameObjU3E5__3_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CgameObjU3E5__3_6() { return &___U3CgameObjU3E5__3_6; }
	inline void set_U3CgameObjU3E5__3_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CgameObjU3E5__3_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjU3E5__3_6), value);
	}

	inline static int32_t get_offset_of_U3CwasActiveU3E5__4_7() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523, ___U3CwasActiveU3E5__4_7)); }
	inline bool get_U3CwasActiveU3E5__4_7() const { return ___U3CwasActiveU3E5__4_7; }
	inline bool* get_address_of_U3CwasActiveU3E5__4_7() { return &___U3CwasActiveU3E5__4_7; }
	inline void set_U3CwasActiveU3E5__4_7(bool value)
	{
		___U3CwasActiveU3E5__4_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__14_T9E97BD0480963043973B6AED4099227B16043523_H
#ifndef CACHEDPROVIDER_T448EC7D52925C5C0695970DE1C374655544F04B8_H
#define CACHEDPROVIDER_T448EC7D52925C5C0695970DE1C374655544F04B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CachedProvider
struct  CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8  : public RuntimeObject
{
public:
	// Zenject.IProvider Zenject.CachedProvider::_creator
	RuntimeObject* ____creator_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.CachedProvider::_instances
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ____instances_1;
	// System.Boolean Zenject.CachedProvider::_isCreatingInstance
	bool ____isCreatingInstance_2;

public:
	inline static int32_t get_offset_of__creator_0() { return static_cast<int32_t>(offsetof(CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8, ____creator_0)); }
	inline RuntimeObject* get__creator_0() const { return ____creator_0; }
	inline RuntimeObject** get_address_of__creator_0() { return &____creator_0; }
	inline void set__creator_0(RuntimeObject* value)
	{
		____creator_0 = value;
		Il2CppCodeGenWriteBarrier((&____creator_0), value);
	}

	inline static int32_t get_offset_of__instances_1() { return static_cast<int32_t>(offsetof(CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8, ____instances_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get__instances_1() const { return ____instances_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of__instances_1() { return &____instances_1; }
	inline void set__instances_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		____instances_1 = value;
		Il2CppCodeGenWriteBarrier((&____instances_1), value);
	}

	inline static int32_t get_offset_of__isCreatingInstance_2() { return static_cast<int32_t>(offsetof(CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8, ____isCreatingInstance_2)); }
	inline bool get__isCreatingInstance_2() const { return ____isCreatingInstance_2; }
	inline bool* get_address_of__isCreatingInstance_2() { return &____isCreatingInstance_2; }
	inline void set__isCreatingInstance_2(bool value)
	{
		____isCreatingInstance_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // CACHEDPROVIDER_T448EC7D52925C5C0695970DE1C374655544F04B8_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T02D141F33C883E69F510E13F4100A6B4BA6B615F_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T02D141F33C883E69F510E13F4100A6B4BA6B615F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5
struct  U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F  : public RuntimeObject
{
public:
	// System.Int32 Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.CachedProvider Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::<>4__this
	CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.List`1<System.Object>> Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::<runner>5__2
	RuntimeObject* ___U3CrunnerU3E5__2_5;
	// System.Boolean Zenject.CachedProvider/<GetAllInstancesWithInjectSplit>d__5::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___U3CU3E4__this_3)); }
	inline CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CrunnerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___U3CrunnerU3E5__2_5)); }
	inline RuntimeObject* get_U3CrunnerU3E5__2_5() const { return ___U3CrunnerU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CrunnerU3E5__2_5() { return &___U3CrunnerU3E5__2_5; }
	inline void set_U3CrunnerU3E5__2_5(RuntimeObject* value)
	{
		___U3CrunnerU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrunnerU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F, ___U3ChasMoreU3E5__3_6)); }
	inline bool get_U3ChasMoreU3E5__3_6() const { return ___U3ChasMoreU3E5__3_6; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_6() { return &___U3ChasMoreU3E5__3_6; }
	inline void set_U3ChasMoreU3E5__3_6(bool value)
	{
		___U3ChasMoreU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T02D141F33C883E69F510E13F4100A6B4BA6B615F_H
#ifndef U3CU3EC__DISPLAYCLASS154_0_TF6F41007256F28E1A543819B7035F43D52A419F7_H
#define U3CU3EC__DISPLAYCLASS154_0_TF6F41007256F28E1A543819B7035F43D52A419F7_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass154_0
struct  U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.DiContainer/<>c__DisplayClass154_0::<>4__this
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CU3E4__this_0;
	// System.Type Zenject.DiContainer/<>c__DisplayClass154_0::contractType
	Type_t * ___contractType_1;
	// System.Object Zenject.DiContainer/<>c__DisplayClass154_0::identifier
	RuntimeObject * ___identifier_2;
	// System.Type Zenject.DiContainer/<>c__DisplayClass154_0::concreteType
	Type_t * ___concreteType_3;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7, ___U3CU3E4__this_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_contractType_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7, ___contractType_1)); }
	inline Type_t * get_contractType_1() const { return ___contractType_1; }
	inline Type_t ** get_address_of_contractType_1() { return &___contractType_1; }
	inline void set_contractType_1(Type_t * value)
	{
		___contractType_1 = value;
		Il2CppCodeGenWriteBarrier((&___contractType_1), value);
	}

	inline static int32_t get_offset_of_identifier_2() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7, ___identifier_2)); }
	inline RuntimeObject * get_identifier_2() const { return ___identifier_2; }
	inline RuntimeObject ** get_address_of_identifier_2() { return &___identifier_2; }
	inline void set_identifier_2(RuntimeObject * value)
	{
		___identifier_2 = value;
		Il2CppCodeGenWriteBarrier((&___identifier_2), value);
	}

	inline static int32_t get_offset_of_concreteType_3() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7, ___concreteType_3)); }
	inline Type_t * get_concreteType_3() const { return ___concreteType_3; }
	inline Type_t ** get_address_of_concreteType_3() { return &___concreteType_3; }
	inline void set_concreteType_3(Type_t * value)
	{
		___concreteType_3 = value;
		Il2CppCodeGenWriteBarrier((&___concreteType_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS154_0_TF6F41007256F28E1A543819B7035F43D52A419F7_H
#ifndef U3CU3EC__DISPLAYCLASS61_0_T5777778EDB97A22666FA08BFF6E4A725E750551B_H
#define U3CU3EC__DISPLAYCLASS61_0_T5777778EDB97A22666FA08BFF6E4A725E750551B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass61_0
struct  U3CU3Ec__DisplayClass61_0_t5777778EDB97A22666FA08BFF6E4A725E750551B  : public RuntimeObject
{
public:
	// Zenject.BindingId Zenject.DiContainer/<>c__DisplayClass61_0::bindingId
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___bindingId_0;

public:
	inline static int32_t get_offset_of_bindingId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass61_0_t5777778EDB97A22666FA08BFF6E4A725E750551B, ___bindingId_0)); }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * get_bindingId_0() const { return ___bindingId_0; }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 ** get_address_of_bindingId_0() { return &___bindingId_0; }
	inline void set_bindingId_0(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * value)
	{
		___bindingId_0 = value;
		Il2CppCodeGenWriteBarrier((&___bindingId_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS61_0_T5777778EDB97A22666FA08BFF6E4A725E750551B_H
#ifndef U3CU3EC__DISPLAYCLASS65_0_T249E7667EDEAC5F1B3A6675A5D854AB072E29D95_H
#define U3CU3EC__DISPLAYCLASS65_0_T249E7667EDEAC5F1B3A6675A5D854AB072E29D95_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass65_0
struct  U3CU3Ec__DisplayClass65_0_t249E7667EDEAC5F1B3A6675A5D854AB072E29D95  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.DiContainer/<>c__DisplayClass65_0::<>4__this
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CU3E4__this_0;
	// Zenject.InjectContext Zenject.DiContainer/<>c__DisplayClass65_0::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_1;

public:
	inline static int32_t get_offset_of_U3CU3E4__this_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t249E7667EDEAC5F1B3A6675A5D854AB072E29D95, ___U3CU3E4__this_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CU3E4__this_0() const { return ___U3CU3E4__this_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CU3E4__this_0() { return &___U3CU3E4__this_0; }
	inline void set_U3CU3E4__this_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CU3E4__this_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_0), value);
	}

	inline static int32_t get_offset_of_context_1() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass65_0_t249E7667EDEAC5F1B3A6675A5D854AB072E29D95, ___context_1)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_1() const { return ___context_1; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_1() { return &___context_1; }
	inline void set_context_1(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_1 = value;
		Il2CppCodeGenWriteBarrier((&___context_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS65_0_T249E7667EDEAC5F1B3A6675A5D854AB072E29D95_H
#ifndef U3CU3EC__DISPLAYCLASS72_0_T24F526DBAB79EBE252A560580A759DD979F83BB8_H
#define U3CU3EC__DISPLAYCLASS72_0_T24F526DBAB79EBE252A560580A759DD979F83BB8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass72_0
struct  U3CU3Ec__DisplayClass72_0_t24F526DBAB79EBE252A560580A759DD979F83BB8  : public RuntimeObject
{
public:
	// Zenject.InjectContext Zenject.DiContainer/<>c__DisplayClass72_0::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_0;

public:
	inline static int32_t get_offset_of_context_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass72_0_t24F526DBAB79EBE252A560580A759DD979F83BB8, ___context_0)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_0() const { return ___context_0; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_0() { return &___context_0; }
	inline void set_context_0(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_0 = value;
		Il2CppCodeGenWriteBarrier((&___context_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS72_0_T24F526DBAB79EBE252A560580A759DD979F83BB8_H
#ifndef U3CU3EC__DISPLAYCLASS73_0_T4DAD416F4B2C837BD087C6963C5F4791D2237DCD_H
#define U3CU3EC__DISPLAYCLASS73_0_T4DAD416F4B2C837BD087C6963C5F4791D2237DCD_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass73_0
struct  U3CU3Ec__DisplayClass73_0_t4DAD416F4B2C837BD087C6963C5F4791D2237DCD  : public RuntimeObject
{
public:
	// System.Collections.Generic.List`1<<>f__AnonymousType0`2<Zenject.DiContainer/ProviderPair,System.Int32>> Zenject.DiContainer/<>c__DisplayClass73_0::sortedProviders
	List_1_t9AC73A41C48344C2C96A2DBD179618B08EB43137 * ___sortedProviders_0;

public:
	inline static int32_t get_offset_of_sortedProviders_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass73_0_t4DAD416F4B2C837BD087C6963C5F4791D2237DCD, ___sortedProviders_0)); }
	inline List_1_t9AC73A41C48344C2C96A2DBD179618B08EB43137 * get_sortedProviders_0() const { return ___sortedProviders_0; }
	inline List_1_t9AC73A41C48344C2C96A2DBD179618B08EB43137 ** get_address_of_sortedProviders_0() { return &___sortedProviders_0; }
	inline void set_sortedProviders_0(List_1_t9AC73A41C48344C2C96A2DBD179618B08EB43137 * value)
	{
		___sortedProviders_0 = value;
		Il2CppCodeGenWriteBarrier((&___sortedProviders_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS73_0_T4DAD416F4B2C837BD087C6963C5F4791D2237DCD_H
#ifndef U3CGETDEPENDENCYCONTRACTSU3ED__79_T39F5B8F4D203C6E11E48FD2C4448D57D162CB519_H
#define U3CGETDEPENDENCYCONTRACTSU3ED__79_T39F5B8F4D203C6E11E48FD2C4448D57D162CB519_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<GetDependencyContracts>d__79
struct  U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519  : public RuntimeObject
{
public:
	// System.Int32 Zenject.DiContainer/<GetDependencyContracts>d__79::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Type Zenject.DiContainer/<GetDependencyContracts>d__79::<>2__current
	Type_t * ___U3CU3E2__current_1;
	// System.Int32 Zenject.DiContainer/<GetDependencyContracts>d__79::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.DiContainer Zenject.DiContainer/<GetDependencyContracts>d__79::<>4__this
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CU3E4__this_3;
	// System.Type Zenject.DiContainer/<GetDependencyContracts>d__79::contract
	Type_t * ___contract_4;
	// System.Type Zenject.DiContainer/<GetDependencyContracts>d__79::<>3__contract
	Type_t * ___U3CU3E3__contract_5;
	// System.Collections.Generic.IEnumerator`1<Zenject.InjectableInfo> Zenject.DiContainer/<GetDependencyContracts>d__79::<>7__wrap1
	RuntimeObject* ___U3CU3E7__wrap1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___U3CU3E2__current_1)); }
	inline Type_t * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline Type_t ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(Type_t * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___U3CU3E4__this_3)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_contract_4() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___contract_4)); }
	inline Type_t * get_contract_4() const { return ___contract_4; }
	inline Type_t ** get_address_of_contract_4() { return &___contract_4; }
	inline void set_contract_4(Type_t * value)
	{
		___contract_4 = value;
		Il2CppCodeGenWriteBarrier((&___contract_4), value);
	}

	inline static int32_t get_offset_of_U3CU3E3__contract_5() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___U3CU3E3__contract_5)); }
	inline Type_t * get_U3CU3E3__contract_5() const { return ___U3CU3E3__contract_5; }
	inline Type_t ** get_address_of_U3CU3E3__contract_5() { return &___U3CU3E3__contract_5; }
	inline void set_U3CU3E3__contract_5(Type_t * value)
	{
		___U3CU3E3__contract_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E3__contract_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519, ___U3CU3E7__wrap1_6)); }
	inline RuntimeObject* get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline RuntimeObject** get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(RuntimeObject* value)
	{
		___U3CU3E7__wrap1_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E7__wrap1_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETDEPENDENCYCONTRACTSU3ED__79_T39F5B8F4D203C6E11E48FD2C4448D57D162CB519_H
#ifndef EMPTYGAMEOBJECTPROVIDER_T917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8_H
#define EMPTYGAMEOBJECTPROVIDER_T917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.EmptyGameObjectProvider
struct  EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.EmptyGameObjectProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// Zenject.GameObjectCreationParameters Zenject.EmptyGameObjectProvider::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_1() { return static_cast<int32_t>(offsetof(EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8, ____gameObjectBindInfo_1)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_1() const { return ____gameObjectBindInfo_1; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_1() { return &____gameObjectBindInfo_1; }
	inline void set__gameObjectBindInfo_1(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // EMPTYGAMEOBJECTPROVIDER_T917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T0D29CFB6F37C3485662E0CD983E98A800E9D220B_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T0D29CFB6F37C3485662E0CD983E98A800E9D220B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.EmptyGameObjectProvider/<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B  : public RuntimeObject
{
public:
	// System.Int32 Zenject.EmptyGameObjectProvider/<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.EmptyGameObjectProvider/<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.EmptyGameObjectProvider/<GetAllInstancesWithInjectSplit>d__4::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_2;
	// Zenject.EmptyGameObjectProvider Zenject.EmptyGameObjectProvider/<GetAllInstancesWithInjectSplit>d__4::<>4__this
	EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8 * ___U3CU3E4__this_3;
	// Zenject.InjectContext Zenject.EmptyGameObjectProvider/<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B, ___args_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_2() const { return ___args_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B, ___U3CU3E4__this_3)); }
	inline EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_context_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B, ___context_4)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_4() const { return ___context_4; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_4() { return &___context_4; }
	inline void set_context_4(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_4 = value;
		Il2CppCodeGenWriteBarrier((&___context_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T0D29CFB6F37C3485662E0CD983E98A800E9D220B_H
#ifndef GETFROMPREFABCOMPONENTPROVIDER_TFEAFD1B8AEC1FDB480001CFE269F6D3342E97003_H
#define GETFROMPREFABCOMPONENTPROVIDER_TFEAFD1B8AEC1FDB480001CFE269F6D3342E97003_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GetFromPrefabComponentProvider
struct  GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.GetFromPrefabComponentProvider::_prefabInstantiator
	RuntimeObject* ____prefabInstantiator_0;
	// System.Type Zenject.GetFromPrefabComponentProvider::_componentType
	Type_t * ____componentType_1;

public:
	inline static int32_t get_offset_of__prefabInstantiator_0() { return static_cast<int32_t>(offsetof(GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003, ____prefabInstantiator_0)); }
	inline RuntimeObject* get__prefabInstantiator_0() const { return ____prefabInstantiator_0; }
	inline RuntimeObject** get_address_of__prefabInstantiator_0() { return &____prefabInstantiator_0; }
	inline void set__prefabInstantiator_0(RuntimeObject* value)
	{
		____prefabInstantiator_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabInstantiator_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // GETFROMPREFABCOMPONENTPROVIDER_TFEAFD1B8AEC1FDB480001CFE269F6D3342E97003_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T1CBBC63B1774B037E25DC1E42802784072BE3634_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T1CBBC63B1774B037E25DC1E42802784072BE3634_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634  : public RuntimeObject
{
public:
	// System.Int32 Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.GetFromPrefabComponentProvider Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<>4__this
	GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<gameObjectRunner>5__2
	RuntimeObject* ___U3CgameObjectRunnerU3E5__2_5;
	// System.Boolean Zenject.GetFromPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___U3CU3E4__this_3)); }
	inline GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CgameObjectRunnerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___U3CgameObjectRunnerU3E5__2_5)); }
	inline RuntimeObject* get_U3CgameObjectRunnerU3E5__2_5() const { return ___U3CgameObjectRunnerU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CgameObjectRunnerU3E5__2_5() { return &___U3CgameObjectRunnerU3E5__2_5; }
	inline void set_U3CgameObjectRunnerU3E5__2_5(RuntimeObject* value)
	{
		___U3CgameObjectRunnerU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectRunnerU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634, ___U3ChasMoreU3E5__3_6)); }
	inline bool get_U3ChasMoreU3E5__3_6() const { return ___U3ChasMoreU3E5__3_6; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_6() { return &___U3ChasMoreU3E5__3_6; }
	inline void set_U3ChasMoreU3E5__3_6(bool value)
	{
		___U3ChasMoreU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T1CBBC63B1774B037E25DC1E42802784072BE3634_H
#ifndef IPROVIDEREXTENSIONS_TD0DDAA15BB230B739EFD01ADD4F9656543786949_H
#define IPROVIDEREXTENSIONS_TD0DDAA15BB230B739EFD01ADD4F9656543786949_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.IProviderExtensions
struct  IProviderExtensions_tD0DDAA15BB230B739EFD01ADD4F9656543786949  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // IPROVIDEREXTENSIONS_TD0DDAA15BB230B739EFD01ADD4F9656543786949_H
#ifndef INSTANCEPROVIDER_T0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2_H
#define INSTANCEPROVIDER_T0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstanceProvider
struct  InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2  : public RuntimeObject
{
public:
	// System.Object Zenject.InstanceProvider::_instance
	RuntimeObject * ____instance_0;
	// System.Type Zenject.InstanceProvider::_instanceType
	Type_t * ____instanceType_1;
	// Zenject.DiContainer Zenject.InstanceProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_2;

public:
	inline static int32_t get_offset_of__instance_0() { return static_cast<int32_t>(offsetof(InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2, ____instance_0)); }
	inline RuntimeObject * get__instance_0() const { return ____instance_0; }
	inline RuntimeObject ** get_address_of__instance_0() { return &____instance_0; }
	inline void set__instance_0(RuntimeObject * value)
	{
		____instance_0 = value;
		Il2CppCodeGenWriteBarrier((&____instance_0), value);
	}

	inline static int32_t get_offset_of__instanceType_1() { return static_cast<int32_t>(offsetof(InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2, ____instanceType_1)); }
	inline Type_t * get__instanceType_1() const { return ____instanceType_1; }
	inline Type_t ** get_address_of__instanceType_1() { return &____instanceType_1; }
	inline void set__instanceType_1(Type_t * value)
	{
		____instanceType_1 = value;
		Il2CppCodeGenWriteBarrier((&____instanceType_1), value);
	}

	inline static int32_t get_offset_of__container_2() { return static_cast<int32_t>(offsetof(InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2, ____container_2)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_2() const { return ____container_2; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_2() { return &____container_2; }
	inline void set__container_2(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_2 = value;
		Il2CppCodeGenWriteBarrier((&____container_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANCEPROVIDER_T0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstanceProvider/<GetAllInstancesWithInjectSplit>d__5
struct  U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InstanceProvider/<GetAllInstancesWithInjectSplit>d__5::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.InstanceProvider/<GetAllInstancesWithInjectSplit>d__5::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InstanceProvider/<GetAllInstancesWithInjectSplit>d__5::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_2;
	// Zenject.InjectContext Zenject.InstanceProvider/<GetAllInstancesWithInjectSplit>d__5::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_3;
	// Zenject.InstanceProvider Zenject.InstanceProvider/<GetAllInstancesWithInjectSplit>d__5::<>4__this
	InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B, ___args_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_2() const { return ___args_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B, ___context_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_3() const { return ___context_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B, ___U3CU3E4__this_4)); }
	inline InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__5_T3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B_H
#ifndef INSTANTIATEONPREFABCOMPONENTPROVIDER_TFF5B6900958B685C76A7A38A3B9944F557562192_H
#define INSTANTIATEONPREFABCOMPONENTPROVIDER_TFF5B6900958B685C76A7A38A3B9944F557562192_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstantiateOnPrefabComponentProvider
struct  InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.InstantiateOnPrefabComponentProvider::_prefabInstantiator
	RuntimeObject* ____prefabInstantiator_0;
	// System.Type Zenject.InstantiateOnPrefabComponentProvider::_componentType
	Type_t * ____componentType_1;

public:
	inline static int32_t get_offset_of__prefabInstantiator_0() { return static_cast<int32_t>(offsetof(InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192, ____prefabInstantiator_0)); }
	inline RuntimeObject* get__prefabInstantiator_0() const { return ____prefabInstantiator_0; }
	inline RuntimeObject** get_address_of__prefabInstantiator_0() { return &____prefabInstantiator_0; }
	inline void set__prefabInstantiator_0(RuntimeObject* value)
	{
		____prefabInstantiator_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabInstantiator_0), value);
	}

	inline static int32_t get_offset_of__componentType_1() { return static_cast<int32_t>(offsetof(InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192, ____componentType_1)); }
	inline Type_t * get__componentType_1() const { return ____componentType_1; }
	inline Type_t ** get_address_of__componentType_1() { return &____componentType_1; }
	inline void set__componentType_1(Type_t * value)
	{
		____componentType_1 = value;
		Il2CppCodeGenWriteBarrier((&____componentType_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INSTANTIATEONPREFABCOMPONENTPROVIDER_TFF5B6900958B685C76A7A38A3B9944F557562192_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1  : public RuntimeObject
{
public:
	// System.Int32 Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.InstantiateOnPrefabComponentProvider Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<>4__this
	InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<gameObjectRunner>5__2
	RuntimeObject* ___U3CgameObjectRunnerU3E5__2_5;
	// System.Boolean Zenject.InstantiateOnPrefabComponentProvider/<GetAllInstancesWithInjectSplit>d__4::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___U3CU3E4__this_3)); }
	inline InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CgameObjectRunnerU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___U3CgameObjectRunnerU3E5__2_5)); }
	inline RuntimeObject* get_U3CgameObjectRunnerU3E5__2_5() const { return ___U3CgameObjectRunnerU3E5__2_5; }
	inline RuntimeObject** get_address_of_U3CgameObjectRunnerU3E5__2_5() { return &___U3CgameObjectRunnerU3E5__2_5; }
	inline void set_U3CgameObjectRunnerU3E5__2_5(RuntimeObject* value)
	{
		___U3CgameObjectRunnerU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectRunnerU3E5__2_5), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_6() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1, ___U3ChasMoreU3E5__3_6)); }
	inline bool get_U3ChasMoreU3E5__3_6() const { return ___U3ChasMoreU3E5__3_6; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_6() { return &___U3ChasMoreU3E5__3_6; }
	inline void set_U3ChasMoreU3E5__3_6(bool value)
	{
		___U3ChasMoreU3E5__3_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1_H
#ifndef LAZYINSTANCEINJECTOR_TC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E_H
#define LAZYINSTANCEINJECTOR_TC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.LazyInstanceInjector
struct  LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.LazyInstanceInjector::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// System.Collections.Generic.HashSet`1<System.Object> Zenject.LazyInstanceInjector::_instancesToInject
	HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * ____instancesToInject_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__instancesToInject_1() { return static_cast<int32_t>(offsetof(LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E, ____instancesToInject_1)); }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * get__instancesToInject_1() const { return ____instancesToInject_1; }
	inline HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 ** get_address_of__instancesToInject_1() { return &____instancesToInject_1; }
	inline void set__instancesToInject_1(HashSet_1_t297CD7F944846107B388993164FCD9E317A338A3 * value)
	{
		____instancesToInject_1 = value;
		Il2CppCodeGenWriteBarrier((&____instancesToInject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // LAZYINSTANCEINJECTOR_TC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E_H
#ifndef METHODPROVIDERUNTYPED_T799F0802EE96546AA0D409C31C77C1DF09FB75DA_H
#define METHODPROVIDERUNTYPED_T799F0802EE96546AA0D409C31C77C1DF09FB75DA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MethodProviderUntyped
struct  MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.MethodProviderUntyped::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// System.Func`2<Zenject.InjectContext,System.Object> Zenject.MethodProviderUntyped::_method
	Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 * ____method_1;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__method_1() { return static_cast<int32_t>(offsetof(MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA, ____method_1)); }
	inline Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 * get__method_1() const { return ____method_1; }
	inline Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 ** get_address_of__method_1() { return &____method_1; }
	inline void set__method_1(Func_2_t7842910C86C796ABE3E05D8251451CC72B0ED191 * value)
	{
		____method_1 = value;
		Il2CppCodeGenWriteBarrier((&____method_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // METHODPROVIDERUNTYPED_T799F0802EE96546AA0D409C31C77C1DF09FB75DA_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.MethodProviderUntyped/<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA  : public RuntimeObject
{
public:
	// System.Int32 Zenject.MethodProviderUntyped/<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.MethodProviderUntyped/<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.MethodProviderUntyped/<GetAllInstancesWithInjectSplit>d__4::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_2;
	// Zenject.InjectContext Zenject.MethodProviderUntyped/<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_3;
	// Zenject.MethodProviderUntyped Zenject.MethodProviderUntyped/<GetAllInstancesWithInjectSplit>d__4::<>4__this
	MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA, ___args_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_2() const { return ___args_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA, ___context_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_3() const { return ___context_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA, ___U3CU3E4__this_4)); }
	inline MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_TA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA_H
#ifndef PREFABGAMEOBJECTPROVIDER_TE637F7A2AD13B6B29F174376A6C903B6B4513B22_H
#define PREFABGAMEOBJECTPROVIDER_TE637F7A2AD13B6B29F174376A6C903B6B4513B22_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabGameObjectProvider
struct  PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.PrefabGameObjectProvider::_prefabCreator
	RuntimeObject* ____prefabCreator_0;

public:
	inline static int32_t get_offset_of__prefabCreator_0() { return static_cast<int32_t>(offsetof(PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22, ____prefabCreator_0)); }
	inline RuntimeObject* get__prefabCreator_0() const { return ____prefabCreator_0; }
	inline RuntimeObject** get_address_of__prefabCreator_0() { return &____prefabCreator_0; }
	inline void set__prefabCreator_0(RuntimeObject* value)
	{
		____prefabCreator_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabCreator_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABGAMEOBJECTPROVIDER_TE637F7A2AD13B6B29F174376A6C903B6B4513B22_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__3_T76AC360ABF05C51EDAAAC4348955E70E57B2D811_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__3_T76AC360ABF05C51EDAAAC4348955E70E57B2D811_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3
struct  U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811  : public RuntimeObject
{
public:
	// System.Int32 Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.PrefabGameObjectProvider Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3::<>4__this
	PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22 * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_3;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3::<runner>5__2
	RuntimeObject* ___U3CrunnerU3E5__2_4;
	// System.Boolean Zenject.PrefabGameObjectProvider/<GetAllInstancesWithInjectSplit>d__3::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811, ___U3CU3E4__this_2)); }
	inline PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22 * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22 ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22 * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_args_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811, ___args_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_3() const { return ___args_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_3() { return &___args_3; }
	inline void set_args_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_3 = value;
		Il2CppCodeGenWriteBarrier((&___args_3), value);
	}

	inline static int32_t get_offset_of_U3CrunnerU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811, ___U3CrunnerU3E5__2_4)); }
	inline RuntimeObject* get_U3CrunnerU3E5__2_4() const { return ___U3CrunnerU3E5__2_4; }
	inline RuntimeObject** get_address_of_U3CrunnerU3E5__2_4() { return &___U3CrunnerU3E5__2_4; }
	inline void set_U3CrunnerU3E5__2_4(RuntimeObject* value)
	{
		___U3CrunnerU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrunnerU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811, ___U3ChasMoreU3E5__3_5)); }
	inline bool get_U3ChasMoreU3E5__3_5() const { return ___U3ChasMoreU3E5__3_5; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_5() { return &___U3ChasMoreU3E5__3_5; }
	inline void set_U3ChasMoreU3E5__3_5(bool value)
	{
		___U3ChasMoreU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__3_T76AC360ABF05C51EDAAAC4348955E70E57B2D811_H
#ifndef PREFABINSTANTIATOR_T6036DEC964BFDA4E4A21D7EBE803918AB60683DE_H
#define PREFABINSTANTIATOR_T6036DEC964BFDA4E4A21D7EBE803918AB60683DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiator
struct  PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE  : public RuntimeObject
{
public:
	// Zenject.IPrefabProvider Zenject.PrefabInstantiator::_prefabProvider
	RuntimeObject* ____prefabProvider_0;
	// Zenject.DiContainer Zenject.PrefabInstantiator::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabInstantiator::_extraArguments
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArguments_2;
	// Zenject.GameObjectCreationParameters Zenject.PrefabInstantiator::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_3;
	// System.Type Zenject.PrefabInstantiator::_argumentTarget
	Type_t * ____argumentTarget_4;

public:
	inline static int32_t get_offset_of__prefabProvider_0() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE, ____prefabProvider_0)); }
	inline RuntimeObject* get__prefabProvider_0() const { return ____prefabProvider_0; }
	inline RuntimeObject** get_address_of__prefabProvider_0() { return &____prefabProvider_0; }
	inline void set__prefabProvider_0(RuntimeObject* value)
	{
		____prefabProvider_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefabProvider_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__extraArguments_2() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE, ____extraArguments_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArguments_2() const { return ____extraArguments_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArguments_2() { return &____extraArguments_2; }
	inline void set__extraArguments_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArguments_2 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_2), value);
	}

	inline static int32_t get_offset_of__gameObjectBindInfo_3() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE, ____gameObjectBindInfo_3)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_3() const { return ____gameObjectBindInfo_3; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_3() { return &____gameObjectBindInfo_3; }
	inline void set__gameObjectBindInfo_3(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_3 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_3), value);
	}

	inline static int32_t get_offset_of__argumentTarget_4() { return static_cast<int32_t>(offsetof(PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE, ____argumentTarget_4)); }
	inline Type_t * get__argumentTarget_4() const { return ____argumentTarget_4; }
	inline Type_t ** get_address_of__argumentTarget_4() { return &____argumentTarget_4; }
	inline void set__argumentTarget_4(Type_t * value)
	{
		____argumentTarget_4 = value;
		Il2CppCodeGenWriteBarrier((&____argumentTarget_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABINSTANTIATOR_T6036DEC964BFDA4E4A21D7EBE803918AB60683DE_H
#ifndef U3CINSTANTIATEU3ED__13_TAEAB79BA7A3BB919F54AB460C45EECAA6585800F_H
#define U3CINSTANTIATEU3ED__13_TAEAB79BA7A3BB919F54AB460C45EECAA6585800F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiator/<Instantiate>d__13
struct  U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F  : public RuntimeObject
{
public:
	// System.Int32 Zenject.PrefabInstantiator/<Instantiate>d__13::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject Zenject.PrefabInstantiator/<Instantiate>d__13::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// Zenject.PrefabInstantiator Zenject.PrefabInstantiator/<Instantiate>d__13::<>4__this
	PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE * ___U3CU3E4__this_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabInstantiator/<Instantiate>d__13::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_3;
	// Zenject.InjectContext Zenject.PrefabInstantiator/<Instantiate>d__13::<context>5__2
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___U3CcontextU3E5__2_4;
	// System.Boolean Zenject.PrefabInstantiator/<Instantiate>d__13::<shouldMakeActive>5__3
	bool ___U3CshouldMakeActiveU3E5__3_5;
	// UnityEngine.GameObject Zenject.PrefabInstantiator/<Instantiate>d__13::<gameObject>5__4
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CgameObjectU3E5__4_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___U3CU3E4__this_2)); }
	inline PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_2), value);
	}

	inline static int32_t get_offset_of_args_3() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___args_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_3() const { return ___args_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_3() { return &___args_3; }
	inline void set_args_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_3 = value;
		Il2CppCodeGenWriteBarrier((&___args_3), value);
	}

	inline static int32_t get_offset_of_U3CcontextU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___U3CcontextU3E5__2_4)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_U3CcontextU3E5__2_4() const { return ___U3CcontextU3E5__2_4; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_U3CcontextU3E5__2_4() { return &___U3CcontextU3E5__2_4; }
	inline void set_U3CcontextU3E5__2_4(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___U3CcontextU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CcontextU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3CshouldMakeActiveU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___U3CshouldMakeActiveU3E5__3_5)); }
	inline bool get_U3CshouldMakeActiveU3E5__3_5() const { return ___U3CshouldMakeActiveU3E5__3_5; }
	inline bool* get_address_of_U3CshouldMakeActiveU3E5__3_5() { return &___U3CshouldMakeActiveU3E5__3_5; }
	inline void set_U3CshouldMakeActiveU3E5__3_5(bool value)
	{
		___U3CshouldMakeActiveU3E5__3_5 = value;
	}

	inline static int32_t get_offset_of_U3CgameObjectU3E5__4_6() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F, ___U3CgameObjectU3E5__4_6)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CgameObjectU3E5__4_6() const { return ___U3CgameObjectU3E5__4_6; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CgameObjectU3E5__4_6() { return &___U3CgameObjectU3E5__4_6; }
	inline void set_U3CgameObjectU3E5__4_6(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CgameObjectU3E5__4_6 = value;
		Il2CppCodeGenWriteBarrier((&___U3CgameObjectU3E5__4_6), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEU3ED__13_TAEAB79BA7A3BB919F54AB460C45EECAA6585800F_H
#ifndef PREFABINSTANTIATORCACHED_TE36AD8F58DF7D3C659AEC758F1C4AF783BE16943_H
#define PREFABINSTANTIATORCACHED_TE36AD8F58DF7D3C659AEC758F1C4AF783BE16943_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiatorCached
struct  PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943  : public RuntimeObject
{
public:
	// Zenject.IPrefabInstantiator Zenject.PrefabInstantiatorCached::_subInstantiator
	RuntimeObject* ____subInstantiator_0;
	// UnityEngine.GameObject Zenject.PrefabInstantiatorCached::_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____gameObject_1;

public:
	inline static int32_t get_offset_of__subInstantiator_0() { return static_cast<int32_t>(offsetof(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943, ____subInstantiator_0)); }
	inline RuntimeObject* get__subInstantiator_0() const { return ____subInstantiator_0; }
	inline RuntimeObject** get_address_of__subInstantiator_0() { return &____subInstantiator_0; }
	inline void set__subInstantiator_0(RuntimeObject* value)
	{
		____subInstantiator_0 = value;
		Il2CppCodeGenWriteBarrier((&____subInstantiator_0), value);
	}

	inline static int32_t get_offset_of__gameObject_1() { return static_cast<int32_t>(offsetof(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943, ____gameObject_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__gameObject_1() const { return ____gameObject_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__gameObject_1() { return &____gameObject_1; }
	inline void set__gameObject_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____gameObject_1 = value;
		Il2CppCodeGenWriteBarrier((&____gameObject_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABINSTANTIATORCACHED_TE36AD8F58DF7D3C659AEC758F1C4AF783BE16943_H
#ifndef U3CINSTANTIATEU3ED__10_T0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6_H
#define U3CINSTANTIATEU3ED__10_T0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabInstantiatorCached/<Instantiate>d__10
struct  U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6  : public RuntimeObject
{
public:
	// System.Int32 Zenject.PrefabInstantiatorCached/<Instantiate>d__10::<>1__state
	int32_t ___U3CU3E1__state_0;
	// UnityEngine.GameObject Zenject.PrefabInstantiatorCached/<Instantiate>d__10::<>2__current
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.PrefabInstantiatorCached/<Instantiate>d__10::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_2;
	// Zenject.PrefabInstantiatorCached Zenject.PrefabInstantiatorCached/<Instantiate>d__10::<>4__this
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * ___U3CU3E4__this_3;
	// System.Collections.Generic.IEnumerator`1<UnityEngine.GameObject> Zenject.PrefabInstantiatorCached/<Instantiate>d__10::<runner>5__2
	RuntimeObject* ___U3CrunnerU3E5__2_4;
	// System.Boolean Zenject.PrefabInstantiatorCached/<Instantiate>d__10::<hasMore>5__3
	bool ___U3ChasMoreU3E5__3_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6, ___U3CU3E2__current_1)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6, ___args_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_2() const { return ___args_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6, ___U3CU3E4__this_3)); }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_U3CrunnerU3E5__2_4() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6, ___U3CrunnerU3E5__2_4)); }
	inline RuntimeObject* get_U3CrunnerU3E5__2_4() const { return ___U3CrunnerU3E5__2_4; }
	inline RuntimeObject** get_address_of_U3CrunnerU3E5__2_4() { return &___U3CrunnerU3E5__2_4; }
	inline void set_U3CrunnerU3E5__2_4(RuntimeObject* value)
	{
		___U3CrunnerU3E5__2_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CrunnerU3E5__2_4), value);
	}

	inline static int32_t get_offset_of_U3ChasMoreU3E5__3_5() { return static_cast<int32_t>(offsetof(U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6, ___U3ChasMoreU3E5__3_5)); }
	inline bool get_U3ChasMoreU3E5__3_5() const { return ___U3ChasMoreU3E5__3_5; }
	inline bool* get_address_of_U3ChasMoreU3E5__3_5() { return &___U3ChasMoreU3E5__3_5; }
	inline void set_U3ChasMoreU3E5__3_5(bool value)
	{
		___U3ChasMoreU3E5__3_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CINSTANTIATEU3ED__10_T0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6_H
#ifndef PREFABPROVIDER_T5B3BBDBD342D6AF349A55D3C0A145D1214561746_H
#define PREFABPROVIDER_T5B3BBDBD342D6AF349A55D3C0A145D1214561746_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabProvider
struct  PrefabProvider_t5B3BBDBD342D6AF349A55D3C0A145D1214561746  : public RuntimeObject
{
public:
	// UnityEngine.Object Zenject.PrefabProvider::_prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ____prefab_0;

public:
	inline static int32_t get_offset_of__prefab_0() { return static_cast<int32_t>(offsetof(PrefabProvider_t5B3BBDBD342D6AF349A55D3C0A145D1214561746, ____prefab_0)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get__prefab_0() const { return ____prefab_0; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of__prefab_0() { return &____prefab_0; }
	inline void set__prefab_0(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		____prefab_0 = value;
		Il2CppCodeGenWriteBarrier((&____prefab_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPROVIDER_T5B3BBDBD342D6AF349A55D3C0A145D1214561746_H
#ifndef PREFABPROVIDERRESOURCE_TD701682D136B97FAF6FCEAF5C4FF723836311B6B_H
#define PREFABPROVIDERRESOURCE_TD701682D136B97FAF6FCEAF5C4FF723836311B6B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabProviderResource
struct  PrefabProviderResource_tD701682D136B97FAF6FCEAF5C4FF723836311B6B  : public RuntimeObject
{
public:
	// System.String Zenject.PrefabProviderResource::_resourcePath
	String_t* ____resourcePath_0;

public:
	inline static int32_t get_offset_of__resourcePath_0() { return static_cast<int32_t>(offsetof(PrefabProviderResource_tD701682D136B97FAF6FCEAF5C4FF723836311B6B, ____resourcePath_0)); }
	inline String_t* get__resourcePath_0() const { return ____resourcePath_0; }
	inline String_t** get_address_of__resourcePath_0() { return &____resourcePath_0; }
	inline void set__resourcePath_0(String_t* value)
	{
		____resourcePath_0 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABPROVIDERRESOURCE_TD701682D136B97FAF6FCEAF5C4FF723836311B6B_H
#ifndef PREFABRESOURCESINGLETONPROVIDERCREATOR_T95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5_H
#define PREFABRESOURCESINGLETONPROVIDERCREATOR_T95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceSingletonProviderCreator
struct  PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.PrefabResourceSingletonProviderCreator::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// Zenject.DiContainer Zenject.PrefabResourceSingletonProviderCreator::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.PrefabResourceSingletonProviderCreator/PrefabId,Zenject.IPrefabInstantiator> Zenject.PrefabResourceSingletonProviderCreator::_prefabCreators
	Dictionary_2_t036520DC7F6A25275BF333DAD200A2E127365F66 * ____prefabCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__prefabCreators_2() { return static_cast<int32_t>(offsetof(PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5, ____prefabCreators_2)); }
	inline Dictionary_2_t036520DC7F6A25275BF333DAD200A2E127365F66 * get__prefabCreators_2() const { return ____prefabCreators_2; }
	inline Dictionary_2_t036520DC7F6A25275BF333DAD200A2E127365F66 ** get_address_of__prefabCreators_2() { return &____prefabCreators_2; }
	inline void set__prefabCreators_2(Dictionary_2_t036520DC7F6A25275BF333DAD200A2E127365F66 * value)
	{
		____prefabCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABRESOURCESINGLETONPROVIDERCREATOR_T95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5_H
#ifndef PREFABID_T5F7794FBC5FD6018155AB01F0387E08A3E18F9B8_H
#define PREFABID_T5F7794FBC5FD6018155AB01F0387E08A3E18F9B8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabResourceSingletonProviderCreator/PrefabId
struct  PrefabId_t5F7794FBC5FD6018155AB01F0387E08A3E18F9B8  : public RuntimeObject
{
public:
	// System.Object Zenject.PrefabResourceSingletonProviderCreator/PrefabId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// System.String Zenject.PrefabResourceSingletonProviderCreator/PrefabId::ResourcePath
	String_t* ___ResourcePath_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(PrefabId_t5F7794FBC5FD6018155AB01F0387E08A3E18F9B8, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_ResourcePath_1() { return static_cast<int32_t>(offsetof(PrefabId_t5F7794FBC5FD6018155AB01F0387E08A3E18F9B8, ___ResourcePath_1)); }
	inline String_t* get_ResourcePath_1() const { return ___ResourcePath_1; }
	inline String_t** get_address_of_ResourcePath_1() { return &___ResourcePath_1; }
	inline void set_ResourcePath_1(String_t* value)
	{
		___ResourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&___ResourcePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABID_T5F7794FBC5FD6018155AB01F0387E08A3E18F9B8_H
#ifndef PREFABSINGLETONPROVIDERCREATOR_T4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F_H
#define PREFABSINGLETONPROVIDERCREATOR_T4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProviderCreator
struct  PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F  : public RuntimeObject
{
public:
	// Zenject.SingletonMarkRegistry Zenject.PrefabSingletonProviderCreator::_markRegistry
	SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * ____markRegistry_0;
	// Zenject.DiContainer Zenject.PrefabSingletonProviderCreator::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Collections.Generic.Dictionary`2<Zenject.PrefabSingletonProviderCreator/PrefabId,Zenject.IPrefabInstantiator> Zenject.PrefabSingletonProviderCreator::_prefabCreators
	Dictionary_2_t695060A2E0C3A93231AB54496586F254B36BAB84 * ____prefabCreators_2;

public:
	inline static int32_t get_offset_of__markRegistry_0() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F, ____markRegistry_0)); }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * get__markRegistry_0() const { return ____markRegistry_0; }
	inline SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 ** get_address_of__markRegistry_0() { return &____markRegistry_0; }
	inline void set__markRegistry_0(SingletonMarkRegistry_t4493AE1D01A2ED5DA483EA44D6E5A5A72D1C0751 * value)
	{
		____markRegistry_0 = value;
		Il2CppCodeGenWriteBarrier((&____markRegistry_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__prefabCreators_2() { return static_cast<int32_t>(offsetof(PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F, ____prefabCreators_2)); }
	inline Dictionary_2_t695060A2E0C3A93231AB54496586F254B36BAB84 * get__prefabCreators_2() const { return ____prefabCreators_2; }
	inline Dictionary_2_t695060A2E0C3A93231AB54496586F254B36BAB84 ** get_address_of__prefabCreators_2() { return &____prefabCreators_2; }
	inline void set__prefabCreators_2(Dictionary_2_t695060A2E0C3A93231AB54496586F254B36BAB84 * value)
	{
		____prefabCreators_2 = value;
		Il2CppCodeGenWriteBarrier((&____prefabCreators_2), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABSINGLETONPROVIDERCREATOR_T4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F_H
#ifndef PREFABID_T996F45923747785EF556C15C030AF54D0464B346_H
#define PREFABID_T996F45923747785EF556C15C030AF54D0464B346_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.PrefabSingletonProviderCreator/PrefabId
struct  PrefabId_t996F45923747785EF556C15C030AF54D0464B346  : public RuntimeObject
{
public:
	// System.Object Zenject.PrefabSingletonProviderCreator/PrefabId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_0;
	// UnityEngine.Object Zenject.PrefabSingletonProviderCreator/PrefabId::Prefab
	Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___Prefab_1;

public:
	inline static int32_t get_offset_of_ConcreteIdentifier_0() { return static_cast<int32_t>(offsetof(PrefabId_t996F45923747785EF556C15C030AF54D0464B346, ___ConcreteIdentifier_0)); }
	inline RuntimeObject * get_ConcreteIdentifier_0() const { return ___ConcreteIdentifier_0; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_0() { return &___ConcreteIdentifier_0; }
	inline void set_ConcreteIdentifier_0(RuntimeObject * value)
	{
		___ConcreteIdentifier_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_0), value);
	}

	inline static int32_t get_offset_of_Prefab_1() { return static_cast<int32_t>(offsetof(PrefabId_t996F45923747785EF556C15C030AF54D0464B346, ___Prefab_1)); }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * get_Prefab_1() const { return ___Prefab_1; }
	inline Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 ** get_address_of_Prefab_1() { return &___Prefab_1; }
	inline void set_Prefab_1(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * value)
	{
		___Prefab_1 = value;
		Il2CppCodeGenWriteBarrier((&___Prefab_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PREFABID_T996F45923747785EF556C15C030AF54D0464B346_H
#ifndef PROVIDERUTIL_T564F359413D4B3158A57FC3315A06AAEA299CF2F_H
#define PROVIDERUTIL_T564F359413D4B3158A57FC3315A06AAEA299CF2F_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ProviderUtil
struct  ProviderUtil_t564F359413D4B3158A57FC3315A06AAEA299CF2F  : public RuntimeObject
{
public:

public:
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // PROVIDERUTIL_T564F359413D4B3158A57FC3315A06AAEA299CF2F_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__7_T90181A0B668943097F9F8DF159B82631EEFA16FE_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__7_T90181A0B668943097F9F8DF159B82631EEFA16FE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResolveProvider/<GetAllInstancesWithInjectSplit>d__7
struct  U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE  : public RuntimeObject
{
public:
	// System.Int32 Zenject.ResolveProvider/<GetAllInstancesWithInjectSplit>d__7::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.ResolveProvider/<GetAllInstancesWithInjectSplit>d__7::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ResolveProvider/<GetAllInstancesWithInjectSplit>d__7::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_2;
	// Zenject.InjectContext Zenject.ResolveProvider/<GetAllInstancesWithInjectSplit>d__7::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_3;
	// Zenject.ResolveProvider Zenject.ResolveProvider/<GetAllInstancesWithInjectSplit>d__7::<>4__this
	ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0 * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE, ___args_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_2() const { return ___args_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE, ___context_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_3() const { return ___context_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE, ___U3CU3E4__this_4)); }
	inline ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0 * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0 ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0 * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__7_T90181A0B668943097F9F8DF159B82631EEFA16FE_H
#ifndef RESOURCEPROVIDER_T743992DF852263C1A1547C431100239717D6D65C_H
#define RESOURCEPROVIDER_T743992DF852263C1A1547C431100239717D6D65C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResourceProvider
struct  ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C  : public RuntimeObject
{
public:
	// System.Type Zenject.ResourceProvider::_resourceType
	Type_t * ____resourceType_0;
	// System.String Zenject.ResourceProvider::_resourcePath
	String_t* ____resourcePath_1;

public:
	inline static int32_t get_offset_of__resourceType_0() { return static_cast<int32_t>(offsetof(ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C, ____resourceType_0)); }
	inline Type_t * get__resourceType_0() const { return ____resourceType_0; }
	inline Type_t ** get_address_of__resourceType_0() { return &____resourceType_0; }
	inline void set__resourceType_0(Type_t * value)
	{
		____resourceType_0 = value;
		Il2CppCodeGenWriteBarrier((&____resourceType_0), value);
	}

	inline static int32_t get_offset_of__resourcePath_1() { return static_cast<int32_t>(offsetof(ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C, ____resourcePath_1)); }
	inline String_t* get__resourcePath_1() const { return ____resourcePath_1; }
	inline String_t** get_address_of__resourcePath_1() { return &____resourcePath_1; }
	inline void set__resourcePath_1(String_t* value)
	{
		____resourcePath_1 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOURCEPROVIDER_T743992DF852263C1A1547C431100239717D6D65C_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T932A09DAD963365D008810A46CEFDEB40483E7DE_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T932A09DAD963365D008810A46CEFDEB40483E7DE_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResourceProvider/<GetAllInstancesWithInjectSplit>d__4
struct  U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE  : public RuntimeObject
{
public:
	// System.Int32 Zenject.ResourceProvider/<GetAllInstancesWithInjectSplit>d__4::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.ResourceProvider/<GetAllInstancesWithInjectSplit>d__4::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ResourceProvider/<GetAllInstancesWithInjectSplit>d__4::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_2;
	// Zenject.InjectContext Zenject.ResourceProvider/<GetAllInstancesWithInjectSplit>d__4::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_3;
	// Zenject.ResourceProvider Zenject.ResourceProvider/<GetAllInstancesWithInjectSplit>d__4::<>4__this
	ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C * ___U3CU3E4__this_4;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_args_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE, ___args_2)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_2() const { return ___args_2; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_2() { return &___args_2; }
	inline void set_args_2(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_2 = value;
		Il2CppCodeGenWriteBarrier((&___args_2), value);
	}

	inline static int32_t get_offset_of_context_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE, ___context_3)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_3() const { return ___context_3; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_3() { return &___context_3; }
	inline void set_context_3(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_3 = value;
		Il2CppCodeGenWriteBarrier((&___context_3), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE, ___U3CU3E4__this_4)); }
	inline ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C * get_U3CU3E4__this_4() const { return ___U3CU3E4__this_4; }
	inline ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C ** get_address_of_U3CU3E4__this_4() { return &___U3CU3E4__this_4; }
	inline void set_U3CU3E4__this_4(ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C * value)
	{
		___U3CU3E4__this_4 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__4_T932A09DAD963365D008810A46CEFDEB40483E7DE_H
#ifndef SCRIPTABLEOBJECTRESOURCEPROVIDER_TA9E7E36A71167184A28D3E44C5F76FB2C9B3E054_H
#define SCRIPTABLEOBJECTRESOURCEPROVIDER_TA9E7E36A71167184A28D3E44C5F76FB2C9B3E054_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectResourceProvider
struct  ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054  : public RuntimeObject
{
public:
	// Zenject.DiContainer Zenject.ScriptableObjectResourceProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_0;
	// System.Type Zenject.ScriptableObjectResourceProvider::_resourceType
	Type_t * ____resourceType_1;
	// System.String Zenject.ScriptableObjectResourceProvider::_resourcePath
	String_t* ____resourcePath_2;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ScriptableObjectResourceProvider::_extraArguments
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ____extraArguments_3;
	// System.Object Zenject.ScriptableObjectResourceProvider::_concreteIdentifier
	RuntimeObject * ____concreteIdentifier_4;
	// System.Boolean Zenject.ScriptableObjectResourceProvider::_createNew
	bool ____createNew_5;

public:
	inline static int32_t get_offset_of__container_0() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054, ____container_0)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_0() const { return ____container_0; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_0() { return &____container_0; }
	inline void set__container_0(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_0 = value;
		Il2CppCodeGenWriteBarrier((&____container_0), value);
	}

	inline static int32_t get_offset_of__resourceType_1() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054, ____resourceType_1)); }
	inline Type_t * get__resourceType_1() const { return ____resourceType_1; }
	inline Type_t ** get_address_of__resourceType_1() { return &____resourceType_1; }
	inline void set__resourceType_1(Type_t * value)
	{
		____resourceType_1 = value;
		Il2CppCodeGenWriteBarrier((&____resourceType_1), value);
	}

	inline static int32_t get_offset_of__resourcePath_2() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054, ____resourcePath_2)); }
	inline String_t* get__resourcePath_2() const { return ____resourcePath_2; }
	inline String_t** get_address_of__resourcePath_2() { return &____resourcePath_2; }
	inline void set__resourcePath_2(String_t* value)
	{
		____resourcePath_2 = value;
		Il2CppCodeGenWriteBarrier((&____resourcePath_2), value);
	}

	inline static int32_t get_offset_of__extraArguments_3() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054, ____extraArguments_3)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get__extraArguments_3() const { return ____extraArguments_3; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of__extraArguments_3() { return &____extraArguments_3; }
	inline void set__extraArguments_3(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		____extraArguments_3 = value;
		Il2CppCodeGenWriteBarrier((&____extraArguments_3), value);
	}

	inline static int32_t get_offset_of__concreteIdentifier_4() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054, ____concreteIdentifier_4)); }
	inline RuntimeObject * get__concreteIdentifier_4() const { return ____concreteIdentifier_4; }
	inline RuntimeObject ** get_address_of__concreteIdentifier_4() { return &____concreteIdentifier_4; }
	inline void set__concreteIdentifier_4(RuntimeObject * value)
	{
		____concreteIdentifier_4 = value;
		Il2CppCodeGenWriteBarrier((&____concreteIdentifier_4), value);
	}

	inline static int32_t get_offset_of__createNew_5() { return static_cast<int32_t>(offsetof(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054, ____createNew_5)); }
	inline bool get__createNew_5() const { return ____createNew_5; }
	inline bool* get_address_of__createNew_5() { return &____createNew_5; }
	inline void set__createNew_5(bool value)
	{
		____createNew_5 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SCRIPTABLEOBJECTRESOURCEPROVIDER_TA9E7E36A71167184A28D3E44C5F76FB2C9B3E054_H
#ifndef U3CU3EC_T11524BBEA26F856485D8496AF473078BD27E7732_H
#define U3CU3EC_T11524BBEA26F856485D8496AF473078BD27E7732_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectResourceProvider/<>c
struct  U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732  : public RuntimeObject
{
public:

public:
};

struct U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732_StaticFields
{
public:
	// Zenject.ScriptableObjectResourceProvider/<>c Zenject.ScriptableObjectResourceProvider/<>c::<>9
	U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732 * ___U3CU3E9_0;
	// System.Func`2<UnityEngine.Object,UnityEngine.Object> Zenject.ScriptableObjectResourceProvider/<>c::<>9__8_0
	Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 * ___U3CU3E9__8_0_1;

public:
	inline static int32_t get_offset_of_U3CU3E9_0() { return static_cast<int32_t>(offsetof(U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732_StaticFields, ___U3CU3E9_0)); }
	inline U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732 * get_U3CU3E9_0() const { return ___U3CU3E9_0; }
	inline U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732 ** get_address_of_U3CU3E9_0() { return &___U3CU3E9_0; }
	inline void set_U3CU3E9_0(U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732 * value)
	{
		___U3CU3E9_0 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9_0), value);
	}

	inline static int32_t get_offset_of_U3CU3E9__8_0_1() { return static_cast<int32_t>(offsetof(U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732_StaticFields, ___U3CU3E9__8_0_1)); }
	inline Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 * get_U3CU3E9__8_0_1() const { return ___U3CU3E9__8_0_1; }
	inline Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 ** get_address_of_U3CU3E9__8_0_1() { return &___U3CU3E9__8_0_1; }
	inline void set_U3CU3E9__8_0_1(Func_2_t7F9671FD00F31AFFC0C1BEB0761BA9E68AB18E89 * value)
	{
		___U3CU3E9__8_0_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E9__8_0_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC_T11524BBEA26F856485D8496AF473078BD27E7732_H
#ifndef U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T9116C58B08212A56B80DD2BF582ACA58E2C0D856_H
#define U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T9116C58B08212A56B80DD2BF582ACA58E2C0D856_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8
struct  U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856  : public RuntimeObject
{
public:
	// System.Int32 Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Collections.Generic.List`1<System.Object> Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8::<>2__current
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CU3E2__current_1;
	// Zenject.InjectContext Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8::context
	InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * ___context_2;
	// Zenject.ScriptableObjectResourceProvider Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8::<>4__this
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054 * ___U3CU3E4__this_3;
	// System.Collections.Generic.List`1<Zenject.TypeValuePair> Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8::args
	List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * ___args_4;
	// System.Collections.Generic.List`1<System.Object> Zenject.ScriptableObjectResourceProvider/<GetAllInstancesWithInjectSplit>d__8::<objects>5__2
	List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * ___U3CobjectsU3E5__2_5;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856, ___U3CU3E2__current_1)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_context_2() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856, ___context_2)); }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * get_context_2() const { return ___context_2; }
	inline InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 ** get_address_of_context_2() { return &___context_2; }
	inline void set_context_2(InjectContext_t1C8120A2BE158ECE706FD7574401F8655F5CE649 * value)
	{
		___context_2 = value;
		Il2CppCodeGenWriteBarrier((&___context_2), value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_3() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856, ___U3CU3E4__this_3)); }
	inline ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054 * get_U3CU3E4__this_3() const { return ___U3CU3E4__this_3; }
	inline ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054 ** get_address_of_U3CU3E4__this_3() { return &___U3CU3E4__this_3; }
	inline void set_U3CU3E4__this_3(ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054 * value)
	{
		___U3CU3E4__this_3 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_3), value);
	}

	inline static int32_t get_offset_of_args_4() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856, ___args_4)); }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * get_args_4() const { return ___args_4; }
	inline List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 ** get_address_of_args_4() { return &___args_4; }
	inline void set_args_4(List_1_t0CA24EFC08E74567DA8B8EDF6A5B8727B616FBE7 * value)
	{
		___args_4 = value;
		Il2CppCodeGenWriteBarrier((&___args_4), value);
	}

	inline static int32_t get_offset_of_U3CobjectsU3E5__2_5() { return static_cast<int32_t>(offsetof(U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856, ___U3CobjectsU3E5__2_5)); }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * get_U3CobjectsU3E5__2_5() const { return ___U3CobjectsU3E5__2_5; }
	inline List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D ** get_address_of_U3CobjectsU3E5__2_5() { return &___U3CobjectsU3E5__2_5; }
	inline void set_U3CobjectsU3E5__2_5(List_1_t05CC3C859AB5E6024394EF9A42E3E696628CA02D * value)
	{
		___U3CobjectsU3E5__2_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CobjectsU3E5__2_5), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLINSTANCESWITHINJECTSPLITU3ED__8_T9116C58B08212A56B80DD2BF582ACA58E2C0D856_H
#ifndef SINGLETONID_T688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3_H
#define SINGLETONID_T688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.SingletonId
struct  SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3  : public RuntimeObject
{
public:
	// System.Type Zenject.SingletonId::ConcreteType
	Type_t * ___ConcreteType_0;
	// System.Object Zenject.SingletonId::ConcreteIdentifier
	RuntimeObject * ___ConcreteIdentifier_1;

public:
	inline static int32_t get_offset_of_ConcreteType_0() { return static_cast<int32_t>(offsetof(SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3, ___ConcreteType_0)); }
	inline Type_t * get_ConcreteType_0() const { return ___ConcreteType_0; }
	inline Type_t ** get_address_of_ConcreteType_0() { return &___ConcreteType_0; }
	inline void set_ConcreteType_0(Type_t * value)
	{
		___ConcreteType_0 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteType_0), value);
	}

	inline static int32_t get_offset_of_ConcreteIdentifier_1() { return static_cast<int32_t>(offsetof(SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3, ___ConcreteIdentifier_1)); }
	inline RuntimeObject * get_ConcreteIdentifier_1() const { return ___ConcreteIdentifier_1; }
	inline RuntimeObject ** get_address_of_ConcreteIdentifier_1() { return &___ConcreteIdentifier_1; }
	inline void set_ConcreteIdentifier_1(RuntimeObject * value)
	{
		___ConcreteIdentifier_1 = value;
		Il2CppCodeGenWriteBarrier((&___ConcreteIdentifier_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // SINGLETONID_T688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3_H
#ifndef ENUMERATOR_TDDE44991E9720BF11581CD6916F2AC6EF18DB007_H
#define ENUMERATOR_TDDE44991E9720BF11581CD6916F2AC6EF18DB007_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Collections.Generic.List`1/Enumerator<Zenject.DiContainer>
struct  Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007 
{
public:
	// System.Collections.Generic.List`1<T> System.Collections.Generic.List`1/Enumerator::list
	List_1_t613607F896640D94824672D01078AB4BBFA62166 * ___list_0;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::index
	int32_t ___index_1;
	// System.Int32 System.Collections.Generic.List`1/Enumerator::version
	int32_t ___version_2;
	// T System.Collections.Generic.List`1/Enumerator::current
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___current_3;

public:
	inline static int32_t get_offset_of_list_0() { return static_cast<int32_t>(offsetof(Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007, ___list_0)); }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 * get_list_0() const { return ___list_0; }
	inline List_1_t613607F896640D94824672D01078AB4BBFA62166 ** get_address_of_list_0() { return &___list_0; }
	inline void set_list_0(List_1_t613607F896640D94824672D01078AB4BBFA62166 * value)
	{
		___list_0 = value;
		Il2CppCodeGenWriteBarrier((&___list_0), value);
	}

	inline static int32_t get_offset_of_index_1() { return static_cast<int32_t>(offsetof(Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007, ___index_1)); }
	inline int32_t get_index_1() const { return ___index_1; }
	inline int32_t* get_address_of_index_1() { return &___index_1; }
	inline void set_index_1(int32_t value)
	{
		___index_1 = value;
	}

	inline static int32_t get_offset_of_version_2() { return static_cast<int32_t>(offsetof(Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007, ___version_2)); }
	inline int32_t get_version_2() const { return ___version_2; }
	inline int32_t* get_address_of_version_2() { return &___version_2; }
	inline void set_version_2(int32_t value)
	{
		___version_2 = value;
	}

	inline static int32_t get_offset_of_current_3() { return static_cast<int32_t>(offsetof(Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007, ___current_3)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_current_3() const { return ___current_3; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_current_3() { return &___current_3; }
	inline void set_current_3(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___current_3 = value;
		Il2CppCodeGenWriteBarrier((&___current_3), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ENUMERATOR_TDDE44991E9720BF11581CD6916F2AC6EF18DB007_H
#ifndef ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#define ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Enum
struct  Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((&___enumSeperatorCharArray_0), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};
#endif // ENUM_T2AF27C02B8653AE29442467390005ABC74D8F521_H
#ifndef ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDER_T282A2D1090EDDE0DB8BF1627268D40D2668ABAF5_H
#define ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDER_T282A2D1090EDDE0DB8BF1627268D40D2668ABAF5_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToExistingGameObjectComponentProvider
struct  AddToExistingGameObjectComponentProvider_t282A2D1090EDDE0DB8BF1627268D40D2668ABAF5  : public AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A
{
public:
	// UnityEngine.GameObject Zenject.AddToExistingGameObjectComponentProvider::_gameObject
	GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * ____gameObject_4;

public:
	inline static int32_t get_offset_of__gameObject_4() { return static_cast<int32_t>(offsetof(AddToExistingGameObjectComponentProvider_t282A2D1090EDDE0DB8BF1627268D40D2668ABAF5, ____gameObject_4)); }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * get__gameObject_4() const { return ____gameObject_4; }
	inline GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F ** get_address_of__gameObject_4() { return &____gameObject_4; }
	inline void set__gameObject_4(GameObject_tBD1244AD56B4E59AAD76E5E7C9282EC5CE434F0F * value)
	{
		____gameObject_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObject_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDER_T282A2D1090EDDE0DB8BF1627268D40D2668ABAF5_H
#ifndef ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDERGETTER_T5D1979D720DF42EB98314EEB895AD101564ECC2C_H
#define ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDERGETTER_T5D1979D720DF42EB98314EEB895AD101564ECC2C_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToExistingGameObjectComponentProviderGetter
struct  AddToExistingGameObjectComponentProviderGetter_t5D1979D720DF42EB98314EEB895AD101564ECC2C  : public AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A
{
public:
	// System.Func`2<Zenject.InjectContext,UnityEngine.GameObject> Zenject.AddToExistingGameObjectComponentProviderGetter::_gameObjectGetter
	Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 * ____gameObjectGetter_4;

public:
	inline static int32_t get_offset_of__gameObjectGetter_4() { return static_cast<int32_t>(offsetof(AddToExistingGameObjectComponentProviderGetter_t5D1979D720DF42EB98314EEB895AD101564ECC2C, ____gameObjectGetter_4)); }
	inline Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 * get__gameObjectGetter_4() const { return ____gameObjectGetter_4; }
	inline Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 ** get_address_of__gameObjectGetter_4() { return &____gameObjectGetter_4; }
	inline void set__gameObjectGetter_4(Func_2_t23BD9C763156A03A62F6D54C2E84B4844A9A4E16 * value)
	{
		____gameObjectGetter_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectGetter_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTOEXISTINGGAMEOBJECTCOMPONENTPROVIDERGETTER_T5D1979D720DF42EB98314EEB895AD101564ECC2C_H
#ifndef ADDTONEWGAMEOBJECTCOMPONENTPROVIDER_T89A73FB909ABE49C7467A47BFEE35968552A5A9B_H
#define ADDTONEWGAMEOBJECTCOMPONENTPROVIDER_T89A73FB909ABE49C7467A47BFEE35968552A5A9B_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.AddToNewGameObjectComponentProvider
struct  AddToNewGameObjectComponentProvider_t89A73FB909ABE49C7467A47BFEE35968552A5A9B  : public AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A
{
public:
	// Zenject.GameObjectCreationParameters Zenject.AddToNewGameObjectComponentProvider::_gameObjectBindInfo
	GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * ____gameObjectBindInfo_4;

public:
	inline static int32_t get_offset_of__gameObjectBindInfo_4() { return static_cast<int32_t>(offsetof(AddToNewGameObjectComponentProvider_t89A73FB909ABE49C7467A47BFEE35968552A5A9B, ____gameObjectBindInfo_4)); }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * get__gameObjectBindInfo_4() const { return ____gameObjectBindInfo_4; }
	inline GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 ** get_address_of__gameObjectBindInfo_4() { return &____gameObjectBindInfo_4; }
	inline void set__gameObjectBindInfo_4(GameObjectCreationParameters_tA8B0A78583E6869811C9ADF2B0CC79D2A03C29F4 * value)
	{
		____gameObjectBindInfo_4 = value;
		Il2CppCodeGenWriteBarrier((&____gameObjectBindInfo_4), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // ADDTONEWGAMEOBJECTCOMPONENTPROVIDER_T89A73FB909ABE49C7467A47BFEE35968552A5A9B_H
#ifndef LOOKUPID_T1FB6A138D9CD34D34E75CE606DCB31620C030DF8_H
#define LOOKUPID_T1FB6A138D9CD34D34E75CE606DCB31620C030DF8_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/LookupId
struct  LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8 
{
public:
	// Zenject.IProvider Zenject.DiContainer/LookupId::Provider
	RuntimeObject* ___Provider_0;
	// Zenject.BindingId Zenject.DiContainer/LookupId::BindingId
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___BindingId_1;

public:
	inline static int32_t get_offset_of_Provider_0() { return static_cast<int32_t>(offsetof(LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8, ___Provider_0)); }
	inline RuntimeObject* get_Provider_0() const { return ___Provider_0; }
	inline RuntimeObject** get_address_of_Provider_0() { return &___Provider_0; }
	inline void set_Provider_0(RuntimeObject* value)
	{
		___Provider_0 = value;
		Il2CppCodeGenWriteBarrier((&___Provider_0), value);
	}

	inline static int32_t get_offset_of_BindingId_1() { return static_cast<int32_t>(offsetof(LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8, ___BindingId_1)); }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * get_BindingId_1() const { return ___BindingId_1; }
	inline BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 ** get_address_of_BindingId_1() { return &___BindingId_1; }
	inline void set_BindingId_1(BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * value)
	{
		___BindingId_1 = value;
		Il2CppCodeGenWriteBarrier((&___BindingId_1), value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// Native definition for P/Invoke marshalling of Zenject.DiContainer/LookupId
struct LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8_marshaled_pinvoke
{
	RuntimeObject* ___Provider_0;
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___BindingId_1;
};
// Native definition for COM marshalling of Zenject.DiContainer/LookupId
struct LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8_marshaled_com
{
	RuntimeObject* ___Provider_0;
	BindingId_tCC0244F0D309405E44E49E79ECDC84102D127DB1 * ___BindingId_1;
};
#endif // LOOKUPID_T1FB6A138D9CD34D34E75CE606DCB31620C030DF8_H
#ifndef U3CU3EC__DISPLAYCLASS75_0_TCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9_H
#define U3CU3EC__DISPLAYCLASS75_0_TCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<>c__DisplayClass75_0
struct  U3CU3Ec__DisplayClass75_0_tCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9  : public RuntimeObject
{
public:
	// Zenject.DiContainer/LookupId Zenject.DiContainer/<>c__DisplayClass75_0::lookupId
	LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8  ___lookupId_0;

public:
	inline static int32_t get_offset_of_lookupId_0() { return static_cast<int32_t>(offsetof(U3CU3Ec__DisplayClass75_0_tCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9, ___lookupId_0)); }
	inline LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8  get_lookupId_0() const { return ___lookupId_0; }
	inline LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8 * get_address_of_lookupId_0() { return &___lookupId_0; }
	inline void set_lookupId_0(LookupId_t1FB6A138D9CD34D34E75CE606DCB31620C030DF8  value)
	{
		___lookupId_0 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CU3EC__DISPLAYCLASS75_0_TCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9_H
#ifndef INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#define INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.InjectSources
struct  InjectSources_t05F31227C87E0653D0F785E74E838313F2158963 
{
public:
	// System.Int32 Zenject.InjectSources::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(InjectSources_t05F31227C87E0653D0F785E74E838313F2158963, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // INJECTSOURCES_T05F31227C87E0653D0F785E74E838313F2158963_H
#ifndef U3CGETALLCONTAINERSTOLOOKUPU3ED__58_T2A6C51961968E9D911DD6C3DB8E8CC508CA8401A_H
#define U3CGETALLCONTAINERSTOLOOKUPU3ED__58_T2A6C51961968E9D911DD6C3DB8E8CC508CA8401A_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.DiContainer/<GetAllContainersToLookup>d__58
struct  U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A  : public RuntimeObject
{
public:
	// System.Int32 Zenject.DiContainer/<GetAllContainersToLookup>d__58::<>1__state
	int32_t ___U3CU3E1__state_0;
	// Zenject.DiContainer Zenject.DiContainer/<GetAllContainersToLookup>d__58::<>2__current
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CU3E2__current_1;
	// System.Int32 Zenject.DiContainer/<GetAllContainersToLookup>d__58::<>l__initialThreadId
	int32_t ___U3CU3El__initialThreadId_2;
	// Zenject.InjectSources Zenject.DiContainer/<GetAllContainersToLookup>d__58::sourceType
	int32_t ___sourceType_3;
	// Zenject.InjectSources Zenject.DiContainer/<GetAllContainersToLookup>d__58::<>3__sourceType
	int32_t ___U3CU3E3__sourceType_4;
	// Zenject.DiContainer Zenject.DiContainer/<GetAllContainersToLookup>d__58::<>4__this
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ___U3CU3E4__this_5;
	// System.Collections.Generic.List`1/Enumerator<Zenject.DiContainer> Zenject.DiContainer/<GetAllContainersToLookup>d__58::<>7__wrap1
	Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007  ___U3CU3E7__wrap1_6;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___U3CU3E2__current_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E2__current_1), value);
	}

	inline static int32_t get_offset_of_U3CU3El__initialThreadId_2() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___U3CU3El__initialThreadId_2)); }
	inline int32_t get_U3CU3El__initialThreadId_2() const { return ___U3CU3El__initialThreadId_2; }
	inline int32_t* get_address_of_U3CU3El__initialThreadId_2() { return &___U3CU3El__initialThreadId_2; }
	inline void set_U3CU3El__initialThreadId_2(int32_t value)
	{
		___U3CU3El__initialThreadId_2 = value;
	}

	inline static int32_t get_offset_of_sourceType_3() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___sourceType_3)); }
	inline int32_t get_sourceType_3() const { return ___sourceType_3; }
	inline int32_t* get_address_of_sourceType_3() { return &___sourceType_3; }
	inline void set_sourceType_3(int32_t value)
	{
		___sourceType_3 = value;
	}

	inline static int32_t get_offset_of_U3CU3E3__sourceType_4() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___U3CU3E3__sourceType_4)); }
	inline int32_t get_U3CU3E3__sourceType_4() const { return ___U3CU3E3__sourceType_4; }
	inline int32_t* get_address_of_U3CU3E3__sourceType_4() { return &___U3CU3E3__sourceType_4; }
	inline void set_U3CU3E3__sourceType_4(int32_t value)
	{
		___U3CU3E3__sourceType_4 = value;
	}

	inline static int32_t get_offset_of_U3CU3E4__this_5() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___U3CU3E4__this_5)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get_U3CU3E4__this_5() const { return ___U3CU3E4__this_5; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of_U3CU3E4__this_5() { return &___U3CU3E4__this_5; }
	inline void set_U3CU3E4__this_5(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		___U3CU3E4__this_5 = value;
		Il2CppCodeGenWriteBarrier((&___U3CU3E4__this_5), value);
	}

	inline static int32_t get_offset_of_U3CU3E7__wrap1_6() { return static_cast<int32_t>(offsetof(U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A, ___U3CU3E7__wrap1_6)); }
	inline Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007  get_U3CU3E7__wrap1_6() const { return ___U3CU3E7__wrap1_6; }
	inline Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007 * get_address_of_U3CU3E7__wrap1_6() { return &___U3CU3E7__wrap1_6; }
	inline void set_U3CU3E7__wrap1_6(Enumerator_tDDE44991E9720BF11581CD6916F2AC6EF18DB007  value)
	{
		___U3CU3E7__wrap1_6 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // U3CGETALLCONTAINERSTOLOOKUPU3ED__58_T2A6C51961968E9D911DD6C3DB8E8CC508CA8401A_H
#ifndef RESOLVEPROVIDER_T859A467985BBACA53F704ED8C986BBC63CD5AED0_H
#define RESOLVEPROVIDER_T859A467985BBACA53F704ED8C986BBC63CD5AED0_H
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Zenject.ResolveProvider
struct  ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0  : public RuntimeObject
{
public:
	// System.Object Zenject.ResolveProvider::_identifier
	RuntimeObject * ____identifier_0;
	// Zenject.DiContainer Zenject.ResolveProvider::_container
	DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * ____container_1;
	// System.Type Zenject.ResolveProvider::_contractType
	Type_t * ____contractType_2;
	// System.Boolean Zenject.ResolveProvider::_isOptional
	bool ____isOptional_3;
	// Zenject.InjectSources Zenject.ResolveProvider::_source
	int32_t ____source_4;

public:
	inline static int32_t get_offset_of__identifier_0() { return static_cast<int32_t>(offsetof(ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0, ____identifier_0)); }
	inline RuntimeObject * get__identifier_0() const { return ____identifier_0; }
	inline RuntimeObject ** get_address_of__identifier_0() { return &____identifier_0; }
	inline void set__identifier_0(RuntimeObject * value)
	{
		____identifier_0 = value;
		Il2CppCodeGenWriteBarrier((&____identifier_0), value);
	}

	inline static int32_t get_offset_of__container_1() { return static_cast<int32_t>(offsetof(ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0, ____container_1)); }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * get__container_1() const { return ____container_1; }
	inline DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 ** get_address_of__container_1() { return &____container_1; }
	inline void set__container_1(DiContainer_tBCBFE3CA0EF2DE2AB459237EE2AC75F46A9D77F9 * value)
	{
		____container_1 = value;
		Il2CppCodeGenWriteBarrier((&____container_1), value);
	}

	inline static int32_t get_offset_of__contractType_2() { return static_cast<int32_t>(offsetof(ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0, ____contractType_2)); }
	inline Type_t * get__contractType_2() const { return ____contractType_2; }
	inline Type_t ** get_address_of__contractType_2() { return &____contractType_2; }
	inline void set__contractType_2(Type_t * value)
	{
		____contractType_2 = value;
		Il2CppCodeGenWriteBarrier((&____contractType_2), value);
	}

	inline static int32_t get_offset_of__isOptional_3() { return static_cast<int32_t>(offsetof(ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0, ____isOptional_3)); }
	inline bool get__isOptional_3() const { return ____isOptional_3; }
	inline bool* get_address_of__isOptional_3() { return &____isOptional_3; }
	inline void set__isOptional_3(bool value)
	{
		____isOptional_3 = value;
	}

	inline static int32_t get_offset_of__source_4() { return static_cast<int32_t>(offsetof(ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0, ____source_4)); }
	inline int32_t get__source_4() const { return ____source_4; }
	inline int32_t* get_address_of__source_4() { return &____source_4; }
	inline void set__source_4(int32_t value)
	{
		____source_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
#endif // RESOLVEPROVIDER_T859A467985BBACA53F704ED8C986BBC63CD5AED0_H





#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7000 = { sizeof (U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7000[7] = 
{
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_sourceType_3(),
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_U3CU3E3__sourceType_4(),
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_U3CU3E4__this_5(),
	U3CGetAllContainersToLookupU3Ed__58_t2A6C51961968E9D911DD6C3DB8E8CC508CA8401A::get_offset_of_U3CU3E7__wrap1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7001 = { sizeof (U3CU3Ec__DisplayClass61_0_t5777778EDB97A22666FA08BFF6E4A725E750551B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7001[1] = 
{
	U3CU3Ec__DisplayClass61_0_t5777778EDB97A22666FA08BFF6E4A725E750551B::get_offset_of_bindingId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7002 = { sizeof (U3CU3Ec__DisplayClass65_0_t249E7667EDEAC5F1B3A6675A5D854AB072E29D95), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7002[2] = 
{
	U3CU3Ec__DisplayClass65_0_t249E7667EDEAC5F1B3A6675A5D854AB072E29D95::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass65_0_t249E7667EDEAC5F1B3A6675A5D854AB072E29D95::get_offset_of_context_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7003 = { sizeof (U3CU3Ec__DisplayClass72_0_t24F526DBAB79EBE252A560580A759DD979F83BB8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7003[1] = 
{
	U3CU3Ec__DisplayClass72_0_t24F526DBAB79EBE252A560580A759DD979F83BB8::get_offset_of_context_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7004 = { sizeof (U3CU3Ec__DisplayClass73_0_t4DAD416F4B2C837BD087C6963C5F4791D2237DCD), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7004[1] = 
{
	U3CU3Ec__DisplayClass73_0_t4DAD416F4B2C837BD087C6963C5F4791D2237DCD::get_offset_of_sortedProviders_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7005 = { sizeof (U3CU3Ec__DisplayClass75_0_tCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7005[1] = 
{
	U3CU3Ec__DisplayClass75_0_tCF7C511CD2D84A0C8409044B8B19F8F68B9DCFB9::get_offset_of_lookupId_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7006 = { sizeof (U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7006[7] = 
{
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_U3CU3E1__state_0(),
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_U3CU3E2__current_1(),
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_U3CU3El__initialThreadId_2(),
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_U3CU3E4__this_3(),
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_contract_4(),
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_U3CU3E3__contract_5(),
	U3CGetDependencyContractsU3Ed__79_t39F5B8F4D203C6E11E48FD2C4448D57D162CB519::get_offset_of_U3CU3E7__wrap1_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7007 = { sizeof (U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7007[4] = 
{
	U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7::get_offset_of_U3CU3E4__this_0(),
	U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7::get_offset_of_contractType_1(),
	U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7::get_offset_of_identifier_2(),
	U3CU3Ec__DisplayClass154_0_tF6F41007256F28E1A543819B7035F43D52A419F7::get_offset_of_concreteType_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7008 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7008[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7009 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7010 = { sizeof (LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7010[2] = 
{
	LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E::get_offset_of__container_0(),
	LazyInstanceInjector_tC8CCC4DD2BE67188E03FDF03E28553F8BC3DB58E::get_offset_of__instancesToInject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7011 = { sizeof (CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7011[3] = 
{
	CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8::get_offset_of__creator_0(),
	CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8::get_offset_of__instances_1(),
	CachedProvider_t448EC7D52925C5C0695970DE1C374655544F04B8::get_offset_of__isCreatingInstance_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7012 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7012[7] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_U3CrunnerU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t02D141F33C883E69F510E13F4100A6B4BA6B615F::get_offset_of_U3ChasMoreU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7013 = { sizeof (AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7013[4] = 
{
	AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C::get_offset_of__concreteIdentifier_0(),
	AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C::get_offset_of__componentType_1(),
	AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C::get_offset_of__container_2(),
	AddToCurrentGameObjectComponentProvider_t0C29BFB8B00F95804A40BDF7967D9E1867D8005C::get_offset_of__extraArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7014 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7014[6] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__12_t945C1ACD6A96018400E3C1F7A59D75FBD8EC2DD8::get_offset_of_U3CinstanceU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7015 = { sizeof (AddToExistingGameObjectComponentProvider_t282A2D1090EDDE0DB8BF1627268D40D2668ABAF5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7015[1] = 
{
	AddToExistingGameObjectComponentProvider_t282A2D1090EDDE0DB8BF1627268D40D2668ABAF5::get_offset_of__gameObject_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7016 = { sizeof (AddToExistingGameObjectComponentProviderGetter_t5D1979D720DF42EB98314EEB895AD101564ECC2C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7016[1] = 
{
	AddToExistingGameObjectComponentProviderGetter_t5D1979D720DF42EB98314EEB895AD101564ECC2C::get_offset_of__gameObjectGetter_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7017 = { sizeof (AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7017[4] = 
{
	AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A::get_offset_of__concreteIdentifier_0(),
	AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A::get_offset_of__componentType_1(),
	AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A::get_offset_of__container_2(),
	AddToGameObjectComponentProviderBase_t20AF22D871709F7F5E792D74A854C96D9720009A::get_offset_of__extraArguments_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7018 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7018[8] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_U3CinstanceU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_U3CgameObjU3E5__3_6(),
	U3CGetAllInstancesWithInjectSplitU3Ed__14_t9E97BD0480963043973B6AED4099227B16043523::get_offset_of_U3CwasActiveU3E5__4_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7019 = { sizeof (AddToNewGameObjectComponentProvider_t89A73FB909ABE49C7467A47BFEE35968552A5A9B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7019[1] = 
{
	AddToNewGameObjectComponentProvider_t89A73FB909ABE49C7467A47BFEE35968552A5A9B::get_offset_of__gameObjectBindInfo_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7020 = { sizeof (GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7020[2] = 
{
	GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003::get_offset_of__prefabInstantiator_0(),
	GetFromPrefabComponentProvider_tFEAFD1B8AEC1FDB480001CFE269F6D3342E97003::get_offset_of__componentType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7021 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7021[7] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_U3CgameObjectRunnerU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t1CBBC63B1774B037E25DC1E42802784072BE3634::get_offset_of_U3ChasMoreU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7022 = { sizeof (InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7022[2] = 
{
	InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192::get_offset_of__prefabInstantiator_0(),
	InstantiateOnPrefabComponentProvider_tFF5B6900958B685C76A7A38A3B9944F557562192::get_offset_of__componentType_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7023 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7023[7] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_U3CgameObjectRunnerU3E5__2_5(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA1F587E8CBCADB3D4B207AA0AEE4B0EE439A6CF1::get_offset_of_U3ChasMoreU3E5__3_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7024 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7024[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7025 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7026 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7026[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7027 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7028 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7028[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7029 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7030 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7030[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7031 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7032 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7032[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7033 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7034 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7034[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7035 = { 0, 0, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7036 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7036[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7037 = { sizeof (EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7037[2] = 
{
	EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8::get_offset_of__container_0(),
	EmptyGameObjectProvider_t917E0959FBDDAD602C2F51DE1AB6B99AD6AB86F8::get_offset_of__gameObjectBindInfo_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7038 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7038[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t0D29CFB6F37C3485662E0CD983E98A800E9D220B::get_offset_of_context_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7039 = { sizeof (PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7039[1] = 
{
	PrefabGameObjectProvider_tE637F7A2AD13B6B29F174376A6C903B6B4513B22::get_offset_of__prefabCreator_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7040 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7040[6] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811::get_offset_of_U3CU3E4__this_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811::get_offset_of_args_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811::get_offset_of_U3CrunnerU3E5__2_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__3_t76AC360ABF05C51EDAAAC4348955E70E57B2D811::get_offset_of_U3ChasMoreU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7041 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7041[3] = 
{
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7042 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7042[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7043 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7043[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7044 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7044[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7045 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7045[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7046 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7046[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7047 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7047[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7048 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7048[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7049 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7049[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7050 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7050[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7051 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7051[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7052 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7052[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7053 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7053[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7054 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7054[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7055 = { sizeof (InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7055[3] = 
{
	InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2::get_offset_of__instance_0(),
	InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2::get_offset_of__instanceType_1(),
	InstanceProvider_t0244AFF996FD42F2BF867A8C942A53B3F7C9CBC2::get_offset_of__container_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7056 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7056[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__5_t3C8BBD946FE9AFFF3A99E888C2F529DA1ECD4B1B::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7057 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7058 = { sizeof (IProviderExtensions_tD0DDAA15BB230B739EFD01ADD4F9656543786949), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7059 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7059[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7060 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7060[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7061 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7061[2] = 
{
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7062 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7062[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7063 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7063[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7064 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7064[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7065 = { sizeof (MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7065[2] = 
{
	MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA::get_offset_of__container_0(),
	MethodProviderUntyped_t799F0802EE96546AA0D409C31C77C1DF09FB75DA::get_offset_of__method_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7066 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7066[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_tA5B7D2F254018F9D9D2F728F8EC660A4AA1AD2FA::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7067 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7067[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7068 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7068[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7069 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7069[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7070 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7070[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7071 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7071[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7072 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7072[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7073 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7073[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7074 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7074[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7075 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7075[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7076 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7076[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7077 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7077[1] = 
{
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7078 = { 0, 0, 0, 0 };
extern const int32_t g_FieldOffsetTable7078[5] = 
{
	0,
	0,
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7079 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7080 = { sizeof (PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7080[5] = 
{
	PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE::get_offset_of__prefabProvider_0(),
	PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE::get_offset_of__container_1(),
	PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE::get_offset_of__extraArguments_2(),
	PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE::get_offset_of__gameObjectBindInfo_3(),
	PrefabInstantiator_t6036DEC964BFDA4E4A21D7EBE803918AB60683DE::get_offset_of__argumentTarget_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7081 = { sizeof (U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7081[7] = 
{
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_U3CU3E1__state_0(),
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_U3CU3E2__current_1(),
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_U3CU3E4__this_2(),
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_args_3(),
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_U3CcontextU3E5__2_4(),
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_U3CshouldMakeActiveU3E5__3_5(),
	U3CInstantiateU3Ed__13_tAEAB79BA7A3BB919F54AB460C45EECAA6585800F::get_offset_of_U3CgameObjectU3E5__4_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7082 = { sizeof (PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7082[2] = 
{
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943::get_offset_of__subInstantiator_0(),
	PrefabInstantiatorCached_tE36AD8F58DF7D3C659AEC758F1C4AF783BE16943::get_offset_of__gameObject_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7083 = { sizeof (U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7083[6] = 
{
	U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6::get_offset_of_U3CU3E1__state_0(),
	U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6::get_offset_of_U3CU3E2__current_1(),
	U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6::get_offset_of_args_2(),
	U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6::get_offset_of_U3CU3E4__this_3(),
	U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6::get_offset_of_U3CrunnerU3E5__2_4(),
	U3CInstantiateU3Ed__10_t0C194C763ADDF66BD7AF2AC73D9B0AA3D2D089F6::get_offset_of_U3ChasMoreU3E5__3_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7084 = { 0, -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7085 = { sizeof (PrefabProvider_t5B3BBDBD342D6AF349A55D3C0A145D1214561746), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7085[1] = 
{
	PrefabProvider_t5B3BBDBD342D6AF349A55D3C0A145D1214561746::get_offset_of__prefab_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7086 = { sizeof (PrefabProviderResource_tD701682D136B97FAF6FCEAF5C4FF723836311B6B), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7086[1] = 
{
	PrefabProviderResource_tD701682D136B97FAF6FCEAF5C4FF723836311B6B::get_offset_of__resourcePath_0(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7087 = { sizeof (ProviderUtil_t564F359413D4B3158A57FC3315A06AAEA299CF2F), -1, 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7088 = { sizeof (ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7088[5] = 
{
	ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0::get_offset_of__identifier_0(),
	ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0::get_offset_of__container_1(),
	ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0::get_offset_of__contractType_2(),
	ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0::get_offset_of__isOptional_3(),
	ResolveProvider_t859A467985BBACA53F704ED8C986BBC63CD5AED0::get_offset_of__source_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7089 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7089[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__7_t90181A0B668943097F9F8DF159B82631EEFA16FE::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7090 = { sizeof (ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7090[2] = 
{
	ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C::get_offset_of__resourceType_0(),
	ResourceProvider_t743992DF852263C1A1547C431100239717D6D65C::get_offset_of__resourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7091 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7091[5] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE::get_offset_of_args_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE::get_offset_of_context_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__4_t932A09DAD963365D008810A46CEFDEB40483E7DE::get_offset_of_U3CU3E4__this_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7092 = { sizeof (ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7092[6] = 
{
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054::get_offset_of__container_0(),
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054::get_offset_of__resourceType_1(),
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054::get_offset_of__resourcePath_2(),
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054::get_offset_of__extraArguments_3(),
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054::get_offset_of__concreteIdentifier_4(),
	ScriptableObjectResourceProvider_tA9E7E36A71167184A28D3E44C5F76FB2C9B3E054::get_offset_of__createNew_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7093 = { sizeof (U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732), -1, sizeof(U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732_StaticFields), 0 };
extern const int32_t g_FieldOffsetTable7093[2] = 
{
	U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732_StaticFields::get_offset_of_U3CU3E9_0(),
	U3CU3Ec_t11524BBEA26F856485D8496AF473078BD27E7732_StaticFields::get_offset_of_U3CU3E9__8_0_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7094 = { sizeof (U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7094[6] = 
{
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856::get_offset_of_U3CU3E1__state_0(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856::get_offset_of_U3CU3E2__current_1(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856::get_offset_of_context_2(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856::get_offset_of_U3CU3E4__this_3(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856::get_offset_of_args_4(),
	U3CGetAllInstancesWithInjectSplitU3Ed__8_t9116C58B08212A56B80DD2BF582ACA58E2C0D856::get_offset_of_U3CobjectsU3E5__2_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7095 = { sizeof (PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7095[3] = 
{
	PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5::get_offset_of__markRegistry_0(),
	PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5::get_offset_of__container_1(),
	PrefabResourceSingletonProviderCreator_t95A15359C8CBD521CEA9FB52AAA86C3E4E1D57D5::get_offset_of__prefabCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7096 = { sizeof (PrefabId_t5F7794FBC5FD6018155AB01F0387E08A3E18F9B8), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7096[2] = 
{
	PrefabId_t5F7794FBC5FD6018155AB01F0387E08A3E18F9B8::get_offset_of_ConcreteIdentifier_0(),
	PrefabId_t5F7794FBC5FD6018155AB01F0387E08A3E18F9B8::get_offset_of_ResourcePath_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7097 = { sizeof (PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7097[3] = 
{
	PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F::get_offset_of__markRegistry_0(),
	PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F::get_offset_of__container_1(),
	PrefabSingletonProviderCreator_t4DE479ABFD2C632A118AA3AE3BAE3D4DF8A9506F::get_offset_of__prefabCreators_2(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7098 = { sizeof (PrefabId_t996F45923747785EF556C15C030AF54D0464B346), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7098[2] = 
{
	PrefabId_t996F45923747785EF556C15C030AF54D0464B346::get_offset_of_ConcreteIdentifier_0(),
	PrefabId_t996F45923747785EF556C15C030AF54D0464B346::get_offset_of_Prefab_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7099 = { sizeof (SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3), -1, 0, 0 };
extern const int32_t g_FieldOffsetTable7099[2] = 
{
	SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3::get_offset_of_ConcreteType_0(),
	SingletonId_t688C4C4A8CDD9C7CA54DBA5F829AE333F8947DF3::get_offset_of_ConcreteIdentifier_1(),
};
#ifdef __clang__
#pragma clang diagnostic pop
#endif
